/**
 * 
 */
package com.temp.utils;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author thangnn
 *
 */
public class TimestampUtils {
	static String FormateerddMMYYYY = "dd/MM/yyyy";
	static String FormateerddMMYYYY_FileName = "ddMMyyyy";

	static String FormateerddMMMyy = "dd-MMM-yy";

	static String FormateerddMMYYYYHHmmss = "dd/MM/yyyy HH:mm:ss";
	static String FormateerddMMYYYYHHmmssz = "dd/MM/yyyy HH:mm:ss z";

	static String FormateerddMMYYYYHHmm = "dd/MM/yyyy HH:mm";
	static String FormateerddMMYYYYHHmmssA = "dd/MM/yyyy HH:mm:ss a";

	/**
	 * convert date to TimeStamp sql
	 * 
	 * @param inputDate
	 * @return Date Utils
	 */
	public static Timestamp DateUtilsParseTimeStamp(Date date) throws ParseException {

		return new Timestamp(date.getTime());

	}

	/**
	 * convert string dd/MM/yyyy to Timestamp
	 * 
	 * @param ddMMyyyy
	 * @return
	 * @throws ParseException
	 */
	public static Date StringtoDate_ddMMyyyy(String ddMMyyyy) throws ParseException {
		Date date = new Date();
		try {
			date = new SimpleDateFormat(FormateerddMMYYYY).parse(ddMMyyyy);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return date;
	}

	public static Timestamp StringParseTimeStamp(String ddMMyyyy) throws ParseException {
		if(ddMMyyyy != null && ddMMyyyy != "") {
			return new Timestamp(DateTimeStringToParseUtils.StringPaserDateSQL_ddMMyyyy(ddMMyyyy).getTime());
		}
		return null;
	}

	/**
	 * convert string dd/MM/yyyy hh:MM:ss to Timestamp
	 * 
	 * @param ddMMyyyyhhMMss
	 * @return
	 * @throws ParseException
	 */
	public static Timestamp StringParseTimeStampWithTime(String ddMMyyyyhhMMss) throws ParseException {
		return new Timestamp(DateTimeStringToParseUtils.StringPaserDateSQL_ddMMyyyhhMMss(ddMMyyyyhhMMss).getTime());
	}

	/**
	 * Convert string type 'yyyy-MM-dd hh:MM:ss' to Timestamp format 'dd-MM-yyyy
	 * hh:MM:ss'
	 * 
	 * @param strDate
	 * @return
	 * @throws Exception
	 */
	public static Timestamp convertStringToTimestampFormat(String strDate) throws Exception {
		if (strDate != null && strDate.length() > 0) {
			DateFormat formatter = new SimpleDateFormat(DateTimeStringToParseUtils.FormateerddMMYYYYHHmmss);
			Date date = formatter.parse(strDate);
			Timestamp timeStampDate = new Timestamp(date.getTime());
			return timeStampDate;
		}
		//System.out.println("trống "+ strDate);
		return null;
	}

	public static Timestamp convertStringToTimestampFormat_ddMMYYYY(String strDate) throws Exception {
		DateFormat formatter = new SimpleDateFormat(DateTimeStringToParseUtils.FormateerddMMYYYY);
		Date date = formatter.parse(strDate);
		Timestamp timeStampDate = new Timestamp(date.getTime());
		return timeStampDate;
	}

	public static String DateToString_ddMMyyyy(Date ts) {
		Date date = new Date();
		if(ts != null) {
			date.setTime(ts.getTime());
			return new SimpleDateFormat(FormateerddMMYYYY).format(date);
		}
		return null;
	}

	public static String DateToString_ddMMyyyyHHmmss(Date ts) {
		Date date = new Date();
		date.setTime(ts.getTime());
		return new SimpleDateFormat(FormateerddMMYYYYHHmmss).format(date);
	}

	public static String TimestampToString_ddMMyyyy(Timestamp ts) {
		if (ts != null) {
			Date date = new Date();
			date.setTime(ts.getTime());
			return new SimpleDateFormat(FormateerddMMYYYY).format(date);
		}
		return null;
	}

	public static String TimestampToString_ddMMyyyyHHmm(Timestamp ts) {
		Date date = new Date();
		date.setTime(ts.getTime());
		return new SimpleDateFormat(FormateerddMMYYYYHHmm).format(date);
	}

	public static String TimestampToString_ddMMyyyyHHmmFileName(Timestamp ts) {
		Date date = new Date();
		date.setTime(ts.getTime());
		return new SimpleDateFormat(FormateerddMMYYYY_FileName).format(date);
	}

	public static String TimestampToString_ddMMyyyyHHmmss(Timestamp ts) {
		Date date = new Date();
		date.setTime(ts.getTime());
		return new SimpleDateFormat(FormateerddMMYYYYHHmmss).format(date);
	}

	public static String getDateCurrentTypeString_ddMMMyyy() {
		Date date = new Date();
		try {
			if (date != null) {
				SimpleDateFormat formatter = new SimpleDateFormat(FormateerddMMMyy);
				String strDate = formatter.format(date);
				return strDate;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return "";
	}

	public static Timestamp getCurrentTimestamp() {
		// Date object
		Date date = new Date();
		// getTime() returns current time in milliseconds
		long time = date.getTime();
		// Passed the milliseconds to constructor of Timestamp class
		Timestamp ts = new Timestamp(time);
		return ts;
	}

	public static Time StringToSqlTime(String date) {
		if (date == null || date.isEmpty()) {
			return null;
		} else {
			SimpleDateFormat formatter = new SimpleDateFormat(FormateerddMMYYYYHHmmssA);
			Time timeValue = null;
			try {
				timeValue = new java.sql.Time(formatter.parse(date).getTime());
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return timeValue;
		}
	}
}
