package com.temp.utils;

public interface native_query {
 
		String SQL_DROPDOWN = "select " + 
				"ID," + 
				"CASE LEVEL" + 
				"  WHEN 1 THEN ''||TEN_CHUC_NANG" + 
				"  WHEN 2 THEN '--'||TEN_CHUC_NANG" + 
				"  WHEN 3 THEN '----'||TEN_CHUC_NANG" + 
				"  WHEN 4 THEN '------'||TEN_CHUC_NANG" + 
				"  WHEN 5 THEN '--------'||TEN_CHUC_NANG" + 
				"  WHEN 6 THEN '----------'||TEN_CHUC_NANG" + 
				"  ELSE 'T*' END AS TEN_CHUC_NANG " + 
				"FROM QT_CHUC_NANG " +
				"START WITH KHOA_CHA_ID = 0 " + 
				"connect by PRIOR ID = KHOA_CHA_ID";      
		
		
		String SQL_QTCN = "SELECT" + 
				"	ID ," + 
				"	CASE LEVEL" + 
				"	  WHEN 1 THEN ''||TEN_CHUC_NANG" + 
				"	  WHEN 2 THEN '--'||TEN_CHUC_NANG" + 
				"	  WHEN 3 THEN '----'||TEN_CHUC_NANG" + 
				"	  WHEN 4 THEN '------'||TEN_CHUC_NANG" + 
				"	  WHEN 5 THEN '--------'||TEN_CHUC_NANG" + 
				"	  WHEN 6 THEN '----------'||TEN_CHUC_NANG" + 
				"	  ELSE 'T*' END AS TEN_CHUC_NANG," + 
				"	KHOA_CHA_ID ," + 
				"	MA_CHUC_NANG ," + 
				"	ACTION ," + 
				"	ICON ," + 
				"	IS_MENU ," + 
				"	IS_CHI_TIET ," + 
				"	PHAN_HE ," + 
				"	THU_TU ," + 
				"	GHI_CHU ," + 
				"	NGUOI_TAO_ID ," + 
				"	NGAY_TAO ," + 
				"	NGUOI_CAP_NHAT_ID ," + 
				"	NGAY_CAP_NHAT ," + 
				"	IS_DELETED ," + 
				"	TRANG_THAI, " + 
				"	LEVEL " +
				"FROM" + 
				"	QT_CHUC_NANG " + 
				"START WITH" + 
				"	KHOA_CHA_ID = 0 " + 
				"CONNECT BY " + 
				"	PRIOR ID = KHOA_CHA_ID" ;
		
		String SQL_QTCN1 = "SELECT" + 
				"	ID ," + 
				"   TEN_CHUC_NANG," + 
				"	KHOA_CHA_ID ," + 
				"	MA_CHUC_NANG ," + 
				"	ACTION ," + 
				"	ICON ," + 
				"	IS_MENU ," + 
				"	IS_CHI_TIET ," + 
				"	PHAN_HE ," + 
				"	THU_TU ," + 
				"	GHI_CHU ," + 
				"	NGUOI_TAO_ID ," + 
				"	NGAY_TAO ," + 
				"	NGUOI_CAP_NHAT_ID ," + 
				"	NGAY_CAP_NHAT ," + 
				"	IS_DELETED ," + 
				"	TRANG_THAI "+
				"FROM" + 
				"	QT_CHUC_NANG U ";
	
	
		
		
	String SQL_DROPDOWN_QTNND = "select " + 
				"ID," + 
				"CASE LEVEL" + 
				"  WHEN 1 THEN ''||TEN_CHUC_NANG" + 
				"  WHEN 2 THEN '--'||TEN_CHUC_NANG" + 
				"  WHEN 3 THEN '----'||TEN_CHUC_NANG" + 
				"  WHEN 4 THEN '------'||TEN_CHUC_NANG" + 
				"  WHEN 5 THEN '--------'||TEN_CHUC_NANG" + 
				"  WHEN 6 THEN '----------'||TEN_CHUC_NANG" + 
				"  ELSE 'T*' END AS TEN_CHUC_NANG," + 
				"KHOA_CHA_ID " + 
				"FROM QT_CHUC_NANG " +
				"START WITH KHOA_CHA_ID = 0 " + 
				"connect by PRIOR ID = KHOA_CHA_ID";      
				 
}
