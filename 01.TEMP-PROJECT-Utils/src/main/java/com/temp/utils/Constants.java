package com.temp.utils;

public interface Constants {

	String KEY_BASE_VUE = "base.vue.url";
	String KEY_BASE_GOOGLE = "google.doc.iframe";
	String BaseUrl = "http://localhost:3500";

	interface Messages {

		String CREATE_SUCCESS = "Tạo mới thành công";
		String CREATE_FAIL = "Tạo mới thất bại";
		String CREATE_EXPRESSION = "Vui lòng thiết lập công thức";
		String CELL_SHEET_EXIST = "Ô bạn vừa thiết lập đã tồn tại ";
		String RC_NOT_EXIST = "Bản ghi không tồn tại";
		String GET_CODONG_DAIDIEN_ID = "Loại hình phải là tổ chức";
		String RC_EXIST = "Bản ghi đã tồn tại";
		String MA_EXIST = "Mã đã tồn tại";
		String RC_EXIST_MA_CONG_TY = "Mã công ty đã tồn tại";
		String UPDATE_FAIL = "Cập nhật không thành công ";
		String UPDATE_CELL_VALI_FAIL = "Chỉ cập nhật được khi từ hàng đến, hàng cùng giá trị ban đầu!";
		String MEGER_CELL_FAIL = "Ô bạn chọn đã được sử dụng hoặc không đủ thông tin gộp ô, vui lòng kiểm tra lại";

		String EXCEPTION_FAIL = "Thao tác thất bại, đã xảy ra ngoại lệ ";
		String RC_USED = "Hệ thống không cho phép xoá vì thông tin này đang được sử dụng tại chức năng khác";
		String KEYSORT_FAIL = "Truyền sai từ khóa sắp xếp";
		String DELETE_SUCCESS = "Xóa thành công";
		String DELETE_FAIL = "Xóa thất bại";
		String USE_CODONG = "Bản ghi đang được sử dụng";
		String USE_BM = "Biểu mẫu đã được sử dụng!";

		String GETBYID_FAIL = "lấy theo id thất bại";
		String GETBYID_SUCCESS = "lấy theo id thành công";

		String LIST = "Danh sách";
		String UPDATE = "Cập nhật";
		String DELETE = "Xóa";
		String EXCEPTION_RULE_DOC = "Có lỗi đọc file";
		String EXCEPTION_EXPORT_EXCEL = "Có lỗi khi xuất file";
		
		// QLRR
		String INVALID_TRONG_SO = "Trọng số của tất cả các nhóm chỉ tiêu ở trạng thái hoạt động < hoặc = 100 (%)";
		String INVALID_YEU_TO_LQ = "Không cho phép chọn trùng Yếu tố liên quan";
		String INVALID_DETETE = "Hệ thống không cho phép xóa nhóm chỉ tiêu vì đang có dữ liệu liên quan.";
		
		// Canh bao chi tieu
		String NO_CBCT_VIPHAM_UPDATE = "Hệ thống không cho phép cập nhật. Chỉ tiêu cảnh báo hiện tại đang có dữ liệu vi phạm liên quan.";
		String NO_CBCT_VIPHAM_DELETE = "Hệ thống không cho phép xóa. Chỉ tiêu cảnh báo hiện tại đang có dữ liệu vi phạm liên quan.";
		String NO_CBCT_CTDK_DELETE = "Hệ thống không cho phép xóa. Chỉ tiêu cảnh báo hiện tại đang có điều kiện cảnh báo.";
		String ACTION_VIPHAM_EXCEPTION_MSG = "Hệ thống không thể kích hoạt các chỉ tiêu cảnh báo do đã xảy ra lỗi hệ thống.";
		String ACTION_VIPHAM_KHONG_SD_EXCEPTION_MSG = "Không thể thực hiện cảnh báo do cảnh báo đang ở trạng thái không sử dụng.";
		String ACTION_VIPHAM_CT_EXCEPTION_MSG = "Công thức chỉ tiêu cảnh báo đang gặp lỗi hoặc không đúng định dạng.";
		String ACTION_VIPHAM_GET_CTCK_EXCEPTION_MSG = "Không tìm thấy thông tin Công ty Chứng Khoán gửi báo cáo theo Kỳ/năm báo cáo.";
		String ACTION_VIPHAM_NO_DK_EXCEPTION_MSG = "Chỉ tiêu cảnh báo này đang không có điều kiện để so sánh.";
		String ACTION_VIPHAM_WRONG_YEAR_EXCEPTION_MSG = "Năm cảnh báo không được để trống.";
		String ACTION_VIPHAM_CHECK_DK_EXCEPTION_MSG = "Thực hiện cảnh báo xảy ra lỗi khi thực hiện so sánh với điều kiện.";
		String ACTION_VIPHAM_CALCULATE_GT_EXCEPTION_MSG = "Thực hiện cảnh báo xảy ra lỗi khi thực hiện tính toán giá trị theo công thức.";
		String TH_CANHBAO_SUCCESS = "Hệ thống thực hiện cảnh báo thành công.";
		
		// Bao cao dinh ky
		String USER_ISNOT_CTCK = "Người dùng đang không thuộc Công ty chứng khoán thành viên nào.";
		String BCDK_BCTHANHVIEN_NOT_UPDATE = "Gửi báo cáo định kỳ: Không thể lưu thông tin báo cáo thành viên.";
		String BCDK_SEND_BC_GT_ERROR = "Gửi báo cáo định kỳ không thành công.";
		String BCDK_BC_GT_SENT = "Báo cáo này đã được gửi!";
		String USER_ISNOT_CTCK_NOTUSE = "Người dùng bắt buộc phải thuộc Công ty Chứng khoán để có thể sử dụng chức năng này.";
		
		String BM_BAOCAO_NOT_MATCH = "File biểu mẫu báo cáo đăng tải không đúng định dạng với biểu mẫu đã chọn.";
		
		// Bao cao dinh ky - update
		String BAOCAO_UPDATE_DATA_NF = "Không tìm thấy dữ liệu Kỳ báo cáo/ Báo cáo liên quan.";
		String BAOCAO_UPDATE_THANHVIEN_NF = "Không tìm thấy dữ liệu Báo cáo Thành viên.";
		String BAOCAO_UPDATE_DA_GUI = "Không cho phép cập nhật báo cáo đã gửi.";
		String BAOCAO_SENT = "Báo cáo này đã được gửi.";
		String BAOCAO_LOCKED = "Báo cáo này đã bị khóa.";
		
		// Bieu mau bao cao
		String DELETE_BM_HAS_VERSION = "Không xóa được vì đã lưu thành phiên bản";
		
		//Lưu trữ báo cáo tổng hợp
		
	}

	interface Logs {
		String EXITS="Kiểm tra tồn tại mã";
		String LIST = "Danh sách";
		String INIT_CREATE = "Khởi tạo màn hình thêm";
		String CREATE = "Tạo mới";
		String UPDATE = "Cập nhật";
		String DELETE = "Xóa";
		String GETBYID = "Lấy danh sách theo Id ";
		String CREATE_EXPRESSION_CHECK = "Thêm mới công thức tính cell";
		String TH_CANHBAO_CT = "Thực hiện cảnh báo";
		String CHECK_USER_NHAC_VIEC = "Check User Nhắc việc";
		String FUNCTION = "Danh sách chức năng được phân quyền";
		String SYNC_NHNCK = "Đồng bộ người hành nghề";
		String CHECK_USER = "Check User Loging";
	}

	/// TYPE log hệ thống
	interface LogSystem {

		String LOG_SYS_LOGIN = "Login";
		String LOG_SYS_LOGOUT = "Logout";

		// Chức vụ
		String LOG_SYS_ADD = "ADD";
		String LOG_SYS_UPDATE = "UPDATE";
		String LOG_SYS_DELETE = "DELETE";

	}

	/// Folder Upload
	interface FolderUpload {
		//common
		String FOLDER_NGUOIDUNG = "uploadFile\\nguoidung\\";
		String FOLDER_BAIVIET = "uploadFile\\baiviet\\";
		String FOLDER_TAILIEU = "uploadFile\\tailieu\\";
		// Thông tin trợ giup
		String FOLDER_TTTG = "uploadFile\\thongtintrogiup\\";
		//Lưu trữ thư mục
		String FOLDER_LTTM = "uploadFile\\luutruthumuc\\";
		
		
		// Công ty chứng khoán
		String F_LICHKIEMTRA = "uploadFile\\lichkiemtra\\";
		// Mối quan hệ cổ đông
		String FOLDER_MQH = "uploadFile\\codongmqh\\";
		String FOLDER_CD_DD = "uploadFile\\codongdd\\";
		String FOLDER_TBCN = "uploadFile\\codongtbcn\\";
		String FOLDER_XLHC = "uploadFile\\xulyhanhchinh\\";
		String FOLDER_CTCK_THONGTIN = "uploadFile\\ctckthongtin\\";
		String FOLDER_CTCK_DICHVU = "uploadFile\\ctckdichvu\\";
		String FOLDER_CTCK_NHANSU = "uploadFile\\ctcknhansucaocap\\";

		// Hồ sơ chi nhánh
		String FOLDER_HSCN = "uploadFile\\hschinhanh\\";
		String FOLDER_HSGD = "uploadFile\\hsgiaodich\\";
		String FOLDER_HSDD = "uploadFile\\hsvpdaidien\\";
		String FOLDER_CBTT = "uploadFile\\cbttbaocao\\";
		String FOLDER_GDCD = "uploadFile\\cbttcodong\\";
		String FOLDER_BMBAOCAO = "uploadFile\\bmbaocao\\";
		String FOLDER_BMBAOCAODINHKY = "uploadFile\\bmbaocaodinhky\\";

		// Tra cứu văn bản pháp luật
				String FOLDER_VBPL = "uploadFile\\vanbanphapluat\\";
	}

	/// TYPE log hệ thống
	interface ByPassAuthor {
		// UPLOAD FILE
		String UPLOAD_FILE = "/api/v1/uploadMultipleFiles";
		// LOG OUT
		String LOG_OUT = "/logmeout";
		// DOWNLOAD FILE
		String DOWNLOAD_FILE = "/api/v1/downloadFile/";
		// REFRESH TOKEN
		String REFRESH_TOKEN = "/refreshToken";
		// UPLOAD FILE IMPORT
		String UPLOAD_FILE_IMPORT = "/api/v1/uploadMultipleFilesImport";

	}

	/// folder save export file tren server
	interface FolderExport {
		// mối quan hệ cổ đông
		String FOLDER_EXPORT_CD = "uploadFile\\exportcodong\\";
		String FOLDER_EXPORT_CD_DD = "uploadFile\\exportcodongdd\\";
		String FOLDER_EXPORT_TBCN = "uploadFile\\exportcodongtbcn\\";

		String FOLDER_CTCK_THONGTIN = "uploadFile\\ctckthongtin\\";
		String FOLDER_NGUOIDUNG = "uploadFile\\nguoidung\\";
		String FOLDER_VBKS = "uploadFile\\vanbankyso\\";

		// hồ sơ chi nhánh
		String FOLDER_HSCN = "uploadFile\\hschinhanh\\";
	}

}
