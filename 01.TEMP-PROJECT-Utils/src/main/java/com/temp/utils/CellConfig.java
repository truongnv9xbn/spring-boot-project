/**
 * 
 */
package com.temp.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;

public class CellConfig {
	public static List<IndexedColors> list;

	static {
		list = new ArrayList<>();
		IndexedColors[] itemColor = IndexColorList();

		Collections.addAll(list, itemColor);
	}

	public static CellStyle createCellStyle(Sheet sheet, Workbook workbook, boolean isSetBold) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set border
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(isSetBold);
		font.setFontHeightInPoints((short) 13);
		headerCellStyle.setFont(font);
		
		for(int i =0; i < 100; i++) {
			sheet.autoSizeColumn(i);
		}
		// set alignment
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		return headerCellStyle;
	}
	
	public static CellStyle createCellStyle2(Sheet sheet, Workbook workbook, boolean isSetBold) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set border
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(isSetBold);
		font.setFontHeightInPoints((short) 13);
		headerCellStyle.setFont(font);
		
		for(int i =0; i < 100; i++) {
			sheet.autoSizeColumn(i);
		}
		// set alignment
		headerCellStyle.setAlignment(HorizontalAlignment.LEFT);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);
		headerCellStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.index);

		return headerCellStyle;
	}
	

	public static CellStyle createStyleForTitleTBCN(Sheet sheet, Workbook workbook) {
		Font headerFont = workbook.createFont();
		headerFont.setColor(IndexedColors.WHITE.index);

		// IndexedColors[] s = IndexColorList();
//		//System.out.println(s);
//		list = new ArrayList<>();
//		IndexedColors[] itemColor = IndexColorList();
//
//		Collections.addAll(list, itemColor);
//		//System.out.println(list);
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();
		// fill foreground color ...
		headerCellStyle.setFillForegroundColor(IndexedColors.BLUE_GREY.index);
		// headerCellStyle.setFillForegroundColor(list.get(2).index);
		// and solid fill pattern produces solid grey cell fill
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		
		for(int i =0; i < 100; i++) {
			sheet.autoSizeColumn(i);
		}
		
		headerCellStyle.setWrapText(true);
//		style.setColor(IndexedColors.BLACK.getIndex());

		return headerCellStyle;
	}
	
	public static CellStyle createStyleForTitleTBCN2(Sheet sheet, Workbook workbook) {
		Font headerFont = workbook.createFont();
		headerFont.setColor(IndexedColors.WHITE.index);

		// IndexedColors[] s = IndexColorList();
//		//System.out.println(s);
//		list = new ArrayList<>();
//		IndexedColors[] itemColor = IndexColorList();
//
//		Collections.addAll(list, itemColor);
//		//System.out.println(list);
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();
		// fill foreground color ...
		headerCellStyle.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.index);
		// headerCellStyle.setFillForegroundColor(list.get(2).index);
		// and solid fill pattern produces solid grey cell fill
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setFont(headerFont);
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		
		for(int i =0; i < 100; i++) {
			sheet.autoSizeColumn(i);
		}
		
		headerCellStyle.setWrapText(true);
//		style.setColor(IndexedColors.BLACK.getIndex());

		return headerCellStyle;
	}

	public static CellStyle createStyleForTitle(Sheet sheet, Workbook workbook) {
		Font headerFont = workbook.createFont();
		headerFont.setColor(IndexedColors.WHITE.index);

		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		headerFont.setFontHeightInPoints((short) 20);
		headerFont.setFontName("Arial");
		headerFont.setColor(IndexedColors.WHITE.getIndex());
		headerFont.setBold(true);
		headerFont.setItalic(false);

		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
//		style.setColor(IndexedColors.BLACK.getIndex());

		return headerCellStyle;
	}

	public static IndexedColors[] IndexColorList() {
		IndexedColors[] s = IndexedColors.values();
		return s;
	}

	public static Cell BorderTable(Cell cell, Sheet sheet) {

		// fill foreground color ...
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setWrapText(true);
		cell.setCellStyle(cellStyle);
//		style.setColor(IndexedColors.BLACK.getIndex());

		return cell;
	}

	public static void autoSizeColumns(Workbook workbook) {
		int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				Row row = sheet.getRow(sheet.getFirstRowNum());
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					int columnIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columnIndex);
					if (sheet.getColumnWidth(columnIndex) < 2000) {
						sheet.setColumnWidth(columnIndex, 4000);
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param sheet
	 * @param workbook
	 * @return number format #,#0.00
	 */
	public static CellStyle createCellStyleNumber(Sheet sheet, Workbook workbook) {
		CellStyle style = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		// font.setBold(isSetBold);
		font.setFontHeightInPoints((short) 13);
		font.setBold(false);

		// set border
		style.setBorderBottom(BorderStyle.THIN);
		style.setBorderTop(BorderStyle.THIN);
		style.setBorderRight(BorderStyle.THIN);
		style.setBorderLeft(BorderStyle.THIN);
		style.setFillBackgroundColor(IndexedColors.GREEN.getIndex());

		DataFormat df = workbook.createDataFormat();

		style.setDataFormat(df.getFormat("#,#0.00"));

		// style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
		// style.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));

		// set font
		style.setFont(font);
		style.setAlignment(HorizontalAlignment.LEFT);
		style.setVerticalAlignment(VerticalAlignment.TOP);

		return style;
	}
}
