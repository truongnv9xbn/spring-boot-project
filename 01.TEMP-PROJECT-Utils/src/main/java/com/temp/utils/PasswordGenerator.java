package com.temp.utils;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Random;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

import org.apache.tomcat.util.codec.binary.Base64;

/*we can use 'MD5 password encoder' or 'SHA encryption algorithms', but spring recommend 
 * us to use 'BCryptPasswordEncoder' a more stable and strong encryption algorithms.*/
public class PasswordGenerator {
	private static final int strength = 12;

	private static final String UNICODE_FORMAT = "UTF8";
	public static final String DESEDE_ENCRYPTION_SCHEME = "DESede";
	private static Cipher cipher;
	static byte[] arrayBytes;
	static SecretKey key;

	private static KeySpec ks;
	private static SecretKeyFactory skf;

	private static String myEncryptionKey;
	private static String myEncryptionScheme;

	static {
		myEncryptionKey = "ThisIsSpartaThisIsSparta";
		myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
		try {
			arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
		} catch (UnsupportedEncodingException e3) {
			e3.printStackTrace();
		}
		try {
			ks = new DESedeKeySpec(arrayBytes);
		} catch (InvalidKeyException e2) {
			e2.printStackTrace();
		}
		try {
			skf = SecretKeyFactory.getInstance(myEncryptionScheme);
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		try {
			cipher = Cipher.getInstance(myEncryptionScheme);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		}
		try {
			key = skf.generateSecret(ks);
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}

	public static String encrypt(String unencryptedString) {
		String encryptedString = null;
		try {
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plainText = unencryptedString.getBytes(UNICODE_FORMAT);
			byte[] encryptedText = cipher.doFinal(plainText);
			encryptedString = new String(Base64.encodeBase64(encryptedText));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encryptedString;
	}

	public static String decrypt(String encryptedString) {
		String decryptedText = null;
		try {
			cipher.init(Cipher.DECRYPT_MODE, key);
			byte[] encryptedText = Base64.decodeBase64(encryptedString);
			byte[] plainText = cipher.doFinal(encryptedText);
			decryptedText = new String(plainText);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return decryptedText;
	}

	// BCryptPasswordEncoder method
//	public static boolean checkHashStrings(String rawPassword, String encodedPassword) {
//		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(strength);
//		return passwordEncoder.matches(rawPassword, encodedPassword);
//	}

	// generate 6 characters 0-9 and a-zA-Z for password
	public static String generateRandomPassword() {
		final String alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdeghijklmnopqrstuvwxyz";
		final int N = alphabet.length();

		Random r = new Random();
		StringBuffer output = new StringBuffer();
		for (int i = 0; i < 8; i++) {
			output.append(alphabet.charAt(r.nextInt(N)));
		}

		return output.toString();
	}

//	public static void main(String[] args) throws JsonProcessingException {
//		String generateRandomPassword = PasswordGenerator.generateRandomPassword();
//		//System.out.println(generateRandomPassword);
//	//	//System.out.println("pttLVT2U");
////		String encode = "3T6CWeoDqUaJJSpQl9fUJg==";
////
////		String decode = decrypt(encode);
//
//	//	//System.out.println(decode);
//	}
}
