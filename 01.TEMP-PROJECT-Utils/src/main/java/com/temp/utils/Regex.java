/**
 * 
 */
package com.temp.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
	private static final String Regex = "^[a-z0-9]$";
	private static Pattern pattern;
	private static Matcher matcher;

	public static boolean kywork(String kywork) {
		pattern = Pattern.compile(Regex);
		matcher = pattern.matcher(kywork);
		return matcher.matches();
	}

}
