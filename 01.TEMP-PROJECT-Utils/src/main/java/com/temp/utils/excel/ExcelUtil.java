/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.utils.excel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



/**
 * manhnn1
 */
public class ExcelUtil<T> {

	private static String FOLDER_TEMPLATE = "/report/";
	private static String EXPORT_SINGLE_SHEET = "export_single_sheet";
	private static String IMPORT_TEMPLATE = "import_template";
	private static int TITLE_ROW = 0;
	private static int HEADER_ROW = 3;
	private static int DATA_ROW_START = 4;
	private static int COL_START = 0;
	private DataFormat df;
	private Workbook wb;
	private XSSFWorkbook xwb;
	private Map<Integer, String[]> convertMap;
	private ArrayList<Workbook> listWB;

	public ExcelUtil() {
	}

	public ExcelUtil(Map<Integer, String[]> convertMap) {
		this.convertMap = convertMap;
	}

	/*
	 * public void toSingleSheetXlsx(String fileName, String title,
	 * List<Object[]> headerInfors, List<String> properties, List<T>
	 * datas,HttpServletRequest request) throws Exception { try { String tmpDir
	 * = StaticUtil.getTmpDir();
	 * 
	 * ServletContext svc = getServletContext(request);
	 * 
	 * StringBuilder fileInput = new StringBuilder(); StringBuilder fileOutput =
	 * new StringBuilder();
	 * 
	 * fileInput.append(EXPORT_SINGLE_SHEET); fileInput.append(StringPool.XLSX);
	 * 
	 * fileOutput.append(fileName); fileOutput.append(StringPool.UNDERLINE);
	 * fileOutput.append(TimeUtil.getShortTimestamp());
	 * fileOutput.append(StringPool.UNDERLINE); fileOutput.append(1);
	 * fileOutput.append(StringPool.XLSX);
	 * 
	 * String pathInput = svc.getRealPath(FOLDER_TEMPLATE +
	 * fileInput.toString());
	 * 
	 * // String pathOut = StaticUtil.getTmpDir() + StringPool.SLASH // +
	 * fileOutput.toString();
	 * 
	 * String pathOut = tmpDir + StringPool.SLASH +
	 * FileUtil.getAutoIncrementName(tmpDir, fileOutput.toString()); //transform
	 * data this.read(pathInput);
	 * 
	 * //transform data this.transformerXlsx(Values.FIRST_INDEX, title,
	 * headerInfors, properties, datas); //write file File fileOut = new
	 * File(pathOut);
	 * 
	 * OutputStream outputStream = new FileOutputStream(fileOut);
	 * 
	 * wb.write(outputStream);
	 * 
	 * outputStream.close(); //download FileUtil.download(fileOut); } catch
	 * (Exception ex) { _log.error(ex.getMessage(), ex); } }
	 * 
	 * public void createImportTemplate(String fileName, List<Object[]>
	 * headerInfors,HttpServletRequest request) throws Exception { try { String
	 * tmpDir = StaticUtil.getTmpDir();
	 * 
	 * ServletContext svc = getServletContext(request);
	 * 
	 * StringBuilder fileInput = new StringBuilder(); StringBuilder fileOutput =
	 * new StringBuilder();
	 * 
	 * fileInput.append(IMPORT_TEMPLATE); fileInput.append(StringPool.XLSX);
	 * 
	 * fileOutput.append(fileName); fileOutput.append(StringPool.UNDERLINE);
	 * fileOutput.append(TimeUtil.getShortTimestamp());
	 * fileOutput.append(StringPool.UNDERLINE); fileOutput.append(1);
	 * fileOutput.append(StringPool.XLSX);
	 * 
	 * String pathInput = svc.getRealPath(FOLDER_TEMPLATE +
	 * fileInput.toString());
	 * 
	 * // String pathOut = StaticUtil.getTmpDir() + StringPool.SLASH // +
	 * fileOutput.toString();
	 * 
	 * String pathOut = tmpDir + StringPool.SLASH +
	 * FileUtil.getAutoIncrementName(tmpDir, fileOutput.toString()); //transform
	 * data this.read(pathInput);
	 * 
	 * //transform data this.transformerXlsx(Values.FIRST_INDEX, headerInfors);
	 * //write file File fileOut = new File(pathOut);
	 * 
	 * OutputStream outputStream = new FileOutputStream(fileOut);
	 * 
	 * wb.write(outputStream);
	 * 
	 * outputStream.close(); //download FileUtil.download(fileOut); } catch
	 * (Exception ex) { _log.error(ex.getMessage(), ex); } }
	 * 
	 * public void transformerXlsx(int sheetPosition, String title,
	 * List<Object[]> headerInfors, List<String> properties, List<T> datas)
	 * throws Exception { try { Sheet sheet = wb.getSheetAt(sheetPosition);
	 * 
	 * int endCol = headerInfors.size() - 1;
	 * 
	 * //get cell style CellStyle titleCellStyle = getCellStyle(sheet,
	 * TITLE_ROW, COL_START); CellStyle headerCellStyle = getCellStyle(sheet,
	 * HEADER_ROW, COL_START); CellStyle dataCellStyle = getCellStyle(sheet,
	 * DATA_ROW_START, COL_START);
	 * 
	 * //title createTitle(sheet, title, titleCellStyle, TITLE_ROW, TITLE_ROW,
	 * COL_START, endCol);
	 * 
	 * //header createHeader(sheet, headerCellStyle, headerInfors, HEADER_ROW);
	 * 
	 * //data createData(sheet, dataCellStyle, properties, datas,
	 * DATA_ROW_START); } catch (Exception ex) { _log.error(ex.getMessage(),
	 * ex); } }
	 * 
	 * public void transformerXlsx(int sheetPosition, List<Object[]>
	 * headerInfors) throws Exception { try { Sheet sheet =
	 * wb.getSheetAt(sheetPosition);
	 * 
	 * CellStyle headerCellStyle = getCellStyle(sheet, TITLE_ROW, COL_START);
	 * 
	 * //header createHeader(sheet, headerCellStyle, headerInfors, TITLE_ROW); }
	 * catch (Exception ex) { _log.error(ex.getMessage(), ex); } }
	 */
	public Workbook read(String filePath) throws FileNotFoundException,
			Exception {
		InputStream inp = null;

		try {
			inp = new FileInputStream(filePath);

			wb = WorkbookFactory.create(inp);

			df = wb.createDataFormat();

		} catch (FileNotFoundException fnfe) {
		//	ErrorHelper.PrintStackTrace(this.getClass().getName(), fnfe,
//					"ExcelUtil.read error ");
		} catch (Exception ex) {
//			ErrorHelper.PrintStackTrace(this.getClass().getName(), ex,
//					"ExcelUtil.read error ");
		} finally {
			try {
				inp.close();
			} catch (IOException ex) {
//				ErrorHelper.PrintStackTrace(this.getClass().getName(), ex,
//						"ExcelUtil.read error ");
			}
		}

		return wb;
	}

	public XSSFWorkbook readXLSX(String filePath) throws FileNotFoundException,
			Exception {
		InputStream inp = null;
		try {
			inp = new FileInputStream(filePath);
			xwb = (XSSFWorkbook) WorkbookFactory.create(inp);
			df = xwb.createDataFormat();
		} catch (FileNotFoundException fnfe) {
//			ErrorHelper.PrintStackTrace(this.getClass().getName(), fnfe,
	//				"ExcelUtil.readXLSX error ");
		} catch (Exception ex) {
//			ErrorHelper.PrintStackTrace(this.getClass().getName(), ex,
	//				"ExcelUtil.readXLSX error ");
		} finally {
			try {
				inp.close();
			} catch (IOException ex) {
//				ErrorHelper.PrintStackTrace(this.getClass().getName(), ex,
//						"ExcelUtil.readXLSX error ");
			}
		}
		return xwb;
	}

	public Row getOrCreateRow(Sheet sheet, int rowIndex) {
		Row row = sheet.getRow(rowIndex);

		if (row == null) {
			row = sheet.createRow(rowIndex);
		}

		return row;
	}

	public Cell getOrCreateCell(Row row, int cellIndex) {
		Cell cell = row.getCell(cellIndex);

		if (cell == null) {
			cell = row.createCell(cellIndex);
		}

		return cell;
	}

	public Cell getOrCreateCell(Row row, int cellIndex, CellStyle cellStyle) {
		Cell cell = getOrCreateCell(row, cellIndex);

		if (cellStyle != null) {
			cell.setCellStyle(cellStyle);
		}

		return cell;
	}

	/*
	 * public Cell getOrCreateCell(Row row, int cellIndex, CellStyle cellStyle,
	 * String value) { Cell cell = getOrCreateCell(row, cellIndex, cellStyle);
	 * 
	 * cell.setCellValue(value);
	 * 
	 * return cell; }
	 */

	public Cell getOrCreateCell(Row row, int cellIndex, HorizontalAlignment cellStyle,
			Object value, int... typeFormat) {
		Cell cell = getOrCreateCell(row, cellIndex);

//		if (value != null) {
//			/*
//			 * if (value instanceof Date) {
//			 * 
//			 * if (cellStyle != null) { cell.setCellValue(new Date());
//			 * CreationHelper createHelper = wb.getCreationHelper(); CellStyle
//			 * cellStyle1 = wb.createCellStyle();
//			 * cellStyle.setDataFormat(createHelper
//			 * .createDataFormat().getFormat("MM/dd/yyyy hh:mm:ss"));
//			 * cell.setCellValue(Calendar.getInstance());
//			 * cell.setCellStyle(cellStyle1); }
//			 * 
//			 * }else
//			 */
//			if (value instanceof Double) {
//				cell.setCellValue((Double) value);
//				if (cellStyle != null) {
//					if (typeFormat != null && typeFormat.length > 0) {
//						cellStyle.setDataFormat((short) typeFormat[0]);
//					} else {
//						cellStyle.setAlignment(cellStyle.AL);
//					}
//				}
//			} else if (value instanceof Long) {
//				cell.setCellValue((Long) value);
//				if (cellStyle != null) {
//					cellStyle.setDataFormat((short) 1);
//				}
//			} else {
//				cell.setCellValue(value.toString());
//			}
//			if (cellStyle != null) {
//				cell.setCellStyle(cellStyle);
//			}
//		}

		return cell;
	}

	public Cell getOrCreateCell2(Row row, int cellIndex, CellStyle cellStyle,
			String value) {
		Cell cell = getOrCreateCell(row, cellIndex);

//		if (value != null) {
//			if (cellStyle != null) {
//				Date d = Utils.parseDate(value, "dd/MM/yyyy");
//				cell.setCellValue(d);
//			}
//			if (cellStyle != null) {
//				cell.setCellStyle(cellStyle);
//			}
//		}

		return cell;
	}

	public Row shiftCellToLeft(Row row, int startCol, int endCol) {
		for (int i = startCol + 1; i < endCol; i++) {
//			Cell sourceCell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
//			Cell desCell = row.getCell(i - 1, Row.CREATE_NULL_AS_BLANK);
//
//			desCell.setCellStyle(sourceCell.getCellStyle());
//			desCell.setCellValue(sourceCell.getStringCellValue());
//
//			row.removeCell(sourceCell);
		}

		return row;
	}

	public List<CellStyle> getCellStyles(Row rowRef, int startCol, int endCol) {
		List<CellStyle> styles = new ArrayList<CellStyle>();

		for (int i = startCol; i <= endCol; i++) {
			Cell cell = rowRef.getCell(i);

			styles.add(cell.getCellStyle());
		}

		return styles;
	}

	public CellStyle getCellStyle(Row rowRef, int cellIndex) {
		Cell cell = rowRef.getCell(cellIndex);

		return cell.getCellStyle();
	}

	public CellStyle getCellStyle(Sheet sheet, int rowIndex, int cellIndex) {
		Row row = getOrCreateRow(sheet, rowIndex);

		return getCellStyle(row, cellIndex);
	}

	public Sheet createTitle(Sheet sheet, String title, CellStyle cellStyle,
			int firstRow, int lastRow, int firstCol, int lastCol) {

		sheet.addMergedRegion(new CellRangeAddress(firstRow, lastRow, firstCol,
				lastCol));

		Row row = getOrCreateRow(sheet, firstRow);

//		getOrCreateCell(row, firstCol, cellStyle, title);

		return sheet;
	}

	public Sheet createHeader(Sheet sheet, CellStyle cellStyle,
			List<Object[]> headerInfors, int headRowIndex) {
		Row row = getOrCreateRow(sheet, headRowIndex);

		int size = headerInfors.size();

		// STT
		// getOrCreateCell(row, 0, cellStyle, STT);

		for (int i = 0; i < size; i++) {
			Object[] infor = headerInfors.get(i);

			String label = (String) infor[0];
			Integer width = (Integer) infor[1];

//			getOrCreateCell(row, i, cellStyle, label);

			sheet.setColumnWidth(i, width.intValue());
		}

		return sheet;
	}

	/*
	 * destinationRowNum: vi tri tao dong moi sourceRowNum : vi tri row can copy
	 * style
	 */
	public Row copyRow(Sheet worksheet, int sourceRowNum, int destinationRowNum) {
		// Get the source / new row
		Row newRow = worksheet.getRow(destinationRowNum);
		Row sourceRow = worksheet.getRow(sourceRowNum);

		// If the row exist in destination, push down all rows by 1 else create
		// a new row
		if (newRow != null) {
			worksheet
					.shiftRows(destinationRowNum, worksheet.getLastRowNum(), 1);
			newRow = worksheet.createRow(destinationRowNum);
		} else {
			newRow = worksheet.createRow(destinationRowNum);
		}

		// Loop through source columns to add to new row
		for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
			// Grab a copy of the old/new cell
			Cell oldCell = sourceRow.getCell(i);
			Cell newCell = newRow.createCell(i);

			// If the old cell is null jump to next cell
			if (oldCell == null) {
				newCell = null;
				continue;
			}

			// Use old cell style
			newCell.setCellStyle(oldCell.getCellStyle());

			/*
			 * CellStyle style=worksheet.getWorkbook().createCellStyle();
			 * style.setBorderBottom(CellStyle.BORDER_THIN);
			 * style.setBorderTop(CellStyle.BORDER_THIN);
			 * style.setBorderRight(CellStyle.BORDER_THIN);
			 * style.setBorderLeft(CellStyle.BORDER_THIN);
			 * newCell.setCellStyle(style);
			 */

			// If there is a cell comment, copy
			if (newCell.getCellComment() != null) {
				newCell.setCellComment(oldCell.getCellComment());
			}

			// If there is a cell hyperlink, copy
			if (oldCell.getHyperlink() != null) {
				newCell.setHyperlink(oldCell.getHyperlink());
			}

			// Set the cell data type
			newCell.setCellType(oldCell.getCellType());

			// Set the cell data value
			switch (oldCell.getCellType()) {
			case Cell.CELL_TYPE_BLANK:
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				newCell.setCellValue(oldCell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_ERROR:
				newCell.setCellErrorValue(oldCell.getErrorCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				newCell.setCellFormula(oldCell.getCellFormula());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				newCell.setCellValue(oldCell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_STRING:
				newCell.setCellValue(oldCell.getRichStringCellValue());
				break;
			}
		}
		return newRow;
	}

	/*
	 * public Sheet createData(Sheet sheet, CellStyle cellStyle, List<String>
	 * properties, List<T> datas, int dataRowIndex) throws Exception { try { if
	 * (datas.isEmpty()) { return sheet; }
	 * 
	 * Row row = null;
	 * 
	 * int count = dataRowIndex;
	 * 
	 * CellStyle[] cellStyles = getDataCellStyle(properties, datas.get(0),
	 * cellStyle);
	 * 
	 * for (int i = 0; i < datas.size(); i++) { T data = datas.get(i);
	 * 
	 * row = getOrCreateRow(sheet, count);
	 * 
	 * //STT getOrCreateCell(row, 0, cellStyle, i + 1);
	 * 
	 * Object value = null;
	 * 
	 * for (int j = 0; j < properties.size(); j++) { if(data instanceof Map){
	 * value = ((Map) data).get(properties.get(j)); }else{ value =
	 * PropertyUtils.getProperty( data, properties.get(j)); }
	 * 
	 * if (Validator.isNotNull(convertMap) &&
	 * Validator.isNotNull(convertMap.get(j))) { value = getStringValue((Long)
	 * value, convertMap.get(j)); }
	 * 
	 * getOrCreateCell(row, j + 1, cellStyles[j], value); }
	 * 
	 * count++; } } catch (Exception ex) { _log.error(ex.getMessage(), ex); }
	 * 
	 * return sheet; }
	 * 
	 * public CellStyle[] getDataCellStyle(List<String> properties, T data,
	 * CellStyle baseStyle) throws Exception { CellStyle[] cellStyles = new
	 * CellStyle[properties.size()];
	 * 
	 * try { for (int i = 0; i < properties.size(); i++) { Object value =
	 * PropertyUtils.getProperty( data, properties.get(i));
	 * 
	 * cellStyles[i] = wb.createCellStyle();
	 * 
	 * cellStyles[i].cloneStyleFrom(baseStyle);
	 * 
	 * if (Validator.isNotNull(convertMap) &&
	 * Validator.isNotNull(convertMap.get(i))) {
	 * cellStyles[i].setAlignment(CellStyle.ALIGN_LEFT); } else { cellStyles[i]
	 * = setTextAlign(cellStyles[i], value); } } } catch (Exception ex) {
	 * _log.error(ex.getMessage(), ex); }
	 * 
	 * return cellStyles; }
	 * 
	 * public CellStyle setTextAlign(CellStyle cellStyle, Object data) { if
	 * (data instanceof Long || data instanceof Integer || data instanceof
	 * Double || data instanceof Short) {
	 * cellStyle.setAlignment(CellStyle.ALIGN_RIGHT); } else if (data instanceof
	 * String) { cellStyle.setAlignment(CellStyle.ALIGN_LEFT); } else if (data
	 * instanceof Date) { cellStyle.setAlignment(CellStyle.ALIGN_CENTER);
	 * cellStyle.setDataFormat(df.getFormat(DateUtil.SHORT_DATE_PATTERN)); }
	 * else { cellStyle.setAlignment(CellStyle.ALIGN_LEFT); }
	 * 
	 * return cellStyle; }
	 * 
	 * private String getStringValue(Long index, String[] values) { String
	 * statusName = StringPool.BLANK;
	 * 
	 * if (Validator.isNotNull(index) && Validator.isNotNull(values) && index <
	 * values.length) { statusName = values[index.intValue()]; }
	 * 
	 * return statusName; }
	 */

	private static ServletContext getServletContext(HttpServletRequest request) {
		return request.getSession().getServletContext();
	}

	/*
	 * private static final Logger _log = Logger.getLogger(ExcelUtil.class);
	 */
	public CellStyle createStyleNoBorder(Workbook workbook, XSSFFont hSSFFont) {
		CellStyle styleBorder = workbook.createCellStyle();
		styleBorder.setFont(hSSFFont);
		return styleBorder;
	}

	public CellStyle createStyleBorder(Workbook workbook, XSSFFont hSSFFont) {
		CellStyle styleBorder = workbook.createCellStyle();
//		styleBorder.setFont(hSSFFont);
//		styleBorder.setBorderBottom(CellStyle.BORDER_THIN);
//		styleBorder.setBorderRight(CellStyle.BORDER_THIN);
//		styleBorder.setBorderLeft(CellStyle.BORDER_THIN);
		return styleBorder;
	}

	public CellStyle createStyleHeader(Workbook workbook, XSSFFont hSSFFont,
			short typeAlign, boolean checkBoder) {
		CellStyle styleBorder = workbook.createCellStyle();
//		styleBorder.setAlignment(typeAlign);
//		styleBorder.setFont(hSSFFont);
//		if (checkBoder) {
//			styleBorder.setBorderTop(CellStyle.BORDER_THIN);
//			styleBorder.setBorderBottom(CellStyle.BORDER_THIN);
//			styleBorder.setBorderRight(CellStyle.BORDER_THIN);
//			styleBorder.setBorderLeft(CellStyle.BORDER_THIN);
//		}
		return styleBorder;
	}

	public CellStyle createStyleBorderAlignRight(Workbook workbook,
			XSSFFont hSSFFont) {
		CellStyle styleBorder = workbook.createCellStyle();
//		styleBorder.setAlignment(CellStyle.ALIGN_RIGHT);
//		styleBorder.setFont(hSSFFont);
//		styleBorder.setBorderBottom(CellStyle.BORDER_THIN);
//		styleBorder.setBorderRight(CellStyle.BORDER_THIN);
//		styleBorder.setBorderLeft(CellStyle.BORDER_THIN);
		return styleBorder;
	}

	public CellStyle createStyleBorderAlignCenter(Workbook workbook,
			XSSFFont hSSFFont) {
		CellStyle styleBorder = workbook.createCellStyle();
//		styleBorder.setAlignment(CellStyle.ALIGN_CENTER);
//		styleBorder.setFont(hSSFFont);
//		styleBorder.setBorderBottom(CellStyle.BORDER_THIN);
//		styleBorder.setBorderRight(CellStyle.BORDER_THIN);
//		styleBorder.setBorderLeft(CellStyle.BORDER_THIN);
		return styleBorder;
	}

 
	public String getCellAsString(Cell cell, Workbook wb) {
		/* LocND */
		/* Doc cac cell ra String (doc ca cac cell co formula) */

		if (cell == null)
			return "";

		double dVal;
		BigDecimal bVal;
		String sVal = "";

		FormulaEvaluator formulaEval = wb.getCreationHelper()
				.createFormulaEvaluator();

		try {
			int cType = formulaEval.evaluateInCell(cell).getCellType();

			switch (cType) {
			case Cell.CELL_TYPE_BOOLEAN:
				sVal = formulaEval.evaluate(cell).getBooleanValue() + "";
				break;

			case Cell.CELL_TYPE_NUMERIC:
				dVal = formulaEval.evaluate(cell).getNumberValue();
				if (dVal % 1 == 0) {
					bVal = new BigDecimal(dVal);
				} else {
					bVal = new BigDecimal(dVal).setScale(2,
							BigDecimal.ROUND_HALF_UP);
				}

				sVal = bVal.toPlainString();
				break;

			case Cell.CELL_TYPE_STRING:
				sVal = formulaEval.evaluate(cell).formatAsString();
				break;

			case Cell.CELL_TYPE_BLANK:
				sVal = "";
				break;

			case Cell.CELL_TYPE_ERROR:
				sVal = formulaEval.evaluate(cell).getErrorValue() + "";
				break;

			default:
				sVal = formulaEval.evaluate(cell).formatAsString();
				break;
			}

		} catch (Exception e) {
			// TODO: handle exception
//			ErrorHelper.PrintStackTrace(this.getClass().getName(), e,
//					"getCellAsString error : ");
		}

		return sVal.replace("\"", "");
	}

	public boolean isRowEmpty(Row row) {
		for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
			Cell cell = row.getCell(c);
			if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK)
				return false;
		}
		return true;
	}

	public ArrayList<Workbook> getListWB() {
		return listWB;
	}

	public void setListWB(ArrayList<Workbook> listWB) {
		this.listWB = listWB;
	}

}
