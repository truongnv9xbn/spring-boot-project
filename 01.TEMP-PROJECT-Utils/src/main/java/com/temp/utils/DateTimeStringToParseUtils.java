/**
 * 
 */
package com.temp.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author thangnn
 *
 */
public class DateTimeStringToParseUtils {
	static String FormateerddMMYYYY = "dd/MM/yyyy";

	static String FormateerddMMYYYYHHmmss = "dd/MM/yyyy HH:mm:ss";

	static String FormateerddMMYYYYHHmm = "dd/MM/yyyy HH:mm";

	/**
	 * Date Utils parser string format dd/MM/yyyy
	 * 
	 * @param inputDate
	 * @return Date Utils
	 */
	public static Date StringPaserDateUtils_ddMMyyyy(String inputStringDate) throws ParseException {
		if(inputStringDate!= null) {
			DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYY);
			return dateFormat.parse(inputStringDate);
		}
		return null;

	}

	/**
	 * Date Utils parser string format dd/MM/yyyy HH:mm:ss
	 * 
	 * @param inputDate
	 * @return Date Utils
	 */
	public static Date StringPaserDateUtils_ddMMyyyhhMMss(String inputStringDate) throws ParseException {
		if (inputStringDate != null) {
			DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYYHHmmss);
			return dateFormat.parse(inputStringDate);
		}
		return null;
	}

	/**
	 * Date Utils parser string format dd/MM/yyyy HH:mm
	 * 
	 * @param inputDate
	 * @return Date Utils
	 * @throws ParseException 
	 */
	public static Date StringPaserDateUtils_ddMMyyyhhMM(String inputStringDate) throws ParseException {
		if (inputStringDate != null) {
			DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYYHHmm);
			return dateFormat.parse(inputStringDate);
		}
		return null;
	}

	/**
	 * Date sql parser string format dd/MM/yyyy
	 * 
	 * @param inputDate
	 * @return Date Utils
	 */
	public static java.sql.Date StringPaserDateSQL_ddMMyyyy(String inputStringDate) throws ParseException {
		return new java.sql.Date(StringPaserDateUtils_ddMMyyyy(inputStringDate).getTime());

	}

	/**
	 * Date sql parser string format dd/MM/yyyy HH:mm:ss
	 * 
	 * @param inputDate
	 * @return Date Utils
	 */
	public static java.sql.Date StringPaserDateSQL_ddMMyyyhhMMss(String inputStringDate) throws ParseException {
		return new java.sql.Date(StringPaserDateUtils_ddMMyyyhhMMss(inputStringDate).getTime());

	}

	/**
	 * Date sql parser string format dd/MM/yyyy HH:mm
	 * 
	 * @param inputDate
	 * @return Date Utils
	 */
	public static java.sql.Date StringPaserDateSQL_ddMMyyyhhMM(String inputStringDate) throws ParseException {
		return new java.sql.Date(StringPaserDateUtils_ddMMyyyhhMM(inputStringDate).getTime());
	}

}
