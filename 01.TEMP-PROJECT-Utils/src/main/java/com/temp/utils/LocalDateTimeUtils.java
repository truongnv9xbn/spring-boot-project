package com.temp.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LocalDateTimeUtils {

	/**
	 * Java 8 sử dụng DateTimeFormatterđể định dạng ZonedDateTime, LocalDateTime,
	 * LocalDatevà LocalTimeđể chuỗi với các mẫu tùy chỉnh được xác định trước. (sử
	 * dụng ngày tháng năm)
	 */
	public static final String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
	public static final String TIMESTAMP = "yyyy-MM-dd HH:mm:ss";
	
	public static final String DEFAULT_HHMMSS = "HH:mm:ss";
	/**
	 * sử dụng ngày tháng năm giờ phút giây
	 */
	public static final String DEFAULT_DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
	public static final String EXPORT_DATE_FORMAT = "yyyyMMddHHmmss";

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public static boolean validateEmail(String emailStr) {
		emailStr = emailStr.trim();
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
		return matcher.find();
	}

	public static String formatLocalDate(LocalDate localDate) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT);
		return localDate == null ? "" : localDate.format(formatter);
	}

	public static String convertDateToString(Date inputDate) {
		String dateString = "";
		SimpleDateFormat sdfr = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
		if(inputDate != null) {
			dateString = sdfr.format(inputDate);
		}
		return dateString;
	}

	public static String formatLocalDateTime(LocalDateTime localDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT);
		return localDateTime == null ? "" : localDateTime.format(formatter);
	}

	public static LocalDate toLocalDate(String strDate) {
		if(strDate != null && strDate != "") {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT);
			return LocalDate.parse(strDate, formatter);
		} else {
			return null;
		}
	}

	public static String formatLocalDateTimeToTimestamp(LocalDateTime localDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TIMESTAMP);
		return localDateTime == null ? "" : localDateTime.format(formatter);
	}

	public static String formatLocalDateTimeToDefaultFormat(LocalDateTime localDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DEFAULT_DATE_TIME_FORMAT);
		return localDateTime == null ? "" : localDateTime.format(formatter);
	}

	public static String formatLocalDateTimeToExportFormat(LocalDateTime localDateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(EXPORT_DATE_FORMAT);
		return localDateTime == null ? "" : localDateTime.format(formatter);
	}

	public static String formatTimestampToString(Timestamp timestamp) {
		if (timestamp == null) {
			return "";
		}
		Date date = new Date();
		date.setTime(timestamp.getTime());
		String formattedDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(date);
		return formattedDate;

	}

	public static String convertTimestampToStringDateTime(Timestamp timestamp) {
		if (timestamp == null) {
			return "";
		}
		Date date = new Date();
		date.setTime(timestamp.getTime());
		String formattedDate = new SimpleDateFormat(DEFAULT_DATE_FORMAT).format(date);
		return formattedDate;

	}

	public static Timestamp convertStringToTimestamp(String strDate) {
		try {
			if(strDate != null && strDate != "") {
				DateFormat formatter = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
				// you can change format of date
				Date date = formatter.parse(strDate);
				Timestamp timeStampDate = new Timestamp(date.getTime());

				return timeStampDate;
			} else {
				return null;
			}
		} catch (ParseException e) {
			//System.out.println("Exception :" + e);
			return null;
		}
	}

	public static String convertTimestampToStringddMMyyyyHHmmss(Timestamp timestamp) {
		if (timestamp == null) {
			return "";
		}
		Date date = new Date();
		date.setTime(timestamp.getTime());
		String formattedDate = new SimpleDateFormat(DEFAULT_DATE_TIME_FORMAT).format(date);
		return formattedDate;

	}
	
	public static String convertTimestampToStringHHmmss(Timestamp timestamp) {
		if (timestamp == null) {
			return "";
		}
		Date date = new Date();
		date.setTime(timestamp.getTime());
		String formattedDate = new SimpleDateFormat(DEFAULT_HHMMSS).format(date);
		return formattedDate;

	}

	public static Timestamp getCurrentTimestamp() {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		return timestamp;
	}

}
