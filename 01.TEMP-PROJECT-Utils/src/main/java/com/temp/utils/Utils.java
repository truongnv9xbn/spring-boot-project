/**
 * 
 */
package com.temp.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.X509Certificate;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.crypt.dsig.SignatureConfig;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo;
import org.apache.poi.poifs.crypt.dsig.SignatureInfo.SignaturePart;
import org.springframework.http.MediaType;
import org.springframework.web.multipart.MultipartFile;

import com.aspose.words.DigitalSignature;
import com.aspose.words.DigitalSignatureCollection;
import com.aspose.words.DigitalSignatureUtil;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.security.PdfPKCS7;

public class Utils {
	public static final String BaseUrl = "http://localhost:3500";
	public static final String PasswordDefault = "123123";
	
	
	@SuppressWarnings("unused")
	private static String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();
	}

	
	public static Integer parseIntegerValue(Object obj) {
		Integer value = 0;
		if(obj != null) {
			if(obj instanceof Integer) {
				value = ((Integer) obj).intValue();
			}else if(obj instanceof BigDecimal){
				value = ((BigDecimal) obj).intValue();
			}else if(obj instanceof Long) {
				value = ((Long) obj).intValue();
			}
		}
		return value;
	}
	
	public static Long parseLongValue(Object obj) {
		Long value = new Long(0);
		if(obj != null) {
			if(obj instanceof Integer) {
				value = ((Integer) obj).longValue();
			}else if(obj instanceof BigDecimal){
				value = ((BigDecimal) obj).longValue();
			}else if(obj instanceof Long) {
				value = ((Long) obj);
			}
		}
		return value;
	}
	
	public static BigDecimal parseBigDecimalValue(Object obj) {
		BigDecimal value = new BigDecimal(0);
		if(obj != null) {
			if(obj instanceof Integer) {
				Integer intTyppe = ((Integer) obj).intValue();
				value = new BigDecimal(intTyppe);
			}else if(obj instanceof BigDecimal){
				value = ((BigDecimal) obj);
			}else if(obj instanceof Long) {
				long longType = ((Long) obj);
				value = new BigDecimal(longType);
			}
		}
		return value;
	}
	
	public static String parseStringValue(Object obj) {
		String value = null;
		if(obj != null) {
			value =  (String) obj;
		}
		return value;
	}
	
	public static Boolean parseBooleanValue(Object obj) {
		Boolean value = false;
		if(obj instanceof Integer) {
			if(((Integer) obj).intValue() == 1) {
				value = true;
			}else {
				value = false;
			}
		}else if(obj instanceof BigDecimal){
			if(((BigDecimal) obj).intValue() == 1) {
				value = true;
			}else {
				value = false;
			}
		}else if(obj instanceof Long){
			if(((Long) obj).intValue() == 1) {
				value = true;
			}else {
				value = false;
			}
		}else if(obj instanceof Boolean){
			value =  obj != null ? (Boolean) obj : false;
		}
		return value;
	}

	public static MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
		// application/pdf
		// application/xml
		// image/gif, ...
		String mineType = servletContext.getMimeType(fileName);
		try {
			MediaType mediaType = MediaType.parseMediaType(mineType);
			return mediaType;
		} catch (Exception e) {
			return MediaType.APPLICATION_OCTET_STREAM;
		}
	}

	/**
	 * Method compare 2 object if field not equal then value set into object soucre
	 * (o1)
	 * 
	 * @param o1
	 * @param o2
	 */
	public static void compareAndApplyDifferences(Object o1, Object o2) {
		if (!o1.getClass().equals(o2.getClass())) {
			return;
		}
		// check if the result of getters in o1 and o2 are different
		boolean isDiff = false;
		Class<? extends Object> c1 = o1.getClass();
		Class<? extends Object> c2 = o2.getClass();
		Method[] methods1 = c1.getMethods();
		Method[] methods2 = c2.getMethods();
		Map<String, Object> hm = new HashMap<String, Object>();
		for (Method method2 : methods2) {
			if (isGetter(method2)) {
				try {
					Object getterResult = method2.invoke(o2);
					hm.put(method2.getName().substring(3), getterResult);
				} catch (IllegalAccessException e) {
				} catch (IllegalArgumentException e) {
				} catch (InvocationTargetException e) {
				}
			}
		}
		for (Method method1 : methods1) {
			if (isGetter(method1)) {
				try {
					Object getterResult = (Object) method1.invoke(o1);
					if (!hm.containsValue(getterResult)) {
						isDiff = true;
						break;
					}
				} catch (IllegalAccessException e) {
				} catch (IllegalArgumentException e) {
				} catch (InvocationTargetException e) {
				}
			}
		}

		if (isDiff) {
			// set the values in o1 as the values in o2
			for (Method method1 : methods1) {
				if (isSetter(method1)) {
					try {
						method1.invoke(o1, hm.get(method1.getName().substring(3)));
					} catch (IllegalAccessException e) {
					} catch (IllegalArgumentException e) {
					} catch (InvocationTargetException e) {
					}
				}
			}
		} else {
			return;
		}
	}

	private static boolean isGetter(Method method) {
		if (!method.getName().startsWith("get"))
			return false;
		if (method.getParameterTypes().length != 0)
			return false;
		if (void.class.equals(method.getReturnType()))
			return false;
		return true;
	}

	private static boolean isSetter(Method method) {
		if (!method.getName().startsWith("set"))
			return false;
		if (method.getParameterTypes().length != 1)
			return false;
		return true;
	}

	public static enum EXTENTION_FILE {
		doc, docx, xls, xlsx, jpg, png, pdf, jpeg;

		public static boolean contains(String s) {
			try {
				EXTENTION_FILE.valueOf(s);
				return true;
			} catch (Exception e) {
				return false;
			}
		}
	}

	public static enum MQH {
		CON(1, "Con"), CHIGAI(2, "Chị gái"), ANHTRAI(3, "Anh trai"), BO(4, "Bố"), CHAU(5, "Cháu"), ME(6, "Mẹ"),
		VO(7, "Vợ"), CHONG(8, "Chồng"), EMTRAI(9, "Em trai"), EMGAI(10, "Em gái");
		private final Integer value;
		private final String text;

		/**
		 * A mapping between the integer code and its corresponding text to facilitate
		 * lookup by code.
		 */
		private static List<MQH> valueToTextMapping;

		private MQH(Integer value, String text) {
			this.value = value;
			this.text = text;
		}

		public static MQH getMQH(MQH i) {
			if (valueToTextMapping == null) {
				initMapping();
			}
			return (MQH) valueToTextMapping.stream().filter(e -> valueToTextMapping.contains(i)).findAny().get();

		}

		public static List<MQH> initMapping() {
			valueToTextMapping = new ArrayList<>();
			for (MQH s : values()) {
				valueToTextMapping.add(s);
			}
			return valueToTextMapping;
		}

		public Integer getValue() {
			return value;
		}

		public String getText() {
			return text;
		}

		@Override
		public String toString() {
			final StringBuilder sb = new StringBuilder();
			sb.append("MQH");
			sb.append("{value=").append(value);
			sb.append(", text='").append(text).append('\'');
			sb.append('}');
			return sb.toString();
		}
	}

	public static enum TrangThaiPheDuyet {
		ChoDuyet(0), PheDuyet(1), TuChoi(2);
		private final int value;

		TrangThaiPheDuyet(final int newValue) {
			value = newValue;
		}

		public int getValue() {
			return value;
		}
	}

	/**
	 * move file to folder tmp to folder base on targetPath
	 * 
	 * @param sourceFile
	 * @param targetPath
	 * @return
	 * @throws IOException
	 */
	public static boolean MoveFileToDirectory(File sourceFile, String targetPath) throws IOException {
		File tDir = new File(targetPath);
		if (!tDir.exists()) {
			boolean result = tDir.mkdirs();

			if (result) {
//				//System.out.println("Folder is created");
//
//				String newFilePath = targetPath + File.separator + sourceFile.getName();
//				File movedFile = new File(newFilePath);
//				if (movedFile.exists())
//					movedFile.delete();
//				return sourceFile.renameTo(new File(newFilePath));
			}
		}
		String newFilePath = targetPath + File.separator + sourceFile.getName();
		File movedFile = new File(newFilePath);
		if (movedFile.exists()) {
			movedFile.delete();
		}

		return sourceFile.renameTo(new File(newFilePath));

	}

	public static String CommonSavePathFile(String fileDinhKem, String folderStore, HttpServletRequest request)
			throws Exception {
		if (fileDinhKem != null && fileDinhKem != "") {

			String[] splipPath = fileDinhKem.split("`");
			String pathNew = "";
			if (splipPath.length > 0) {
				for (int i = 0; i < splipPath.length; i++) {
					String path = splipPath[i];
					String[] pathRelative = path.split("~");
					if (pathRelative.length > 0 && pathRelative.length < 3) {
						File file = new File(pathRelative[0]);
						Path tmpPath = Paths.get(Constant.UPLOADNOTTEMPFOLDER + "\\" + folderStore);
						String tmp = tmpPath.toFile().toString();
						try {
							Utils.MoveFileToDirectory(file, tmp);
						} catch (IOException e) {

							throw new Exception(e);
						}
						pathNew += folderStore + pathRelative[1] + ",";
					}

				}

			}
			if (pathNew != null && pathNew.length()>0) {
				return pathNew.substring(0, pathNew.length() - 1);

			}

		}
		return "";
	}

	/**
	 * Delete file base on path
	 * 
	 * @param sourceFile
	 * @param targetPath
	 * @return
	 * @throws IOException
	 */
	public static boolean DeleteFile(String pathRemove) throws IOException {
//		if (pathRemove.length() > 0) {
//			pathRemove = pathRemove.substring(1, pathRemove.length());
//		}
		// File tDir = new File(pathRemove);

		Path path = Paths.get(pathRemove);
		return Files.deleteIfExists(path);

	}

	public static StringBuffer AppenLoaiCoDong(String str) {
		StringBuffer sbf = new StringBuffer();
		String[] splitLCD = null;
		if (str != null) {
			splitLCD = str.split(",");
			if (splitLCD == null) {
				sbf.append(SwichCaseLoaiCoDong(str));
			}
		}
		if (splitLCD != null && splitLCD.length > 0) {

			for (String lcd : splitLCD) {
				sbf.append(SwichCaseLoaiCoDong(lcd) + ", ");
			}
			sbf.substring(sbf.length() - 2, sbf.length());
		}
		return sbf;

	}

	public static String SwichCaseLoaiCoDong(String key) {
		switch (key) {
		case "CDSL":
			return "Cổ đông sáng lập";
		case "TVSL":
			return "Thành viên sáng lập";
		case "CDL":
			return "Cổ đông lớn";
		case "K":
			return "Khác";

		}
		return null;

	}

	public static String getClientIp(HttpServletRequest request) {

		String remoteAddr = "";

		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		return remoteAddr;
	}

	public static Timestamp getCurrentDate() {
		Date curr = new Date();
		try {
			return TimestampUtils.DateUtilsParseTimeStamp(curr);
		} catch (ParseException e) {

			e.printStackTrace();
		}
		return null;
	}
	
	public static Integer tryParseInt(String value) {
	    try {
	        return Integer.parseInt(value);
	    } catch (NumberFormatException e) {
	        return 0;
	    }
	}
	//Kiểm tra ký số file PDF
	public static String verifyPdfSignatures(MultipartFile file, List<Object> cts) throws IOException {
		//File có ký số hay không
 	     boolean isSigned = false;
	    //ký đúng trong thời gian ký số hiệu lực hay không
		 boolean validDate = false;
		//Đúng người ký hay không
		 boolean validSigner = false;
		 if(Constant.CHECKTENCKS.equals("1") && (cts == null || cts.size() == 0))
			 return "Tài khoản hiện chưa đăng ký chữ ký số nào";
		 try {
//			 Security.addProvider(new BouncyCastleProvider());
		        PdfReader reader = new PdfReader(file.getInputStream());
		        AcroFields af = reader.getAcroFields();
		        ArrayList<String> names = af.getSignatureNames();
		        if (names.size() > 0) {
		        	isSigned = true;
		        	for (String name : names) {
		                PdfPKCS7 pk = af.verifySignature(name);
		                X509Certificate cert = pk.getSigningCertificate();
		                String strSubjectDN = cert.getSubjectDN().getName();
		                Calendar cal = pk.getSignDate();

		                validDate =
		                    isWithinDateRange(cal.getTime(), cert.getNotBefore(),
		                                      cert.getNotAfter());
		                if(Constant.CHECKTENCKS.equals("0"))
		                	validSigner = true;
		                else {
		                if(cts != null && cts.size() > 0) {
				            Gson gson = new Gson();
			                for(Object temp : cts) {
			                	JsonObject jsonCts = (JsonObject) gson.toJsonTree(temp);
				                if (jsonCts != null && jsonCts.get("subject") != null && !jsonCts.get("subject").getAsString().isEmpty()) {
				                	validSigner = getValByAttributeTypeFromIssuerDN(strSubjectDN, jsonCts.get("subject").getAsString());
				                }
				                if(validSigner == true)
				                	break;
			                }
		                	
		                }
		                }
		                if(validSigner == true)
		                	break;
		                
		            }
		        }
		        if(isSigned == false)
					return "File chưa có chữ ký số";
				else if(isSigned == true && (validDate == false || validSigner == false))
					return "Chữ ký số trong file chưa hợp lệ";
				else if(isSigned == true && validDate == true && validSigner == true)
					return "";
		 }
		 catch(Exception e) {
			 e.printStackTrace();
			 return "File không hợp lệ, vui lòng kiểm tra lại!";
		 }
		 return "File không hợp lệ, vui lòng kiểm tra lại!";
		
	}
	
//    //Kiểm tra ký số File thuộc microsoft
//
	@SuppressWarnings("null")
	public static String verifyMicrosoftOfficeSignatures(MultipartFile file, List<Object> cts) throws Exception {
		//File có ký số hay không
	     boolean isSigned = false;
	    //ký đúng trong thời gian ký số hiệu lực hay không
		 boolean validDate = false;
		//Đúng người ký hay không
		 boolean validSigner = false;
		 if(Constant.CHECKTENCKS.equals("1") && (cts == null || cts.size() == 0))
			 return "Tài khoản hiện chưa đăng ký chữ ký số nào";
//		 Security.addProvider(new BouncyCastleProvider());
	     try {
	    	 OPCPackage pkg = null;
	    	 try {
	    		 pkg = OPCPackage.open(file.getInputStream());
	    	 }
	    	 catch(Exception e) {
	    	 }
	    	 if(pkg != null) {
	    		 SignatureConfig sic = new SignatureConfig();
				 sic.setOpcPackage(pkg);
				 SignatureInfo si = new SignatureInfo();
				 si.setSignatureConfig(sic);
//				 isSigned = si.verifySignature();
				 isSigned = true;
				 if(isSigned == true) {
					 List<X509Certificate> result = new ArrayList<X509Certificate>();
					 for (SignaturePart sp : si.getSignatureParts()) {
					     if (sp.validate()) {
					         result.add(sp.getSigner());
					         
					     }
					 }
					 for(X509Certificate cert : result) {
						 String strSubjectDN = cert.getSubjectDN().getName();
				            Date signDate = si.getSignatureConfig().getExecutionTime();

				            validDate =
				                isWithinDateRange(signDate, cert.getNotBefore(),
				                                  cert.getNotAfter());
				            if(Constant.CHECKTENCKS.equals("0"))
			                	validSigner = true;
			                else {
				                if(cts != null && cts.size() > 0) {
						            Gson gson = new Gson();
					                for(Object temp : cts) {
					                	JsonObject jsonCts = (JsonObject) gson.toJsonTree(temp);
						                if (jsonCts != null && jsonCts.get("subject") != null && !jsonCts.get("subject").getAsString().isEmpty()) {
						                	validSigner = getValByAttributeTypeFromIssuerDN(strSubjectDN, jsonCts.get("subject").getAsString());
						                }
						                if(validSigner == true)
						                	break;
					                }
				                	
				                }
			                }
				            if(validSigner == true)
			                	break;
					 }
				 }
				 if(isSigned == false)
						return "File chưa có chữ ký số";
					else if(isSigned == true && (validDate == false || validSigner == false))
						return "Chữ ký số trong file chưa hợp lệ";
					else if(isSigned == true && validDate == true && validSigner == true)
						return "";
	    	 }
	    	 else {
	    		 DigitalSignatureCollection dsc =
	    		            DigitalSignatureUtil.loadSignatures(file.getInputStream());
	    		 if (dsc != null || dsc.getCount() > 0) {
	    			 isSigned = true;
	    	        }
	    		 
	    		 if(isSigned == true) {
	    			 for (DigitalSignature ds : dsc) {
	    				 X509Certificate cert = ds.getCertificate();
	    		            String strSubjectDN = cert.getSubjectDN().getName();
	    		            Date signed_date = ds.getSignTime();

	    		            validDate =
	    		                isWithinDateRange(signed_date, cert.getNotBefore(),
	    		                                  cert.getNotAfter());
	    		            if(Constant.CHECKTENCKS.equals("0"))
			                	validSigner = true;
			                else {
				                if(cts != null && cts.size() > 0) {
						            Gson gson = new Gson();
					                for(Object temp : cts) {
					                	JsonObject jsonCts = (JsonObject) gson.toJsonTree(temp);
						                if (jsonCts != null && jsonCts.get("subject") != null && !jsonCts.get("subject").getAsString().isEmpty()) {
						                	validSigner = getValByAttributeTypeFromIssuerDN(strSubjectDN, jsonCts.get("subject").getAsString());
						                }
						                if(validSigner == true)
						                	break;
					                }
				                	
				                }
			                }
	    		            if(validSigner == true)
			                	break;
	    		 }
	    			 if(isSigned == false)
	 					return "File chưa có chữ ký số";
	 				else if(isSigned == true && (validDate == false || validSigner == false))
	 					return "Chữ ký số trong file chưa hợp lệ";
	 				else if(isSigned == true && validDate == true && validSigner == true)
	 					return "";
	 		 }
	    	 }
	     }
	 		 catch(Exception e) {
	 			 e.printStackTrace();
	 			 return "File không hợp lệ, vui lòng kiểm tra lại!";
	 		 }
	     return "File không hợp lệ, vui lòng kiểm tra lại!";
    }
	
	
	//Kiểm tra thời hạn chứ ký hiệu lực
	private static boolean isWithinDateRange(Date signedDate, Date startDate,
            Date endDate) {
		if(signedDate == null || startDate == null)
			return false;
		else
			return signedDate.getTime() >= startDate.getTime() && signedDate.getTime() <= endDate.getTime();
	}
	
	//Get Value Attribute From Issuer

    private static Boolean getValByAttributeTypeFromIssuerDN(String subject,
                                                            String subjectCts) {
        String[] subjectList = subject.split(",");
        String[] subjectCtsList = subjectCts.split(",");
        Integer sizeA = subjectList.length;
        Integer sizeB = subjectCtsList.length;
        for (Object aSplit : subjectList) {
            for (Object bSplit : subjectCtsList) {
            	if(aSplit.toString().trim().equals(bSplit.toString().trim())) {
            		sizeA--;
            		sizeB--;
            		break;
            	}
            }
        }
        if(sizeA == 0 || sizeB == 0)
        	return true;
        return false;
    }
    
    public static String formatDecimal(String strNumber) {
    	double number = 0;
    	try{
    		String formatNumber = "";
    		number = Double.parseDouble(strNumber);
        	float epsilon = 0.004f; // 4 tenths of a cent
        	if (Math.abs(Math.round(number) - number) < epsilon) {
            	DecimalFormat df = new DecimalFormat("#,##0");
            	formatNumber = df.format(number);
//        		return String.format("%10.0f", number); // sdb
        	} else {
            	DecimalFormat df = new DecimalFormat("###,###.##");
            	formatNumber = df.format(number);
//        		return String.format("%10.2f", number); // dj_segfault
        	}
        	return formatNumber;
        }catch(NumberFormatException e){
            return strNumber;
        }
	}
    
    public static String verifyPdfSignaturesForFile(File file, List<Object> cts) throws IOException {
		//File có ký số hay không
 	     boolean isSigned = false;
	    //ký đúng trong thời gian ký số hiệu lực hay không
		 boolean validDate = false;
		//Đúng người ký hay không
		 boolean validSigner = false;
		 if(Constant.CHECKTENCKS.equals("1") && (cts == null || cts.size() == 0))
			 return "Tài khoản hiện chưa đăng ký chữ ký số nào";
		 try {
//			 Security.addProvider(new BouncyCastleProvider());
			 InputStream fileInputStream = new FileInputStream(file);
		        PdfReader reader = new PdfReader(fileInputStream);
		        AcroFields af = reader.getAcroFields();
		        ArrayList<String> names = af.getSignatureNames();
		        if (names.size() > 0) {
		        	isSigned = true;
		        	for (String name : names) {
		                PdfPKCS7 pk = af.verifySignature(name);
		                X509Certificate cert = pk.getSigningCertificate();
		                String strSubjectDN = cert.getSubjectDN().getName();
		                Calendar cal = pk.getSignDate();

		                validDate =
		                    isWithinDateRange(cal.getTime(), cert.getNotBefore(),
		                                      cert.getNotAfter());
		                if(Constant.CHECKTENCKS.equals("0"))
		                	validSigner = true;
		                else {
		                if(cts != null && cts.size() > 0) {
				            Gson gson = new Gson();
			                for(Object temp : cts) {
			                	JsonObject jsonCts = (JsonObject) gson.toJsonTree(temp);
				                if (jsonCts != null && jsonCts.get("subject") != null && !jsonCts.get("subject").getAsString().isEmpty()) {
				                	validSigner = getValByAttributeTypeFromIssuerDN(strSubjectDN, jsonCts.get("subject").getAsString());
				                }
				                if(validSigner == true)
				                	break;
			                }
		                	
		                }
		                }
		                if(validSigner == true)
		                	break;
		                
		            }
		        }
		        if(isSigned == false)
					return "File chưa có chữ ký số";
				else if(isSigned == true && (validDate == false || validSigner == false))
					return "Chữ ký số trong file chưa hợp lệ";
				else if(isSigned == true && validDate == true && validSigner == true)
					return "";
		 }
		 catch(Exception e) {
			 e.printStackTrace();
			 return "";
		 }
		return "";
	}
    public static void createFileXML(String namePC, String stringXml, String nameFile) {
		try {
			File theDir = new File("C:\\Users\\" + namePC + "\\Desktop\\fileXMLs");

			if (!theDir.exists()) {
				// nếu folder không tồn tại thì khởi tạo
				theDir.mkdirs();
			}

			FileWriter myWriter = new FileWriter("C:\\Users\\" + namePC + "\\Desktop\\fileXMLs\\" + nameFile);
			myWriter.write(stringXml);
			myWriter.close();
			//System.out.println("Đã ghi ra file : " + nameFile);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SecurityException se) {
			se.printStackTrace();
		}
	}
//    public static Connection connect() throws ClassNotFoundException {
//		Connection conn = null;
//		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//		try {
//			String dbURL = "jdbc:sqlserver://192.168.50.97:1433;databaseName=NHNCK;";
//			String user = "sa";
//			String pass = "123456a@";
//			conn = DriverManager.getConnection(dbURL, user, pass);
//		} catch (SQLException ex) {
//			ex.printStackTrace();
//		} 
//		return conn;
//	}
}
