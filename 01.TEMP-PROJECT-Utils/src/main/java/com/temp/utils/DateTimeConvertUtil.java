/**
 * 
 */
package com.temp.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author thangnn
 *
 */
public class DateTimeConvertUtil {
	static String FormateerddMMYYYY = "dd/MM/yyyy";

	static String FormateerddMMYYYYHHmmss = "dd/MM/yyyy HH:mm:ss";

	static String FormateerddMMYYYYHHmm = "dd/MM/yyyy HH:mm";

	/**
	 * ConvertDate to string format dd/MM/yyyy
	 * 
	 * @param inputDate
	 * @return
	 */
	public static String ConvertDateToString_ddMMyyyy(Date inputDate) {
		String strddMMyyyy="";
		try {
			
			DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYY);
		 strddMMyyyy = dateFormat.format(inputDate);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return strddMMyyyy;

	}
	public static String ConvertDateToString_ddMMyyyyHHmm(Date inputDate) {
		String strddMMyyyy="";
		try {
			
			DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYYHHmm);
		 strddMMyyyy = dateFormat.format(inputDate);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return strddMMyyyy;

	}
	public static String ConvertDateToString_ddMMyyyhhMMss(Date inputDate) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYYHHmmss);
		String StrddMMYYYYHHmmss = dateFormat.format(inputDate);
		return StrddMMYYYYHHmmss;
	}

	public static String ConvertDateToString_ddMMyyyhhMM(Date inputDate) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(FormateerddMMYYYYHHmm);
		String StrddMMYYYYHHmm = dateFormat.format(inputDate);
		return StrddMMYYYYHHmm;
	}
}
