package com.temp.utils;

public class Constant {
	static {
		BaseUrl = FilePropertiesGlobal.readPropertiesFile().getProperty(Constants.KEY_BASE_VUE);

	}
	public static final String CHECKFOMULA = "CHECKFOMULA";
	public static final String CHECKIP = "CHECK_IP";
	public static final String CODONGLON = "PERCENT";
	public static final String SIZE_FILE_CHECK ="FILESIZE";
	public static final String MAIL_SERVER ="MAIL_SERVER";
	public static final String MAIL_CONTENT ="MAIL_CONTENT";
	public static final String MAIL_SUBJECT ="MAIL_SUBJECT";
	//---------------------Hủy báo cáo
	public static final String MAIL_SUBJECT_CANCEL_REPORT ="MAIL_SUBJECT_CANCEL_REPORT";
	public static final String MAIL_CONTENT_CANCEL_REPORT ="MAIL_CONTENT_CANCEL_REPORT";
	
	//---------------------Cấp lại mật khẩu
	public static final String MAIL_SUBJECT_FORGOTPASSWORD ="MAIL_SUBJECT_FORGOTPASSWORD";
	
	//---------- thay đổi hồ sơ công ty
	public static final String MAIL_SUBJECT_PROFILE ="MAIL_SUBJECT_PROFILE";
	public static final String MAIL_CONTENT_PROFILE ="MAIL_CONTENT_PROFILE";
	
	//-----Hủy phê duyệt thay đổi hồ sơ công ty 
	public static final String MAIL_SUBJECT_CANCEL_PROFILE ="MAIL_SUBJECT_CANCEL_PROFILE";
	public static final String MAIL_CONTENT_CANCEL_PROFILE ="MAIL_CONTENT_CANCEL_PROFILE";
	
	//-----Thông báo xác nhận gửi CBTT 
	public static final String MAIL_SUBJECT_CBTT ="MAIL_SUBJECT_CBTT";
	public static final String MAIL_CONTENT_CBTT ="MAIL_CONTENT_CBTT";
	public static final String MAIL_BCC ="MAIL_BCC";
	public static final String MAIL_BCC_CONFIG = "MAIL_BCC_CONFIG";
	public static final String MAIL_PORT ="MAIL_PORT";
	public static final String MAIL_USER ="MAIL_USER";
	public static final String MAIL_PASSWORD ="MAIL_PASSWORD";
	public static  String BaseUrl = "";
	public static final String MESSAGE_NOT_FOUND = "Không tồn tại! ";
	public static final String MESSAGE_CREATE = "Thêm mới";
	public static final String MESSAGE_CREATE_SUCCESS = "Thêm mới thành công";
	public static final String MESSAGE_CREATE_ERROR = "Tạo mới không thành công";
	public static final String MESSAGE_UPDATE_FALSE = "Cập nhật không thành công ";
	public static final String MESSAGE_FIND_ID = "Tìm id ";
	public static final String MESSAGE_FIND_NOT_ID = "Không tìm thấy id";
	public static final String MESSAGE_AN_ERROR_OCCURRED = "Đã có lỗi xảy ra";
	// message cập nhật
	public static final String MESSAGE_UPDATE = "Cập nhật";
	public static final String MESSAGE_UPDATE_SUCCESS = "Cập nhật thành công";
	// message xóa
	public static final String MESSAGE_FIND_AND_DELETE_ID = "Tìm và xóa theo id";
	public static final String MESSAGE_NOT_DELETE = "Xóa thất bại ";
	// message check
	public static final String MESSAGE_NOT_EXIST = "Bản ghi không tồn tại!";
	public static final String MESSAGE_EXIST = "Bản ghi đã tồn tại!";
	public static final String MESSAGE_EXIST_MA = "Mã đã tồn tại!";
	public static final String MESSAGE_NOT_CREATE_ALREADY_EXIST_NAME = "không thể tạo mới với tên đã tồn tại:";
	public static final String MESSAGE_NOT_NULL_NAME = "Tên không được để trống!";
	public static final String MESSAGE_NAME_EXCEED_NAME = "Tên không được vượt quá 250 ký tự!";
	public static final String MESSAGE_NAME_EXCEED_MA = "Mã không được vượt quá 50 ký tự!";
	public static final String MESSAGE_NAME_EXCEED_GHI_CHU = "Ghi chú không được vượt quá 1000 ký tự!";

	public static final String LOG_LIST = "Danh sách";
	public static final String LOG_CREATE = "Tạo mới";
	public static final String LOG_UPDATE = "Cập nhật";
	public static final String LOG_DELETE = "Xóa";

	public static final String LOG_OUT_SUCCESS = "Logout Success";

	public static final String EMPTY = "";

	public static final String UPLOADTMPFOLDER = "/uploadFileTmp/";
	
	public static final String HOTLINE = "HOTLINE";
	public static final String BCKT = "BCKT";
	
	public static  String API_HSDVC = "";
	public static  String API_CTKT = "";
	public static  String CHECKTENCKS = "";
	public static  Integer IDUSER = null;
	public static  boolean ISKYSOUSER = false;
	public static  String UPLOADNOTTEMPFOLDER = "C:\\PROJECT_FILE\\";

	public static  String TIME_CANH_BAO = "";

	public static String IDS_IDSTTTCKTCTTGKTV = "http://192.168.50.97:9867/IDSService.asmx/IDSTTTCKTCTTGKTV?";
	public static String IDS_IDSTHONGTINCOSO = "http://192.168.50.97:9867/IDSService.asmx/IDSTHONGTINCOSO?";
	
	public static String EXCEPTION_FAIL = "Thao tác thất bại, đã xảy ra ngoại lệ ";
	
	public static String IDS_GETKIEMTOANVIEN = "http://192.168.50.97:6789/IDSService.asmx/GETKIEMTOANVIEN";
	
	public static String IDS_CONGTY = "IDS_CONGTY";//IDS.TTCONGTY.INBOX.QUEUE
	public static String IDS_BAOCAO = "IDS_BAOCAO";//SCMS.BC334.INBOX.QUEUE
	public static String QUEUE_MGR = "QUEUE_MGR";//ESB.KNT.QMGR
	
	public static String QUEUE_HOSTNAME = "QUEUE_HOSTNAME";
	public static String QUEUE_CHANNEL = "QUEUE_CHANNEL";
	public static String QUEUE_PORT = "QUEUE_PORT";
	public static String QUEUE_USERID = "QUEUE_USERID";
	public static String QUEUE_PASSWORD = "QUEUE_PASSWORD";
	public static final String ChiTieuXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><root><ct100>A. TÀI SẢN NGẮN HẠN (100 = 110 + 130)</ct100><ct110>I. Tài sản tài chính</ct110><ct111>1.Tiền và các khoản tương đương tiền</ct111><ct111_1>1.1. Tiền</ct111_1><ct111_2>1.2. Các khoản tương đương tiền</ct111_2><ct112>2. Các tài sản tài chính ghi nhận thông qua lãi/lỗ (FVTPL)</ct112><ct113>3. Các  khoản đầu tư  nắm giữ đến ngày đáo hạn (HTM)</ct113><ct114>4. Các khoản cho vay</ct114><ct115>5. Tài sản tài chính sẵn sàng để bán (AFS)</ct115><ct116>6. Dự phòng suy giảm giá trị các tài sản tài chính và tài sản thế chấp</ct116><ct117>7. Các khoản phải thu</ct117><ct117_1>7.1. Phải thu bán các tài sản tài chính</ct117_1><ct117_2>7.2. Phải thu và dự thu cổ tức, tiền lãi các tài sản tài chính</ct117_2><ct117_3>7.2.1. Phải thu cổ tức, tiền lãi đến ngày nhận</ct117_3><ct117_4>7.2.2. Dự thu cổ tức, tiền lãi chưa đến ngày nhận</ct117_4><ct118>8. Trả trước cho người bán</ct118><ct119>9. Phải thu các dịch vụ CTCK cung cấp</ct119><ct120>10. Phải thu nội bộ</ct120><ct121>11. Phải thu về lỗi giao dịch chứng khoán</ct121><ct122>12. Các khoản phải thu khác</ct122><ct129>13. Dự phòng suy giảm giá trị các khoản phải thu (*)</ct129><ct130>II. Tài sản ngắn hạn khác</ct130><ct131>1. Tạm ứng</ct131><ct132>2. Vật tư văn phòng, công cụ, dụng cụ</ct132><ct133>3. Chi phí trả trước ngắn hạn</ct133><ct134>4. Cầm cố, thế chấp, ký quỹ, ký cược ngắn hạn</ct134><ct135>5. Thuế giá trị gia tăng được khấu trừ</ct135><ct136>6. Thuế và các khoản khác phải thu Nhà nước</ct136><ct137>7. Tài sản ngắn hạn khác</ct137><ct138>8. Giao dịch mua bán lại trái phiếu Chính phủ</ct138><ct139>9. Dự phòng suy giảm giá trị tài sản ngắn hạn khác</ct139><ct200>B. TÀI SẢN DÀI HẠN (200 = 210 + 220 + 230 + 240 + 250 - 260)</ct200><ct210>I. Tài sản tài chính dài hạn</ct210><ct211>1. Các khoản phải thu dài hạn</ct211><ct212>2. Các khoản đầu tư</ct212><ct212_1>2.1.Các khoản đầu tư nắm giữ đến ngày đáo hạn</ct212_1><ct212_2>2.2. Đầu tư vào công ty con</ct212_2><ct212_3>2.3. Đầu tư vào công ty liên doanh, liên kết</ct212_3><ct212_4>2.4. Đầu tư dài hạn khác</ct212_4><ct213>3. Dự phòng suy giảm tài sản tài chính dài hạn</ct213><ct220>II. Tài sản cố định</ct220><ct221>1. Tài sản cố định hữu hình</ct221><ct222>- Nguyên giá</ct222><ct223a>- Giá trị hao mòn luỹ kế (*)</ct223a><ct223b>- Đánh giá TSCĐHH theo giá trị hợp lý</ct223b><ct224>2. Tài sản cố định thuê tài chính</ct224><ct225>- Nguyên giá</ct225><ct226a>- Giá trị hao mòn luỹ kế (*)</ct226a><ct226b>- Đánh giá TSCĐTTC theo giá trị hợp lý</ct226b><ct227>3. Tài sản cố định vô hình</ct227><ct228>- Nguyên giá</ct228><ct229a>- Giá trị hao mòn luỹ kế (*)</ct229a><ct229b>- Đánh giá TSCĐVH theo giá trị hợp lý</ct229b><ct230>III. Bất động sản đầu tư</ct230><ct231>- Nguyên giá</ct231><ct232a>- Giá trị hao mòn luỹ kế (*)</ct232a><ct232b>- Đánh giá BĐSĐT theo giá trị hợp lý</ct232b><ct240>IV.Chi phí xây dựng cơ bản dở dang</ct240><ct250>V. Tài sản dài hạn khác</ct250><ct251>1. Cầm cố, thế chấp, ký quỹ, ký cược dài hạn</ct251><ct252>2. Chi phí trả trước dài hạn</ct252><ct253>3. Tài sản thuế thu nhập hoãn lại</ct253><ct254>4. Tiền nộp Quỹ Hỗ trợ thanh toán</ct254><ct255>5. Tài sản dài hạn khác</ct255><ct260>VI. Dự phòng suy giảm giá trị tài sản dài hạn</ct260><ct270>TỔNG CỘNG TÀI SẢN (270 = 100 + 200)</ct270><ct300>C. NỢ PHẢI TRẢ (300 = 310 + 340)</ct300><ct310>I. Nợ phải trả ngắn hạn</ct310><ct311>1. Vay và nợ thuê tài chính ngắn hạn</ct311><ct312>1.1. Vay ngắn hạn</ct312><ct313>1.2. Nợ thuê tài chính ngắn hạn</ct313><ct314>2. Vay tài sản tài chính ngắn hạn</ct314><ct315>3. Trái phiếu chuyển đổi ngắn hạn - Cấu phần nợ</ct315><ct316>4. Trái phiếu phát hành ngắn hạn</ct316><ct317>5. Vay Quỹ Hỗ trợ thanh toán</ct317><ct318>6. Phải trả hoạt động giao dịch chứng khoán</ct318><ct319>7. Phải trả về lỗi giao dịch các tài sản tài chính</ct319><ct320>8.  Phải trả người bán ngắn hạn</ct320><ct321>9. Người mua trả tiền trước ngắn hạn</ct321><ct322>10. Thuế và các khoản phải nộp Nhà nước</ct322><ct323>11. Phải trả người lao động</ct323><ct324>12.Các khoản trích nộp phúc lợi nhân viên</ct324><ct325>13. Chi phí phải trả ngắn hạn</ct325><ct326>14. Phải trả nội bộ ngắn hạn</ct326><ct327>15. Doanh thu chưa thực hiện ngắn hạn</ct327><ct328>16. Nhận ký quỹ, ký cược ngắn hạn</ct328><ct329>17.Các khoản phải trả, phải nộp khác ngắn hạn</ct329><ct330>18. Dự phòng phải trả ngắn hạn</ct330><ct331>19. Quỹ khen thưởng, phúc lợi</ct331><ct332>20. Giao dịch mua bán lại trái phiếu Chính phủ</ct332><ct340>II. Nợ phải trả dài hạn</ct340><ct341>1. Vay và nợ thuê tài chính dài hạn</ct341><ct342>1.1.Vay dài hạn</ct342><ct343>1.2. Nợ thuê tài chính dài hạn</ct343><ct344>2. Vay tài sản tài chính dài hạn</ct344><ct345>3.Trái phiếu chuyển đổi dài hạn - Cấu phần nợ</ct345><ct346>4.Trái phiếu phát hành dài hạn</ct346><ct347>5.  Phải trả người bán dài hạn</ct347><ct348>6. Người mua trả tiền trước dài hạn</ct348><ct349>7. Chi phí phải trả dài hạn</ct349><ct350>8. Phải trả nội bộ dài hạn</ct350><ct351>9. Doanh thu chưa thực hiện dài hạn</ct351><ct352>10. Nhận ký quỹ, ký cược dài hạn</ct352><ct353>11. Các khoản phải trả, phải nộp khác dài hạn</ct353><ct354>12. Dự phòng phải trả dài hạn</ct354><ct355>13. Quỹ bảo vệ Nhà đầu tư</ct355><ct356>14. Thuế thu nhập hoãn lại phải trả</ct356><ct357>15. Quỹ phát triển khoa học và công nghệ</ct357><ct400>D. VỐN CHỦ SỞ HỮU (400 = 410 + 420)</ct400><ct410>I. Vốn chủ sở hữu</ct410><ct411>1. Vốn đầu tư của chủ sở hữu</ct411><ct411_1>1.1. Vốn góp của chủ sở hữu</ct411_1><ct411_1a>a. Cổ phiếu phổ thông có quyền biểu quyết</ct411_1a><ct411_1b>b. Cổ phiếu ưu đãi</ct411_1b><ct411_2>1.2. Thặng dư vốn cổ phần</ct411_2><ct411_3>1.3. Quyền chọn chuyển đổi trái phiếu - Cấu phần vốn</ct411_3><ct411_4>1.4. Vốn khác của chủ sở hữu</ct411_4><ct411_5>1.5. Cổ phiếu quỹ (*)</ct411_5><ct412>2. Chênh lệch đánh giá tài sản theo giá trị hợp lý</ct412><ct413>3. Chênh lệch tỷ giá hối đoái</ct413><ct414>4. Quỹ dự trữ bổ sung vốn  điều lệ</ct414><ct415>5. Quỹ dự phòng tài chính và rủi ro nghiệp vụ</ct415><ct416>6. Các Quỹ khác thuộc vốn chủ sở hữu</ct416><ct417>7. Lợi nhuận chưa phân phối</ct417><ct417_1>7.1. Lợi nhuận sau thuế đã thực hiện</ct417_1><ct417_2>7.2. Lợi nhuận chưa thực hiện</ct417_2><ct420>II. Nguồn kinh phí và quỹ khác</ct420><ct440>TỔNG CỘNG NỢ  VÀ VỐN CHỦ SỞ HỮU (440 = 300 + 400)</ct440><ChiTieuNgoaiBang_ct001>1. Tài sản cố định thuê ngoài</ChiTieuNgoaiBang_ct001><ChiTieuNgoaiBang_ct002>2. Chứng chỉ có giá nhận giữ hộ</ChiTieuNgoaiBang_ct002><ChiTieuNgoaiBang_ct003>3. Tài sản nhận thế chấp</ChiTieuNgoaiBang_ct003><ChiTieuNgoaiBang_ct004>4. Nợ khó đòi đã xử lý</ChiTieuNgoaiBang_ct004><ChiTieuNgoaiBang_ct005>5. Ngoại tệ các loại</ChiTieuNgoaiBang_ct005><ChiTieuNgoaiBang_ct006>6. Cổ phiếu đang lưu hành</ChiTieuNgoaiBang_ct006><ChiTieuNgoaiBang_ct007>7. Cổ phiếu quỹ</ChiTieuNgoaiBang_ct007><ChiTieuNgoaiBang_ct008>8. Tài sản tài chính niêm yết/đăng ký giao dịch tại VSD của CTCK</ChiTieuNgoaiBang_ct008><ChiTieuNgoaiBang_ct009>9. Tài sản tài chính đã lưu ký tại VSD và chưa giao dịch của CTCK</ChiTieuNgoaiBang_ct009><ChiTieuNgoaiBang_ct010>10. Tài sản tài chính chờ về của CTCK</ChiTieuNgoaiBang_ct010><ChiTieuNgoaiBang_ct011>11. Tài sản tài chính sửa lỗi giao dịch của CTCK</ChiTieuNgoaiBang_ct011><ChiTieuNgoaiBang_ct012>12. Tài sản tài chính chưa lưu ký tại VSD của CTCK</ChiTieuNgoaiBang_ct012><ChiTieuNgoaiBang_ct013>13.Tài sản tài chính được hưởng quyền của CTCK</ChiTieuNgoaiBang_ct013><ChiTieuNgoaiBang_ct021>1.Tài sản tài chính niêm yết/đăng ký giao dịch tại VSD của Nhà đầu tư</ChiTieuNgoaiBang_ct021><ChiTieuNgoaiBang_ct021_1>a.Tài sản tài chính giao dịch tự do chuyển nhượng</ChiTieuNgoaiBang_ct021_1><ChiTieuNgoaiBang_ct021_2>b.Tài sản tài chính hạn chế chuyển nhượng</ChiTieuNgoaiBang_ct021_2><ChiTieuNgoaiBang_ct021_3>c.Tài sản tài chính giao dịch cầm cố</ChiTieuNgoaiBang_ct021_3><ChiTieuNgoaiBang_ct021_4>d.Tài sản tài chính phong tỏa, tạm giữ</ChiTieuNgoaiBang_ct021_4><ChiTieuNgoaiBang_ct021_5>e.Tài sản tài chính chờ thanh toán</ChiTieuNgoaiBang_ct021_5><ChiTieuNgoaiBang_ct021_6>f. Tài sản tài chính chờ cho vay</ChiTieuNgoaiBang_ct021_6><ChiTieuNgoaiBang_ct022>2. Tài sản tài chính đã lưu ký tại VSD và chưa giao dịch của Nhà đầu tư</ChiTieuNgoaiBang_ct022><ChiTieuNgoaiBang_ct022_1>a.Tài sản tài chính đã lưu ký tại VSD và chưa giao dịch, tự do chuyển nhượng</ChiTieuNgoaiBang_ct022_1><ChiTieuNgoaiBang_ct022_2>b.Tài sản tài chính đã lưu ký tại VSD và chưa giao dịch, hạn chế chuyển nhượng</ChiTieuNgoaiBang_ct022_2><ChiTieuNgoaiBang_ct022_3>c.Tài sản tài chính đã lưu ký tại VSD và chưa giao dịch, cầm cố</ChiTieuNgoaiBang_ct022_3><ChiTieuNgoaiBang_ct022_4>d.Tài sản tài chính đã lưu ký tại VSD và chưa giao dịch, phong tỏa, tạm giữ</ChiTieuNgoaiBang_ct022_4><ChiTieuNgoaiBang_ct023>3. Tài sản tài chính chờ về của Nhà đầu tư</ChiTieuNgoaiBang_ct023><ChiTieuNgoaiBang_ct024a>4. Tài sản tài chính sửa lỗi giao dịch của Nhà đầu tư</ChiTieuNgoaiBang_ct024a><ChiTieuNgoaiBang_ct024b>5.Tài sản tài chính chưa lưu ký tại VSD của Nhà đầu tư</ChiTieuNgoaiBang_ct024b><ChiTieuNgoaiBang_ct025>6.Tài sản tài chính được hưởng quyền của Nhà đầu tư</ChiTieuNgoaiBang_ct025><ChiTieuNgoaiBang_ct026>7. Tiền gửi của khách hàng</ChiTieuNgoaiBang_ct026><ChiTieuNgoaiBang_ct027>7.1. Tiền gửi của Nhà đầu tư về giao dịch chứng khoán theo phương thức CTCK quản lý</ChiTieuNgoaiBang_ct027><ChiTieuNgoaiBang_ct028>7.2.Tiền gửi tổng hợp giao dịch chứng khoán cho khách hàng</ChiTieuNgoaiBang_ct028><ChiTieuNgoaiBang_ct029>7.3. Tiền gửi bù trừ và thanh toán giao dịch chứng khoán</ChiTieuNgoaiBang_ct029><ChiTieuNgoaiBang_ct029_1>a. Tiền gửi bù trừ và thanh toán giao dịch chứng khoán Nhà đầu tư trong nước</ChiTieuNgoaiBang_ct029_1><ChiTieuNgoaiBang_ct029_2>b. Tiền gửi bù trừ và thanh toán giao dịch chứng khoán Nhà đầu tư nước ngoài</ChiTieuNgoaiBang_ct029_2><ChiTieuNgoaiBang_ct030>7.4. Tiền gửi của Tổ chức phát hành chứng khoán</ChiTieuNgoaiBang_ct030><ChiTieuNgoaiBang_ct031>8. Phải trả Nhà đầu tư về tiền gửi giao dịch chứng khoán theo phương thức CTCK quản lý</ChiTieuNgoaiBang_ct031><ChiTieuNgoaiBang_ct031_1>8.1. Phải trả Nhà đầu tư trong nước về tiền gửi giao dịch chứng khoán theo phương thức CTCK quản lý</ChiTieuNgoaiBang_ct031_1><ChiTieuNgoaiBang_ct031_2>8.2. Phải trả Nhà đầu tư nước ngoài về tiền gửi giao dịch chứng khoán theo phương thức CTCK quản lý</ChiTieuNgoaiBang_ct031_2><ChiTieuNgoaiBang_ct032>9. Phải trả Tổ chức phát hành chứng khoán</ChiTieuNgoaiBang_ct032><ChiTieuNgoaiBang_ct033>10. Phải thu của khách hàng về lỗi giao dịch các tài sản tài chính</ChiTieuNgoaiBang_ct033><ChiTieuNgoaiBang_ct034>11. Phải trả của khách hàng về lỗi giao dịch các tài sản tài chính</ChiTieuNgoaiBang_ct034><ChiTieuNgoaiBang_ct035>12. Phải trả cổ tức, gốc và lãi trái phiếu</ChiTieuNgoaiBang_ct035></root>";

	public static  String API_NHN = "http://192.168.50.97:6789/NHNService.asmx/NHNCK_SCMS?soChungChi=";//http://192.168.50.97:6789
	
}
