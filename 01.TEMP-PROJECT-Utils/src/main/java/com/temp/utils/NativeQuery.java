package com.temp.utils;

public interface NativeQuery {

	interface NhomNguoiDung{
		
		String DROPDOWN = "select  " + 
				"ID, " + 
				"CASE LEVEL " + 
				"  WHEN 1 THEN ''||TEN_NHOM_NGUOI_DUNG" + 
				"  WHEN 2 THEN '--'||TEN_NHOM_NGUOI_DUNG" + 
				"  WHEN 3 THEN '----'||TEN_NHOM_NGUOI_DUNG" + 
				"  WHEN 4 THEN '------'||TEN_NHOM_NGUOI_DUNG" + 
				"  WHEN 5 THEN '--------'||TEN_NHOM_NGUOI_DUNG" + 
				"  WHEN 6 THEN '----------'||TEN_NHOM_NGUOI_DUNG" + 
				"  ELSE '>>'||TEN_NHOM_NGUOI_DUNG END AS TEN_NHOM_NGUOI_DUNG " + 
				"FROM QT_NHOM_NGUOI_DUNG " + 
				"START WITH KHOA_CHA_ID = 0  " + 
				"connect by PRIOR ID = KHOA_CHA_ID";     
		
	}

	interface ChucNang {
		String SQL_DROPDOWN = "select " + 
				"ID," + 
				"CASE LEVEL" + 
				"  WHEN 1 THEN ''||TEN_CHUC_NANG" + 
				"  WHEN 2 THEN '--'||TEN_CHUC_NANG" + 
				"  WHEN 3 THEN '----'||TEN_CHUC_NANG" + 
				"  WHEN 4 THEN '------'||TEN_CHUC_NANG" + 
				"  WHEN 5 THEN '--------'||TEN_CHUC_NANG" + 
				"  WHEN 6 THEN '----------'||TEN_CHUC_NANG" + 
				"  ELSE '>>'||TEN_CHUC_NANG END AS TEN_CHUC_NANG " + 
				"FROM QT_CHUC_NANG " +
				"START WITH KHOA_CHA_ID = 0 " + 
				"connect by PRIOR ID = KHOA_CHA_ID";   
		
		String SQL_LIST = "SELECT " + 
				"	a.ID , " + 
				"	a.KHOA_CHA_ID , " + 
				"	a.TEN_CHUC_NANG , " + 
				"	a.ICON , " + 
				"	a.TRANG_THAI , " + 
				"	(SELECT TEN_CHUC_NANG FROM QT_CHUC_NANG_CHI_TIET WHERE ID = a.ACTION_ID and rownum = 1) AS ACTION_NAME " + 
				"FROM " + 
				"	QT_CHUC_NANG a";   
		
		 
	}
	
	interface Quyen {
		String SQL_DROPDOWN = "select " + 
				"ID," + 
				"CASE LEVEL" + 
				"  WHEN 1 THEN ''||TEN_CHUC_NANG" + 
				"  WHEN 2 THEN '--'||TEN_CHUC_NANG" + 
				"  WHEN 3 THEN '----'||TEN_CHUC_NANG" + 
				"  WHEN 4 THEN '------'||TEN_CHUC_NANG" + 
				"  WHEN 5 THEN '--------'||TEN_CHUC_NANG" + 
				"  WHEN 6 THEN '----------'||TEN_CHUC_NANG" + 
				"  ELSE '>>'||TEN_CHUC_NANG END AS TEN_CHUC_NANG " + 
				"FROM QT_CHUC_NANG " +
				"START WITH KHOA_CHA_ID = 0 " + 
				"connect by PRIOR ID = KHOA_CHA_ID";   
		
		String SQL_LIST = "SELECT " + 
				"	a.ID , " + 
				"	a.KHOA_CHA_ID , " + 
				"	a.TEN_CHUC_NANG , " + 
				"	a.ICON , " + 
				"	a.TRANG_THAI , " + 
				"	(SELECT TEN_CHUC_NANG FROM QT_CHUC_NANG_CHI_TIET WHERE ID = a.ACTION_ID and rownum = 1) AS ACTION_NAME " + 
				"FROM " + 
				"	QT_CHUC_NANG a";   
		
		 
	}
	
	interface NhanSuCaoCap{
		String SQL_NHAN_SU_LIST = "SELECT " + 
				"        t1.ID, " + 
				"        t1.ctck_thong_tin_id," + 
				"          (select c.ten_viet_tat ||'-'|| c.TEN_TIENG_VIET   from ctck_thong_tin C  WHERE C.ctck_thong_tin_id = t1.ctck_thong_tin_id AND IS_BANG_TAM = 0 )AS tEN_CONG_TY," + 
				"        HO_TEN, " + 
				"        SO_CMND, " + 
				"        (Select cn.ten_day_du from CTCK_CHI_NHANH cn where t1.ctck_chi_nhanh_id = cn.ctck_chi_nhanh_id) ," + 
				"        (Select pgd.ten_day_du from CTCK_PHONG_GIAO_DICH pgd where t1.ctck_phong_giao_dich_id = pgd.id) ," + 
				"        (Select vpdd.ten_day_du from CTCK_VP_DAI_DIEN vpdd where t1.ctck_vp_dai_dien_id = vpdd.ctck_vp_dai_dien_id)  ," + 
				"        CASE WHEN t1.chuc_vu_id IS NOT NULL THEN (SELECT LISTAGG(TEN_CHUC_VU, ', ') " + 
				"            WITHIN GROUP(ORDER BY TEN_CHUC_VU) ten_chuc_vu FROM DM_CHUC_VU t WHERE ID IN " + 
				"            (SELECT regexp_substr(t1.CHUC_VU_ID,'[^,]+', 1, level) AS list FROM dual CONNECT BY regexp_substr(t1.CHUC_VU_ID, '[^,]+', 1, level) IS NOT NULL)) END AS TEN_CHUC_VU_JOIN," + 
				"        (SELECT LISTAGG(SO_CHUNG_CHI_HNCK, ', ') WITHIN GROUP (ORDER BY SO_CHUNG_CHI_HNCK) FROM CTCK_NGUOI_HANH_NGHE_CK WHERE CTCK_NHAN_SU_ID = t1.ID) SO_CCHN_JOIN," + 
				"		 t1.ngay_bat_dau_lam_viec," + 
				"        t1.ngay_thoi_viec " +
				" FROM CTCK_NHAN_SU_CAO_CAP t1";
	}
	
	interface TongHop{
		String CAMEL="SELECT "
				+ "CTCK_THONG_TIN_ID,"
				+ "QLRR_KY_BAO_CAO_ID,"
				+ "C SUMC,"
				+ "A SUMA,"
				+ "M SUMM, " 
				+ "E SUME,"
				+ "L SUML," 
				+ "(C * TTSNC + " 
				+ " A * TTSNA + "  
				+ " M * TTSNM + " 
				+ " E * TTSNE + "  
				+ " L * TTSNL) TONG_DIEM " 
//				+ "C/(CASE WHEN TSC = 0 THEN 1 ELSE TSC END)  SUMC,"
//				+ "A/(CASE WHEN TSA = 0 THEN 1 ELSE TSA END) SUMA,"
//				+ "M/(CASE WHEN TSM = 0 THEN 1 ELSE TSM END) SUMM, " 
//				+ "E/(CASE WHEN TSE = 0 THEN 1 ELSE TSE END) SUME,"
//				+ "L/(CASE WHEN TSL = 0 THEN 1 ELSE TSL END) SUML," 
//				+ "(C/(CASE WHEN TSC = 0 THEN 1 ELSE TSC END * TTSNC) + " 
//				+ " A/(CASE WHEN TSA = 0 THEN 1 ELSE TSA END * TTSNA) + "  
//				+ " M/(CASE WHEN TSM = 0 THEN 1 ELSE TSM END * TTSNM) + " 
//				+ " E/(CASE WHEN TSE = 0 THEN 1 ELSE TSE END * TTSNE) + "  
//				+ " L/(CASE WHEN TSL = 0 THEN 1 ELSE TSL END * TTSNL)) TONG_DIEM " 
				+ " FROM ( " + 
					"SELECT  " + 
					"	CTCK_THONG_TIN_ID,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'C' AND DIEM IS NOT NULL THEN TRONG_SO/100 ELSE 0 END) TSC,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'A' AND DIEM IS NOT NULL THEN TRONG_SO/100 ELSE 0 END) TSA,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'M' AND DIEM IS NOT NULL THEN TRONG_SO/100 ELSE 0 END) TSM,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'E' AND DIEM IS NOT NULL THEN TRONG_SO/100 ELSE 0 END) TSE,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'L' AND DIEM IS NOT NULL THEN TRONG_SO/100 ELSE 0 END) TSL,  " + 
					"	(SELECT TRONG_SO/100 FROM QLRR_NHOM_CHI_TIEU WHERE TRANG_THAI = 1 AND LOAI_HINH LIKE '%C%') TTSNC," + 
					"	(SELECT TRONG_SO/100 FROM QLRR_NHOM_CHI_TIEU WHERE TRANG_THAI = 1 AND LOAI_HINH LIKE '%A%') TTSNA," + 
					"	(SELECT TRONG_SO/100 FROM QLRR_NHOM_CHI_TIEU WHERE TRANG_THAI = 1 AND LOAI_HINH LIKE '%M%') TTSNM," + 
					"	(SELECT TRONG_SO/100 FROM QLRR_NHOM_CHI_TIEU WHERE TRANG_THAI = 1 AND LOAI_HINH LIKE '%E%') TTSNE," + 
					"	(SELECT TRONG_SO/100 FROM QLRR_NHOM_CHI_TIEU WHERE TRANG_THAI = 1 AND LOAI_HINH LIKE '%L%') TTSNL," +
					"	SUM(CASE WHEN LOAI_HINH = 'C' THEN DIEM ELSE 0 END) C,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'A' THEN DIEM ELSE 0 END) A ,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'M' THEN DIEM ELSE 0 END) M ,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'E' THEN DIEM ELSE 0 END) E ,  " + 
					"	SUM(CASE WHEN LOAI_HINH = 'L' THEN DIEM ELSE 0 END) L ,  " + 
					"	QLRR_KY_BAO_CAO_ID   " + 
					"FROM  " + 
					"	QLRR_KBC_CTCK_CHI_TIET  " + 
					"	  " + 
					"	WHERE qlrr_Ky_bao_cao_id IN :LIST_KY_BAO_CAO_ID  " + 
					"GROUP BY  " + 
					"	CTCK_THONG_TIN_ID,QLRR_KY_BAO_CAO_ID " + 
				"	)";
		
		
		String CHI_TIET="SELECT  " + 
				"	CTCK_THONG_TIN_ID,  " + 
				"	QLRR_NHOM_CHI_TIEU_ID,  " + 
				"	CASE " + 
				"		WHEN (SUM(a.diem) * b.TRONG_SO) is NULL THEN 0 " + 
				"		ELSE (SUM(a.diem) * b.TRONG_SO)  " + 
				"	END AS TONG_DIEM " + 
				"	 " + 
				"FROM  " + 
				"	QLRR_KBC_CTCK_CHI_TIET A  " + 
				"LEFT JOIN QLRR_NHOM_CHI_TIEU b ON  " + 
				"	a.QLRR_NHOM_CHI_TIEU_ID = b.id  " + 
				"GROUP BY  " + 
				"	A.QLRR_NHOM_CHI_TIEU_ID,  " + 
				"	b.TRONG_SO,  " + 
				"	a.CTCK_THONG_TIN_ID ";
		
		String COLUMN="SELECT  " + 
				"	DISTINCT  id,  " + 
				"	 TEN_NHOM, " + 
				"	 LOAI_HINH, " + 
				"	 TRONG_SO " + 
				"FROM  " + 
				"	QLRR_NHOM_CHI_TIEU  WHERE TRANG_THAI = 1";
	}
	
}
