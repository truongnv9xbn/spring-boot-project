/**
 * 
 */
package com.temp.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author thangnn
 *
 */
public class FilePropertiesGlobal {
	public static Properties readPropertiesFile() {
		String fileName = "application.properties";
		InputStream fis = null;
		Properties prop = null;
		try {
			fis = FilePropertiesGlobal.class.getClassLoader().getResourceAsStream(fileName);
			prop = new Properties();
			prop.load(fis);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {

			ioe.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return prop;
	}
}
