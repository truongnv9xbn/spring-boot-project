package com.temp.utils;

import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

public class UtilsXml {
	public static <T> String jaxbObjectToXML(T docClass) {
		String xmlContent = "";
		try {
			// Create JAXB Context
			JAXBContext jaxbContext = JAXBContext.newInstance(docClass.getClass());

			// Create Marshaller
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

			// Required formatting??
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			// Print XML String to Console
			StringWriter sw = new StringWriter();
			// Write XML to StringWriter
			jaxbMarshaller.marshal(docClass, sw);

			// Verify XML Content
			xmlContent = sw.toString();

		} catch (JAXBException e) {
			//System.out.println("Lỗi jaxbObjectToXML UtilsXml");
			e.printStackTrace();
			//System.out.println("-----------------------------");
		}
		return xmlContent;
	}


	public static <T> T convertXMLtoObject(T docClass,String xmlString ) {
	JAXBContext jaxbContext;
	try
	{
	    jaxbContext = JAXBContext.newInstance(docClass.getClass());              
	 
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	 
	    docClass = (T) jaxbUnmarshaller.unmarshal(new StringReader(xmlString));
	     
	}
	catch (JAXBException e) 
	{
	    e.printStackTrace();
	}
		return docClass;
	}
	
	public static Document convertStringToXMLDocument(String xmlString) {
		// Parser that produces DOM object trees from XML content
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

		// API to obtain DOM Document instance
		DocumentBuilder builder = null;
		try {
			// Create DocumentBuilder with default configuration
			builder = factory.newDocumentBuilder();

			// Parse the content to Document object
			Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
//
//	public static Map<String, String> parse(InputSource inputSource)
//			throws SAXException, IOException, ParserConfigurationException {
//		final DataCollector handler = new DataCollector();
//		SAXParserFactory.newInstance().newSAXParser().parse(inputSource, handler);
//		return handler.result;
//	}
//
//	private static class DataCollector extends DefaultHandler {
//		private final StringBuilder buffer = new StringBuilder();
//		private final Map<String, String> result = new HashMap<String, String>();
//
//		@Override
//		public void endElement(String uri, String localName, String qName) throws SAXException {
//			final String value = buffer.toString().trim();
//			if (value.length() > 0) {
//				result.put(qName, value);
//			}
//			buffer.setLength(0);
//		}
//
//		@Override
//		public void characters(char[] ch, int start, int length) throws SAXException {
//			buffer.append(ch, start, length);
//		}
//	}

}
