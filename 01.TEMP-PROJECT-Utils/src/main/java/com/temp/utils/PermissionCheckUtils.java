/**
 * 
 */
package com.temp.utils;

/**
 * @author thangnn
 *
 */
public class PermissionCheckUtils {
	public static boolean byPassAuthority(String UriApi) {
		if (UriApi.matches(Constants.ByPassAuthor.DOWNLOAD_FILE + "(.*)")
				|| UriApi.matches(Constants.ByPassAuthor.LOG_OUT + "(.*)")
				|| UriApi.matches(Constants.ByPassAuthor.UPLOAD_FILE + "(.*)")
				|| UriApi.matches(Constants.ByPassAuthor.UPLOAD_FILE_IMPORT + "(.*)")
				|| UriApi.matches(Constants.ByPassAuthor.REFRESH_TOKEN + "(.*)")) {
			return true;
		}

		return false;
	}
}
