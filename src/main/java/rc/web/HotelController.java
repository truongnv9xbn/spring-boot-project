package rc.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import rc.Model.Hotel_BDTO;
import rc.Service.ServiceHotel;

@RestController
@Component
public class HotelController {

	@Autowired
	@Qualifier("ServiceHotels")
	private ServiceHotel service;

//    public HotelController(HotelRepository hotelRepository){
//        this.hotelRepository = hotelRepository;
//    }

//	    @Autowired // inject FirstServiceImpl
//	    public HotelController(@Qualifier("ServiceHotels") ServiceHotel service) {
//	        this.service = service;
//	    }

	@GetMapping("/hotels")
	public List<Hotel_BDTO> getHotels() {
		List<Hotel_BDTO> hotels = service.getAll();

		return hotels;
	}
}
