package com.temp.authencite;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.temp.authen.utils.JwtTokenUtil;
//import com.temp.dichvuService.DichVuService;
import com.temp.global.SendMail;
import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.ForgotPassDTO;
import com.temp.model.Common.ThamSoHeThongBDTO;
import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.authen.JwtRequest;
import com.temp.model.authen.JwtResponse;
import com.temp.model.authen.UserInfoAuthorDTO;
import com.temp.model.authen.VerifyLoginDto;
import com.temp.model.file.ChucNangUserBDTO;
import com.temp.service.Common.ThamSoHeThongService;
import com.temp.service.NguoiDung.QtNguoiDungService;
import com.temp.service.NguoiDung.userDetailsService;
import com.temp.utils.Constant;
import com.temp.utils.PasswordGenerator;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
public class JwtAuthenticationController {

	public static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationController.class);

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private userDetailsService qtService;
	
	@Autowired
	private QtNguoiDungService serviceCheckInOut;

	@Autowired
	private ThamSoHeThongService thamSoHeThongService;

	@Autowired
	@Qualifier("QtNguoiDungServiceImpl")
	private QtNguoiDungService ndService;

	/**
	 * login filter acc
	 * 
	 * @param authenticationRequest
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest,
			@Context HttpServletRequest request) throws Exception {

		String token = StringUtils.EMPTY;
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		QtNguoiDungDTO userDto = this.qtService.findByUsername(authenticationRequest.getUsername());

		UserDetails userDetails = null;
		UserInfoAuthorDTO userInfo = null;
		if (userDto != null) {
			userDetails = new org.springframework.security.core.userdetails.User(userDto.taiKhoan, userDto.matKhau,
					new ArrayList<>());
			Constant.IDUSER = userDto.getId();
		}
		String ipAddress = Utils.getClientIp(request);
		// gen token
		if (userDetails != null && userDto != null) {
			token = jwtTokenUtil.generateToken(userDetails);
			// userInfo =
//			 this.qtService.findByUsername(authenticationRequest.getUsername());

			userDto.setTokenUser(token);

			// login check ở đây
			VerifyLoginDto verify = this.serviceCheckInOut.isLoginVerity(ipAddress, userDto, true);
			if (verify != null && verify.returnVerify) {
				// set quyền và menu
				ChucNangUserBDTO lstQuyenAndMenu = null;
				if (userDto.getTaiKhoan().toLowerCase().equals("administrator")) {
					lstQuyenAndMenu = this.serviceCheckInOut.getMenuAdmin();
				} else {
					lstQuyenAndMenu = this.serviceCheckInOut.getChucNangAndMenu(userDto.getId(), true);
					if (lstQuyenAndMenu == null) {
						userDto.setQuyenJson(null);
					} else {
						userDto.setQuyenJson(new Gson().toJson(lstQuyenAndMenu.getLstChucNangQuyen()));
					}
				}
				if (lstQuyenAndMenu != null && lstQuyenAndMenu.getMenuGenerate() != null
						&& !lstQuyenAndMenu.getMenuGenerate().equals("")) {
					if (lstQuyenAndMenu.getMenuGenerate() != null) {
						String mainMenu = lstQuyenAndMenu.getMenuGenerate();
						if (!mainMenu.equals("")) {
							lstQuyenAndMenu.setMenuGenerate(mainMenu);
						}
					}
				}
				// set thong tin người dùng
				String hotLine = "";
				ThamSoHeThongBDTO thamso = thamSoHeThongService.listThamSoHeThongs("", "", "", "", 0, 9999, "", true,
						"");
				if (thamso.lstThamSoHeThong != null && thamso.lstThamSoHeThong.size() > 0) {
					for (ThamSoHeThongDTO dto : thamso.lstThamSoHeThong) {
						if (dto.getThamSo().equals(Constant.HOTLINE)) {
							hotLine = "Hotline: " + dto.getGiaTri();
						}
						if (dto.getThamSo().equals("API_HSDVC")) {
							Constant.API_HSDVC = dto.getGiaTri();
						}
						if (dto.getThamSo().equals("API_CTKT")) {
							Constant.API_CTKT = dto.getGiaTri();
						}
						if (dto.getThamSo().equals("CHECKTENCKS")) {
							Constant.CHECKTENCKS = dto.getGiaTri();
						}
						if (dto.getThamSo().equals("UPLOADNOTTEMPFOLDER")) {
							Constant.UPLOADNOTTEMPFOLDER = dto.getGiaTri();
						}
					}
				}
				;

				userInfo = new UserInfoAuthorDTO(userDto.getId(), userDto.getHoTen(), userDto.getMaNguoiDung(),
						userDto.getTrangThai(), userDto.getEmail(), userDto.getTaiKhoan(), hotLine, userDto.getAdmin(),
						userDto.getAnhDaiDien());
				this.qtService.findByUsername(authenticationRequest.getUsername());

				// TODO set các chức năng được phân quyền
				logger.debug("Login Thành công");
				if ((lstQuyenAndMenu != null && lstQuyenAndMenu.getLstChucNangMenu() != null
						&& lstQuyenAndMenu.getLstChucNangQuyen() != null)
						|| userDto.getTaiKhoan().toLowerCase().equals("administrator")) {

					return new ResponseEntity<JwtResponse>(new JwtResponse(token, lstQuyenAndMenu.getLstChucNangQuyen(),
							userInfo, lstQuyenAndMenu.getMenuGenerate(), null), HttpStatus.OK);
				}

				return new ResponseEntity<JwtResponse>(new JwtResponse(token, null, userInfo, null, null),
						HttpStatus.OK);
				// return new ResponseEntity<JwtResponse>(HttpStatus.BAD_GATEWAY);

			} else {
				if (verify != null && verify.getIpAddress() == null) {
					ipAddress = null;
				}
				return new ResponseEntity<JwtResponse>(new JwtResponse(null, null, null, null, ipAddress),
						HttpStatus.BAD_GATEWAY);
			}

		}

		return new ResponseEntity<JwtResponse>(HttpStatus.FORBIDDEN);

	}

	/**
	 * register user
	 * 
	 * @param user
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/refreshToken", method = RequestMethod.POST)
	public ResponseEntity<?> RefreshToken(@Context HttpServletRequest request) throws Exception {
		QtNguoiDungDTO userCRR = UserInfoGlobal.getUserInfoAuthor();
		String token = StringUtils.EMPTY;
		UserDetails userDetails = null;
		UserInfoAuthorDTO userInfo = null;
		if (userCRR != null) {
			userDetails = new org.springframework.security.core.userdetails.User(userCRR.taiKhoan, userCRR.matKhau,
					new ArrayList<>());
			Constant.IDUSER = userCRR.getId();
		}

		// gen token
		if (userDetails != null && userCRR != null) {
			token = jwtTokenUtil.generateToken(userDetails);
			// userInfo =
			// this.qtService.findByUsername(authenticationRequest.getUsername());

			userCRR.setTokenUser(token);

			this.serviceCheckInOut.refreshSaveToken(userCRR);
			// set thong tin người dùng
			String hotLine = "";

			ThamSoHeThongBDTO thamso = thamSoHeThongService.listThamSoHeThongs("", "", "", "", 0, 9999, "", true, "");
			if (thamso.lstThamSoHeThong != null && thamso.lstThamSoHeThong.size() > 0) {
				for (ThamSoHeThongDTO dto : thamso.lstThamSoHeThong) {
					if (dto.getThamSo().equals(Constant.HOTLINE)) {
						hotLine = "Hotline: " + dto.getGiaTri();
					}
					if (dto.getThamSo().equals("API_HSDVC")) {
						Constant.API_HSDVC = dto.getGiaTri();
					}
					if (dto.getThamSo().equals("API_CTKT")) {
						Constant.API_CTKT = dto.getGiaTri();
					}
					if (dto.getThamSo().equals("CHECKTENCKS")) {
						Constant.CHECKTENCKS = dto.getGiaTri();
					}
					if (dto.getThamSo().equals("UPLOADNOTTEMPFOLDER")) {
						Constant.UPLOADNOTTEMPFOLDER = dto.getGiaTri();
					}
				}
			}
			;
			ChucNangUserBDTO lstQuyenAndMenu = this.serviceCheckInOut.getChucNangAndMenu(userCRR.getId(), true);
			if (lstQuyenAndMenu.getLstChucNangMenu() != null && lstQuyenAndMenu.getLstChucNangMenu().size() > 0) {
				for (int a = 0; a < lstQuyenAndMenu.getLstChucNangMenu().size(); a++) {
					if (lstQuyenAndMenu.getLstChucNangMenu().get(a).getTenChucNang().equals("Báo cáo khai thác")) {
						String url = lstQuyenAndMenu.getLstChucNangMenu().get(a).getVueRoute();
						url += "loaithanhvienid=" + userCRR.getTaiKhoan() + "&" + "thanhvienid=" + userCRR.getMatKhau();
						lstQuyenAndMenu.getLstChucNangMenu().get(a).setVueRoute(url);
					}
				}
			}
			userInfo = new UserInfoAuthorDTO(userCRR.getId(), userCRR.getHoTen(), userCRR.getMaNguoiDung(),
					userCRR.getTrangThai(), userCRR.getEmail(), userCRR.getTaiKhoan(), hotLine, userCRR.getAdmin(),
					userCRR.getAnhDaiDien());

			if (lstQuyenAndMenu != null && lstQuyenAndMenu.getLstChucNangMenu() != null
					&& lstQuyenAndMenu.getLstChucNangQuyen() != null) {
				logger.debug("Login Thành công");
				return new ResponseEntity<JwtResponse>(new JwtResponse(token, lstQuyenAndMenu.getLstChucNangQuyen(),
						userInfo, lstQuyenAndMenu.getMenuGenerate(), null), HttpStatus.OK);
			}
			return new ResponseEntity<JwtResponse>(HttpStatus.BAD_GATEWAY);
		}

		return new ResponseEntity<JwtResponse>(HttpStatus.FORBIDDEN);
	}

	/**
	 * func logout
	 * 
	 * @param request
	 * @param response
	 * @return status code
	 */
	@RequestMapping(value = "/logmeout", method = RequestMethod.POST)
	public ResponseEntity<?> logoutPage(HttpServletRequest request, HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().setClearAuthentication(true);
			QtNguoiDungDTO user = this.qtService.findByUsername(auth.getName());
			user.tokenUser = StringUtils.EMPTY;
			this.serviceCheckInOut.isLoginVerity(request.getRemoteAddr(), user, false);

		}
		return new ResponseEntity<Object>(Constant.LOG_OUT_SUCCESS, HttpStatus.PARTIAL_CONTENT);
	}

//	@ResponseBody
	@RequestMapping(value = "/qtnguoidung/forgotpassword", method = RequestMethod.POST)
	public ResponseEntity<?> forgotpsassword(@RequestBody ForgotPassDTO qtNguoiDungDTO) throws Exception {
		logger.info("Quên mật khẩu");
		QtNguoiDungDTO object = qtService.findByUsername(qtNguoiDungDTO.getUsername());
		if (object.getId() > 0) {
			if (object.getEmail().equals(qtNguoiDungDTO.getEmail())) {
				try {
					String newPassword = PasswordGenerator.generateRandomPassword();
					ndService.changePasswordQtNguoiDung(object.getId(), newPassword);
					if (qtNguoiDungDTO.getEmail() != null && !qtNguoiDungDTO.getEmail().equals("")) {
						try {
							String emailTo = qtNguoiDungDTO.getEmail();
							String tieuDe = "Hệ thống SCMS cấp lại mật khẩu mới cho bạn! Vui lòng đổi lại mật khẩu để bảo vệ tài khoản";
							String noiDung = newPassword;
							SendMail.SendMailTemplate(emailTo, tieuDe, noiDung);
						} catch (Exception e) {
							return new ResponseEntity<ForgotPassDTO>(new ForgotPassDTO(), HttpStatus.ACCEPTED);
						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException("Có lỗi xảy ra khi gửi mail cấp lại mật khẩu!",
							HttpStatus.EXPECTATION_FAILED);
				}
				return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
			}
		}
		return null;
	}

	@RequestMapping(value = "/checksendmail", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> checkSendEmail(@RequestBody JwtRequest authenBody) throws Exception {
		try {
			SendMail.SendMail(authenBody.getUsername(), "Check send Email", "Check send Email body");
			return new ResponseEntity<String>("Send DONE!", HttpStatus.OK);
		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<String>(e.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/menu", method = RequestMethod.GET)
	public ResponseEntity<?> genMenu(HttpServletRequest request, HttpServletResponse response) {
		QtNguoiDungDTO userCRR = UserInfoGlobal.getUserInfoAuthor();
		if (userCRR != null) {

			this.serviceCheckInOut.getChucNangAndMenu(userCRR.getId(), false);

		}
		return new ResponseEntity<Object>(Constant.LOG_OUT_SUCCESS, HttpStatus.PARTIAL_CONTENT);
	}

	private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

//	@RequestMapping(value = "/cbtt/dropdownctckall", method = RequestMethod.GET)
//	public ResponseEntity<?> getDropdownCtckAll() throws ApiRequestException {
//		try {
//			List<DropDownDTO> lstDropdown = new ArrayList<>();
//
//			CtckThongTinBDTO objCTCK = ttCtckService.listAllFilter();
//			if (!objCTCK.getLstCtckThongTin().isEmpty()) {
//				for (CtckThongTinDTO item : objCTCK.getLstCtckThongTin()) {
//					lstDropdown.add(new DropDownDTO(item.getId(), item.getTenTiengViet()));
//				}
//			}
//			return ResponseEntity.ok().body(lstDropdown);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}

//	@RequestMapping(value = "/ctck/iframe", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> listCTCK(@RequestBody RequestBody_ctckThongTinDTO model) throws ApiRequestException {
//
////		if (!checkKeySort(keySort)) {
////			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
////		}
//		Integer pageNo = model.getPageNo();
//		Integer pageSize = model.getPageSize();
//		String keySort = model.getKeySort();
//		boolean desc = true;
//		String strfilter = model.getStrfilter();
//
//		String sTenVietTat = model.getSTenVietTat();
//		String sTenCongTy = model.getSTenCongTy();
//		Long vonDLTu = model.getVonDLTu();
//		Long vonDLDen = model.getVonDLDen();
//		String sMaSoThue = model.getSMaSoThue();
//
//		Integer nganhNgheKDId = model.getNganhNgheKDId();
//		Integer dichVuId = model.getDichVuId();
//		Integer trangThai = model.getTrangThai();
//
//		CtckThongTinJoinAllBDTO lstCtckThongTin = new CtckThongTinJoinAllBDTO();
////		if (!checkKeySort(keySort)) {
////			CtckThongTinJoinAllBDTO bdto = new CtckThongTinJoinAllBDTO();
////			lstCtckThongTin.setMessage("truyền sai keySort");
////			return new ResponseEntity<CtckThongTinJoinAllBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
////		}
//		try {
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//			lstCtckThongTin = ctckttService.listIframe(strfilter, sTenVietTat, sTenCongTy, vonDLTu, vonDLDen, sMaSoThue,
//					nganhNgheKDId, dichVuId, pageNo, pageSize, keySort, desc, trangThai, 0);
//			if (lstCtckThongTin != null) {
//				return new ResponseEntity<CtckThongTinJoinAllBDTO>(lstCtckThongTin, HttpStatus.OK);
//			}
//			return new ResponseEntity<CtckThongTinJoinAllBDTO>(lstCtckThongTin, HttpStatus.INTERNAL_SERVER_ERROR);
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error(e.toString());
//			return new ResponseEntity<CtckThongTinJoinAllBDTO>(lstCtckThongTin, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

//	@RequestMapping(value = "/ctck/dropdown", method = RequestMethod.GET)
//	@ResponseBody
//	public ResponseEntity<?> DropdownInCTCK() throws ApiRequestException {
//		Map<String, List<DropDownDTO>> map = new HashMap<>();
//		map.put("dichVu", null);
//		map.put("nganhNgheKinhDoanh", null);
//		map.put("trangThai", null);
//		try {
//			// Dropdown Dich Vu
//			DichVuBDTO lstDichVu = new DichVuBDTO();
//			lstDichVu = cvService.listDichVus("", "", "", 0, 0, "", true, "");
//			if (lstDichVu.lstDichVu != null && lstDichVu.lstDichVu.size() > 0) {
//				List<DropDownDTO> lst = new ArrayList<DropDownDTO>();
//				for (DichVuDTO d : lstDichVu.lstDichVu) {
//					DropDownDTO dropdown = new DropDownDTO();
//					dropdown.setLabel(d.getTenDichVu());
//					dropdown.setValue(d.getId());
//					lst.add(dropdown);
//				}
//				map.put("dichVu", lst);
//			}
//			// -----------------------------
//
//			// Dropdown Trang thai
//			TrangThaiCtckBDTO lstTrangThaiCtck = new TrangThaiCtckBDTO();
//			lstTrangThaiCtck = ttctkcService.listTrangThaiCtcks("", "", "", 0, 0, "", true, "");
//			if (lstTrangThaiCtck.lstTrangThaiCtck != null && lstTrangThaiCtck.lstTrangThaiCtck.size() > 0) {
//				List<DropDownDTO> lst = new ArrayList<DropDownDTO>();
//				for (TrangThaiCtckDTO d : lstTrangThaiCtck.lstTrangThaiCtck) {
//					DropDownDTO dropdown = new DropDownDTO();
//					dropdown.setLabel(d.getTenTrangThaiCtck());
//					dropdown.setValue(d.getId());
//					lst.add(dropdown);
//				}
//				map.put("trangThai", lst);
//			}
//			// -----------------------------
//
//			// Dropdown Nganh nghe kinh doanh
//			NganhNgheKdBDTO lstNganhNghe = new NganhNgheKdBDTO();
//			lstNganhNghe = nnkdService.listNganhNgheKds("", "", "", 0, 0, "", true, "");
//			if (lstNganhNghe.lstNganhNgheKd != null && lstNganhNghe.lstNganhNgheKd.size() > 0) {
//				List<DropDownDTO> lst = new ArrayList<DropDownDTO>();
//				for (NganhNgheKdDTO d : lstNganhNghe.lstNganhNgheKd) {
//					DropDownDTO dropdown = new DropDownDTO();
//					dropdown.setLabel(d.getTenNganhNgheKd());
//					dropdown.setValue(d.getId());
//					lst.add(dropdown);
//				}
//				map.put("nganhNgheKinhDoanh", lst);
//			}
//			// -----------------------------
//			if (map != null) {
//				return new ResponseEntity<Map<String, List<DropDownDTO>>>(map, HttpStatus.OK);
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error(e.toString());
//		}
//		return new ResponseEntity<Map<String, List<DropDownDTO>>>(map, HttpStatus.INTERNAL_SERVER_ERROR);
//	}

//	@RequestMapping(value = "/cbtt/iframe", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> list(@RequestBody RequestBody_CbttBaoCaoDTO model) throws ApiRequestException {
//
////		if (!checkKeySort(keySort)) {
////			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
////		}
//		Integer pageNo = model.getPageNo();
//		Integer pageSize = model.getPageSize();
//		String keySort = model.getKeySort();
//		boolean desc = true;
//		String filterkey = model.getFilterkey();
//
//		Integer ctckThongTinId = model.getCtckThongTinId();
//		String loaiCongBo = model.getLoaiCongBo();
//		String loaiTin = model.getLoaiCongBo();
//		String tieuDe = model.getTieuDe();
//		String noiDungTrichYeu = model.getNoiDungTrichYeu();
//
//		String ngayCbttTu = model.getNgayCbttTu();
//		String ngayCbttDen = model.getNgayCbttDen();
//		String ngayGuiTu = model.getNgayGuiTu();
//		String ngayGuiDen = model.getNgayGuiDen();
//		String trangThai = model.getTrangThai();
//		try {
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//			CongboThongTinBDTO cbttDto = ctckCongBoThongTin.listIframe(pageNo, pageSize, keySort, desc, filterkey,
//					ctckThongTinId, loaiCongBo, loaiTin, tieuDe, noiDungTrichYeu, ngayCbttTu, ngayCbttDen, ngayGuiTu,
//					ngayGuiDen, trangThai);
//
//			return new ResponseEntity<CongboThongTinBDTO>(cbttDto, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			e.printStackTrace();
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}

//	@RequestMapping(value = "/ctck/getbyid", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> getByIdInCTCK(@RequestBody RequestBody_ctckThongTinDTO model) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			CtckThongTinJoinAllDTO qt = ctckttService.findByJoinAllId(model.getId());
//			if (qt == null) {
//				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
//						HttpStatus.BAD_REQUEST);
//			}
//			if (qt.getGdttNgayQD() != null) {
//				qt.setGdttNgayQDStr(TimestampUtils.DateToString_ddMMyyyy(qt.getGdttNgayQD()));
//			}
//			return ResponseEntity.ok().body(qt);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}

//	@RequestMapping(value = "/ctck/chinhanh", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> listChiNhanh(@RequestBody ModelMap model) throws ApiRequestException {
//		CtckHSChiNhanhGroupBDTO ChiNhanhGroup = new CtckHSChiNhanhGroupBDTO();
//		try {
//			logger.info(Constants.Logs.LIST);
//			Integer ctckId = Integer.parseInt(model.get("sCTyCKId").toString());
//			ChiNhanhGroup = ctckcnService.listGroupByCongTy_iframe("", "", ctckId, "", "", "", "");
//
//			return new ResponseEntity<CtckHSChiNhanhGroupBDTO>(ChiNhanhGroup, HttpStatus.OK);
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<CtckHSChiNhanhGroupBDTO>(ChiNhanhGroup, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

//	@RequestMapping(value = "/ctck/cbtt", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> getCbttByIdInCTCK(@RequestBody RequestBody_CbttBaoCaoDTO model)
//			throws ApiRequestException {
//		CtckNhanSuCapCaoJoinAllBDTO lstCtckNhanSuCapCao = new CtckNhanSuCapCaoJoinAllBDTO();
//		if (!checkKeySort(model.getKeySort())) {
//			lstCtckNhanSuCapCao.setMessage("truyền sai keySort");
//			return new ResponseEntity<CtckNhanSuCapCaoJoinAllBDTO>(lstCtckNhanSuCapCao, HttpStatus.METHOD_NOT_ALLOWED);
//		}
//
//		try {
//			logger.info(Constants.Logs.LIST);
//			if (model.getPageNo() > 0) {
//				model.setPageNo(model.getPageNo() - 1);
//			}
//
//			lstCtckNhanSuCapCao = nsService.getNguoiCBTTByCtckId(model.getPageNo(), model.getPageSize(),
//					model.getKeySort(), model.getDesc(), model.getCtckThongTinId());
//
//			return new ResponseEntity<CtckNhanSuCapCaoJoinAllBDTO>(lstCtckNhanSuCapCao, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<CtckNhanSuCapCaoJoinAllBDTO>(lstCtckNhanSuCapCao,
//					HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

//	@RequestMapping(value = "/ctck/dichvu", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> getDichVuByIdInCTCK(@RequestBody RequestBody_DichVuDTO model) throws ApiRequestException {
//		CtckDichVuBDTO lstCtckDichVu = new CtckDichVuBDTO();
//		if (!checkKeySort(model.getKeySort())) {
//			lstCtckDichVu.setMessage("truyền sai keySort");
//			return new ResponseEntity<CtckDichVuBDTO>(lstCtckDichVu, HttpStatus.METHOD_NOT_ALLOWED);
//		}
//		try {
//			logger.info(Constants.Logs.LIST);
//			if (model.getPageNo() > 0) {
//				model.setPageNo(model.getPageNo() - 1);
//			}
//			lstCtckDichVu = ctckDichVuService.listCtckDichVus_iframe("", null, model.getCtckThongTinID(), "", "", "",
//					"", "", "", model.getPageNo(), model.getPageSize(), model.getKeySort(), model.getDesc(), "");
//
//			return new ResponseEntity<CtckDichVuBDTO>(lstCtckDichVu, HttpStatus.OK);
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<CtckDichVuBDTO>(lstCtckDichVu, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

//	@RequestMapping(value = "/cbtt/getbyid", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<?> getById(@RequestBody RequestBody_CbttBaoCaoDTO model) throws ApiRequestException {
//
//		CbttBaoCaoDTO dto = new CbttBaoCaoDTO();
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			if (model.getId() != null) {
//				dto = ctckCongBoThongTin.findById(model.getId());
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//		return ResponseEntity.ok().body(dto);
//	}
//
//	@RequestMapping(value = "/cbtt/init", method = RequestMethod.POST)
//	@ResponseBody
//	public ResponseEntity<CongboThongTinBDTO> init(@RequestBody RequestBody_CbttBaoCaoDTO model)
//			throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.INIT_CREATE);
//
//			CongboThongTinBDTO cbttDto = ctckCongBoThongTin.initCreate(model.getCtckThongTinId());
//			return new ResponseEntity<CongboThongTinBDTO>(cbttDto, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}

//	private boolean checkKeySort(String keySort) {
//
//		Field fld[] = CtckXuLyHanhChinhDTO.class.getDeclaredFields();
//		List<String> listName = new ArrayList<String>();
//
//		for (int i = 0; i < fld.length; i++) {
//			listName.add(fld[i].getName());
//		}
//
//		return listName.contains(keySort);
//	}
}