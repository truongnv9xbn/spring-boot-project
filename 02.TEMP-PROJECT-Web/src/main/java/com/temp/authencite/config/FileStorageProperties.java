/**
 * 
 */
package com.temp.authencite.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author thang
 *
 */
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {

	 String uploadDir;

	public String getUploadDir() {
		return uploadDir;
	}

	public void setUploadDir(String uploadDir) {
		this.uploadDir = uploadDir;
	}
}
