package com.temp.authencite.config;

import java.io.IOException;
import java.nio.channels.AcceptPendingException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.naming.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.temp.authen.utils.JwtTokenUtil;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.file.ChucNangUserBDTO;
import com.temp.service.NguoiDung.QtNguoiDungService;
import com.temp.service.NguoiDung.userDetailsService;
import com.temp.springboot.web.apps.FileStorage;
import com.temp.utils.Utils;

import io.jsonwebtoken.ExpiredJwtException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

	@Autowired
	private userDetailsService qtService;
	
	@Autowired
	private QtNguoiDungService qtNguoiDungService;

	@Autowired
	private JwtTokenUtil jwtTokenUtil;
	
	@Autowired
	private FileStorage fileStorageService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws ServletException, IOException {
		try {
//			if(request.getRequestURI().contains("remixKySo")){
//				kySo(request, response);
//			}
//			else {
				final String requestTokenHeader = request.getHeader("Authorization");
				boolean isPreview = false;
				String username = null;
				String jwtToken = null;
				String uri = request.getRequestURI().toLowerCase().toLowerCase();

				// check url download lấy token
				if ((uri.contains("/uploadfile/") || uri.contains("/uploadFile/") )) {
					String tokenArr[] = (request.getRequestURL() + "").split("%60");
					if (tokenArr.length > 1 && tokenArr.length < 3) {
						jwtToken = tokenArr[tokenArr.length - 1];
						isPreview = true;
					}
				}
				String ip = Utils.getClientIp(request);
				//System.out.println("----- đây là ip request từ client-----");
				//System.out.println(ip);
				//System.out.println("----- END-----");

				// JWT Token is in the form "Bearer token". Remove Bearer word and get
				// only the Token
				if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ") || isPreview) {
					if (!isPreview) {
						jwtToken = requestTokenHeader.substring(7);

					}
					try {
						username = jwtTokenUtil.getUsernameFromToken(jwtToken);

					} catch (IllegalArgumentException e) {
						throw new AuthenticationException();
//						//System.out.println("Unable to get JWT Token");
					} catch (ExpiredJwtException e) {
						throw new AuthenticationException();
//						//System.out.println("JWT Token has expired");
					}
				} else {
					logger.warn("JWT Token does not begin with Bearer String");
				}

				// Once we get the token validate it.
				if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
					QtNguoiDungDTO user = this.qtService.findByUsername(username);
					if (StringUtils.isEmpty(user.getTokenUser())) {
						throw new AcceptPendingException();
					}
					UserDetails userDetails = this.qtService.loadUserByUsername(username);

					// if token is valid configure Spring Security to manually set
					// authentication
					if (userDetails != null && jwtTokenUtil.validateToken(jwtToken, userDetails)) {

						// khi token có valid thì vào check quyền
						// mặc định by pass những chức năng k cần phân quyền
						if (userDetails.getUsername().toLowerCase().equals("administrator")) {
							// không làm gì cả, mặc định auto bypass filter quyền
						}
						else {
							//List chức năng check quyền của hệ thống
							List<String> lstAllChucNangChiTiet = this.qtNguoiDungService.getAllChucNangChiTiet();
							if(lstAllChucNangChiTiet != null && lstAllChucNangChiTiet.size() > 0) {
								//check uri có thuộc list chức năng cần check
								boolean check = lstAllChucNangChiTiet.stream().anyMatch(str -> str != null && uri != null 
										&& (str.toLowerCase().trim().contains(uri.toLowerCase().trim()) 
												|| uri.toLowerCase().trim().contains(str.toLowerCase().trim())));
 								if(check) {
									ChucNangUserBDTO lstQuyenAndMenu = null;
									lstQuyenAndMenu = this.qtNguoiDungService.getChucNangAndMenu(user.getId(), false);
									if (lstQuyenAndMenu == null || (lstQuyenAndMenu != null && lstQuyenAndMenu.getLstChucNangQuyen() != null && lstQuyenAndMenu.getLstChucNangQuyen().size() > 0))
									{
										check = lstQuyenAndMenu.getLstChucNangQuyen().stream().anyMatch(str -> str != null && (str.trim().contains(uri) || uri.trim().contains(str)));
										if(!check) {
											//System.out.println(uri);
											throw new LockedException("No permission");
										}
									}
								}
							}
						}
						UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
								userDetails, null, userDetails.getAuthorities());
						usernamePasswordAuthenticationToken
								.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
						// After setting the Authentication in the context, we specify
						// that the current user is authenticated. So it passes the
						// Spring Security Configurations successfully.
						SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
					} else if (username != null) {
						UserDetails userDetail = this.qtService.loadUserByUsername(username);
						if (jwtTokenUtil.validateToken(jwtToken, userDetail)) {
							throw new ServletException();
						}

					}

					if (!user.tokenUser.equals(jwtToken)
							&& !request.getRequestURI().toLowerCase().contains("/authenticate")) {
						throw new AuthenticationException();
//						throw new server
					}
				}
//			}

			chain.doFilter(request, response);
			

		} catch (AuthenticationException e) {
			response.setStatus(HttpStatus.UNAUTHORIZED.value());
		} catch (LockedException e) {
			response.setStatus(HttpStatus.LOCKED.value());
		} catch (AcceptPendingException e) {
			response.setStatus(HttpStatus.FAILED_DEPENDENCY.value());
		}

	}

//	private String getFileName(String submittedFileName) {
//		String[] splitFile = org.springframework.util.StringUtils.cleanPath(submittedFileName).split("\\.");
//		String fileName = org.apache.commons.lang3.StringUtils.EMPTY;
//		String extentionFile = org.apache.commons.lang3.StringUtils.EMPTY;
//		String defineFileName = org.apache.commons.lang3.StringUtils.EMPTY;
//		Date date = new Date();
//		if (splitFile != null && splitFile.length > 0) {
//			fileName = splitFile[0];
//			extentionFile = splitFile[splitFile.length - 1];
//			fileName = fileName.replaceAll("~", "");
//			fileName = fileName.replaceAll("`", "");
//			defineFileName = fileName + date.getTime() + PasswordGenerator.generateRandomPassword() + "."
//					+ extentionFile;
//			// Copy file to the target location (Replacing existing file with the same name)
//			Path targetLocation = this.fileStorageLocation.resolve(defineFileName);
//			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
//			return null;
//		}
//		else
//			return submittedFileName;
//	}

	// nếu được quyền return false
	/// còn không return true
	private boolean checkIgoneQuyen(String URIapi, List<String> dsQuyen) {

		if (dsQuyen != null) {
			String url = dsQuyen.stream().filter(x -> (URIapi).matches(x + "(.*)")).findAny().orElse(null);
			if (StringUtils.isEmpty(url)) {
				return true;

			}
		}
		return false;
	}
	
	private String getFileName(Part part) {
	    String contentDisp = part.getHeader("content-disposition");
	    //System.out.println("content-disposition header= " + contentDisp);
	    String[] tokens = contentDisp.split(";");
	    for (String token : tokens) {
	        if (token.trim().startsWith("filename")) {
	            return token.substring(token.indexOf("=") + 2, token.length() - 1);
	        }
	    }
	    return "";
	}
	
//	private void kySo(HttpServletRequest request, HttpServletResponse response) {
//		try {
//		Part filePart = request.getPart("uploadfile");
////		String tempLokation = System.getProperty("user.home") + "\\Desktop\\";
//		Path tmpPath = Paths.get(Constants.FolderExport.FOLDER_VBKS);
//		String tmp = request.getServletContext().getRealPath("/") + tmpPath.toFile();
//		File tDir = new File(tmp);
//		if (!tDir.exists()) {
//			boolean resultDir = tDir.mkdirs();
//		}
//		//System.out.println("Check : "+request.getServletContext().getRealPath("/") + tmpPath.toFile());
//
//			
//		String tempLokation = "uploadFileTmp//";
//		String[] tempName = filePart.getSubmittedFileName().split("\\.");
//		Date date = new Date();
//		String fileName = tempName[0] + "_" + date.getTime() + "_signed." + tempName[1];
//		File file = new File(tempLokation + fileName);
//		InputStream tempIs = filePart.getInputStream();
//		try(OutputStream outputStream = new FileOutputStream(file)){
//		    IOUtils.copy(tempIs, outputStream);
//		    outputStream.flush();
//		    IOUtils.closeQuietly(outputStream);
//		    IOUtils.closeQuietly(tempIs);
//		} catch (Exception e) {
//		    // handle exception here
//		}
//		Utils.CommonSavePathFile(tempLokation + fileName + "~" + fileName,
//				Constants.FolderExport.FOLDER_VBKS, request);
//		response.setContentType("text/html; charset=UTF-8");
//        PrintWriter out = response.getWriter();
//        out.println("{\"Status\":true, \"Message\": \"\", \"FileName\": \"" + "" + "\", \"FileServer\": \"" + fileName + "\"}");
//        out.close();
//		}
//		catch(Exception e) {
//			
//		}
//		 
//	}
	@Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		Collection<String> excludeUrlPatterns = new HashSet<String>();
		AntPathMatcher pathMatcher = new AntPathMatcher(); 
		try {
			excludeUrlPatterns.add("/cbtt/**");
			excludeUrlPatterns.add("/ctck/**");
			excludeUrlPatterns.add("/service/**");
			excludeUrlPatterns.add("/lien-thong/**");
			excludeUrlPatterns.add("/dong-bo/**");
			excludeUrlPatterns.add("/printing/**");
			excludeUrlPatterns.add("/printingReal/**");
			excludeUrlPatterns.add("/favicon.ico");
			excludeUrlPatterns.add("/remixKySo");
			excludeUrlPatterns.add("/checksendmail");
			excludeUrlPatterns.add("/qtnguoidung/forgotpassword");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        return excludeUrlPatterns.stream()
                .anyMatch(p -> pathMatcher.match(p, request.getServletPath()));
    }//new AntPathMatcher().match("/cbtt/**", request.getServletPath());
}