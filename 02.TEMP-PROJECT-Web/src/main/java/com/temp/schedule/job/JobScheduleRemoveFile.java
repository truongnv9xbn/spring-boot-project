///**
// * 
// */
//package com.temp.schedule.job;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.HashMap;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//
//import com.example.DTO_LienThong.NguoiHanhNgheLtDTO;
//import com.example.DTO_LienThong.NguoiHanhNgheXmlBDTO;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.common.base.Joiner;
//
//import okhttp3.MediaType;
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.RequestBody;
//import okhttp3.Response;
//import com.temp.authencite.config.FileStorageProperties;
//import com.temp.ctcknguoihanhngheckService.CtckNguoiHanhNgheCkService;
//import com.temp.model.CtckNguoiHanhNgheCkJoinAllBDTO;
//import com.temp.model.CtckNguoiHanhNgheCkJoinAllDTO;
//import com.temp.persistance.entity.CtckNguoiHanhNgheCkEntity;
//import com.temp.persistence.responsitories.CtckNguoiHanhNgheCkEntityRepository;
//import com.temp.springboot.web.controllerlienthong.LT_MainRESTController;
//import com.temp.utils.Constants;
//import com.temp.utils.ObjectMapperUtils;
//import com.temp.utils.TimestampUtils;
//import com.temp.utils.UtilsXml;
//
///**
// * @author thangnn
// *
// */
//@ControllerAdvice
//public class JobScheduleRemoveFile {
//
//	public static final Logger logger = LoggerFactory.getLogger(JobScheduleRemoveFile.class);
//
//	@Autowired
//	private LT_MainRESTController MainRESTController;
//	
//	@Autowired
//	private FileStorageProperties fileStorage;
//
//	@Autowired
//	@Qualifier("CtckNguoiHanhNgheCkServiceImpl")
//	private CtckNguoiHanhNgheCkService NHNService;
//
//	private CtckNguoiHanhNgheCkEntityRepository ctckNguoiHanhNgheCkEntityRepository;
//
//	@Autowired
//	public JobScheduleRemoveFile(CtckNguoiHanhNgheCkEntityRepository ctckNguoiHanhNgheCkEntityRepository) {
//		this.ctckNguoiHanhNgheCkEntityRepository = ctckNguoiHanhNgheCkEntityRepository;
//	}
//
//	/**
//	 * job xóa file folder upload trung giang nếu file còn tồn đọng sẽ đc xóa lúc 0h
//	 * 00' hàng ngày.
//	 */
//	@Scheduled(cron = "0 0 0 * * *")
//	public void run() {
//		logger.info("Job xóa file rác chạy lúc :: " + Calendar.getInstance().getTime());
//		try {
//			String tmp = Paths.get(fileStorage.getUploadDir()).toAbsolutePath().normalize().toString();
//			this.findFiles(tmp);
//		} catch (IOException e) {
//
//			e.printStackTrace();
//		}
//	}
//
////	https://o7planning.org/vi/11131/chay-cac-nhiem-vu-nen-theo-lich-trinh-trong-spring#a4401487
////	"0 0 * * * *" // Đầu giờ của tất cả các giờ của tất cả các ngày.
////	"*/10 * * * * *" // Mỗi 10 giây (số giây chia hết cho 10).
////	"0 0 8-10 * * *" // 8, 9 và 10 giờ các ngày
////	"0 0/30 8-10 * * *" // 8:00, 8:30, 9:00, 9:30 và 10 tất cả các ngày
////	"0 0 9-17 * * MON-FRI" // 9, .. 17 giờ các ngày thứ 2 tới thứ 6 (monday & friday)
////	"0 0 0 25 12 ?" // Tất cả các ngày giáng sinh, nửa đêm.
//	@Scheduled(cron = "0 0 8 * * *", zone = "Asia/Saigon") // "0 0 8 * * *" "*/20 0 0 * * *"
//	public void IsExitsNHNCK() {
//		logger.info(Constants.Logs.SYNC_NHNCK);
//		CtckNguoiHanhNgheCkJoinAllBDTO bdto = new CtckNguoiHanhNgheCkJoinAllBDTO();
//		ObjectMapper objectMapper = new ObjectMapper();
//		NguoiHanhNgheXmlBDTO NHNBDTO = new NguoiHanhNgheXmlBDTO();
//		try {
//			bdto = NHNService.listAll(0);
//			if (bdto.lstCtckNguoiHanhNgheCk != null && bdto.lstCtckNguoiHanhNgheCk.size() > 0) {
//				//System.out.println("NguoiHanhNgheCk: " + bdto.lstCtckNguoiHanhNgheCk.size() + " " + TimestampUtils.getCurrentTimestamp());
////				OkHttpClient client = new OkHttpClient();
//
////				MediaType mediaType = MediaType.parse("application/json");
//				Map<String, String> map = new HashMap<String, String>();
//
//				Set<String> lstSCC = new HashSet<String>();
//				for (CtckNguoiHanhNgheCkJoinAllDTO dto : bdto.lstCtckNguoiHanhNgheCk) {
//					lstSCC.add("'" + dto.getSoCCHNCK() + "'");
//				}
//				String sSCC = Joiner.on(", ").join(lstSCC);
//				map.put("soChungChi", sSCC);
//				String requestJson = objectMapper.writeValueAsString(map);
//				String returnXml = "";
//
//				// get string xml
////				RequestBody body = RequestBody.create(mediaType, requestJson);// "{\"soChungChi\":\"000050/PTTC\"}"
////				Request request = new Request.Builder().url("http://localhost:8080/NHNCK").post(body)
////						.addHeader("content-type", "application/json").build();
////				Response response = client.newCall(request).execute();
////				returnXml = response.body().string();
////				response.close();
//				// end
//				returnXml = MainRESTController.checkNHNCK(map);
//				 
//				NHNBDTO = UtilsXml.convertXMLtoObject(new NguoiHanhNgheXmlBDTO(), returnXml);
//				if (NHNBDTO.lst != null && NHNBDTO.lst.size() > 0) {
//					for (CtckNguoiHanhNgheCkJoinAllDTO d : bdto.lstCtckNguoiHanhNgheCk) {
//						if (NHNBDTO.lst.stream()
//								.filter(x -> x.getSoChungChi().toUpperCase().equals(d.getSoCCHNCK().toUpperCase()))
//								.collect(Collectors.toList()).size() == 0) {
//							d.setTrangThai(false);
//							d.setGhiChu("Số chứng chỉ không tồn tại");
//							saveAndFlush_NHN(d);
//							continue;
//						}
//
//						for (NguoiHanhNgheLtDTO o : NHNBDTO.lst.stream()
//								.filter(x -> x.getSoChungChi().toUpperCase().equals(d.getSoCCHNCK().toUpperCase()))
//								.collect(Collectors.toList())) {
//							String ghiChu = "";
//							Timestamp oNgaySinh = null;
//							Timestamp dNgaySinh = null;
//							if (o.getNgaySinh() != null && d.getNgaySinh() != null) {
//								oNgaySinh = TimestampUtils.convertStringToTimestampFormat_ddMMYYYY(o.getNgaySinh());
//								dNgaySinh = TimestampUtils.convertStringToTimestampFormat_ddMMYYYY(d.getNgaySinhStr());
//							}
//
//							if (o.getHoTen().toUpperCase().equals(d.getHoTen().toUpperCase()) && oNgaySinh != null
//									&& dNgaySinh != null && oNgaySinh.compareTo(dNgaySinh) == 0
//									&& (o.getCmtndHoChieu().equals(d.getSoCmnd()) || o.getSoCmt().equals(d.getSoCmnd())
//											|| o.getSoCmtCu().equals(d.getSoCmnd()))) {
//								d.setTrangThai(true);
//								d.setGhiChu("Xác nhận");
//								saveAndFlush_NHN(d);
//								break;
//							} else if (!o.getHoTen().toUpperCase().equals(d.getHoTen().toUpperCase())) {
//								ghiChu += "Họ tên không trùng khớp, ";
//							} else if (oNgaySinh.compareTo(dNgaySinh) != 0) {
//								ghiChu += "Ngày sinh không trùng khớp, ";
//							} else if ((!o.getCmtndHoChieu().equals(d.getSoCmnd())
//									|| !o.getSoCmt().equals(d.getSoCmnd()) || !o.getSoCmtCu().equals(d.getSoCmnd()))) {
//								ghiChu += "Số cmnd không trùng khớp, ";
//							}
//							d.setTrangThai(false);
//							d.setGhiChu(ghiChu.substring(0, ghiChu.length() - 2));
//							saveAndFlush_NHN(d);
//						}
//
//					}
//
//				}
//
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
////		------------------------------------------------------------------------//
//		try {
//			OkHttpClient client = new OkHttpClient();
//			String returnXml = "";
//			bdto = NHNService.listAll(1);
//			if (bdto.lstCtckNguoiHanhNgheCk != null && bdto.lstCtckNguoiHanhNgheCk.size() > 0) {
//				NguoiHanhNgheXmlBDTO  s = new NguoiHanhNgheXmlBDTO();
//				List<NguoiHanhNgheLtDTO> o = new ArrayList<NguoiHanhNgheLtDTO>();
//				for (CtckNguoiHanhNgheCkJoinAllDTO d : bdto.lstCtckNguoiHanhNgheCk) {
//					NguoiHanhNgheLtDTO t = new NguoiHanhNgheLtDTO();
//					t.setId(d.getId());
//					t.setHoTen(d.getHoTen());
//					t.setNgaySinh(d.getNgaySinhStr());
//					t.setSoCmt(d.getSoCmnd());
//					t.setSoChungChi(d.getSoCCHNCK());
//					t.setNoiLamViec(d.getTenCtck());
//					t.setNgayBatDauLamViec(TimestampUtils.DateToString_ddMMyyyy(d.getNgayBatDauLamViec()));
//					t.setNgayNghiViec(TimestampUtils.DateToString_ddMMyyyy(d.getNgayThoiViec()));
//					o.add(t);
//				}
//				s.lst = o;
//				// get string xml
//				String requestJson = objectMapper.writeValueAsString(s);
//				MediaType mediaType = MediaType.parse("application/json");
//				RequestBody body = RequestBody.create(mediaType, requestJson);// "{\"soChungChi\":\"000050/PTTC\"}"
//				Request request = new Request.Builder().url("http://localhost:8080/syncNHN").post(body)
//						.addHeader("content-type", "application/json").build();
//				Response response = client.newCall(request).execute();
//				returnXml = response.body().string();
//				response.close();
//			}
//			// end
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//	};
//
//	private void saveAndFlush_NHN(CtckNguoiHanhNgheCkJoinAllDTO d) {
//		try {
//			CtckNguoiHanhNgheCkEntity entity = ObjectMapperUtils.map(d, CtckNguoiHanhNgheCkEntity.class);
//			entity.setThoiGianSync(TimestampUtils.getCurrentTimestamp());
//			ctckNguoiHanhNgheCkEntityRepository.saveAndFlush(entity);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
//
//	private void findFiles(String filePath) throws IOException {
//		List<File> files = Files.list(Paths.get(filePath)).map(path -> path.toFile()).collect(Collectors.toList());
//		for (File file : files) {
//
//			deleteFile(file);
//
//		}
//
//	}
//
//	private void deleteFile(File file) {
//		file.delete();
//	}
//
//}
