///**
// * 
// */
//package com.temp.schedule.job;
//
//import java.lang.reflect.Type;
//import java.sql.Timestamp;
//import java.util.ArrayList;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//import java.util.stream.Collectors;
//
//import javax.script.ScriptEngine;
//import javax.script.ScriptEngineManager;
//import javax.script.ScriptException;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.scheduling.Trigger;
//import org.springframework.scheduling.TriggerContext;
//import org.springframework.scheduling.annotation.SchedulingConfigurer;
//import org.springframework.scheduling.config.ScheduledTaskRegistrar;
//
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//
//import com.temp.bcThanhVienService.BcThanhVienService;
//import com.temp.bcbaocaogtService.BcBaoCaoGtService;
//import com.temp.bmbaocaoService.BmBaoCaoService;
//import com.temp.cbChitieuService.CanhBaoChiTieuService;
//import com.temp.cbctckviphamService.CbCtckViPhamService;
//import com.temp.chitieudieukienService.CbChiTieuDieuKienService;
//import com.temp.ctckthongtin.Service.CtckThongTinService;
//import com.temp.global.exception.ApiRequestException;
//import com.temp.model.BcBaoCaoGtDTO;
//import com.temp.model.BcThanhVienDTO;
//import com.temp.model.BmSheetCtDTO;
//import com.temp.model.CbChiTieuBDTO;
//import com.temp.model.CbChiTieuDTO;
//import com.temp.model.CbChiTieuDieuKienDTO;
//import com.temp.model.CbChiTieuJoinChiTieuDisplayBDTO;
//import com.temp.model.CbChiTieuThucHienDTO;
//import com.temp.model.CbCtckViPhamDTO;
//import com.temp.model.CongThucThucHienDTO;
//import com.temp.model.CtckThongTinDTO;
//import com.temp.model.LkNguoiDungCtckDTO;
//import com.temp.model.NguoiDungJoinAllDTO;
//import com.temp.model.QtNguoiDungBDTO;
//import com.temp.model.ThamSoHeThongDTO;
//import com.temp.persistence.dao.LkNguoiDungCtckDao;
//import com.temp.persistence.dao.QtNguoiDungDao;
//import com.temp.qtLogHeThongService.QtLogHeThongService;
//import com.temp.sheetService.SheetService;
//import com.temp.sheetcellService.SheetCellService;
//import com.temp.thamsohethongService.ThamSoHeThongService;
//import com.temp.thietLapCongThucLogic.ThietLapCongThucLogic;
//import com.temp.thongbaoService.ThongBaoService;
//import com.temp.utils.Constants;
//
///**
// * @author thangnn
// *
// */
//	@Configuration
//	public class JobScheduleRemoveFile2 implements SchedulingConfigurer {
//
//	ThamSoHeThongDTO thamso = new ThamSoHeThongDTO();
//	ThamSoHeThongDTO thamsoCheck = new ThamSoHeThongDTO();
//	
//	CbChiTieuBDTO cbct = new CbChiTieuBDTO();
//	
//	
//	@Autowired
//	@Qualifier("ThamSoHeThongServiceImpl")
//	private ThamSoHeThongService thamSoHeThongService;
//
//	@Autowired
//	@Qualifier("CbChitieuServiceImpl")
//	private CanhBaoChiTieuService canhBaoChiTieuService;
//
//	@Autowired
//	@Qualifier("QtLogHeThongServiceImpl")
//	private QtLogHeThongService logHeThongService;
//
//	@Autowired
//	@Qualifier("CbChiTieuDieuKienServiceImpl")
//	private CbChiTieuDieuKienService ctdkService;
//
//	@Autowired
//	@Qualifier("BcThanhVienServiceImpl")
//	private BcThanhVienService bcThanhVienService;
//
//	@Autowired
//	@Qualifier("BcBaoCaoGtServiceImpl")
//	private BcBaoCaoGtService bcBaoCaoGtService;
//
//	@Autowired
//	@Qualifier("CbCtckViPhamServiceImpl")
//	private CbCtckViPhamService cbCtckViPhamService;
//
//	@Autowired
//	@Qualifier("SheetCellServiceImpl")
//	private SheetCellService sheetCTService;
//
//	@Autowired
//	@Qualifier("BmBaoCaoServiceImp")
//	private BmBaoCaoService bcService;
//
//	@Autowired
//	@Qualifier("SheetServiceImpl")
//	private SheetService sheetService;
//
//	@Autowired
//	@Qualifier("ThongBaoServiceImpl")
//	private ThongBaoService thongBaoService;
//
//	@Autowired
//	private LkNguoiDungCtckDao lkNdCtckDao;
//
//	@Autowired
//	@Qualifier("CtckThongTinServiceImpl")
//	private CtckThongTinService ctckttService;
//
//	@Autowired
//	private ThietLapCongThucLogic thietLapCongThucLogic;
//
//	@Autowired
//	private QtNguoiDungDao qtNguoiDungDao;
//	 
//	@Override
//	public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
//	taskRegistrar.addFixedRateTask(new Runnable() {
//	@Override
//	public void run() {
//		try {
//			thamsoCheck = thamSoHeThongService.findByThamSo("CHECK_AUTO_CANH_BAO");
//		} catch (Exception e) {
////			e.printStackTrace();
//		}
//		if(thamsoCheck != null && thamsoCheck.getGiaTri() != null && thamsoCheck.getGiaTri().toString().equals("1")) {
//			//System.out.println("Started running Schedular..."+Calendar.getInstance().getTime());
//			cbct = canhBaoChiTieuService.getListCanhBaoChiTieuService(null, null, null, null, null, null, 0, 9999999, null, true);
//			List<CbChiTieuThucHienDTO> result = new ArrayList<CbChiTieuThucHienDTO>();
//			if(cbct != null && cbct.getLstCbChiTieuJoinDisplayBDTO() != null && cbct.getLstCbChiTieuJoinDisplayBDTO().size() > 0) {
//				List<String> kyBaoCao = new ArrayList<String>();
//				kyBaoCao.add("Q");
//				kyBaoCao.add("TH");
//				kyBaoCao.add("BN");
//				kyBaoCao.add("NAM");
//				for(CbChiTieuJoinChiTieuDisplayBDTO bdto : cbct.getLstCbChiTieuJoinDisplayBDTO()) {
//					for(String ky : kyBaoCao) {
//						CbChiTieuThucHienDTO temp = new CbChiTieuThucHienDTO();
//						temp.setCanhBaoId(bdto.getCanhBaoGroupId());
//						temp.setKyCanhBao(ky);
//						temp.setNamCanhBao(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
//						result.add(temp);
//					}
//				}
//			}
//			//System.out.println("Running Schedular..."+Calendar.getInstance().getTime());
//			for(CbChiTieuThucHienDTO requestDTO : result) {
//				try {
//					canhBaoChiTIeu(requestDTO);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			//System.out.println("Finished running Schedular..."+Calendar.getInstance().getTime());
//	    }
//		else {
//			//System.out.println("Stopping Schedular..."+Calendar.getInstance().getTime());
//		}
//	}
//
//	private String canhBaoChiTIeu(CbChiTieuThucHienDTO requestDTO) throws Exception {
//		   String errorMsg = "Lỗi khi thực hiện cảnh báo chỉ tiêu. Vui lòng liên hệ với QTV";
//		   List<CbCtckViPhamDTO> lstAll = cbCtckViPhamService.getAllCbCtckViPham();
//
//			// Check canh bao ID
//			if (requestDTO.getCanhBaoId() == null) {
//				errorMsg = "Không tìm thấy dữ liệu cảnh báo.";
//				return errorMsg;
//			}
//			// #1: Lay du lieu canh bao chi tieu
//			CbChiTieuBDTO cbChiTieuBDTO = canhBaoChiTieuService
//					.getCanhBaoChiTieuByIdService(requestDTO.getCanhBaoId());
//			// Check du lieu CbChiTieu get by Id
//			if (cbChiTieuBDTO == null || cbChiTieuBDTO.getCanhBaoChiTieuDetail() == null) {
//				errorMsg = "Không tìm thấy dữ liệu cảnh báo.";
//				return errorMsg;
//			}
//			List<CbCtckViPhamDTO> lsCongTyViPhamDieuKien = null;
//			//Check neu o ghi chu khong co thi se la cac chi tieu tinh theo cong thuc
//			if(cbChiTieuBDTO.getCanhBaoChiTieuDetail().getLoaiChiTieuCB() == null || cbChiTieuBDTO.getCanhBaoChiTieuDetail().getLoaiChiTieuCB() == 0) {
//				// #2: Lay du lieu cong thuc thuc hien canh bao
//				List<CongThucThucHienDTO> lsDuLieuCongThuc = this
//						.getDuLieuCongThucCanhBaoLs(cbChiTieuBDTO.getCanhBaoChiTieuDetail().getCongThucText());
//		
//				// check ls cong thuc.
//				if (lsDuLieuCongThuc == null) {
//					errorMsg = "Công thức thực hiện cảnh báo không có hoặc sai định dạng.";
//					return errorMsg;
//				}
//				
//				// #3: Lay ds du lieu bao cao thanh vien theo ky/nam canh bao
//				List<BcThanhVienDTO> lsThanhVien = bcThanhVienService
//						.getDsThanhVienByKyAndNamCanhBao(requestDTO.getNamCanhBao(), requestDTO.getKyCanhBao());
//				if (lsThanhVien == null || lsThanhVien.isEmpty()) {
//					errorMsg = "Không tìm thấy dữ liệu báo cáo thành viên.";
//					return errorMsg;
//				}
//
//				// #4: Lay ds du lieu Ctck kiem tra dieu kien
//				List<Integer> lsCtckId = this.getCtckIdFromThanhVienLs(lsThanhVien);
//				if (lsCtckId == null) {
//					errorMsg = "Không tìm thấy dữ liệu báo Công ty CK gửi báo cáo.";
//					return errorMsg;
//				}
//				
//				// #5: Lay gia tri chi tieu, tinh toan, thuc hien phep toan, tra ve ds ctck vi
//				// pham
//				/// Tinh toan gia tri chi tieu theo tung cong ty.
//				/// Tra ve danh sach du lieu ctck vi pham.
//				lsCongTyViPhamDieuKien = this.xuLyCanhBaoChiTieuDieuKien(lsDuLieuCongThuc, lsCtckId,
//						lsThanhVien, cbChiTieuBDTO.getCanhBaoChiTieuDetail(), lstAll);
//			}
//			//Neu la chi tieu canh bao nhnck
//			else if(cbChiTieuBDTO.getCanhBaoChiTieuDetail().getLoaiChiTieuCB() == null || cbChiTieuBDTO.getCanhBaoChiTieuDetail().getLoaiChiTieuCB() == 1) { 
//				// #2: Lay danh sach ctck theo chi nhanh
//				lsCongTyViPhamDieuKien = cbCtckViPhamService.getExistNguoiHanhNgheCtCkViPham(cbChiTieuBDTO.getCanhBaoChiTieuDetail());
//			}
//
//			try {
//				// #6: Them moi ds ctck vi pham.
//				// write update log
//				if (lsCongTyViPhamDieuKien != null && !lsCongTyViPhamDieuKien.isEmpty()) {
//					boolean isAddedCtCkViPham = cbCtckViPhamService
//							.addDsCtckViPhamChiTieuService(lsCongTyViPhamDieuKien);
//					if (isAddedCtCkViPham) {
//						this.addThongBao(lsCongTyViPhamDieuKien);
//					} else if (!isAddedCtCkViPham) {
//						errorMsg = "Xảy ra lỗi khi thêm mới Công ty chứng khoán vi phạm!";
//						return errorMsg;
//					}
//				}
//				return "Cảnh báo thành công";
//
//			} catch (Exception e) {
//				e.printStackTrace();
//				return errorMsg;
//			}
//	   }
//	   
//	   private List<CongThucThucHienDTO> getDuLieuCongThucCanhBaoLs(String congThucText) throws Exception {
//			try {
//
//				// Check du lieu CbChiTieu get by Id
//				if (congThucText == null || congThucText.isEmpty()) {
//					return null;
//				}
//
//				// Parse Cong thuc json to Cong thuc detail list
//				Gson gson = new Gson();
//				Type userListType = new TypeToken<List<CongThucThucHienDTO>>() {
//				}.getType();
//				List<CongThucThucHienDTO> congThucDetailLs = gson.fromJson(congThucText, userListType);
//
//				// Check result.
//				if (congThucDetailLs == null || congThucDetailLs.isEmpty()) {
//					return null;
//				}
//
//				return congThucDetailLs;
//
//			} catch (Exception e) {
//				e.printStackTrace();
//				return null;
//			}
//		}
//	   
//	   private List<Integer> getCtckIdFromThanhVienLs(List<BcThanhVienDTO> lsThanhVien) throws Exception {
//			List<Integer> lsCtck = new ArrayList<Integer>();
//
//			// get ds ctck.
//			lsCtck = lsThanhVien.stream().map(m -> m.getCtckThongTinId()).distinct().collect(Collectors.toList());
//
//			return (lsCtck == null || lsCtck.isEmpty()) ? null : lsCtck;
//		}
//	   
//	   private void addThongBao(List<CbCtckViPhamDTO> lsCongTyViPhamDieuKien) {
//			List<LkNguoiDungCtckDTO> tempLk = new ArrayList<LkNguoiDungCtckDTO>();
//			CtckThongTinDTO tempCtck = new CtckThongTinDTO();
//			CbChiTieuBDTO tempCbCt = new CbChiTieuBDTO();
//			if (lsCongTyViPhamDieuKien != null && lsCongTyViPhamDieuKien.size() > 0) {
//				for (CbCtckViPhamDTO tempCtckViPham : lsCongTyViPhamDieuKien) {
//					if (tempCtckViPham != null && tempCtckViPham.getCtckThongTinId() > 0
//							&& tempCtckViPham.getCbChiTieuId() > 0) {
//						tempLk = lkNdCtckDao.getLstNguoiDungFromCtck(tempCtckViPham.getCtckThongTinId());
//						if (tempLk != null && tempLk.size() > 0) {
//							Set<Integer> nguoiNhan = new HashSet<Integer>();
//							if (tempLk != null && tempLk.size() > 0) {
//								for (LkNguoiDungCtckDTO tempDto : tempLk)
//									nguoiNhan.add(tempDto.getNguoiDungId());
//							}
//							QtNguoiDungBDTO tempBDTO = new QtNguoiDungBDTO();
//							List<NguoiDungJoinAllDTO> temp = new ArrayList<NguoiDungJoinAllDTO>();
//							tempBDTO = qtNguoiDungDao.listAllNguoiDung("id", true, "1");
//							if (tempBDTO != null) {
//								temp = tempBDTO.getLstQtNguoiDung().stream()
//										.filter(x -> x.getCtckId() != null
//												&& x.getCtckId().equals(tempCtckViPham.getCtckThongTinId())
//												&& x.getThanhVien() == true && x.getTrangThai() == true)
//										.collect(Collectors.toList());
////	    					(oldDTO.getCtckThongTinId()
//							}
//							if (temp.size() > 0) {
//								for (NguoiDungJoinAllDTO tempDto : temp)
//									nguoiNhan.add(tempDto.getId());
//							}
//							tempCtck = ctckttService.getById(tempCtckViPham.getCtckThongTinId());
//							tempCbCt = canhBaoChiTieuService.getCanhBaoChiTieuByIdService(tempCtckViPham.getCbChiTieuId());
//							if (tempCtck != null && nguoiNhan.size() > 0 && tempCbCt != null
//									&& tempCtck.getTenTiengViet() != null && tempCbCt.getCanhBaoChiTieuDetail() != null
//									&& tempCbCt.getCanhBaoChiTieuDetail().getTenCanhBao() != null) {
//								thongBaoService.addThongBao(
//										tempCtck.getTenTiengViet() + " vi phạm tham số cảnh báo về "
//												+ tempCbCt.getCanhBaoChiTieuDetail().getTenCanhBao(),
//										tempCtck.getTenTiengViet() + " vi phạm tham số cảnh báo về "
//												+ tempCbCt.getCanhBaoChiTieuDetail().getTenCanhBao(),
//										"dscongtyvipham", nguoiNhan);
//							}
//
//						}
//					}
//				}
//			}
//
//		}
//	   
//	   private List<CbCtckViPhamDTO> xuLyCanhBaoChiTieuDieuKien(List<CongThucThucHienDTO> lsDuLieuCongThuc,
//				List<Integer> lsCtckId, List<BcThanhVienDTO> lsThanhVien, CbChiTieuDTO cbChiTieuDTO, List<CbCtckViPhamDTO> lstAll) throws Exception {
//			// Output: Danh sach cong ty Ck vi pham dieu kien
//			List<CbCtckViPhamDTO> lsCongTyViPhamDieuKien = new ArrayList<CbCtckViPhamDTO>();
//
//			for (Integer ctck : lsCtckId) {
//
//				// #1. Lay danh sach ctck thanh vien theo tung ky/gia tri ky.
//				List<BcThanhVienDTO> lsThanhVienCtck = this.getLsThanhVienOfCtCk(lsThanhVien, ctck);
//
//				// Check list thanh vien ctck
//				if (lsThanhVienCtck == null) {
//					continue;
//				}
//
//				for (BcThanhVienDTO ctckThanhVien : lsThanhVienCtck) {
//					// #2. Neu cong ty Ck da co du lieu trong Ds Cong ty vi pham, bo qua.
//					// Check danh sach vi pham co ton tai du lieu Ctck & Ky & Nam Canh Bao.
//					/// example: Danh sach Vi pham co du lieu: CtCk A, Ky bao cao TH , gia tri
//					/// ky: 12, nam canh bao 2019
//					CbCtckViPhamDTO existDulieuCtckViPham = cbCtckViPhamService.getExistDuLieuCtCkViPham(ctck,
//							ctckThanhVien.getNamCanhBao(), ctckThanhVien.getKyBaoCao(), ctckThanhVien.getGiaTriKyBc());
//					/// Neu co du lieu, bo qua.
//					if (existDulieuCtckViPham != null) {
//						continue;
//					}
//
//					// #3. Lay gia tri theo cong thuc chi tieu.
//					// lay list gia tri chi tieu theo cong thuc cua tung Ctck & gia tri ky xac dinh.
//					List<CbChiTieuThucHienDTO> lsGiaTriChitieuCongThucByCtck = this
//							.getLsGiaTriChiTieuCongThucByCtckAndGtKy(ctckThanhVien, lsDuLieuCongThuc);
//
//					// Generate phep toan theo gia tri va cong thuc
//					// ex: 1+2-3
//					String phepToanGiaTri = this.generatePhepToanGiaTriCongThuc(lsGiaTriChitieuCongThucByCtck);
//					if (phepToanGiaTri == null || phepToanGiaTri.isEmpty()) {
//						continue;
//					}
//
//					// #4. Tinh toan gia tri chi tieu (cua Ctck/gia tri ky) theo Cong thuc.
//					/// tinh toan gia tri theo phep toan.
//					Double giaTriPhepToan = this.thucHienTinhToanGiaTriCongThuc(phepToanGiaTri);
//					if (giaTriPhepToan == null) {
//						continue;
//					}
//
//					// #5. So sanh gia tri vua tinh toan voi dieu kien canh bao.
//					/// return null neu gia tri valid.
//					/// return du lieu ctck vi phep neu gia tri invalid.
//					CbCtckViPhamDTO ctckViPhamData = this.kiemTraGiaTriTinhToanVoiDieuKienCanhBao(giaTriPhepToan,
//							cbChiTieuDTO, ctckThanhVien, lstAll);
//					if (ctckViPhamData == null) {
//						continue;
//					}
//					// Add du lieu ctck vi pham vao ds ctck vi pham
//					lsCongTyViPhamDieuKien.add(ctckViPhamData);
//
//				}
//
//			}
//			return lsCongTyViPhamDieuKien;
//
//		}
//	   
//	   private void getGiaTriSuDungHam(CongThucThucHienDTO congThuc, Integer thanhVienId,
//				CbChiTieuThucHienDTO cbChiTieuThucHien) throws Exception {
//			// kiem tra ham tinh toan
//			if (congThuc.getHamTinhToan() == null || congThuc.getHamTinhToan().isEmpty()) {
//				cbChiTieuThucHien.setGiaTriKqTinhToan("a");
//			} else {
//
//				// Xu ly tinh toan lay gia tri theo ham SUM, AVG, ...
//				switch (congThuc.getHamTinhToan()) {
//				/// Tổng tất cả các ô của cột đã chọn
//				case "SUM":
//					Integer sumResult = bcBaoCaoGtService.getGiaTriTinhToanHamSum(congThuc, thanhVienId);
//					cbChiTieuThucHien.setGiaTriKqTinhToan((sumResult == null) ? "a" : sumResult.toString());
//					break;
//				/// Trung bình tổng của các ô cột đã chọn
//				case "AVG":
//					// TODO Tinh gia tri ham AVG
//					break;
//				/// Nếu có giá trị thì + thêm 1 (= >= <= > < IN NOTIN)
//				case "CIF":
//					// TODO Tinh gia tri ham CIF
//					break;
//				/// Tổng tất cả các ô có giá trị (không group)
//				case "SUMALL":
//					// TODO Tinh gia tri ham SUMALL
//					break;
//				/// Get tất cả các báo cáo có kỳ giá trị nhỏ hơn theo năm
//				case "ACC":
//					// TODO Tinh gia tri ham ACC
//					break;
//
//				default:
//					cbChiTieuThucHien.setGiaTriKqTinhToan("a");
//					break;
//				}
//			}
//
//		}
//
//		/**
//		 * <pre>
//		 * Lay Gia tri o chi tieu trong bang BC_BAO_CAO_GT.
//		 * </pre>
//		 * 
//		 * <pre>
//		 * Neu khong co gia tri. 
//		 * Lay gia tri GIA_TRI_DEFAULT trong bang BM_SHEET_CT
//		 * </pre>
//		 * 
//		 * @param bmSheetChiTieuId
//		 * @param id
//		 * @param cbChiTieuThucHien
//		 * @throws Exception
//		 */
//		private void getGiaTriOChiTieu(String bmSheetChiTieuId, Integer thanhVienId, CbChiTieuThucHienDTO cbChiTieuThucHien)
//				throws Exception {
//			BcBaoCaoGtDTO bcGiaTriDto = bcBaoCaoGtService.getGiaTriOChiTieuCongThuc(thanhVienId,
//					Integer.parseInt(bmSheetChiTieuId));
//
//			// Gia tri default khi cong thuc khong co gia tri.
//			String giaTri = "a";
//
//			// Neu khong co gia tri, lay gia tri default tu bang BC_SHEET_CT
//			if (bcGiaTriDto == null || bcGiaTriDto.getGiaTri() == null) {
//				/// Lay gia tri sheet CT.
//				/// DucHV [CR-20200518] Update get bmSheetCT theo field BmSheetVaoID.
////		    BmSheetCtDTO bmSheetCt = this.sheetCTService
////			    .FindById(Integer.parseInt(bmSheetChiTieuId));
//				BmSheetCtDTO bmSheetCt = sheetCTService.findByBmSheetCtVaoId(Integer.parseInt(bmSheetChiTieuId));
//				/// Lay gia tri default bmSheetCt
//				if (bmSheetCt != null && bmSheetCt.getGiaTriDefault() != null) {
//					giaTri = bmSheetCt.getGiaTriDefault().toString();
//				}
//
//			}
//			// Set gia tri to kq.
//			else if (bcGiaTriDto.getGiaTri() != null && !bcGiaTriDto.getGiaTri().isEmpty()) {
//				giaTri = bcGiaTriDto.getGiaTri();
//			} else {
//				giaTri = "a";
//			}
//			// Set gia tri o chi tieu
//			cbChiTieuThucHien.setGiaTriKqTinhToan(giaTri);
//		}
//
//		/**
//		 * Lay danh sach bao cao thanh vien cua cong ty chung khoan.
//		 * 
//		 * @param lsThanhVien
//		 * @param lsCtckId
//		 * @return
//		 * @throws Ex
//		 */
//		private List<BcThanhVienDTO> getLsThanhVienOfCtCk(List<BcThanhVienDTO> lsThanhVien, Integer ctck) throws Exception {
//
//			List<BcThanhVienDTO> lsThanhVienCtck = new ArrayList<BcThanhVienDTO>();
//			lsThanhVienCtck = lsThanhVien.stream().filter(f -> f.getCtckThongTinId().intValue() == ctck.intValue())
//					.collect(Collectors.toList());
//
//			return (lsThanhVienCtck == null || lsThanhVienCtck.isEmpty()) ? null : lsThanhVienCtck;
//		}
//		
//		private CbCtckViPhamDTO kiemTraGiaTriTinhToanVoiDieuKienCanhBao(Double giaTriPhepToan, CbChiTieuDTO cbChiTieuDTO,
//				BcThanhVienDTO ctckThanhVien, List<CbCtckViPhamDTO> lstAll) throws Exception {
//			String errorMsg = Constants.Messages.ACTION_VIPHAM_CHECK_DK_EXCEPTION_MSG;
//
//			// Build dieu kien so sanh.
//			StringBuilder dieuKienSoSanh = new StringBuilder();
//			StringBuilder dieuKienSoSanhText = new StringBuilder();
//			String prefix = "";
//			for (CbChiTieuDieuKienDTO dieuKien : cbChiTieuDTO.getCbChiTieuDieuKiens()) {
//				dieuKienSoSanh.append(prefix);
//				dieuKienSoSanhText.append(prefix); // gen dieu kien so sanh text
//				prefix = " && ";
//				dieuKienSoSanh.append(giaTriPhepToan);
//				dieuKienSoSanhText.append(" " + dieuKien.getTenDieuKien()); // gen dieu kien so sanh
//				// text
//				dieuKienSoSanh.append(dieuKien.getDieuKien());
//				dieuKienSoSanhText.append(" " + dieuKien.getDieuKien()); // gen dieu kien so sanh text
//				dieuKienSoSanh.append(dieuKien.getGiaTriSoSanh());
//				dieuKienSoSanhText.append(" " + dieuKien.getGiaTriSoSanh()); // gen dieu kien so sanh
//				// text
//			}
//
//			// Thuc hien kiem tra dieu kien hop le.
//			ScriptEngineManager manager = new ScriptEngineManager();
//			ScriptEngine engine = manager.getEngineByName("js");
//			Object result = null;
//
//			try {
//				result = engine.eval(dieuKienSoSanh.toString());
//
//			} catch (ScriptException e) {
//				// e.printStackTrace();
//				//System.out.println(e.getMessage());
//				throw new ApiRequestException(errorMsg, HttpStatus.EXPECTATION_FAILED);
//			}
//			// Get ket qua kiem tra theo dieu kien canh bao
//			boolean isValided = Boolean.TRUE.equals(result);
//
//			// Return du lieu ctck vi phep neu gia tri invalid.
//			if (isValided) {
//				CbCtckViPhamDTO temp = lstAll.stream()
//						.filter(x -> String.valueOf(x.getCtckThongTinId()).equals(String.valueOf((ctckThanhVien.getCtckThongTinId())))
//							&& 	String.valueOf(x.getCbChiTieuId()).equals(String.valueOf((ctckThanhVien.getId())))
//							&& 	String.valueOf(x.getNamCanhBao()).equals(String.valueOf((ctckThanhVien.getNamCanhBao())))
//							&& 	String.valueOf(x.getKyBaoCao()).equals(String.valueOf((ctckThanhVien.getKyBaoCao())))
//							&& 	String.valueOf(x.getGiaTriKyBc()).equals(String.valueOf((ctckThanhVien.getGiaTriKyBc())))
//								)
//						.findAny().orElse(null);
//				if(temp != null)
//					return null;
//				else {
//					// set du lieu cong ty ck vi pham.
//					CbCtckViPhamDTO ctckViPhamData = new CbCtckViPhamDTO();
//					ctckViPhamData.setGiaTriThucTe(giaTriPhepToan.longValue());
//					ctckViPhamData.setDieuKien(dieuKienSoSanhText.toString());
//					ctckViPhamData.setCbChiTieuId(cbChiTieuDTO.getId());
//					ctckViPhamData.setCapDo(cbChiTieuDTO.getCapDo());
//					ctckViPhamData.setCtckThongTinId(ctckThanhVien.getCtckThongTinId());
//					ctckViPhamData.setNamCanhBao(ctckThanhVien.getNamCanhBao());
//					ctckViPhamData.setKyBaoCao(ctckThanhVien.getKyBaoCao());
//					ctckViPhamData.setGiaTriKyBc(ctckThanhVien.getGiaTriKyBc());
//					ctckViPhamData.setNgayTao(new Timestamp(new Date().getTime()));
//
//					return ctckViPhamData;
//					
//				}
//			}
//			return null;
//		}
//		
//		private List<CbChiTieuThucHienDTO> getLsGiaTriChiTieuCongThucByCtckAndGtKy(BcThanhVienDTO ctckThanhVien,
//				List<CongThucThucHienDTO> lsDuLieuCongThuc) throws Exception {
//			List<CbChiTieuThucHienDTO> lsGiaTriCongThucOutput = new ArrayList<CbChiTieuThucHienDTO>();
//			CbChiTieuThucHienDTO giaTriCongThuc = null;
//			for (CongThucThucHienDTO congThuc : lsDuLieuCongThuc) {
//				giaTriCongThuc = new CbChiTieuThucHienDTO();
//
//				// get gia tri chi tieu, gia tri ham, he so, phep tinh chi tiet.
//				giaTriCongThuc = this.getGiaTriChiTieuChitiet(ctckThanhVien, congThuc);
//				// set gia tri to list gia tri cong thuc.
//				lsGiaTriCongThucOutput.add(giaTriCongThuc);
//			}
//			return lsGiaTriCongThucOutput;
//		}
//		
//		private Double thucHienTinhToanGiaTriCongThuc(String phepToanGiaTri) throws Exception {
//			Object result = null;
//			if (phepToanGiaTri.isEmpty()) {
//				return null;
//			}
//			try {
//				double valueParse = Long.parseLong(phepToanGiaTri);
//				return valueParse;
//			} catch (Exception e) {
//				// TODO: handle exception
//				//System.out.println(e.getMessage());
//			}
//			try {
//				// Thuc hien tinh toan lay gia tri
//				ScriptEngineManager manager = new ScriptEngineManager();
//				ScriptEngine engine = manager.getEngineByName("js");
//				result = engine.eval(phepToanGiaTri);
////		    String strDouble = String.format("%.0f", result);
////		    BigDecimal kqParse = new BigDecimal(strDouble);
////		    return kqParse.longValue();
//				// return ket qua phep toan
//				Double kqParse = Double.parseDouble(result.toString());
////		    double check1 = kqParse.doubleValue();
//				return kqParse;
//			} catch (Exception e) {
//				// e.printStackTrace();
//				//System.out.println(e.getMessage());
//				return null;
//			}
//
//		}
//
//		/**
//		 * Generate phep toan theo gia tri vs cong thuc
//		 * 
//		 * @param lsGiaTriChitieuCongThucByCtck
//		 * @return phep toan theo cong thuc. ex: 1+2-3
//		 * @throws Exception
//		 */
//		private String generatePhepToanGiaTriCongThuc(List<CbChiTieuThucHienDTO> lsGiaTriChitieuCongThucByCtck)
//				throws Exception {
//			StringBuilder phepToanGiaTri = new StringBuilder();
//
//			if (lsGiaTriChitieuCongThucByCtck.isEmpty()) {
//				return null;
//			}
//
//			for (CbChiTieuThucHienDTO giaTriCtChiTieu : lsGiaTriChitieuCongThucByCtck) {
//				if (giaTriCtChiTieu != null) {
//					phepToanGiaTri.append(giaTriCtChiTieu.getGiaTriKqTinhToan());
//				}
//			}
//			return phepToanGiaTri.toString();
//		}
//		
//		private CbChiTieuThucHienDTO getGiaTriChiTieuChitiet(BcThanhVienDTO ctckThanhVien, CongThucThucHienDTO congThuc)
//				throws Exception {
//
//			CbChiTieuThucHienDTO cbChiTieuThucHien = new CbChiTieuThucHienDTO();
//			cbChiTieuThucHien.setCtCkID(ctckThanhVien.getCtckThongTinId());
//			cbChiTieuThucHien.setGiaTriKyCanhBao(ctckThanhVien.getGiaTriKyBc());
//			cbChiTieuThucHien.setKyCanhBao(ctckThanhVien.getKyBaoCao());
//			cbChiTieuThucHien.setGiatriOChitieu(congThuc.getGiatriOChitieu());
//
//			// Check He So, tra ve gia tri he so.
//			if (congThuc.getHeSo() != null && !congThuc.getHeSo().isEmpty()) {
//				// Set gia tri he so
//				cbChiTieuThucHien.setGiaTriKqTinhToan(congThuc.getHeSo().toString());
//				return cbChiTieuThucHien;
//			}
//
//			// Check Phep toan, tra ve phep toan
//			if (congThuc.getPhepToan() != null && !congThuc.getPhepToan().isEmpty()) {
//				// Set gia tri phep toan.
//				cbChiTieuThucHien.setGiaTriKqTinhToan(congThuc.getPhepToan());
//				return cbChiTieuThucHien;
//			}
//
//			// Get gia tri chi tieu
//			if ((congThuc.getSudungHam() != null && !congThuc.getSudungHam().isEmpty())
//					&& (congThuc.getSudungHam().equals("0"))) {
//				this.getGiaTriOChiTieu(congThuc.getBmSheetChiTieuId(), ctckThanhVien.getId(), cbChiTieuThucHien);
//				return cbChiTieuThucHien;
//			}
//
//			if (congThuc.getDinhDangKyTu() != null) {
//				String ketQua = thietLapCongThucLogic.getGiaTriDinhDang(congThuc.getDinhDangKyTu(),
//						ctckThanhVien.getCtckThongTinId());
//				cbChiTieuThucHien.setGiaTriKqTinhToan(ketQua);
//				return cbChiTieuThucHien;
//			}
//
//			// Get gia tri su dung ham.
//			if ((congThuc.getSudungHam() != null && !congThuc.getSudungHam().isEmpty())
//					&& Integer.parseInt(congThuc.getSudungHam()) == 1) {
//				// Get gia tri su dung ham.
//				this.getGiaTriSuDungHam(congThuc, ctckThanhVien.getId(), cbChiTieuThucHien);
//				return cbChiTieuThucHien;
//			}
//
//			return null;
//
//		}
//	   }, 
////	new Trigger() {
////	@Override
////	public Date nextExecutionTime(TriggerContext triggerContext) {
//////		ThamSoHeThongDTO thamso = new ThamSoHeThongDTO();
////		try {
////			thamso = thamSoHeThongService.findByThamSo("TIME_CANH_BAO");
////		} catch (Exception e) {
//////			e.printStackTrace();
////		}
////	      Calendar nextExecutionTime = new GregorianCalendar();
////	      Date lastActualExecutionTime = triggerContext.lastActualExecutionTime();
////	      nextExecutionTime.setTime(lastActualExecutionTime != null ? lastActualExecutionTime : new Date());
////	      nextExecutionTime.add(Calendar.MILLISECOND, getNewExecutionTime(thamso));
////	      return nextExecutionTime.getTime();
////	     }
////	   }
//			getNewExecutionTimeLong()
//	   );
//	
//}
//
////	   private int getNewExecutionTime(ThamSoHeThongDTO thamso) {
////		   //minute to millisecond
////		   if(thamso.getGiaTri() != null)
////			   return Integer.parseInt(thamso.getGiaTri())*60000;
////		   else return 20*60000;
////	   }
//	   
//	   private Long getNewExecutionTimeLong() {
//		   try {
//			thamso = thamSoHeThongService.findByThamSo("TIME_CANH_BAO");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		   //minute to millisecond
//		   if(thamso.getGiaTri() != null)
//			   return Long.parseLong(thamso.getGiaTri())*60000;
//		   else 
//			   return 20L*60000L;
//	   }
//	   
//	   
//	}
