package com.temp.soapservice;

//import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WSConfig extends WsConfigurerAdapter {
	@Bean
	public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setApplicationContext(applicationContext);
		servlet.setTransformWsdlLocations(true);
		return new ServletRegistrationBean(servlet, "/service/*");
	}

	@Bean(name = "CongtyDetailsWsdl")
	public DefaultWsdl11Definition congTyWsdl11Definition(XsdSchema schema) {	
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("CongtysPort");
		wsdl11Definition.setLocationUri("/service/congty");
		wsdl11Definition.setTargetNamespace("https://scms.tinhvan.com/congty");
		wsdl11Definition.setSchema(schema);
		return wsdl11Definition;
	}
//	@Bean(name = "BaocaoDetailsWsdl")
//	public DefaultWsdl11Definition baocaoWsdl11Definition(XsdSchema schema) {
//		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
//		wsdl11Definition.setPortTypeName("BaocaosPort");
//		wsdl11Definition.setLocationUri("/service/baocao");
//		wsdl11Definition.setTargetNamespace("https://scms.tinhvan.com/baocao");
//		wsdl11Definition.setSchema(schema);
//		return wsdl11Definition;
//	}
	@Bean 
	public XsdSchema congtySchema() {
		return new SimpleXsdSchema(new ClassPathResource("configXSD.xsd"));
	}
}