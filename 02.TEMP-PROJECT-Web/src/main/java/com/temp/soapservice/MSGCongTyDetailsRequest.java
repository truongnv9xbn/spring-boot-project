package com.temp.soapservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "rowPerpage", "maSoThue"})
@XmlRootElement(name = "MSGCongTyDetailsRequest")
@Data
public class MSGCongTyDetailsRequest {
	protected Integer rowPerpage;
	protected String maSoThue;

	public MSGCongTyDetailsRequest(Integer rowPerpage, String maSoThue) {
		super();
		this.rowPerpage = rowPerpage != null ? rowPerpage : 0;
		this.maSoThue = maSoThue != null ? maSoThue : null;
	}
}
