//package com.temp.soapservice;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.ws.server.endpoint.annotation.Endpoint;
//import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
//import org.springframework.ws.server.endpoint.annotation.RequestPayload;
//import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
//
//import https.scms_tinhvan_com.congty.MSG;
//import https.scms_tinhvan_com.congty.MSGCongTyDetailsRequest;
//import https.scms_tinhvan_com.congty.MSGCongTyDetailsResponse;
//import https.scms_tinhvan_com.congty.MSGDetailsRequest;
//import https.scms_tinhvan_com.congty.MSGDetailsResponse;
//import com.temp.lienthongService.SinhDuLieuService;
//
//@Endpoint
//public class SinhDuLieuEndpoint {
////	private static final String BAOCAO_URI = "https://scms.tinhvan.com/baocao";
//	private static final String CONGTY_URI = "https://scms.tinhvan.com/congty";
//
//	@Autowired
//	@Qualifier("SinhDuLieuServiceImpl")
//	private SinhDuLieuService qtService;
//
//	@PayloadRoot(namespace = CONGTY_URI, localPart = "MSGDetailsRequest")
//	@ResponsePayload
//	public MSGDetailsResponse getBaoCao(@RequestPayload MSGDetailsRequest request) {
//		MSG msg = new MSG();
//		try {
//			com.example.DTO_LienThong.MSG msgDTO = qtService.sendNotificationBaoCao1(request.getPKyBaoCao(), request.getPLoaiBaoCao(),
//					request.getPMaSoThue());
//			msg.setError(msgDTO.getError());
//			msg.setMsg(msgDTO.getMsg());
//			msg.setXML(msgDTO.getXML());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		MSGDetailsResponse eResponse = new MSGDetailsResponse();
//		eResponse.setMSGDetailsResult(msg);
//		return eResponse;
//	}
//
//	@PayloadRoot(namespace = CONGTY_URI, localPart = "MSGCongTyDetailsRequest")
//	@ResponsePayload
//	public MSGCongTyDetailsResponse getCongTy(@RequestPayload MSGCongTyDetailsRequest request) {
//		MSG msg = new MSG();
//		try {
//			String message = qtService.sendNotificationCongTy(request.getRowPerpage(), request.getMaSoThue());
//			if (message != "" && message.equals("Thành công")) {
//				msg.setError("false");
//				msg.setMsg("Trả dữ liệu thành công");
//				msg.setXML("");
//			} else {
//				msg.setError("true");
//				msg.setMsg("Có lỗi khi lấy dữ liệu hoặc không có dữ liệu");
//				msg.setXML("");
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		MSGCongTyDetailsResponse eResponse = new MSGCongTyDetailsResponse();
//		eResponse.setMSGCongTyDetailsResult(msg);
//		return eResponse;
//	}
//
//}