package com.temp.springboot.web.controller.Common;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.Common.BaiVietBDTO;
import com.temp.model.Common.BaiVietDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.Common.BaiVietService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class BaiVietApiController {

	public static final Logger logger = LoggerFactory.getLogger(BaiVietApiController.class);
	@Autowired
	@Qualifier("BaiVietServiceImpl")
	private BaiVietService baiVietService;

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService baiVietServiceLog;

	@RequestMapping(value = "/baiviet/list", method = RequestMethod.GET)
	public ResponseEntity<BaiVietBDTO> getList(
			@Valid @RequestParam(value = "keysearch", required = false) String keysearch,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sChuyenMucId", required = false) Integer sChuyenMucId,
			@RequestParam(value = "sTieuDe", required = false) String sTieuDe,
			@RequestParam(value = "sMoTaNgan", required = false) String sMoTaNgan,
			@RequestParam(value = "sNoiDung", required = false) String sNoiDung,
			@RequestParam(value = "sNguoiTao", required = false) String sNguoiTao,
			@RequestParam(value = "sNoiBat", required = false) Integer sNoiBat,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		BaiVietBDTO lstBaiViet = new BaiVietBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			lstBaiViet = baiVietService.getList(keysearch, sChuyenMucId, sTieuDe, sMoTaNgan, sNoiDung, sNguoiTao, sNoiBat,
					trangThai, pageNo, pageSize, keySort, desc);

			return new ResponseEntity<BaiVietBDTO>(lstBaiViet, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PostMapping("/baiviet/add")
	public ResponseEntity<?> add(@Valid @RequestBody BaiVietDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.CREATE);

		if (!baiVietService.isExistBaiVietAdd(dto)) {
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			dto.setNguoiTao(userInfo.getHoTen());
			try {
				baiVietService.saveBaiViet(dto);

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_BAI_VIET");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới bài viết");
				// save db
				baiVietServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<BaiVietDTO>(dto, HttpStatus.CREATED);

			} catch (Exception e) {
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	@PutMapping("/baiviet/update")
	public ResponseEntity<?> update(@Valid @RequestBody BaiVietDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		logger.info(Constants.Logs.UPDATE);

		if (baiVietService.isExistById(dto.getId())) {
			if (!baiVietService.isExistBaiVietUpdate(dto)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				try {
					baiVietService.updateBaiViet(dto);

					Timestamp timeStamp = new Timestamp(new Date().getTime());
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BAI_VIET");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật bài viết");
					// save db
					baiVietServiceLog.AddLogHeThong(dtoLog);

					return new ResponseEntity<BaiVietDTO>(dto, HttpStatus.ACCEPTED);
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}

			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

	}

	@DeleteMapping("/baiviet/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			if (!baiVietService.isExistById(id)) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

			}
			if (baiVietService.deleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_BAI_VIET");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa bài viết");
				// save db
				baiVietServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/baiviet/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			BaiVietDTO qt = baiVietService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/baiviet/pheduyet/{id}")
	public ResponseEntity<?> pheDuyet(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			BaiVietDTO qt = baiVietService.findById(id);
			baiVietService.updateBaiViet(qt);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = BaiVietDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}

//	@RequestMapping(value = "/baiviet/exitsMa", method = RequestMethod.GET)
//	public ResponseEntity<Boolean> checkExitsByMa(@RequestParam(value = "maBaiViet", required = false) String maBaiViet,
//			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
//		Boolean result = false;
//
//		try {
//			logger.info(Constants.Logs.EXITS);
//
//			result = baiVietService.isExistByMa(maBaiViet, id);
//
//			return new ResponseEntity<Boolean>(result, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	

//	@RequestMapping(value = "/baiviet/list", method = RequestMethod.GET)
//	public ResponseEntity<BaiVietBDTO> getList(
//			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "sTenBaiViet", required = false) String sTenBaiViet,
//			@RequestParam(value = "sMaBaiViet", required = false) String sMaBaiViet,
//			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {
//
//		if (!checkKeySort(keySort)) {
//
//			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//
//		BaiVietBDTO lstBaiViet = new BaiVietBDTO();
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//			if (trangThai != null && trangThai.trim().toLowerCase() == "Sử Dụng") {
//				trangThai = "1";
//			} else if (trangThai != null && trangThai.trim().toLowerCase() == "Không Sử dụng") {
//				trangThai = "0";
//			}
//
//			lstBaiViet = baiVietService.listBaiViets(strfilter, sTenBaiViet, sMaBaiViet, pageNo, pageSize, keySort, desc,
//					trangThai);
//
//			return new ResponseEntity<BaiVietBDTO>(lstBaiViet, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}

}