package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.BieuMauBaoCao.BcThanhVienDTO;
import com.temp.model.BieuMauBaoCao.BieuMauBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.NguoiDung.QtNguoiDungDao;
import com.temp.service.BieuMauBaoCao.BieuMauBaoCaoService;
import com.temp.service.BieuMauBaoCao.BmBaoCaoService;
import com.temp.service.Common.ThongBaoService;
import com.temp.utils.Constants;
import com.temp.utils.TimestampUtils;
import com.temp.utils.Utils;


@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class BieuMauBaoCaoApiController {

	public static final Logger logger = LoggerFactory.getLogger(BieuMauBaoCaoApiController.class);

	@Autowired
	@Qualifier("BieuMauBaoCaoServiceImpl")
	private BieuMauBaoCaoService qtService;
	
	@Autowired
	@Qualifier("ThongBaoServiceImpl")
	private ThongBaoService thongBaoService;
	
	@Autowired
	private QtNguoiDungDao qtNguoiDungDao;
	
	@Autowired
	@Qualifier("BmBaoCaoServiceImp")
	private BmBaoCaoService bmBaoCaoService;


	@RequestMapping(value = "/bieumaubaocao/getlist", method = RequestMethod.GET)
	public ResponseEntity<?> getlist(@Valid @RequestParam(value = "canCuPhapLy", required = false) String canCuPhapLy,
			@Valid @RequestParam(value = "tenBaoCao", required = false) String tenBaoCao,
			@Valid @RequestParam(value = "strFilter", required = false) String strFilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc) throws ApiRequestException {

		BieuMauBaoCaoBDTO BDTO = new BieuMauBaoCaoBDTO();

		try {

			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			BDTO = qtService.getlist(pageNo, pageSize, desc, strFilter, tenBaoCao, canCuPhapLy);

			return ResponseEntity.ok().body(BDTO);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/bieumaubaocao/getall", method = RequestMethod.GET)
	public ResponseEntity<?> getlistall(@Valid @RequestParam(value = "canCuPhapLy", required = false) String canCuPhapLy,
			@Valid @RequestParam(value = "tenBaoCao", required = false) String tenBaoCao,
			@Valid @RequestParam(value = "strFilter", required = false) String strFilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc) throws ApiRequestException {

		BieuMauBaoCaoBDTO BDTO = new BieuMauBaoCaoBDTO();

		try {

			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			BDTO = qtService.getlist(pageNo, pageSize, desc, strFilter, tenBaoCao, canCuPhapLy);

			return ResponseEntity.ok().body(BDTO);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

//	@PutMapping("/bieumaubaocao/giahan")
//	public ResponseEntity<?> giaHanGuiBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Gia hạn thời gian gửi báo cáo");
//			
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
//					Date dt = oldDTO.getThoiHanGui();
//					Calendar c = Calendar.getInstance();
//					c.setTime(dt);
//					c.add(Calendar.DATE, dto.getPhienBan());
//					dt = c.getTime();
//					Timestamp timeStamp = new Timestamp(dt.getTime());
//					oldDTO.setThoiHanGiaHan(timeStamp);
//					oldDTO.setLyDoGiaHan(dto.getLyDoGiaHan());
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Gia hạn thời gian gửi báo cáo");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						this.addThongBao("UBCK gia hạn thời gian nộp báo cáo ", "UBCK gia hạn thời gian nộp báo cáo ", "thongbaoList", oldDTO);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " gia hạn thời gian gửi báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Gia hạn gửi báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}

	
//	@PutMapping("/bieumaubaocao/khoabaocao")
//	public ResponseEntity<?> khoaBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Khóa báo cáo");
//
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setLyDoGuiLai(dto.getLyDoGuiLai());
//					oldDTO.setKhoaBaoCao(true);
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Khóa báo cáo (" + oldDTO.getLyDoGuiLai() + ")");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						this.addThongBao("UBCK khóa báo cáo ", "UBCK khóa báo cáo ", "thongbaoList", oldDTO);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " khóa báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Khóa báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	
//	@PutMapping("/bieumaubaocao/mokhoabaocao")
//	public ResponseEntity<?> moKhoaBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Mở khóa báo cáo");
//
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setLyDoGuiLai(dto.getLyDoGuiLai());
//					oldDTO.setKhoaBaoCao(false);
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Mở khóa báo cáo (" + oldDTO.getLyDoGuiLai() + ")");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						this.addThongBao("UBCK mở khóa báo cáo ", "UBCK mở khóa báo cáo ", "thongbaoList", oldDTO);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " mở khóa báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Khóa báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	
	//UBCK hủy báo cáo và ko cần duyệt
//	@PutMapping("/bieumaubaocao/yeucauguilai")
//	public ResponseEntity<?> yeuCauGuiLaiBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Yêu cầu gửi lại báo cáo");
//
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setLyDoGuiLai(dto.getLyDoGuiLai());
//					oldDTO.setTrangThai(5);
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Yêu cầu gửi lại báo cáo (" + oldDTO.getLyDoGuiLai() + ")");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//						this.addThongBao("UBCK yêu cầu gửi lại báo cáo ", "UBCK yêu cầu gửi lại báo cáo ", "thongbaoList", oldDTO);
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " yêu cầu gửi lại báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Yêu cầu gửi lại báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	

	//ctck xin ubck đc phép gửi lại báo cáo
//	@PutMapping("/bieumaubaocao/yeucauhuy")
//	public ResponseEntity<?> yeuCauHuyBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Yêu cầu hủy báo cáo");
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setLyDo(dto.getLyDo());
//					oldDTO.setNguoiDungId(userInfo.getId());
//					oldDTO.setHuyBaoCao(2);
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Yêu cầu hủy báo cáo (" + oldDTO.getLyDo() + ")");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//						this.addThongBaoFromCtck(" yêu cầu hủy báo cáo ", " yêu cầu hủy báo cáo ", "thongbaoList", oldDTO);
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " yêu cầu hủy báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Yêu cầu hủy báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	
	//UBCK duyệt yêu cầu xin đc gửi lại của CTCK
//	@PutMapping("/bieumaubaocao/chapnhanhuy")
//	public ResponseEntity<?> chapNhanYeuCauHuyBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Chấp nhận yêu cầu hủy báo cáo");
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
////					oldDTO.setLyDo(dto.getLyDo());
//					oldDTO.setHuyBaoCao(1);
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Chấp nhận yêu cầu hủy báo cáo");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						this.addThongBao("UBCK chấp nhận yêu cầu gửi lại báo cáo ", "UBCK chấp nhận yêu cầu huỷ báo cáo ", "thongbaoList", oldDTO);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " chấp nhận yêu cầu hủy báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						
//						// Send Mail
//						try {
//							ThamSoHeThongDTO mailTieuDe = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_SUBJECT_CANCEL_REPORT);
//							ThamSoHeThongDTO mailNoiDung = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_CONTENT_CANCEL_REPORT);
//							
//							QtNguoiDungDTO ngDung = new QtNguoiDungDTO();
//							ngDung = qtNguoiDungDao.findById(oldDTO.getNguoiDungId());
//							
//							String emailTo = ngDung.getEmail();
//							String tieuDe = mailTieuDe.getGiaTri();
//							String noiDung = mailNoiDung.getGiaTri();
//							SendMail.SendMailTemplate(emailTo, tieuDe, noiDung.replace("{0}", oldDTO.getKyBaoCao()));
//						} catch (Exception e) {
//							return new ResponseEntity<BcThanhVienDTO>(new BcThanhVienDTO(), HttpStatus.ACCEPTED);
//						}
//						
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Chấp nhận hủy báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	
	//UBCK từ chối yêu cầu xin đc gửi lại của 
//	@PutMapping("/bieumaubaocao/tuchoihuy")
//	public ResponseEntity<?> tuChoiYeuCauHuyBaoCao(@Valid @RequestBody BcThanhVienDTO dto,
//		    @Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			//logger.info("Từ chối yêu cầu hủy báo cáo");
//
//			if (dto.getId() > 0) {
//				BcThanhVienDTO oldDTO = bcTvService.findById(dto.getId());
//				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setHuyBaoCao(0);
//					BcThanhVienDTO check = bcTvService.addOrUpdateThanhVienService(oldDTO);
//					if (check != null) {
//						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//						BcThanhVienLsDTO dtoLichSu = new BcThanhVienLsDTO();
//						dtoLichSu.setCtckThongTinId(oldDTO.getCtckThongTinId());
//						dtoLichSu.setBcThanhVienId(oldDTO.getId());
//						dtoLichSu.setBmBaoCaoId(oldDTO.getBmBaoCaoId());
//						dtoLichSu.setBmBaoCaoDinhKyId(oldDTO.getBmBaoCaoDinhKyId());
//						dtoLichSu.setNguoiDungId(userInfo.getId());
//						dtoLichSu.setMoTa("Từ chối yêu cầu hủy báo cáo");
//						dtoLichSu.setTrangThai(oldDTO.getTrangThai().longValue());
//						dtoLichSu.setNgayTao(new Timestamp(System.currentTimeMillis()));
//						bcTvLsService.saveBcThanhVienLs(dtoLichSu);
//						this.addThongBao("UBCK từ chối yêu cầu gửi lại báo cáo ", "UBCK từ chối yêu cầu huỷ báo cáo ", "thongbaoList", oldDTO);
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
//						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " từ chối yêu cầu hủy báo cáo");
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
//					}
//				}
//			}
//			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException("Từ chối hủy báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
//		}
//	}
	
	@PutMapping("/bieumaubaocao/ngungsudung")
	public ResponseEntity<?> ngungSuDung(@Valid @RequestBody BcThanhVienDTO dto,
		    @Context HttpServletRequest request) throws ApiRequestException {
		try {
			//logger.info("Ngừng sử dụng biểu mẫu báo cáo");

			if (dto.getId() > 0) {
				BmBaoCaoDTO oldDTO = bmBaoCaoService.findById(dto.getId());
				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setLyDo(dto.getLyDo());
					oldDTO.setSuDung(0);
					BmBaoCaoDTO check = bmBaoCaoService.addorUpdateBmBaoCao(oldDTO, "Ngừng sử dụng biểu mẫu báo cáo");
					boolean checkNgung = bmBaoCaoService.suDungBaoCao(dto.getId(), 0);
					if (check != null) {
						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

						// set IP thực hiện lấy IP remote client
						dtoLog.setIpThucHien(Utils.getClientIp(request));
						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
						dtoLog.setNguoiTaoId(userInfo.getId());
						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " ngừng sử dụng biểu mẫu báo cáo");
						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
					}
				}
			}
			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException("Ngừng sử dụng biểu mẫu báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
		}
	}
	@PutMapping("/bieumaubaocao/sudung")
	public ResponseEntity<?> suDung(@Valid @RequestBody BcThanhVienDTO dto,
		    @Context HttpServletRequest request) throws ApiRequestException {
		try {
			//logger.info("Sử dụng biểu mẫu báo cáo");

			if (dto.getId() > 0) {
				BmBaoCaoDTO oldDTO = bmBaoCaoService.findById(dto.getId());
				if (oldDTO != null && oldDTO.getId() > 0) {
//					oldDTO.setLyDo(dto.getLyDo());
					oldDTO.setSuDung(1);
					BmBaoCaoDTO check = bmBaoCaoService.addorUpdateBmBaoCao(oldDTO, "Sử dụng lại biểu mẫu báo cáo");
					boolean checkNgung = bmBaoCaoService.suDungBaoCao(dto.getId(), 1);
					if (check != null && checkNgung) {
						QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

						// set IP thực hiện lấy IP remote client
						dtoLog.setIpThucHien(Utils.getClientIp(request));
						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
						dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
						dtoLog.setNguoiTaoId(userInfo.getId());
						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " sử dụng lại biểu mẫu báo cáo");
						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
						return new ResponseEntity<BcThanhVienDTO>(dto, HttpStatus.ACCEPTED);
					}
				}
			}
			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException("Sử dụng biểu mẫu báo cáo không thành công", HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	//addThongBao den thanh vien ctck UBCK tuong tac bao cao
	private boolean addThongBao(String tieuDe, String noiDung, String linkHref, BcThanhVienDTO bctv) {
		String result = "";

		BmBaoCaoDTO tempBmBc = new BmBaoCaoDTO();;
		try {
			tempBmBc = bmBaoCaoService.findById(bctv.getBmBaoCaoId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(tempBmBc != null && tempBmBc.getTenBaoCao() != null) {
			result += tempBmBc.getTenBaoCao();
		}
		if(bctv != null) {
			if(bctv.getKyBaoCao() != null) {
				result += " kỳ ";
				switch(bctv.getKyBaoCao()) {
					case "N":
						result += "ngày ";
						break;
					case "TU":
						result += "tuần ";
						break;
					case "TH":
						result += "tháng ";
						break;
					case "Q":
						result += "quý ";
						break;
					case "BN":
						result += "bán niên ";
						break;
					default:
						result += "năm ";
						break;
				
				}
			}
			if(bctv.getGiaTriKyBc() != null) {
				result += bctv.getGiaTriKyBc() + " ";
			}
			if(bctv.getNamCanhBao() != null) {
		    	result += "năm " + bctv.getNamCanhBao();
		    }
			else if(bctv.getNgayTao() != null) {
				if(bctv.getKyBaoCao() != null && (bctv.getKyBaoCao().equals("NAM") || bctv.getKyBaoCao().equals("BN"))) {
					String temp = TimestampUtils.TimestampToString_ddMMyyyy(bctv.getNgayTao());
					if(temp.length() > 4 && temp.length() < 11) {
						result += "năm " + temp.substring(temp.length() - 4);
					}
				}
			}
			
			tieuDe = tieuDe + result;
			noiDung = noiDung + result;
			Set<Integer> nguoiNhan = new HashSet<Integer>();
			QtNguoiDungBDTO tempBDTO = new QtNguoiDungBDTO();
			List<NguoiDungJoinAllDTO> temp = new ArrayList<NguoiDungJoinAllDTO>();
			tempBDTO = qtNguoiDungDao.listAllNguoiDung("id", true, "1");
			if(tempBDTO != null && bctv.getCtckThongTinId() != null) {
				temp = tempBDTO.getLstQtNguoiDung().stream().filter(x -> !x.getAdmin() && x.getTrangThai() == true).collect(Collectors.toList());
//				(oldDTO.getCtckThongTinId()
			}
			if(temp.size() > 0) {
				for(NguoiDungJoinAllDTO tempDto : temp) nguoiNhan.add(tempDto.getId());
			}
//			if(nguoiNhan.size() > 0) {
//				QtNguoiDungDTO user = UserInfoGlobal.getUserInfoAuthor();
//				return thongBaoService.addThongBao(tieuDe, noiDung, linkHref, nguoiNhan, user != null ? user.getId() : null);
//			}
			
		}
		
		return false;
	}
	
//	private boolean addThongBaoFromCtck(String tieuDe, String noiDung, String linkHref, BcThanhVienDTO bctv) {
//		String result = "";
//		List<LkNguoiDungCtckDTO> tempLk = new ArrayList<LkNguoiDungCtckDTO>();
//		CtckThongTinDTO tempCtck = new CtckThongTinDTO();
//		BmBaoCaoDTO tempBmBc = new BmBaoCaoDTO();
//		try {
//			if(bctv != null) {
//				if(bctv.getCtckThongTinId() != null) {
//					tempCtck = ctckttService.getById(bctv.getCtckThongTinId());
//					tempLk = lkNdCtckDao.getLstNguoiDungFromCtck(bctv.getCtckThongTinId());
//				}
//				if(bctv.getBmBaoCaoId() != null) {
//					tempBmBc = bmBaoCaoService.findById(bctv.getBmBaoCaoId());
//				}
//			}
//			
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		if(tempCtck != null && tempCtck.getTenTiengViet() != null) {
//			tieuDe = tempCtck.getTenTiengViet() + tieuDe;
//			noiDung = tempCtck.getTenTiengViet() + noiDung;
//		}
//		if(tempBmBc != null && tempBmBc.getTenBaoCao() != null) {
//			result += tempBmBc.getTenBaoCao();
//		}
//		if(bctv != null) {
//			if(bctv.getKyBaoCao() != null) {
//				result += " kỳ ";
//				switch(bctv.getKyBaoCao()) {
//					case "N":
//						result += "ngày ";
//						break;
//					case "TU":
//						result += "tuần ";
//						break;
//					case "TH":
//						result += "tháng ";
//						break;
//					case "Q":
//						result += "quý ";
//						break;
//					case "BN":
//						result += "bán niên ";
//						break;
//					default:
//						result += "năm ";
//						break;
//				
//				}
//			}
//			if(bctv.getGiaTriKyBc() != null) {
//				result += bctv.getGiaTriKyBc() + " ";
//			}
//			if(bctv.getNamCanhBao() != null) {
//		    	result += "năm " + bctv.getNamCanhBao();
//		    }
//			else if(bctv.getNgayTao() != null) {
//				if(bctv.getKyBaoCao() != null && (bctv.getKyBaoCao().equals("NAM") || bctv.getKyBaoCao().equals("BN"))) {
//					String temp = TimestampUtils.TimestampToString_ddMMyyyy(bctv.getNgayTao());
//					if(temp.length() > 4 && temp.length() < 11) {
//						result += "năm " + temp.substring(temp.length() - 4);
//					}
//				}
//			}
//			
//			tieuDe = tieuDe + result;
//			noiDung = noiDung + result;
//			Set<Integer> nguoiNhan = new HashSet<Integer>();
//			if(tempLk != null && tempLk.size() > 0) {
//				for(LkNguoiDungCtckDTO tempDto : tempLk) nguoiNhan.add(tempDto.getNguoiDungId());
//			}
////			if(nguoiNhan.size() > 0) {
////				QtNguoiDungDTO user = UserInfoGlobal.getUserInfoAuthor();
////				return thongBaoService.addThongBao(tieuDe, noiDung, linkHref, nguoiNhan, user != null ? user.getId() : null);
////			}
//		}
//		
//		return false;
//	}
	
	 
}