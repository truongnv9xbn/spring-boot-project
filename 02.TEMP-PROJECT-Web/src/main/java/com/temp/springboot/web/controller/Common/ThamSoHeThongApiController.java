package com.temp.springboot.web.controller.Common;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.Common.ThamSoHeThongBDTO;
import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.Common.ThamSoHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class ThamSoHeThongApiController {

	public static final Logger logger = LoggerFactory.getLogger(ThamSoHeThongApiController.class);

	@Autowired
	@Qualifier("ThamSoHeThongServiceImpl")
	private ThamSoHeThongService qtService;

	@RequestMapping(value = "/thamsohethong/filter", method = RequestMethod.GET)
	public ResponseEntity<ThamSoHeThongBDTO> listThamSoHeThongs(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sPhanHe", required = false) String sPhanHe,
			@RequestParam(value = "sThamSo", required = false) String sThamSo,
			@RequestParam(value = "sGiaTri", required = false) String sGiaTri,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			ThamSoHeThongBDTO bdto = new ThamSoHeThongBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<ThamSoHeThongBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		ThamSoHeThongBDTO lstThamSoHeThong = new ThamSoHeThongBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			lstThamSoHeThong = qtService.listThamSoHeThongs(strfilter, sPhanHe, sThamSo, sGiaTri, pageNo, pageSize,
					keySort, desc, trangThai);

			return new ResponseEntity<ThamSoHeThongBDTO>(lstThamSoHeThong, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<ThamSoHeThongBDTO>(lstThamSoHeThong, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * cập nhập
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/thamsohethong/capnhat")
	public ResponseEntity<?> updateThamSoHeThong(@Valid @RequestBody ThamSoHeThongDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if (qtService.isExistById(dto.getId())) {
				qtService.updateThamSoHeThong(dto);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_THAM_SO_HE_THONG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật tham số hệ thống");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<ThamSoHeThongDTO>(dto, HttpStatus.ACCEPTED);

			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@GetMapping("/thamsohethong/chitiet/{id}")
	public ResponseEntity<?> getByIdThamSoHeThong(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			ThamSoHeThongDTO qt = qtService.findById(id);
			if (qt == null) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/thamsohethong/chitietbygiatrithamso/{thamso}")
	public ResponseEntity<?> getByThamSo(@PathVariable("thamso") String thamSo) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			ThamSoHeThongDTO qt = qtService.findByThamSo(thamSo);
			if (qt == null) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = ThamSoHeThongDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}