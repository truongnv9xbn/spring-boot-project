package com.temp.springboot.web.controller;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.ThongBaoBDTO;
import com.temp.model.ThongBaoDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.Common.ThongBaoService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class ThongBaoApiController {

	public static final Logger logger = LoggerFactory.getLogger(ThongBaoApiController.class);

	@Autowired
	@Qualifier("ThongBaoServiceImpl")
	private ThongBaoService tbService;

	@RequestMapping(value = "/thongbao/filter", method = RequestMethod.GET)
	public ResponseEntity<ThongBaoBDTO> listThongBaos(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "list") String list) throws ApiRequestException {

		ThongBaoBDTO lstThongBao = new ThongBaoBDTO();

		try {
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			logger.info(Constants.Logs.LIST);
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			lstThongBao = tbService.listThongBao(pageNo, pageSize, strfilter, userInfo.getId(), true, 1, list);

			return new ResponseEntity<ThongBaoBDTO>(lstThongBao, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

//	@RequestMapping(value = "/ThongBao/getall", method = RequestMethod.GET)
//	public ResponseEntity<ThongBaoBDTO> listAlls(
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "trangThai", required = false) String trangThai) {
//
//		if (!checkKeySort(keySort)) {
//
//			ThongBaoBDTO bdto = new ThongBaoBDTO();
//			bdto.setMessage("truyền sai keySort");
//
//			return new ResponseEntity<ThongBaoBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
//		}
//
//		ThongBaoBDTO lst = new ThongBaoBDTO();
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			lst = cvService.listAll(keySort, desc, trangThai);
//
//			return new ResponseEntity<ThongBaoBDTO>(lst, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<ThongBaoBDTO>(lst, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

	/**
	 * Create ThongBao
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
//	@PostMapping("/ThongBao/themmoi")
//	public ResponseEntity<?> createThongBao(@Valid @RequestBody ThongBaoDTO dto,
//			@Context HttpServletRequest request) throws ApiRequestException {
//		logger.info(Constants.Logs.CREATE);
//
//		if (!cvService.isExistThongBaoAdd(dto)) {
//			// lấy thông tin user
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//			dto.setNguoiTaoId(userInfo.getId());
//			try {
//				cvService.saveThongBao(dto);
//
//				Timestamp timeStamp = new Timestamp(new Date().getTime());
//				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//				// set IP thực hiện lấy IP remote client
//				dtoLog.setIpThucHien(Utils.getClientIp(request));
//				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_ThongBao");
//				dtoLog.setNgayTao(timeStamp);
//				dtoLog.setNguoiTaoId(userInfo.getId());
//				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới trạng thái ctck");
//				// save db
//				cvServiceLog.AddLogHeThong(dtoLog);
//				return new ResponseEntity<ThongBaoDTO>(dto, HttpStatus.CREATED);
//
//			} catch (Exception e) {
//				logger.error(e.getMessage());
//
//				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
//
//			}
//
//		}
//		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
//
//	}

	@SuppressWarnings("unused")
	@PutMapping("/thongbao/capnhat")
	public ResponseEntity<?> updateThongBao(@RequestBody ThongBaoDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if (tbService.isExistById(dto.getId())) {
				if (tbService.updateSeen(dto.getId())) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_THONG_BAO");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật thông báo");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				}
				return new ResponseEntity<ThongBaoDTO>(dto, HttpStatus.ACCEPTED);

			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@PutMapping("/thongbao/seenAll")
	public ResponseEntity<?> seenAllThongBao(@RequestBody String test, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			if (userInfo != null && !test.isEmpty()) {
				ThongBaoBDTO list = tbService.listThongBao(0, 0, "", userInfo.id, true, 1, "");
				List<Integer> lstThongBaoId = new ArrayList<Integer>();
				if (list.lstThongBao != null && list.lstThongBao.size() > 0) {
					list.lstThongBao = list.lstThongBao.stream()
							.filter(f -> f.getListNguoiDaXem() != null && f.getListNguoiDaXem().length() > 0
									&& !f.getListNguoiDaXem().contains(";" + userInfo.getId().toString() + ";"))
							.collect(Collectors.toList());
					for (ThongBaoDTO dto1 : list.lstThongBao) {
						lstThongBaoId.add(dto1.getId());
					}
				}

				if (tbService.seenAll(lstThongBaoId, userInfo.getId())) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_THONG_BAO");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " gửi thông báo");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				}

				return new ResponseEntity<Object>(null, HttpStatus.ACCEPTED);

			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

//	@DeleteMapping("/ThongBao/xoa/{id}")
//	public ResponseEntity<?> deleteThongBao(@PathVariable("id") int id, @Context HttpServletRequest request)
//			throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.DELETE);
//
//			if (!cvService.isExistById(id)) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//
//			}
//			if (cvService.deleteById(id)) {
//				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//				Timestamp timeStamp = new Timestamp(new Date().getTime());
//				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//				// set IP thực hiện lấy IP remote client
//				dtoLog.setIpThucHien(Utils.getClientIp(request));
//				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUCVU");
//				dtoLog.setNgayTao(timeStamp);
//				dtoLog.setNguoiTaoId(userInfo.getId());
//				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa quốc tịch");
//				// save db
//				cvServiceLog.AddLogHeThong(dtoLog);
//				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
//			}
//			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}

//	@GetMapping("/ThongBao/chitiet/{id}")
//	public ResponseEntity<?> getByIdThongBao(@PathVariable("id") int id) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			ThongBaoDTO qt = cvService.findById(id);
//			if (qt == null) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
//			}
//			return ResponseEntity.ok().body(qt);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}

	@SuppressWarnings("unused")
	private boolean checkKeySort(String keySort) {

		Field fld[] = ThongBaoDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}