package com.temp.springboot.web.controller.NguoiDung;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNDIpJoinNguoiDungDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpBDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpDTO;
import com.temp.service.NguoiDung.QtNguoiDungIpService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class NguoiDungIpApiController {

	public static final Logger logger = LoggerFactory.getLogger(NguoiDungIpApiController.class);

	@Autowired
	@Qualifier("QtNguoiDungIpServiceImpl")
	private QtNguoiDungIpService qtService;

	@RequestMapping(value = "/qtnguoidungip/filter", method = RequestMethod.GET)
	public ResponseEntity<QtNguoiDungIpBDTO> listQtNguoiDungIps(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "nguoiDungId", required = false) Integer nguoiDungId,
			@RequestParam(value = "sIp", required = false) String ip,
			@RequestParam(value = "hoatDong", required = false) String hoatDong) {

		if (!checkKeySort(keySort)) {

			QtNguoiDungIpBDTO bdto = new QtNguoiDungIpBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QtNguoiDungIpBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QtNguoiDungIpBDTO lstQtNguoiDungIp = new QtNguoiDungIpBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			lstQtNguoiDungIp = qtService.listQtNguoiDungIps(strfilter, nguoiDungId, ip, pageNo, pageSize, keySort, desc,
					hoatDong);

			return new ResponseEntity<QtNguoiDungIpBDTO>(lstQtNguoiDungIp, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QtNguoiDungIpBDTO>(lstQtNguoiDungIp, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Create
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/qtnguoidungip/themmoi")
	public ResponseEntity<?> createQtNguoiDungIp(@Valid @RequestBody QtNguoiDungIpDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.CREATE);

			if (!qtService.isExistQtNguoiDungIp(dto)) {
				if (qtService.saveQtNguoiDungIp(dto)) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_NGUOI_DUNG_IP");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới IP người dùng");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				}

				return new ResponseEntity<QtNguoiDungIpDTO>(dto, HttpStatus.CREATED);
			}

			return new ResponseEntity<Object>(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

		} catch (Exception e) {
			logger.error(e.getMessage());

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * cập nhập người dùng ip
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/qtnguoidungip/capnhat")
	public ResponseEntity<?> updateQtNguoiDungIp(@Valid @RequestBody QtNguoiDungIpDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.UPDATE);

			if (qtService.isExistById(dto.getId())) {
				if (qtService.updateQtNguoiDungIp(dto)) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_NGUOI_DUNG_IP");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật IP người dùng");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				}
				return new ResponseEntity<QtNguoiDungIpDTO>(dto, HttpStatus.ACCEPTED);

			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@GetMapping("/qtnguoidungip/chitiet/{id}")
	public ResponseEntity<?> getByIdNguoiDungIp(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			QtNDIpJoinNguoiDungDTO qt = qtService.findByJoinNDId(id);
			if (qt == null) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * xóa 1 người dùng ip
	 * 
	 * @param id của người dùng ip
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/qtnguoidungip/xoa/{id}")
	public ResponseEntity<?> deleteQtNguoiDungIp(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.DELETE);

			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_NGUOI_DUNG_IP");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa IP người dùng id = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = QtNguoiDungIpDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}