package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotMenuBDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.BieuMauBaoCao.SheetCotService;
import com.temp.utils.CellConfig;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class SheetCotApiController {

	public static final Logger logger = LoggerFactory.getLogger(SheetCotApiController.class);

	@Autowired
	@Qualifier("SheetCotServiceImpl")
	private SheetCotService sheetCotService;

	/**
	 * Create sheet
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/cotsheet/themmoi")
	public ResponseEntity<?> createSheet(@Valid @RequestBody BmSheetCotDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		//logger.info(Constants.Logs.CREATE);

		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

		try {

			sheetCotService.AddOrUpdateCotSheet(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_SHEET_COT");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới cột sheet");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmSheetCotDTO>(dto, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * cập nhập sheet
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/cotsheet/capnhat")
	public ResponseEntity<?> updateSheet(@Valid @RequestBody BmSheetCotDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		//logger.info(Constants.Logs.UPDATE);

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		try {

			sheetCotService.AddOrUpdateCotSheet(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_SHEET_COT");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật cột sheet ");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmSheetCotDTO>(dto, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@RequestMapping(value = "/cotsheet/mausaclist", method = RequestMethod.GET)
	public ResponseEntity<?> getColorList() throws ApiRequestException {
		// //System.out.println(CellConfig.list);
		return ResponseEntity.ok().body(CellConfig.list);

	}

	@GetMapping("/cotsheet/chitiet/{id}")
	public ResponseEntity<?> getByIdSheet(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			BmSheetCotDTO qt = sheetCotService.FindById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	@GetMapping("/cotsheet/checkexitsmacot")
	public ResponseEntity<?> getByIdChucVu(
		 @RequestParam(name = "macot", defaultValue = "") String macot,
			@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId
			) throws ApiRequestException {
		try {

			if (sheetCotService.checkExistMaCotSheet(macot,sheetId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	// param
	// truyền
	// lên
	@GetMapping("/cotsheet/checkexitsmacotupdate")
	public ResponseEntity<?> getBychecksheetUpdate(
			@RequestParam(name = "macot", defaultValue = "") String macot,
			@RequestParam(name = "id", defaultValue = "0") Integer id,
			@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId
			) throws ApiRequestException {
		try {

			if (sheetCotService.checkExistMaCotSheetUpdate(macot, id,sheetId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	// param
	// truyền
	// lên
	@GetMapping("/cotsheet/checkexitsmacotlienthong")
	public ResponseEntity<?> getByCheckMaCotLT(
			@RequestParam(name = "maCotLT", defaultValue = "") String maCotLT,
			@RequestParam(name = "id", defaultValue = "0") Integer id,
			@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId
			) throws ApiRequestException {
		try {

			if (sheetCotService.checkExistMaCotLT(maCotLT, id,sheetId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * lấy danh sách các cột được thiết lập trong sheet
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/cotsheet/dscotcauhinh", method = RequestMethod.GET)
	public ResponseEntity<BmSheetCotMenuBDTO> listCotSheet(
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(value = "sheetId", required = false) Integer sheetId
			) throws ApiRequestException {

		BmSheetCotMenuBDTO lst = null;

		try {

			Integer pageNoReqest = 0;
			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNoReqest = pageNo - 1;
			}
			lst = sheetCotService.List(sheetId,pageNoReqest,pageSize);

			return new ResponseEntity<BmSheetCotMenuBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * lấy danh sách các cột được thiết lập trong sheet làm dropdown
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/cotsheet/dscotcongthuc", method = RequestMethod.GET)
	public ResponseEntity<List<DropDownDTO>> listCotSheetCongThuc(
			@RequestParam(value = "sheetId", required = false) Integer sheetId) throws ApiRequestException {

		List<DropDownDTO> lst = null;

		try {

			//logger.info(Constants.Logs.LIST);

			lst = sheetCotService.listCotDropdown(sheetId);

			return new ResponseEntity<List<DropDownDTO>>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@DeleteMapping("/cotsheet/xoa/{id}") 
	public ResponseEntity<?> deleteCot(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.DELETE);
			
			if (sheetCotService.deleteColumn(id)) { // xoastheo id
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_SHEET_COT");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa cột sheet");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);

			}
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/cotsheet/maxstt/{id}")
	public ResponseEntity<?> getMaxSTTBySheetID(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			long qt = sheetCotService.maxSTTBySheetId(id);
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	@SuppressWarnings("unused")
	private boolean checkKeySort(String keySort) {

//		Field fld[] = ChucVuDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

//		for (int i = 0; i < fld.length; i++) {
//			listName.add(fld[i].getName());
//		}

		return listName.contains(keySort);
	}

}

//------------------- Delete All Users-----------------------------

//	@RequestMapping(value = "/chucvu/", method = RequestMethod.DELETE)
//	@DeleteMapping("/user/")
//	public ResponseEntity<UserBDTO> deleteAllUsers() {
//		//logger.info("Deleting All Users");
//		try {
//			userService.deleteAllUsers();
//			return new ResponseEntity<UserBDTO>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity(new ApiRequestException("Unable to delete."), HttpStatus.NOT_FOUND);
//		}
//
//	}
