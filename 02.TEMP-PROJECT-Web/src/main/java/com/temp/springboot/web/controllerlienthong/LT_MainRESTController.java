//package com.temp.springboot.web.controllerlienthong;
//
//import java.text.ParseException;
//import java.util.Date;
//import java.util.Map;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
////import com.example.DTO_LienThong.BaoCaoParamsDTO;
//import com.example.DTO_LienThong.NguoiHanhNgheXmlBDTO;
////import com.example.DTO_LienThong.ObjectBaoCaoXMLDTO;
//
//import com.temp.global.ThamSoHeThongGlobal;
//import com.temp.global.exception.ApiRequestException;
//import com.temp.lienthongService.SinhDuLieuService;
//import com.temp.model.ChucVuBDTO;
//import com.temp.model.ThamSoHeThongDTO;
//import com.temp.utils.DateTimeStringToParseUtils;
//import com.temp.utils.UtilsXml;
//
//
//@RestController
//@RequestMapping("/lien-thong")
//public class LT_MainRESTController {
//	
//	@Autowired
//	@Qualifier("SinhDuLieuServiceImpl")
//	private SinhDuLieuService qtService;
//
//	@RequestMapping(value = "/ntfbaocao/{pKyBaoCao}/{pLoaiBaoCao}/{pMaSoThue}",produces = { MediaType.APPLICATION_XML_VALUE})
//	@ResponseBody
//	public String sendNotificationBaoCao(@PathVariable("pKyBaoCao") String pKyBaoCao,
//			@PathVariable("pLoaiBaoCao") String pLoaiBaoCao,
//			@PathVariable("pMaSoThue") String pMaSoThue){
//		//System.out.println("return----> sendNotificationBaoCao");
//		return qtService.sendNotificationBaoCao(pKyBaoCao, pLoaiBaoCao,pMaSoThue);
////		return UtilsXml.jaxbObjectToXML(qtService.sendNotificationBaoCao(pKyBaoCao, pLoaiBaoCao,pMaSoThue));
////		return new ResponseEntity<ObjectBaoCaoXMLDTO>(baoCaoDTO, HttpStatus.OK);
//	}
//	
////	@RequestMapping(value = "/getreport", method = RequestMethod.POST,produces = { MediaType.APPLICATION_XML_VALUE} )//,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
////	public String getBaoCao(@RequestBody BaoCaoParamsDTO  params) throws ApiRequestException, ParseException  {
////		//System.out.println("return----> checkNHNCK"+i++ +" "+ new Date().toString());
//////		i=i++;
//////		String pSoChungChi = model.get("soChungChi").toString();
//////		String api = "";
//////		ThamSoHeThongDTO thamso  = ThamSoHeThongGlobal.giaTriThamSo("API_NHN");
//////		if(thamso!=null) {
//////			api = thamso.getGiaTri();
//////		}
//////		return UtilsXml.jaxbObjectToXML(qtService.isExitsCrudNguoiHanhNghe(pSoChungChi,api));
////		Boolean isValid = true;
////		if(params == null) {
////			throw new ApiRequestException("Lỗi, vui lòng liên hệ với Administrator!!!");
////		}
////		else {
////			if (params.getMaBaoCao() == null) {
////				isValid = false;
////				throw new ApiRequestException("Mã báo cáo phải nhập");
////
////			}
////			if(params.getMaCongTy() == null || (params.getMaCongTy() != null && params.getMaCongTy().intValue() <= 0)) {
////				isValid = false;
////				throw new ApiRequestException("Mã công ty sai");
////			}
////			if(!params.getTuNgay().equals("")) {
////				Date check = DateTimeStringToParseUtils.StringPaserDateUtils_ddMMyyyy(params.getTuNgay());
////				if(check == null || (check != null && check.getYear() <= 0)) {
////					isValid = false;
////					throw new ApiRequestException("Từ ngày sai định dạng! Định dạng kiểu dd/MM/yyyy");
////				}
////			}
////			if(!params.getDenNgay().equals("")) {
////				Date check = DateTimeStringToParseUtils.StringPaserDateUtils_ddMMyyyy(params.getDenNgay());
////				if(check == null || (check != null && check.getYear() <= 0)) {
////					isValid = false;
////					throw new ApiRequestException("Đến ngày sai định dạng! Định dạng kiểu dd/MM/yyyy");
////				}
////			}
////			if(isValid) {
////				BaoCaoParamsDTO result = qtService.getReport(params);
////				
////				if(result == null) {
////					throw new ApiRequestException("Lỗi, vui lòng liên hệ với Administrator!!!");
////				}
////				if(result.getErrorMessage() != null && !result.getErrorMessage().equals("")) {
////					throw new ApiRequestException( result.getErrorMessage());
////				}
////				else {
////					return result.getXmlResult();
////				}
////			}
////			else {
////				throw new ApiRequestException("Lỗi, vui lòng liên hệ với Administrator!!!");
////			}
////		}
////		
////	}
//	
//	@RequestMapping(value = "/ntfcongty", method = RequestMethod.GET,produces = { MediaType.APPLICATION_XML_VALUE} )
//	@ResponseBody
//	public String sendNotificationCongTy(){
//		//System.out.println("return----> sendNotificationCongTy");
//		return UtilsXml.jaxbObjectToXML(qtService.sendNotificationCongTy()) ;
//	}
//	int i = 0;
//	@RequestMapping(value = "/nhnck", method = RequestMethod.POST,produces = { MediaType.APPLICATION_XML_VALUE} )//,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
//	public String checkNHNCK(@RequestBody Map<String,String>  model)  {
//		//System.out.println("return----> checkNHNCK"+i++ +" "+ new Date().toString());
//		i=i++;
//		String pSoChungChi = model.get("soChungChi").toString();
//		String api = "";
//		ThamSoHeThongDTO thamso  = ThamSoHeThongGlobal.giaTriThamSo("API_NHN");
//		if(thamso!=null) {
//			api = thamso.getGiaTri();
//		}
//		return UtilsXml.jaxbObjectToXML(qtService.isExitsCrudNguoiHanhNghe(pSoChungChi,api));
//	}
//	
//	@RequestMapping(value = "/syncnhn", method = RequestMethod.POST,produces = { MediaType.APPLICATION_XML_VALUE} )//,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
//	@ResponseBody
//	public String SyncNHN(@RequestBody NguoiHanhNgheXmlBDTO  bdto) {
//		return UtilsXml.jaxbObjectToXML(qtService.SyncNHN(bdto));
//	}
//}
