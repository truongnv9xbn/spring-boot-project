package com.temp.springboot.web.controller;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.MessageBDTO;
import com.temp.model.MessageDTO;
import com.temp.model.NguoiDung.QtNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.MessageService;
import com.temp.service.NguoiDung.QtNguoiDungService;
import com.temp.utils.Constants;
import com.temp.utils.LocalDateTimeUtils;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class MessageApiController {
	public static final Logger logger = LoggerFactory.getLogger(MessageApiController.class);

	@Autowired
	@Qualifier("MessageServiceImpl")
	private MessageService service;

	@Autowired
	@Qualifier("QtNguoiDungServiceImpl")
	private QtNguoiDungService qtService;

	@RequestMapping(value = "/message/filter", method = RequestMethod.GET)
	public ResponseEntity<MessageBDTO> listMessages(@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		MessageBDTO listMessages = new MessageBDTO();
		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();

		lstQtNguoiDung = qtService.listQtNguoiDungs("", null, "", "", "", "", pageNo, pageSize, keySort, desc,
				trangThai, null, null);

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			if (trangThai != null && trangThai.trim().toLowerCase() == "sử dụng") {
				trangThai = "1";
			} else if (trangThai != null && trangThai.trim().toLowerCase() == "không sử dụng") {
				trangThai = "0";
			}

			listMessages = service.filter(pageNo, pageSize, keySort, desc, trangThai);

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			LocalDateTime now = LocalDateTime.now();
			Date date1 = sdf.parse(now.toString());

			if (listMessages.lstMessage != null && listMessages.lstMessage.size() > 0) {
				for (int i = 0; i < listMessages.lstMessage.size(); i++) {
					Date date2 = sdf.parse(listMessages.lstMessage.get(i).getNgayTao().toString());
					if (date1.compareTo(date2) == 0) {
						listMessages.lstMessage.get(i).setNgayTaoStr(LocalDateTimeUtils.convertTimestampToStringHHmmss(
								listMessages.lstMessage.get(i).getNgayTao()) + " Hôm nay");
					} else {
						listMessages.lstMessage.get(i).setNgayTaoStr(LocalDateTimeUtils
								.convertTimestampToStringddMMyyyyHHmmss(listMessages.lstMessage.get(i).getNgayTao()));
					}
					// set avatar
					if (lstQtNguoiDung != null && lstQtNguoiDung.lstQtNguoiDung != null
							&& lstQtNguoiDung.lstQtNguoiDung.size() > 0) {
						for (int a = 0; a < lstQtNguoiDung.lstQtNguoiDung.size(); a++) {
							if (lstQtNguoiDung.lstQtNguoiDung.get(a).getAnhDaiDien() != null
									&& lstQtNguoiDung.lstQtNguoiDung.get(a).getAnhDaiDien() != "") {
								if ((listMessages.lstMessage.get(i).getNguoiGuiId() != null) && listMessages.lstMessage
										.get(i).getNguoiGuiId() == lstQtNguoiDung.lstQtNguoiDung.get(a).getId()) {
									listMessages.lstMessage.get(i)
											.setAvatarNguoiGui(lstQtNguoiDung.lstQtNguoiDung.get(a).getAnhDaiDien());
								}
								if ((listMessages.lstMessage.get(i).getNguoiNhanId() != null) && listMessages.lstMessage
										.get(i).getNguoiNhanId() == lstQtNguoiDung.lstQtNguoiDung.get(a).getId()) {
									listMessages.lstMessage.get(i)
											.setAvatarNguoiNhan(lstQtNguoiDung.lstQtNguoiDung.get(a).getAnhDaiDien());
								}
							}
						}
					}
				}
			}
			return new ResponseEntity<MessageBDTO>(listMessages, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PostMapping("/message/create")
	public ResponseEntity<?> create(@Valid @RequestBody MessageDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		logger.info(Constants.Logs.CREATE);
		// lấy thông tin user
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		dto.setNguoiGuiId(userInfo.getId());
		dto.setNgayTao(timeStamp);
		dto.setTrangThai(true);
		dto.setTaiKhoan(userInfo.getTaiKhoan());
		try {
			service.save(dto);
			return new ResponseEntity<MessageDTO>(dto, HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

//	@RequestMapping(value = "/chucvu/exitsMa", method = RequestMethod.GET)
//	public ResponseEntity<Boolean> checkExitsByMa(
//			@RequestParam(value = "maChucVu", required = false) String maChucVu,
//			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
//		Boolean result=false;
//
//		try {
//			logger.info(Constants.Logs.EXITS);
//			
//			result = cvService.isExistByMa(maChucVu,id);
//
//			return new ResponseEntity<Boolean>(result, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}
//
//	@RequestMapping(value = "/chucvu/getall", method = RequestMethod.GET)
//	public ResponseEntity<ChucVuBDTO> listAllChucVus(
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "trangThai", required = false) String trangThai) {
//
//		if (!checkKeySort(keySort)) {
//
//			ChucVuBDTO bdto = new ChucVuBDTO();
//			bdto.setMessage("truyền sai keySort");
//
//			return new ResponseEntity<ChucVuBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
//		}
//
//		ChucVuBDTO lstChucVu = new ChucVuBDTO();
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			lstChucVu = cvService.listAllChucVu(keySort, desc, trangThai);
//
//			return new ResponseEntity<ChucVuBDTO>(lstChucVu, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<ChucVuBDTO>(lstChucVu, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	/**
//	 * Create chucvu
//	 * 
//	 * @param qt json object
//	 * @return ResponseEntity
//	 * @throws ApiRequestException
//	 */

//
//	/**
//	 * cập nhập chức vụ
//	 * 
//	 * @param qtBdto 1 đối tượng chứa id> 0
//	 * @return ResponseEntity
//	 * @throws ApiRequestException
//	 */
//	@PutMapping("/chucvu/capnhat")
//	public ResponseEntity<?> updateChucVu(@Valid @RequestBody ChucVuDTO dto, @Context HttpServletRequest request)
//			throws ApiRequestException {
//		logger.info(Constants.Logs.UPDATE);
//
//		if (cvService.isExistById(dto.getId())) {
//			if (!cvService.isExistChucVuUpdate(dto)) {
//				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//				try {
//					cvService.updateChucVu(dto);
//
//					Timestamp timeStamp = new Timestamp(new Date().getTime());
//					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//					// set IP thực hiện lấy IP remote client
//					dtoLog.setIpThucHien(Utils.getClientIp(request));
//					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_ChucVu");
//					dtoLog.setNgayTao(timeStamp);
//					dtoLog.setNguoiTaoId(userInfo.getId());
//					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật chức vụ");
//					// save db
//					cvServiceLog.AddLogHeThong(dtoLog);
//
//					return new ResponseEntity<ChucVuDTO>(dto, HttpStatus.ACCEPTED);
//				} catch (Exception e) {
//					logger.error(e.getMessage());
//					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);
//
//				}
//
//			}
//			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
//		}
//		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//	}
//
//	/**
//	 * xóa 1 chức vụ
//	 * 
//	 * @param id của chức vụ
//	 * @return ResponseEntity
//	 * @throws ApiRequestException
//	 */
//	@DeleteMapping("/chucvu/xoa/{id}")
//	public ResponseEntity<?> deleteChucVu(@PathVariable("id") int id, @Context HttpServletRequest request)
//			throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.DELETE);
//
//			if (!cvService.isExistById(id)) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
////				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
////						HttpStatus.NOT_FOUND);
//			}
//			if (cvService.deleteById(id)) {
//				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//				Timestamp timeStamp = new Timestamp(new Date().getTime());
//				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//
//				// set IP thực hiện lấy IP remote client
//				dtoLog.setIpThucHien(Utils.getClientIp(request));
//				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUCVU");
//				dtoLog.setNgayTao(timeStamp);
//				dtoLog.setNguoiTaoId(userInfo.getId());
//				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa chức vụ ");
//				// save db
//				cvServiceLog.AddLogHeThong(dtoLog);
//				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
//			}
//			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}
//
//	@GetMapping("/chucvu/chitiet/{id}")
//	public ResponseEntity<?> getByIdChucVu(@PathVariable("id") int id) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			ChucVuDTO qt = cvService.findById(id);
//			if (qt == null) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
////				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
////						HttpStatus.BAD_REQUEST);
//			}
//			return ResponseEntity.ok().body(qt);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = MessageDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}
