package com.temp.springboot.web.controller.DanhMuc;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DanhMuc.TinhThanhBDTO;
import com.temp.model.DanhMuc.TinhThanhDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.DanhMuc.TinhThanhService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class TinhThanhApiController {

	public static final Logger logger = LoggerFactory.getLogger(TinhThanhApiController.class);
	@Autowired
	@Qualifier("TinhThanhServiceImpl")
	private TinhThanhService cvService;

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService cvServiceLog;

	@RequestMapping(value = "/tinhthanh/filter", method = RequestMethod.GET)
	public ResponseEntity<TinhThanhBDTO> listTinhThanhs(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenTinhThanh", required = false) String sTenTinhThanh,
			@RequestParam(value = "sMaTinhThanh", required = false) String sMaTinhThanh,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		TinhThanhBDTO lstTinhThanh = new TinhThanhBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			if (trangThai != null && trangThai.trim().toLowerCase() == "Sử Dụng") {
				trangThai = "1";
			} else if (trangThai != null && trangThai.trim().toLowerCase() == "Không Sử dụng") {
				trangThai = "0";
			}

			lstTinhThanh = cvService.listTinhThanhs(strfilter, sTenTinhThanh, sMaTinhThanh, pageNo, pageSize, keySort,
					desc, trangThai);

			return new ResponseEntity<TinhThanhBDTO>(lstTinhThanh, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}
	/**
	 * Lấy tất cả quốc tịch
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	@RequestMapping(value = "/tinhthanh/getall", method = RequestMethod.GET)
	public ResponseEntity<TinhThanhBDTO> listAlls(
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			TinhThanhBDTO bdto = new TinhThanhBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<TinhThanhBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		TinhThanhBDTO lst = new TinhThanhBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			lst = cvService.listAll(keySort, desc, trangThai);

			return new ResponseEntity<TinhThanhBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<TinhThanhBDTO>(lst, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	/**
	 * Create TinhThanh
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/tinhthanh/themmoi")
	public ResponseEntity<?> createTinhThanh(@Valid @RequestBody TinhThanhDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.CREATE);

		if (!cvService.isExistTinhThanhAdd(dto)) {
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			dto.setNguoiTaoId(userInfo.getId());
			try {
				cvService.saveTinhThanh(dto);

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_TINH_THANH");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới tỉnh thành");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<TinhThanhDTO>(dto, HttpStatus.CREATED);

			} catch (Exception e) {
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	@PutMapping("/tinhthanh/capnhat")
	public ResponseEntity<?> updateTinhThanh(@Valid @RequestBody TinhThanhDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		logger.info(Constants.Logs.UPDATE);

		if (cvService.isExistById(dto.getId())) {
			if (!cvService.isExistTinhThanhUpdate(dto)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				try {
					cvService.updateTinhThanh(dto);

					Timestamp timeStamp = new Timestamp(new Date().getTime());
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_TINH_THANH");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật tỉnh thành");
					// save db
					cvServiceLog.AddLogHeThong(dtoLog);

					return new ResponseEntity<TinhThanhDTO>(dto, HttpStatus.ACCEPTED);
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}

			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

	}

	@DeleteMapping("/tinhthanh/xoa/{id}")
	public ResponseEntity<?> deleteTinhThanh(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			if (!cvService.isExistById(id)) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

			}
			if (cvService.deleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_TINH_THANH");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa tỉnh thành");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.USE_CODONG, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/tinhthanh/chitiet/{id}")
	public ResponseEntity<?> getByIdTinhThanh(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			TinhThanhDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = TinhThanhDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}