package com.temp.springboot.web.controller.NguoiDung;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.NhomNguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungDTO;
import com.temp.service.NguoiDung.QtNhomNguoiDungService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class QtNhomNguoiDungApiController {

	public static final Logger logger = LoggerFactory.getLogger(QtNhomNguoiDungApiController.class);

	@Autowired
	private QtNhomNguoiDungService qtService;

	@RequestMapping(value = "/nhomnguoidung/list", method = RequestMethod.GET)
	public ResponseEntity<QtNhomNguoiDungBDTO> getListGroupUser(
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sKeySearch", required = false) String keySearch,
			@RequestParam(value = "sTenNhomNguoiDung", required = false) String tenNhomNguoiDung,
			@RequestParam(value = "sGhiChu", required = false) String ghiChu,
			@RequestParam(value = "sTrangThai", required = false) String trangThai) {

		QtNhomNguoiDungBDTO bdto = new QtNhomNguoiDungBDTO();
		logger.info(Constants.Logs.LIST);
		if (pageNo > 0) {
			pageNo = pageNo - 1;
		}
		try {

			bdto = qtService.getList(pageNo, pageSize, keySort, desc, keySearch, tenNhomNguoiDung, ghiChu, trangThai);

			return new ResponseEntity<QtNhomNguoiDungBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QtNhomNguoiDungBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/nhomnguoidung/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			QtNhomNguoiDungDTO dto = qtService.findById(id);
			if (dto == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(dto);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}


	/**
	 * Create Qt nhóm người dùng
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json
	 */
	@PostMapping("/nhomnguoidung/add")
	public ResponseEntity<?> add(@Valid @RequestBody QtNhomNguoiDungDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		QtNhomNguoiDungDTO dtoRespon = new QtNhomNguoiDungDTO();
		try {
			logger.info(Constants.Logs.CREATE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if (!qtService.isExistQtNhomNguoiDung(dto)) {
				dtoRespon = qtService.createQtNhomNguoiDung(dto);
				if (dtoRespon != null) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_QT_NHOM_NGUOI_DUNG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới nhóm người dùng");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
					return new ResponseEntity<QtNhomNguoiDungDTO>(dtoRespon, HttpStatus.CREATED);
				}

				return new ResponseEntity<QtNhomNguoiDungDTO>(dto, HttpStatus.FOUND);
			}

			return new ResponseEntity<Object>(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * Cập nhập Qt nhóm người dùng
	 * 
	 * @param qtBdto 1 đối tượng chứa id > 0
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@PutMapping("/nhomnguoidung/update")
	public ResponseEntity<?> update(@Valid @RequestBody QtNhomNguoiDungDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			QtNhomNguoiDungDTO dtoRespon = new QtNhomNguoiDungDTO();
			if (qtService.isExistById(dto.getId())) {
				dtoRespon = qtService.updateQtNhomNguoiDung(dto);
				if (dtoRespon != null) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_NHOM_NGUOI_DUNG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật nhóm người dùng");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
					return new ResponseEntity<QtNhomNguoiDungDTO>(dtoRespon, HttpStatus.ACCEPTED);
				}
				return new ResponseEntity<QtNhomNguoiDungDTO>(dto, HttpStatus.FOUND);

			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@DeleteMapping("/nhomnguoidung/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_QT_NHOM_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa nhóm người dùng");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/nhomnguoidung/chucnang", method = RequestMethod.GET)
	public ResponseEntity<NguoiDungJoinAllDTO> getTreeFunctionShow() throws ApiRequestException {
		try {
			logger.info(Constants.Logs.FUNCTION);
			NguoiDungJoinAllDTO outPut = qtService.getTreeFunctionShow();
			return new ResponseEntity<NguoiDungJoinAllDTO>(outPut, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}
	
	@RequestMapping(value = "/nhomnguoidung/changestatus/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> changeStatus(@PathVariable("id") int id,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());

			if (qtService.isExistById(id)) {
				qtService.changeStatus(id);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_NHOM_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thay đổi trạng thái");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
			}
			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	// --------------------------------------------------------------------------------END
//	@RequestMapping(value = "/qtnhomnguoidung/filter", method = RequestMethod.GET)
//	public ResponseEntity<QtNhomNguoiDungBDTO> getList(
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "sKeySearch", required = false) String keySearch,
//			@RequestParam(value = "sTenNhomNguoiDung", required = false) String tenNhomNguoiDung,
//			@RequestParam(value = "sGhiChu", required = false) String ghiChu,
//			@RequestParam(value = "sTrangThai", required = false) String trangThai) {
//
//		QtNhomNguoiDungBDTO bdto = new QtNhomNguoiDungBDTO();
//		logger.info(Constants.Logs.LIST);
//		if (pageNo > 0) {
//			pageNo = pageNo - 1;
//		}
//		try {
//
//			bdto = qtService.getList(pageNo, pageSize, keySort, desc, keySearch, tenNhomNguoiDung, ghiChu, trangThai);
//
//			return new ResponseEntity<QtNhomNguoiDungBDTO>(bdto, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<QtNhomNguoiDungBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}
//
//	@GetMapping("/qtnhomnguoidung/{id}")
//	public ResponseEntity<?> getById(@PathVariable("id") int id) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			QtNhomNguoiDungDTO dto = qtService.findById(id);
//			if (dto == null) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
//			}
//			return ResponseEntity.ok().body(dto);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}
//
//	@GetMapping("/qtnhomnguoidung/chitiet/{id}")
//	public ResponseEntity<?> getByIdQtNguoiDung(@PathVariable("id") int id) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			NhomNguoiDungJoinAllDTO qt = qtService.findByJoinAllId(id);
//			if (qt == null) {
//				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
//						HttpStatus.BAD_REQUEST);
//			}
//			return ResponseEntity.ok().body(qt);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}
//
//	/**
//	 * Cập nhật trạng thái
//	 * 
//	 * @param id
//	 * @return
//	 * @throws ApiRequestException
//	 */
//	@RequestMapping(value = "/qtnhomnguoidung/changestatus", method = RequestMethod.GET)
//	public ResponseEntity<?> changeStatus(@RequestParam(value = "id", defaultValue = "0") Integer id,
//			@RequestParam(name = "trangThai", defaultValue = "false") boolean trangThai,
//			@Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.UPDATE);
//			// lấy thông tin user
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//			Timestamp timeStamp = new Timestamp(new Date().getTime());
//
//			if (qtService.isExistById(id)) {
//				qtService.changeStatus(id);
//
//				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//				// set IP thực hiện lấy IP remote client
//				dtoLog.setIpThucHien(Utils.getClientIp(request));
//				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_NHOM_NGUOI_DUNG");
//				dtoLog.setNgayTao(timeStamp);
//				dtoLog.setNguoiTaoId(userInfo.getId());
//				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thay đổi trạng thái");
//				// save db
//				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//
//				return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
//			}
//			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);
//
//		}
//
//	}

	/**
	 * Create Qt nhóm người dùng
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json
	 */
//	@PostMapping("/qtnhomnguoidung/themmoi")
//	public ResponseEntity<?> createQtNhomNguoiDung(@Valid @RequestBody QtNhomNguoiDungDTO dto,
//			@Context HttpServletRequest request) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.CREATE);
//			// lấy thông tin user
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//			Timestamp timeStamp = new Timestamp(new Date().getTime());
//			if (!qtService.isExistQtNhomNguoiDung(dto)) {
//				if (qtService.createQtNhomNguoiDung(dto)) {
//					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//					// set IP thực hiện lấy IP remote client
//					dtoLog.setIpThucHien(Utils.getClientIp(request));
//					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_QT_NHOM_NGUOI_DUNG");
//					dtoLog.setNgayTao(timeStamp);
//					dtoLog.setNguoiTaoId(userInfo.getId());
//					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới nhóm người dùng");
//					// save db
//					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//				}
//
//				return new ResponseEntity<QtNhomNguoiDungDTO>(dto, HttpStatus.CREATED);
//			}
//
//			return new ResponseEntity<Object>(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
//
//		}
//
//	}

	/**
	 * Cập nhập Qt nhóm người dùng
	 * 
	 * @param qtBdto 1 đối tượng chứa id > 0
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@PutMapping("/qtnhomnguoidung/capnhat")
	public ResponseEntity<?> updateQtNhomNguoiDung(@Valid @RequestBody QtNhomNguoiDungDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if (qtService.isExistById(dto.getId())) {
				qtService.updateQtNhomNguoiDung(dto);
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_NHOM_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật nhóm người dùng");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<QtNhomNguoiDungDTO>(dto, HttpStatus.ACCEPTED);

			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * Lấy tất cả nhóm người dùng
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	@RequestMapping(value = "/qtnhomnguoidung/getall", method = RequestMethod.GET)
	public ResponseEntity<QtNhomNguoiDungBDTO> listAlls(
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			QtNhomNguoiDungBDTO bdto = new QtNhomNguoiDungBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QtNhomNguoiDungBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QtNhomNguoiDungBDTO lst = new QtNhomNguoiDungBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			lst = qtService.listAll(keySort, desc, trangThai);

			return new ResponseEntity<QtNhomNguoiDungBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QtNhomNguoiDungBDTO>(lst, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Xóa 1 Qt nhóm người dùng
	 * 
	 * @param id của Qt nhóm người dùng
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/qtnhomnguoidung/xoa/{id}")
	public ResponseEntity<?> deleteQtNhomNguoiDung(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_QT_NHOM_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa nhóm người dùng");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Validate keySort param
	 * 
	 * @param keySort
	 * @return true or false
	 */
	private boolean checkKeySort(String keySort) {

		Field fld[] = QtNhomNguoiDungDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}

	/**
	 * Create Qt nhóm người dùng
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json
	 */

//	@RequestMapping(value = "/qtnhomnguoidung/chucnang", method = RequestMethod.GET)
//	public ResponseEntity<NguoiDungJoinAllDTO> getTreeFunctionShow() throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.FUNCTION);
//			NguoiDungJoinAllDTO outPut = qtService.getTreeFunctionShow();
//			return new ResponseEntity<NguoiDungJoinAllDTO>(outPut, HttpStatus.CREATED);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
//
//		}
//
//	}

}