package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DropDownDTO;
import com.temp.model.DropDownStringDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCellMenuBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.BieuMauBaoCao.SheetCellService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class SheetCellApiController {

	public static final Logger logger = LoggerFactory.getLogger(SheetCellApiController.class);

	@Autowired
	@Qualifier("SheetCellServiceImpl")
	private SheetCellService sheetCellService;

	/**
	 * Create cell sheet
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/cellsheet/themmoi")
	public ResponseEntity<?> createSheet(@Valid @RequestBody BmSheetCtDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		//logger.info(Constants.Logs.CREATE);

		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//	if (this.sheetCellService.checkExistAddCell(dto.getSheetId(), dto.getBmSheetCotId(),
//		dto.getBmSheetHangTu(), dto.getBmSheetHangDen())) {
//	    throw new ApiRequestException(Constants.Messages.CELL_SHEET_EXIST,
//		    HttpStatus.EXPECTATION_FAILED);
//	}
		if (dto.getDinhDang() != null && "CT".equals(dto.getDinhDang().toUpperCase()) && dto.getCongThucDto() == null) {
			throw new ApiRequestException(Constants.Messages.CREATE_EXPRESSION, HttpStatus.EXPECTATION_FAILED);
		}

		try {

			// [U - 20200509] update use Thiet lap cong thuc
			/*
			 * if (dto.getDinhDang() != null && "CT".equals(dto.getDinhDang().toUpperCase())
			 * && dto.getCongThucDto() != null) { // kiểm tra tính hợp lệ của công thức
			 * không phải là hàm if (!dto.getCongThucDto().isDungHam()) {
			 * ScriptEngineManager mgr = new ScriptEngineManager(); ScriptEngine engine =
			 * mgr.getEngineByName("JavaScript");
			 * //logger.info(Constants.Logs.CREATE_EXPRESSION_CHECK); //
			 * engine.eval(dto.getCongThucDto().getCongThuc()); } Gson convertJson = new
			 * Gson(); // covert sang json lưu dưới dạng text
			 * dto.setCongThucText(convertJson.toJson(dto.getCongThucDto())); }
			 */

			BmSheetCtDTO output = this.sheetCellService.AddOrUpdateSheet(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_CELLSHEET");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới cấu hình cell sheet");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmSheetCtDTO>(output, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * cập nhập cell sheet
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/cellsheet/capnhat")
	public ResponseEntity<?> updateSheet(@Valid @RequestBody BmSheetCtDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		//logger.info(Constants.Logs.UPDATE);

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		BmSheetCtDTO qt = sheetCellService.FindById(dto.getId());

		if (qt == null) {
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
		try {
			// [U - 20200509] update use Thiet lap cong thuc
			/*
			 * if (dto.getDinhDang() != null && "CT".equals(dto.getDinhDang().toUpperCase())
			 * && dto.getCongThucDto() != null) { // kiểm tra tính hợp lệ của công thức
			 * không phải là hàm if (!dto.getCongThucDto().isDungHam()) {
			 * ScriptEngineManager mgr = new ScriptEngineManager(); ScriptEngine engine =
			 * mgr.getEngineByName("JavaScript");
			 * //logger.info(Constants.Logs.CREATE_EXPRESSION_CHECK); //
			 * engine.eval(dto.getCongThucDto().getCongThuc()); } Gson convertJson = new
			 * Gson(); // covert sang json lưu dưới dạng text
			 * dto.setCongThucText(convertJson.toJson(dto.getCongThucDto())); }
			 */
			sheetCellService.AddOrUpdateSheet(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_ELLSHEET");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật cấu hình cell sheet ");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmSheetCtDTO>(dto, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * lấy chi tiết cell
	 * 
	 * @param id
	 * @return
	 * @throws ApiRequestException
	 */
	@GetMapping("/cellsheet/chitiet/{id}")
	public ResponseEntity<?> getByIdSheet(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			BmSheetCtDTO qt = sheetCellService.FindById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}

			// [U-20200509] - update Thiet lap cong thuc
			/*
			 * if (qt.getCongThucText() != null) { Gson gson = new Gson(); CongThucDTO
			 * objParser = gson.fromJson(qt.getCongThucText(), CongThucDTO.class);
			 * qt.setCongThucDto(objParser); }
			 */

			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * lấy chi tiết cell
	 * 
	 * @param id
	 * @return
	 * @throws ApiRequestException
	 */
	@GetMapping("/cellsheet/validatechonhang")
	public ResponseEntity<?> validateChonDenHang(@RequestParam(value = "tuHangId", required = true) Integer tuHangId,
			@RequestParam(value = "denHangId", required = true) Integer denHangId) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			if (tuHangId != null && denHangId != null) {
				boolean qt = sheetCellService.validateTuHangDenHang(tuHangId, denHangId);

				return ResponseEntity.ok().body(qt);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * lấy danh sách các cell được thiết lập trong sheet
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/cellsheet/dscellcauhinh", method = RequestMethod.GET)
	public ResponseEntity<BmSheetCellMenuBDTO> listCotSheet(
			@RequestParam(value = "sheetId", required = false) Integer sheetId,
			@RequestParam(value = "cotId", required = false) Integer cotId,
			@RequestParam(value = "hangId", required = false) Integer hangId,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) throws ApiRequestException {

		BmSheetCellMenuBDTO lst = null;

		try {

			Integer pageNoReqest = 0;
			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNoReqest = pageNo - 1;
			}
			lst = sheetCellService.listMenuLeftCellSheet(sheetId, cotId, hangId, pageNoReqest, pageSize);

			return new ResponseEntity<BmSheetCellMenuBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@DeleteMapping("/cellsheet/xoa/{id}")
	public ResponseEntity<?> deleteCell(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.DELETE);
			BmSheetCtDTO oldDTO = sheetCellService.FindById(id);
			if (oldDTO.getBmSheetCtVaoId() != null && oldDTO.getBmSheetCtVaoId() > 0) {
				throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
			}
			if (sheetCellService.DeleteById(id)) { // xoastheo id
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CELLSHEET");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa ô sheet");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * lấy danh sách các cột được thiết lập trong sheet làm dropdown
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/cellsheet/dschitieu", method = RequestMethod.GET)
	public ResponseEntity<List<DropDownDTO>> listCotSheetCongThuc(
			@RequestParam(value = "sheedId", required = false) Integer sheedId,
			@RequestParam(value = "hangId", required = false) Integer hangId,
			@RequestParam(value = "cotId", required = false) Integer cotId) throws ApiRequestException {

		List<DropDownDTO> lst = null;

		try {

			//logger.info(Constants.Logs.LIST);

			lst = sheetCellService.dropdownChitieu(sheedId, cotId, hangId);

			return new ResponseEntity<List<DropDownDTO>>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * lấy danh sách các cột/ cong thuc được thiết lập trong sheet làm dropdown
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/cellsheet/chitieucongthuc", method = RequestMethod.GET)
	public ResponseEntity<List<DropDownStringDTO>> listCotSheetCongThucDropDown(
			@RequestParam(value = "sheedId", required = false) Integer sheedId,
			@RequestParam(value = "hangId", required = false) Integer hangId,
			@RequestParam(value = "cotId", required = false) Integer cotId) throws ApiRequestException {

		List<DropDownStringDTO> lst = null;

		try {

			//logger.info(Constants.Logs.LIST);

			lst = sheetCellService.dropdownChitieuCongThuc(sheedId, cotId, hangId);

			return new ResponseEntity<List<DropDownStringDTO>>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@SuppressWarnings("unused")
	private boolean checkKeySort(String keySort) {

//		Field fld[] = ChucVuDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

//		for (int i = 0; i < fld.length; i++) {
//			listName.add(fld[i].getName());
//		}

		return listName.contains(keySort);
	}

}

//------------------- Delete All Users-----------------------------

//	@RequestMapping(value = "/chucvu/", method = RequestMethod.DELETE)
//	@DeleteMapping("/user/")
//	public ResponseEntity<UserBDTO> deleteAllUsers() {
//		//logger.info("Deleting All Users");
//		try {
//			userService.deleteAllUsers();
//			return new ResponseEntity<UserBDTO>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity(new ApiRequestException("Unable to delete."), HttpStatus.NOT_FOUND);
//		}
//
//	}
