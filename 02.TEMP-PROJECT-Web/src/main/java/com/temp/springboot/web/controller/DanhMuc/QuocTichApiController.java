package com.temp.springboot.web.controller.DanhMuc;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DanhMuc.QuocTichBDTO;
import com.temp.model.DanhMuc.QuocTichDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.DanhMuc.QuocTichService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController // phục vụ cho restful webservice
@RequestMapping("/api/v1") // tất cả các đường dẫn có thêm /api/v1
public class QuocTichApiController {

	public static final Logger logger = LoggerFactory.getLogger(QuocTichApiController.class); // log ra file

	@Autowired
	@Qualifier("QuocTichServiceImpl") // chỉ đích danh thằng bean khi có 2 thằng cùng implement nó sẽ kb sử dụng thằng
										// nào..cái này để xử lí nhiều bean trong cùng 1 projec
	private QuocTichService cvService; // gán bean ch biến hay inject vào đối tượng

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService cvServiceLog;

	@RequestMapping(value = "/quoctich/filter", method = RequestMethod.GET) // phương thức get
	public ResponseEntity<QuocTichBDTO> listQuocTichs(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter, // bắt validate
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo, // param truyền lên
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenQuocTich", required = false) String sTenQuocTich,
			@RequestParam(value = "sMaQuocTich", required = false) String sMaQuocTich,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException { // sử
																												// dụng
																												// ApiRequestException
																												// để
																												// bắt
																												// ngoại
																												// lệ

		if (!checkKeySort(keySort)) { // all check keySort khi truyền sau param k dúng sẽ thống báo cho người dùng.gọi
										// đên hàm checkKeySort với số truyền vào keySort

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED); // trả về
																											// ApiRequestException
		}

		QuocTichBDTO lstQuocTich = new QuocTichBDTO(); // tạo đối tượng

		try { // bắt try

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			if (trangThai != null && trangThai.trim().toLowerCase() == "Sử Dụng") {
				trangThai = "1"; // trạng thái khác null và xóa khoảng trống đâu và không phân biệt chữ hoa
									// thương thì trang thái trangThai = "1" còn không ngược lại
			} else if (trangThai != null && trangThai.trim().toLowerCase() == "Không Sử dụng") {
				trangThai = "0";
			}

			lstQuocTich = cvService.listQuocTichs(strfilter, sTenQuocTich, sMaQuocTich, pageNo, pageSize, keySort, desc,
					trangThai); // list gọi đến hàm ở serve tương ứng với các param truyền lên

			return new ResponseEntity<QuocTichBDTO>(lstQuocTich, HttpStatus.OK); // trả về list json

		} catch (Exception e) { // bắt ngoại lệ
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED); // ngoại lệ
		}
	}

	/**
	 * Lấy tất cả quốc tịch
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	@RequestMapping(value = "/quoctich/getall", method = RequestMethod.GET)
	public ResponseEntity<QuocTichBDTO> listAlls(@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			QuocTichBDTO bdto = new QuocTichBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QuocTichBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QuocTichBDTO lst = new QuocTichBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			lst = cvService.listAll(keySort, desc, trangThai);

			return new ResponseEntity<QuocTichBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QuocTichBDTO>(lst, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Create QuocTich
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/quoctich/themmoi") // method post để thêm mới
	public ResponseEntity<?> createQuocTich(@Valid @RequestBody QuocTichDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException { // @RequestBody truyền lên 1 đối tượng để thêm
		logger.info(Constants.Logs.CREATE);

		if (!cvService.isExistQuocTichAdd(dto)) { // nếu khác hàm check add thì cho add và log
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor(); // thông tin người dùng
			dto.setNguoiTaoId(userInfo.getId());
			try { // bắt ngoại lệ
				cvService.saveQuocTich(dto); // gọi đến servive add

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_QUOC_TICH");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới Quốc tịch");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<QuocTichDTO>(dto, HttpStatus.CREATED); // thành công trả về HttpStatus.CREATED

			} catch (Exception e) { // bắn ra exception
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED); // bắn ra exception
																									// khi check k thỏa
																									// mãn

	}

	@PutMapping("/quoctich/capnhat") // cập nhật dùng method put
	public ResponseEntity<?> updateQuocTich(@Valid @RequestBody QuocTichDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException { // truyền lên đối tượng

		logger.info(Constants.Logs.UPDATE);
		if (cvService.isExistById(dto.getId())) { // gọi hàm check ở service
			if (!cvService.isExistQuocTichUpdate(dto)) { // check update khác
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				try { // bắt ngoại lệ
					cvService.updateQuocTich(dto); // cập nhật lưu db

					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QUOC_TICH");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật quốc tịch");
					// save db
					cvServiceLog.AddLogHeThong(dtoLog);

					return new ResponseEntity<QuocTichDTO>(dto, HttpStatus.ACCEPTED); // thành công trả về
																						// ResponseEntity
				} catch (Exception e) { // ngược lại sih ra ngoại lệ
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}

			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED); // ngoại lệ khi
																										// không có id
																										// phù hợp
		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND); // ngoại lệ khi check
																								// trùng

	}

	@DeleteMapping("/quoctich/xoa/{id}") // xóa với tham số truyền lên @PathVariable id
	public ResponseEntity<?> deleteQuocTich(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			if (!cvService.isExistById(id)) { // check id
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND); // bắn ra ngoại lệ

			}
			if (cvService.deleteById(id)) { // xoastheo id
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_QUOC_TICH");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa quốc tịch");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT); // thành
																												// công
																												// sẽ
																												// trả
																												// về
																												// json
																												// HttpStatus.NO_CONTENT
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST); // sẽ sinh ra ngoại
																									// lệ

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/quoctich/chitiet/{id}") // lấy theo id
	public ResponseEntity<?> getByIdQuocTich(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			QuocTichDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = QuocTichDTO.class.getDeclaredFields(); // 1 mảng đổi tượng
		List<String> listName = new ArrayList<String>(); // tạo 1 lits

		for (int i = 0; i < fld.length; i++) { // for mảng add vào lits lấy tên
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}