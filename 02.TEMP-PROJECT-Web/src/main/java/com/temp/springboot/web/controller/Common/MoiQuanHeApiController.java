package com.temp.springboot.web.controller.Common;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.Common.MoiQuanHeBDTO;
import com.temp.model.Common.MoiQuanHeDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.Common.MoiQuanHeService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class MoiQuanHeApiController {

	public static final Logger logger = LoggerFactory.getLogger(MoiQuanHeApiController.class);
	@Autowired
	@Qualifier("MoiQuanHeServiceImpl")
	private MoiQuanHeService cvService;

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService cvServiceLog;

	@RequestMapping(value = "/moiquanhe/filter", method = RequestMethod.GET)
	public ResponseEntity<MoiQuanHeBDTO> listMoiQuanHes(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenMoiQuanHe", required = false) String sTenMoiQuanHe,
			@RequestParam(value = "sMaMoiQuanHe", required = false) String sMaMoiQuanHe,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		MoiQuanHeBDTO lstMoiQuanHe = new MoiQuanHeBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			if (trangThai != null && trangThai.trim().toLowerCase() == "Sử Dụng") {
				trangThai = "1";
			} else if (trangThai != null && trangThai.trim().toLowerCase() == "Không Sử dụng") {
				trangThai = "0";
			}

			lstMoiQuanHe = cvService.listMoiQuanHes(strfilter, sTenMoiQuanHe, sMaMoiQuanHe, pageNo, pageSize, keySort,
					desc, trangThai);

			return new ResponseEntity<MoiQuanHeBDTO>(lstMoiQuanHe, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Create MoiQuanHe
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/moiquanhe/themmoi")
	public ResponseEntity<?> createMoiQuanHe(@Valid @RequestBody MoiQuanHeDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.CREATE);

		if (!cvService.isExistMoiQuanHeAdd(dto)) {
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			dto.setNguoiTaoId(userInfo.getId());
			try {
				cvService.saveMoiQuanHe(dto);

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_MOI_QUAN_HE");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới mối quan hệ");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<MoiQuanHeDTO>(dto, HttpStatus.CREATED);

			} catch (Exception e) {
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	@PutMapping("/moiquanhe/capnhat")
	public ResponseEntity<?> updateMoiQuanHe(@Valid @RequestBody MoiQuanHeDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		logger.info(Constants.Logs.UPDATE);

		if (cvService.isExistById(dto.getId())) {
			if (!cvService.isExistMoiQuanHeUpdate(dto)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				try {
					cvService.updateMoiQuanHe(dto);

					Timestamp timeStamp = new Timestamp(new Date().getTime());
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_MOI_QUAN_HE");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật mối quan hệ");
					// save db
					cvServiceLog.AddLogHeThong(dtoLog);

					return new ResponseEntity<MoiQuanHeDTO>(dto, HttpStatus.ACCEPTED);
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}

			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

	}

	@DeleteMapping("/moiquanhe/xoa/{id}")
	public ResponseEntity<?> deleteMoiQuanHe(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			if (!cvService.isExistById(id)) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

			}
			if (cvService.deleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_MOI_QUAN_HE");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa mối quan hệ");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/moiquanhe/chitiet/{id}")
	public ResponseEntity<?> getByIdMoiQuanHe(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			MoiQuanHeDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = MoiQuanHeDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
	
	@RequestMapping(value = "/moiquanhe/exitsMa", method = RequestMethod.GET)
	public ResponseEntity<Boolean> checkExitsByMa(
			@RequestParam(value = "maMoiQuanHe", required = false) String maMoiQuanHe,
			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
		Boolean result=false;

		try {
			logger.info(Constants.Logs.EXITS);
			
			result = cvService.isExistByMa(maMoiQuanHe,id);

			return new ResponseEntity<Boolean>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}
}