package com.temp.springboot.web.controller.NguoiDung;

import java.sql.Timestamp;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.ChucNangChiTietBDTO;
import com.temp.model.NguoiDung.ChucNangChiTietDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.NguoiDung.ChucNangChiTietService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class ChucNangChiTietApiController {

	public static final Logger logger = LoggerFactory.getLogger(ChucNangChiTietApiController.class);

	@Autowired
	@Qualifier("ChucNangChiTietServiceImpl")
	private ChucNangChiTietService cvService;

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService cvServiceLog;

	@RequestMapping(value = "/chucnangchitiet/list", method = RequestMethod.GET)
	public ResponseEntity<ChucNangChiTietBDTO> getList(
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "15") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "keySearch", required = false) String keySearch,
			@RequestParam(value = "tenChucNang", required = false) String tenChucNang,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		ChucNangChiTietBDTO bdto = new ChucNangChiTietBDTO();

		logger.info(Constants.Logs.LIST);

		if (pageNo > 0) {
			pageNo = pageNo - 1;
		}
		try {
			bdto = cvService.filter(keySearch, tenChucNang, trangThai, pageNo, pageSize, keySort, desc);

			return new ResponseEntity<ChucNangChiTietBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<ChucNangChiTietBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/chucnangchitiet/add")
	public ResponseEntity<?> add(@Valid @RequestBody ChucNangChiTietDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		logger.info(Constants.Logs.CREATE);

		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		try {
			cvService.addOrUpdate(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_CHUCNANGCHITIET");
			dtoLog.setNgayTao(timeStamp);
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung(
					"Tài khoản " + userInfo.getTaiKhoan() + " thêm mới chức năng chi tiết " + dto.getTenChucNang());
			// save db
			cvServiceLog.AddLogHeThong(dtoLog);
			return new ResponseEntity<ChucNangChiTietDTO>(dto, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@PutMapping("/chucnangchitiet/update")
	public ResponseEntity<?> update(@Valid @RequestBody ChucNangChiTietDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.UPDATE);

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		try {
			cvService.addOrUpdate(dto);

			Timestamp timeStamp = new Timestamp(new Date().getTime());
			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_CHUCNANGCHITIET");
			dtoLog.setNgayTao(timeStamp);
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật chức năng chi tiết");
			// save db
			cvServiceLog.AddLogHeThong(dtoLog);

			return new ResponseEntity<ChucNangChiTietDTO>(dto, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@DeleteMapping("/chucnangchitiet/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			ChucNangChiTietDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			if (cvService.deleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUCNANGCHITIET");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa chức năng chi tiết ");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/chucnangchitiet/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			ChucNangChiTietDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}
}
