package com.temp.springboot.web.controller.DanhMuc;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DanhMuc.PhuongXaBDTO;
import com.temp.model.DanhMuc.PhuongXaDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.DanhMuc.PhuongXaService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class PhuongXaApiController {

	public static final Logger logger = LoggerFactory.getLogger(PhuongXaApiController.class);
	@Autowired
	@Qualifier("PhuongXaServiceImpl")
	private PhuongXaService cvService;

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService cvServiceLog;

	@RequestMapping(value = "/phuongxa/filter", method = RequestMethod.GET)
	public ResponseEntity<PhuongXaBDTO> listPhuongXas(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenPhuongXa", required = false) String sTenPhuongXa,
			@RequestParam(value = "sMaPhuongXa", required = false) String sMaPhuongXa,
			@RequestParam(value = "sTenQuanHuyen", required = false) String sTenQuanHuyen,
			@RequestParam(value = "sTenTinhThanh", required = false) String sTenTinhThanh,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		PhuongXaBDTO lstPhuongXa = new PhuongXaBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			if (trangThai != null && trangThai.trim().toLowerCase() == "Sử Dụng") {
				trangThai = "1";
			} else if (trangThai != null && trangThai.trim().toLowerCase() == "Không Sử dụng") {
				trangThai = "0";
			}

			lstPhuongXa = cvService.listPhuongXas(strfilter, sTenPhuongXa, sMaPhuongXa, pageNo, pageSize, keySort, desc,
					trangThai, sTenTinhThanh, sTenQuanHuyen);

			return new ResponseEntity<PhuongXaBDTO>(lstPhuongXa, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Lấy tất cả phường xá
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	@RequestMapping(value = "/phuongxa/getall", method = RequestMethod.GET)
	public ResponseEntity<PhuongXaBDTO> listAlls(@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "trangThai", required = false) String trangThai,
			@RequestParam(value = "tinhThanhId", required = false) Integer tinhThanhId,
			@RequestParam(value = "quanHuyenId", required = false) Integer quanHuyenId) {

		if (!checkKeySort(keySort)) {

			PhuongXaBDTO bdto = new PhuongXaBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<PhuongXaBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		PhuongXaBDTO lst = new PhuongXaBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			lst = cvService.getListByTTandQH(keySort, desc, trangThai, tinhThanhId, quanHuyenId);

			return new ResponseEntity<PhuongXaBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<PhuongXaBDTO>(lst, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Create PhuongXa
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/phuongxa/themmoi")
	public ResponseEntity<?> createPhuongXa(@Valid @RequestBody PhuongXaDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.CREATE);

		if (!cvService.isExistPhuongXaAdd(dto)) {
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			dto.setNguoiTaoId(userInfo.getId());
			try {
				cvService.savePhuongXa(dto);

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_PHUONG_XA");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới phường xá");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<PhuongXaDTO>(dto, HttpStatus.CREATED);

			} catch (Exception e) {
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
			}
		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
	}

	@PutMapping("/phuongxa/capnhat")
	public ResponseEntity<?> updatePhuongXa(@Valid @RequestBody PhuongXaDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		logger.info(Constants.Logs.UPDATE);

		if (cvService.isExistById(dto.getId())) {
			if (!cvService.isExistPhuongXaUpdate(dto)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				try {
					cvService.updatePhuongXa(dto);

					Timestamp timeStamp = new Timestamp(new Date().getTime());
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_PHUONG_XA");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật phường xá");
					// save db
					cvServiceLog.AddLogHeThong(dtoLog);

					return new ResponseEntity<PhuongXaDTO>(dto, HttpStatus.ACCEPTED);
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}

			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

	}

	@DeleteMapping("/phuongxa/xoa/{id}")
	public ResponseEntity<?> deletePhuongXa(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			if (!cvService.isExistById(id)) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

			}
			if (cvService.deleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_PHUONG_XA");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa phường xá id = " + id);
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/phuongxa/chitiet/{id}")
	public ResponseEntity<?> getByIdPhuongXa(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			PhuongXaDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = PhuongXaDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}