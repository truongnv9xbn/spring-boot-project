package com.temp.springboot.web.controller.NguoiDung;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.QtLichSuTruyCapBDTO;
import com.temp.model.NguoiDung.QtLichSuTruyCapExportDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.service.NguoiDung.LichSuTruyCapService;
import com.temp.utils.CellConfig;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

/**
 * @author DucHV1
 *
 */
@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class QtLichSuTruyCapApiController {
    public static final Logger logger = LoggerFactory.getLogger(QtLichSuTruyCapApiController.class);

    @Autowired
    @Qualifier("LichSuTruyCapServiceImpl")
    private LichSuTruyCapService lichSuTruyCapService;

    /**
     * Get list filter lich su truy cap.
     * 
     * @param keysearch
     * @param denNgay
     * @param ipThucHien
     * @param tenTaiKhoan
     * @param tuNgay
     * @param logType
     * @param pageNo
     * @param pageSize
     * @param keySort
     * @param desc
     * @return
     */
    @RequestMapping(value = "/qtlichsu/filter", method = RequestMethod.GET)
    public ResponseEntity<QtLichSuTruyCapBDTO> listLichSuTruyCap(
	    @Valid @RequestParam(value = "keysearch", required = false) String keysearch,
	    @RequestParam(value = "denNgay", required = false) String denNgay,
	    @RequestParam(value = "ipThucHien", required = false) String ipThucHien,
	    @RequestParam(value = "tenTaiKhoan", required = false) String tenTaiKhoan,
	    @RequestParam(value = "loaiNguoiDung", required = false) String loaiNguoiDung,
	    @RequestParam(value = "tuNgay", required = false) String tuNgay,
	    @RequestParam(value = "logType", required = false) String logType,
	    @RequestParam(value = "ctckThongTinId", required = false) Integer ctckThongTinId,
	    @RequestParam(name = "pageNo", defaultValue = "0") int pageNo,
	    @RequestParam(name = "pageSize", defaultValue = "10") int pageSize,
	    @RequestParam(name = "keySort", defaultValue = "id") String keySort,
	    @RequestParam(name = "desc", defaultValue = "false") boolean desc) {

	QtLichSuTruyCapBDTO lstcBDTO = new QtLichSuTruyCapBDTO();
	if (!checkKeySort(keySort)) {

	    lstcBDTO.setMessage("truyền sai keySort");

	    return new ResponseEntity<QtLichSuTruyCapBDTO>(lstcBDTO, HttpStatus.METHOD_NOT_ALLOWED);
	}

	try {
	    logger.info(Constants.Logs.LIST);
	    if (pageNo > 0) {
		pageNo = pageNo - 1;
	    }

	    lstcBDTO = lichSuTruyCapService.getListLichSuTruyCapService(keysearch, denNgay,
		    ipThucHien, tenTaiKhoan, tuNgay, logType, pageNo, pageSize, keySort, desc,
		    loaiNguoiDung, ctckThongTinId);

	    return new ResponseEntity<QtLichSuTruyCapBDTO>(lstcBDTO, HttpStatus.OK);

	} catch (Exception e) {
	    logger.error(e.toString() + "-\n" + e.getStackTrace());
	    return new ResponseEntity<QtLichSuTruyCapBDTO>(lstcBDTO,
		    HttpStatus.INTERNAL_SERVER_ERROR);
	}

    }

    /**
     * Thong ke lich su truy cap.
     * 
     * @param tuNgay
     * @param denNgay
     * @param keySort
     * @param desc
     * @return
     * @throws ApiRequestException
     */
    @RequestMapping(value = "/qtlichsu/export", method = RequestMethod.GET)
    public ResponseEntity<ByteArrayResource> ExportExcel(
	    @RequestParam(value = "tuNgay", required = false) String tuNgay,
	    @RequestParam(value = "denNgay", required = false) String denNgay,
	    @RequestParam(name = "keySort", defaultValue = "id") String keySort,
	    @RequestParam(name = "desc", defaultValue = "false") boolean desc)
	    throws ApiRequestException {

	try {

	    logger.info(Constants.Logs.LIST);
	    QtLichSuTruyCapBDTO lstcBDtoExport = new QtLichSuTruyCapBDTO();

	    // init workbook, sheet, cell, and row.
	    Workbook workbook = new XSSFWorkbook();
	    Sheet sheet = workbook.createSheet("Thong_ke_lich_su_truy_cap");
	    int rownum = 0;
	    Cell cell = null;
	    Row row = null;

	    // set cell style
	    CellStyle style = this.createStyleForThongKeTruyCap(sheet, workbook);

	    // Get list report
	    lstcBDtoExport = this.lichSuTruyCapService.getListLichSuTruyCapExportService(tuNgay,
		    denNgay, keySort, desc);

	    // create header
	    this.createHeaderRow(row, sheet, cell, rownum, style);

	    // Fill value
	    if (lstcBDtoExport.getLstExportExcelLichSuTruyCap() != null
		    && lstcBDtoExport.getLstExportExcelLichSuTruyCap().size() > 0) {
		rownum = 1;
		for (QtLichSuTruyCapExportDTO exportData : lstcBDtoExport
			.getLstExportExcelLichSuTruyCap()) {
		    rownum++;
		    if (rownum > 1) {
			row = sheet.createRow((rownum));

			// STT column
			cell = row.createCell(0, CellType.STRING);
			cell.setCellValue((rownum - 1));
			cell.setCellStyle(style);

			/*
			 * if (tuNgay != null || denNgay != null) { // Thời gian đăng xuất gần nhất
			 * column cell = row.createCell(1, CellType.STRING);
			 * cell.setCellValue(lstcBDtoExport.getExportTuNgay() + " - " +
			 * lstcBDtoExport.getExportDenNgay()); cell.setCellStyle(style); } else {
			 */
			// Thời gian đăng xuất gần nhất column
			cell = row.createCell(1, CellType.STRING);
			cell.setCellValue(exportData.getNgayHienTaiStr());
			cell.setCellStyle(style);
			// }

			// SL TK 1 column
			cell = row.createCell(2, CellType.STRING);
			cell.setCellValue(exportData.getTongSoTkUBCN());

			cell.setCellStyle(style);

			// SL TK 2 column
			cell = row.createCell(3, CellType.STRING);
			cell.setCellValue(exportData.getTongSoThanhVien());
			cell.setCellStyle(style);
		    }
		}

	    }
	    sheet.autoSizeColumn(0, true);

	    CellConfig.autoSizeColumns(workbook);

	    String fileName = "Thong_ke_lich_su_truy_cap.xls";

	    FileOutputStream out = null;
	    if (workbook instanceof HSSFWorkbook) {
		out = new FileOutputStream(fileName);
	    } else if (workbook instanceof XSSFWorkbook) {
		out = new FileOutputStream("Thong_ke_lich_su_truy_cap.xlsx");
	    }
	    ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    workbook.write(bos);

	    byte[] bytes = bos.toByteArray();
	    ByteArrayResource resource = new ByteArrayResource(bytes);

	    out.close();
	    workbook.close();
	    return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION)
		    .contentLength(bytes.length) //
		    .body(resource);

	} catch (Exception e) {
	    logger.error(e.toString() + "-\n" + e.getStackTrace());
	    throw new ApiRequestException(Constants.Messages.EXCEPTION_EXPORT_EXCEL,
		    HttpStatus.BAD_REQUEST);
	}

    }

    /**
     * Create header row width style
     * 
     * @param row
     * @param sheet
     * @param cell
     * @param rownum
     * @param style
     */
    private void createHeaderRow(Row row, Sheet sheet, Cell cell, int rownum, CellStyle style) {
	// Row 1
	row = sheet.createRow(rownum);
	// merger cell STT
	sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
	// merger cell Thời gian đăng xuất gần nhất.
	sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
	// merger cell So luong tai khoan.
	sheet.addMergedRegion(new CellRangeAddress(0, 0, 2, 3));
	sheet.autoSizeColumn(0, true);
	sheet.autoSizeColumn(1, true);
	sheet.autoSizeColumn(2, true);

	// STT column
	cell = row.createCell(0, CellType.STRING);
	cell.setCellValue("STT");
	cell.setCellStyle(style);

	// Thời gian đăng xuất gần nhất column
	cell = row.createCell(1, CellType.STRING);
	cell.setCellValue("Thời gian đăng xuất gần nhất");
	cell.setCellStyle(style);

	// SL TK 1 column
	cell = row.createCell(2, CellType.STRING);
	cell.setCellValue("Số lượng tài khoản");
	cell.setCellStyle(style);

	// SL TK 2 column
	cell = row.createCell(3, CellType.STRING);
	cell.setCellValue("");
	cell.setCellStyle(style);

	// Row 2
	row = sheet.createRow(1);
	// STT column
	cell = row.createCell(0, CellType.STRING);
	cell.setCellStyle(style);

	// Thời gian đăng xuất gần nhất column
	cell = row.createCell(1, CellType.STRING);
	cell.setCellStyle(style);

	// SL TK 1 column
	cell = row.createCell(2, CellType.STRING);
	cell.setCellValue("UBCKNN");
	cell.setCellStyle(style);

	// SL TK 2 column
	cell = row.createCell(3, CellType.STRING);
	cell.setCellValue("Thành viên");
	cell.setCellStyle(style);

    }

    /**
     * Set cell style for header
     * 
     * @param sheet
     * @param workbook
     * @return
     */
    private CellStyle createStyleForThongKeTruyCap(Sheet sheet, Workbook workbook) {
	CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

	// set border
	headerCellStyle.setBorderBottom(BorderStyle.THIN);
	headerCellStyle.setBorderTop(BorderStyle.THIN);
	headerCellStyle.setBorderRight(BorderStyle.THIN);
	headerCellStyle.setBorderLeft(BorderStyle.THIN);

	// set font
	final Font font = sheet.getWorkbook().createFont();
	font.setFontName("Times New Roman");
	font.setBold(true);
	font.setFontHeightInPoints((short) 13);
	headerCellStyle.setFont(font);

	// set alignment
	headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
	headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

	return headerCellStyle;
    }

    /**
     * @param keySort
     * @return
     */
    private boolean checkKeySort(String keySort) {

	Field fld[] = QtLogHeThongDTO.class.getDeclaredFields();
	List<String> listName = new ArrayList<String>();

	for (int i = 0; i < fld.length; i++) {
	    listName.add(fld[i].getName());
	}

	return listName.contains(keySort);
    }

}
