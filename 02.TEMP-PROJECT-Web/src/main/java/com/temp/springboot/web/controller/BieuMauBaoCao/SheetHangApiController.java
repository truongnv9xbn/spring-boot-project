package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangMenuBDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.BieuMauBaoCao.SheetHangService;
import com.temp.service.BieuMauBaoCao.SheetMegerHeaderService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class SheetHangApiController {

	public static final Logger logger = LoggerFactory.getLogger(SheetHangApiController.class);

	@Autowired
	@Qualifier("SheetHangServiceImpl")
	private SheetHangService sheetHangService;
	
	@Autowired
    @Qualifier("SheetMegerHeaderServiceImpl")
    private SheetMegerHeaderService sheetMegerHeaderService;

	   /**
     * Create sheet
     * 
     * @param qt json object
     * @return ResponseEntity
     * @throws ApiRequestException
     */
    @SuppressWarnings("unused")
	@PostMapping("/hangsheet/themmoi")
    public ResponseEntity<?> createSheet(@Valid @RequestBody BmSheetHangDTO dto,
	    @Context HttpServletRequest request) throws ApiRequestException {

	//logger.info(Constants.Logs.CREATE);

	// lấy thông tin user
	QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

	try {

	    // Add new hang sheet.
	    sheetHangService.AddOrUpdateHangSheet(dto);

	    // Check sheet header exist. If not exist, create new.
	    boolean isExistTieuDeHang = this.sheetMegerHeaderService
		    .checkExistTieuDeHang(dto.getBmSheetByBmSheetId().getId());
//	    if (!isExistTieuDeHang) {
//		// Add sheet hang header
//		BmTieuDeHangDTO addTieuDeHangDTO = new BmTieuDeHangDTO();
//		addTieuDeHangDTO.setSheetId(dto.getBmSheetByBmSheetId().getId().longValue());
//		this.sheetMegerHeaderService.addOrUpdateHang(addTieuDeHangDTO);
//	    }

	    QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

	    // set IP thực hiện lấy IP remote client
	    dtoLog.setIpThucHien(Utils.getClientIp(request));
	    dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "SHEET_HANG");
	    dtoLog.setNgayTao(Utils.getCurrentDate());
	    dtoLog.setNguoiTaoId(userInfo.getId());
	    dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới hàng sheet");
	    // save db
	    WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

	    return new ResponseEntity<BmSheetHangDTO>(dto, HttpStatus.CREATED);

	} catch (Exception e) {
	    logger.error(e.getMessage());

	    throw new ApiRequestException(Constants.Messages.CREATE_FAIL,
		    HttpStatus.EXPECTATION_FAILED);

	}

    }

	/**
	 * cập nhập hàng sheet
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/hangsheet/capnhat")
	public ResponseEntity<?> updateSheet(@Valid @RequestBody BmSheetHangDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		//logger.info(Constants.Logs.UPDATE);

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		try {

			BmSheetHangDTO qt = sheetHangService.AddOrUpdateHangSheet(dto);
			if(qt == null) {
				throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);
			}
			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "SHEET_HANG");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật hàng sheet ");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmSheetHangDTO>(dto, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@GetMapping("/hangsheet/chitiet/{id}")
	public ResponseEntity<?> getByIdSheet(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			BmSheetHangDTO qt = sheetHangService.FindById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/hangsheet/maxstt/{id}")
	public ResponseEntity<?> getMaxSTTBySheetID(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			long qt = sheetHangService.maxSTTBySheetId(id);
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}


	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	@GetMapping("/hangsheet/checkexitsmahang")
	public ResponseEntity<?> getByIdChucVu(
			@RequestParam(name = "maHang", defaultValue = "") String maHang,
			@RequestParam(name = "id", defaultValue = "") Integer id,
			@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId
			) throws ApiRequestException {
		try {

			if (sheetHangService.checkExistMaHangSheet(maHang, id,sheetId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	
	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maHangLT
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	@GetMapping("/hangsheet/checkexitsmahanglienthong")
	public ResponseEntity<?> getByIdMaHangLT(
			@RequestParam(name = "maHangLT", defaultValue = "") String maHangLT,
			@RequestParam(name = "id", defaultValue = "") Integer id,
			@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId
			) throws ApiRequestException {
		try {

			if (sheetHangService.checkExistMaHangLT(maHangLT, id,sheetId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	

	 

	/**
	 * lấy danh sách các cột được thiết lập trong sheet
	 * 
	 * @param sheedId
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/hangsheet/dshangcauhinh", method = RequestMethod.GET)
	public ResponseEntity<BmSheetHangMenuBDTO> listHangSheet(
			@RequestParam(value = "sheetId", required = false) Integer sheetId,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize) throws ApiRequestException {

		BmSheetHangMenuBDTO lst = null;
		
		try {
			Integer pageNoReqest = 0;
			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNoReqest = pageNo - 1;
			}

			lst = sheetHangService.List(sheetId, pageNoReqest, pageSize);

			return new ResponseEntity<BmSheetHangMenuBDTO>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * lấy danh sách các cột được thiết lập trong sheet làm dropdown
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/hangsheet/dshangcongthuc", method = RequestMethod.GET)
	public ResponseEntity<List<DropDownDTO>> listCotSheetCongThuc(
			@RequestParam(value = "sheetId", required = false) Integer sheetId) throws ApiRequestException {

		List<DropDownDTO> lst = null;

		try {

			//logger.info(Constants.Logs.LIST);

			lst = sheetHangService.listHangDropdown(sheetId);

			return new ResponseEntity<List<DropDownDTO>>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@DeleteMapping("/hangsheet/xoa/{id}")
	public ResponseEntity<?> deleteHang(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.DELETE);

			if (sheetHangService.delete(id)) { // xoastheo id
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
	
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "SHEET_HANG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa hàng sheet");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/hangsheet/resetstt/{id}")
	public ResponseEntity<?> resetSTT(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			
			if (sheetHangService.resetSTT(id)) {
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/hangsheet/setmahang/{id}")
	public ResponseEntity<?> setMaHangBySTT(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			
			if (sheetHangService.setMaHangBySTT(id)) {
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_BM_HAS_VERSION, HttpStatus.NOT_FOUND);
		}
	}

	@SuppressWarnings("unused")
	private boolean checkKeySort(String keySort) {

//		Field fld[] = ChucVuDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

//		for (int i = 0; i < fld.length; i++) {
//			listName.add(fld[i].getName());
//		}

		return listName.contains(keySort);
	}

}

