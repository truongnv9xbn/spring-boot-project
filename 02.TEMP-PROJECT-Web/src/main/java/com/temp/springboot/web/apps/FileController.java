/**
 * 
 */
package com.temp.springboot.web.apps;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.file.UploadPathDTO;
//import com.temp.quyenDocService.QuyenXemDocService;
import com.temp.utils.Constant;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

/**
 * @author thangnn
 *
 */
@RestController
@CrossOrigin(origins = Constants.BaseUrl)
public class FileController {
	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@Autowired
	private FileStorage fileStorageService;

	@Autowired
	private ServletContext servletContext;

//	@Autowired
//	private QuyenXemDocService quyenXemDoc;

//	@Autowired
//	private ChungThuSoService ctsService;

	/**
	 * upload file tạm trên server
	 * 
	 * @param files
	 * @return
	 * @throws ApiRequestException
	 */
	@PostMapping("/uploadMultipleFiles")
	public ResponseEntity<List<UploadPathDTO>> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files,
			HttpServletRequest request) throws ApiRequestException {

		List<UploadPathDTO> lstUpload = new ArrayList<>();
		try {
			for (MultipartFile mfile : files) {
				UploadPathDTO dtoFile = fileStorageService.storeFile(mfile);

				// dtoFile.setFileDownloadUri();

				lstUpload.add(dtoFile);
			}
		} catch (ApiRequestException e) {
//			logger.error(e.getMessage());
			logger.error("uploadMultipleFiles => ApiRequestException");
			throw new ApiRequestException(e.getMessage(), HttpStatus.FOUND);
			// throw e;
		}
		return new ResponseEntity<List<UploadPathDTO>>(lstUpload, HttpStatus.CREATED);
	}

	@PostMapping("/uploadMultipleFilesImport")
	public ResponseEntity<List<UploadPathDTO>> uploadMultipleFilesImport(@RequestParam("files") MultipartFile[] files,
			HttpServletRequest request) throws ApiRequestException {

		List<UploadPathDTO> lstUpload = new ArrayList<>();
		try {
			for (MultipartFile mfile : files) {
				UploadPathDTO dtoFile = fileStorageService.storeFileImport(mfile);

				// dtoFile.setFileDownloadUri();

				lstUpload.add(dtoFile);
			}
		} catch (ApiRequestException e) {
//			logger.error(e.getMessage());
			logger.error("uploadMultipleFiles => ApiRequestException");
			throw new ApiRequestException(e.getMessage(), HttpStatus.FOUND);
			// throw e;
		}
		return new ResponseEntity<List<UploadPathDTO>>(lstUpload, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/download/{fileName:.+}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<ByteArrayResource> ReaderFile(@PathVariable("fileName") String fileName,
			HttpServletRequest request) throws IOException {
		// logger.info("download file");
		String uriTmp = request.getRequestURI().toLowerCase().replace("%20", " ");
		String uriFull = "";

		if (!StringUtils.isEmpty(uriTmp)) {
			uriFull = fileName.replace("`", "\\");
		}
		String uploadFileMQH = Constant.UPLOADNOTTEMPFOLDER + "\\" + uriFull;
		uploadFileMQH = uploadFileMQH.replace("\\api\\v1\\download", "");
		MediaType mediaType = Utils.getMediaTypeForFileName(this.servletContext, uploadFileMQH);

		Path path = Paths.get(uploadFileMQH);
		byte[] data = Files.readAllBytes(path);
		ByteArrayResource resource = new ByteArrayResource(data);

		return ResponseEntity.ok()
				// Content-Disposition
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
				// Content-Type
				.contentType(mediaType) //
				// Content-Lengh
				.contentLength(data.length) //
				.body(resource);
	}

//	@GetMapping(value = "/download/{fileName:.+}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
//	public ResponseEntity<ByteArrayResource> ReaderFile(@PathVariable("fileName") String fileName,
//			HttpServletRequest request) throws IOException {
//		logger.info("download file");
//		String uriTmp = request.getRequestURI().toLowerCase().replace("%20", " ");
//		String uriFull = "";
//
//		if (!StringUtils.isEmpty(uriTmp)) {
////			String[] tmpFull = uriTmp.split("/");
////			uriFull = tmpFull[tmpFull.length - 1].replace("%60", "\\");
//			uriFull = fileName.replace("`", "\\");
//		}
//
////		String uploadFileMQH = request.getServletContext().getRealPath("/") + uriFull;
//		String uploadFileMQH = Constant.UPLOADNOTTEMPFOLDER + "\\" + uriFull;
//		uploadFileMQH = uploadFileMQH.replace("\\api\\v1\\download", "");
//		MediaType mediaType = Utils.getMediaTypeForFileName(this.servletContext, uploadFileMQH);
//
//		Path path = Paths.get(uploadFileMQH);
////		Path path = Paths.get("C:\\SCMS_FILE\\uploadFile\\bmbaocaodinhky\\sample911202011225_signed.pdf");
////		DirectoryStream<Path> files = Files.newDirectoryStream(path);
//		byte[] data = Files.readAllBytes(path);
//		ByteArrayResource resource = new ByteArrayResource(data);
//
//		return ResponseEntity.ok()
//				// Content-Disposition
//				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
//				// Content-Type
//				.contentType(mediaType) //
//				// Content-Lengh
//				.contentLength(data.length) //
//				.body(resource);
//	}

//	@GetMapping(value = "/printing/{fileName:.+}", produces = MediaType.APPLICATION_PDF_VALUE)
//	public @ResponseBody byte[] print(@PathVariable("fileName") String fileName, HttpServletRequest request) {
//
//		try {
//			logger.info("printing file");
//			String uriTmp = request.getRequestURI().toLowerCase().replace("%20", " ");
//			String uriFull = "";
//
//			if (!StringUtils.isEmpty(uriTmp)) {
//				String[] tmpFull = uriTmp.split("/");
//				uriFull = tmpFull[tmpFull.length - 1].replace("%60", "\\");
//			}
//
////			String uploadFileMQH = request.getServletContext().getRealPath("/") + uriFull;
//			String uploadFileMQH = fileStorageService.getFileStorageLocation().toString();
//			uploadFileMQH = uploadFileMQH.replace("\\api\\v1\\download", "");
//
//			Path path = Paths.get(uploadFileMQH + "/" + uriFull);
//			byte[] data = Files.readAllBytes(path);
//			return data;
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	@GetMapping(value = "/printing/{fileName:.+}", produces = MediaType.APPLICATION_PDF_VALUE)
	public @ResponseBody byte[] print(@PathVariable("fileName") String fileName, HttpServletRequest request) {

		try {
			// logger.info("printing file");
			String uriTmp = request.getRequestURI().toLowerCase().replace("%20", " ");
			String uriFull = "";

			if (!StringUtils.isEmpty(uriTmp)) {
				String[] tmpFull = uriTmp.split("/");
				uriFull = tmpFull[tmpFull.length - 1].replace("%60", "\\");
			}
			uriFull = URLDecoder.decode(uriFull, StandardCharsets.UTF_8.name());
//			String uploadFileMQH = request.getServletContext().getRealPath("/") + uriFull;
			String uploadFileMQH = fileStorageService.getFileStorageLocation().toString();
			String uploadFileMQHTemp = fileStorageService.getFileStorageLocation().toString();
			uploadFileMQH = uploadFileMQH.replace("\\api\\v1\\download", "");
			uploadFileMQHTemp = uploadFileMQHTemp.replace("\\api\\v1\\download", "");
			Path path = null;
			byte[] data = null;
			String pathStr = Constant.UPLOADNOTTEMPFOLDER + uriFull;
			//Check file exist
			File file = new File(pathStr);
			//Neu file khong ton tai
			if(!file.exists()) { 
				//Replace path
				int index = uriFull.lastIndexOf("\\");
				String nameFile = uriFull.substring(index+1, uriFull.length());
				path = Paths.get(uploadFileMQHTemp + "/" + nameFile);
			}else {
				path = Paths.get(Constant.UPLOADNOTTEMPFOLDER + uriFull);
			}
			//Lay du lieu file
			try {
				//Hàm này lấy data file
				data = Files.readAllBytes(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return data;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@GetMapping(value = "/printingReal/{fileName:.+}", produces = MediaType.APPLICATION_PDF_VALUE)
//	@Produces({"application/xml", "application/json","application/pdf","application/vnd.ms-excel","application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"})
	public @ResponseBody byte[] printReal(@PathVariable("fileName") String fileName, HttpServletRequest request) {

		try {
			logger.info("printing file");
			String uriTmp = request.getRequestURI().toLowerCase().replace("%20", " ");
			String uriFull = "";

			if (!StringUtils.isEmpty(uriTmp)) {
				String[] tmpFull = uriTmp.split("/");
				uriFull = tmpFull[tmpFull.length - 1].replace("%60", "\\");
			}

			String uploadFileMQH = Constant.UPLOADNOTTEMPFOLDER;
//			String uploadFileMQH = fileStorageService.getFileStorageLocation().toString();
			uploadFileMQH = uploadFileMQH.replace("\\api\\v1\\download", "");

			Path path = Paths.get(uploadFileMQH + "/" + uriFull);
			byte[] data = Files.readAllBytes(path);
			return data;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/uploadFile/**", method = RequestMethod.GET)
	public void Preview(HttpServletRequest request, HttpServletResponse response) throws ApiRequestException {
		// String uriTmp = fileName.replace("%20", " ");
		// String uriFull = "";

		String uploadFilePath = request.getRequestURI() + "";
		String[] splitStringUpload = uploadFilePath.split("/uploadFile/");
		String tokenArr[] = splitStringUpload[splitStringUpload.length - 1].split("%60");
		String[] idFuncArr = uploadFilePath.split("~");
		int idFunc = 0;
		if (idFuncArr != null && idFuncArr.length > 1) {
			String idStr = idFuncArr[idFuncArr.length - 1];
			if (idStr != null && idStr != "") {
				idFunc = Integer.parseInt(idFuncArr[idFuncArr.length - 1]);
			}
		}

		// String token = tokenArr[tokenArr.length - 1];
		// check user phan quyen;

		uploadFilePath = Constant.UPLOADNOTTEMPFOLDER + "/uploadFile/" + tokenArr[0];
		try {
			uploadFilePath = java.net.URLDecoder.decode(uploadFilePath, StandardCharsets.UTF_8.name());
		} catch (UnsupportedEncodingException e) {
			// not going to happen - value came from JDK's own StandardCharsets
			throw new ApiRequestException(Constants.Messages.EXCEPTION_RULE_DOC, HttpStatus.EXPECTATION_FAILED);
		}
		File file = new File(uploadFilePath);
		if (file.exists()) {
			QtNguoiDungDTO user = UserInfoGlobal.getUserInfoAuthor();
			// check quyền

			String lowPath = uploadFilePath.toLowerCase();
			// get the mimetype

//			if (lowPath.contains("/codongmqh/")) {
//				if (!this.quyenXemDoc.XemDocMQHCoDong(idFunc, user)) {
//					throw new ApiRequestException(Constants.Messages.EXCEPTION_RULE_DOC, HttpStatus.EXPECTATION_FAILED);
//				}
//
//			} else {
//				throw new ApiRequestException(Constants.Messages.EXCEPTION_RULE_DOC, HttpStatus.EXPECTATION_FAILED);
//			}

			// Làm gì đó tại đây ...

			String mimeType = URLConnection.guessContentTypeFromName(file.getName());
			if (mimeType == null) {
				// unknown mimetype so set the mimetype to application/octet-stream
				mimeType = "application/octet-stream";
			}

			response.setContentType(mimeType);

			response.setHeader("Content-Disposition", String.format("inline; filename=\"" + file.getName() + "\""));

			response.setContentLength((int) file.length());

			try {
				InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
				FileCopyUtils.copy(inputStream, response.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	@RequestMapping(value = "/remixKySo")
	private void kySo(HttpServletRequest request, HttpServletResponse response) {
		try {
			Part filePart = request.getPart("uploadfile");
//		String tempLokation = "uploadFileTmp//";
//		Path tmpPath = Paths.get(Constants.FolderExport.FOLDER_VBKS);
//		String tmp = request.getServletContext().getRealPath("/") + tmpPath.toFile();
			String uploadFileTemp = fileStorageService.getFileStorageLocation().toString();
			File tDir = new File(uploadFileTemp);
			if (!tDir.exists()) {
				boolean resultDir = tDir.mkdirs();
			}
//		//System.out.println("Check : "+request.getServletContext().getRealPath("/") + tmpPath.toFile());

			String[] tempName = filePart.getSubmittedFileName().split("\\.");
			String fileName = tempName[0] + "_signed." + tempName[1];
			File file = new File(uploadFileTemp + "/" + fileName);

			InputStream tempIs = filePart.getInputStream();

			try (OutputStream outputStream = new FileOutputStream(file)) {
				IOUtils.copy(tempIs, outputStream);
				outputStream.flush();
				IOUtils.closeQuietly(outputStream);
				IOUtils.closeQuietly(tempIs);
			} catch (Exception e) {
				// handle exception here
			}
			File result = new File(uploadFileTemp + "/" + fileName);
			Long a = result.length() / (1024);
			String checkCks = "";
			List<Object> ctsUser = null;
//			if (Constant.IDUSER != null)
//				ctsUser = ctsService.findByUserId(Constant.IDUSER);

			if (Constant.ISKYSOUSER == true) {
//			if(tempName[1].equals("pdf")) 
//			{
				try {
					checkCks = Utils.verifyPdfSignaturesForFile(result, ctsUser);
				} catch (Exception e) {
					e.printStackTrace();
				}
//			}

			}
			if (Constant.ISKYSOUSER == true && !checkCks.isEmpty()) {
				response.setContentType("text/html; charset=UTF-8");
				response.setCharacterEncoding("UTF-8");
				PrintWriter out = response.getWriter();
				out.println("{\"Status\":true, \"Message\": \"\", \"FileName\": \"" + "" + "\", \"FileServer\": \""
						+ fileName + "||" + a.toString() + "||" + checkCks + "\"}");
				out.close();
			}
//		Utils.CommonSavePathFile(tempLokation + fileName + "~" + fileName,
//				Constants.FolderExport.FOLDER_VBKS, request);
			else {
				response.setContentType("text/html; charset=UTF-8");
				response.setCharacterEncoding("UTF-8");
				PrintWriter out = response.getWriter();
				out.println("{\"Status\":true, \"Message\": \"\", \"FileName\": \"" + "" + "\", \"FileServer\": \""
						+ fileName + ";;" + a.toString() + "\"}");
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
