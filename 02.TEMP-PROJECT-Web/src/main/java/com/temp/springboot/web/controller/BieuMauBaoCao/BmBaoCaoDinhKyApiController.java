package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DropDownDTO;
import com.temp.model.ThongTinDuLieuBcDetailBDTO;
import com.temp.model.BieuMauBaoCao.BcThanhVienJoinDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;
import com.temp.model.BieuMauBaoCao.KyBcGiaTriBcDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.BieuMauBaoCao.BmBaoCaoDinhKyService;
import com.temp.service.BieuMauBaoCao.BmBaoCaoService;
import com.temp.service.BieuMauBaoCao.SheetService;
import com.temp.utils.Constants;
import com.temp.utils.TimestampUtils;
import com.temp.utils.Utils;
import com.temp.utils.Utils.MQH;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class BmBaoCaoDinhKyApiController {

	public static final Logger logger = LoggerFactory.getLogger(BmBaoCaoDinhKyApiController.class);

	@Autowired
	@Qualifier("BmBaoCaoDinhKyServiceImp")
	private BmBaoCaoDinhKyService bcService;

	@Autowired
	@Qualifier("SheetServiceImpl")
	private SheetService sheetService;

//    @Autowired
//    @Qualifier("BcBaoCaoGtServiceImpl")
//    private BcBaoCaoGtService bcBaoCaoGtService;

	@Autowired
	@Qualifier("BmBaoCaoServiceImp")
	private BmBaoCaoService bmBaoCaoService;

	/**
	 * danh sách mqh các cổ đông công ty chứng khoans
	 * 
	 * @param strfilter
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @param sIdCTCK
	 * @param sTenCoDong
	 * @param sLoaiCoDong
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/bmbaocaodinhky/filter", method = RequestMethod.GET)
	public ResponseEntity<BmBaoCaoDinhKyBDTO> listCoDongMQH(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenCTCK", required = false) Integer sIdCTCK,
			@RequestParam(value = "bmBaoCaoId", required = false) Integer bmBaoCaoId,
			@RequestParam(value = "sCMND", required = false) String sCMND) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		try {

			// logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			BmBaoCaoDinhKyBDTO lstCoDong = bcService.listBmBaoCaoDinhKy(strfilter, sIdCTCK, bmBaoCaoId, sCMND, pageNo,
					pageSize, keySort, desc);

			return new ResponseEntity<BmBaoCaoDinhKyBDTO>(lstCoDong, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Create mqh cổ đông
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/bmbaocaodinhky/themmoi")
	public ResponseEntity<?> createBmBaoCaoDinhKy(@Valid @RequestBody BmBaoCaoDinhKyDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		// logger.info(Constants.Logs.CREATE);

		if (bcService.isExistAdd(dto)) {
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			dto.setNguoiTaoId(userInfo.getId());
			try {
//				if (!StringUtils.isEmpty(dto.getFileDinhKem())) {
//					dto.setFileDinhKem(
//							Utils.CommonSavePathFile(dto.getFileDinhKem(), Constants.FolderUpload.FOLDER_BMBAOCAO, request));
//
//				}

				bcService.addorUpdateBmBaoCaoDinhKy(dto);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_BMBAOCAO");
				dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới biểu mẫu báo cáo định kỳ "
						+ dto.getTenBmBaoCao());
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<BmBaoCaoDinhKyDTO>(dto, HttpStatus.CREATED);

			} catch (Exception e) {
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	@PutMapping("/bmbaocaodinhky/capnhat")
	public ResponseEntity<?> updateBmBaoCaoDinhKy(@Valid @RequestBody BmBaoCaoDinhKyDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {

		// logger.info(Constants.Logs.UPDATE);

		if (bcService.isExistUpdate(dto)) {
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			try {
				this.bcService.findById(dto.getId());

				bcService.addorUpdateBmBaoCaoDinhKy(dto);

//				if (!StringUtils.isEmpty(oldObject.getFileDinhKem())) {
//					// lấy các path của old và new
//					String[] pathOld = oldObject.getFileDinhKem().split(",");
//					String[] pathNew = newObject.getFileDinhKem().split(",");
//					List<String> oldList = null;
//					List<String> NewList = null;
//
//					if (pathOld.length > 0) {
//						oldList = Arrays.asList(pathOld);
//					}
//					if (pathNew.length > 0) {
//						NewList = Arrays.asList(pathNew);
//					}
//					// biến chứa path cần remove
//					List<String> pathRemove = new ArrayList<>();
//					// check các trường hợp để add vào pathRemove
//					if (oldList != null && NewList == null) {
//						pathRemove.addAll(oldList);
//					} else if (oldList != null && NewList != null) {
//						for (String path : oldList) {
//							if (!StringUtils.isEmpty(path) && !NewList.contains(path)) {
//								pathRemove.add(path);
//							}
//						}
//					}
//					// kiểm tra nếu path cần xóa thì xóa
//					if (!pathRemove.isEmpty()) {
//						for (String pathx : pathRemove) {
//							Utils.DeleteFile(Constant.UPLOADNOTTEMPFOLDER + pathx);
//						}
//					}
//
//				}

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_BMBAOCAO");
				dtoLog.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật biểu mẫu báo cáo định kỳ "
						+ dto.getTenBmBaoCao());
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<BmBaoCaoDinhKyDTO>(dto, HttpStatus.ACCEPTED);
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	@DeleteMapping("/bmbaocaodinhky/xoa/{id}")
	public ResponseEntity<?> deleteBmBaoCaoDinhKy(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			// logger.info(Constants.Logs.DELETE);
			if (bcService.isExistById(id)) {
				String tenBaoCao = bcService.findById(id).getTenBmBaoCao();
				if (bcService.deleteById(id)) {
					QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
					Timestamp timeStamp = new Timestamp(new Date().getTime());
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_BMBAOCAO");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung(
							"Tài khoản " + userInfo.getTaiKhoan() + " xóa biểu mẫu báo cáo định kỳ " + tenBaoCao);
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

					return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
				}
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/bmbaocaodinhky/chitiet/{id}")
	public ResponseEntity<?> getByIdBmBaoCaoDinhKy(@PathVariable("id") int id) throws ApiRequestException {
		try {
			// logger.info(Constants.Logs.GETBYID);
			BmBaoCaoDinhKyDTO qt = bcService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
//			if (qt.getNgayCap() != null) {
//				qt.setNgayCapStr(TimestampUtils.TimestampToString_ddMMyyyy(qt.getNgayCap()));
//			}

			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/bmbaocaodinhky/mqhdropdown")
	public ResponseEntity<?> getBmBaoCaoDinhKyDropdown() throws ApiRequestException {
		try {
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

			if (!userInfo.getAdmin() || userInfo.getTaiKhoan().toLowerCase().equals("administrator")) {
				List<DropDownDTO> lstDropdown = new ArrayList<>();
				List<MQH> lstEnum = Utils.MQH.initMapping();
				if (lstEnum != null && !lstEnum.isEmpty()) {
					for (MQH mqh : lstEnum) {
						lstDropdown.add(new DropDownDTO(mqh.getValue(), mqh.getText()));
					}
				}

				return ResponseEntity.ok().body(lstDropdown);
			} else {
				throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/bmbaocaodinhky/kybaocaodropdown", method = RequestMethod.GET)
	public ResponseEntity<List<DropDownDTO>> getBaoCaoDinhKyFromBcDropdown(
			@RequestParam(value = "bieumauId", required = false) int bieumauId) throws ApiRequestException {
		try {
			List<DropDownDTO> lstDropdown = new ArrayList<DropDownDTO>();
			lstDropdown = this.bcService.getBaoCaoDinhKyFromBcDropdownService(bieumauId);
			return ResponseEntity.ok().body(lstDropdown);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Thiet lap bao cao sheet de nhap du lieu
	 * 
	 * @param sheetId    - sheet bao cao id
	 * @param kyBaoCaoId - ky bao cao id
	 * @param baoCaoId-  bao cao id
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping("/bmbaocaodinhky/thietlapnoidungbaocaosheet/{baocaoId}")
	public ResponseEntity<ThongTinDuLieuBcDetailBDTO> getThongTinBaoCaoDetail(
			@PathVariable("baocaoId") Integer baocaoId) throws ApiRequestException {
		try {

			ThongTinDuLieuBcDetailBDTO thongTinDuLieuDetail = new ThongTinDuLieuBcDetailBDTO();

			// logger.info(Constants.Logs.GETBYID);

			// Get list sheet id.
			List<DropDownDTO> lsSheetIds = this.sheetService.getlsSheetbyBaoCaoService(baocaoId);
			if (lsSheetIds.size() > 0) {
				BmSheetDTO firstSheet = this.sheetService.getNoiDungBaoCaoSheetId(lsSheetIds.get(0).getValue(), "", 0,
						0);
				if (firstSheet != null) {
					thongTinDuLieuDetail.setBmSheetDto(firstSheet);
				}
				thongTinDuLieuDetail.setLsSheetNameAndId(lsSheetIds);
			}

			// Get Bm Bao Cao
			BmBaoCaoDTO bmBaoCaoDetail = this.bmBaoCaoService.findById(baocaoId);
			if (bmBaoCaoDetail != null) {
				thongTinDuLieuDetail.setBmBaoCaoDTO(bmBaoCaoDetail);
			}

			// Get thong tin Thanh vien, ky gui bao cao, ten don vi gui & ngay gui.
//			BcThanhVienJoinDTO bcThanhVienJoin = this.bcThanhVienService.getThongTinThanhVienGuiBaoCao(baocaoId, 0);
//			if (bcThanhVienJoin != null) {
//				thongTinDuLieuDetail.setBcThanhVienJoinDto(bcThanhVienJoin);
//			}
			// Response result.
			return ResponseEntity.ok().body(thongTinDuLieuDetail);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Thiet lap bao cao sheet de nhap du lieu
	 * 
	 * @param sheetId    - sheet bao cao id
	 * @param kyBaoCaoId - ky bao cao id
	 * @param baoCaoId-  bao cao id
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping("/bmbaocaodinhky/giatribaocaochitiet")
	public ResponseEntity<BmSheetDTO> getGiaTriBaoCaoChiTiet(
			@RequestParam(value = "sheetId", required = false) Integer sheetId,
			@RequestParam(value = "kyBaoCaoId", required = false) Integer kyBaoCaoId,
			@RequestParam(value = "baoCaoId", required = false) Integer baoCaoId) throws ApiRequestException {
		try {
			// logger.info(Constants.Logs.GETBYID);
			BmSheetDTO sheetValueDTO = this.sheetService.getGiaTriBaoCaoSheetId(sheetId, kyBaoCaoId, baoCaoId);
			if (sheetValueDTO == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(sheetValueDTO);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/bmbaocaodinhky/getlstkybaocao", method = RequestMethod.GET)
	public @ResponseBody ResponseEntity<Map<String, List<String>>> getLstKyBaoCao(
			@RequestParam(value = "lstId", required = false) String lstCty) throws ApiRequestException {
		try {
			String[] temp = lstCty.split(",");
			List<Integer> lstCtyInt = new ArrayList<Integer>();
			for (int index = 0; index < temp.length; index++)
				lstCtyInt.add(Integer.parseInt(temp[index]));

			List<KyBcGiaTriBcDTO> lstKyBaoCao = bcService.getListKyBaoCaoFromBcThanhVien(lstCtyInt);
			Map<String, List<String>> result = new LinkedHashMap<>();
			Map.Entry<String, List<String>> entry;
			Iterator<Map.Entry<String, List<String>>> it;
			for (KyBcGiaTriBcDTO a : lstKyBaoCao) {
				if (result != null && result.size() == 0) {
					List<String> tempLst = new ArrayList<String>();
					tempLst.add(a.getGiaTriKyBc());
					result.put(a.getKyBaoCao(), tempLst);
				} else {
					boolean check = false;
					it = result.entrySet().iterator();
					while (it.hasNext()) {
						entry = it.next();
						if (entry.getKey().equals(a.getKyBaoCao())) {
							entry.getValue().add(a.getGiaTriKyBc());
							check = true;
						}
					}
					if (!check) {
						List<String> tempLst = new ArrayList<String>();
						tempLst.add(a.getGiaTriKyBc());
						result.put(a.getKyBaoCao(), tempLst);
					}
				}
			}
			return ResponseEntity.ok().body(result);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = BmBaoCaoDinhKyDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}