//package com.temp.springboot.web.controller.NguoiDung;
//
//import java.lang.reflect.Field;
//import java.sql.Timestamp;
//import java.text.ParseException;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.validation.Valid;
//import javax.ws.rs.core.Context;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.DeleteMapping;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.PutMapping;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.temp.global.UserInfoGlobal;
//import com.temp.global.WriteLogInDataBaseGlobal;
//import com.temp.global.exception.ApiRequestException;
//import com.temp.model.NguoiDung.ChungThuSoBDTO;
//import com.temp.model.NguoiDung.ChungThuSoDTO;
//import com.temp.model.NguoiDung.QtLogHeThongDTO;
//import com.temp.model.NguoiDung.QtNguoiDungDTO;
//import com.temp.persistance.entity.NguoiDung.ChungThuSoEntity;
//import com.temp.service.Common.ThongBaoService;
//import com.temp.service.NguoiDung.ChungThuSoService;
//import com.temp.utils.Constants;
//import com.temp.utils.Utils;
//
//@CrossOrigin(origins = Utils.BaseUrl)
//@RestController
//@RequestMapping("/api/v1")
//public class ChungThuSoApiController {
//
//	public static final Logger logger = LoggerFactory.getLogger(ChungThuSoApiController.class);
//
//	@Autowired
//	@Qualifier("ChungThuSoServiceImpl")
//	private ChungThuSoService ctsService;
//	
//	@Autowired
//	@Qualifier("ThongBaoServiceImpl")
//	private ThongBaoService thongBaoService;
//
//	@RequestMapping(value = "/chungthuso/filter", method = RequestMethod.GET)
//	public ResponseEntity<ChungThuSoBDTO> listDichVus(
//			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
////			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "sTenTaiKhoan", required = false) String taiKhoan,
//			@RequestParam(name = "trangThai", required = false) Integer trangThai,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(name = "tenCongTy", required = false) String tenCongTy) throws ApiRequestException {
//
////		if (!checkKeySort(keySort)) {
////
////			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
////		}
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//
//			ChungThuSoBDTO lstCts = ctsService.list(pageNo, pageSize, desc, strfilter, taiKhoan, trangThai);
//
//			return new ResponseEntity<ChungThuSoBDTO>(lstCts, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}
//
//	@GetMapping("/chungthuso/chitiet/{id}") // lấy theo id
//	public ResponseEntity<?> getChungThuSoById(@PathVariable("id") int id) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			ChungThuSoDTO cts = ctsService.findById(id);
//			if (cts == null) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
//			}
//			return ResponseEntity.ok().body(cts);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}
//
//	@PutMapping("/chungthuso/capnhat")
//	public ResponseEntity<?> updateChungThuSo(@RequestBody ChungThuSoDTO dto, @Context HttpServletRequest request)
//			throws ApiRequestException {
//		// lấy thông tin user
//		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//		Timestamp timeStamp = new Timestamp(new Date().getTime());
//		try {
//			logger.info(Constants.Logs.UPDATE);
//
//			if (ctsService.isExistById(dto.getId())) {
//				ctsService.changeStatus(dto.getId(), dto.getTrangThai());
//
//				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//				// set IP thực hiện lấy IP remote client
//				dtoLog.setIpThucHien(Utils.getClientIp(request));
//				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_CHUNG_THU_SO");
//				dtoLog.setNgayTao(timeStamp);
//				dtoLog.setNguoiTaoId(userInfo.getId());
//				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật chứng thư số");
//				// save db
//				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//
//				return new ResponseEntity<ChungThuSoDTO>(dto, HttpStatus.ACCEPTED);
//
//			} else {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//			}
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);
//
//		}
//
//	}
//
//	@DeleteMapping("/chungthuso/{id}")
//	public ResponseEntity<?> deleteCts(@PathVariable("id") int id, @Context HttpServletRequest request)
//			throws ApiRequestException {
//		// lấy thông tin user
//		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//		Timestamp timeStamp = new Timestamp(new Date().getTime());
//		try {
//			logger.info(Constants.Logs.DELETE);
//
//			if (!ctsService.isExistById(id)) {
//				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
//						HttpStatus.NOT_FOUND);
//			}
//			if (ctsService.deleteById(id)) {
//
//				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//				// set IP thực hiện lấy IP remote client
//				dtoLog.setIpThucHien(Utils.getClientIp(request));
//				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUNG_THU_SO");
//				dtoLog.setNgayTao(timeStamp);
//				dtoLog.setNguoiTaoId(userInfo.getId());
//				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa chứng thư số ");
//				// save db
//				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//
//				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
//			}
//			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}
//
//	@PostMapping("/chungthuso/themmoi")
//	public ResponseEntity<?> createCts(@Valid @RequestBody ChungThuSoDTO dto, @Context HttpServletRequest request)
//			throws ApiRequestException, ParseException {
//		// lấy thông tin user
//		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//		Timestamp timeStamp = new Timestamp(new Date().getTime());
//
//		logger.info(Constants.Logs.CREATE);
////		this.ConvertData(dto);
//		if (dto.getId() == null || !ctsService.isExistById(dto.getId())) {
//			String validateStr = this.validateData(dto);
//			String result = Constants.Messages.CREATE_FAIL;
//			if (validateStr.equals("")) {
//				try {
//					if (ctsService.checkExisted(dto) == true) {
//						result = "Dữ liệu đã tồn tại trên hệ thống";
//						throw new ApiRequestException("Dữ liệu đã tồn tại trên hệ thống",
//								HttpStatus.FOUND);
//					} else {
//						ChungThuSoEntity resultEntity = ctsService.saveCts(dto);
////						if(resultEntity != null && resultEntity.getId() != null)
////							this.addThongBao(resultEntity);
//						
//						QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//						// set IP thực hiện lấy IP remote client
//						dtoLog.setIpThucHien(Utils.getClientIp(request));
//						dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUNG_THU_SO");
//						dtoLog.setNgayTao(timeStamp);
//						dtoLog.setNguoiTaoId(userInfo.getId());
//						dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới chứng thư số ");
//						// save db
//						WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//					}
//				} catch (Exception e) {
//					logger.error(e.getMessage());
//					throw new ApiRequestException(result, HttpStatus.FOUND);
//				}
//				return new ResponseEntity<ChungThuSoDTO>(dto, HttpStatus.CREATED);
//			} else {
//				throw new ApiRequestException(validateStr, HttpStatus.EXPECTATION_FAILED);
//			}
//
//		} else {
//			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
//		}
//
//	}
//
//	private String validateData(ChungThuSoDTO dto) {
//		String err = "";
//		@SuppressWarnings("unused")
//		Date curDate = new Date();
//		try {
////			if (dto.getObjCtckHsLichSu() != null) {
////				if (dto.getObjCtckHsLichSu().getNgayHopLeStr() != null&& (!"".endsWith(dto.getObjCtckHsLichSu().getNgayHopLeStr()))) {
////					Date ngayHopLeDate = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getObjCtckHsLichSu().getNgayHopLeStr());
////					if (ngayHopLeDate.compareTo(curDate) > 0) {
////						err += "Ngày hợp lệ không được lớn hơn ngày hiện tại </br>";
////					}
////				}
////				if (dto.getObjCtckHsLichSu().getNgayChapThuanStr() != null&& (!"".endsWith(dto.getObjCtckHsLichSu().getNgayChapThuanStr()))) {
////					Date ngayChapThuanDate = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getObjCtckHsLichSu().getNgayChapThuanStr());
////					if (ngayChapThuanDate.compareTo(curDate) > 0) {
////						err += "Ngày chấp thuận không được lớn hơn ngày hiện tại </br>";
////					}
////				}
////				if (dto.getObjCtckHsLichSu().getNgayHopLeStr() != null&& (!"".endsWith(dto.getObjCtckHsLichSu().getNgayHopLeStr())) && dto.getObjCtckHsLichSu().getNgayChapThuanStr() != null&& (!"".endsWith(dto.getObjCtckHsLichSu().getNgayChapThuanStr()))) {
////					Date ngayHopLeDate = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getObjCtckHsLichSu().getNgayHopLeStr());
////					Date ngayChapThuanDate = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getObjCtckHsLichSu().getNgayChapThuanStr());
////					if (ngayHopLeDate.compareTo(ngayChapThuanDate) > 0) {
////						err += "Ngày hợp lệ không được lớn hơn ngày chấp thuận </br>";
////					}
////				}
////				if (dto.getObjCtckHsLichSu().getNgayBatDauStr() != null&& (!"".endsWith(dto.getObjCtckHsLichSu().getNgayBatDauStr())) && dto.getObjCtckHsLichSu().getNgayKetThucStr() != null&& (!"".endsWith(dto.getObjCtckHsLichSu().getNgayKetThucStr()))) {
////					Date ngayBDDate = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getObjCtckHsLichSu().getNgayBatDauStr());
////					Date ngayKTDate = new SimpleDateFormat("dd/MM/yyyy").parse(dto.getObjCtckHsLichSu().getNgayKetThucStr());
////					if (ngayBDDate.compareTo(ngayKTDate) > 0) {
////						err += "Ngày bắt đầu không được lớn hơn ngày kết thúc </br>";
////					}
////				}
////			}
//			return err;
//		} catch (Exception e) {
//			// TODO: handle exception
//			return "Kiêm tra lại dữ liệu nhập vào!";
//		}
//	}
//
//	@SuppressWarnings("unused")
//	private boolean checkKeySort(String keySort) {
//
//		Field fld[] = ChungThuSoDTO.class.getDeclaredFields();
//		List<String> listName = new ArrayList<String>();
//
//		for (int i = 0; i < fld.length; i++) {
//			listName.add(fld[i].getName());
//		}
//
//		return listName.contains(keySort);
//	}
//	
////	private boolean addThongBao(ChungThuSoEntity resultEntity) {
////		String result = "";
////		result = "Tài khoản " + resultEntity.getTaiKhoan() + " đã đăng ký thành công [Chứng thư số] chờ UBCK phê duyệt";
////		
////			Set<Integer> nguoiNhan = new HashSet<Integer>();
//////			QtNguoiDungBDTO tempBDTO = new QtNguoiDungBDTO();
////			List<LkNguoiDungCtckDTO> temp = new ArrayList<LkNguoiDungCtckDTO>();
////			if (resultEntity.getNguoiDungId() != null) {
////				temp = lkNdCtckDao.getLstNguoiDungFromCtckChuaUserId(resultEntity.getNguoiDungId());
////			}
////
////			if (temp != null && temp.size() > 0) {
////				for (LkNguoiDungCtckDTO tempDto : temp)
////					nguoiNhan.add(tempDto.getNguoiDungId());
////			}
////			if (nguoiNhan.size() > 0)
////				return thongBaoService.addThongBao(result, result, "chungThuSoList", nguoiNhan);
////		
////
////		return false;
////	}
//
//}
