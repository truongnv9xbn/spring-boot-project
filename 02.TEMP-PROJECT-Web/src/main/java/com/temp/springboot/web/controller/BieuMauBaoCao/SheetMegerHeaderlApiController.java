package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.BieuMauBaoCao.BmSheetMegerCellDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangCotDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.dropdown.DropdownMegerCell;
import com.temp.service.BieuMauBaoCao.SheetCotService;
import com.temp.service.BieuMauBaoCao.SheetMegerHeaderService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class SheetMegerHeaderlApiController {

	public static final Logger logger = LoggerFactory.getLogger(SheetMegerHeaderlApiController.class);

	@Autowired
	@Qualifier("SheetMegerHeaderServiceImpl")
	private SheetMegerHeaderService sheetMegerHeaderService;

	@Autowired
	@Qualifier("SheetCotServiceImpl")
	private SheetCotService sheetCotService;

	/**
	 * dropdown hàng
	 * 
	 * @param sheetId
	 * @return
	 * @throws ApiRequestException
	 */
	@GetMapping("/sheetmegerheader/dropdownhang")
	public ResponseEntity<?> getDropDownHang(@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			List<DropdownMegerCell> qt = sheetMegerHeaderService.DropDownHang(sheetId);
			if (qt == null) {
//				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * dropdown cột
	 * 
	 * @param sheetId
	 * @return
	 * @throws ApiRequestException
	 */
	@GetMapping("/sheetmegerheader/dropdowncot")
	public ResponseEntity<?> getDropDownCot(@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			List<DropdownMegerCell> qt = this.sheetCotService.DropDownMegerHangCot(sheetId);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Create sheet
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@GetMapping("/sheetmegerheader/initdisplay")
	public ResponseEntity<?> initMegerCell(@RequestParam(name = "sheetId", defaultValue = "0") Integer sheetId,
			@Context HttpServletRequest request) throws ApiRequestException {

		//logger.info(Constants.Logs.CREATE);

		try {

			BmSheetMegerCellDTO s = sheetMegerHeaderService.InitDisplayCauHinhMegerHangCot(sheetId);

			return new ResponseEntity<BmSheetMegerCellDTO>(s, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * Create sheet
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/sheetmegerheader/themmoihang")
	public ResponseEntity<?> createRowsHeader(@Valid @RequestBody BmTieuDeHangDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {

		//logger.info(Constants.Logs.CREATE);

		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

		try {

			sheetMegerHeaderService.addOrUpdateHang(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_HANGSHEET");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới cột hàng sheet");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmTieuDeHangDTO>(dto, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * cập nhập sheet
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/sheetmegerheader/themmoicothang")
	public ResponseEntity<?> CreateHeaderMergerCell(@Valid @RequestBody BmTieuDeHangCotDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		//logger.info(Constants.Logs.UPDATE);

		this.validateCheckParrm(dto);

		int fisrtRow = 0;
		int lastRow = 0;
		int fisrtColumn = 0;
		int lastColumn = 0;
		if (dto.getFisrtRow() != null) {
			fisrtRow = dto.getFisrtRow().intValue();
		}
		if (dto.getLastRow() != null) {
			lastRow = dto.getLastRow().intValue();
		}
		if (dto.getFisrtColumn() != null) {
			fisrtColumn = dto.getFisrtColumn().intValue();
		}
		if (dto.getLastColumn() != null) {
			lastColumn = dto.getLastColumn().intValue();
		}

		if (this.sheetMegerHeaderService.checkExistAddMeger(dto.getSheetId(), fisrtRow, lastRow, fisrtColumn,
				lastColumn)) {
			throw new ApiRequestException(Constants.Messages.MEGER_CELL_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		try {

			this.sheetMegerHeaderService.addOrUpdateHangCot(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_MegerHeaderTable");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " merger cột hàng sheet ");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmTieuDeHangCotDTO>(dto, HttpStatus.ACCEPTED);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	private void validateCheckParrm(BmTieuDeHangCotDTO dto) throws ApiRequestException {
		if (dto.getFisrtRow() == null) {
			throw new ApiRequestException("Hàng đầu tiên không được để trống", HttpStatus.EXPECTATION_FAILED);
		}
		if (dto.getFisrtRow() != null && dto.getFisrtColumn() == null) {
			throw new ApiRequestException("Cột đầu tiên không được để trống", HttpStatus.EXPECTATION_FAILED);
		}
	}

	@DeleteMapping("/sheetmegerheader/xoarow/{tieudehangid}")
	public ResponseEntity<?> deleteRows(@PathVariable("tieudehangid") int tieuDeHangId,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.DELETE);

			if (tieuDeHangId > 0) {

				this.sheetMegerHeaderService.DeleteRows(tieuDeHangId);

				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CAUHINH_MEGER_CELL");
				dtoLog.setNgayTao(Utils.getCurrentDate());
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa cấu hình merger cell");
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping("/sheetmegerheader/xoatieudehangcot/{tieudehcid}")
	public ResponseEntity<?> deleteTieuDeHangCot(@PathVariable("tieudehcid") int tieuDeHangId,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.DELETE);

			if (tieuDeHangId > 0) {

				this.sheetMegerHeaderService.DeleteRowHangCot(tieuDeHangId);

				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CAUHINH_MEGER_CELL");
				dtoLog.setNgayTao(Utils.getCurrentDate());
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa cấu hình merger cell");
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

}

//------------------- Delete All Users-----------------------------

//	@RequestMapping(value = "/chucvu/", method = RequestMethod.DELETE)
//	@DeleteMapping("/user/")
//	public ResponseEntity<UserBDTO> deleteAllUsers() {
//		//logger.info("Deleting All Users");
//		try {
//			userService.deleteAllUsers();
//			return new ResponseEntity<UserBDTO>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity(new ApiRequestException("Unable to delete."), HttpStatus.NOT_FOUND);
//		}
//
//	}
