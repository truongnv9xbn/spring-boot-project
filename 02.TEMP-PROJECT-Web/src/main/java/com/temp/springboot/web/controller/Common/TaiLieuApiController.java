package com.temp.springboot.web.controller.Common;

import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.Common.TaiLieuBDTO;
import com.temp.model.Common.TaiLieuDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.Common.TaiLieuService;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.Constant;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class TaiLieuApiController {

	public static final Logger logger = LoggerFactory.getLogger(TaiLieuApiController.class);
	@Autowired
	@Qualifier("TaiLieuServiceImpl")
	private TaiLieuService cvService;

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService cvServiceLog;

	@Autowired
	private ServletContext servletContext;

	@RequestMapping(value = "/tailieu/list", method = RequestMethod.GET)
	public ResponseEntity<TaiLieuBDTO> getList(
			@Valid @RequestParam(value = "keysearch", required = false) String keysearch,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sSoVanBan", required = false) String sSoVanBan,
			@RequestParam(value = "sTrichYeu", required = false) String sTrichYeu,
			@RequestParam(value = "sNguoiTao", required = false) String sNguoiTao,
			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {

		if (!checkKeySort(keySort)) {
			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		TaiLieuBDTO lstTaiLieu = new TaiLieuBDTO();

		try {
			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			lstTaiLieu = cvService.getList(keysearch, sSoVanBan, sTrichYeu, sNguoiTao, trangThai, pageNo, pageSize,
					keySort, desc);

			return new ResponseEntity<TaiLieuBDTO>(lstTaiLieu, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

//	@RequestMapping(value = "/tailieu/list", method = RequestMethod.GET)
//	public ResponseEntity<TaiLieuBDTO> getList(
//			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "sTenTaiLieu", required = false) String sTenTaiLieu,
//			@RequestParam(value = "sMaTaiLieu", required = false) String sMaTaiLieu,
//			@RequestParam(value = "trangThai", required = false) String trangThai) throws ApiRequestException {
//
//		if (!checkKeySort(keySort)) {
//
//			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//
//		TaiLieuBDTO lstTaiLieu = new TaiLieuBDTO();
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//			if (trangThai != null && trangThai.trim().toLowerCase() == "Sử Dụng") {
//				trangThai = "1";
//			} else if (trangThai != null && trangThai.trim().toLowerCase() == "Không Sử dụng") {
//				trangThai = "0";
//			}
//
//			lstTaiLieu = cvService.listTaiLieus(strfilter, sTenTaiLieu, sMaTaiLieu, pageNo, pageSize, keySort,
//					desc, trangThai);
//
//			return new ResponseEntity<TaiLieuBDTO>(lstTaiLieu, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}

	@PostMapping("/tailieu/add")
	public ResponseEntity<?> add(@Valid @RequestBody TaiLieuDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.CREATE);

		if (!cvService.isExistTaiLieuAdd(dto)) {
			// lấy thông tin user
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			dto.setNguoiTao(userInfo.getHoTen());
			try {
				cvService.saveTaiLieu(dto);

				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_TAI_LIEU");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới tài liệu");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<TaiLieuDTO>(dto, HttpStatus.CREATED);

			} catch (Exception e) {
				logger.error(e.getMessage());

				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	@PutMapping("/tailieu/update")
	public ResponseEntity<?> update(@Valid @RequestBody TaiLieuDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		logger.info(Constants.Logs.UPDATE);

		if (cvService.isExistById(dto.getId())) {
			if (!cvService.isExistTaiLieuUpdate(dto)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				try {
					cvService.updateTaiLieu(dto);

					Timestamp timeStamp = new Timestamp(new Date().getTime());
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_TAI_LIEU");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật tài liệu");
					// save db
					cvServiceLog.AddLogHeThong(dtoLog);

					return new ResponseEntity<TaiLieuDTO>(dto, HttpStatus.ACCEPTED);
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}

			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

	}

	@DeleteMapping("/tailieu/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);

			if (!cvService.isExistById(id)) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);

			}
			if (cvService.deleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_TAI_LIEU");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa tài liệu");
				// save db
				cvServiceLog.AddLogHeThong(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/tailieu/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			TaiLieuDTO qt = cvService.findById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/tailieu/download/{fileName:.+}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	public ResponseEntity<ByteArrayResource> download(@PathVariable("fileName") String fileName,
			HttpServletRequest request) throws IOException {
		logger.info("download file");
		String uriTmp = request.getRequestURI().toLowerCase().replace("%20", " ");
		String uriFull = "";

		if (!StringUtils.isEmpty(uriTmp)) {
//			String[] tmpFull = uriTmp.split("/");
//			uriFull = tmpFull[tmpFull.length - 1].replace("%60", "\\");
			uriFull = fileName.replace("`", "\\");
		}

//		String uploadFileMQH = request.getServletContext().getRealPath("/") + uriFull;
		String uploadFileMQH = Constant.UPLOADNOTTEMPFOLDER + "\\" + uriFull;
		uploadFileMQH = uploadFileMQH.replace("\\api\\v1\\download", "");
		MediaType mediaType = Utils.getMediaTypeForFileName(this.servletContext, uploadFileMQH);

		Path path = Paths.get(uploadFileMQH);
//		Path path = Paths.get("C:\\SCMS_FILE\\uploadFile\\bmbaocaodinhky\\sample911202011225_signed.pdf");
//		DirectoryStream<Path> files = Files.newDirectoryStream(path);
		byte[] data = Files.readAllBytes(path);
		ByteArrayResource resource = new ByteArrayResource(data);

		return ResponseEntity.ok()
				// Content-Disposition
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + path.getFileName().toString())
				// Content-Type
				.contentType(mediaType) //
				// Content-Lengh
				.contentLength(data.length) //
				.body(resource);
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = TaiLieuDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}

//	@RequestMapping(value = "/tailieu/exitsMa", method = RequestMethod.GET)
//	public ResponseEntity<Boolean> checkExitsByMa(@RequestParam(value = "maTaiLieu", required = false) String maTaiLieu,
//			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
//		Boolean result = false;
//
//		try {
//			logger.info(Constants.Logs.EXITS);
//
//			result = cvService.isExistByMa(maTaiLieu, id);
//
//			return new ResponseEntity<Boolean>(result, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}
}