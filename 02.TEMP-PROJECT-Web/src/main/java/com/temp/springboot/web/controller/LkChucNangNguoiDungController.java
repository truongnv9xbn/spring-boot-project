package com.temp.springboot.web.controller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.ChucNangDTO;
import com.temp.model.NguoiDung.LkChucNangNguoiBDTO;
import com.temp.service.NguoiDung.LkChucNangNguoiService;
import com.temp.utils.Constants;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class LkChucNangNguoiDungController {

	public static final Logger logger = LoggerFactory.getLogger(LkChucNangNguoiDungController.class);

	@Autowired
	private LkChucNangNguoiService lkChucNangNguoiService;
	
	@RequestMapping(value = "/quyen/filter", method = RequestMethod.GET)
	public ResponseEntity<LkChucNangNguoiBDTO> listQuyen(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "idNguoiDung", required = true) int idNguoiDung) {
		LkChucNangNguoiBDTO bdto = new LkChucNangNguoiBDTO();
		logger.info(Constants.Logs.LIST);
		try {

		if (!checkKeySort(keySort)) {
			
			bdto.setMessage("truyền sai keySort");
			return new ResponseEntity<LkChucNangNguoiBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

			bdto = lkChucNangNguoiService.listChucNangs(strfilter, pageNo, pageSize, keySort, desc, idNguoiDung);
			
			return new ResponseEntity<LkChucNangNguoiBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<LkChucNangNguoiBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Create Chức năng
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json 
	 */
	@PostMapping("/quyen/")
	public ResponseEntity<?> updateQuyen(@Valid @RequestBody LkChucNangNguoiBDTO bdto) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.CREATE);

			if (bdto.getListDto()!=null) {
				lkChucNangNguoiService.saveChucNang(bdto);

				return new ResponseEntity<LkChucNangNguoiBDTO>(bdto, HttpStatus.CREATED);
			}

			return new ResponseEntity<Object>(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	 

	/**
	 * Xóa 1 Chức năng
	 * 
	 * @param id của Chức năng
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/quyen/{id}")
	public ResponseEntity<?> deleteChucNang(@PathVariable("idNguoiDung") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);
			lkChucNangNguoiService.deleteById(id);
			return new ResponseEntity<Object>(Constants.Messages.CREATE_SUCCESS, HttpStatus.OK);
			 
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}
	
	

	
/** Validate keySort param
 * 
 * @param keySort
 * @return true or false
 */
	private boolean checkKeySort(String keySort) {

		Field fld[] = ChucNangDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}