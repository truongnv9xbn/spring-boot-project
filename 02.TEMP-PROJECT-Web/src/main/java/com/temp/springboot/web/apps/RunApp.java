package com.temp.springboot.web.apps;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.temp.authencite.config.FileStorageProperties;
import com.temp.presistance.configuration.JpaConfiguration;
import com.temp.soapservice.WSConfig;
import com.temp.utils.Constants;
import com.temp.utils.FilePropertiesGlobal;

//

@Configuration
@Import({ JpaConfiguration.class,WSConfig.class })

@ComponentScan(basePackages = { "com.temp" })
@EnableAutoConfiguration
@EnableConfigurationProperties({ FileStorageProperties.class })
@EnableScheduling
public class RunApp {

	public static void main(String[] args) {

		SpringApplication.run(RunApp.class, args);
			
	}

	// cross config
	@Bean
	public FilterRegistrationBean simpleCorsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		// *** URL below needs to match the Vue client URL and port Constants.BaseUrl
		// ***
		List<String> AllowedOriginsList = new ArrayList<>();
		AllowedOriginsList.add(FilePropertiesGlobal.readPropertiesFile().getProperty(Constants.KEY_BASE_VUE));
		AllowedOriginsList.add(FilePropertiesGlobal.readPropertiesFile().getProperty(Constants.KEY_BASE_GOOGLE));
		config.setAllowedOrigins(AllowedOriginsList);
		config.setAllowedMethods(Collections.singletonList("*"));
		config.setAllowedHeaders(Collections.singletonList("*"));
		source.registerCorsConfiguration("/**", config);
		FilterRegistrationBean bean = new FilterRegistrationBean<>(new CorsFilter(source));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}

}
