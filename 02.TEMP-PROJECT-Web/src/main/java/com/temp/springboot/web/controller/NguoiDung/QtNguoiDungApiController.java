package com.temp.springboot.web.controller.NguoiDung;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.SendMail;
import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.uploadfile.UploadFileDefaultDTO;
import com.temp.service.NguoiDung.QtNguoiDungService;
import com.temp.utils.CellConfig;
import com.temp.utils.Constant;
import com.temp.utils.Constants;
import com.temp.utils.PasswordGenerator;
import com.temp.utils.TimestampUtils;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class QtNguoiDungApiController {

	public static final Logger logger = LoggerFactory.getLogger(QtNguoiDungApiController.class);

	@Autowired
	private PasswordEncoder bcryptEncoder;

	@Autowired
	@Qualifier("QtNguoiDungServiceImpl")
	private QtNguoiDungService qtService;

	@RequestMapping(value = "/nguoidung/list", method = RequestMethod.GET)
	public ResponseEntity<QtNguoiDungBDTO> getList(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "hoTen", required = false) String hoTen,
			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
			@RequestParam(value = "sEmail", required = false) String sEmail,
			@RequestParam(value = "sDiDong", required = false) String sDiDong,
			@RequestParam(value = "sMaNguoiDung", required = false) String sMaNguoiDung,
			@RequestParam(value = "trangThai", required = false) String trangThai,
			@RequestParam(value = "nhomNguoiDungId", required = false) Integer nhomNguoiDungId,
			@RequestParam(value = "loaiNguoiDung", required = false) Integer loaiNguoiDung) {

		if (!checkKeySort(keySort)) {

			QtNguoiDungBDTO bdto = new QtNguoiDungBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QtNguoiDungBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			lstQtNguoiDung = qtService.listQtNguoiDungs(strfilter, hoTen, taiKhoan, sEmail, sDiDong, sMaNguoiDung,
					pageNo, pageSize, keySort, desc, trangThai, nhomNguoiDungId, loaiNguoiDung);
			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.OK);

		} catch (Exception e) {

			e.printStackTrace();
			logger.error(e.toString());
			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/nguoiDung/exitsMa", method = RequestMethod.GET)
	public ResponseEntity<Boolean> checkExitsByMa(
			@RequestParam(value = "maNguoiDung", required = false) String maNguoiDung,
			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
		Boolean result=false;

		try {
			logger.info(Constants.Logs.EXITS);
			
			result = qtService.isExistByMa(maNguoiDung,id);

			return new ResponseEntity<Boolean>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@PostMapping("/nguoidung/add")
	public ResponseEntity<?> save(@Valid @RequestBody QtNguoiDungDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.CREATE);
		if (!qtService.isExistQtNguoiDung(dto)) {
			try {
				this.ConvertData(dto);
				if (!StringUtils.isEmpty(dto.getAnhDaiDien())) {
					dto.setAnhDaiDien(Utils.CommonSavePathFile(dto.getAnhDaiDien(),
							Constants.FolderUpload.FOLDER_NGUOIDUNG, request));
				}
				qtService.saveQtNguoiDung(dto);
				// Send Mail
//				if (dto.getEmail() != null && !dto.getEmail().equals("")) {
//					String emailTo = dto.getEmail();
//					String tieuDe = "Dang ky tai khoan He thong quan ly cong ty chung khoan SCMS";
//					String noiDung = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-bottom:20px;max-width:516px;min-width:220px\"><tbody><tr><td width=\"8\" style=\"width:8px\"></td><td><div style=\"border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px\" align=\"center\" class=\"m_205523402140519040mdv2rw\">"
//							+ "<img src=\"http://scms.tinhvan.com/img/logo.png\" width=\"74\" height=\"74\" aria-hidden=\"true\" style=\"margin-bottom:16px\" alt=\"SCMS\" class=\"CToWUd\"><div style=\"font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word\"><div style=\"font-size:24px\"><a style=\"text-decoration:none;color:rgba(0,0,0,0.87)\">Bạn đã đăng ký thành công tài khoản</a></div></div><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:left\"><h2><b>Thông tin tài khoản</b></h2><p><b>Tài khoản: </b><a style=\"text-decoration:none;color:rgba(0,0,0,0.87)\">"
//							+ dto.getTaiKhoan()
//							+ "</a></p><p><b>Mật khẩu: </b><a style=\"text-decoration:none;color:rgba(0,0,0,0.87)\">"
//							+ dto.getMatKhauDefault() + "</a></p><p>Truy cập hệ thống "
//							+ "<a href='http://scms.tinhvan.com/scms/login'><b>tại đây</b></a> và <b>ĐỔI MẬT KHẨU</b> ngay lập tức để chủ động bảo vệ tài khoản của bạn!.</p><p><i>Mọi thắc mắc liên quan đến tài khoản, vui lòng liên hệ với quản trị hệ thống.</i></p></div></div><div style=\"text-align:left\"><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center\"><div>Vui lòng giữ bảo mật thông tin tài khoản đã được cấp</div><div style=\"direction:ltr\">© 2020 SCMS - Ủy ban chứng khoán nhà nước</div></div></div></td><td width=\"8\" style=\"width:8px\"></td></tr></tbody></table>";
//					SendMail.SendMail(emailTo, tieuDe, noiDung);
//				}
			} catch (Exception e) {
				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
			}
			return new ResponseEntity<QtNguoiDungDTO>(dto, HttpStatus.CREATED);
		} else {
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * cập nhập người dùng
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/nguoidung/update")
	public ResponseEntity<?> update(@Valid @RequestBody QtNguoiDungDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			// check upload file
			String PathUpload_update = "";
			if (dto.getArrFileDinhKem() != null && !dto.getArrFileDinhKem().isEmpty()) {
				for (UploadFileDefaultDTO e : dto.getArrFileDinhKem()) {
					if (!StringUtils.isEmpty(e.getNameFile())) {
						PathUpload_update += e.getPath() + ",";
					}
				}
				if (PathUpload_update != "") {
					PathUpload_update = PathUpload_update.substring(0, PathUpload_update.length() - 1);
				}
			}

			if (!StringUtils.isEmpty(dto.getAnhDaiDien())) {
				dto.setAnhDaiDien(Utils.CommonSavePathFile(dto.getAnhDaiDien(), Constants.FolderUpload.FOLDER_NGUOIDUNG,
						request));
			} else {
				dto.setAnhDaiDien(dto.getAnhDaiDien().split("~")[0]);
			}

			if (qtService.isExistById(dto.getId())) {

				try {
					this.ConvertData(dto);
					qtService.updateQtNguoiDung(dto);
					// lấy dữ liệu cũ trươc skhi add
					QtNguoiDungDTO oldObject = this.qtService.findById(dto.getId());

					QtNguoiDungDTO newObject = qtService.saveQtNguoiDungGetObject(dto);

					if (!StringUtils.isEmpty(oldObject.getAnhDaiDien())) {
						// lấy các path của old và new
						String[] pathOld = oldObject.getAnhDaiDien().split(",");
						String[] pathNew = newObject.getAnhDaiDien().split(",");
						List<String> oldList = null;
						List<String> NewList = null;

						if (pathOld.length > 0) {
							oldList = Arrays.asList(pathOld);
						}
						if (pathNew.length > 0) {
							NewList = Arrays.asList(pathNew);
						}
						// biến chứa path cần remove
						List<String> pathRemove = new ArrayList<>();
						// check các trường hợp để add vào pathRemove
						if (oldList != null && NewList == null) {
							pathRemove.addAll(oldList);
						} else if (oldList != null && NewList != null) {
							for (String path : oldList) {
								if (!StringUtils.isEmpty(path) && !NewList.contains(path)) {
									pathRemove.add(path);
								}
							}
						}
						// kiểm tra nếu path cần xóa thì xóa
						if (!pathRemove.isEmpty()) {
							for (String pathx : pathRemove) {
								Utils.DeleteFile(Constant.UPLOADNOTTEMPFOLDER + pathx);
							}
						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}
				return new ResponseEntity<QtNguoiDungDTO>(dto, HttpStatus.ACCEPTED);
			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}
	}

	@PutMapping("/phanquyen/")
	public ResponseEntity<?> phanQuyenNguoiDung(@RequestBody NguoiDungJoinAllDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		logger.info(Constants.Logs.UPDATE);

		if (qtService.isExistById(dto.getId())) {

			try {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				qtService.phanQuyenChucNang(dto);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " phân quyền cho người dùng " + dto.getHoTen()
						+ " với tài khoản : " + dto.getTaiKhoan());
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}
			return new ResponseEntity<NguoiDungJoinAllDTO>(dto, HttpStatus.ACCEPTED);
		} else {
			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/nguoidung/changepassword", method = RequestMethod.GET)
	public ResponseEntity<?> changePass(@RequestParam(value = "id", defaultValue = "0") Integer id,
			@RequestParam(value = "matKhauCu", required = false) String matKhauCu,
			@RequestParam(value = "matKhauMoi", required = false) String matKhauMoi,
			@RequestParam(value = "matKhauMoiConfirm", required = false) String matKhauMoiConfirm,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			id = userInfo.getId();
			if (qtService.isExistById(id)) {
				String validateStr = validateData(id, matKhauCu, matKhauMoi, matKhauMoiConfirm);
				if (validateStr.equals("")) {
					Timestamp timeStamp = new Timestamp(new Date().getTime());
					qtService.changePasswordQtNguoiDung(id, matKhauMoi);

					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_NGUOI_DUNG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " đổi mật khẩu ");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
					return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
				} else {
					return new ResponseEntity<String>(validateStr, HttpStatus.ACCEPTED);
				}
			}
			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@DeleteMapping("/nguoidung/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			if(id == 1) {
				return new ResponseEntity<>(new ApiRequestException("Bình tĩnh ! Người dùng này không thể xóa ! "),
						HttpStatus.LOCKED);
			}
			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa người dùng ID = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/nguoidung/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			NguoiDungJoinAllDTO qt = qtService.findByJoinAllId(id);
			if (qt == null) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/nguoidung/export", method = RequestMethod.GET)
	public ResponseEntity<ByteArrayResource> ExportExcel(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "hoTen", required = false) String hoTen,
			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
			@RequestParam(value = "sEmail", required = false) String sEmail,
			@RequestParam(value = "sDiDong", required = false) String sDiDong,
			@RequestParam(value = "sMaNguoiDung", required = false) String sMaNguoiDung,
			@RequestParam(value = "trangThai", required = false) String trangThai,
			@RequestParam(value = "nhomNguoiDungId", required = false) Integer nhomNguoiDungId,
			@RequestParam(value = "loaiNguoiDung", required = false) Integer loaiNguoiDung) throws ApiRequestException {

		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();
		try {
			Workbook workbook = new XSSFWorkbook();

			lstQtNguoiDung = qtService.listQtNguoiDungs(strfilter, hoTen, taiKhoan, sEmail, sDiDong, sMaNguoiDung, 0,
					999999999, keySort, desc, trangThai, nhomNguoiDungId, loaiNguoiDung);

			Sheet sheet = workbook.createSheet("Danh sách người dùng");

			CellStyle style = CellConfig.createStyleForTitleTBCN(sheet, workbook);
			CellStyle style1 = CellConfig.createCellStyle(sheet, workbook, true);

			Cell cell;
			Row row;
			Integer rownum = 0;
			row = sheet.createRow((rownum));
			cell = row.createCell(0, CellType.STRING);
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
			cell.setCellValue(("Danh sách người dùng".toUpperCase()));
			cell.setCellStyle(style1);

			// row bat dau
			rownum = 1;
			// Tạo tiêu đề
			row = sheet.createRow(rownum);

			cell = row.createCell(0, CellType.STRING);
			cell.setCellValue("Tài khoản");
			cell.setCellStyle(style);

			cell = row.createCell(1, CellType.STRING);
			cell.setCellValue("Họ tên");
			cell.setCellStyle(style);

			cell = row.createCell(2, CellType.STRING);
			cell.setCellValue("Mật khẩu mặc định");
			cell.setCellStyle(style);

			cell = row.createCell(3, CellType.STRING);
			cell.setCellValue("Nhóm người dùng");
			cell.setCellStyle(style);

//			cell = row.createCell(4, CellType.STRING);
//			cell.setCellValue("Công ty");
//			cell.setCellStyle(style);

			cell = row.createCell(4, CellType.STRING);
			cell.setCellValue("Trạng thái kích hoạt");
			cell.setCellStyle(style);

			cell = row.createCell(5, CellType.STRING);
			cell.setCellValue("Loại người dùng");
			cell.setCellStyle(style);

//			cell = row.createCell(6, CellType.STRING);
//			cell.setCellValue("Sử dụng chữ ký số");
//			cell.setCellStyle(style);

			if (lstQtNguoiDung != null && lstQtNguoiDung.getLstQtNguoiDung().size() > 0) {

				for (NguoiDungJoinAllDTO item : lstQtNguoiDung.getLstQtNguoiDung()) {
					rownum++;
					row = sheet.createRow(rownum);

					cell = row.createCell(0, CellType.STRING);
					cell.setCellValue(item.getTaiKhoan());
					CellConfig.BorderTable(cell, sheet);

					cell = row.createCell(1, CellType.STRING);
					cell.setCellValue(item.getHoTen());
					CellConfig.BorderTable(cell, sheet);

					cell = row.createCell(2, CellType.STRING);
					cell.setCellValue(item.getMatKhauDefault());
					CellConfig.BorderTable(cell, sheet);

					cell = row.createCell(3, CellType.STRING);
					cell.setCellValue(item.getTenNhomNguoiDung());
					CellConfig.BorderTable(cell, sheet);

//					String congTy = "";
//					List<CtckThongTinDTO> lstCongTys = item.getLis;
//					for (ChucVuDTO temp : lstChucVu) {
//						if (temp != null) {
//							chucVu += temp.getTenChucVu() == null ? "" : temp.getTenChucVu() + "/";
//						}
//					}
					String trangThaiKichHoat = "";
					if (item.getTrangThai() == true) {
						trangThaiKichHoat = "Đã kích hoạt";
					} else {
						trangThaiKichHoat = "Chưa kích hoạt";
					}

					cell = row.createCell(4, CellType.STRING);
					cell.setCellValue(trangThaiKichHoat);
					CellConfig.BorderTable(cell, sheet);

					String admin = "";
					if (item.getAdmin()) {
						admin = "Admin";
					} else {
						admin = "Không là Admin";
					}

					cell = row.createCell(5, CellType.STRING);
					cell.setCellValue(admin);
					CellConfig.BorderTable(cell, sheet);
				}
				for (int i = 0; i < 6; i++) {
					sheet.autoSizeColumn(i);
				}

				CellConfig.BorderTable(cell, sheet);
			}
			CellConfig.autoSizeColumns(workbook);

			String fileName = "danh_sach_nguoi_dung"
					+ TimestampUtils.TimestampToString_ddMMyyyyHHmmFileName(Utils.getCurrentDate()) + ".xlsx";

			try {
				FileOutputStream out = new FileOutputStream(fileName);
				workbook.write(out);
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				workbook.write(bos);

				byte[] bytes = bos.toByteArray();
				ByteArrayResource resource = new ByteArrayResource(bytes);

				out.close();
				workbook.close();
				return ResponseEntity.ok()
						.contentLength(bytes.length) //
						.body(resource);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		} catch (Exception e) {
			logger.error(e.toString());
			return null;
		}
	}

	// ------------------------------------------------------------------------------------------------------------

//	@RequestMapping(value = "/qtnguoidung/filter", method = RequestMethod.GET)
//	public ResponseEntity<QtNguoiDungBDTO> listQtNguoiDungs(
//			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "dmChucVuId", required = false) Integer dmChucVuId,
//			@RequestParam(value = "hoTen", required = false) String hoTen,
//			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
//			@RequestParam(value = "sEmail", required = false) String sEmail,
//			@RequestParam(value = "sDiDong", required = false) String sDiDong,
//			@RequestParam(value = "sMaNguoiDung", required = false) String sMaNguoiDung,
//			@RequestParam(value = "trangThai", required = false) String trangThai,
//			@RequestParam(value = "tenCTCK", required = false) String tenCTCK,
//			@RequestParam(value = "nhomNguoiDungId", required = false) Integer nhomNguoiDungId,
//			@RequestParam(value = "loaiNguoiDung", required = false) Integer loaiNguoiDung,
//			@RequestParam(value = "trangThaiCTCK", required = false) Integer trangThaiCTCK) {
//
//		if (!checkKeySort(keySort)) {
//
//			QtNguoiDungBDTO bdto = new QtNguoiDungBDTO();
//			bdto.setMessage("truyền sai keySort");
//
//			return new ResponseEntity<QtNguoiDungBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
//		}
//
//		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//
//			lstQtNguoiDung = qtService.listQtNguoiDungs(strfilter, dmChucVuId, hoTen, taiKhoan, sEmail, sDiDong,
//					sMaNguoiDung, pageNo, pageSize, keySort, desc, trangThai, tenCTCK, trangThaiCTCK, nhomNguoiDungId,
//					loaiNguoiDung);
//			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.OK);
//
//		} catch (Exception e) {
//
//			e.printStackTrace();
//			logger.error(e.toString());
//			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

//	@RequestMapping(value = "/qtnguoidungtv/filter", method = RequestMethod.GET)
//	public ResponseEntity<QtNguoiDungBDTO> listQtNguoiDungsTV(
//			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "dmChucVuId", required = false) Integer dmChucVuId,
//			@RequestParam(value = "hoTen", required = false) String hoTen,
//			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
//			@RequestParam(value = "sEmail", required = false) String sEmail,
//			@RequestParam(value = "sDiDong", required = false) String sDiDong,
//			@RequestParam(value = "sMaNguoiDung", required = false) String sMaNguoiDung,
//			@RequestParam(value = "trangThai", required = false) String trangThai,
//			@RequestParam(value = "tenCTCK", required = false) String tenCTCK,
//			@RequestParam(value = "nhomNguoiDungId", required = false) Integer nhomNguoiDungId,
//			@RequestParam(value = "loaiNguoiDung", required = false) Integer loaiNguoiDung,
//			@RequestParam(value = "trangThaiCTCK", required = false) Integer trangThaiCTCK) {
//
//		if (!checkKeySort(keySort)) {
//
//			QtNguoiDungBDTO bdto = new QtNguoiDungBDTO();
//			bdto.setMessage("truyền sai keySort");
//
//			return new ResponseEntity<QtNguoiDungBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
//		}
//
//		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();
//
//		try {
//
//			logger.info(Constants.Logs.LIST);
//			if (pageNo > 0) {
//				pageNo = pageNo - 1;
//			}
//
//			lstQtNguoiDung = qtService.listQtNguoiDungs(strfilter, dmChucVuId, hoTen, taiKhoan, sEmail, sDiDong,
//					sMaNguoiDung, pageNo, pageSize, keySort, desc, trangThai, tenCTCK, trangThaiCTCK, nhomNguoiDungId,
//					loaiNguoiDung);
//			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.OK);
//
//		} catch (Exception e) {
//
//			e.printStackTrace();
//			logger.error(e.toString());
//			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

//	@RequestMapping(value = "/qtnguoidung/export", method = RequestMethod.GET)
//	public ResponseEntity<ByteArrayResource> ExportExcel(
//			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
//			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
//			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
//			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
//			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
//			@RequestParam(value = "dmChucVuId", required = false) Integer dmChucVuId,
//			@RequestParam(value = "hoTen", required = false) String hoTen,
//			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
//			@RequestParam(value = "sEmail", required = false) String sEmail,
//			@RequestParam(value = "sDiDong", required = false) String sDiDong,
//			@RequestParam(value = "sMaNguoiDung", required = false) String sMaNguoiDung,
//			@RequestParam(value = "trangThai", required = false) String trangThai,
//			@RequestParam(value = "tenCTCK", required = false) String tenCTCK,
//			@RequestParam(value = "nhomNguoiDungId", required = false) Integer nhomNguoiDungId,
//			@RequestParam(value = "loaiNguoiDung", required = false) Integer loaiNguoiDung,
//			@RequestParam(value = "trangThaiCTCK", required = false) Integer trangThaiCTCK) throws ApiRequestException {
//
//		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();
//		try {
//			Workbook workbook = new XSSFWorkbook();
//
//			lstQtNguoiDung = qtService.listQtNguoiDungs(strfilter, dmChucVuId, hoTen, taiKhoan, sEmail, sDiDong,
//					sMaNguoiDung, 0, 999999999, keySort, desc, trangThai, tenCTCK, trangThaiCTCK, nhomNguoiDungId,
//					loaiNguoiDung);
//
//			Sheet sheet = workbook.createSheet("Danh sách người dùng");
//
//			CellStyle style = CellConfig.createStyleForTitleTBCN(sheet, workbook);
//			CellStyle style1 = CellConfig.createCellStyle(sheet, workbook, true);
//
//			Cell cell;
//			Row row;
//			Integer rownum = 0;
//			row = sheet.createRow((rownum));
//			cell = row.createCell(0, CellType.STRING);
//			sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 6));
//			cell.setCellValue(("Danh sách người dùng".toUpperCase()));
//			cell.setCellStyle(style1);
//
//			// row bat dau
//			rownum = 1;
//			// Tạo tiêu đề
//			row = sheet.createRow(rownum);
//
//			cell = row.createCell(0, CellType.STRING);
//			cell.setCellValue("Tài khoản");
//			cell.setCellStyle(style);
//
//			cell = row.createCell(1, CellType.STRING);
//			cell.setCellValue("Họ tên");
//			cell.setCellStyle(style);
//
//			cell = row.createCell(2, CellType.STRING);
//			cell.setCellValue("Mật khẩu mặc định");
//			cell.setCellStyle(style);
//
//			cell = row.createCell(3, CellType.STRING);
//			cell.setCellValue("Nhóm người dùng");
//			cell.setCellStyle(style);
//
////			cell = row.createCell(4, CellType.STRING);
////			cell.setCellValue("Công ty");
////			cell.setCellStyle(style);
//
//			cell = row.createCell(4, CellType.STRING);
//			cell.setCellValue("Trạng thái kích hoạt");
//			cell.setCellStyle(style);
//
//			cell = row.createCell(5, CellType.STRING);
//			cell.setCellValue("Loại người dùng");
//			cell.setCellStyle(style);
//
//			cell = row.createCell(6, CellType.STRING);
//			cell.setCellValue("Sử dụng chữ ký số");
//			cell.setCellStyle(style);
//
//			if (lstQtNguoiDung != null && lstQtNguoiDung.getLstQtNguoiDung().size() > 0) {
//
//				for (NguoiDungJoinAllDTO item : lstQtNguoiDung.getLstQtNguoiDung()) {
//					rownum++;
//					row = sheet.createRow(rownum);
//
//					cell = row.createCell(0, CellType.STRING);
//					cell.setCellValue(item.getTaiKhoan());
//					CellConfig.BorderTable(cell, sheet);
//
//					cell = row.createCell(1, CellType.STRING);
//					cell.setCellValue(item.getHoTen());
//					CellConfig.BorderTable(cell, sheet);
//
//					cell = row.createCell(2, CellType.STRING);
//					cell.setCellValue(item.getMatKhauDefault());
//					CellConfig.BorderTable(cell, sheet);
//
//					cell = row.createCell(3, CellType.STRING);
//					cell.setCellValue(item.getTenNhomNguoiDung());
//					CellConfig.BorderTable(cell, sheet);
//
////					String congTy = "";
////					List<CtckThongTinDTO> lstCongTys = item.getLis;
////					for (ChucVuDTO temp : lstChucVu) {
////						if (temp != null) {
////							chucVu += temp.getTenChucVu() == null ? "" : temp.getTenChucVu() + "/";
////						}
////					}
//					String trangThaiKichHoat = "";
//					if (item.getTrangThai() == true) {
//						trangThaiKichHoat = "Đã kích hoạt";
//					} else {
//						trangThaiKichHoat = "Chưa kích hoạt";
//					}
//
//					cell = row.createCell(4, CellType.STRING);
//					cell.setCellValue(trangThaiKichHoat);
//					CellConfig.BorderTable(cell, sheet);
//
//					String thanhVien = "";
//					if (item.getThanhVien() == true) {
//						thanhVien = "Thành viên";
//					} else {
//						thanhVien = "UBCK";
//					}
//
//					cell = row.createCell(5, CellType.STRING);
//					cell.setCellValue(thanhVien);
//					CellConfig.BorderTable(cell, sheet);
//
//					String chuKySo = "";
//					if (item.getChuKySo() == true) {
//						chuKySo = "Có";
//					} else {
//						chuKySo = "Không";
//					}
//
//					cell = row.createCell(6, CellType.STRING);
//					cell.setCellValue(chuKySo);
//					CellConfig.BorderTable(cell, sheet);
//
//				}
//				for (int i = 0; i < 7; i++) {
//					sheet.autoSizeColumn(i);
//				}
//
//				CellConfig.BorderTable(cell, sheet);
//			}
//			CellConfig.autoSizeColumns(workbook);
//
//			String fileName = "danh_sach_nguoi_dung"
//					+ TimestampUtils.TimestampToString_ddMMyyyyHHmmFileName(Utils.getCurrentDate()) + ".xlsx";
//
//			try {
//				FileOutputStream out = new FileOutputStream(fileName);
//				workbook.write(out);
//				ByteArrayOutputStream bos = new ByteArrayOutputStream();
//				workbook.write(bos);
//
//				byte[] bytes = bos.toByteArray();
//				ByteArrayResource resource = new ByteArrayResource(bytes);
//
//				out.close();
//				workbook.close();
//				return ResponseEntity.ok()
//						// Content-Disposition
//						// Content-Type
//						// .contentType(mediaType) //
//						// Content-Lengh
//						.contentLength(bytes.length) //
//						.body(resource);
//			} catch (FileNotFoundException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			return null;
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return null;
//		}
//	}

	/**
	 * Danh sach user online
	 * 
	 * @param strfilter
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @param dmChucVuId
	 * @param hoTen
	 * @param taiKhoan
	 * @param sEmail
	 * @param sDiDong
	 * @param sMaNguoiDung
	 * @param trangThai
	 * @return listQtNguoiDungLogins
	 */
	@RequestMapping(value = "/qtnguoidung/filterlogin", method = RequestMethod.GET)
	public ResponseEntity<QtNguoiDungBDTO> listQtNguoiDungLogins(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "dmChucVuId", required = false) Integer dmChucVuId,
			@RequestParam(value = "hoTen", required = false) String hoTen,
			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
			@RequestParam(value = "sEmail", required = false) String sEmail,
			@RequestParam(value = "sDiDong", required = false) String sDiDong,
			@RequestParam(value = "sMaNguoiDung", required = false) String sMaNguoiDung,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			QtNguoiDungBDTO bdto = new QtNguoiDungBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QtNguoiDungBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			lstQtNguoiDung = qtService.listQtNguoiDungLogins(strfilter, dmChucVuId, hoTen, taiKhoan, sEmail, sDiDong,
					sMaNguoiDung, pageNo, pageSize, keySort, desc, trangThai);

			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/qtnguoidung/getall", method = RequestMethod.GET)
	public ResponseEntity<QtNguoiDungBDTO> listQtNguoiDungs(
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			QtNguoiDungBDTO bdto = new QtNguoiDungBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QtNguoiDungBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QtNguoiDungBDTO lstQtNguoiDung = new QtNguoiDungBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			lstQtNguoiDung = qtService.listAllNguoiDung(keySort, desc, trangThai);

			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QtNguoiDungBDTO>(lstQtNguoiDung, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Create
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 * @throws Exception
	 */
	@PostMapping("/qtnguoidung/themmoi")
	public ResponseEntity<?> createQtNguoiDung(@Valid @RequestBody QtNguoiDungDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {

		logger.info(Constants.Logs.CREATE);
		if(dto.getTaiKhoan() != null) {
			dto.setTaiKhoan(dto.getTaiKhoan().replaceAll(" ", ""));
		}
		if (!qtService.isExistQtNguoiDung(dto)) {
			try {
				this.ConvertData(dto);
				if (!StringUtils.isEmpty(dto.getAnhDaiDien())) {
					dto.setAnhDaiDien(Utils.CommonSavePathFile(dto.getAnhDaiDien(),
							Constants.FolderUpload.FOLDER_NGUOIDUNG, request));
				}
				qtService.saveQtNguoiDung(dto);
				// Send Mail
				if (dto.getEmail() != null && !dto.getEmail().equals("")) {
					String emailTo = dto.getEmail();
					String tieuDe = "Dang ky tai khoan He thong quan ly cong ty chung khoan SCMS";
					String noiDung = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"padding-bottom:20px;max-width:516px;min-width:220px\"><tbody><tr><td width=\"8\" style=\"width:8px\"></td><td><div style=\"border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px\" align=\"center\" class=\"m_205523402140519040mdv2rw\">"
							+ "<img src=\"http://scms.tinhvan.com/img/logo.png\" width=\"74\" height=\"74\" aria-hidden=\"true\" style=\"margin-bottom:16px\" alt=\"SCMS\" class=\"CToWUd\"><div style=\"font-family:'Google Sans',Roboto,RobotoDraft,Helvetica,Arial,sans-serif;border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word\"><div style=\"font-size:24px\"><a style=\"text-decoration:none;color:rgba(0,0,0,0.87)\">Bạn đã đăng ký thành công tài khoản</a></div></div><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:left\"><h2><b>Thông tin tài khoản</b></h2><p><b>Tài khoản: </b><a style=\"text-decoration:none;color:rgba(0,0,0,0.87)\">"
							+ dto.getTaiKhoan()
							+ "</a></p><p><b>Mật khẩu: </b><a style=\"text-decoration:none;color:rgba(0,0,0,0.87)\">"
							+ dto.getMatKhauDefault() + "</a></p><p>Truy cập hệ thống "
							+ "<a href='http://scms.tinhvan.com/scms/login'><b>tại đây</b></a> và <b>ĐỔI MẬT KHẨU</b> ngay lập tức để chủ động bảo vệ tài khoản của bạn!.</p><p><i>Mọi thắc mắc liên quan đến tài khoản, vui lòng liên hệ với quản trị hệ thống.</i></p></div></div><div style=\"text-align:left\"><div style=\"font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:11px;line-height:18px;padding-top:12px;text-align:center\"><div>Vui lòng giữ bảo mật thông tin tài khoản đã được cấp</div><div style=\"direction:ltr\">© 2020 SCMS - Ủy ban chứng khoán nhà nước</div></div></div></td><td width=\"8\" style=\"width:8px\"></td></tr></tbody></table>";
					SendMail.SendMail(emailTo, tieuDe, noiDung);
				}
			} catch (Exception e) {
				throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);
			}
			return new ResponseEntity<QtNguoiDungDTO>(dto, HttpStatus.CREATED);
		} else {
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);
		}
//			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.EXPECTATION_FAILED);

	}

	/**
	 * @throws ParseException
	 * 
	 */
	private void ConvertData(QtNguoiDungDTO dto) throws ParseException {
		if (dto.getNgayHetHanStr() != null && (!"".endsWith(dto.getNgayHetHanStr()))) {
			dto.setNgayHetHan(
					new java.sql.Timestamp(new SimpleDateFormat("dd/MM/yyyy").parse(dto.getNgayHetHanStr()).getTime()));
		}
	}

	/**
	 * cập nhập người dùng
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/qtnguoidung/capnhat")
	public ResponseEntity<?> updateQtNguoiDung(@Valid @RequestBody QtNguoiDungDTO dto,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);

			// check upload file
			String PathUpload_update = "";
			if (dto.getArrFileDinhKem() != null && !dto.getArrFileDinhKem().isEmpty()) {
				for (UploadFileDefaultDTO e : dto.getArrFileDinhKem()) {
					if (!StringUtils.isEmpty(e.getNameFile())) {
						PathUpload_update += e.getPath() + ",";
					}
				}
				if (PathUpload_update != "") {
					PathUpload_update = PathUpload_update.substring(0, PathUpload_update.length() - 1);
				}
			}

			// add file moi
//		if (dto.getArrFileDinhKem() != null) {
			if (!StringUtils.isEmpty(dto.getAnhDaiDien())) {
				dto.setAnhDaiDien(Utils.CommonSavePathFile(dto.getAnhDaiDien(), Constants.FolderUpload.FOLDER_NGUOIDUNG,
						request));
//				String UrlPathCDNLQ = Constants.FolderUpload.FOLDER_NGUOIDUNG;
//				String[] splipPath = dto.getAnhDaiDien().split("`");
//				String pathNew = "";
//				if (splipPath.length > 0) {
//					for (int i = 0; i < splipPath.length; i++) {
//						String path = splipPath[i];
//						String[] pathRelative = path.split("~");
//						if (pathRelative.length > 0 && pathRelative.length < 3) {
//							//System.out.println(pathRelative[1]);
//							//System.out.println(request.getServletContext().getRealPath("/") + pathRelative[0]);
//							File file = new File(pathRelative[0]);
//							Path tmpPath = Paths.get(UrlPathCDNLQ);
//							String tmp = request.getServletContext().getRealPath("/") + tmpPath.toFile();
//							try {
//								Utils.MoveFileToDirectory(file, tmp);
//							} catch (IOException e) {
//
//								throw new ApiRequestException("Đăng tải tài liệu không thành công!",
//										HttpStatus.EXPECTATION_FAILED);
//							}
//							pathNew += UrlPathCDNLQ + pathRelative[1] + ",";
//
//						}
//
//					}
//
//				}
//				if (pathNew != null) {
//					pathNew = pathNew.substring(0, pathNew.length() - 1);
//					if (PathUpload_update != "") {
//						PathUpload_update = PathUpload_update + "," + pathNew;
//					} else {
//						PathUpload_update = pathNew;
//					}
//
//				}
//
//			}
//			PathUpload_update = PathUpload_update.replace("\\", "/");
//			dto.setAnhDaiDien(PathUpload_update);
			} else {
				dto.setAnhDaiDien(dto.getAnhDaiDien().split("~")[0]);
			}

			if (qtService.isExistById(dto.getId())) {

				try {
					this.ConvertData(dto);
					qtService.updateQtNguoiDung(dto);

					// lấy dữ liệu cũ trươc skhi add
					QtNguoiDungDTO oldObject = this.qtService.findById(dto.getId());

					QtNguoiDungDTO newObject = qtService.saveQtNguoiDungGetObject(dto);

					if (!StringUtils.isEmpty(oldObject.getAnhDaiDien())) {
						// lấy các path của old và new
						String[] pathOld = oldObject.getAnhDaiDien().split(",");
						String[] pathNew = newObject.getAnhDaiDien().split(",");
						List<String> oldList = null;
						List<String> NewList = null;

						if (pathOld.length > 0) {
							oldList = Arrays.asList(pathOld);
						}
						if (pathNew.length > 0) {
							NewList = Arrays.asList(pathNew);
						}
						// biến chứa path cần remove
						List<String> pathRemove = new ArrayList<>();
						// check các trường hợp để add vào pathRemove
						if (oldList != null && NewList == null) {
							pathRemove.addAll(oldList);
						} else if (oldList != null && NewList != null) {
							for (String path : oldList) {
								if (!StringUtils.isEmpty(path) && !NewList.contains(path)) {
									pathRemove.add(path);
								}
							}
						}
						// kiểm tra nếu path cần xóa thì xóa
						if (!pathRemove.isEmpty()) {
							for (String pathx : pathRemove) {
								Utils.DeleteFile(Constant.UPLOADNOTTEMPFOLDER + pathx);
							}
						}

					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

				}
				return new ResponseEntity<QtNguoiDungDTO>(dto, HttpStatus.ACCEPTED);
			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}
	}

	/**
	 * cập nhập người dùng
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/phanquyennguoidung/")
	public ResponseEntity<?> phanQuyen(@RequestBody NguoiDungJoinAllDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		logger.info(Constants.Logs.UPDATE);

		if (qtService.isExistById(dto.getId())) {

			try {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				qtService.phanQuyenChucNang(dto);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " phân quyền cho người dùng " + dto.getHoTen()
						+ " với tài khoản : " + dto.getTaiKhoan());
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}
			return new ResponseEntity<NguoiDungJoinAllDTO>(dto, HttpStatus.ACCEPTED);
		} else {
			throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
		}

	}

	@RequestMapping(value = "/qtnguoidung/changestatus", method = RequestMethod.GET)
	public ResponseEntity<?> changeStatus(@RequestParam(value = "id", defaultValue = "0") Integer id,
			@RequestParam(name = "trangThai", defaultValue = "false") boolean trangThai,
			@RequestParam(name = "thanhVien", defaultValue = "false") boolean thanhVien,
			@RequestParam(name = "chuKySo", defaultValue = "false") boolean chuKySo,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());
			logger.info(Constants.Logs.UPDATE);
			if (qtService.isExistById(id)) {
				qtService.changeStatusQtNguoiDung(id, trangThai, thanhVien, chuKySo);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung(
						"Tài khoản " + userInfo.getTaiKhoan() + " cập nhật trạng thái cho người dùng ID = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
			}
			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@RequestMapping(value = "/qtnguoidung/changepassword", method = RequestMethod.GET)
	public ResponseEntity<?> changePassword(@RequestParam(value = "id", defaultValue = "0") Integer id,
			@RequestParam(value = "matKhauCu", required = false) String matKhauCu,
			@RequestParam(value = "matKhauMoi", required = false) String matKhauMoi,
			@RequestParam(value = "matKhauMoiConfirm", required = false) String matKhauMoiConfirm,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			id = userInfo.getId();
			if (qtService.isExistById(id)) {
				String validateStr = validateData(id, matKhauCu, matKhauMoi, matKhauMoiConfirm);
				if (validateStr.equals("")) {
					Timestamp timeStamp = new Timestamp(new Date().getTime());
					qtService.changePasswordQtNguoiDung(id, matKhauMoi);

					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_NGUOI_DUNG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " đổi mật khẩu ");
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
					return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
				} else {
					return new ResponseEntity<String>(validateStr, HttpStatus.ACCEPTED);
				}
			}
			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@RequestMapping(value = "/qtnguoidung/resetpassword", method = RequestMethod.GET)
	public ResponseEntity<?> resetPassword(@RequestParam(value = "id", defaultValue = "0") Integer id,
			@Context HttpServletRequest request) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());

			if (qtService.isExistById(id)) {
				qtService.resetPasswordQtNguoiDung(id);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " reset mật khẩu cho tài khoản ID = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<String>("Reset mật khẩu thành công!", HttpStatus.ACCEPTED);
			}
			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@RequestMapping(value = "/qtnguoidung/logoutuser", method = RequestMethod.GET)
	public ResponseEntity<?> logoutUser(@RequestParam(value = "id", defaultValue = "0") Integer id)
			throws ApiRequestException {
		try {
			logger.info("Logout tài khoản đang đăng nhập");

			if (qtService.isExistById(id)) {
				qtService.logoutUserLoginQtNguoiDung(id);
				return new ResponseEntity<String>("Logout tài khoản thành công!", HttpStatus.ACCEPTED);
			}
			return new ResponseEntity<String>(Constants.Messages.RC_EXIST, HttpStatus.ACCEPTED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException("Có lỗi xảy ra khi logout tài khoản!", HttpStatus.EXPECTATION_FAILED);

		}

	}

	@RequestMapping(value = "/qtnguoidung/forgotpassword", method = RequestMethod.GET)
	public ResponseEntity<?> forgotPassword(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "didong", required = false) String didong) throws ApiRequestException {
		logger.info("Quên mật khẩu");
		QtNguoiDungDTO object = qtService.findByUsername(username);
		if (object.getId() > 0) {
			try {
				qtService.changePasswordQtNguoiDung(object.getId(), PasswordGenerator.generateRandomPassword());
//				Gửi mail

			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new ApiRequestException("Có lỗi xảy ra khi gửi mail reset mật khẩu!",
						HttpStatus.EXPECTATION_FAILED);
			}
			return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
		} else {
			throw new ApiRequestException("Tài khoản không tồn tại trên hệ thống!", HttpStatus.EXPECTATION_FAILED);
		}
	}

//	@RequestMapping(value = "/qtnguoidung/forgotpassword", method = RequestMethod.POST)
//	public ResponseEntity<?> forgotPassword(@RequestBody ForgotPassDTO qtNguoiDungDTO,
//			@Context HttpServletRequest request) throws Exception {
//		logger.info("Quên mật khẩu");
//		QtNguoiDungDTO object = qtService.findByUsername(qtNguoiDungDTO.getUsername());
//		if (object.getId() > 0) {
//			if (object.getEmail().equals(qtNguoiDungDTO.getEmail())) {
//				try {
//					String newPass = PasswordGenerator.generateRandomPassword();
//					qtService.changePasswordQtNguoiDung(object.getId(), newPass);
//
//					Timestamp timeStamp = new Timestamp(new Date().getTime());
//					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
//					// set IP thực hiện lấy IP remote client
//					dtoLog.setIpThucHien(Utils.getClientIp(request));
//					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_NGUOI_DUNG");
//					dtoLog.setNgayTao(timeStamp);
//					dtoLog.setNguoiTaoId(object.getId());
//					dtoLog.setNoiDung("Tài khoản " + object.getTaiKhoan() + " yêu cầu cấp lại mật khẩu ! Mật khẩu mới được gửi về email " + qtNguoiDungDTO.getEmail());
//					// save db
//					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
//
//					if (qtNguoiDungDTO.getEmail() != null && !qtNguoiDungDTO.getEmail().equals("")) {
//						try {
//							
//							String emailTo = qtNguoiDungDTO.getEmail();
//							String tieuDe = "MAIL_SUBJECT_FORGOT_PASSWORD";
//							String noiDung = "Mật khẩu mới :  " + newPass + ". Vui lòng đổi lại mật khẩu ";
//							SendMail.SendMailTemplate(emailTo, tieuDe, noiDung);
//						} catch (Exception e) {
//							return new ResponseEntity<ForgotPassDTO>(new ForgotPassDTO(), HttpStatus.ACCEPTED);
//						}
//					}
//				} catch (Exception e) {
//					logger.error(e.getMessage());
//					throw new ApiRequestException("Có lỗi xảy ra khi gửi mail reset mật khẩu!",
//							HttpStatus.EXPECTATION_FAILED);
//				}
//				return new ResponseEntity<String>("", HttpStatus.ACCEPTED);
//			}
//		}
//		return null;
//	}

	public String validateData(Integer id, String matKhauCu, String matKhauMoi, String matKhauMoiConfirm) {
		String err = "";
		try {
			QtNguoiDungDTO objectCheck = qtService.findById(id);
			if (objectCheck != null && !bcryptEncoder.matches(matKhauCu, objectCheck.getMatKhau())) {
				err += "Mật khẩu cũ không đúng";
			}
			if (matKhauCu.equals("") || matKhauMoi.equals("") || matKhauMoiConfirm.equals("")) {
				err += "Chưa nhập đầy đủ dữ liệu";
			}
			if (!matKhauMoi.equals("") && !matKhauMoiConfirm.equals("")
					&& !matKhauMoi.trim().toLowerCase().equals(matKhauMoiConfirm.trim().toLowerCase())) {
				err += "Mật khẩu mới không trùng khớp";
			}
		} catch (Exception e) {
			// TODO: handle exception
			err += "Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị!";
		}
		return err;
	}

	/**
	 * xóa 1 người dùng
	 * 
	 * @param id của người dùng
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/qtnguoidung/xoa/{id}")
	public ResponseEntity<?> deleteQtNguoiDung(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			logger.info(Constants.Logs.DELETE);
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			Timestamp timeStamp = new Timestamp(new Date().getTime());

			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_NGUOI_DUNG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa người dùng ID = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

//	@GetMapping("/qtnguoidung/chitiet/{id}")
//	public ResponseEntity<?> getByIdQtNguoiDung(@PathVariable("id") int id) throws ApiRequestException {
//		try {
//			logger.info(Constants.Logs.GETBYID);
//			NguoiDungJoinAllDTO qt = qtService.findByJoinAllId(id);
//			if (qt == null) {
//				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
//						HttpStatus.BAD_REQUEST);
//			}
//			return ResponseEntity.ok().body(qt);
//
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
//		}
//	}

//	@GetMapping("/qtnguoidung/checkctck")
//	public int checkThanhVien() throws ApiRequestException {
//		try {
//			// -1: Cán bộ UBCK; 0: Thành viên nhưng chưa có công ty; Khác: Công ty id
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//			if (userInfo != null && !userInfo.getAdmin()) {
//				if (userInfo.getCtckId() != null && userInfo.getCtckId() > 0) {
//					CtckThongTinDTO ctckThongTinDTO = ctckService.getById(userInfo.getCtckId());
//					if (ctckThongTinDTO != null && ctckThongTinDTO.getId() > 0) {
//						return ctckThongTinDTO.getId();
//					}
//				}
//				return 0;
//			}
//			return -1;
//		} catch (Exception e) {
//			logger.error(e.getMessage());
//			return -1;
//		}
//	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = QtNguoiDungDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}

//	private String uploadFileDinhKem(List<UploadFileDefaultDTO> arrFileDinhKem, String fileDinhKem,
//			HttpServletRequest request) throws Exception {
//		if (arrFileDinhKem == null || arrFileDinhKem.size() == 0) {
//			return StringUtils.EMPTY;
//		}
//
//		String PathUpload_update = StringUtils.EMPTY;
//		if (arrFileDinhKem != null && !arrFileDinhKem.isEmpty()) {
//			for (UploadFileDefaultDTO e : arrFileDinhKem) {
//				if (!StringUtils.isEmpty(e.getNameFile())) {
//					PathUpload_update += e.getPath() + ",";
//				}
//			}
//			if (!PathUpload_update.isEmpty()) {
//				PathUpload_update = PathUpload_update.substring(0, PathUpload_update.length() - 1);
//			}
//		}
//
//		// add file moi
//		if (!StringUtils.isEmpty(fileDinhKem)) {
//			String UrlPathCDNLQ = Constants.FolderUpload.FOLDER_NGUOIDUNG;
//			String[] splipPath = fileDinhKem.split("`");
//			String pathNew = "";
//			if (splipPath.length > 0) {
//				for (int i = 0; i < splipPath.length; i++) {
//					String path = splipPath[i];
//					String[] pathRelative = path.split("~");
//					if (pathRelative.length > 0 && pathRelative.length < 3) {
//						try {
//							pathNew += Utils.CommonSavePathFile(fileDinhKem, UrlPathCDNLQ, null) + ",";
//						} catch (IOException e) {
//							return null;
//						}
//					}
//
//				}
//
//			}
//			if (pathNew != null) {
//				pathNew = pathNew.substring(0, pathNew.length() - 1);
//				if (!PathUpload_update.isEmpty()) {
//					PathUpload_update = PathUpload_update + "," + pathNew;
//				} else {
//					PathUpload_update = pathNew;
//				}
//
//			}
//
//		}
//		PathUpload_update = PathUpload_update.replace("\\", "/");
//
//		return PathUpload_update;
//	}

}