package com.temp.springboot.web.controller.NguoiDung;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.ChucNangBDTO;
import com.temp.model.NguoiDung.ChucNangDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.SubChucNangDTO;
import com.temp.service.NguoiDung.ChucNangService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class ChucNangApiController {

	public static final Logger logger = LoggerFactory.getLogger(ChucNangApiController.class);

	@Autowired
	private ChucNangService chucNangService;

//	@RequestMapping(value = "/chucnang/list", method = RequestMethod.GET)
//	public ResponseEntity<ChucNangBDTO> getList(
//			@RequestParam(value = "keySearch", required = false) String keySearch,
//			@RequestParam(value = "tenChucNang", required = false) String tenChucNang,
//			@RequestParam(value = "maChucNang", required = false) String maChucNang,
//			@RequestParam(value = "actionId", required = false) Integer actionId,
//			@RequestParam(value = "phanHe", required = false) String phanHe,
//			@RequestParam(value = "ghiChu", required = false) String ghiChu,
//			@RequestParam(value = "trangThai", required = false) String trangThai) {
//		ChucNangBDTO bdto = new ChucNangBDTO();
//		logger.info(Constants.Logs.LIST);
//		try {
//
//			bdto = chucNangService.getAll(keySearch, tenChucNang, maChucNang, actionId, phanHe, ghiChu, trangThai);
//
//			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.OK);
//
//		} catch (Exception e) {
//			logger.error(e.toString());
//			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
//		}
//	}

	@RequestMapping(value = "/chucnang/list", method = RequestMethod.GET)
	public ResponseEntity<ChucNangBDTO> getList(
			@RequestParam(value = "keySearch", required = false) String keySearch,
			@RequestParam(value = "tenChucNang", required = false) String tenChucNang,
			@RequestParam(value = "maChucNang", required = false) String maChucNang,
			@RequestParam(value = "actionId", required = false) Integer actionId,
			@RequestParam(value = "phanHe", required = false) String phanHe,
			@RequestParam(value = "ghiChu", required = false) String ghiChu,
			@RequestParam(value = "trangThai", required = false) String trangThai) {
		ChucNangBDTO bdto = new ChucNangBDTO();
		logger.info(Constants.Logs.LIST);
		try {

			bdto = chucNangService.listChucNangs(keySearch, tenChucNang, maChucNang, actionId, phanHe, ghiChu, trangThai);

			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/chucnang/exitsMa", method = RequestMethod.GET)
	public ResponseEntity<Boolean> checkExitsByMa(
			@RequestParam(value = "maChucNang", required = false) String maChucNang,
			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
		Boolean result=false;

		try {
			logger.info(Constants.Logs.EXITS);
			
			result = chucNangService.isExistByMa(maChucNang,id);

			return new ResponseEntity<Boolean>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Create Chức năng
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json
	 */

	@PostMapping("/chucnang/add")
	public ResponseEntity<?> add(@Valid @RequestBody ChucNangDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.CREATE);

			if (!chucNangService.isExistChucNang(dto)) {
				if (chucNangService.saveChucNang(dto)) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_CHUC_NANG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung(
							"Tài khoản " + userInfo.getTaiKhoan() + " Thêm mới chức năng " + dto.getTenChucNang());
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

					return new ResponseEntity<ChucNangDTO>(dto, HttpStatus.CREATED);
				}
			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.CONFLICT);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * change status Chức năng
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json
	 */
	@PutMapping("/chucnang/changestatus/")
	public ResponseEntity<?> changeStatus(@RequestBody SubChucNangDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.CREATE);

			if (chucNangService.isExistById(dto.getId())) {
				if (chucNangService.changeStatus(dto)) {

					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_CHUC_NANG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thay đổi trang thái chức năng có id ="
							+ dto.getId());
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

					return new ResponseEntity<SubChucNangDTO>(dto, HttpStatus.CREATED);
				}
			}

			return new ResponseEntity<Object>(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * Cập nhật Chức năng
	 * 
	 * @param qtBdto 1 đối tượng chứa id > 0
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@PutMapping("/chucnang/update")
	public ResponseEntity<?> update(@Valid @RequestBody ChucNangDTO dto) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);

			if (chucNangService.isExistById(dto.getId())) {
				chucNangService.updateChucNang(dto);
				return new ResponseEntity<ChucNangDTO>(dto, HttpStatus.ACCEPTED);
			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@GetMapping("/chucnang/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			ChucNangDTO dto = chucNangService.findById(id);
			if (dto == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(dto);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Xóa 1 Chức năng
	 * 
	 * @param id của Chức năng
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/chucnang/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request) throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.DELETE);

			if (!chucNangService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (chucNangService.deleteById(id)) {
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUC_NANG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa chức năng có id = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			} else {
				throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
			}
//			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.RC_USED, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Get list dropdown
	 * 
	 * @return BDto
	 */
	@RequestMapping(value = "/chucnang/dropdownkhoacha", method = RequestMethod.GET)
	public ResponseEntity<ChucNangBDTO> getDropdownKhoaChaId(
			@RequestParam(value = "trangThai", required = false) String trangThai) {
		logger.info(Constants.Logs.LIST);
		ChucNangBDTO bdto = new ChucNangBDTO();

		try {
			bdto = chucNangService.getDropdownKhoaChaId();
			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Get list dropdown
	 * 
	 * @return BDto
	 */
	@RequestMapping(value = "/chucnang/dropdownaction", method = RequestMethod.GET)
	public ResponseEntity<ChucNangBDTO> getDropdownAction(
			@RequestParam(value = "trangThai", required = false) String trangThai) {
		logger.info(Constants.Logs.LIST);
		ChucNangBDTO bdto = new ChucNangBDTO();

		try {
			bdto = chucNangService.getDropdownAction();
			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<ChucNangBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Validate keySort param
	 * 
	 * @param keySort
	 * @return true or false
	 */
	@SuppressWarnings("unused")
	private boolean checkKeySort(String keySort) {

		Field fld[] = ChucNangDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}