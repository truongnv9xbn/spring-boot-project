package com.temp.springboot.web.controller.BieuMauBaoCao;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.BieuMauBaoCao.SheetService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class SheetApiController {

	public static final Logger logger = LoggerFactory.getLogger(SheetApiController.class);

	@Autowired
	@Qualifier("SheetServiceImpl")
	private SheetService sheetService;

	@RequestMapping(value = "/bmsheet/filter", method = RequestMethod.GET)
	public ResponseEntity<BmSheetBDTO> listSheet(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenCTCK", required = false) Integer sIdCTCK,
			@RequestParam(value = "bmBaoCaoId", required = false) Integer bmBaoCaoId,
			@RequestParam(value = "sCMND", required = false) String sCMND) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		try {

			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			BmSheetBDTO lstSheet = sheetService.listSheet(strfilter, sIdCTCK, bmBaoCaoId, sCMND, pageNo, pageSize,
					keySort, desc);

			return new ResponseEntity<BmSheetBDTO>(lstSheet, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@RequestMapping(value = "/bmsheetList/filter", method = RequestMethod.GET)
	public ResponseEntity<BmSheetBDTO> listSheetLazy(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "sTenCTCK", required = false) Integer sIdCTCK,
			@RequestParam(value = "bmBaoCaoId", required = false) Integer bmBaoCaoId,
			@RequestParam(value = "sCMND", required = false) String sCMND) throws ApiRequestException {

		if (!checkKeySort(keySort)) {

			throw new ApiRequestException(Constants.Messages.KEYSORT_FAIL, HttpStatus.EXPECTATION_FAILED);
		}

		try {

			//logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}

			BmSheetBDTO lstSheet = sheetService.listSheetLazy(strfilter, sIdCTCK, bmBaoCaoId, sCMND, pageNo, pageSize,
					keySort, desc);

			return new ResponseEntity<BmSheetBDTO>(lstSheet, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Create sheet
	 * 
	 * @param qt json object
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PostMapping("/sheet/themmoi")
	public ResponseEntity<?> createSheet(@Valid @RequestBody BmSheetDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {

		//logger.info(Constants.Logs.CREATE);

		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		dto.setNguoiTaoId(userInfo.getId());
		try {
			if (dto.getTrangBia() != null && dto.getTrangBia()) {
				dto.setTieuDeChinh(null);
				dto.setTieuDePhu(null);
				dto.setKieuBaoCao(null);
				dto.setChieuGiay(null);
			}
			BmSheetDTO output = sheetService.AddOrUpdateSheet(dto);

			QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

			// set IP thực hiện lấy IP remote client
			dtoLog.setIpThucHien(Utils.getClientIp(request));
			dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_SHEET");
			dtoLog.setNgayTao(Utils.getCurrentDate());
			dtoLog.setNguoiTaoId(userInfo.getId());
			dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thêm mới sheet");
			// save db
			WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

			return new ResponseEntity<BmSheetDTO>(output, HttpStatus.CREATED);

		} catch (Exception e) {
			logger.error(e.getMessage());

			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * cập nhập sheet
	 * 
	 * @param qtBdto 1 đối tượng chứa id> 0
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@PutMapping("/sheet/capnhat")
	public ResponseEntity<?> updateSheet(@Valid @RequestBody BmSheetDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		//logger.info(Constants.Logs.UPDATE);

		if (sheetService.isExistUpdate(dto)) {
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			try {
				if (dto.getTrangBia() != null && dto.getTrangBia()) {
					dto.setTieuDeChinh(null);
					dto.setTieuDePhu(null);
					dto.setKieuBaoCao(null);
					dto.setChieuGiay(null);
				}
				if (dto.getTrangThaibool() != null && dto.getTrangThaibool()) {
					dto.setTrangThai(1L);
				} else {
					dto.setTrangThai(0L);
				}
				sheetService.AddOrUpdateSheet(dto);

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_SHEET");
				dtoLog.setNgayTao(Utils.getCurrentDate());
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " cập nhật sheet");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<BmSheetDTO>(dto, HttpStatus.ACCEPTED);
			} catch (Exception e) {
				logger.error(e.getMessage());
				throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

			}

		}
		throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
	}

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id của chức vụ
	 * @return ResponseEntity
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/sheet/xoa/{id}")
	public ResponseEntity<?> deleteChucVu(@PathVariable("id") Integer id, @Context HttpServletRequest request)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.DELETE);

			if (!sheetService.isExistById(id)) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
//				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
//						HttpStatus.NOT_FOUND);
			}
			if (sheetService.DeleteById(id)) {
				QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
				Timestamp timeStamp = new Timestamp(new Date().getTime());
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();

				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_SHEET");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa sheet");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@SuppressWarnings("unused")
	@GetMapping("/sheet/chitiet/{id}")
	public ResponseEntity<?> getByIdSheet(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			BmSheetDTO qt = sheetService.InitDisplayEditSheet(id);
			if (qt.getArrTieuDePhu() == null || qt.getArrTieuDePhu().size() < 1) {
				qt.setArrTieuDePhu(new ArrayList<String>(Arrays.asList("")));
			}
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/sheet/chitietbia")
	public ResponseEntity<?> getSheetBia(@RequestParam(name = "bmBaoCaoId", defaultValue = "0") Integer bmBaoCaoId)
			throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			BmSheetDTO qt = sheetService.getSheetBia(bmBaoCaoId);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/sheet/thongtinsheet/{id}")
	public ResponseEntity<?> getByIdSheetInfo(@PathVariable("id") Integer id) throws ApiRequestException {
		try {
			//logger.info(Constants.Logs.GETBYID);
			BmSheetDTO qt = sheetService.FindById(id);
			if (qt == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(qt);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	@GetMapping("/sheet/checkexitsmasheet")
	public ResponseEntity<?> getByIdChucVu(@RequestParam(name = "maSheet", defaultValue = "") String maSheet,
			@RequestParam(name = "bmSheetId", defaultValue = "0") Integer sheetId,
			@RequestParam(name = "bmBaoCaoId", defaultValue = "0") Integer bmBaoCaoId) throws ApiRequestException {
		try {
			if (!StringUtils.isEmpty(maSheet) && maSheet != null) {
				maSheet = maSheet.trim();
			}

			if (sheetService.checkExistMaSheet(maSheet, sheetId, bmBaoCaoId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/sheet/checkexitsmasheetlienthong")
	public ResponseEntity<?> getByIdSheetLienThong(
			@RequestParam(name = "maSheetLT", defaultValue = "") String maSheetLienThong,
			@RequestParam(name = "bmSheetId", defaultValue = "0") Integer sheetId,
			@RequestParam(name = "bmBaoCaoId", defaultValue = "0") Integer bmBaoCaoId) throws ApiRequestException {
		try {
			if (!StringUtils.isEmpty(maSheetLienThong) && maSheetLienThong != null) {
				maSheetLienThong = maSheetLienThong.trim();
			}

			if (sheetService.checkExistMaSheetLienThong(maSheetLienThong, sheetId, bmBaoCaoId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	@GetMapping("/sheet/checkexitssheetbia")
	public ResponseEntity<?> checkExitsBia(@RequestParam(name = "bmBaoCaoId", defaultValue = "0") Integer bmBaoCaoId)
			throws ApiRequestException {
		try {

			if (sheetService.checkExistBia(bmBaoCaoId)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * nếu tồn tại mã thì trả về true
	 * 
	 * @param maSheet
	 * @return true hoặc false
	 * @throws ApiRequestException
	 */
	// param
	// truyền
	// lên
	@GetMapping("/sheet/checkexitsmasheetupdate")
	public ResponseEntity<?> getBychecksheetUpdate(@RequestParam(name = "masheet", defaultValue = "") String masheet,
			@RequestParam(name = "id", defaultValue = "0") Integer id) throws ApiRequestException {
		try {
			if (!StringUtils.isEmpty(masheet) && masheet != null) {
				masheet = masheet.trim();
			} else if (id == null) {
				throw new Exception();
			}
			if (sheetService.checkExistMaSheetUpdate(masheet, id)) {
				return ResponseEntity.ok().body(true);
			}
			return ResponseEntity.ok().body(false);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * lấy danh sách các sheet được thiết lập trong cùng 1 biểu mẫu
	 * 
	 * @param sheedId
	 * 
	 * @return
	 * @throws ApiRequestException
	 */
	@RequestMapping(value = "/sheet/bmdssheet", method = RequestMethod.GET)
	public ResponseEntity<List<DropDownDTO>> listCotSheet(
			@RequestParam(value = "bieumauId", required = false) Integer bieumauId) throws ApiRequestException {

		List<DropDownDTO> lst = null;

		try {

			//logger.info(Constants.Logs.LIST);

			lst = sheetService.listSheet(bieumauId);

			return new ResponseEntity<List<DropDownDTO>>(lst, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@GetMapping("/bmsheet/getsheetfrombaocao/{baocaoId}")
	public ResponseEntity<?> getBmSheetFromBmBaoCaoDropDown(@PathVariable("baocaoId") int baocaoId)
			throws ApiRequestException {
		try {

			List<DropDownDTO> bmSheetoDropDownRsl = new ArrayList<DropDownDTO>();
			bmSheetoDropDownRsl = this.sheetService.getSheetFromBcDropdownService(baocaoId);
			return new ResponseEntity<List<DropDownDTO>>(bmSheetoDropDownRsl, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	private boolean checkKeySort(String keySort) {

//		Field fld[] = ChucVuDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

//		for (int i = 0; i < fld.length; i++) {
//			listName.add(fld[i].getName());
//		}

		return listName.contains(keySort);
	}

}

//------------------- Delete All Users-----------------------------

//	@RequestMapping(value = "/chucvu/", method = RequestMethod.DELETE)
//	@DeleteMapping("/user/")
//	public ResponseEntity<UserBDTO> deleteAllUsers() {
//		//logger.info("Deleting All Users");
//		try {
//			userService.deleteAllUsers();
//			return new ResponseEntity<UserBDTO>(HttpStatus.NO_CONTENT);
//		} catch (Exception e) {
//			return new ResponseEntity(new ApiRequestException("Unable to delete."), HttpStatus.NOT_FOUND);
//		}
//
//	}
