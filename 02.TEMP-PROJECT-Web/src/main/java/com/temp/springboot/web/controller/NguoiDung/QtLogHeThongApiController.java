package com.temp.springboot.web.controller.NguoiDung;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.net.HttpHeaders;
import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.QtLogHeThongBDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.NguoiDung.QtLogHeThongService;
import com.temp.utils.CellConfig;
import com.temp.utils.Constants;
import com.temp.utils.TimestampUtils;
import com.temp.utils.Utils;

@CrossOrigin(origins = Utils.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class QtLogHeThongApiController {

	public static final Logger logger = LoggerFactory.getLogger(QtLogHeThongApiController.class);

	@Autowired
	@Qualifier("QtLogHeThongServiceImpl")
	private QtLogHeThongService qtService;

	@RequestMapping(value = "/qtloghethong/filter", method = RequestMethod.GET)
	public ResponseEntity<QtLogHeThongBDTO> listQtLogHeThongs(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,	
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "ipThucHien", required = false) String ipThucHien,
			@RequestParam(value = "noiDung", required = false) String noiDung,
			@RequestParam(value = "tuNgay", required = false) String tuNgay,
			@RequestParam(value = "denNgay", required = false) String denNgay,
			@RequestParam(value = "logType", required = false) String logType,
			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
			@RequestParam(value = "trangThai", required = false) String trangThai) {

		if (!checkKeySort(keySort)) {

			QtLogHeThongBDTO bdto = new QtLogHeThongBDTO();
			bdto.setMessage("truyền sai keySort");

			return new ResponseEntity<QtLogHeThongBDTO>(bdto, HttpStatus.METHOD_NOT_ALLOWED);
		}

		QtLogHeThongBDTO lstQtLogHeThong = new QtLogHeThongBDTO();

		try {

			logger.info(Constants.Logs.LIST);
			if (pageNo > 0) {
				pageNo = pageNo - 1;
			}
			lstQtLogHeThong = qtService.listQtLogHeThongs(strfilter, ipThucHien, noiDung, tuNgay, denNgay, logType,
					pageNo, pageSize, keySort, desc, trangThai, taiKhoan);

			return new ResponseEntity<QtLogHeThongBDTO>(lstQtLogHeThong, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QtLogHeThongBDTO>(lstQtLogHeThong, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/qtloghethong/export", method = RequestMethod.GET)
	public ResponseEntity<ByteArrayResource> ExportExcel(
			@Valid @RequestParam(value = "strfilter", required = false) String strfilter,
			@RequestParam(name = "pageNo", defaultValue = "0") Integer pageNo,
			@RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
			@RequestParam(name = "keySort", defaultValue = "id") String keySort,
			@RequestParam(name = "desc", defaultValue = "false") boolean desc,
			@RequestParam(value = "ipThucHien", required = false) String ipThucHien,
			@RequestParam(value = "noiDung", required = false) String noiDung,
			@RequestParam(value = "tuNgay", required = false) String tuNgay,
			@RequestParam(value = "denNgay", required = false) String denNgay,
			@RequestParam(value = "taiKhoan", required = false) String taiKhoan,
			@RequestParam(value = "logType", required = false) String logType,
			@RequestParam(value = "trangThai", required = false) String trangThai)

			throws ApiRequestException {
		QtLogHeThongBDTO list = new QtLogHeThongBDTO();
		Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("sheet1");

		CellStyle style = CellConfig.createStyleForTitleTBCN(sheet, workbook);

		list = qtService.listQtLogHeThongs(strfilter, ipThucHien, noiDung, tuNgay, denNgay, logType, pageNo, pageSize,
				keySort, desc, trangThai, taiKhoan);
		Integer rownum = 0;
		Cell cell;
		Row row;

		row = sheet.createRow(rownum);

		cell = row.createCell(0, CellType.STRING);
		cell.setCellValue("STT");
		cell.setCellStyle(style);

		cell = row.createCell(1, CellType.STRING);
		cell.setCellValue("Tài khoản");
		cell.setCellStyle(style);

		cell = row.createCell(2, CellType.STRING);
		cell.setCellValue("Nội dung");
		cell.setCellStyle(style);

		cell = row.createCell(3, CellType.STRING);
		cell.setCellValue("Log Type");
		cell.setCellStyle(style);

		cell = row.createCell(4, CellType.STRING);
		cell.setCellValue("Thời gian thực hiện");
		cell.setCellStyle(style);

		// Data

		if (list != null && list.getLstQtLogHeThong() != null) {
			for (QtLogHeThongDTO dto : list.getLstQtLogHeThong()) {
				rownum++;
				row = sheet.createRow(rownum);

				cell = row.createCell(0, CellType.STRING);
				cell.setCellValue(rownum);
				CellConfig.BorderTable(cell, sheet);

				cell = row.createCell(1, CellType.STRING);
				cell.setCellValue(dto.getTaiKhoan());
				CellConfig.BorderTable(cell, sheet);

				cell = row.createCell(2, CellType.STRING);
				cell.setCellValue(dto.getNoiDung());
				CellConfig.BorderTable(cell, sheet);

				cell = row.createCell(3, CellType.STRING);
				cell.setCellValue(dto.getLogType());
				CellConfig.BorderTable(cell, sheet);

				cell = row.createCell(4, CellType.STRING);
				cell.setCellValue(TimestampUtils.TimestampToString_ddMMyyyyHHmmss(dto.getNgayTao()));
				CellConfig.BorderTable(cell, sheet);

			}
		}
		CellConfig.autoSizeColumns(workbook);

		String fileName = "log_he_thong_list.xls";

		try {
			FileOutputStream out = null;
			if (workbook instanceof HSSFWorkbook) {
				out = new FileOutputStream(fileName);
			} else if (workbook instanceof XSSFWorkbook) {
				out = new FileOutputStream("log_he_thong_list.xlsx");
			}
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			workbook.write(bos);

			byte[] bytes = bos.toByteArray();
			ByteArrayResource resource = new ByteArrayResource(bytes);

			out.close();
			workbook.close();
			return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION).contentLength(bytes.length) //
					.body(resource);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private boolean checkKeySort(String keySort) {

		Field fld[] = QtLogHeThongDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}

	@PostMapping("/qtloghethong/deletemulti")
	public ResponseEntity<?> deleteMulti(@Valid @RequestBody List<Integer> lstId, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.DELETE);
			if (qtService.deleteMulti(lstId)) {

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_LOG_HE_THONG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa " + lstId.size() + " log hệ thống");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/qtloghethong/xoa/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.DELETE);

			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {

				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_UPDATE + "_QT_LOG_HE_THONG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa log hệ thống");
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
		}
	}
}