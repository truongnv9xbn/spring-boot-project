package com.temp.springboot.web.controller.NguoiDung;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.temp.global.UserInfoGlobal;
import com.temp.global.WriteLogInDataBaseGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.NguoiDung.QuyenBDTO;
import com.temp.model.NguoiDung.QuyenDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.SubQuyenDTO;
import com.temp.service.NguoiDung.QuyenService;
import com.temp.utils.Constants;
import com.temp.utils.Utils;

@CrossOrigin(origins = Constants.BaseUrl)
@RestController
@RequestMapping("/api/v1")
public class QuyenApiController {

	public static final Logger logger = LoggerFactory.getLogger(QuyenApiController.class);

	@Autowired
	private QuyenService qtService;

	@RequestMapping(value = "/quyen/list", method = RequestMethod.GET)
	public ResponseEntity<QuyenBDTO> getList(
			@RequestParam(value = "keySearch", required = false) String keySearch,
			@RequestParam(value = "tenQuyen", required = false) String tenQuyen,
			@RequestParam(value = "maQuyen", required = false) String maQuyen,
			@RequestParam(value = "actionId", required = false) Integer actionId,
			@RequestParam(value = "phanHe", required = false) String phanHe,
			@RequestParam(value = "ghiChu", required = false) String ghiChu,
			@RequestParam(value = "trangThai", required = false) String trangThai) {
		QuyenBDTO bdto = new QuyenBDTO();
		logger.info(Constants.Logs.LIST);
		try {

			bdto = qtService.getAll(keySearch, tenQuyen, maQuyen, actionId, phanHe, ghiChu, trangThai);

			return new ResponseEntity<QuyenBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QuyenBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/quyen/exitsMa", method = RequestMethod.GET)
	public ResponseEntity<Boolean> checkExitsByMa(
			@RequestParam(value = "maQuyen", required = false) String maQuyen,
			@RequestParam(value = "id", required = false) Integer id) throws ApiRequestException {
		Boolean result=false;

		try {
			logger.info(Constants.Logs.EXITS);
			
			result = qtService.isExistByMa(maQuyen,id);

			return new ResponseEntity<Boolean>(result, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			throw new ApiRequestException(Constants.Messages.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
		}
	}

	/**
	 * Create quyền
	 * 
	 * @param qt json object
	 * @return ResponseEntity data json
	 * @throws ResponseEntity Data json
	 */

	@PostMapping("/quyen/add")
	public ResponseEntity<?> add(@Valid @RequestBody QuyenDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.CREATE);

			if (!qtService.isExistQuyen(dto)) {
				if (qtService.saveQuyen(dto)) {
					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_CHUC_NANG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung(
							"Tài khoản " + userInfo.getTaiKhoan() + " Thêm mới quyền " + dto.getTenQuyen());
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

					return new ResponseEntity<QuyenDTO>(dto, HttpStatus.CREATED);
				}
			}
			throw new ApiRequestException(Constants.Messages.RC_EXIST, HttpStatus.CONFLICT);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.CREATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}
	
	@PutMapping("/quyen/changestatus/")
	public ResponseEntity<?> changeStatus(@RequestBody SubQuyenDTO dto, @Context HttpServletRequest request)
			throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.CREATE);

			if (qtService.isExistById(dto.getId())) {
				if (qtService.changeStatus(dto)) {

					QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
					// set IP thực hiện lấy IP remote client
					dtoLog.setIpThucHien(Utils.getClientIp(request));
					dtoLog.setLogType(Constants.LogSystem.LOG_SYS_ADD + "_CHUC_NANG");
					dtoLog.setNgayTao(timeStamp);
					dtoLog.setNguoiTaoId(userInfo.getId());
					dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " thay đổi trang thái quyền có id ="
							+ dto.getId());
					// save db
					WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);

					return new ResponseEntity<SubQuyenDTO>(dto, HttpStatus.CREATED);
				}
			}

			return new ResponseEntity<Object>(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	/**
	 * Cập nhật quyền
	 * 
	 * @param qtBdto 1 đối tượng chứa id > 0
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@PutMapping("/quyen/update")
	public ResponseEntity<?> update(@Valid @RequestBody QuyenDTO dto) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.UPDATE);

			if (qtService.isExistById(dto.getId())) {
				qtService.updateQuyen(dto);
				return new ResponseEntity<QuyenDTO>(dto, HttpStatus.ACCEPTED);
			} else {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.UPDATE_FAIL, HttpStatus.EXPECTATION_FAILED);

		}

	}

	@GetMapping("/quyen/detail/{id}")
	public ResponseEntity<?> detail(@PathVariable("id") int id) throws ApiRequestException {
		try {
			logger.info(Constants.Logs.GETBYID);
			QuyenDTO dto = qtService.findById(id);
			if (dto == null) {
				throw new ApiRequestException(Constants.Messages.RC_NOT_EXIST, HttpStatus.BAD_REQUEST);
			}
			return ResponseEntity.ok().body(dto);

		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.GETBYID_FAIL, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Xóa 1 quyền
	 * 
	 * @param id của quyền
	 * @return ResponseEntity data json
	 * @throws ApiRequestException
	 */
	@DeleteMapping("/quyen/delete/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") int id, @Context HttpServletRequest request) throws ApiRequestException {
		// lấy thông tin user
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Timestamp timeStamp = new Timestamp(new Date().getTime());
		try {
			logger.info(Constants.Logs.DELETE);

			if (!qtService.isExistById(id)) {
				return new ResponseEntity<>(new ApiRequestException(Constants.Messages.RC_NOT_EXIST),
						HttpStatus.NOT_FOUND);
			}
			if (qtService.deleteById(id)) {
				QtLogHeThongDTO dtoLog = new QtLogHeThongDTO();
				// set IP thực hiện lấy IP remote client
				dtoLog.setIpThucHien(Utils.getClientIp(request));
				dtoLog.setLogType(Constants.LogSystem.LOG_SYS_DELETE + "_CHUC_NANG");
				dtoLog.setNgayTao(timeStamp);
				dtoLog.setNguoiTaoId(userInfo.getId());
				dtoLog.setNoiDung("Tài khoản " + userInfo.getTaiKhoan() + " xóa quyền có id = " + id);
				// save db
				WriteLogInDataBaseGlobal.WriteLogDB(dtoLog);
				
				return new ResponseEntity<Object>(Constants.Messages.DELETE_SUCCESS, HttpStatus.NO_CONTENT);
			} else {
				throw new ApiRequestException(Constants.Messages.DELETE_FAIL, HttpStatus.NOT_FOUND);
			}
//			return new ResponseEntity<Object>(Constants.Messages.DELETE_FAIL, HttpStatus.BAD_REQUEST);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ApiRequestException(Constants.Messages.RC_USED, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/quyen/dropdownkhoacha", method = RequestMethod.GET)
	public ResponseEntity<QuyenBDTO> getDropdownKhoaChaId(
			@RequestParam(value = "trangThai", required = false) String trangThai) {
		logger.info(Constants.Logs.LIST);
		QuyenBDTO bdto = new QuyenBDTO();

		try {
			bdto = qtService.getDropdownKhoaChaId();
			return new ResponseEntity<QuyenBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QuyenBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/quyen/dropdownaction", method = RequestMethod.GET)
	public ResponseEntity<QuyenBDTO> getDropdownAction(
			@RequestParam(value = "trangThai", required = false) String trangThai) {
		logger.info(Constants.Logs.LIST);
		QuyenBDTO bdto = new QuyenBDTO();

		try {
			bdto = qtService.getDropdownAction();
			return new ResponseEntity<QuyenBDTO>(bdto, HttpStatus.OK);

		} catch (Exception e) {
			logger.error(e.toString());
			return new ResponseEntity<QuyenBDTO>(bdto, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@SuppressWarnings("unused")
	private boolean checkKeySort(String keySort) {

		Field fld[] = QuyenDTO.class.getDeclaredFields();
		List<String> listName = new ArrayList<String>();

		for (int i = 0; i < fld.length; i++) {
			listName.add(fld[i].getName());
		}

		return listName.contains(keySort);
	}
}