/**
 * 
 */
package com.temp.springboot.web.apps;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.temp.authencite.config.FileStorageProperties;
import com.temp.global.ThamSoHeThongGlobal;
import com.temp.global.UserInfoGlobal;
import com.temp.global.exception.ApiRequestException;
import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.file.UploadPathDTO;
import com.temp.service.NguoiDung.ChungThuSoService;
import com.temp.utils.Constant;
import com.temp.utils.PasswordGenerator;
import com.temp.utils.Utils;

/**
 * @author thang
 *
 */
@Service
public class FileStorage {

	private final Path fileStorageLocation;
	
	public Path getFileStorageLocation() {
		return fileStorageLocation;
	}

//	@Autowired
//	@Qualifier("ChungThuSoServiceImpl")
//	private ChungThuSoService ctsService;

	@Autowired
	public FileStorage(FileStorageProperties fileStorageProperties) throws ApiRequestException {
		this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();

		try {
			Files.createDirectories(this.fileStorageLocation);
		} catch (Exception ex) {
			throw new ApiRequestException("Could not create the directory where the uploaded files will be stored.",
					ex);
		}
	}
	
	//ham upload check ky so
	public UploadPathDTO storeFile(MultipartFile file) throws ApiRequestException {
		// Normalize file name
		String[] splitFile = StringUtils.cleanPath(file.getOriginalFilename()).split("\\.");
		String fileName = org.apache.commons.lang3.StringUtils.EMPTY;
		String extentionFile = org.apache.commons.lang3.StringUtils.EMPTY;
		long size = file.getSize();
		BigDecimal sizeBig = new BigDecimal(size);
		sizeBig = sizeBig.divide(new BigDecimal(1024)).divide(new BigDecimal(1024));
		double sizeMegaByte = sizeBig.doubleValue();
		ThamSoHeThongDTO thamSo = ThamSoHeThongGlobal.giaTriThamSo(Constant.SIZE_FILE_CHECK);
		if (thamSo != null && thamSo.getGiaTri() != null) {
			double mockSize = Double.parseDouble(thamSo.getGiaTri());
			if (mockSize < sizeMegaByte) {
				throw new ApiRequestException("File đã vượt quá dung lượng, dung lượng cho tối đa phép là " + mockSize+" MB",
						HttpStatus.FOUND);
			}
		}

		String checkCks = "";
		int sizeLength = splitFile.length;
		QtNguoiDungDTO user = UserInfoGlobal.getUserInfoAuthor();
//		List<Object> ctsUser = ctsService.findByUserId(user.getId());
//		List<ChungThuSoDTO> ctsUserDto  = null;
//		if(ctsUser != null && ctsUser.size() > 0) {
//			ctsUserDto = new ArrayList<ChungThuSoDTO>();
//			ctsUserDto = ObjectMapperUtils.mapAll(ctsUser, ChungThuSoDTO.class);
//		}
		
		if (splitFile != null && sizeLength > 0) {
			fileName = splitFile[0];
			extentionFile = splitFile[sizeLength - 1];
//			if(user.chuKySo == true) {
//				if(extentionFile.equals("pdf")) 
//				{
//					try {
//						checkCks = Utils.verifyPdfSignatures(file, ctsUser);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//				else if(extentionFile.equals("xlsx") || extentionFile.equals("xls") || extentionFile.equals("doc")
//						|| extentionFile.equals("docx"))
//				{
//					try {
//							checkCks = Utils.verifyMicrosoftOfficeSignatures(file, ctsUser);
//					} catch (Exception e) {
//							e.printStackTrace();
//					}
//					
//				}

			
//		}
			
		}
//		if(user.chuKySo == true && checkCks == false) {
//			throw new ApiRequestException("File chưa có ký số hoặc ký số không hợp lệ", HttpStatus.FOUND);
//		}
//		else {
		String defineFileName = org.apache.commons.lang3.StringUtils.EMPTY;
		Date date = new Date();
		if (fileName.contains("..")) {
			throw new ApiRequestException("Sorry! Filename contains invalid path sequence " + fileName,
					HttpStatus.FOUND);
		} else if (!Utils.EXTENTION_FILE.contains(extentionFile.toLowerCase())) {
			throw new ApiRequestException("Định dạng không hợp lệ " + fileName, HttpStatus.FOUND);
		}
		try {
			// Check if the file's name contains invalid characters

			fileName = fileName.replaceAll("~", "");
			fileName = fileName.replaceAll("`", "");
			defineFileName = fileName + date.getTime() + PasswordGenerator.generateRandomPassword() + "."
					+ extentionFile;
			// Copy file to the target location (Replacing existing file with the same name)
			Path targetLocation = this.fileStorageLocation.resolve(defineFileName);
			Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
			// //System.out.println(targetLocation.relativize(targetLocation.getFileName().toAbsolutePath()));

			//System.out.println(targetLocation.toFile().getPath());
			//System.out.println(targetLocation.toString());
			UploadPathDTO result = new UploadPathDTO();
			result.setFileName(defineFileName);
			result.setFileNameOriginal(file.getOriginalFilename());

			result.setPathFile("uploadFileTmp/" + defineFileName);
			result.setSize(file.getSize());
			result.setExtenstionFile(extentionFile);
//			if(user.chuKySo == true && !checkCks.isEmpty()) {
//				result.setMessage(checkCks);
//			}
			if(!checkCks.isEmpty()) {
				result.setMessage(checkCks);
			}
			
			return result;
			
		} catch (IOException ex) {
			//System.out.println(ex.getMessage());
			throw new ApiRequestException("Could not store file " + fileName + ". Please try again!", ex);
		}
//		}
	}
	
	//ham upload ko check ky so
	public UploadPathDTO storeFileImport(MultipartFile file) throws ApiRequestException {
		// Normalize file name
				String[] splitFile = StringUtils.cleanPath(file.getOriginalFilename()).split("\\.");
				String fileName = org.apache.commons.lang3.StringUtils.EMPTY;
				String extentionFile = org.apache.commons.lang3.StringUtils.EMPTY;
				long size = file.getSize();
				BigDecimal sizeBig = new BigDecimal(size);
				sizeBig = sizeBig.divide(new BigDecimal(1024)).divide(new BigDecimal(1024));
				double sizeMegaByte = sizeBig.doubleValue();
				ThamSoHeThongDTO thamSo = ThamSoHeThongGlobal.giaTriThamSo(Constant.SIZE_FILE_CHECK);
				if (thamSo != null && thamSo.getGiaTri() != null) {
					double mockSize = Double.parseDouble(thamSo.getGiaTri());
					if (mockSize < sizeMegaByte) {
						throw new ApiRequestException("File đã vượt quá dung lượng, dung lượng cho tối đa phép là " + mockSize+" MB",
								HttpStatus.FOUND);
					}
				}

				String checkCks = "";
				int sizeLength = splitFile.length;
//				QtNguoiDungDTO user = UserInfoGlobal.getUserInfoAuthor();
//				List<Object> ctsUser = ctsService.findByUserId(user.getId());
//				List<ChungThuSoDTO> ctsUserDto  = null;
//				if(ctsUser != null && ctsUser.size() > 0) {
//					ctsUserDto = new ArrayList<ChungThuSoDTO>();
//					ctsUserDto = ObjectMapperUtils.mapAll(ctsUser, ChungThuSoDTO.class);
//				}
				
				if (splitFile != null && sizeLength > 0) {
					fileName = splitFile[0];
					extentionFile = splitFile[sizeLength - 1];
//					if(user.chuKySo == true) {
//						if(extentionFile.equals("pdf")) 
//						{
//							try {
//								checkCks = Utils.verifyPdfSignatures(file, ctsUser);
//							} catch (Exception e) {
//								e.printStackTrace();
//							}
//						}
//						else if(extentionFile.equals("xlsx") || extentionFile.equals("xls") || extentionFile.equals("doc")
//								|| extentionFile.equals("docx"))
//						{
//							try {
//									checkCks = Utils.verifyMicrosoftOfficeSignatures(file, ctsUser);
//							} catch (Exception e) {
//									e.printStackTrace();
//							}
//							
//						}
//
//						
//					
				}
//					
//				}
//				if(user.chuKySo == true && checkCks == false) {
//					throw new ApiRequestException("File chưa có ký số hoặc ký số không hợp lệ", HttpStatus.FOUND);
//				}
//				else {
				String defineFileName = org.apache.commons.lang3.StringUtils.EMPTY;
				Date date = new Date();
				if (fileName.contains("..")) {
					throw new ApiRequestException("Sorry! Filename contains invalid path sequence " + fileName,
							HttpStatus.FOUND);
				} else if (!Utils.EXTENTION_FILE.contains(extentionFile.toLowerCase())) {
					throw new ApiRequestException("Định dạng không hợp lệ " + fileName, HttpStatus.FOUND);
				}
				try {
					// Check if the file's name contains invalid characters

					fileName = fileName.replaceAll("~", "");
					fileName = fileName.replaceAll("`", "");
					defineFileName = fileName + date.getTime() + PasswordGenerator.generateRandomPassword() + "."
							+ extentionFile;
					// Copy file to the target location (Replacing existing file with the same name)
					Path targetLocation = this.fileStorageLocation.resolve(defineFileName);
					Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
					// //System.out.println(targetLocation.relativize(targetLocation.getFileName().toAbsolutePath()));

					//System.out.println(targetLocation.toFile().getPath());
					//System.out.println(targetLocation.toString());
					UploadPathDTO result = new UploadPathDTO();
					result.setFileName(defineFileName);
					result.setFileNameOriginal(file.getOriginalFilename());

					result.setPathFile("uploadFileTmp/" + defineFileName);
					result.setSize(file.getSize());
					result.setExtenstionFile(extentionFile);
//					if(user.chuKySo == true && !checkCks.isEmpty()) {
//						result.setMessage(checkCks);
//					}
					return result;
					
				} catch (IOException ex) {
					//System.out.println(ex.getMessage());
					throw new ApiRequestException("Could not store file " + fileName + ". Please try again!", ex);
				}
//				}
			}
				
	
//	public UploadPathDTO storeFileRemix(File file) throws ApiRequestException {
//		// Normalize file name
//		String[] splitFile = StringUtils.cleanPath(file.getName()).split("\\.");
//		String fileName = org.apache.commons.lang3.StringUtils.EMPTY;
//		String extentionFile = org.apache.commons.lang3.StringUtils.EMPTY;
//		long size = file.length();
//		BigDecimal sizeBig = new BigDecimal(size);
//		sizeBig = sizeBig.divide(new BigDecimal(1024)).divide(new BigDecimal(1024));
//		double sizeMegaByte = sizeBig.doubleValue();
//		ThamSoHeThongDTO thamSo = ThamSoHeThongGlobal.giaTriThamSo(Constant.SIZE_FILE_CHECK);
//		if (thamSo != null && thamSo.getGiaTri() != null) {
//			double mockSize = Double.parseDouble(thamSo.getGiaTri());
//			if (mockSize < sizeMegaByte) {
//				throw new ApiRequestException("File đã vượt quá dung lượng, dung lượng cho tối đa phép là " + mockSize+" MB",
//						HttpStatus.FOUND);
//			}
//		}
//		int sizeLength = splitFile.length;
//		if (splitFile != null && sizeLength > 
//		0) {
//			fileName = splitFile[0];
//			extentionFile = splitFile[sizeLength - 1];
////			if(user.chuKySo == true) {
//			}
//			
////		}
//
//		String defineFileName = org.apache.commons.lang3.StringUtils.EMPTY;
//		Date date = new Date();
//		if (fileName.contains("..")) {
//			throw new ApiRequestException("Sorry! Filename contains invalid path sequence " + fileName,
//					HttpStatus.FOUND);
//		} else if (!Utils.EXTENTION_FILE.contains(extentionFile.toLowerCase())) {
//			throw new ApiRequestException("Định dạng không hợp lệ " + fileName, HttpStatus.FOUND);
//		}
//		try {
//			// Check if the file's name contains invalid characters
//
//			fileName = fileName.replaceAll("~", "");
//			fileName = fileName.replaceAll("`", "");
//			defineFileName = fileName + date.getTime() + PasswordGenerator.generateRandomPassword() + "."
//					+ extentionFile;
//			// Copy file to the target location (Replacing existing file with the same name)
//			Path targetLocation = this.fileStorageLocation.resolve(defineFileName);
//			Files.copy(new FileInputStream(file), targetLocation, StandardCopyOption.REPLACE_EXISTING);
//			// //System.out.println(targetLocation.relativize(targetLocation.getFileName().toAbsolutePath()));
//
//			//System.out.println(targetLocation.toFile().getPath());
//			//System.out.println(targetLocation.toString());
//			UploadPathDTO result = new UploadPathDTO();
//			result.setFileName(defineFileName);
//			result.setFileNameOriginal(file.getName());
//
//			result.setPathFile("uploadFileTmp/" + defineFileName);
//			result.setSize(file.length());
//			result.setExtenstionFile(extentionFile);
//			return result;
//		} catch (IOException ex) {
//			//System.out.println(ex.getMessage());
//			throw new ApiRequestException("Could not store file " + fileName + ". Please try again!", ex);
//		}
//	}

}
