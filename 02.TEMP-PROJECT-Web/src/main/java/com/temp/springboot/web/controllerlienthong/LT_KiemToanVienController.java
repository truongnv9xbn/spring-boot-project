//package com.temp.springboot.web.controllerlienthong;
//
//import javax.validation.Valid;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpStatus;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//
//import com.example.DTO_LienThong.KiemToanVienXmlBDTO;
//
//import com.temp.global.exception.ApiRequestException;
//import com.temp.kiemtoanvienService.KiemToanVienService;
//import com.temp.utils.Constant;
//
//@RestController
//@RequestMapping("/lien-thong")
//public class LT_KiemToanVienController {
//	@Autowired
//	private KiemToanVienService kiemToanVienService;
//	
//	@RequestMapping(value = "/ktv/filter", method = RequestMethod.GET)
//	public String listKiemToanViens(
//			@Valid @RequestParam(value = "r", required = false) String r,
//			@Valid @RequestParam(value = "p", required = false) String p) throws ApiRequestException{ 
//		try {
//			String a = "Thất bại";
//			KiemToanVienXmlBDTO result = new KiemToanVienXmlBDTO();
//			result = kiemToanVienService.getByMaRP(r, p);
//			if (result != null) {
//				a = "Thành công";
//			}
//			return a;
//
//		} catch (Exception e) {
//			throw new ApiRequestException(Constant.EXCEPTION_FAIL, HttpStatus.EXPECTATION_FAILED);
//		}
//	}
//}
