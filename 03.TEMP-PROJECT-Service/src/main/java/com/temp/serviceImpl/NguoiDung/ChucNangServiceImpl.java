package com.temp.serviceImpl.NguoiDung;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.temp.logic.NguoiDung.ChucNangLogic;
import com.temp.model.NguoiDung.ChucNangBDTO;
import com.temp.model.NguoiDung.ChucNangDTO;
import com.temp.model.NguoiDung.SubChucNangDTO;
import com.temp.service.NguoiDung.ChucNangService;

@Service("ChucNangServiceImpl")
@Transactional
public class ChucNangServiceImpl implements ChucNangService {

	@Autowired
	private ChucNangLogic qtLogic;

	@Override
	public ChucNangDTO findById(int id) throws Exception {
		return qtLogic.getById(id);
	}


	@Override
	public boolean saveChucNang(ChucNangDTO dto) throws Exception {
		return qtLogic.createChucNang(dto);
	}

	@Override
	public boolean updateChucNang(ChucNangDTO dto) throws Exception {
		return qtLogic.upDateChucNang(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return qtLogic.deleteChucNang(id);
	}


	@Override
	public ChucNangBDTO listChucNangs(String keySearch, String tenChucNang, String maChucNang,  Integer actionId, String phanHe,String ghiChu,String trangThai) {

		return qtLogic.listChucNangs(keySearch, tenChucNang, maChucNang, actionId, phanHe, ghiChu, trangThai);
	}

	@Override
	public boolean isExistChucNang(ChucNangDTO dto) {
	
		return qtLogic.isExistChucNang(dto);
		 
	}


	@Override
	public boolean isExistById(int id) {
		
		return qtLogic.isExistById(id);
	}


	@Override
	public ChucNangBDTO getDropdownKhoaChaId() {
		return qtLogic.getDropdownKhoaChaId();
	}


	@Override
	public boolean changeStatus(SubChucNangDTO dto) throws Exception {
		return qtLogic.changeStatus(dto);
	}


	@Override
	public ChucNangBDTO getDropdownAction() {
		return qtLogic.getDropdownAction();
	}


	@Override
	public ChucNangBDTO getAll(String keySearch, String tenChucNang, String maChucNang, Integer action, String phanHe,
			String ghiChu, String trangThai) {
		return qtLogic.getAll(keySearch, tenChucNang, maChucNang, action, phanHe, ghiChu, trangThai);
	}


	@Override
	public boolean isExistByMa(String maChucNang, Integer id) {
		return qtLogic.isExistByMa(maChucNang,id);
	}

}
