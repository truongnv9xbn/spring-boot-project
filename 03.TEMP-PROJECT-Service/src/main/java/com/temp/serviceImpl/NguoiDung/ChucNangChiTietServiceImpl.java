package com.temp.serviceImpl.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.temp.logic.NguoiDung.ChucNangChiTietLogic;
import com.temp.model.NguoiDung.ChucNangChiTietBDTO;
import com.temp.model.NguoiDung.ChucNangChiTietDTO;
import com.temp.service.NguoiDung.ChucNangChiTietService;

@Service("ChucNangChiTietServiceImpl")
@Transactional
public class ChucNangChiTietServiceImpl implements ChucNangChiTietService{
	
	@Autowired
	private ChucNangChiTietLogic chucNangChiTietLogic;

	@Override
	public ChucNangChiTietBDTO filter(String keySearch, String tenChucNang, String trangThai,Integer pageNo,Integer pageSize, String keySort, boolean desc) {
		return chucNangChiTietLogic.filter(keySearch,tenChucNang,trangThai, pageNo, pageSize, keySort, desc);
	}

	@Override
	public boolean addOrUpdate(ChucNangChiTietDTO dto) throws Exception {
		return chucNangChiTietLogic.addOrUpDate(dto);
	}

	@Override
	public ChucNangChiTietDTO findById(int id) throws Exception {
		return chucNangChiTietLogic.getById(id);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return chucNangChiTietLogic.deleteById(id);
	}
	

}
