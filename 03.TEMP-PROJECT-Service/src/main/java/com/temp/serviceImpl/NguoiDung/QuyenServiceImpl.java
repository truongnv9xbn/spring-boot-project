package com.temp.serviceImpl.NguoiDung;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.temp.logic.NguoiDung.QuyenLogic;
import com.temp.model.NguoiDung.QuyenBDTO;
import com.temp.model.NguoiDung.QuyenDTO;
import com.temp.model.NguoiDung.SubQuyenDTO;
import com.temp.service.NguoiDung.QuyenService;

@Service("QuyenServiceImpl")
@Transactional
public class QuyenServiceImpl implements QuyenService {

	@Autowired
	private QuyenLogic qtLogic;

	@Override
	public QuyenDTO findById(int id) throws Exception {
		return qtLogic.getById(id);
	}


	@Override
	public boolean saveQuyen(QuyenDTO dto) throws Exception {
		return qtLogic.createQuyen(dto);
	}

	@Override
	public boolean updateQuyen(QuyenDTO dto) throws Exception {
		return qtLogic.upDateQuyen(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return qtLogic.deleteQuyen(id);
	}


	@Override
	public QuyenBDTO listQuyens(String keySearch, String tenQuyen, String maQuyen,  Integer actionId, String phanHe,String ghiChu,String trangThai) {

		return qtLogic.listQuyens(keySearch, tenQuyen, maQuyen, actionId, phanHe, ghiChu, trangThai);
	}

	@Override
	public boolean isExistQuyen(QuyenDTO dto) {
	
		return qtLogic.isExistQuyen(dto);
		 
	}


	@Override
	public boolean isExistById(int id) {
		
		return qtLogic.isExistById(id);
	}


	@Override
	public QuyenBDTO getDropdownKhoaChaId() {
		return qtLogic.getDropdownKhoaChaId();
	}


	@Override
	public boolean changeStatus(SubQuyenDTO dto) throws Exception {
		return qtLogic.changeStatus(dto);
	}


	@Override
	public QuyenBDTO getDropdownAction() {
		return qtLogic.getDropdownAction();
	}


	@Override
	public QuyenBDTO getAll(String keySearch, String tenQuyen, String maQuyen, Integer action, String phanHe,
			String ghiChu, String trangThai) {
		return qtLogic.getAll(keySearch, tenQuyen, maQuyen, action, phanHe, ghiChu, trangThai);
	}


	@Override
	public boolean isExistByMa(String maQuyen, Integer id) {
		return qtLogic.isExistByMa(maQuyen,id);
	}

}
