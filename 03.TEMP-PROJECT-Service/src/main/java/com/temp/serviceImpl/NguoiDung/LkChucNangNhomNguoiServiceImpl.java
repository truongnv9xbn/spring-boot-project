package com.temp.serviceImpl.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.temp.logic.NguoiDung.LkChucNangNhomNguoiLogic;
import com.temp.model.NguoiDung.LkChucNangNhomNguoiBDTO;
import com.temp.model.NguoiDung.LkChucNangNhomNguoiDTO;
import com.temp.service.NguoiDung.LkChucNangNhomNguoiService;

@Service("LkChucNangNhomNguoiServiceImpl")
@Transactional
public class LkChucNangNhomNguoiServiceImpl implements LkChucNangNhomNguoiService {

	@Autowired
	private LkChucNangNhomNguoiLogic lkLogic;

	@Override
	public LkChucNangNhomNguoiBDTO filterChucVu(String filterkey, Integer pageNo, Integer pageSize, String keySort,
			boolean desc) {

		return lkLogic.listLkChucNangNhomNguois(filterkey, pageNo, pageSize, keySort, desc);
	}

	@Override
	public boolean deleteLkChucNangNhomNguoi(int nhomNguoiDungId) throws Exception {
		return lkLogic.deleteLkChucNangNhomNguoi(nhomNguoiDungId);
	}

	@Override
	public boolean save(LkChucNangNhomNguoiDTO dto) throws Exception {
		return lkLogic.add(dto);
	}

	@Override
	public boolean update(LkChucNangNhomNguoiDTO dto) throws Exception {

		return lkLogic.updatekChucNangNhomNguoi(dto);
	}

	@Override
	public LkChucNangNhomNguoiDTO findByNhomNguoiDungId(int nhomNguoiDungId) throws Exception {

		return lkLogic.getByNhomNguoiDungId(nhomNguoiDungId);
	}

	@Override
	public boolean isLkChucNangNhomNguoiExistAdd(int nhomNguoiDungId, int chucNangId) {
		// TODO Auto-generated method stub
		return lkLogic.isLkChucNangNhomNguoiExistAdd(nhomNguoiDungId, chucNangId);
	}

//	@Override
//	public boolean updatekChucNangNhomNguoi(int nhomNguoiDungId, int chucNangId) throws Exception {
//		return lkLogic.updatekChucNangNhomNguoi(nhomNguoiDungId, chucNangId);
//	}

//	@Override
//	public boolean isExistByNhomNguoiDungId(int nhomNguoiDungId) {
//
//		return lkLogic.isExistById(nhomNguoiDungId);
//	}

}
