package com.temp.serviceImpl.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.global.UserInfoGlobal;
import com.temp.logic.NguoiDung.QtNguoiDungLogic;
import com.temp.logic.NguoiDung.QtNhomNguoiDungLogic;
import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.NhomNguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungDTO;
import com.temp.service.NguoiDung.QtNhomNguoiDungService;

@Service("QtNhomNguoiDungServiceImpl")
public class QtNhomNguoiDungServiceImpl implements QtNhomNguoiDungService {

	@Autowired
	private QtNhomNguoiDungLogic qtnndLogic;

	@Autowired
	private QtNguoiDungLogic qtndLogic;

	@Override
	public QtNhomNguoiDungDTO findById(int id) throws Exception {
		return qtnndLogic.getById(id);
	}

	@Override
	public QtNhomNguoiDungDTO createQtNhomNguoiDung(QtNhomNguoiDungDTO dto) throws Exception {
		return qtnndLogic.createQtNhomNguoiDung(dto);
	}

	@Override
	public QtNhomNguoiDungDTO updateQtNhomNguoiDung(QtNhomNguoiDungDTO dto) throws Exception {
		QtNhomNguoiDungDTO dtoRespon = new QtNhomNguoiDungDTO();
		dtoRespon = qtnndLogic.upDateQtNhomNguoiDung(dto);
		return dtoRespon;
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return qtnndLogic.deleteQtNhomNguoiDungById(id);
	}

	@Override
	public boolean isExistQtNhomNguoiDung(QtNhomNguoiDungDTO dto) {

		return qtnndLogic.isExistQtNhomNguoiDung(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return qtnndLogic.isExistById(id);
	}

	@Override
	public QtNhomNguoiDungBDTO getDropdown() {
		return qtnndLogic.getDropdown();
	}

	@Override
	public boolean changeStatus(int id) throws Exception {
		return qtnndLogic.changeStatus(id);
	}

	@Override
	public QtNhomNguoiDungBDTO getList(Integer pageNo, Integer pageSize, String keySort, boolean desc, String keySearch,
			String tenNhomNguoiDung, String ghiChu, String trangThai) {
		return qtnndLogic.getList(pageNo, pageSize, keySort, desc, keySearch, tenNhomNguoiDung, ghiChu, trangThai);
	}

	@Override
	public QtNhomNguoiDungBDTO listAll(String keySort, boolean desc, String trangThai) {

		return qtnndLogic.listAll(keySort, desc, trangThai);
	}

	@Override
	public NguoiDungJoinAllDTO getTreeFunctionShow() {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

		return qtndLogic.findByJoinAllId(userInfo.getId());
	}

	@Override
	public NhomNguoiDungJoinAllDTO findByJoinAllId(int id) throws Exception {
		return qtndLogic.findByJoinAllId2(id);
	}

}
