package com.temp.serviceImpl.NguoiDung;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.NguoiDung.QtLogHeThongLogic;
import com.temp.model.NguoiDung.QtLogHeThongBDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.service.NguoiDung.QtLogHeThongService;

@Service("QtLogHeThongServiceImpl")
public class QtLogHeThongServiceImpl implements QtLogHeThongService {

	@Autowired
	private QtLogHeThongLogic logLogic;

	@Override
	public QtLogHeThongBDTO listQtLogHeThongs(String filterkey, String ipThucHien, String noiDung, String tuNgay,
			String denNgay, String logType, Integer pageNo, Integer pageSize, String keySort, boolean desc,
			String trangThai,String taiKhoan) {

		return logLogic.listQtlogHeThongs(filterkey, ipThucHien, noiDung, tuNgay, denNgay, logType, pageNo, pageSize,
				keySort, desc, trangThai,taiKhoan);
	}

	@Override
	public boolean AddLogHeThong(QtLogHeThongDTO dto) throws Exception {
		if (logLogic.AddLogSYS(dto)) {
			return true;
		}
		return false;
	}
	@Override
	public boolean deleteMulti(List<Integer> lstId) {
		return logLogic.deleteMulti(lstId);
	}
	
	@Override
	public boolean deleteById(int id) throws Exception {
		// TODO Auto-generated method stub
		return logLogic.deleteById(id);
	}
	@Override
	public boolean isExistById(int id) throws Exception {
		// TODO Auto-generated method stub
		return logLogic.isExistById(id);
	}
}
