package com.temp.serviceImpl.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.NguoiDung.QtLichSuTruyCapLogic;
import com.temp.model.NguoiDung.QtLichSuTruyCapBDTO;
import com.temp.service.NguoiDung.LichSuTruyCapService;

@Service("LichSuTruyCapServiceImpl")
public class LichSuTruyCapServiceImpl implements LichSuTruyCapService {

	@Autowired
	public QtLichSuTruyCapLogic lstcLogic;

	@Override
    public QtLichSuTruyCapBDTO getListLichSuTruyCapService(String keysearch, String denNgay,
	    String ipThucHien, String tenTaiKhoan, String tuNgay, String logType, Integer pageNo,
	    Integer pageSize, String keySort, boolean desc, String loaiNguoiDung, Integer ctckThongTinId) {

	return lstcLogic.getListLichSuTruyCap(keysearch, ipThucHien, tenTaiKhoan, tuNgay, denNgay,
		logType, pageNo, pageSize, keySort, desc, loaiNguoiDung, ctckThongTinId);
    }

	@Override
	public QtLichSuTruyCapBDTO getListLichSuTruyCapExportService(String tuNgay, String denNgay, String keySort,
			boolean desc) throws Exception {

		return lstcLogic.getListLichSuTruyCapExport(tuNgay, denNgay, keySort, desc);
	}

}
