package com.temp.serviceImpl.NguoiDung;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.LkNguoiDungNhomLogic;
import com.temp.model.NguoiDung.LkNguoiDungNhomDTO;
import com.temp.service.NguoiDung.LkNguoiDungNhomService;

@Service("LkNguoiDungNhomServiceImpl")
public class LkNguoiDungNhomServiceImpl implements LkNguoiDungNhomService {

	@Autowired
	private LkNguoiDungNhomLogic qtndLogic;

	@Override
	public LkNguoiDungNhomDTO findById(int id) throws Exception {
		return null;
	}

	@Override
	public boolean saveLkNguoiDungNhom(List<LkNguoiDungNhomDTO> LitsLkNguoiDungNhomDTO) throws Exception {
		return false;
	}

	@Override
	public boolean updateLkNguoiDungNhom(List<LkNguoiDungNhomDTO> LitsLkNguoiDungNhomDTO) throws Exception {
		return false;
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return false;
	}

	
	
}
