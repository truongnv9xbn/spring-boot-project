package com.temp.serviceImpl.NguoiDung;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.NguoiDung.LkChucNangNhomNguoiLogic;
import com.temp.logic.NguoiDung.QtNguoiDungLogic;
import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.authen.VerifyLoginDto;
import com.temp.model.file.ChucNangUserBDTO;
import com.temp.service.NguoiDung.QtNguoiDungService;

@Service("QtNguoiDungServiceImpl")
public class QtNguoiDungServiceImpl implements QtNguoiDungService {

	@Autowired
	private QtNguoiDungLogic qtndLogic;

	@Autowired
	private LkChucNangNhomNguoiLogic qtlkLogic;

	@Override
	public QtNguoiDungDTO findById(int id) throws Exception {
		return qtndLogic.getById(id);
	}

	@Override
	public QtNguoiDungDTO findByUsername(String username) {
		return qtndLogic.findByName(username);
	}

	@Override
	public boolean isExistByUsername(String username) {
		return qtndLogic.isExistByUsername(username);
	}

	@Override
	public NguoiDungJoinAllDTO findByJoinAllId(int id) throws Exception {
		return qtndLogic.findByJoinAllId(id);
	}

	@Override
	public boolean saveQtNguoiDung(QtNguoiDungDTO dto) throws Exception {
		return qtndLogic.addOrUpdate(dto);
	}

	@Override
	public QtNguoiDungDTO saveQtNguoiDungGetObject(QtNguoiDungDTO dto) throws Exception {
		return qtndLogic.addOrUpDateGetObject(dto);
	}

	@Override
	public QtNguoiDungDTO refreshSaveToken(QtNguoiDungDTO dto) throws Exception {
		return qtndLogic.RefreshSaveToken(dto);

	}

	@Override
	public boolean updateQtNguoiDung(QtNguoiDungDTO dto) throws Exception {
		return qtndLogic.addOrUpdate(dto);
	}

	@Override
	public boolean changeStatusQtNguoiDung(Integer id, boolean trangThai, boolean thanhVien, boolean chuKySo)
			throws Exception {
		return qtndLogic.changeStatusQtNguoiDung(id, trangThai, thanhVien, chuKySo);
	}

	@Override
	public boolean changePasswordQtNguoiDung(Integer id, String matKhauMoi) throws Exception {
		return qtndLogic.changePasswordQtNguoiDung(id, matKhauMoi);
	}

	@Override
	public boolean resetPasswordQtNguoiDung(Integer id) throws Exception {
		return qtndLogic.resetPasswordQtNguoiDung(id);
	}

	@Override
	public boolean logoutUserLoginQtNguoiDung(Integer id) throws Exception {
		return qtndLogic.logoutUserLoginQtNguoiDung(id);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return qtndLogic.deleteQtNguoiDung(id);
	}

	@Override
	public QtNguoiDungBDTO listQtNguoiDungs(String filterkey, String hoTen, String taiKhoan,
			String sEmail, String sDiDong, String sMaNguoiDung, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String trangThai,Integer nhomNguoiDungId, Integer loaiNguoiDung) {

		return qtndLogic.listQtNguoiDungs(filterkey, hoTen, taiKhoan, sEmail, sDiDong, sMaNguoiDung, pageNo,
				pageSize, keySort, desc, trangThai,nhomNguoiDungId, loaiNguoiDung);
	}

	@Override
	public QtNguoiDungBDTO listQtNguoiDungLogins(String filterkey, Integer dmChucVuId, String hoTen, String taiKhoan,
			String sEmail, String sDiDong, String sMaNguoiDung, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String trangThai) {

		return qtndLogic.listQtNguoiDungLogins(filterkey, dmChucVuId, hoTen, taiKhoan, sEmail, sDiDong, sMaNguoiDung,
				pageNo, pageSize, keySort, desc, trangThai);
	}

	@Override
	public QtNguoiDungBDTO listAllNguoiDung(String keySort, boolean desc, String trangThai) {

		return qtndLogic.listAllNguoiDung(keySort, desc, trangThai);
	}

	@Override
	public boolean isExistQtNguoiDung(QtNguoiDungDTO dto) {

		return qtndLogic.isExistQtNguoiDung(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return qtndLogic.isExistById(id);
	}

	@Override
	public VerifyLoginDto isLoginVerity(String ip, QtNguoiDungDTO user, boolean isLogin) {
		return qtndLogic.isLoginVerity(ip, user, isLogin);
	}

	@Override
	public boolean phanQuyenChucNang(NguoiDungJoinAllDTO dto) {

		return qtndLogic.phanQuyenChucNang(dto);
	}
	
	@Override
	public List<String> getAllChucNangChiTiet() {
		return qtlkLogic.getAllChucNangChiTiet();
	}

	@Override
	public ChucNangUserBDTO getChucNangAndMenu(int nguoiDungId, boolean getTreeMenu) {
		return qtlkLogic.getQuyenAndMenuUser(nguoiDungId, getTreeMenu);
	}

	@Override
	public ChucNangUserBDTO getMenuAdmin() {
		return qtlkLogic.getMenuAdmin();
	}

	@Override
	public boolean isExistByMa(String maNguoiDung, Integer id) {
		return qtndLogic.isExistByMa(maNguoiDung,id);
	}
}
