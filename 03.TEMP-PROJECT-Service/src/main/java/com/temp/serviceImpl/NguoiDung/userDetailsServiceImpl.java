package com.temp.serviceImpl.NguoiDung;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.temp.logic.NguoiDung.QtNguoiDungLogic;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.NguoiDung.userDetailsService;

@Service("userDetailsService")
public class userDetailsServiceImpl implements UserDetailsService, userDetailsService {

	@Autowired
	private QtNguoiDungLogic ndLogic;

	@Override
	public QtNguoiDungDTO findByUsername(String username) {

		return ndLogic.findByName(username);
	}

//	@Override
//	public boolean addOrUpdate(QtNguoiDungDTO user) {
//
//		return ndLogic.addOrUpdate(user,user.getLstIp());
//	}

//	@Override
//	public QtNguoiDungDTO loadUserByUsername(String username) {
//		QtNguoiDungDTO dto = ndLogic.findByName(username);
//		if (dto != null && dto.id > 0) {
//			return dto;
//		}
//		return null;
//
//	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		QtNguoiDungDTO dto = ndLogic.findByName(username);
		if (dto != null && dto.id > 0) {
			return new org.springframework.security.core.userdetails.User(dto.taiKhoan, dto.matKhau, new ArrayList<>());
		}
		return null;
	}

}
