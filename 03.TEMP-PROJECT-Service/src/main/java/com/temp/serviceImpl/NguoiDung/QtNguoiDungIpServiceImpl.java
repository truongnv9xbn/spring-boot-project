package com.temp.serviceImpl.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.NguoiDung.QtNguoiDungIpLogic;
import com.temp.model.NguoiDung.QtNDIpJoinNguoiDungDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpBDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpDTO;
import com.temp.service.NguoiDung.QtNguoiDungIpService;

@Service("QtNguoiDungIpServiceImpl")
public class QtNguoiDungIpServiceImpl implements QtNguoiDungIpService {
	@Autowired
	private QtNguoiDungIpLogic qtNguoiDungIpLogic;

	@Override
	public QtNguoiDungIpDTO findById(int id) throws Exception {
		return qtNguoiDungIpLogic.findById(id);
	}
	
	@Override
	public QtNDIpJoinNguoiDungDTO findByJoinNDId(int id) throws Exception {
		return qtNguoiDungIpLogic.findByJoinNDId(id);
	}

	@Override
	public boolean saveQtNguoiDungIp(QtNguoiDungIpDTO dto) throws Exception {
		return qtNguoiDungIpLogic.addOrUpDateNguoiDungIp(dto);
	}

	@Override
	public boolean updateQtNguoiDungIp(QtNguoiDungIpDTO dto) throws Exception {
		return qtNguoiDungIpLogic.addOrUpDateNguoiDungIp(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return qtNguoiDungIpLogic.delete(id);
	}
	@Override
	public boolean deleteByNguoiDungId(int id) throws Exception {
		return qtNguoiDungIpLogic.delete(id);
	}

	@Override
	public boolean isExistQtNguoiDungIp(QtNguoiDungIpDTO dto) {
		return qtNguoiDungIpLogic.isExistNguoiDungIp(dto);
	}

	@Override
	public boolean isExistById(int id) {
		return qtNguoiDungIpLogic.isExistById(id);
	}

	@Override
	public QtNguoiDungIpBDTO listQtNguoiDungIps(String filterkey,Integer nguoiDungId, String ip, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String hoatDong) {
		return qtNguoiDungIpLogic.listNguoiDungIps(filterkey, nguoiDungId,ip, pageNo, pageSize, keySort, desc, hoatDong);
	}


}
