package com.temp.serviceImpl.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.NguoiDung.LkChucNangNguoiDungLogic;
import com.temp.model.NguoiDung.LkChucNangNguoiBDTO;
import com.temp.service.NguoiDung.LkChucNangNguoiService;

@Service("LkChucNangNguoiDungServiceImpl")
public class LkChucNangNguoiDungServiceImpl implements LkChucNangNguoiService {

	@Autowired
	private LkChucNangNguoiDungLogic lkLogic;

	@Override
	public boolean saveChucNang(LkChucNangNguoiBDTO bdto) throws Exception {
		return lkLogic.addOrUpdateLkChucNangNguoi(bdto);
	}

	@Override
	public boolean deleteById(int idNguoiDung) throws Exception {
		return lkLogic.deleteByNguoiDung(idNguoiDung);
	}

	@Override
	public LkChucNangNguoiBDTO listChucNangs(String filterkey, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, Integer idNguoiDung) {

		return lkLogic.listQuyen(filterkey, pageNo, pageSize, keySort, desc, idNguoiDung);
	}

}
