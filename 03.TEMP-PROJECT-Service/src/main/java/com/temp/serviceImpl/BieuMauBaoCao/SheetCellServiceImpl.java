package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.SheetCtLogic;
import com.temp.model.DropDownDTO;
import com.temp.model.DropDownStringDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCellMenuBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.service.BieuMauBaoCao.SheetCellService;

@Service("SheetCellServiceImpl")
public class SheetCellServiceImpl implements SheetCellService {

    @Autowired
    private SheetCtLogic sheetLogic;

    @Override
    public BmSheetCtDTO AddOrUpdateSheet(BmSheetCtDTO dto) throws Exception {

	return sheetLogic.addOrUpdate(dto);
    }

    @Override
    public boolean isExistById(Integer id) {
	BmSheetCtDTO obj = this.sheetLogic.findById(id);
	if (obj == null) {
	    return false;
	}
	return true;
    }

    @Override
    public BmSheetCtDTO FindById(Integer id) {

	return this.sheetLogic.findById(id);
    }

    @Override
    public boolean DeleteById(Integer id) {
	return this.sheetLogic.deleteCell(id);

    }

    @Override
    public BmSheetCellMenuBDTO listMenuLeftCellSheet(Integer sheetId, Integer cotId, Integer hangId,
	    Integer pageNo, Integer pageSize) {
	return sheetLogic.GetList(sheetId, cotId, hangId, pageNo, pageSize);
    }

    @Override
    public boolean validateTuHangDenHang(Integer tuHangId, Integer denHangId) {
	return sheetLogic.validateTuHangDenHang(tuHangId, denHangId);
    }

    @Override
    public List<DropDownDTO> dropdownChitieu(Integer SheetId, Integer cotId, Integer hangId) {
	return this.sheetLogic.getDropDown(SheetId, cotId, hangId);
    }

    @Override
    public boolean checkExistAddCell(Integer SheetId, Integer cotId, Integer hangIdFrom,
	    Integer hangIdTo) {

	return this.sheetLogic.checkExistAddCell(SheetId, cotId, hangIdFrom, hangIdTo);
    }

    @Override
    public List<DropDownStringDTO> dropdownChitieuCongThuc(Integer sheedId, Integer cotId,
	    Integer hangId) {

	return this.sheetLogic.getCongThucDropDown(sheedId, cotId, hangId);
    }

    @Override
    public List<Integer> getDsSheetCtByLsSheetHang(String congThuc, String baoCaoId,
	    String sheetBcId, String cotId) throws Exception {
	// TODO Auto-generated method stub
	return this.sheetLogic.getDsSheetCtByLsSheetHangLogic(congThuc, baoCaoId, sheetBcId, cotId);
    }

    @Override
    public List<BmSheetCtDTO> getDsCtFromBmBcId(List<Integer> bmBaoCaoId) throws Exception {
	// TODO Auto-generated method stub
	return this.sheetLogic.getDsCtFromBmBcId(bmBaoCaoId);
    }

    @Override
    public List<DropDownDTO> getOChiTieuCanhCanhDropdown(Integer sheedId, Integer cotId,
	    Integer hangId) throws Exception {
	return this.sheetLogic.getOChiTieuCanhCanhDropdown(sheedId, cotId, hangId);
    }

    @Override
    public BmSheetCtDTO findByBmSheetCtVaoId(int bmSheetCtVaoId) throws Exception {
	return this.sheetLogic.getBmSheetCtByBmSheetCtVaoIdLogic(bmSheetCtVaoId);
    }

}