package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.SheetHangLogic;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangMenuBDTO;
import com.temp.service.BieuMauBaoCao.SheetHangService;

@Service("SheetHangServiceImpl")
public class SheetHangServiceImpl implements SheetHangService {

	@Autowired
	private SheetHangLogic sheetHangLogic;

	@Override
	public BmSheetHangDTO AddOrUpdateHangSheet(BmSheetHangDTO dto) throws Exception {

		return this.sheetHangLogic.addOrUpdate(dto);
	}

	@Override
	public BmSheetHangMenuBDTO List(Integer idBieuMau, Integer pageNo, Integer pageSize) throws Exception {

		return this.sheetHangLogic.GetList(idBieuMau,pageNo,pageSize);
	}

	@Override
	public boolean isExistByHangSheetId(Integer id) {

		BmSheetHangDTO obj = this.sheetHangLogic.findById(id);
		if (obj.getId() != null && obj.getId() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public BmSheetHangDTO FindById(Integer id) {

		return this.sheetHangLogic.findById(id);
	}

	@Override
	public boolean checkExistMaHangSheet(String maHang, Integer id, Integer sheetId) {

		return this.sheetHangLogic.checkExistMaHangSheet(maHang, id, sheetId);
	}
	
	@Override
	public boolean checkExistMaHangLT(String maHangLT, Integer id, Integer sheetId) {

		return this.sheetHangLogic.checkExistMaHangLT(maHangLT, id, sheetId);
	}

	@Override
	public boolean checkExistMaHangSheetUpdate(String maHang, Integer id,Integer sheetId) {

		return this.sheetHangLogic.checkExistUpdateMaHangSheet(maHang, id,sheetId);
	}

	@Override
	public List<DropDownDTO> listHangDropdown(Integer sheedId) throws Exception {
		// TODO Auto-generated method stub
		return this.sheetHangLogic.GetListDropDownHang(sheedId);
	}

	@Override
	public boolean delete(Integer id) {
		return this.sheetHangLogic.deleteHang(id);
	}
	
	@Override
	public boolean resetSTT(Integer id) {
		return this.sheetHangLogic.resetSTT(id);
	}
	
	@Override
	public boolean setMaHangBySTT(Integer id) {
		return this.sheetHangLogic.setMaHangBySTT(id);
	}
	
	@Override
	public long maxSTTBySheetId(Integer id) {
		return this.sheetHangLogic.maxSTTBySheetId(id);
	}

}