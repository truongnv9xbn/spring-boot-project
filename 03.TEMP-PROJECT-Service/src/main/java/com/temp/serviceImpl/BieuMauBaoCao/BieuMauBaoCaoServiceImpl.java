package com.temp.serviceImpl.BieuMauBaoCao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.temp.logic.BieuMauBaoCao.BieuMauBaoCaoLogic;
import com.temp.model.BieuMauBaoCao.BieuMauBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.service.BieuMauBaoCao.BieuMauBaoCaoService;
@Service("BieuMauBaoCaoServiceImpl")
@Transactional
public class BieuMauBaoCaoServiceImpl implements BieuMauBaoCaoService {
	@Autowired
	BieuMauBaoCaoLogic logic;

	@Override
	public BieuMauBaoCaoBDTO getlist(Integer pageNo, Integer pageSize, boolean desc, String strFilter, String tenBaoCao,
			String canCuPhapLy) {
		// TODO Auto-generated method stub
		return logic.getlist(pageNo, pageSize, desc, strFilter, tenBaoCao, canCuPhapLy);
	}

	@Override
	public boolean save(BmBaoCaoDTO dto) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(BmBaoCaoDTO dto) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isExistById(int id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BmBaoCaoDTO findById(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	 

}
