package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.SheetMegerHeaderLogic;
import com.temp.model.BieuMauBaoCao.BmSheetMegerCellDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangCotDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangDTO;
import com.temp.model.dropdown.DropdownMegerCell;
import com.temp.service.BieuMauBaoCao.SheetMegerHeaderService;

@Service("SheetMegerHeaderServiceImpl")
public class SheetMegerHeaderServiceImpl implements SheetMegerHeaderService {

	@Autowired
	private SheetMegerHeaderLogic megerHeaderLogic;

	@Override
	public BmSheetMegerCellDTO InitDisplayCauHinhMegerHangCot(int sheetId) {

		return this.megerHeaderLogic.InitDisplayCauHinhMegerHangCot(sheetId);
	}

	@Override
	public BmTieuDeHangDTO addOrUpdateHang(BmTieuDeHangDTO dto) {

		return this.megerHeaderLogic.addOrUpdateHang(dto);
	}

	@Override
	public BmTieuDeHangCotDTO addOrUpdateHangCot(BmTieuDeHangCotDTO megerHangCot) {

		return this.megerHeaderLogic.addOrUpdateHangCot(megerHangCot);
	}

	@Override
	public boolean DeleteRows(Integer tieuDeHangId) {

		return this.megerHeaderLogic.DeleteRows(tieuDeHangId);
	}
	
	@Override
	public boolean DeleteRowHangCot(Integer tieuDeHangId) {

		return this.megerHeaderLogic.DeleteRowHangCot(tieuDeHangId);
	}

	@Override
	public List<DropdownMegerCell> DropDownHang(Integer sheetId) {
		return this.megerHeaderLogic.DropDownHang(sheetId);
	}

	@Override
	public boolean checkExistAddMeger(Integer sheetId, int fisrtRow, int lastRow, int fisrtColumn, int lastColumn) {
		return this.megerHeaderLogic.checkExistAddMeger(sheetId, fisrtRow, lastRow, fisrtColumn, lastColumn);
	}

	@Override
	public boolean checkExistTieuDeHang(Integer sheetId) throws Exception {
	    // TODO Auto-generated method stub
	    return this.megerHeaderLogic.checkExistTieuDeHangLogic(sheetId);
	}

}