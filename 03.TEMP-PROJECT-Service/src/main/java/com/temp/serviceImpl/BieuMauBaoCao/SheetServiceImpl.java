package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.SheetLogic;
import com.temp.model.DataImportExcelBDTO;
import com.temp.model.DataImportExcelDTO;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BcThanhVienDTO;
import com.temp.model.BieuMauBaoCao.BmSheetBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;
import com.temp.service.BieuMauBaoCao.SheetService;

@Service("SheetServiceImpl")
public class SheetServiceImpl implements SheetService {

    @Autowired
    private SheetLogic sheetLogic;

    @Override
    public BmSheetBDTO listSheet(String strfilter, Integer CtckId, Integer bmBaoCaoId,
	    String sLoaiCoDong, Integer pageNo, Integer pageSize, String keySort, boolean desc) {
	return this.sheetLogic.listSheet(strfilter, CtckId, bmBaoCaoId, sLoaiCoDong, pageNo,
		pageSize, keySort, desc);
    }

    
    @Override
    public BmSheetBDTO listSheetLazy(String strfilter, Integer CtckId, Integer bmBaoCaoId,
	    String sLoaiCoDong, Integer pageNo, Integer pageSize, String keySort, boolean desc) {
	return this.sheetLogic.listSheetLazy(strfilter, CtckId, bmBaoCaoId, sLoaiCoDong, pageNo,
		pageSize, keySort, desc);
    }
    
    @Override
    public BmSheetDTO AddOrUpdateSheet(BmSheetDTO dto) throws Exception {

	return sheetLogic.addOrUpdate(dto);
    }

    @Override
    public boolean isExistUpdate(BmSheetDTO dto) {
	BmSheetDTO obj = this.sheetLogic.findById(dto.getId());
	if (obj == null) {
	    return false;
	}
	return true;
    }

    @Override
    public boolean isExistById(Integer id) {
	BmSheetDTO obj = this.sheetLogic.findById(id);
	if (obj == null) {
	    return false;
	}
	return true;
    }

    @Override
    public BmSheetDTO FindById(Integer id) {

	return this.sheetLogic.findById(id);
    }

    @Override
    public BmSheetDTO InitDisplayEditSheet(Integer id) throws Exception {

	return this.sheetLogic.initDisplayEdit(id);
    }

    @Override
    public boolean DeleteById(Integer id) throws Exception {
	return this.sheetLogic.deleteById(id);

    }

    @Override
    public boolean checkExistMaSheet(String maSheet, Integer sheetId, Integer bmBaoCaoId) {
	return this.sheetLogic.checkExistMaSheet(maSheet, sheetId, bmBaoCaoId);

    }
    
    @Override
    public boolean checkExistMaSheetLienThong(String maSheetLienThong, Integer sheetId, Integer bmBaoCaoId) {
	return this.sheetLogic.checkExistMaSheetLienThong(maSheetLienThong, sheetId, bmBaoCaoId);

    }

    @Override
    public boolean checkExistMaSheetUpdate(String maSheet, Integer id) {
	return this.sheetLogic.checkExistMaSheetUpdate(maSheet, id);
    }

    @Override
    public List<DropDownDTO> listSheet(Integer id) {
	return this.sheetLogic.lstDsSheet(id);
    }

    @Override
    public List<DropDownDTO> getSheetFromBcDropdownService(int baocaoId) throws Exception {
	return this.sheetLogic.getSheetFromBcDropdownLogic(baocaoId);
    }

    @Override
    public BmSheetDTO getNoiDungBaoCaoSheetId(Integer id, String giaTriKyBaoCao, Integer baoCaoId,
	    int thanhVienId) throws Exception {
	return this.sheetLogic.getNoiDungBaoCaoSheetIdLogic(id, giaTriKyBaoCao, baoCaoId, thanhVienId);
    }

    @Override
    public List<DropDownDTO> getlsSheetbyBaoCaoService(Integer baocaoId) throws Exception {
	return this.sheetLogic.getlsSheetbyBaoCaoLogic(baocaoId);
    }

    @Override
    public BmSheetDTO getGiaTriBaoCaoSheetId(Integer sheetId, Integer thanhVienId, Integer baoCaoId)
	    throws Exception {
	return this.sheetLogic.getGiaTriBaoCaoSheetIdLogic(sheetId, thanhVienId, baoCaoId);
    }

    @Override
    public List<BmSheetDTO> buildSheetDataFromImportExcelFileService(
	    List<DataImportExcelDTO> result, List<DropDownDTO> lsSheetDropDown, int baoCaoId)
	    throws Exception {
	return this.sheetLogic.buildSheetDataFromImportExcelFileLogic(result, lsSheetDropDown,
		baoCaoId);
    }


    @Override
    public boolean checkExistBia(Integer bmBaoCaoId) {
	// TODO Auto-generated method stub
	return sheetLogic.checkExistBia(bmBaoCaoId);
    }

    @Override
    public BmSheetDTO getSheetBia(Integer bmBaoCaoId) {

	return this.sheetLogic.getSheetBia(bmBaoCaoId);
    }

    @Override
    public List<BmSheetDTO> buildSheetDynamicImportExcelService(DataImportExcelBDTO result,
	    List<DropDownDTO> lsSheetIds) throws Exception {
	return this.sheetLogic.buildSheetDynamicImportExcelLogic(result, lsSheetIds);
    }

    @Override
    public List<DropDownDTO> getlsSheetNotTrangBiabyBaoCaoService(Integer baocaoId)
	    throws Exception {

	return this.sheetLogic.getlsSheetNotTrangBiabyBaoCaoLogic(baocaoId);
    }

    @Override
    public BmSheetDTO initEformGuiBaoCaoKhac(Integer sheetId, BcThanhVienDTO thanhVienDto) throws Exception {
	return this.sheetLogic.initEformGuiBaoCaoKhacLogic(sheetId, thanhVienDto);
    }


	@Override
	public List<BmSheetDTO> getNoiDungBaoCaoAllSheet(String giaTriKyBaoCao, Integer baoCaoId, int thanhVienId)
			throws Exception {
		return this.sheetLogic.getNoiDungBaoCaoAllSheet(giaTriKyBaoCao, baoCaoId, thanhVienId);
	}

}