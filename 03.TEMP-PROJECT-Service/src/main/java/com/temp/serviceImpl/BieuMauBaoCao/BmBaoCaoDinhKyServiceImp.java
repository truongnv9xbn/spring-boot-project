package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.BmBaoCaoDinhKyLogic;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyDTO;
import com.temp.model.BieuMauBaoCao.KyBcGiaTriBcDTO;
import com.temp.service.BieuMauBaoCao.BmBaoCaoDinhKyService;

@Service("BmBaoCaoDinhKyServiceImp")
public class BmBaoCaoDinhKyServiceImp implements BmBaoCaoDinhKyService {
	@Autowired
	private BmBaoCaoDinhKyLogic bmBaoCao;

	@Override
	public BmBaoCaoDinhKyDTO findById(int id) throws Exception {

		return this.bmBaoCao.getById(id);
	}

	@Override
	public BmBaoCaoDinhKyDTO addorUpdateBmBaoCaoDinhKy(BmBaoCaoDinhKyDTO dto) throws Exception {
		return this.bmBaoCao.addOrUpDateBmBaoCaoDinhKy(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {

		return this.bmBaoCao.deleteBmBaoCaoDinhKy(id);
	}

	@Override
	public boolean isExistById(int id) {
		return this.bmBaoCao.isExistById(id);
	}

	@Override
	public BmBaoCaoDinhKyBDTO listBmBaoCaoDinhKy(String strfilter, Integer CtckId, Integer bmBaoCaoId, String sLoaiCoDong,
			Integer pageNo, Integer pageSize, String keySort, boolean desc) {
		return this.bmBaoCao.listBmBaoCaoDinhKys(strfilter, CtckId, bmBaoCaoId, sLoaiCoDong, pageNo, pageSize,
				keySort, desc);
	}

	@Override
	public boolean isExistUpdate(BmBaoCaoDinhKyDTO dto) {
		return this.bmBaoCao.isExistBmBaoCaoDinhKyUpdate(dto);
	};

	@Override
	public boolean isExistAdd(BmBaoCaoDinhKyDTO dto) {
		return this.bmBaoCao.isExistBmBaoCaoDinhKyAdd(dto);
	}

	@Override
	public List<DropDownDTO> getBaoCaoDinhKyFromBcDropdownService(int bieumauId)
		throws Exception {
	    return this.bmBaoCao.getBaoCaoDinhKyFromBcDropdownLogic(bieumauId);
	}
	
	@Override
	public List<KyBcGiaTriBcDTO> getListKyBaoCaoFromBcThanhVien(List<Integer> lstCty) throws Exception{
		return this.bmBaoCao.getListKyBaoCaoFromBcThanhVien(lstCty);
	}

}