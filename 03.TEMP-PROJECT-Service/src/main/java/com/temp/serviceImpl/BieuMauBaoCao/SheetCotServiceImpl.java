package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.SheetcotLogic;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotMenuBDTO;
import com.temp.model.dropdown.DropdownMegerCell;
import com.temp.service.BieuMauBaoCao.SheetCotService;

@Service("SheetCotServiceImpl")
public class SheetCotServiceImpl implements SheetCotService {

	@Autowired
	private SheetcotLogic sheetcotLogic;

	@Override
	public BmSheetCotDTO AddOrUpdateCotSheet(BmSheetCotDTO dto) throws Exception {

		return this.sheetcotLogic.addOrUpdate(dto);
	}

	@Override
	public BmSheetCotMenuBDTO List(Integer idSheet, Integer pageNo,Integer pageSize) throws Exception {

		return this.sheetcotLogic.GetList(idSheet,pageNo,pageSize);
	}

	@Override
	public boolean isExistByCotSheetId(Integer id) {

		BmSheetCotDTO obj = this.sheetcotLogic.findById(id);
		if (obj.getId() != null && obj.getId() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public BmSheetCotDTO FindById(Integer id) {

		return this.sheetcotLogic.findById(id);
	}

	@Override
	public boolean checkExistMaCotSheet(String maCot,Integer sheetId) {

		return this.sheetcotLogic.checkExistMaCotSheet(maCot,sheetId);
	}

	@Override
	public boolean checkExistMaCotSheetUpdate(String maCot, Integer id, Integer sheetId) {

		return this.sheetcotLogic.checkExistUpdateMaCotSheet(maCot, id,sheetId);
	}
	
	@Override
	public boolean checkExistMaCotLT(String maCotLT, Integer id, Integer sheetId) {

		return this.sheetcotLogic.checkExistMaCotLT(maCotLT, id,sheetId);
	}
	
	@Override
	public List<DropDownDTO> listCotDropdown(Integer idSheet) throws Exception {

		return this.sheetcotLogic.GetListDropDownCot(idSheet);
	}

	@Override
	public java.util.List<DropdownMegerCell> DropDownMegerHangCot(Integer sheetId) {

		return this.sheetcotLogic.GetListDropDownMegerCot(sheetId);
	}

	@Override
	public boolean deleteColumn(Integer id) {
		// TODO Auto-generated method stub
		return this.sheetcotLogic.deleteColumns(id);
	}
	
	@Override
	public long maxSTTBySheetId(Integer id) {
		return this.sheetcotLogic.maxSTTBySheetId(id);
	}
}