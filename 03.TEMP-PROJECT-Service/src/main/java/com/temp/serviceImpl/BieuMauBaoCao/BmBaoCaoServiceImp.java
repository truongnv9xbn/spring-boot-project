package com.temp.serviceImpl.BieuMauBaoCao;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.BieuMauBaoCao.BmBaoCaoLogic;
import com.temp.logic.BieuMauBaoCao.SheetCtLogic;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BcKhaiThacGtBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoExportDTO;
import com.temp.model.BieuMauBaoCao.BmLichSuBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.service.BieuMauBaoCao.BmBaoCaoService;

@Service("BmBaoCaoServiceImp")
public class BmBaoCaoServiceImp implements BmBaoCaoService {
    @Autowired
    private BmBaoCaoLogic bmBaoCaoLogic;

    @Autowired
    private SheetCtLogic sheetCtLogic;

    @Override
    public BmBaoCaoDTO findById(int id) throws Exception {

	return bmBaoCaoLogic.getById(id);
    }

    @Override
    public BmBaoCaoDTO addorUpdateBmBaoCao(BmBaoCaoDTO dto) throws Exception {
    	return bmBaoCaoLogic.addOrUpDateBmBaoCao(dto);
    }
    
    @Override
    public BmBaoCaoDTO addorUpdateBmBaoCao(BmBaoCaoDTO dto, String lichSuStr) throws Exception {
    	return bmBaoCaoLogic.addOrUpDateBmBaoCao(dto, lichSuStr);
    }

    @Override
    public boolean deleteById(int id) throws Exception {

	return bmBaoCaoLogic.deleteBmBaoCao(id);
    }

    @Override
    public boolean copyById(int id) throws Exception {

	return bmBaoCaoLogic.copyById(id);
    }

    @Override
    public boolean duaVaoSuDung(int id) throws Exception {

	return bmBaoCaoLogic.duaVaoSuDung(id);
    }
    
    @Override
    public boolean suDungBaoCao(int id, int isSuDung) throws Exception {

	return bmBaoCaoLogic.suDungBaoCao(id, isSuDung);
    }


    @Override
    public boolean isExistById(int id) {
	return false;
    }

    @Override
    public boolean isExistUpdate(BmBaoCaoDTO dto) {
	return bmBaoCaoLogic.isExistBmBaoCaoUpdate(dto);
    };

    @Override
    public boolean isExistAdd(BmBaoCaoDTO dto) {
	return bmBaoCaoLogic.isExistBmBaoCaoAdd(dto);
    }

    @Override
    public List<DropDownDTO> getBmBaoCaoDropDownService() throws Exception {
	// TODO Auto-generated method stub
	return bmBaoCaoLogic.getBmBaoCaoDropDownLogic();
    }

    // (keySearch, tenBaoCao, canCuPhapLy, trangThai, loaiBaoCao,pageNo, pageSize,
    // keySort, desc)

    @Override
    public BmBaoCaoBDTO listBieuMauBaoCao(String keySearch, String tenBaoCao, String canCuPhapLy,
	    Integer trangThai, String loaiBaoCao, Integer pageNo, Integer pageSize, String keySort,
	    boolean desc, Integer kieuBaoCao, String maBaoCao, String nhomBaoCao) {
	return bmBaoCaoLogic.listBieuMauBaoCao(keySearch, tenBaoCao, canCuPhapLy, trangThai,
		loaiBaoCao, pageNo, pageSize, keySort, desc, kieuBaoCao, maBaoCao, nhomBaoCao);
    }

    @Override
    public BmBaoCaoExportDTO getDataExport(int bmBaoCaoId) throws Exception {
	return bmBaoCaoLogic.getDataExport(bmBaoCaoId);
    }

    @Override
    public Workbook generateFileTemplateExcel(BmBaoCaoExportDTO dto,Workbook workbook, Boolean bieuMauDauRa ) throws Exception {
	// TODO Auto-generated method stub
	return bmBaoCaoLogic.generateFileTemplateExcel(dto,workbook,bieuMauDauRa);
    }

    @Override
    public BmBaoCaoBDTO listBmBaoCao(Integer nguoiDungId, Integer kieuBaoCao, String strfilter, String tenBaoCao,
	    String maBaoCao, String ccPhapLy, Integer pageNo, Integer pageSize, String keySort,
	    boolean desc) {
	return this.bmBaoCaoLogic.listBmBaoCaos(nguoiDungId, kieuBaoCao, strfilter, tenBaoCao, maBaoCao,
		ccPhapLy, pageNo, pageSize, keySort, desc);
    }

    @Override
    public BmLichSuBaoCaoBDTO lichSuThayDoi(int pageNo, Integer pageSize, String keySort,
	    int bmBaoCaoId) throws Exception {
	// TODO Auto-generated method stub
	return bmBaoCaoLogic.lichSuThayDoi(pageNo, pageSize, keySort, bmBaoCaoId);
    }

    @Override
    public BmSheetCtDTO getBmSheetCtById(Integer bmSheetCtId) {
	// TODO Auto-generated method stub
	return sheetCtLogic.findById(bmSheetCtId);
    }

    @Override
    public List<DropDownDTO> getLsBaoCaoKhacDropDownService(String baoCaoType) throws Exception {
	return this.bmBaoCaoLogic.getLsBaoCaoKhacDropDownLogic(baoCaoType);
    }

    @Override
    public List<DropDownDTO> getBmBaoCaoForCanhBaoCtService() throws Exception {
	// TODO Auto-generated method stub
	return this.bmBaoCaoLogic.getBmBaoCaoForCanhBaoCtLogic();
    }
    
    @Override
    public boolean checkExistMaBaoCao(String maBaoCao, Integer bmId, Integer bmBaoCaoId) throws Exception {
	return this.bmBaoCaoLogic.checkExistMaBaoCao(maBaoCao, bmId, bmBaoCaoId);

    }
    
    @Override
    public boolean checkExistLoaiBaoCaoLT(String loaiBaoCaoLT, Integer bmId, Integer bmBaoCaoId) throws Exception {
	return this.bmBaoCaoLogic.checkExistLoaiBaoCaoLT(loaiBaoCaoLT, bmId, bmBaoCaoId);

    }

	@Override
	public BmBaoCaoBDTO getLsBaoCaoKhacDropDownServiceBDTO(String baoCaoType) throws Exception {
		return this.bmBaoCaoLogic.getLsBaoCaoKhacDropDownLogicBDTO(baoCaoType);
	}

	@Override
	public Workbook generateFileTemplateExcelKhaiThac(BcKhaiThacGtBDTO lstBcKhaiThacGt, BmBaoCaoExportDTO dto, Workbook workbook, Boolean bieuMauDauRa, Map<Integer, Integer> mapSheet) throws Exception {
		return bmBaoCaoLogic.generateFileTemplateExcelKhaiThac(lstBcKhaiThacGt, dto,workbook,bieuMauDauRa, mapSheet);
	}
    
    @Override
    public boolean checkBatBuocCTBaoCao(Integer bcThanhVien, Integer bmBaoCaoId) throws Exception {
    	return this.bmBaoCaoLogic.checkBatBuocCTBaoCao(bcThanhVien, bmBaoCaoId);
    }
}