package com.temp.serviceImpl.trangchu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.trangchu.TrangChuLogic;
import com.temp.model.TrangChuBDTO;
import com.temp.model.TrangChuDTO;
import com.temp.service.trangchu.TrangChuService;

@Service("TrangChuServiceImpl")
public class TrangChuServiceImpl implements TrangChuService {

	@Autowired
	private TrangChuLogic logic;

	@Override
	public TrangChuBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai) {
		return logic.filter(pageNo, pageSize, keySort, desc, trangThai);
	}

	@Override
	public boolean save(TrangChuDTO dto) throws Exception {
		return logic.save(dto);
	}

}
