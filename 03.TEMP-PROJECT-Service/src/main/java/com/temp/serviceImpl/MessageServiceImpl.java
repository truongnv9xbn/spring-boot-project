package com.temp.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.MessageLogic;
import com.temp.model.MessageBDTO;
import com.temp.model.MessageDTO;
import com.temp.service.MessageService;

@Service("MessageServiceImpl")
public class MessageServiceImpl implements MessageService {

	@Autowired
	private MessageLogic logic;

	@Override
	public MessageBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai) {
		return logic.filter(pageNo, pageSize, keySort, desc, trangThai);
	}

	@Override
	public boolean save(MessageDTO dto) throws Exception {
		return logic.save(dto);
	}

}
