package com.temp.serviceImpl.Common;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.global.UserInfoGlobal;
import com.temp.logic.NguoiDung.ThongBaoLogic;
import com.temp.model.ThongBaoBDTO;
import com.temp.model.ThongBaoDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.service.Common.ThongBaoService;

@Service("ThongBaoServiceImpl")
public class ThongBaoServiceImpl implements ThongBaoService {

	@Autowired
	private ThongBaoLogic tshtLogic;

//	@Override
//	public ThongBaoDTO findById(int id) throws Exception {
//		return tshtLogic.getById(id);
//	}
	
//	@Override
//	public ThongBaoDTO findByThamSo(String thamSo) throws Exception {
//		return tshtLogic.getByThamSo(thamSo);
//	}

//	@Override
//	public boolean updateThongBao(ThongBaoDTO dto) throws Exception {
//		return tshtLogic.addOrUpDateThongBao(dto);
//	}

	@Override
	public ThongBaoBDTO listThongBao(Integer pageNo, Integer pageSize, String filterKey, Integer nguoiDungId, boolean desc, Integer trangThai, String list) {

		return tshtLogic.listThongBao(pageNo, pageSize, filterKey, nguoiDungId, desc,
				trangThai, list);
	}

	@Override
	public boolean isExistById(Integer id) {

		return tshtLogic.isExistById(id);
	}
	
	public ThongBaoDTO findById(Integer id) {
		return tshtLogic.findById(id);
	}

	public boolean updateSeen(Integer id) {
		return tshtLogic.updateSeen(id);
	}

	@Override
	public boolean addThongBao(String tieuDe, String noiDung, String linkHref, Set<Integer> nguoiNhan) {
		String nguoiNhanStr = ";";
		for(Integer temp : nguoiNhan) {
			nguoiNhanStr += temp + ";";
		}
//		nguoiNhanStr += ',';
		if(nguoiNhanStr.equals(";;"))
			nguoiNhanStr = null;
		Date curr = new Date();
		ThongBaoDTO finalResult = new ThongBaoDTO();
		finalResult.setLinkHref(linkHref);
		finalResult.setListNguoiNhan(nguoiNhanStr);
		finalResult.setNoiDung(noiDung);
		finalResult.setTieuDe(tieuDe);
		finalResult.setNgayTao(new Timestamp(curr.getTime()));
		finalResult.setTrangThai(true);
		finalResult.setListNguoiDaXem(";");
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		finalResult.setNguoiTaoId(userInfo != null ? userInfo.getId() : null);
		return tshtLogic.addThongBao(finalResult);
	}
	
	public boolean seenAll(List<Integer> lstTbId, Integer nguoiDungId) {
		return tshtLogic.seenAll(lstTbId, nguoiDungId);
	}
}
