package com.temp.serviceImpl.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.Common.BaiVietLogic;
import com.temp.model.Common.BaiVietBDTO;
import com.temp.model.Common.BaiVietDTO;
import com.temp.service.Common.BaiVietService;

@Service("BaiVietServiceImpl")
public class BaiVietServiceImpl implements BaiVietService {

	@Autowired
	private BaiVietLogic baiVietLogic;

	@Override
	public BaiVietBDTO getList(String keysearch, Integer sChuyenMucId, String sTieuDe, String sMoTaNgan,
			String sNoiDung, String sNguoiTao, Integer sNoiBat, String trangThai, Integer pageNo, Integer pageSize,
			String keySort, boolean desc) {
		return baiVietLogic.getList(keysearch, sChuyenMucId, sTieuDe, sMoTaNgan, sNoiDung, sNguoiTao, sNoiBat,
				trangThai, pageNo, pageSize, keySort, desc);
	}

	@Override
	public BaiVietDTO findById(int id) throws Exception {
		return baiVietLogic.getById(id);
	}

	@Override
	public boolean saveBaiViet(BaiVietDTO dto) throws Exception {
		return baiVietLogic.addOrUpDateBaiViet(dto);
	}

	@Override
	public boolean updateBaiViet(BaiVietDTO dto) throws Exception {
		return baiVietLogic.addOrUpDateBaiViet(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return baiVietLogic.deleteBaiViet(id);
	}

	@Override
	public BaiVietBDTO listBaiViets(String filterkey, String sTenBaiViet, String sMaBaiViet, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		return baiVietLogic.listBaiViets(filterkey, sTenBaiViet, sMaBaiViet, pageNo, pageSize, keySort, desc,
				trangThai);
	}

	@Override
	public boolean isExistBaiVietUpdate(BaiVietDTO dto) {

		return baiVietLogic.isExistBaiVietUpdate(dto);

	}

	@Override
	public boolean isExistBaiVietAdd(BaiVietDTO dto) {

		return baiVietLogic.isExistBaiVietAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return baiVietLogic.isExistById(id);
	}

//	@Override
//	public boolean isExistByMa(String maBaiViet, Integer id) {
//		// TODO Auto-generated method stub
//		return baiVietLogic.isExistByMa(maBaiViet, id);
//	}

}
