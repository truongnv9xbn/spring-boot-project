package com.temp.serviceImpl.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.Common.TaiLieuLogic;
import com.temp.model.Common.TaiLieuBDTO;
import com.temp.model.Common.TaiLieuDTO;
import com.temp.service.Common.TaiLieuService;

@Service("TaiLieuServiceImpl")
public class TaiLieuServiceImpl implements TaiLieuService {

	@Autowired
	private TaiLieuLogic taiLieuLogic;

	@Override
	public TaiLieuBDTO getList(String keysearch, String sSoVanBan, String sTrichYeu, String sNguoiTao, String trangThai,
			Integer pageNo, Integer pageSize, String keySort, boolean desc) {
		return taiLieuLogic.getList(keysearch, sSoVanBan, sTrichYeu, sNguoiTao, trangThai, pageNo, pageSize, keySort,
				desc);
	}

	@Override
	public TaiLieuDTO findById(int id) throws Exception {
		return taiLieuLogic.getById(id);
	}

	@Override
	public boolean saveTaiLieu(TaiLieuDTO dto) throws Exception {
		return taiLieuLogic.addOrUpDateTaiLieu(dto);
	}

	@Override
	public boolean updateTaiLieu(TaiLieuDTO dto) throws Exception {
		return taiLieuLogic.addOrUpDateTaiLieu(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return taiLieuLogic.deleteTaiLieu(id);
	}

	@Override
	public TaiLieuBDTO listTaiLieus(String filterkey, String sTenTaiLieu, String sMaTaiLieu, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		return taiLieuLogic.listTaiLieus(filterkey, sTenTaiLieu, sMaTaiLieu, pageNo, pageSize, keySort, desc,
				trangThai);
	}

	@Override
	public boolean isExistTaiLieuUpdate(TaiLieuDTO dto) {

		return taiLieuLogic.isExistTaiLieuUpdate(dto);

	}

	@Override
	public boolean isExistTaiLieuAdd(TaiLieuDTO dto) {

		return taiLieuLogic.isExistTaiLieuAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return taiLieuLogic.isExistById(id);
	}

//	@Override
//	public boolean isExistByMa(String maTaiLieu, Integer id) {
//		// TODO Auto-generated method stub
//		return taiLieuLogic.isExistByMa(maTaiLieu, id);
//	}

}
