package com.temp.serviceImpl.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.Common.ChuyenMucLogic;
import com.temp.model.Common.ChuyenMucBDTO;
import com.temp.model.Common.ChuyenMucDTO;
import com.temp.service.Common.ChuyenMucService;

@Service("ChuyenMucServiceImpl")
public class ChuyenMucServiceImpl implements ChuyenMucService {

	@Autowired
	private ChuyenMucLogic chuyenMucLogic;

	@Override
	public ChuyenMucBDTO getList(String keysearch, String sTenChuyenMuc, String sNoiDung, String sNguoiTao,
			String trangThai, Integer pageNo, Integer pageSize, String keySort, boolean desc) {
		return chuyenMucLogic.getList(keysearch, sTenChuyenMuc, sNoiDung, sNguoiTao, trangThai, pageNo,
				pageSize, keySort, desc);
	}
	
	@Override
	public ChuyenMucDTO findById(int id) throws Exception {
		return chuyenMucLogic.getById(id);
	}

	@Override
	public boolean saveChuyenMuc(ChuyenMucDTO dto) throws Exception {
		return chuyenMucLogic.addOrUpDateChuyenMuc(dto);
	}

	@Override
	public boolean updateChuyenMuc(ChuyenMucDTO dto) throws Exception {
		return chuyenMucLogic.addOrUpDateChuyenMuc(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return chuyenMucLogic.deleteChuyenMuc(id);
	}

	@Override
	public ChuyenMucBDTO listChuyenMucs(String filterkey, String sTenChuyenMuc, String sMaChuyenMuc, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		return chuyenMucLogic.listChuyenMucs(filterkey, sTenChuyenMuc, sMaChuyenMuc, pageNo, pageSize, keySort, desc,
				trangThai);
	}

	@Override
	public boolean isExistChuyenMucUpdate(ChuyenMucDTO dto) {

		return chuyenMucLogic.isExistChuyenMucUpdate(dto);

	}

	@Override
	public boolean isExistChuyenMucAdd(ChuyenMucDTO dto) {

		return chuyenMucLogic.isExistChuyenMucAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return chuyenMucLogic.isExistById(id);
	}

//	@Override
//	public boolean isExistByMa(String maChuyenMuc, Integer id) {
//		// TODO Auto-generated method stub
//		return chuyenMucLogic.isExistByMa(maChuyenMuc,id);
//	}

}
