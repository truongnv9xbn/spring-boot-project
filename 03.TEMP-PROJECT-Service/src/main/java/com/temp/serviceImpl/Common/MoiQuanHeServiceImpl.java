package com.temp.serviceImpl.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.Common.MoiQuanHeLogic;
import com.temp.model.Common.MoiQuanHeBDTO;
import com.temp.model.Common.MoiQuanHeDTO;
import com.temp.service.Common.MoiQuanHeService;

@Service("MoiQuanHeServiceImpl")
public class MoiQuanHeServiceImpl implements MoiQuanHeService {

	@Autowired
	private MoiQuanHeLogic mqhLogic;

	@Override
	public MoiQuanHeDTO findById(int id) throws Exception {
		return mqhLogic.getById(id);
	}

	@Override
	public boolean saveMoiQuanHe(MoiQuanHeDTO dto) throws Exception {
		return mqhLogic.addOrUpDateMoiQuanHe(dto);
	}

	@Override
	public boolean updateMoiQuanHe(MoiQuanHeDTO dto) throws Exception {
		return mqhLogic.addOrUpDateMoiQuanHe(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return mqhLogic.deleteMoiQuanHe(id);
	}

	@Override
	public MoiQuanHeBDTO listMoiQuanHes(String filterkey, String sTenMoiQuanHe, String sMaMoiQuanHe, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		return mqhLogic.listMoiQuanHes(filterkey, sTenMoiQuanHe, sMaMoiQuanHe, pageNo, pageSize, keySort, desc,
				trangThai);
	}

	@Override
	public boolean isExistMoiQuanHeUpdate(MoiQuanHeDTO dto) {

		return mqhLogic.isExistMoiQuanHeUpdate(dto);

	}

	@Override
	public boolean isExistMoiQuanHeAdd(MoiQuanHeDTO dto) {

		return mqhLogic.isExistMoiQuanHeAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return mqhLogic.isExistById(id);
	}

	@Override
	public boolean isExistByMa(String maMoiQuanHe, Integer id) {
		// TODO Auto-generated method stub
		return mqhLogic.isExistByMa(maMoiQuanHe,id);
	}

}
