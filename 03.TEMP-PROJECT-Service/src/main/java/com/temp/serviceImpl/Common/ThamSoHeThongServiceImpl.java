package com.temp.serviceImpl.Common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.Common.ThamSoHeThongLogic;
import com.temp.model.Common.ThamSoHeThongBDTO;
import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.service.Common.ThamSoHeThongService;

@Service("ThamSoHeThongServiceImpl")
public class ThamSoHeThongServiceImpl implements ThamSoHeThongService {

	@Autowired
	private ThamSoHeThongLogic tshtLogic;

	@Override
	public ThamSoHeThongDTO findById(int id) throws Exception {
		return tshtLogic.getById(id);
	}
	
	@Override
	public ThamSoHeThongDTO findByThamSo(String thamSo) throws Exception {
		return tshtLogic.getByThamSo(thamSo);
	}

	@Override
	public boolean updateThamSoHeThong(ThamSoHeThongDTO dto) throws Exception {
		return tshtLogic.addOrUpDateThamSoHeThong(dto);
	}

	@Override
	public ThamSoHeThongBDTO listThamSoHeThongs(String filterkey, String sPhanHe, String sThamSo, String sGiaTri,
			Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai) {

		return tshtLogic.listThamSoHeThongs(filterkey, sPhanHe, sThamSo, sGiaTri, pageNo, pageSize, keySort, desc,
				trangThai);
	}

	@Override
	public boolean isExistById(int id) {

		return tshtLogic.isExistById(id);
	}

}
