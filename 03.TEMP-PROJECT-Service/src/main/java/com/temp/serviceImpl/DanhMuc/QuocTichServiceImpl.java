package com.temp.serviceImpl.DanhMuc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.DanhMuc.QuocTichLogic;
import com.temp.model.DropDownDTO;
import com.temp.model.DanhMuc.QuocTichBDTO;
import com.temp.model.DanhMuc.QuocTichDTO;
import com.temp.service.DanhMuc.QuocTichService;

@Service("QuocTichServiceImpl")
public class QuocTichServiceImpl implements QuocTichService {

	@Autowired
	private QuocTichLogic qtLogic; // gan bean cho bien

	@Override
	public QuocTichDTO findById(int id) throws Exception {
		return qtLogic.getById(id);
	}

	@Override
	public boolean saveQuocTich(QuocTichDTO dto) throws Exception {
		return qtLogic.addOrUpDateQuocTich(dto);
	}

	@Override
	public boolean updateQuocTich(QuocTichDTO dto) throws Exception {
		return qtLogic.addOrUpDateQuocTich(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return qtLogic.deleteQuocTich(id);
	}

	@Override
	public QuocTichBDTO listQuocTichs(String filterkey, String sTenQuocTich, String sMaQuocTich, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		return qtLogic.listQuocTichs(filterkey, sTenQuocTich, sMaQuocTich, pageNo, pageSize, keySort, desc, trangThai);
	}

	@Override
	public QuocTichBDTO listAll(String keySort, boolean desc, String trangThai) {

		return qtLogic.listAll(keySort, desc, trangThai);
	}

	@Override
	public boolean isExistQuocTichUpdate(QuocTichDTO dto) {

		return qtLogic.isExistQuocTichUpdate(dto);

	}

	@Override
	public boolean isExistQuocTichAdd(QuocTichDTO dto) {

		return qtLogic.isExistQuocTichAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return qtLogic.isExistById(id);
	}

	@Override
	public List<DropDownDTO> dropDownQuocTich() {
		return qtLogic.dropdownQuocTich();
	};
	
	@Override
	public int getIdByName(String name) {

		return qtLogic.getIdByName(name);
	}

}
