package com.temp.serviceImpl.DanhMuc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.DanhMuc.TinhThanhLogic;
import com.temp.model.DanhMuc.TinhThanhBDTO;
import com.temp.model.DanhMuc.TinhThanhDTO;
import com.temp.service.DanhMuc.TinhThanhService;

@Service("TinhThanhServiceImpl")
public class TinhThanhServiceImpl implements TinhThanhService {

	@Autowired
	private TinhThanhLogic ttLogic;

	@Override
	public TinhThanhDTO findById(int id) throws Exception {
		return ttLogic.getById(id);
	}

	@Override
	public boolean saveTinhThanh(TinhThanhDTO dto) throws Exception {
		return ttLogic.addOrUpDateTinhThanh(dto);
	}

	@Override
	public boolean updateTinhThanh(TinhThanhDTO dto) throws Exception {
		return ttLogic.addOrUpDateTinhThanh(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return ttLogic.deleteTinhThanh(id);
	}

	@Override
	public TinhThanhBDTO listTinhThanhs(String filterkey, String sTenTinhThanh, String sMaTinhThanh, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		return ttLogic.listTinhThanhs(filterkey, sTenTinhThanh, sMaTinhThanh, pageNo, pageSize, keySort, desc,
				trangThai);
	}
	@Override
	public TinhThanhBDTO listAll(String keySort, boolean desc,
			String trangThai) {

		return ttLogic.listAll(keySort, desc, trangThai);
	}

	@Override
	public boolean isExistTinhThanhUpdate(TinhThanhDTO dto) {

		return ttLogic.isExistTinhThanhUpdate(dto);

	}

	@Override
	public boolean isExistTinhThanhAdd(TinhThanhDTO dto) {

		return ttLogic.isExistTinhThanhAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return ttLogic.isExistById(id);
	}

}
