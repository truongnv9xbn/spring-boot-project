package com.temp.serviceImpl.DanhMuc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.DanhMuc.QuanHuyenLogic;
import com.temp.model.DanhMuc.QuanHuyenBDTO;
import com.temp.model.DanhMuc.QuanHuyenDTO;
import com.temp.service.DanhMuc.QuanHuyenService;

@Service("QuanHuyenServiceImpl")
public class QuanHuyenServiceImpl implements QuanHuyenService {

	@Autowired
	private QuanHuyenLogic ttLogic;

	@Override
	public QuanHuyenDTO findById(int id) throws Exception {
		return ttLogic.getById(id);
	}

	@Override
	public boolean saveQuanHuyen(QuanHuyenDTO dto) throws Exception {
		return ttLogic.addOrUpDateQuanHuyen(dto);
	}

	@Override
	public boolean updateQuanHuyen(QuanHuyenDTO dto) throws Exception {
		return ttLogic.addOrUpDateQuanHuyen(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return ttLogic.deleteQuanHuyen(id);
	}

	@Override
	public QuanHuyenBDTO listQuanHuyens(String filterkey, String sTenQuanHuyen, String sMaQuanHuyen, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai,String sTenTinhThanh) {

		return ttLogic.listQuanHuyens(filterkey, sTenQuanHuyen, sMaQuanHuyen, pageNo, pageSize, keySort, desc,
				trangThai,sTenTinhThanh);
	}
	@Override
	public QuanHuyenBDTO getListByTinhThanhID(String keySort, boolean desc, String trangThai,Integer tinhThanhId) {
		return ttLogic.getListByTinhThanhID(keySort, desc, trangThai,tinhThanhId);
	}

	@Override
	public boolean isExistQuanHuyenUpdate(QuanHuyenDTO dto) {

		return ttLogic.isExistQuanHuyenUpdate(dto);

	}

	@Override
	public boolean isExistQuanHuyenAdd(QuanHuyenDTO dto) {

		return ttLogic.isExistQuanHuyenAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return ttLogic.isExistById(id);
	}

}
