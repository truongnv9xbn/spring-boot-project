package com.temp.serviceImpl.DanhMuc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.temp.logic.DanhMuc.PhuongXaLogic;
import com.temp.model.DanhMuc.PhuongXaBDTO;
import com.temp.model.DanhMuc.PhuongXaDTO;
import com.temp.service.DanhMuc.PhuongXaService;

@Service("PhuongXaServiceImpl")
public class PhuongXaServiceImpl implements PhuongXaService {

	@Autowired
	private PhuongXaLogic ttLogic;

	@Override
	public PhuongXaDTO findById(int id) throws Exception {
		return ttLogic.getById(id);
	}

	@Override
	public boolean savePhuongXa(PhuongXaDTO dto) throws Exception {
		return ttLogic.addOrUpDatePhuongXa(dto);
	}

	@Override
	public boolean updatePhuongXa(PhuongXaDTO dto) throws Exception {
		return ttLogic.addOrUpDatePhuongXa(dto);
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		return ttLogic.deletePhuongXa(id);
	}

	@Override
	public PhuongXaBDTO listPhuongXas(String strfilter, String sTenPhuongXa, String sMaPhuongXa, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai,String sTenTinhThanh,String sTenQuanHuyen) {

		return ttLogic.listPhuongXas(strfilter, sTenPhuongXa, sMaPhuongXa, pageNo, pageSize, keySort, desc, trangThai, sTenTinhThanh, sTenQuanHuyen);
	}
	@Override
	public PhuongXaBDTO getListByTTandQH(String keySort, boolean desc,
			String trangThai,Integer tinhThanhId,Integer quanHuyenId) {

		return ttLogic.getListByTTandQH(keySort, desc, trangThai, tinhThanhId, quanHuyenId);
	}

	@Override
	public boolean isExistPhuongXaUpdate(PhuongXaDTO dto) {

		return ttLogic.isExistPhuongXaUpdate(dto);

	}

	@Override
	public boolean isExistPhuongXaAdd(PhuongXaDTO dto) {

		return ttLogic.isExistPhuongXaAdd(dto);

	}

	@Override
	public boolean isExistById(int id) {

		return ttLogic.isExistById(id);
	}

}
