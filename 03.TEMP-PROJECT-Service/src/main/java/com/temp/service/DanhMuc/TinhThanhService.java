package com.temp.service.DanhMuc;

import com.temp.model.DanhMuc.TinhThanhBDTO;
import com.temp.model.DanhMuc.TinhThanhDTO;

public interface TinhThanhService {

	/**
	 * serivce tìm kiếm 1 tỉnh thành
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	TinhThanhDTO findById(int id) throws Exception;

	/**
	 * service add mới tỉnh thành
	 * 
	 * @param TinhThanhDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveTinhThanh(TinhThanhDTO dto) throws Exception;

	/**
	 * service cập nhập tỉnh thành
	 * 
	 * @param TinhThanhDTO chưa id của tỉnh thành
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateTinhThanh(TinhThanhDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 tỉnh thành
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistTinhThanhUpdate(TinhThanhDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistTinhThanhAdd(TinhThanhDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	TinhThanhBDTO listTinhThanhs(String filterkey, String sTenTinhThanh, String sMaTinhThanh, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai);
	
	/**
	 * Lấy tất cả 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	
	TinhThanhBDTO listAll(String keySort, boolean desc, String trangThai);

}