package com.temp.service.DanhMuc;

import java.util.List;

import com.temp.model.DropDownDTO;
import com.temp.model.DanhMuc.QuocTichBDTO;
import com.temp.model.DanhMuc.QuocTichDTO;

public interface QuocTichService {

	/**
	 * serivce tìm kiếm 1 quốc tịch
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	QuocTichDTO findById(int id) throws Exception;

	/**
	 * service add mới quốc tịch
	 * 
	 * @param QuocTichDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveQuocTich(QuocTichDTO dto) throws Exception;

	/**
	 * service cập nhập quốc tịch
	 * 
	 * @param QuocTichDTO chưa id của quốc tịch
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateQuocTich(QuocTichDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 quốc tịch
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistQuocTichUpdate(QuocTichDTO dto);

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistQuocTichAdd(QuocTichDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	QuocTichBDTO listQuocTichs(String filterkey, String sTenQuocTich, String sMaQuocTich, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai);

	/**
	 * Lấy tất cả
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */

	QuocTichBDTO listAll(String keySort, boolean desc, String trangThai);

	/**
	 * Dropdown all dữ liệu
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	List<DropDownDTO> dropDownQuocTich();

	int getIdByName(String name);

}