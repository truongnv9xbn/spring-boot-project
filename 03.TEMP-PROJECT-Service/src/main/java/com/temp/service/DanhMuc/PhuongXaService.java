package com.temp.service.DanhMuc;

import com.temp.model.DanhMuc.PhuongXaBDTO;
import com.temp.model.DanhMuc.PhuongXaDTO;

public interface PhuongXaService {

	/**
	 * serivce tìm kiếm 1 tỉnh thành
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	PhuongXaDTO findById(int id) throws Exception;

	/**
	 * service add mới tỉnh thành
	 * 
	 * @param PhuongXaDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean savePhuongXa(PhuongXaDTO dto) throws Exception;

	/**
	 * service cập nhập tỉnh thành
	 * 
	 * @param PhuongXaDTO chưa id của tỉnh thành
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updatePhuongXa(PhuongXaDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 tỉnh thành
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistPhuongXaUpdate(PhuongXaDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistPhuongXaAdd(PhuongXaDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	PhuongXaBDTO listPhuongXas(String strfilter, String sTenPhuongXa, String sMaPhuongXa, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai,String sTenTinhThanh,String sTenQuanHuyen);

	/**
	 * Lấy tất cả
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */

	PhuongXaBDTO getListByTTandQH(String keySort, boolean desc, String trangThai, Integer tinhThanhId,
			Integer quanHuyenId);

}