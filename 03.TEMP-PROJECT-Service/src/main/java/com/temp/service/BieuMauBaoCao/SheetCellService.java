package com.temp.service.BieuMauBaoCao;

import java.util.List;

import com.temp.model.DropDownDTO;
import com.temp.model.DropDownStringDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCellMenuBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;

public interface SheetCellService {

    /**
     * service add mới sheet
     * 
     * @param DichVuDTO
     * @return trả về object được thêm hoặc đã được cập nhật
     * @throws Exception
     */
    BmSheetCtDTO AddOrUpdateSheet(BmSheetCtDTO dto) throws Exception;

    boolean isExistById(Integer id);

    BmSheetCtDTO FindById(Integer id);

    boolean DeleteById(Integer id);

    /**
     * lấy danh sách cell trong biểu mẫu
     * 
     * @param id
     * @return
     */

    BmSheetCellMenuBDTO listMenuLeftCellSheet(Integer SheetId, Integer cotId, Integer hangId,
	    Integer pageNo, Integer pageSize);

    boolean validateTuHangDenHang(Integer tuHangId, Integer denHangId);

    List<DropDownDTO> dropdownChitieu(Integer SheetId, Integer cotId, Integer hangId);

    boolean checkExistAddCell(Integer SheetId, Integer cotId, Integer hangIdFrom, Integer hangIdTo);

    List<DropDownStringDTO> dropdownChitieuCongThuc(Integer sheedId, Integer cotId, Integer hangId);

    /**
     * Lay danh sach sheet chi tieu theo danh sach sheet hang.
     * 
     * @param congThuc
     * @return
     */
    List<Integer> getDsSheetCtByLsSheetHang(String congThuc, String baoCaoId, String sheetBcId,
	    String cotId) throws Exception;

    List<BmSheetCtDTO> getDsCtFromBmBcId(List<Integer> bmBaoCaoId) throws Exception;

    /**
     * Get o chi tieu for canh bao chi tieu.
     * 
     * @param sheedId
     * @param cotId
     * @param hangId
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getOChiTieuCanhCanhDropdown(Integer sheedId, Integer cotId, Integer hangId)
	    throws Exception;

    /**
     * [C-20200518] Get data BmSheetCtDTO by field BmSheetCtVaoId.
     * 
     * @param parseInt
     * @return
     * @throws Exception
     */
    BmSheetCtDTO findByBmSheetCtVaoId(int BmSheetCtVaoId) throws Exception;

}