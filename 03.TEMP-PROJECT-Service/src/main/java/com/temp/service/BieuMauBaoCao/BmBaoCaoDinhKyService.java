package com.temp.service.BieuMauBaoCao;

import java.util.List;

import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyDTO;
import com.temp.model.BieuMauBaoCao.KyBcGiaTriBcDTO;

public interface BmBaoCaoDinhKyService {

	/**
	 * serivce tìm kiếm 1 cổ đông
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	BmBaoCaoDinhKyDTO findById(int id) throws Exception;

	/**
	 * thêm mới hoặc cập nhập 1 cổ đông ctck
	 * 
	 * @param dto
	 * @return
	 * @throws Exception
	 */
	BmBaoCaoDinhKyDTO addorUpdateBmBaoCaoDinhKy(BmBaoCaoDinhKyDTO dto) throws Exception;

	/**
	 * xóa 1 cổ đông
	 * 
	 * @param id truyền lên 1 cổ đông
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * check Exit update
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistUpdate(BmBaoCaoDinhKyDTO dto);

	/**
	 * check Exit add
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistAdd(BmBaoCaoDinhKyDTO dto);

	/**
	 * filter list theo điều kiện
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	BmBaoCaoDinhKyBDTO listBmBaoCaoDinhKy(String strfilter, Integer CtckId, Integer bmBaoCaoId, String sLoaiCoDong, Integer pageNo,
			Integer pageSize, String keySort, boolean desc);

	List<DropDownDTO> getBaoCaoDinhKyFromBcDropdownService(int bieumauId) throws Exception ;
	
	List<KyBcGiaTriBcDTO> getListKyBaoCaoFromBcThanhVien(List<Integer> lstCty) throws Exception;

}