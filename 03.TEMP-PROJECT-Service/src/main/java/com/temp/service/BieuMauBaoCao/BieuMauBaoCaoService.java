package com.temp.service.BieuMauBaoCao;

import com.temp.model.BieuMauBaoCao.BieuMauBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;

public interface BieuMauBaoCaoService {
	BieuMauBaoCaoBDTO getlist(
			Integer pageNo,
			Integer pageSize,
			boolean desc, 
			String strFilter,
			String tenBaoCao,
			String canCuPhapLy);
	boolean save(BmBaoCaoDTO dto) throws Exception;
	boolean update(BmBaoCaoDTO dto) throws Exception;
	boolean deleteById(int id) throws Exception;
	boolean isExistById(int id) throws Exception;
	BmBaoCaoDTO findById(int id) throws Exception;
}
