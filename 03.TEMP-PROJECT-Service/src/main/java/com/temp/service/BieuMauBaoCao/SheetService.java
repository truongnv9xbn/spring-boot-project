package com.temp.service.BieuMauBaoCao;

import java.util.List;

import com.temp.model.DataImportExcelBDTO;
import com.temp.model.DataImportExcelDTO;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BcThanhVienDTO;
import com.temp.model.BieuMauBaoCao.BmSheetBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;

public interface SheetService {

    /**
     * filter list theo điều kiện
     * 
     * @param filterkey
     * @param pageNo
     * @param pageSize
     * @param keySort
     * @param desc
     * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
     */
    BmSheetBDTO listSheet(String strfilter, Integer CtckId, Integer bmBaoCaoId, String sLoaiCoDong,
	    Integer pageNo, Integer pageSize, String keySort, boolean desc);
    
    BmSheetBDTO listSheetLazy(String strfilter, Integer CtckId, Integer bmBaoCaoId, String sLoaiCoDong,
    	    Integer pageNo, Integer pageSize, String keySort, boolean desc);


    /**
     * service add mới sheet
     * 
     * @param DichVuDTO
     * @return trả về object được thêm hoặc đã được cập nhật
     * @throws Exception
     */
    BmSheetDTO AddOrUpdateSheet(BmSheetDTO dto) throws Exception;

    boolean isExistUpdate(BmSheetDTO dto);

    boolean isExistById(Integer id);

    BmSheetDTO FindById(Integer id);

    BmSheetDTO getSheetBia(Integer bmBaoCaoId);

    BmSheetDTO InitDisplayEditSheet(Integer id) throws Exception;

    boolean checkExistMaSheet(String maSheet, Integer sheetId, Integer bmBaoCaoId);
    
    //check existsMaSheetLienThong
    boolean checkExistMaSheetLienThong(String maSheetLienThong, Integer sheetId, Integer bmBaoCaoId);

    boolean checkExistBia(Integer bmBaoCaoId);

    boolean checkExistMaSheetUpdate(String maSheet, Integer id);

    boolean DeleteById(Integer id) throws Exception;

    /**
     * lấy danh sách sheet trong biểu mẫu
     * 
     * @param id
     * @return
     */

    List<DropDownDTO> listSheet(Integer id);

    /**
     * Lay thong tin sheet theo bao cao
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getSheetFromBcDropdownService(int baocaoId) throws Exception;

    /**
     * Thiet lap noi dung sheet
     * 
     * @param id
     * @return
     * @throws Exception
     */
    BmSheetDTO getNoiDungBaoCaoSheetId(Integer id, String giaTriKyBaoCao, Integer baoCaoId,
	    int thanhVienId) throws Exception;
    
    List<BmSheetDTO> getNoiDungBaoCaoAllSheet(String giaTriKyBaoCao, Integer baoCaoId,
    	    int thanhVienId) throws Exception;

    /**
     * Lay danh sach sheet id theo bieu mau
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getlsSheetbyBaoCaoService(Integer baocaoId) throws Exception;
    
    /**
     * Lay danh sach sheet id theo bieu mau (Not trang bia).
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getlsSheetNotTrangBiabyBaoCaoService(Integer baocaoId) throws Exception;

    /**
     * Lay gia tri trong bao cao sheet chi tiet.
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    BmSheetDTO getGiaTriBaoCaoSheetId(Integer sheetId, Integer thanhVienId, Integer baoCaoId)
	    throws Exception;

    /**
     * Get sheet form data from import excel file service
     * 
     * @param result
     * @return
     */
    List<BmSheetDTO> buildSheetDataFromImportExcelFileService(List<DataImportExcelDTO> result,
	    List<DropDownDTO> lsSheetDropDown, int baoCaoId) throws Exception;

    /**
     * Build dynamic sheet form data import excel file service 
     * 
     * @param result
     * @return
     * @throws Exception
     */
    List<BmSheetDTO> buildSheetDynamicImportExcelService(DataImportExcelBDTO result, List<DropDownDTO> lsSheetIds)
	    throws Exception;

    /**
     * init Eform Gui Bao Cao Khac
     * 
     * @param sheetId
     * @param baoCaoId
     * @return
     * @throws Exception
     */
    BmSheetDTO initEformGuiBaoCaoKhac(Integer sheetId, BcThanhVienDTO thanhVienDto) throws Exception;

}