package com.temp.service.BieuMauBaoCao;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Workbook;

import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BcKhaiThacGtBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoExportDTO;
import com.temp.model.BieuMauBaoCao.BmLichSuBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;

public interface BmBaoCaoService {

    BmLichSuBaoCaoBDTO lichSuThayDoi(int pageNo, Integer pageSize, String keySort, int bmBaoCaoId)
	    throws Exception;

    /**
     * serivce tìm kiếm 1 cổ đông
     * 
     * @param id tham số truyền vào
     * @return 1 object
     * @throws Exception khi xảy ra exception
     */
    BmBaoCaoDTO findById(int id) throws Exception;

    BmSheetCtDTO getBmSheetCtById(Integer bmSheetCtId);

    BmBaoCaoExportDTO getDataExport(int bmBaoCaoId) throws Exception;

    /**
     * thêm mới hoặc cập nhập 1 cổ đông ctck
     * 
     * @param dto
     * @return
     * @throws Exception
     */
    BmBaoCaoDTO addorUpdateBmBaoCao(BmBaoCaoDTO dto) throws Exception;
    BmBaoCaoDTO addorUpdateBmBaoCao(BmBaoCaoDTO dto, String lichSuStr) throws Exception;

    /**
     * xóa 1 cổ đông
     * 
     * @param id truyền lên 1 cổ đông
     * @return trả về true nếu thành công hoặc ngược lại
     * @throws Exception khi xảy ra ngoai lệ
     */
    boolean deleteById(int id) throws Exception;

    boolean copyById(int id) throws Exception;

    boolean duaVaoSuDung(int id) throws Exception;

    /*
     * @param isSuDung - 1: Su dung; 0: Khong su dung
     * */
    boolean suDungBaoCao(int id, int isSuDung) throws Exception;

    /**
     * check Exit by ID
     * 
     * @param dto
     * @return
     */
    boolean isExistById(int id);

    /**
     * check Exit update
     * 
     * @param dto
     * @return
     */
    boolean isExistUpdate(BmBaoCaoDTO dto);

    /**
     * check Exit add
     * 
     * @param dto
     * @return
     */
    boolean isExistAdd(BmBaoCaoDTO dto);

    BmBaoCaoBDTO listBieuMauBaoCao(String keySearch, String tenBaoCao, String canCuPhapLy,
	    Integer trangThai, String loaiBaoCao, Integer pageNo, Integer pageSize, String keySort,
	    boolean desc, Integer kieuBaoCao, String maBaoCao, String nhomBaoCao);

    /**
     * check Exit add
     * 
     * @param dto
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getBmBaoCaoDropDownService() throws Exception;

    Workbook generateFileTemplateExcel(BmBaoCaoExportDTO dto,Workbook workbook, Boolean bieuMauDauRa ) throws Exception;

    BmBaoCaoBDTO listBmBaoCao(Integer nguoiDungId, Integer kieuBaoCao, String strfilter, String tenBaoCao,
	    String maBaoCao, String ccPhapLy, Integer pageNo, Integer pageSize, String keySort,
	    boolean desc);

    /**
     * Lay ds Bao cao (not Bao Cao dinh ky).
     * 
     * @param baoCaoType
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getLsBaoCaoKhacDropDownService(String baoCaoType) throws Exception;
    
    BmBaoCaoBDTO getLsBaoCaoKhacDropDownServiceBDTO(String baoCaoType) throws Exception;

    
    /**
     * Get danh sach bao cao for canh bao
     * 
     * @return
     * @throws Exception
     */
    List<DropDownDTO> getBmBaoCaoForCanhBaoCtService() throws Exception;
    
    boolean checkExistMaBaoCao(String maBaoCao, Integer bmId, Integer bmBaoCaoId) throws Exception ;
    
    boolean checkExistLoaiBaoCaoLT(String loaiBaoCaoLT, Integer bmId, Integer bmBaoCaoId) throws Exception ;

	Workbook generateFileTemplateExcelKhaiThac(BcKhaiThacGtBDTO lstBcKhaiThacGt, BmBaoCaoExportDTO dto, Workbook workbook, Boolean bieuMauDauRa, Map<Integer, Integer> mapSheet) throws Exception;

    boolean checkBatBuocCTBaoCao(Integer bcThanhVien, Integer bmBaoCaoId) throws Exception ;
    
}