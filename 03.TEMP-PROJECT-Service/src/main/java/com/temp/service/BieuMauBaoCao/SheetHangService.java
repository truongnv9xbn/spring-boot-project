package com.temp.service.BieuMauBaoCao;

import java.util.List;

import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangMenuBDTO;

public interface SheetHangService {

	/**
	 * service add mới sheet
	 * 
	 * @param DichVuDTO
	 * @return trả về object được thêm hoặc đã được cập nhật
	 * @throws Exception
	 */
	BmSheetHangDTO AddOrUpdateHangSheet(BmSheetHangDTO dto) throws Exception;

	/**
	 * Lấy danh sách cột cấu hình sheet
	 * 
	 * @param DichVuDTO
	 * @return trả về danh sách được thêm hoặc đã được cập nhật
	 * @throws Exception
	 */
	BmSheetHangMenuBDTO List(Integer idBieuMau, Integer pageNo, Integer pageSize) throws Exception;

	/**
	 * Lấy danh sách hàng cấu hình sheet
	 * 
	 * @param DichVuDTO
	 * @return trả về danh sách được thêm hoặc đã được cập nhật
	 * @throws Exception
	 */
	List<DropDownDTO> listHangDropdown(Integer sheedId) throws Exception;

	boolean isExistByHangSheetId(Integer id);

	BmSheetHangDTO FindById(Integer id);

	boolean checkExistMaHangSheet(String maHang , Integer id, Integer sheetId);
	
	boolean checkExistMaHangLT(String maHangLT , Integer id, Integer sheetId);

	boolean checkExistMaHangSheetUpdate(String maHang, Integer id,Integer sheetId);

	boolean delete(Integer id);

	boolean resetSTT(Integer id);

	boolean setMaHangBySTT(Integer id);
	
	long maxSTTBySheetId(Integer id);

}