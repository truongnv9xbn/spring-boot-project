package com.temp.service.BieuMauBaoCao;

import java.util.List;

import com.temp.model.BieuMauBaoCao.BmSheetMegerCellDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangCotDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangDTO;
import com.temp.model.dropdown.DropdownMegerCell;

public interface SheetMegerHeaderService {

    BmSheetMegerCellDTO InitDisplayCauHinhMegerHangCot(int sheetId);

    BmTieuDeHangDTO addOrUpdateHang(BmTieuDeHangDTO dto);

    BmTieuDeHangCotDTO addOrUpdateHangCot(BmTieuDeHangCotDTO megerHangCot);

    boolean DeleteRows(Integer tieuDeHangId);
    boolean DeleteRowHangCot(Integer tieuDeHangId);

    List<DropdownMegerCell> DropDownHang(Integer sheetId);

    boolean checkExistAddMeger(Integer sheetId, int fisrtRow, int lastRow, int fisrtColumn,
	    int lastColumn);

    boolean checkExistTieuDeHang(Integer sheetId) throws Exception;

}