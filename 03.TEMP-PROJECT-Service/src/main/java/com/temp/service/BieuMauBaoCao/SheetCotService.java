package com.temp.service.BieuMauBaoCao;

import java.util.List;

import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotMenuBDTO;
import com.temp.model.dropdown.DropdownMegerCell;

public interface SheetCotService {

	/**
	 * service add mới sheet
	 * 
	 * @param DichVuDTO
	 * @return trả về object được thêm hoặc đã được cập nhật
	 * @throws Exception
	 */
	BmSheetCotDTO AddOrUpdateCotSheet(BmSheetCotDTO dto) throws Exception;

	/**
	 * Lấy danh sách cột cấu hình sheet
	 * 
	 * @param DichVuDTO
	 * @return trả về danh sách được thêm hoặc đã được cập nhật
	 * @throws Exception
	 */
	BmSheetCotMenuBDTO List(Integer idSheet, Integer pageNo,Integer pageSize) throws Exception;

	/**
	 * Lấy danh sách dropdown tính công thức
	 * 
	 * @param idBieuMau
	 * @return trả về danh sách được thêm hoặc đã được cập nhật
	 * @throws Exception
	 */
	List<DropDownDTO> listCotDropdown(Integer idSheet) throws Exception;

	boolean isExistByCotSheetId(Integer id);

	BmSheetCotDTO FindById(Integer id);

	boolean checkExistMaCotSheet(String maCot,Integer sheetId);

	boolean checkExistMaCotSheetUpdate(String maCot, Integer id, Integer sheetId);
	
	boolean checkExistMaCotLT(String maCotLT, Integer id, Integer sheetId);
	
	/**
	 * dropdown dùng cho màn hình merger ô
	 * 
	 * @param sheetId
	 * @return
	 */
	List<DropdownMegerCell> DropDownMegerHangCot(Integer sheetId);

	boolean deleteColumn(Integer id);
	
	long maxSTTBySheetId(Integer id);

}