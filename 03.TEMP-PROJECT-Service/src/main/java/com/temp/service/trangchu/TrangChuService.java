package com.temp.service.trangchu;

import com.temp.model.TrangChuBDTO;
import com.temp.model.TrangChuDTO;

public interface TrangChuService {
	TrangChuBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc,String trangThai);
	
	boolean save(TrangChuDTO dto) throws Exception;
}
