package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.LkChucNangNhomNguoiBDTO;
import com.temp.model.NguoiDung.LkChucNangNhomNguoiDTO;

public interface LkChucNangNhomNguoiService {

	/**
	 * fiter list theo Ä‘iá»u kiá»‡n
	 * 
	 * @param filterkey
	 * @param sNhomChucVu
	 * @param sTenChucVu
	 * @param sMaChucVu
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return tráº£ vá» list Ä‘á»‘i tÆ°á»£ng thá»a mÃ£n filter hoáº·c 1 list
	 *         rá»—ng
	 */
	LkChucNangNhomNguoiBDTO filterChucVu(String filterkey, Integer pageNo, Integer pageSize, String keySort,
			boolean desc);

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean save(LkChucNangNhomNguoiDTO dto) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
//	boolean isExistAdd(LkChucNangNhomNguoiDTO dto);

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean update(LkChucNangNhomNguoiDTO dto) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
//	boolean isExistUpdate(LkChucNangNhomNguoiDTO dto);

//	/**
//	 * Check exits
//	 * 
//	 * @param nhomNguoiDungId
//	 * @return true if exits else return false
//	 */
//	boolean isExistByNhomNguoiDungId(int nhomNguoiDungId);

	/**
	 * xÃ³a
	 * 
	 * @param nhomNguoiDungId truyá»n lÃªn
	 * @return tráº£ vá» true náº¿u thÃ nh cÃ´ng hoáº·c ngÆ°á»£c láº¡i
	 * @throws Exception khi xáº£y ra ngoai lá»‡
	 */
	boolean deleteLkChucNangNhomNguoi(int nhomNguoiDungId) throws Exception;

	LkChucNangNhomNguoiDTO findByNhomNguoiDungId(int nhomNguoiDungId) throws Exception;

	public boolean isLkChucNangNhomNguoiExistAdd(int nhomNguoiDungId, int chucNangId);

}