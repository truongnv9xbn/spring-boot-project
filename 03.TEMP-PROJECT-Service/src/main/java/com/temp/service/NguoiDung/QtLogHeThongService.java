package com.temp.service.NguoiDung;

import java.util.List;

import com.temp.model.NguoiDung.QtLogHeThongBDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;


public interface QtLogHeThongService {

	QtLogHeThongBDTO listQtLogHeThongs(String filterkey, String ipThucHien, String noiDung, String tuNgay, String denNgay, String logType, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai,String taiKhoan);
	/**
	 * service add mới ngành nghề kd
	 * 
	 * @param NganhNgheKdDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean AddLogHeThong(QtLogHeThongDTO dto) throws Exception;
	boolean deleteMulti(List<Integer> lstId);
	boolean deleteById(int id)throws Exception;
	boolean isExistById(int id) throws Exception;
}