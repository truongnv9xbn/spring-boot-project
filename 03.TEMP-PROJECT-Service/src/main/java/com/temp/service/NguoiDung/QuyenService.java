package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.QuyenBDTO;
import com.temp.model.NguoiDung.QuyenDTO;
import com.temp.model.NguoiDung.SubQuyenDTO;

public interface QuyenService {
	QuyenDTO findById(int id) throws Exception;

	boolean isExistByMa(String maQuyen, Integer id);

	boolean saveQuyen(QuyenDTO dto) throws Exception;

	boolean changeStatus(SubQuyenDTO dto) throws Exception;

	boolean updateQuyen(QuyenDTO dto) throws Exception;

	boolean deleteById(int id) throws Exception;

	boolean isExistQuyen(QuyenDTO dto);

	boolean isExistById(int id);

	QuyenBDTO getAll(String keySearch, String tenQuyen, String maQuyen, Integer action, String phanHe,
			String ghiChu, String trangThai);

	QuyenBDTO listQuyens(String keySearch, String tenQuyen, String maQuyen, Integer action, String phanHe,
			String ghiChu, String trangThai);

	QuyenBDTO getDropdownKhoaChaId();

	QuyenBDTO getDropdownAction();

}