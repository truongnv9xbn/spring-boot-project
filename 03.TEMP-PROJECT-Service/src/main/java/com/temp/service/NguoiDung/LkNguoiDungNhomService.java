package com.temp.service.NguoiDung;

import java.util.List;

import com.temp.model.NguoiDung.LkNguoiDungNhomDTO;


public interface LkNguoiDungNhomService {

	/**
	 * serivce tìm kiếm 1 tỉnh thành
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	LkNguoiDungNhomDTO findById(int id) throws Exception;

	/**
	 * service add mới tỉnh thành
	 * 
	 * @param LkNguoiDungNhomDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveLkNguoiDungNhom(List<LkNguoiDungNhomDTO> LitsLkNguoiDungNhomDTO) throws Exception;

	/**
	 * service cập nhập tỉnh thành
	 * 
	 * @param LkNguoiDungNhomDTO chưa id của tỉnh thành
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateLkNguoiDungNhom(List<LkNguoiDungNhomDTO> LitsLkNguoiDungNhomDTO) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 tỉnh thành
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

}