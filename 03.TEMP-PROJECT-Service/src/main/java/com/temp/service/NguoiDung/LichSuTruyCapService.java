package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.QtLichSuTruyCapBDTO;

public interface LichSuTruyCapService {
	
	
	/**
	 * getListLichSuTruyCapServ - Service
	 * 
	 * @param keysearch
	 * @param denNgay
	 * @param ipThucHien
	 * @param tenTaiKhoan
	 * @param tuNgay
	 * @param logType
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return
	 */
	QtLichSuTruyCapBDTO getListLichSuTruyCapService(String keysearch, String denNgay, String ipThucHien,
			String tenTaiKhoan, String tuNgay, String logType, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String loaiNguoiDung,Integer ctckThongTinId);

	/**
	 * Get list lich su truy cap for export excel
	 * 
	 * @param tuNgay
	 * @param denNgay
	 * @param keySort
	 * @param desc
	 * @return
	 */
	QtLichSuTruyCapBDTO getListLichSuTruyCapExportService(String tuNgay, String denNgay,
		String keySort, boolean desc) throws Exception;
}
