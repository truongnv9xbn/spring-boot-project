package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.ChucNangBDTO;
import com.temp.model.NguoiDung.ChucNangDTO;
import com.temp.model.NguoiDung.SubChucNangDTO;

public interface ChucNangService {

	/**
	 * serivce tìm kiếm 1 chức năng
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	ChucNangDTO findById(int id) throws Exception;

	/**
	 * service add mới chức năng
	 * 
	 * @param ChucNangDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean isExistByMa(String maChucNang, Integer id);
	
	boolean saveChucNang(ChucNangDTO dto) throws Exception;

	/**
	 * service cập nhập chức năng
	 * 
	 * @param ChucNangDTO chưa id của chức năng
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean changeStatus(SubChucNangDTO dto) throws Exception;

	/**
	 * service cập nhập chức năng
	 * 
	 * @param ChucNangDTO chưa id của chức năng
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateChucNang(ChucNangDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 chức năng
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistChucNang(ChucNangDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	ChucNangBDTO getAll(String keySearch, String tenChucNang, String maChucNang, Integer action, String phanHe,
			String ghiChu, String trangThai);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	ChucNangBDTO listChucNangs(String keySearch, String tenChucNang, String maChucNang, Integer action, String phanHe,
			String ghiChu, String trangThai);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	ChucNangBDTO getDropdownKhoaChaId();

	ChucNangBDTO getDropdownAction();

}