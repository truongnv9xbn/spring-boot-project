package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.LkChucNangNguoiBDTO;

public interface LkChucNangNguoiService {

 

	/**
	 * service add mới quốc tịch
	 * 
	 * @param ChucNangDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveChucNang(LkChucNangNguoiBDTO dto) throws Exception;

 

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 quốc tịch
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

 

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	 LkChucNangNguoiBDTO listChucNangs(String filterkey, Integer pageNo,Integer pageSize, String keySort, boolean desc, Integer idNguoiDung);
			
	
	
 

}