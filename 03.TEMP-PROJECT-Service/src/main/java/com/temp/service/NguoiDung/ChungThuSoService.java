package com.temp.service.NguoiDung;

import java.util.List;

import com.temp.model.NguoiDung.ChungThuSoBDTO;
import com.temp.model.NguoiDung.ChungThuSoDTO;
import com.temp.persistance.entity.NguoiDung.ChungThuSoEntity;

public interface ChungThuSoService {
	ChungThuSoBDTO list(Integer pageNo, Integer pageSize, boolean desc, String filterkey, String taiKhoan, Integer trangThai);
	ChungThuSoDTO findById(Integer id);
	List<Object> findByUserId(Integer id);
	boolean isExistById(Integer id);
	
	//update nhưng cts chỉ sửa mỗi trạng thái
	boolean changeStatus(Integer id, Integer trangThai) throws Exception;
	
	boolean deleteById(Integer id) throws Exception;
	
	ChungThuSoEntity saveCts(ChungThuSoDTO dto) throws Exception;
	
	boolean checkExisted(ChungThuSoDTO dto) throws Exception;
}
