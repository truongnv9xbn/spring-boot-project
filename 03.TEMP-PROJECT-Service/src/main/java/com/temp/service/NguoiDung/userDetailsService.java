package com.temp.service.NguoiDung;

import org.springframework.security.core.userdetails.UserDetails;

import com.temp.model.NguoiDung.QtNguoiDungDTO;


public interface userDetailsService {

	/**
	 * serivce tìm kiếm 1 tỉnh thành
	 * 
	 * @param id tham số truyền vào
	 * @throws Exception khi xảy ra exception
	 */
	QtNguoiDungDTO findByUsername(String username);

	UserDetails loadUserByUsername(String username);


//	boolean addOrUpdate(QtNguoiDungDTO user);
}