package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.ChucNangChiTietBDTO;
import com.temp.model.NguoiDung.ChucNangChiTietDTO;

public interface ChucNangChiTietService {
	ChucNangChiTietBDTO filter(String keySearch, String tenChucNang, String trangThai,Integer pageNo,Integer pageSize, String keySort, boolean desc);
	
	boolean addOrUpdate(ChucNangChiTietDTO dto) throws Exception;
	
	ChucNangChiTietDTO findById(int id) throws Exception;
	
	boolean deleteById(int id) throws Exception;
}
