package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.QtNDIpJoinNguoiDungDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpBDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpDTO;

public interface QtNguoiDungIpService {

	/**
	 * serivce tìm kiếm 1 Qt Nguoi dung ip
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	QtNguoiDungIpDTO findById(int id) throws Exception;
	
	QtNDIpJoinNguoiDungDTO findByJoinNDId(int id) throws Exception;

	/**
	 * service add mới Qt Nguoi dung ip
	 * 
	 * @param QtNguoiDungIpDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveQtNguoiDungIp(QtNguoiDungIpDTO dto) throws Exception;

	/**
	 * service cập nhập Qt Nguoi dung ip
	 * 
	 * @param QtNguoiDungIpDTO chưa id của Qt Nguoi dung ip
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateQtNguoiDungIp(QtNguoiDungIpDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 Qt Nguoi dung ip
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;
	boolean deleteByNguoiDungId(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistQtNguoiDungIp(QtNguoiDungIpDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	QtNguoiDungIpBDTO listQtNguoiDungIps(String filterkey, Integer nguoiDungId,String ip,Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String hoatDong);

}