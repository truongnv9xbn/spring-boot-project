package com.temp.service.NguoiDung;

import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.NhomNguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungDTO;

public interface QtNhomNguoiDungService {

	/**
	 * serivce tìm kiếm 1 qt nhóm người dùng
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	QtNhomNguoiDungDTO findById(int id) throws Exception;
	
	NhomNguoiDungJoinAllDTO findByJoinAllId(int id) throws Exception;

	/**
	 * service add mới qt nhóm người dùng
	 * 
	 * @param QtNhomNguoiDungDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	QtNhomNguoiDungDTO createQtNhomNguoiDung(QtNhomNguoiDungDTO dto) throws Exception;

	/**
	 * service cập nhập qt nhóm người dùng
	 * 
	 * @param QtNhomNguoiDungDTO chưa id của qt nhóm người dùng
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	QtNhomNguoiDungDTO updateQtNhomNguoiDung(QtNhomNguoiDungDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 qt nhóm người dùng
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistQtNhomNguoiDung(QtNhomNguoiDungDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);
	
	boolean changeStatus(int id) throws Exception;

 
	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	
	QtNhomNguoiDungBDTO getList(Integer pageNo,Integer pageSize, String keySort, boolean desc, String keySearch, String tenNhomNguoiDung, String ghiChu, String trangThai);

	/**
	 * Lấy tất cả
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */

	QtNhomNguoiDungBDTO listAll(String keySort, boolean desc, String trangThai);
	
	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	QtNhomNguoiDungBDTO getDropdown();
	
	NguoiDungJoinAllDTO getTreeFunctionShow();

}