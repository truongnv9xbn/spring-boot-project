package com.temp.service.NguoiDung;

import java.util.List;

import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.authen.VerifyLoginDto;
import com.temp.model.file.ChucNangUserBDTO;

public interface QtNguoiDungService {

	/**
	 * serivce tìm kiếm 1 tỉnh thành
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	QtNguoiDungDTO findById(int id) throws Exception;

	QtNguoiDungDTO findByUsername(String username);

	NguoiDungJoinAllDTO findByJoinAllId(int id) throws Exception;
	
	boolean isExistByMa(String maNguoiDung, Integer id);

	/**
	 * service add mới tỉnh thành
	 * 
	 * @param QtNguoiDungDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveQtNguoiDung(QtNguoiDungDTO dto) throws Exception;

	QtNguoiDungDTO saveQtNguoiDungGetObject(QtNguoiDungDTO dto) throws Exception;

	QtNguoiDungDTO refreshSaveToken(QtNguoiDungDTO dto) throws Exception;

	/**
	 * service cập nhập tỉnh thành
	 * 
	 * @param QtNguoiDungDTO chưa id của tỉnh thành
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateQtNguoiDung(QtNguoiDungDTO dto) throws Exception;

	boolean changeStatusQtNguoiDung(Integer id, boolean trangThai, boolean thanhVien, boolean chuKySo) throws Exception;

	boolean changePasswordQtNguoiDung(Integer id, String matKhauMoi) throws Exception;

	boolean resetPasswordQtNguoiDung(Integer id) throws Exception;

	boolean logoutUserLoginQtNguoiDung(Integer id) throws Exception;

	boolean isExistByUsername(String username);

	boolean phanQuyenChucNang(NguoiDungJoinAllDTO dto);

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 tỉnh thành
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistQtNguoiDung(QtNguoiDungDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	QtNguoiDungBDTO listQtNguoiDungs(String filterkey, String hoTen, String taiKhoan, String sEmail, String sDiDong,
			String sMaNguoiDung, Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai,
			Integer nhomNguoiDungId, Integer loaiNguoiDung);

	QtNguoiDungBDTO listQtNguoiDungLogins(String filterkey, Integer dmChucVuId, String hoTen, String taiKhoan,
			String sEmail, String sDiDong, String sMaNguoiDung, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String trangThai);

	QtNguoiDungBDTO listAllNguoiDung(String keySort, boolean desc, String trangThai);

	/**
	 * login
	 * 
	 * @param ip
	 * @param user
	 * @return
	 */
	public VerifyLoginDto isLoginVerity(String ip, QtNguoiDungDTO user, boolean isLogin);

	List<String> getAllChucNangChiTiet();

	ChucNangUserBDTO getChucNangAndMenu(int nguoiDungId, boolean getTreeMenu);

	ChucNangUserBDTO getMenuAdmin();

}