package com.temp.service.Common;

import com.temp.model.Common.ThamSoHeThongBDTO;
import com.temp.model.Common.ThamSoHeThongDTO;

public interface ThamSoHeThongService {

	/**
	 * serivce tìm kiếm 1 tham số hệ thống
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	ThamSoHeThongDTO findById(int id) throws Exception;
	
	ThamSoHeThongDTO findByThamSo(String thamSo) throws Exception;

	/**
	 * service cập nhập tham số hệ thống
	 * 
	 * @param ThamSoHeThongDTO chưa id của tham số hệ thống
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateThamSoHeThong(ThamSoHeThongDTO dto) throws Exception;

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	ThamSoHeThongBDTO listThamSoHeThongs(String filterkey, String sPhanHe, String sThamSo, String sGiaTri,
			Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai);

}