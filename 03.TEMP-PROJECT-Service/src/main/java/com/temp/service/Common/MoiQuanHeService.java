package com.temp.service.Common;

import com.temp.model.Common.MoiQuanHeBDTO;
import com.temp.model.Common.MoiQuanHeDTO;

public interface MoiQuanHeService {
	/**
	 * serivce tìm kiếm 1 mối quan hệ
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	MoiQuanHeDTO findById(int id) throws Exception;

	/**
	 * service add mới mối quan hệ
	 * 
	 * @param MoiQuanHeDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveMoiQuanHe(MoiQuanHeDTO dto) throws Exception;

	/**
	 * service cập nhập mối quan hệ
	 * 
	 * @param MoiQuanHeDTO chưa id của mối quan hệ
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateMoiQuanHe(MoiQuanHeDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 mối quan hệ
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistMoiQuanHeUpdate(MoiQuanHeDTO dto);

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistMoiQuanHeAdd(MoiQuanHeDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	MoiQuanHeBDTO listMoiQuanHes(String filterkey, String sTenMoiQuanHe, String sMaMoiQuanHe, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai);
	
	
	boolean isExistByMa(String maMoiQuanHe, Integer id);

}