package com.temp.service.Common;

import com.temp.model.Common.TaiLieuBDTO;
import com.temp.model.Common.TaiLieuDTO;

public interface TaiLieuService {
	
	TaiLieuBDTO getList(String keysearch, String sSoVanBan, String sTrichYeu, String sNguoiTao, String trangThai,
			Integer pageNo, Integer pageSize, String keySort, boolean desc);
	
	/**
	 * serivce tìm kiếm 1 mối quan hệ
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	TaiLieuDTO findById(int id) throws Exception;

	/**
	 * service add mới mối quan hệ
	 * 
	 * @param TaiLieuDTO
	 * @return trả về true hoặc false
	 * @throws Exception
	 */
	boolean saveTaiLieu(TaiLieuDTO dto) throws Exception;

	/**
	 * service cập nhập mối quan hệ
	 * 
	 * @param TaiLieuDTO chưa id của mối quan hệ
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

	boolean updateTaiLieu(TaiLieuDTO dto) throws Exception;

	/**
	 * xóa 1 chức vụ
	 * 
	 * @param id truyền lên 1 mối quan hệ
	 * @return trả về true nếu thành công hoặc ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */
	boolean deleteById(int id) throws Exception;

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistTaiLieuUpdate(TaiLieuDTO dto);

	/**
	 * Check exits
	 * 
	 * @param dto
	 * @return true if exits else return false
	 */
	boolean isExistTaiLieuAdd(TaiLieuDTO dto);

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(int id);

	/**
	 * filter list theo điều kiện cách 1
	 * 
	 * @param filterkey
	 * @param pageNo
	 * @param pageSize
	 * @param keySort
	 * @param desc
	 * @return trả về list đối tượng thỏa mãn filter hoặc 1 list rỗng
	 */
	TaiLieuBDTO listTaiLieus(String filterkey, String sTenTaiLieu, String sMaTaiLieu, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai);
	
	
//	boolean isExistByMa(String maTaiLieu, Integer id);

}