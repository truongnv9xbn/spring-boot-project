package com.temp.service.Common;

import java.util.List;
import java.util.Set;

import com.temp.model.ThongBaoBDTO;
import com.temp.model.ThongBaoDTO;

public interface ThongBaoService {

	/**
	 * serivce tìm kiếm 1 tham số hệ thống
	 * 
	 * @param id tham số truyền vào
	 * @return 1 object
	 * @throws Exception khi xảy ra exception
	 */
	ThongBaoDTO findById(Integer id) throws Exception;
	
//	ThongBaoDTO findByThamSo(String thamSo) throws Exception;

	/**
	 * service cập nhập tham số hệ thống
	 * 
	 * @param ThongBaoDTO chưa id của tham số hệ thống
	 * @return trả về true nếu thành công và ngược lại
	 * @throws Exception khi xảy ra ngoai lệ
	 */

//	boolean updateThongBao(ThongBaoDTO dto) throws Exception;

	/**
	 * check Exit by ID
	 * 
	 * @param dto
	 * @return
	 */
	boolean isExistById(Integer id);

	ThongBaoBDTO listThongBao(Integer pageNo, Integer pageSize, String filterKey, Integer nguoiDungId, boolean desc, Integer trangThai, String list);
	
	boolean addThongBao(String tieuDe, String noiDung, String linkHref, Set<Integer> nguoiNhan);
	
	boolean updateSeen(Integer id);
	
	boolean seenAll(List<Integer> lstTbId, Integer nguoiDungId);

}