package com.temp.service;

import com.temp.model.MessageBDTO;
import com.temp.model.MessageDTO;

public interface MessageService {
	MessageBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc,String trangThai);
	
	boolean save(MessageDTO dto) throws Exception;
}
