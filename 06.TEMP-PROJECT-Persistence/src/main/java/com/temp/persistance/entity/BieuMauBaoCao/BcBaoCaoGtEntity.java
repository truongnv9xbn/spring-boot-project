package com.temp.persistance.entity.BieuMauBaoCao;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BC_BAO_CAO_GT", catalog = "")
public class BcBaoCaoGtEntity {
    private Integer id;
    private String tenDanhMuc;
    private String idDanhMuc;
    private String textDanhMuc;
    private String giaTri;
    private String tenHang;
    private String tenCot;
    private String tenSheet;
    private Integer rowId;
    private String congThuc;
    private String congThucTong;
    private Long phienBan;
    private Timestamp ngaySoLieu;
    private Timestamp ngayTao;
    private Integer nguoiTaoId;
    private Timestamp ngayCapNhat;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "TEN_DANH_MUC", nullable = true, length = 1000)
    public String getTenDanhMuc() {
        return tenDanhMuc;
    }

    public void setTenDanhMuc(String tenDanhMuc) {
        this.tenDanhMuc = tenDanhMuc;
    }

    @Basic
    @Column(name = "ID_DANH_MUC", nullable = true, length = 2000)
    public String getIdDanhMuc() {
        return idDanhMuc;
    }

    public void setIdDanhMuc(String idDanhMuc) {
        this.idDanhMuc = idDanhMuc;
    }

    @Basic
    @Column(name = "TEXT_DANH_MUC", nullable = true, length = 1000)
    public String getTextDanhMuc() {
        return textDanhMuc;
    }

    public void setTextDanhMuc(String textDanhMuc) {
        this.textDanhMuc = textDanhMuc;
    }

    @Basic
    @Column(name = "GIA_TRI", nullable = true, length = 4000)
    public String getGiaTri() {
        return giaTri;
    }

    public void setGiaTri(String giaTri) {
        this.giaTri = giaTri;
    }

    @Basic
    @Column(name = "TEN_HANG", nullable = true, length = 1000)
    public String getTenHang() {
        return tenHang;
    }

    public void setTenHang(String tenHang) {
        this.tenHang = tenHang;
    }

    @Basic
    @Column(name = "TEN_COT", nullable = true, length = 1000)
    public String getTenCot() {
        return tenCot;
    }

    public void setTenCot(String tenCot) {
        this.tenCot = tenCot;
    }

    @Basic
    @Column(name = "TEN_SHEET", nullable = true, length = 1000)
    public String getTenSheet() {
        return tenSheet;
    }

    public void setTenSheet(String tenSheet) {
        this.tenSheet = tenSheet;
    }

    @Basic
    @Column(name = "ROW_ID", nullable = true, precision = 0)
    public Integer getRowId() {
        return rowId;
    }

    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    @Basic
    @Column(name = "CONG_THUC", nullable = true, length = 4000)
    public String getCongThuc() {
        return congThuc;
    }

    public void setCongThuc(String congThuc) {
        this.congThuc = congThuc;
    }

    @Basic
    @Column(name = "CONG_THUC_TONG", nullable = true, length = 2000)
    public String getCongThucTong() {
        return congThucTong;
    }

    public void setCongThucTong(String congThucTong) {
        this.congThucTong = congThucTong;
    }

    @Basic
    @Column(name = "PHIEN_BAN", nullable = true, precision = 0)
    public Long getPhienBan() {
        return phienBan;
    }

    public void setPhienBan(Long phienBan) {
        this.phienBan = phienBan;
    }

    @Basic
    @Column(name = "NGAY_SO_LIEU", nullable = true)
    public Timestamp getNgaySoLieu() {
        return ngaySoLieu;
    }

    public void setNgaySoLieu(Timestamp ngaySoLieu) {
        this.ngaySoLieu = ngaySoLieu;
    }

    @Basic
    @Column(name = "NGAY_TAO", nullable = true)
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_CAP_NHAT", nullable = true)
    public Timestamp getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Timestamp ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BcBaoCaoGtEntity that = (BcBaoCaoGtEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(tenDanhMuc, that.tenDanhMuc) &&
                Objects.equals(idDanhMuc, that.idDanhMuc) &&
                Objects.equals(textDanhMuc, that.textDanhMuc) &&
                Objects.equals(giaTri, that.giaTri) &&
                Objects.equals(tenHang, that.tenHang) &&
                Objects.equals(tenCot, that.tenCot) &&
                Objects.equals(tenSheet, that.tenSheet) &&
                Objects.equals(rowId, that.rowId) &&
                Objects.equals(congThuc, that.congThuc) &&
                Objects.equals(congThucTong, that.congThucTong) &&
                Objects.equals(phienBan, that.phienBan) &&
                Objects.equals(ngaySoLieu, that.ngaySoLieu) &&
                Objects.equals(ngayTao, that.ngayTao) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayCapNhat, that.ngayCapNhat);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tenDanhMuc, idDanhMuc, textDanhMuc, giaTri, tenHang, tenCot, tenSheet, rowId, congThuc, congThucTong, phienBan, ngaySoLieu, ngayTao, nguoiTaoId, ngayCapNhat);
    }
}
