/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author vuongtm1
 */
@Entity
@Table(name = "BM_SHEET_CT", catalog = "")
@XmlRootElement
public class BmSheetCtEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "BM_SHEET_CT_VAO_ID")
	private Integer bmSheetCtVaoId;

	@Size(max = 200)
	@Column(name = "DINH_DANG", length = 200)
	private String dinhDang;

	@Column(name = "DON_VI_TINH")
	private Long donViTinh;

	@Column(name = "GIA_TRI_DEFAULT")
	private Long giaTriDefault;

	@Column(name = "THU_TU")
	private Long thuTu;

	@Size(max = 4000)
	@Column(name = "MO_TA", length = 4000)
	private String moTa;

	@Column(name = "NGAY_CAP_NHAT")
	private Timestamp ngayCapNhat;

	@Column(name = "SU_DUNG")
	private Boolean suDung;

	@Column(name = "BAT_BUOC")
	private Boolean batBuoc;

	@Column(name = "CONG_THUC")
	private String congThuc;

	@Column(name = "CONG_THUC_TONG")
	private String congThucTong;

	@Column(name = "CONG_THUC_TEXT")
	private String congThucText;

	@Column(name = "PHIEN_BAN")
	private Long phienBan;

	@Size(max = 1000)
	@Column(length = 1000)
	private String label;

	@Basic
	@Column(name = "BM_BAO_CAO_ID", nullable = true, precision = 0)
	private Integer bmBaoCaoId;

	@JoinColumn(name = "BM_SHEET_ID", referencedColumnName = "ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private BmSheetEntity bmSheetId;

	@Basic
	@Column(name = "BM_SHEET_COT_ID", nullable = true, precision = 0)
	private Integer bmSheetCotId;

	@Basic
	@Column(name = "BM_SHEET_HANG_ID", nullable = true, precision = 0)
	private Integer bmSheetHangId;

	@Basic
	@Column(name = "DM_CHI_TIEU_ID", nullable = true, precision = 0)
	private Integer dmChiTieuId;

	@Column(name = "MAU_SAC", length = 20)
	private Integer mauSac;
	@Size(max = 20)
	@Column(name = "CAN_LE", length = 20)
	private String canLe;
	@Column(name = "IN_DAM")
	private Boolean inDam;
	@Column(name = "IN_NGHIENG")
	private Boolean inNghieng;
	@Column(name = "IN_HOA")
	private Boolean inHoa;
	@Column(name = "GACH_CHAN")
	private Boolean gachChan;

	@Size(max = 20)
	@Column(name = "DINH_DANG_KY_TU", length = 20)
	private String dinhDangKyTu;

	@Basic
	@Column(name = "DINH_DANG_BM_BAO_CAO_ID", nullable = true, precision = 0)
	private Integer dinhDangBmBaoCaoId;

	@Size(max = 100)
	@Column(name = "FILTER_ID", length = 100)
	private String filterId;

	public String getFilterId() {
		return filterId;
	}

	public void setFilterId(String filterId) {
		this.filterId = filterId;
	}

	public Integer getMauSac() {
		return mauSac;
	}

	public void setMauSac(Integer mauSac) {
		this.mauSac = mauSac;
	}

	public String getCanLe() {
		return canLe;
	}

	public void setCanLe(String canLe) {
		this.canLe = canLe;
	}

	public Boolean getInDam() {
		return inDam;
	}

	public Integer getDinhDangBmBaoCaoId() {
		return dinhDangBmBaoCaoId;
	}

	public void setDinhDangBmBaoCaoId(Integer dinhDangBmBaoCaoId) {
		this.dinhDangBmBaoCaoId = dinhDangBmBaoCaoId;
	}

	public Boolean getBatBuoc() {
		return batBuoc;
	}

	public void setBatBuoc(Boolean batBuoc) {
		this.batBuoc = batBuoc;
	}

	public void setInDam(Boolean inDam) {
		this.inDam = inDam;
	}

	public Boolean getInNghieng() {
		return inNghieng;
	}

	public void setInNghieng(Boolean inNghieng) {
		this.inNghieng = inNghieng;
	}

	public String getDinhDangKyTu() {
		return dinhDangKyTu;
	}

	public void setDinhDangKyTu(String dinhDangKyTu) {
		this.dinhDangKyTu = dinhDangKyTu;
	}

	public Boolean getInHoa() {
		return inHoa;
	}

	public void setInHoa(Boolean inHoa) {
		this.inHoa = inHoa;
	}

	public Boolean getGachChan() {
		return gachChan;
	}

	public void setGachChan(Boolean gachChan) {
		this.gachChan = gachChan;
	}

	public BmSheetCtEntity() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBmSheetCtVaoId() {
		return bmSheetCtVaoId;
	}

	public void setBmSheetCtVaoId(Integer bmSheetCtVaoId) {
		this.bmSheetCtVaoId = bmSheetCtVaoId;
	}

	public String getDinhDang() {
		return dinhDang;
	}

	public void setDinhDang(String dinhDang) {
		this.dinhDang = dinhDang;
	}

	public Long getDonViTinh() {
		return donViTinh;
	}

	public void setDonViTinh(Long donViTinh) {
		this.donViTinh = donViTinh;
	}

	public Long getGiaTriDefault() {
		return giaTriDefault;
	}

	public void setGiaTriDefault(Long giaTriDefault) {
		this.giaTriDefault = giaTriDefault;
	}

	public Long getThuTu() {
		return thuTu;
	}

	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}

	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public Boolean getSuDung() {
		return suDung;
	}

	public void setSuDung(Boolean suDung) {
		this.suDung = suDung;
	}

	public String getCongThuc() {
		return congThuc;
	}

	public void setCongThuc(String congThuc) {
		this.congThuc = congThuc;
	}

	public String getCongThucTong() {
		return congThucTong;
	}

	public void setCongThucTong(String congThucTong) {
		this.congThucTong = congThucTong;
	}

	public String getCongThucText() {
		return congThucText;
	}

	public void setCongThucText(String congThucText) {
		this.congThucText = congThucText;
	}

	public Long getPhienBan() {
		return phienBan;
	}

	public void setPhienBan(Long phienBan) {
		this.phienBan = phienBan;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getBmBaoCaoId() {
		return bmBaoCaoId;
	}

	public void setBmBaoCaoId(Integer bmBaoCaoId) {
		this.bmBaoCaoId = bmBaoCaoId;
	}

	public BmSheetEntity getBmSheetId() {
		return bmSheetId;
	}

	public void setBmSheetId(BmSheetEntity bmSheetId) {
		this.bmSheetId = bmSheetId;
	}

	public Integer getBmSheetCotId() {
		return bmSheetCotId;
	}

	public void setBmSheetCotId(Integer bmSheetCotId) {
		this.bmSheetCotId = bmSheetCotId;
	}

	public Integer getBmSheetHangId() {
		return bmSheetHangId;
	}

	public void setBmSheetHangId(Integer bmSheetHangId) {
		this.bmSheetHangId = bmSheetHangId;
	}

	public Integer getDmChiTieuId() {
		return dmChiTieuId;
	}

	public void setDmChiTieuId(Integer dmChiTieuId) {
		this.dmChiTieuId = dmChiTieuId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof BmSheetCtEntity)) {
			return false;
		}
		BmSheetCtEntity other = (BmSheetCtEntity) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Entity.BmSheetCt[ id=" + id + " ]";
	}

}
