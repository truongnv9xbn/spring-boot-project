package com.temp.persistance.entity.NguoiDung;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CHUNG_THU_SO", catalog = "")
public class ChungThuSoEntity {
	private Integer id;
	private Integer nguoiDungId;
	private String taiKhoan;
	private String keys;
	private String serial;
	private String subject;
	private String issuer;
	private Boolean isCaNhan;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Integer nguoiCapNhatId;
	private Timestamp ngayCapNhat;
	private Integer trangThai;
	
	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Basic
	@Column(name = "NGUOI_DUNG_ID")
	public Integer getNguoiDungId() {
		return nguoiDungId;
	}
	
	public void setNguoiDungId(Integer nguoiDungId) {
		this.nguoiDungId = nguoiDungId;
	}

	@Basic
	@Column(name = "TAI_KHOAN")
	public String getTaiKhoan() {
		return taiKhoan;
	}
	
	public void setTaiKhoan(String taiKhoan) {
		this.taiKhoan = taiKhoan;
	}

	@Basic
	@Column(name = "KEYS")
	public String getKeys() {
		return keys;
	}
	
	public void setKeys(String keys) {
		this.keys = keys;
	}

	@Basic
	@Column(name = "SERIAL")
	public String getSerial() {
		return serial;
	}
	
	public void setSerial(String serial) {
		this.serial = serial;
	}

	@Basic
	@Column(name = "SUBJECT")
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Basic
	@Column(name = "ISSUER")
	public String getIssuer() {
		return issuer;
	}
	
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	@Basic
	@Column(name = "IS_CA_NHAN")
	public Boolean getIsCaNhan() {
		return isCaNhan;
	}
	
	public void setIsCaNhan(Boolean isCaNhan) {
		this.isCaNhan = isCaNhan;
	}

	@Basic
	@Column(name = "NGUOI_TAO_ID")
	public Integer getNguoiTaoId() {
		return nguoiTaoId;
	}
	
	public void setNguoiTaoId(Integer nguoiTaoId) {
		this.nguoiTaoId = nguoiTaoId;
	}

	@Basic
	@Column(name = "NGAY_TAO")
	public Timestamp getNgayTao() {
		return ngayTao;
	}
	
	public void setNgayTao(Timestamp nguoiTao) {
		this.ngayTao = nguoiTao;
	}

	@Basic
	@Column(name = "NGUOI_CAP_NHAT_ID")
	public Integer getNguoiCapNhatId() {
		return nguoiCapNhatId;
	}
	
	public void setNguoiCapNhatId(Integer nguoiCapNhatId) {
		this.nguoiCapNhatId = nguoiCapNhatId;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT")
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}
	
	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "TRANG_THAI")
	public Integer getTrangThai() {
		return trangThai;
	}
	
	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChungThuSoEntity that = (ChungThuSoEntity) o;
        return id == that.id &&
                Objects.equals(nguoiDungId, that.nguoiDungId) &&
                Objects.equals(taiKhoan, that.taiKhoan) &&
                Objects.equals(keys, that.keys) &&
                Objects.equals(serial, that.serial) &&
                Objects.equals(subject, that.subject) &&
                Objects.equals(issuer, that.issuer) &&
                Objects.equals(isCaNhan, that.isCaNhan) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao) &&
                Objects.equals(nguoiCapNhatId, that.nguoiCapNhatId) &&
                Objects.equals(ngayCapNhat, that.ngayCapNhat) &&
                Objects.equals(trangThai, that.trangThai);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nguoiDungId, taiKhoan, keys, serial, subject, issuer, isCaNhan, nguoiTaoId, ngayTao, nguoiCapNhatId, ngayCapNhat, trangThai);
    }
}
