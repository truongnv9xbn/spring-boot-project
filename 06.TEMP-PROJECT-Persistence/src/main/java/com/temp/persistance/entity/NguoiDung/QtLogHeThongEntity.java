package com.temp.persistance.entity.NguoiDung;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QT_LOG_HE_THONG", catalog = "")
public class QtLogHeThongEntity {
    private Integer id;
    private String ipThucHien;
    private String noiDung;
    private String logType;
    private Integer nguoiTaoId;
    private Timestamp ngayTao;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "IP_THUC_HIEN")
    public String getIpThucHien() {
        return ipThucHien;
    }

    public void setIpThucHien(String ipThucHien) {
        this.ipThucHien = ipThucHien;
    }

    @Basic
    @Column(name = "NOI_DUNG")
    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }

    @Basic
    @Column(name = "LOG_TYPE")
    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID")
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_TAO")
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QtLogHeThongEntity that = (QtLogHeThongEntity) o;
        return id == that.id &&
                Objects.equals(ipThucHien, that.ipThucHien) &&
                Objects.equals(noiDung, that.noiDung) &&
                Objects.equals(logType, that.logType) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, ipThucHien, noiDung, logType, nguoiTaoId, ngayTao);
    }
}
