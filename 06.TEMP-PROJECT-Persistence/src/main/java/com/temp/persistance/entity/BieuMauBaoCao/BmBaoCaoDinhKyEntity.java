package com.temp.persistance.entity.BieuMauBaoCao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BM_BAO_CAO_DINH_KY", catalog = "")
public class BmBaoCaoDinhKyEntity {
    private Integer id;
    private String kyBaoCao;
    private Long t;
    private Long thoiGianMuonNhat;
    private Boolean donViTinh;
    private Long thoiGianLap;
    private Long gioPhutGui;
    private Date ngayDuKienInsert;
    private Integer suDung;
    private Boolean trangThai;
    private String ghiChu;
    private Integer nguoiTaoId;
    private Timestamp ngayTao;
    private Integer bmBaoCaoId;
    private Integer vaoNgay;
    

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "BM_BAO_CAO_ID")
    public Integer getBmBaoCaoId() {
        return bmBaoCaoId;
    }

    public void setBmBaoCaoId(Integer bmBaoCaoId) {
        this.bmBaoCaoId = bmBaoCaoId;
    }

    @Basic
    @Column(name = "KY_BAO_CAO", nullable = true, length = 20)
    public String getKyBaoCao() {
        return kyBaoCao;
    }

    public void setKyBaoCao(String kyBaoCao) {
        this.kyBaoCao = kyBaoCao;
    }

    @Basic
    @Column(name = "T", nullable = true, precision = 0)
    public Long getT() {
        return t;
    }

    public void setT(Long t) {
        this.t = t;
    }

    @Basic
    @Column(name = "THOI_GIAN_MUON_NHAT", nullable = true, precision = 0)
    public Long getThoiGianMuonNhat() {
        return thoiGianMuonNhat;
    }

    public void setThoiGianMuonNhat(Long thoiGianMuonNhat) {
        this.thoiGianMuonNhat = thoiGianMuonNhat;
    }

    @Basic
    @Column(name = "DON_VI_TINH", nullable = true, precision = 0)
    public Boolean getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(Boolean donViTinh) {
        this.donViTinh = donViTinh;
    }

    @Basic
    @Column(name = "THOI_GIAN_LAP", nullable = true, precision = 0)
    public Long getThoiGianLap() {
        return thoiGianLap;
    }

    public void setThoiGianLap(Long thoiGianLap) {
        this.thoiGianLap = thoiGianLap;
    }

    @Basic
    @Column(name = "GIO_PHUT_GUI", nullable = true, precision = 0)
    public Long getGioPhutGui() {
        return gioPhutGui;
    }

    public void setGioPhutGui(Long gioPhutGui) {
        this.gioPhutGui = gioPhutGui;
    }

    @Basic
    @Column(name = "NGAY_DU_KIEN_INSERT", nullable = true)
    public Date getNgayDuKienInsert() {
        return ngayDuKienInsert;
    }

    public void setNgayDuKienInsert(Date ngayDuKienInsert) {
        this.ngayDuKienInsert = ngayDuKienInsert;
    }

    @Basic
    @Column(name = "SU_DUNG", nullable = true, precision = 0)
    public Integer getSuDung() {
        return suDung;
    }

    public void setSuDung(Integer suDung) {
        this.suDung = suDung;
    }

    @Basic
    @Column(name = "TRANG_THAI", nullable = true, precision = 0)
    public Boolean getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    @Basic
    @Column(name = "GHI_CHU", nullable = true, length = 500)
    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_TAO", nullable = true)
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }
    
    @Basic
    @Column(name = "VAO_NGAY")
    public Integer getVaoNgay() {
        return vaoNgay;
    }

    public void setVaoNgay(Integer vaoNgay) {
        this.vaoNgay = vaoNgay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BmBaoCaoDinhKyEntity that = (BmBaoCaoDinhKyEntity) o;
        return Objects.equals(id, that.id) &&
        		Objects.equals(bmBaoCaoId, that.bmBaoCaoId) &&
                Objects.equals(kyBaoCao, that.kyBaoCao) &&
                Objects.equals(t, that.t) &&
                Objects.equals(thoiGianMuonNhat, that.thoiGianMuonNhat) &&
                Objects.equals(donViTinh, that.donViTinh) &&
                Objects.equals(thoiGianLap, that.thoiGianLap) &&
                Objects.equals(gioPhutGui, that.gioPhutGui) &&
                Objects.equals(ngayDuKienInsert, that.ngayDuKienInsert) &&
                Objects.equals(suDung, that.suDung) &&
                Objects.equals(trangThai, that.trangThai) &&
                Objects.equals(ghiChu, that.ghiChu) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao)&&
                Objects.equals(vaoNgay, that.vaoNgay);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bmBaoCaoId, kyBaoCao, t, thoiGianMuonNhat, donViTinh, thoiGianLap, gioPhutGui, ngayDuKienInsert, suDung, trangThai, ghiChu, nguoiTaoId, ngayTao, vaoNgay);
    }

}
