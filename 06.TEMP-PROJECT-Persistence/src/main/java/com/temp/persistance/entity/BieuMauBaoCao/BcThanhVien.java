package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the BC_THANH_VIEN database table.
 * 
 */
@Entity
@Table(name = "BC_THANH_VIEN")
public class BcThanhVien implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "BM_BAO_CAO_DINH_KY_ID")
	private Integer bmBaoCaoDinhKyId;

	@Column(name = "BM_BAO_CAO_ID")
	private Integer bmBaoCaoId;

	@Column(name = "CT_KIEM_TOAN_ID")
	private Integer ctKiemToanId;

	@Column(name = "CT_KIEM_TOAN_VIEN_ID")
	private Integer ctKiemToanVienId;

	@Column(name = "CTCK_THONG_TIN_ID")
	private Integer ctckThongTinId;

	@Column(name = "FILE_DINH_KEM")
	private String fileDinhKem;

	@Column(name = "GIA_TRI_KY_BC")
	private String giaTriKyBc;

	@Column(name = "KY_BAO_CAO")
	private String kyBaoCao;

	@Column(name = "LY_DO")
	private String lyDo;

	@Column(name = "LY_DO_GUI_LAI")
	private String lyDoGuiLai;

	@Column(name = "MO_TA")
	private String moTa;

	@Column(name = "NAM_CANH_BAO")
	private String namCanhBao;

	@Column(name = "NGAY_GUI")
	private Timestamp ngayGui;

	@Column(name = "NGAY_SO_LIEU")
	private Timestamp ngaySoLieu;

	@Column(name = "NGAY_TAO")
	private Timestamp ngayTao;

	@Column(name = "NGUOI_DUNG_ID")
	private Integer nguoiDungId;

	@Column(name = "PHIEN_BAN")
	private Integer phienBan;

	@Column(name = "THOI_DIEM_GUI")
	private String thoiDiemGui;

	@Column(name = "THOI_HAN_GIA_HAN")
	private Timestamp thoiHanGiaHan;

	@Column(name = "THOI_HAN_GUI")
	private Timestamp thoiHanGui;

	@Column(name = "TRANG_THAI")
	private Integer trangThai;

	@Column(name = "TRICH_YEU")
	private String trichYeu;

	@Column(name = "XOA_DU_LIEU")
	private Integer xoaDuLieu;

	@Column(name = "TRANG_THAI_KIEM_TOAN")
	private String trangThaiKiemToan;

	@Column(name = "LOAI_BC_GUI")
	private String loaiBcGui;

	@Column(name = "KHOA_BAO_CAO")
	private Integer khoaBaoCao;

	@Column(name = "HUY_BAO_CAO")
	private Integer huyBaoCao;

	@Column(name = "TEN_BAO_CAO")
	private String tenBaoCao;
	
	@Column(name = "LY_DO_GIA_HAN")
	private String lyDoGiaHan;
	
	@Column(name = "FILE_BAO_CAO")
	private String fileBaoCao;

	@Column(name = "IS_VALID_GUI_BC")
	private Integer isValidGuiBc;
	
	@Column(name = "FILENAME")
	private String fileName;
	
	@Column(name = "FILEDATA")
	private byte[] data;
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
	
	
	public String getFileBaoCao() {
		return fileBaoCao;
	}

	public void setFileBaoCao(String fileBaoCao) {
		this.fileBaoCao = fileBaoCao;
	}

	public Integer getIsValidGuiBc() {
		return isValidGuiBc;
	}

	public void setIsValidGuiBc(Integer isValidGuiBc) {
		this.isValidGuiBc = isValidGuiBc;
	}

	public Integer getHuyBaoCao() {
		return this.huyBaoCao;
	}

	public void setHuyBaoCao(Integer huyBaoCao) {
		this.huyBaoCao = huyBaoCao;
	}

	// bi-directional many-to-one association to BcBaoCaoGt
	@OneToMany(mappedBy = "bcThanhVien")
	private List<BcBaoCaoGt> bcBaoCaoGts;

	public BcThanhVien() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBmBaoCaoDinhKyId() {
		return this.bmBaoCaoDinhKyId;
	}

	public void setBmBaoCaoDinhKyId(Integer bmBaoCaoDinhKyId) {
		this.bmBaoCaoDinhKyId = bmBaoCaoDinhKyId;
	}

	public Integer getBmBaoCaoId() {
		return this.bmBaoCaoId;
	}

	public void setBmBaoCaoId(Integer bmBaoCaoId) {
		this.bmBaoCaoId = bmBaoCaoId;
	}

	public Integer getCtKiemToanId() {
		return this.ctKiemToanId;
	}

	public void setCtKiemToanId(Integer ctKiemToanId) {
		this.ctKiemToanId = ctKiemToanId;
	}

	public Integer getCtKiemToanVienId() {
		return this.ctKiemToanVienId;
	}

	public void setCtKiemToanVienId(Integer ctKiemToanVienId) {
		this.ctKiemToanVienId = ctKiemToanVienId;
	}

	public Integer getCtckThongTinId() {
		return this.ctckThongTinId;
	}

	public void setCtckThongTinId(Integer ctckThongTinId) {
		this.ctckThongTinId = ctckThongTinId;
	}

	public String getFileDinhKem() {
		return this.fileDinhKem;
	}

	public void setFileDinhKem(String fileDinhKem) {
		this.fileDinhKem = fileDinhKem;
	}

	public String getGiaTriKyBc() {
		return this.giaTriKyBc;
	}

	public void setGiaTriKyBc(String giaTriKyBc) {
		this.giaTriKyBc = giaTriKyBc;
	}

	public String getKyBaoCao() {
		return this.kyBaoCao;
	}

	public void setKyBaoCao(String kyBaoCao) {
		this.kyBaoCao = kyBaoCao;
	}

	public String getLyDo() {
		return this.lyDo;
	}

	public void setLyDo(String lyDo) {
		this.lyDo = lyDo;
	}

	public String getLyDoGuiLai() {
		return this.lyDoGuiLai;
	}

	public void setLyDoGuiLai(String lyDoGuiLai) {
		this.lyDoGuiLai = lyDoGuiLai;
	}

	public String getMoTa() {
		return this.moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	public String getNamCanhBao() {
		return this.namCanhBao;
	}

	public void setNamCanhBao(String namCanhBao) {
		this.namCanhBao = namCanhBao;
	}

	public Timestamp getNgayGui() {
		return this.ngayGui;
	}

	public void setNgayGui(Timestamp ngayGui) {
		this.ngayGui = ngayGui;
	}

	public Timestamp getNgaySoLieu() {
		return this.ngaySoLieu;
	}

	public void setNgaySoLieu(Timestamp ngaySoLieu) {
		this.ngaySoLieu = ngaySoLieu;
	}

	public Timestamp getNgayTao() {
		return this.ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	public Integer getNguoiDungId() {
		return this.nguoiDungId;
	}

	public void setNguoiDungId(Integer nguoiDungId) {
		this.nguoiDungId = nguoiDungId;
	}

	public Integer getPhienBan() {
		return this.phienBan;
	}

	public void setPhienBan(Integer phienBan) {
		this.phienBan = phienBan;
	}

	public String getThoiDiemGui() {
		return this.thoiDiemGui;
	}

	public void setThoiDiemGui(String thoiDiemGui) {
		this.thoiDiemGui = thoiDiemGui;
	}

	public Timestamp getThoiHanGiaHan() {
		return this.thoiHanGiaHan;
	}

	public void setThoiHanGiaHan(Timestamp thoiHanGiaHan) {
		this.thoiHanGiaHan = thoiHanGiaHan;
	}

	public Timestamp getThoiHanGui() {
		return this.thoiHanGui;
	}

	public void setThoiHanGui(Timestamp thoiHanGui) {
		this.thoiHanGui = thoiHanGui;
	}

	public Integer getTrangThai() {
		return this.trangThai;
	}

	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}

	public String getTrichYeu() {
		return this.trichYeu;
	}

	public void setTrichYeu(String trichYeu) {
		this.trichYeu = trichYeu;
	}

	public Integer getXoaDuLieu() {
		return this.xoaDuLieu;
	}

	public void setXoaDuLieu(Integer xoaDuLieu) {
		this.xoaDuLieu = xoaDuLieu;
	}

	public Integer getKhoaBaoCao() {
		return this.khoaBaoCao;
	}

	public void setKhoaBaoCao(Integer khoaBaoCao) {
		this.khoaBaoCao = khoaBaoCao;
	}

	public List<BcBaoCaoGt> getBcBaoCaoGts() {
		return this.bcBaoCaoGts;
	}

	public void setBcBaoCaoGts(List<BcBaoCaoGt> bcBaoCaoGts) {
		this.bcBaoCaoGts = bcBaoCaoGts;
	}

	public String getTrangThaiKiemToan() {
		return trangThaiKiemToan;
	}

	public void setTrangThaiKiemToan(String trangThaiKiemToan) {
		this.trangThaiKiemToan = trangThaiKiemToan;
	}

	public String getLoaiBcGui() {
		return loaiBcGui;
	}

	public void setLoaiBcGui(String loaiBcGui) {
		this.loaiBcGui = loaiBcGui;
	}

	public String getTenBaoCao() {
		return this.tenBaoCao;
	}

	public void setTenBaoCao(String tenBaoCao) {
		this.tenBaoCao = tenBaoCao;
	}
	
	public String getLyDoGiaHan() {
		return this.lyDoGiaHan;
	}

	public void setLyDoGiaHan(String lyDoGiaHan) {
		this.lyDoGiaHan = lyDoGiaHan;
	}


	public BcBaoCaoGt addBcBaoCaoGt(BcBaoCaoGt bcBaoCaoGt) {
		getBcBaoCaoGts().add(bcBaoCaoGt);
		bcBaoCaoGt.setBcThanhVien(this);

		return bcBaoCaoGt;
	}

	public BcBaoCaoGt removeBcBaoCaoGt(BcBaoCaoGt bcBaoCaoGt) {
		getBcBaoCaoGts().remove(bcBaoCaoGt);
		bcBaoCaoGt.setBcThanhVien(null);

		return bcBaoCaoGt;
	}

}