package com.temp.persistance.entity.NguoiDung;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QT_NHOM_NGUOI_DUNG", catalog = "")
public class QtNhomNguoiDungEntity {
	private int id;
	private Integer khoaChaId;
	private String maNhomNguoiDung;
	private String tenNhomNguoiDung;
	private String ghiChu;
	private String nguoiTao;
	private Timestamp ngayTao;
	private String nguoiCapNhat;
	private Timestamp ngayCapNhat;
	private Boolean trangThai;
//    private Integer isThanhVien;
//    private Integer nguoiTaoId;
//    private Timestamp ngayHetHan;
//    private Timestamp ngayBatDauDung;
//    private Integer nguoiCapNhatId;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "KHOA_CHA_ID")
	public Integer getKhoaChaId() {
		return khoaChaId;
	}

	public void setKhoaChaId(Integer khoaChaId) {
		this.khoaChaId = khoaChaId;
	}

	@Basic
	@Column(name = "MA_NHOM_NGUOI_DUNG")
	public String getMaNhomNguoiDung() {
		return maNhomNguoiDung;
	}

	public void setMaNhomNguoiDung(String maNhomNguoiDung) {
		this.maNhomNguoiDung = maNhomNguoiDung;
	}

	@Basic
	@Column(name = "TEN_NHOM_NGUOI_DUNG")
	public String getTenNhomNguoiDung() {
		return tenNhomNguoiDung;
	}

	public void setTenNhomNguoiDung(String tenNhomNguoiDung) {
		this.tenNhomNguoiDung = tenNhomNguoiDung;
	}

	@Basic
	@Column(name = "GHI_CHU")
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "NGAY_TAO")
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT")
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	/**
	 * author TruongNV date Mar 26, 2022
	 * 
	 * @return the nguoiTao
	 */
	@Basic
	@Column(name = "NGUOI_TAO")
	public String getNguoiTao() {
		return nguoiTao;
	}

	/**
	 * author TruongNV date Mar 26, 2022
	 * 
	 * @param nguoiTao the nguoiTao to set
	 */
	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	/**
	 * author TruongNV date Mar 26, 2022
	 * 
	 * @return the nguoiCapNhat
	 */
	@Basic
	@Column(name = "NGUOI_CAP_NHAT")
	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	/**
	 * author TruongNV date Mar 26, 2022
	 * 
	 * @param nguoiCapNhat the nguoiCapNhat to set
	 */
	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	@Basic
	@Column(name = "TRANG_THAI")
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}

	// bỏ
//    @Basic
//    @Column(name = "NGUOI_CAP_NHAT_ID")
//    public Integer getNguoiCapNhatId() {
//        return nguoiCapNhatId;
//    }
//
//    public void setNguoiCapNhatId(Integer nguoiCapNhatId) {
//        this.nguoiCapNhatId = nguoiCapNhatId;
//    }

//    @Basic
//    @Column(name = "NGAY_HET_HAN")
//    public Timestamp getNgayHetHan() {
//        return ngayHetHan;
//    }
//
//    public void setNgayHetHan(Timestamp ngayHetHan) {
//        this.ngayHetHan = ngayHetHan;
//    }
//    
//    @Basic
//    @Column(name = "NGAY_BAT_DAU_DUNG")
//    public Timestamp getNgayBatDauDung() {
//        return ngayBatDauDung;
//    }
//
//    public void setNgayBatDauDung(Timestamp ngayBatDauDung) {
//        this.ngayBatDauDung = ngayBatDauDung;
//    }
//    
//    @Basic
//    @Column(name = "IS_THANH_VIEN")
//    public Integer getIsThanhVien() {
//		return isThanhVien;
//	}
//
//	public void setIsThanhVien(Integer isThanhVien) {
//		this.isThanhVien = isThanhVien;
//	}
//	
//	@Basic
//    @Column(name = "NGUOI_TAO_ID")
//    public Integer getNguoiTaoId() {
//        return nguoiTaoId;
//    }
//
//    public void setNguoiTaoId(Integer nguoiTaoId) {
//        this.nguoiTaoId = nguoiTaoId;
//    }

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		QtNhomNguoiDungEntity that = (QtNhomNguoiDungEntity) o;
		return id == that.id && Objects.equals(khoaChaId, that.khoaChaId)
				&& Objects.equals(maNhomNguoiDung, that.maNhomNguoiDung)
				&& Objects.equals(tenNhomNguoiDung, that.tenNhomNguoiDung) && Objects.equals(ghiChu, that.ghiChu)
				&& Objects.equals(nguoiTao, that.nguoiTao) && Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(nguoiCapNhat, that.nguoiCapNhat) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(trangThai, that.trangThai);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, khoaChaId, maNhomNguoiDung, tenNhomNguoiDung, ghiChu, nguoiTao, ngayTao, nguoiCapNhat,
				ngayCapNhat, trangThai);
	}
}
