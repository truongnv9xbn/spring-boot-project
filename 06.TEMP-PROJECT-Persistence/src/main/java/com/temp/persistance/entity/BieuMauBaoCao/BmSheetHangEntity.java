/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author VuongTM
 */
@Entity
@Table(name = "BM_SHEET_HANG", catalog = "")
@XmlRootElement
 
public class BmSheetHangEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Size(max = 200)
    @Column(name = "MA_HANG", length = 200)
    private String maHang;
    
    @Size(max = 200)
    @Column(name = "MA_HANG_LT", length = 200)
    private String maHangLT;
    
    @Size(max = 1000)
    @Column(name = "TEN_HANG", length = 1000)
    private String tenHang;
    @Column(name = "THU_TU")
    private Long thuTu;
    @Size(max = 20)
    @Column(name = "DINH_DANG_TEN", length = 20)
    private String dinhDangTen;
    @Size(max = 20)
    @Column(name = "MAU_SAC", length = 20)
    private String mauSac;
    @Size(max = 20)
    @Column(name = "CAN_LE", length = 20)
    private String canLe;
    @Column(name = "HANG_DONG")
    private Boolean hangDong;
    @Size(max = 4000)
    @Column(name = "MO_TA", length = 4000)
    private String moTa;
    @Column(name = "SU_DUNG")
    private Boolean suDung;
    @Column(name = "TRANG_THAI")
    private Long trangThai;
    @Column(name = "PHIEN_BAN")
    private Integer phienBan;
    @Column(name = "NGAY_CAP_NHAT")
    private Timestamp ngayCapNhat;
    @JoinColumn(name = "BM_BAO_CAO_ID", referencedColumnName = "ID")
    @ManyToOne
    private BmBaoCaoEntity bmBaoCaoByBmBaoCaoId;
    @JoinColumn(name = "BM_SHEET_ID", referencedColumnName = "ID")
    
    @ManyToOne
    private BmSheetEntity bmSheetByBmSheetId;
    
    @OneToMany(mappedBy = "bmSheetHangId")
    private Collection<BmSheetCtEntity> bmSheetCtCollection;

    public BmSheetHangEntity() {
    }

    public BmSheetHangEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMaHang() {
        return maHang;
    }

    public void setMaHang(String maHang) {
        this.maHang = maHang;
    }
    
    public String getMaHangLT() {
		return maHangLT;
	}

	public void setMaHangLT(String maHangLT) {
		this.maHangLT = maHangLT;
	}

	public String getTenHang() {
        return tenHang;
    }

    public void setTenHang(String tenHang) {
        this.tenHang = tenHang;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public String getDinhDangTen() {
        return dinhDangTen;
    }

    public void setDinhDangTen(String dinhDangTen) {
        this.dinhDangTen = dinhDangTen;
    }

    public String getMauSac() {
        return mauSac;
    }

    public void setMauSac(String mauSac) {
        this.mauSac = mauSac;
    }

    public String getCanLe() {
        return canLe;
    }

    public void setCanLe(String canLe) {
        this.canLe = canLe;
    }

    public Boolean getHangDong() {
        return hangDong;
    }

    public void setHangDong(Boolean hangDong) {
        this.hangDong = hangDong;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Boolean getSuDung() {
        return suDung;
    }

    public void setSuDung(Boolean suDung) {
        this.suDung = suDung;
    }

    public Long getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Long trangThai) {
        this.trangThai = trangThai;
    }

    public Integer getPhienBan() {
        return phienBan;
    }

    public void setPhienBan(Integer phienBan) {
        this.phienBan = phienBan;
    }

    public Timestamp getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Timestamp ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public BmBaoCaoEntity getBmBaoCaoId() {
        return bmBaoCaoByBmBaoCaoId;
    }

    public void setBmBaoCaoId(BmBaoCaoEntity bmBaoCaoByBmBaoCaoId) {
        this.bmBaoCaoByBmBaoCaoId = bmBaoCaoByBmBaoCaoId;
    }

    public BmSheetEntity getBmSheetId() {
        return bmSheetByBmSheetId;
    }

    public void setBmSheetId(BmSheetEntity bmSheetByBmSheetId) {
        this.bmSheetByBmSheetId = bmSheetByBmSheetId;
    }

    @XmlTransient
    public Collection<BmSheetCtEntity> getBmSheetCtCollection() {
        return bmSheetCtCollection;
    }

    public void setBmSheetCtCollection(Collection<BmSheetCtEntity> bmSheetCtCollection) {
        this.bmSheetCtCollection = bmSheetCtCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmSheetHangEntity)) {
            return false;
        }
        BmSheetHangEntity other = (BmSheetHangEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "java_demo.folder.BmSheetHang[ id=" + id + " ]";
    }
    
}
