package com.temp.persistance.entity.DanhMuc;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DM_PHUONG_XA", catalog = "")
public class DmPhuongXaEntity {
    private Integer id;
    private Integer tinhThanhId;
    private Integer quanHuyenId;
    private String maPhuongXa;
    private String tenPhuongXa;
    private String ghiChu;
    private Integer nguoiTaoId;
    private Timestamp ngayTao;
    private Boolean trangThai;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MA_PHUONG_XA", length = 50)
    public String getMaPhuongXa() {
        return maPhuongXa;
    }

    public void setMaPhuongXa(String maPhuongXa) {
        this.maPhuongXa = maPhuongXa;
    }

    @Basic
    @Column(name = "TEN_PHUONG_XA",length = 250)
    public String getTenPhuongXa() {
        return tenPhuongXa;
    }

    public void setTenPhuongXa(String tenPhuongXa) {
        this.tenPhuongXa = tenPhuongXa;
    }

    @Basic
    @Column(name = "GHI_CHU", nullable = true, length = 1000)
    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_TAO", nullable = true)
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Basic
    @Column(name = "TRANG_THAI", nullable = true, precision = 0)
    public Boolean getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    @Column(name = "QUAN_HUYEN_ID")
	public Integer getQuanHuyenId() {
		return quanHuyenId;
	}

	public void setQuanHuyenId(Integer quanHuyenId) {
		this.quanHuyenId = quanHuyenId;
	}

	public void setTinhThanhId(Integer tinhThanhId) {
		this.tinhThanhId = tinhThanhId;
	}

    @Column(name = "TINH_THANH_ID")
	public Integer getTinhThanhId() {
		return tinhThanhId;
	}

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DmPhuongXaEntity that = (DmPhuongXaEntity) o;
        return id == that.id &&
        		tinhThanhId == that.tinhThanhId &&
        		quanHuyenId == that.quanHuyenId &&
                Objects.equals(maPhuongXa, that.maPhuongXa) &&
                Objects.equals(tenPhuongXa, that.tenPhuongXa) &&
                Objects.equals(ghiChu, that.ghiChu) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao) &&
                Objects.equals(trangThai, that.trangThai);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id,tinhThanhId,quanHuyenId, maPhuongXa, tenPhuongXa, ghiChu, nguoiTaoId, ngayTao, trangThai);
    }
}
