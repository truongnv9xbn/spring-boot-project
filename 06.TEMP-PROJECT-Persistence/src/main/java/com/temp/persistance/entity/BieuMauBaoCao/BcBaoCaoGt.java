package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the BC_BAO_CAO_GT database table.
 * 
 */
@Entity
@Table(name = "BC_BAO_CAO_GT")
public class BcBaoCaoGt implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "BM_BAO_CAO_DINH_KY_ID")
    private Integer bmBaoCaoDinhKyId;

    @Column(name = "BM_BAO_CAO_ID")
    private Integer bmBaoCaoId;

    @Column(name = "BM_SHEET_COT_ID")
    private Integer bmSheetCotId;

    @Column(name = "BM_SHEET_CT_ID")
    private Integer bmSheetCtId;

    @Column(name = "BM_SHEET_HANG_ID")
    private Integer bmSheetHangId;

    @Column(name = "BM_SHEET_ID")
    private Integer bmSheetId;

    @Column(name = "CONG_THUC")
    private String congThuc;

    @Column(name = "CONG_THUC_TONG")
    private String congThucTong;

    @Column(name = "CTCK_THONG_TIN_ID")
    private Integer ctckThongTinId;

    @Column(name = "DM_CHI_TIEU_ID")
    private Integer dmChiTieuId;

    @Column(name = "GIA_TRI")
    private String giaTri;

    @Column(name = "ID_DANH_MUC")
    private String idDanhMuc;

    @Column(name = "NGAY_CAP_NHAT")
    private Timestamp ngayCapNhat;

    @Column(name = "NGAY_SO_LIEU")
    private String ngaySoLieu;

    @Column(name = "NGAY_TAO")
    private Timestamp ngayTao;

    @Column(name = "NGUOI_TAO_ID")
    private Integer nguoiTaoId;

    @Column(name = "PHIEN_BAN")
    private Integer phienBan;

    @Column(name = "ROW_ID")
    private Integer rowId;

    @Column(name = "TEN_COT")
    private String tenCot;

    @Column(name = "TEN_DANH_MUC")
    private String tenDanhMuc;

    @Column(name = "TEN_HANG")
    private String tenHang;

    @Column(name = "TEN_SHEET")
    private String tenSheet;

    @Column(name = "TEXT_DANH_MUC")
    private String textDanhMuc;

    // update entities
    @Column(name = "BM_SHEET_CT_VAO_ID")
    private Integer bmSheetCtVaoId;

    @Column(name = "GIA_TRI_KY_BC")
    private String giaTriKyBc;

    @Column(name = "KY_BAO_CAO")
    private String kyBaoCao;

    @Column(name = "NAM")
    private String nam;

    @Column(name = "TRANG_THAI_GUI")
    private boolean trangThaiGui;
    
    @Column(name = "DON_VI_TINH")
    private Integer donViTinh;
    
    @Column(name = "DINH_DANG")
    private String dinhDang;
    
    @Column(name = "THU_TU_HANG")
    private Integer thuTuHang;
    
    public Integer getThuTuHang() {
		return thuTuHang;
	}

	public void setThuTuHang(Integer thuTuHang) {
		this.thuTuHang = thuTuHang;
	}

	// bi-directional many-to-one association to BcThanhVien
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "BC_THANH_VIEN_ID")
    private BcThanhVien bcThanhVien;

    // Use for Gui Bao Cao Dinh Ky

    public BcBaoCaoGt() {
    }

    public long getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    public Integer getBmBaoCaoDinhKyId() {
	return this.bmBaoCaoDinhKyId;
    }

    public void setBmBaoCaoDinhKyId(Integer bmBaoCaoDinhKyId) {
	this.bmBaoCaoDinhKyId = bmBaoCaoDinhKyId;
    }

    public Integer getBmBaoCaoId() {
	return this.bmBaoCaoId;
    }

    public void setBmBaoCaoId(Integer bmBaoCaoId) {
	this.bmBaoCaoId = bmBaoCaoId;
    }

    public Integer getBmSheetCotId() {
	return this.bmSheetCotId;
    }

    public void setBmSheetCotId(Integer bmSheetCotId) {
	this.bmSheetCotId = bmSheetCotId;
    }

    public Integer getBmSheetCtId() {
	return this.bmSheetCtId;
    }

    public void setBmSheetCtId(Integer bmSheetCtId) {
	this.bmSheetCtId = bmSheetCtId;
    }

    public Integer getBmSheetHangId() {
	return this.bmSheetHangId;
    }

    public void setBmSheetHangId(Integer bmSheetHangId) {
	this.bmSheetHangId = bmSheetHangId;
    }

    public Integer getBmSheetId() {
	return this.bmSheetId;
    }

    public void setBmSheetId(Integer bmSheetId) {
	this.bmSheetId = bmSheetId;
    }

    public String getCongThuc() {
	return this.congThuc;
    }

    public void setCongThuc(String congThuc) {
	this.congThuc = congThuc;
    }

    public String getCongThucTong() {
	return this.congThucTong;
    }

    public void setCongThucTong(String congThucTong) {
	this.congThucTong = congThucTong;
    }

    public Integer getCtckThongTinId() {
	return this.ctckThongTinId;
    }

    public void setCtckThongTinId(Integer ctckThongTinId) {
	this.ctckThongTinId = ctckThongTinId;
    }

    public Integer getDmChiTieuId() {
	return this.dmChiTieuId;
    }

    public void setDmChiTieuId(Integer dmChiTieuId) {
	this.dmChiTieuId = dmChiTieuId;
    }

    public String getGiaTri() {
	return this.giaTri;
    }

    public void setGiaTri(String giaTri) {
	this.giaTri = giaTri;
    }

    public String getIdDanhMuc() {
	return this.idDanhMuc;
    }

    public void setIdDanhMuc(String idDanhMuc) {
	this.idDanhMuc = idDanhMuc;
    }

    public Timestamp getNgayCapNhat() {
	return this.ngayCapNhat;
    }

    public void setNgayCapNhat(Timestamp ngayCapNhat) {
	this.ngayCapNhat = ngayCapNhat;
    }

    public String getNgaySoLieu() {
	return this.ngaySoLieu;
    }

    public void setNgaySoLieu(String ngaySoLieu) {
	this.ngaySoLieu = ngaySoLieu;
    }

    public Timestamp getNgayTao() {
	return this.ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
	this.ngayTao = ngayTao;
    }

    public Integer getNguoiTaoId() {
	return this.nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
	this.nguoiTaoId = nguoiTaoId;
    }

    public Integer getPhienBan() {
	return this.phienBan;
    }

    public void setPhienBan(Integer phienBan) {
	this.phienBan = phienBan;
    }

    public Integer getRowId() {
	return this.rowId;
    }

    public void setRowId(Integer rowId) {
	this.rowId = rowId;
    }

    public String getTenCot() {
	return this.tenCot;
    }

    public void setTenCot(String tenCot) {
	this.tenCot = tenCot;
    }

    public String getTenDanhMuc() {
	return this.tenDanhMuc;
    }

    public void setTenDanhMuc(String tenDanhMuc) {
	this.tenDanhMuc = tenDanhMuc;
    }

    public String getTenHang() {
	return this.tenHang;
    }

    public void setTenHang(String tenHang) {
	this.tenHang = tenHang;
    }

    public String getTenSheet() {
	return this.tenSheet;
    }

    public void setTenSheet(String tenSheet) {
	this.tenSheet = tenSheet;
    }

    public String getTextDanhMuc() {
	return this.textDanhMuc;
    }

    public void setTextDanhMuc(String textDanhMuc) {
	this.textDanhMuc = textDanhMuc;
    }

    public BcThanhVien getBcThanhVien() {
	return this.bcThanhVien;
    }

    public void setBcThanhVien(BcThanhVien bcThanhVien) {
	this.bcThanhVien = bcThanhVien;
    }

    public Integer getBmSheetCtVaoId() {
	return bmSheetCtVaoId;
    }

    public void setBmSheetCtVaoId(Integer bmSheetCtVaoId) {
	this.bmSheetCtVaoId = bmSheetCtVaoId;
    }

    public String getGiaTriKyBc() {
	return giaTriKyBc;
    }

    public void setGiaTriKyBc(String giaTriKyBc) {
	this.giaTriKyBc = giaTriKyBc;
    }

    public String getKyBaoCao() {
	return kyBaoCao;
    }

    public void setKyBaoCao(String kyBaoCao) {
	this.kyBaoCao = kyBaoCao;
    }

    public String getNam() {
	return nam;
    }

    public void setNam(String nam) {
	this.nam = nam;
    }

    public boolean isTrangThaiGui() {
        return trangThaiGui;
    }

    public void setTrangThaiGui(boolean trangThaiGui) {
        this.trangThaiGui = trangThaiGui;
    }

	public Integer getDonViTinh() {
		return donViTinh;
	}

	public void setDonViTinh(Integer donViTinh) {
		this.donViTinh = donViTinh;
	}

	public String getDinhDang() {
		return dinhDang;
	}

	public void setDinhDang(String dinhDang) {
		this.dinhDang = dinhDang;
	}
    
    

}