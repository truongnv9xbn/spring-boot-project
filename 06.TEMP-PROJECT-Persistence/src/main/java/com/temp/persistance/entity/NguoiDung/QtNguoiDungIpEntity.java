package com.temp.persistance.entity.NguoiDung;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QT_NGUOI_DUNG_IP", catalog = "")
public class QtNguoiDungIpEntity {
    private Integer id;
    private Integer nguoiDungId;
    private Timestamp thoiGianDangNhap;
    private String ip;
//    private String nguoiDungHoTen;
    private Boolean isHoatDong;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "NGUOI_DUNG_ID")
    public Integer getNguoiDungId() {
        return nguoiDungId;
    }

    public void setNguoiDungId(int nguoiDungId) {
        this.nguoiDungId = nguoiDungId;
    }

    public void setNguoiDungId(Integer nguoiDungId) {
        this.nguoiDungId = nguoiDungId;
    }

    @Basic
    @Column(name = "THOI_GIAN_DANG_NHAP")
    public Timestamp getThoiGianDangNhap() {
        return thoiGianDangNhap;
    }

    public void setThoiGianDangNhap(Timestamp thoiGianDangNhap) {
        this.thoiGianDangNhap = thoiGianDangNhap;
    }

    @Basic
    @Column(name = "IP")
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "IS_HOAT_DONG")
    public Boolean getHoatDong() {
        return isHoatDong;
    }

    public void setHoatDong(Boolean hoatDong) {
        isHoatDong = hoatDong;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QtNguoiDungIpEntity that = (QtNguoiDungIpEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nguoiDungId, that.nguoiDungId) &&
                Objects.equals(thoiGianDangNhap, that.thoiGianDangNhap) &&
                Objects.equals(ip, that.ip) &&
                Objects.equals(isHoatDong, that.isHoatDong);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nguoiDungId, thoiGianDangNhap, ip, isHoatDong);
    }
}
