package com.temp.persistance.entity.NguoiDung;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QUYEN_CHI_TIET", catalog = "")
public class QuyenChiTietEntity {
	private int id;
	private String tenQuyen;
	private String vueRoute;
	private String apiRoute;

	private Boolean trangThai;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "TEN_QUYEN")
	public String getTenQuyen() {
		return tenQuyen;
	}

	public void setTenQuyen(String tenQuyen) {
		this.tenQuyen = tenQuyen;
	}

	@Basic
	@Column(name = "VUE_ROUTE")
	public String getVueRoute() {
		return vueRoute;
	}

	public void setVueRoute(String vueRoute) {
		this.vueRoute = vueRoute;
	}

	@Basic
	@Column(name = "API_ROUTE")
	public String getApiRoute() {
		return apiRoute;
	}

	public void setApiRoute(String apiRoute) {
		this.apiRoute = apiRoute;
	}

	@Basic
	@Column(name = "TRANG_THAI")
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}
}
