package com.temp.persistance.entity.NguoiDung;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QT_CHUC_NANG_CHI_TIET", catalog = "")
public class QtChucNangChiTietEntity {
	private int id;
	private String tenChucNang;
	private String vueRoute;
	private String apiRoute;

	private Boolean trangThai;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "TEN_CHUC_NANG")
	public String getTenChucNang() {
		return tenChucNang;
	}

	public void setTenChucNang(String tenChucNang) {
		this.tenChucNang = tenChucNang;
	}

	@Basic
	@Column(name = "VUE_ROUTE")
	public String getVueRoute() {
		return vueRoute;
	}

	public void setVueRoute(String vueRoute) {
		this.vueRoute = vueRoute;
	}

	@Basic
	@Column(name = "API_ROUTE")
	public String getApiRoute() {
		return apiRoute;
	}

	public void setApiRoute(String apiRoute) {
		this.apiRoute = apiRoute;
	}

	@Basic
	@Column(name = "TRANG_THAI")
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}
}
