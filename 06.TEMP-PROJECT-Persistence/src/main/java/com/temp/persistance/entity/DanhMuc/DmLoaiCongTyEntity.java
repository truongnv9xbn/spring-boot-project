package com.temp.persistance.entity.DanhMuc;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DM_LOAI_CONG_TY", catalog = "")
public class DmLoaiCongTyEntity {
	private int id;
	private String maLoaiCongTy;
	private String tenLoaiCongTy;
	private String ghiChu;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Boolean trangThai;
	private Integer ctCoPhan;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "MA_LOAI_CONG_TY", nullable = true, length = 50)
	public String getMaLoaiCongTy() {
		return maLoaiCongTy;
	}

	public void setMaLoaiCongTy(String maLoaiCongTy) {
		this.maLoaiCongTy = maLoaiCongTy;
	}

	@Basic
	@Column(name = "TEN_LOAI_CONG_TY", nullable = true, length = 250)
	public String getTenLoaiCongTy() {
		return tenLoaiCongTy;
	}

	public void setTenLoaiCongTy(String tenLoaiCongTy) {
		this.tenLoaiCongTy = tenLoaiCongTy;
	}

	@Basic
	@Column(name = "GHI_CHU", nullable = true, length = 1000)
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
	public Integer getNguoiTaoId() {
		return nguoiTaoId;
	}

	public void setNguoiTaoId(Integer nguoiTaoId) {
		this.nguoiTaoId = nguoiTaoId;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true, precision = 0)
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}

	@Basic
	@Column(name = "CT_CO_PHAN",nullable = true, precision = 0) // nullable = true,
	public Integer getCtCoPhan() {
		return ctCoPhan;
	}

	public void setCtCoPhan(Integer ctCoPhan) {
		this.ctCoPhan = ctCoPhan;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		DmLoaiCongTyEntity that = (DmLoaiCongTyEntity) o;
		return id == that.id && Objects.equals(maLoaiCongTy, that.maLoaiCongTy)
				&& Objects.equals(tenLoaiCongTy, that.tenLoaiCongTy) && Objects.equals(ghiChu, that.ghiChu)
				&& Objects.equals(nguoiTaoId, that.nguoiTaoId) && Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(trangThai, that.trangThai) && Objects.equals(ctCoPhan, that.ctCoPhan);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, maLoaiCongTy, tenLoaiCongTy, ghiChu, nguoiTaoId, ngayTao, trangThai, ctCoPhan);
	}
}
