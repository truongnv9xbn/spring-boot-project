package com.temp.persistance.entity.DanhMuc;

import java.sql.Time;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DM_QUOC_TICH", catalog = "")
public class DmQuocTichEntity {
	
	
    private int id;
    

    private String maQuocTich;
    
    
    private String tenQuocTich;
    
    private String ghiChu;
    
    private Integer nguoiTaoId;
    
    private Time ngayTao;
    private Boolean trangThai;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MA_QUOC_TICH", nullable = true, length = 20)
    public String getMaQuocTich() {
        return maQuocTich;
    }

    public void setMaQuocTich(String maQuocTich) {
        this.maQuocTich = maQuocTich;
    }

    @Basic
    @Column(name = "TEN_QUOC_TICH", nullable = true, length = 250)
    public String getTenQuocTich() {
        return tenQuocTich;
    }

    public void setTenQuocTich(String tenQuocTich) {
        this.tenQuocTich = tenQuocTich;
    }

    @Basic
    @Column(name = "GHI_CHU", nullable = true, length = 1000)
    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_TAO", nullable = true)
    public Time getNgayTao() {
        return ngayTao;
    }


    public void setNgayTao(Time ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Basic
    @Column(name = "TRANG_THAI", nullable = true, precision = 0)
    public Boolean getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DmQuocTichEntity that = (DmQuocTichEntity) o;
        return id == that.id &&
                Objects.equals(maQuocTich, that.maQuocTich) &&
                Objects.equals(tenQuocTich, that.tenQuocTich) &&
                Objects.equals(ghiChu, that.ghiChu) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao) &&
                Objects.equals(trangThai, that.trangThai);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, maQuocTich, tenQuocTich, ghiChu, nguoiTaoId, ngayTao, trangThai);
    }
}
