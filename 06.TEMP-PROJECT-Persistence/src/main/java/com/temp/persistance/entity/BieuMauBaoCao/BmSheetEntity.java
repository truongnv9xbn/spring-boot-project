/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author VuongTM
 */
@Entity
@Table(name = "BM_SHEET", catalog = "")
@XmlRootElement

public class BmSheetEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Size(max = 200)
	@Column(name = "MA_SHEET", length = 200)
	private String maSheet;

	@Size(max = 200)
	@Column(name = "MA_SHEET_LT", length = 200)
	private String maSheetLT;

	@Size(max = 1000)
	@Column(name = "TEN_SHEET", length = 1000)
	private String tenSheet;
	@Size(max = 4000)
	@Column(name = "TIEU_DE_CHINH", length = 4000)
	private String tieuDeChinh;
	@Size(max = 4000)
	@Column(name = "TIEU_DE_PHU", length = 4000)
	private String tieuDePhu;
	@Column(name = "IS_TRANG_BIA")
	private Boolean trangBia;
	@Size(max = 20)
	@Column(name = "KHO_GIAY", length = 20)
	private String khoGiay;
	@Size(max = 20)
	@Column(name = "CHIEU_GIAY", length = 20)
	private String chieuGiay;
	@Column(name = "THU_TU")
	private Long thuTu;
	@Size(max = 4000)
	@Column(name = "GHI_CHU", length = 4000)
	private String ghiChu;
	@Size(max = 4000)
	@Column(name = "NGUOI_KY", length = 4000)
	private String nguoiKy;
	@Size(max = 20)
	@Column(name = "KIEU_BAO_CAO", length = 20)
	private String kieuBaoCao;
	@Column(name = "SU_DUNG")
	private Boolean suDung;
	@Column(name = "TRANG_THAI")
	private Long trangThai;
	@Column(name = "NGUOI_TAO_ID")
	private Integer nguoiTaoId;
	@Column(name = "NGAY_TAO")
	private Timestamp ngayTao;
	@Column(name = "NGAY_CAP_NHAT")
	private Timestamp ngayCapNhat;
	@Size(max = 20)
	@Column(name = "VI_TRI_TIEU_DE_PHU", length = 20)
	private String viTriTieuDePhu;
	@Size(max = 20)
	@Column(name = "CAN_TIEU_DE_PHU", length = 20)
	private String canTieuDePhu;

	@Column(name = "BM_SHEET_ID")
	private Integer bmSheetId;
	
	@Size(max = 4000)
	@Column(name = "THONG_TIN_CONG_TY", length = 4000)
	private String thongTinCongTy;
	
	@Size(max = 4000)
	@Column(name = "QUOC_HIEU", length = 4000)
	private String quocHieu;

//	@OneToMany(mappedBy = "bmSheetByBmSheetId")
//	private Collection<BmSheetCotEntity> bmSheetCotCollection;
//	@OneToMany(mappedBy = "bmSheetByBmSheetId")
//	private Collection<BmSheetHangEntity> bmSheetHangCollection;

	@OneToMany(mappedBy = "bmSheetId")
	private Collection<BmTieuDeHangEntity> bmTieuDeHangCollection;

	@JoinColumn(name = "BM_BAO_CAO_ID", referencedColumnName = "ID")
	@ManyToOne
	private BmBaoCaoEntity bmBaoCaoByBmBaoCaoId;

	@OneToMany(mappedBy = "bmSheetId")
	private Collection<BmTieuDeHangCotEntity> bmTieuDeHangCotCollection;

//	@OneToMany(mappedBy = "bmSheetId")
//	private Collection<BmSheetCtEntity> bmSheetCtCollection;

//	public Collection<BmSheetCtEntity> getBmSheetCtCollection() {
//		return bmSheetCtCollection;
//	}
//
//	public void setBmSheetCtCollection(Collection<BmSheetCtEntity> bmSheetCtCollection) {
//		this.bmSheetCtCollection = bmSheetCtCollection;
//	}
//
	public Collection<BmTieuDeHangCotEntity> getBmTieuDeHangCotCollection() {
		return bmTieuDeHangCotCollection;
	}

	public void setBmTieuDeHangCotCollection(Collection<BmTieuDeHangCotEntity> bmTieuDeHangCotCollection) {
		this.bmTieuDeHangCotCollection = bmTieuDeHangCotCollection;
	}

	public Collection<BmTieuDeHangEntity> getBmTieuDeHangCollection() {
		return bmTieuDeHangCollection;
	}

	public void setBmTieuDeHangCollection(Collection<BmTieuDeHangEntity> bmTieuDeHangCollection) {
		this.bmTieuDeHangCollection = bmTieuDeHangCollection;
	}

	public BmSheetEntity() {
	}

	public BmSheetEntity(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMaSheet() {
		return maSheet;
	}

	public void setMaSheet(String maSheet) {
		this.maSheet = maSheet;
	}
	
	public String getMaSheetLT() {
		return maSheetLT;
	}

	public void setMaSheetLT(String maSheetLT) {
		this.maSheetLT = maSheetLT;
	}

	public String getTenSheet() {
		return tenSheet;
	}

	public void setTenSheet(String tenSheet) {
		this.tenSheet = tenSheet;
	}

	public String getTieuDeChinh() {
		return tieuDeChinh;
	}

	public void setTieuDeChinh(String tieuDeChinh) {
		this.tieuDeChinh = tieuDeChinh;
	}

	public String getTieuDePhu() {
		return tieuDePhu;
	}

	public void setTieuDePhu(String tieuDePhu) {
		this.tieuDePhu = tieuDePhu;
	}

	public Boolean getTrangBia() {
		return trangBia;
	}

	public void setTrangBia(Boolean trangBia) {
		this.trangBia = trangBia;
	}

	public String getKhoGiay() {
		return khoGiay;
	}

	public void setKhoGiay(String khoGiay) {
		this.khoGiay = khoGiay;
	}

	public String getChieuGiay() {
		return chieuGiay;
	}

	public void setChieuGiay(String chieuGiay) {
		this.chieuGiay = chieuGiay;
	}

	public Long getThuTu() {
		return thuTu;
	}

	public void setThuTu(Long thuTu) {
		this.thuTu = thuTu;
	}

	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	public String getNguoiKy() {
		return nguoiKy;
	}

	public void setNguoiKy(String nguoiKy) {
		this.nguoiKy = nguoiKy;
	}

	public String getKieuBaoCao() {
		return kieuBaoCao;
	}

	public void setKieuBaoCao(String kieuBaoCao) {
		this.kieuBaoCao = kieuBaoCao;
	}

	public Boolean getSuDung() {
		return suDung;
	}

	public void setSuDung(Boolean suDung) {
		this.suDung = suDung;
	}

	public Long getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Long trangThai) {
		this.trangThai = trangThai;
	}

	public Integer getNguoiTaoId() {
		return nguoiTaoId;
	}

	public void setNguoiTaoId(Integer nguoiTaoId) {
		this.nguoiTaoId = nguoiTaoId;
	}

	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public String getViTriTieuDePhu() {
		return viTriTieuDePhu;
	}

	public void setViTriTieuDePhu(String viTriTieuDePhu) {
		this.viTriTieuDePhu = viTriTieuDePhu;
	}

	public String getCanTieuDePhu() {
		return canTieuDePhu;
	}

	public void setCanTieuDePhu(String canTieuDePhu) {
		this.canTieuDePhu = canTieuDePhu;
	}
	
	

//	@XmlTransient
//	public Collection<BmSheetCotEntity> getBmSheetCotCollection() {
//		return bmSheetCotCollection;
//	}
//
//	public void setBmSheetCotCollection(Collection<BmSheetCotEntity> bmSheetCotCollection) {
//		this.bmSheetCotCollection = bmSheetCotCollection;
//	}
//
//	@XmlTransient
//	public Collection<BmSheetHangEntity> getBmSheetHangCollection() {
//		return bmSheetHangCollection;
//	}
//
//	public void setBmSheetHangCollection(Collection<BmSheetHangEntity> bmSheetHangCollection) {
//		this.bmSheetHangCollection = bmSheetHangCollection;
//	}

//    @XmlTransient
//    public Collection<BmSheetCtEntity> getBmSheetCtCollection() {
//        return bmSheetCtCollection;
//    }
//
//    public void setBmSheetCtCollection(Collection<BmSheetCtEntity> bmSheetCtCollection) {
//        this.bmSheetCtCollection = bmSheetCtCollection;
//    }

	public String getThongTinCongTy() {
		return thongTinCongTy;
	}

	public void setThongTinCongTy(String thongTinCongTy) {
		this.thongTinCongTy = thongTinCongTy;
	}

	public String getQuocHieu() {
		return quocHieu;
	}

	public void setQuocHieu(String quocHieu) {
		this.quocHieu = quocHieu;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	public BmBaoCaoEntity getBmBaoCaoByBmBaoCaoId() {
		return bmBaoCaoByBmBaoCaoId;
	}

	public void setBmBaoCaoByBmBaoCaoId(BmBaoCaoEntity bmBaoCaoByBmBaoCaoId) {
		this.bmBaoCaoByBmBaoCaoId = bmBaoCaoByBmBaoCaoId;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof BmSheetEntity)) {
			return false;
		}
		BmSheetEntity other = (BmSheetEntity) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "java_demo.folder.BmSheet[ id=" + id + " ]";
	}

}
