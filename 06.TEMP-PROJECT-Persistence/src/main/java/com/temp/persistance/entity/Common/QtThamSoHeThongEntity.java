package com.temp.persistance.entity.Common;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QT_THAM_SO_HE_THONG", catalog = "")
public class QtThamSoHeThongEntity {
	private int id;
	private String phanHe;
	private String thamSo;
	private String giaTri;
	private String ghiChu;
	private String nguoiCapNhat;
	private Timestamp ngayCapNhat;
	private Boolean trangThai;
//	private Integer nguoiCapNhatId;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "PHAN_HE", nullable = true, length = 50)
	public String getPhanHe() {
		return phanHe;
	}

	public void setPhanHe(String phanHe) {
		this.phanHe = phanHe;
	}

	@Basic
	@Column(name = "THAM_SO", nullable = true, length = 250)
	public String getThamSo() {
		return thamSo;
	}

	public void setThamSo(String thamSo) {
		this.thamSo = thamSo;
	}

	@Basic
	@Column(name = "GIA_TRI", nullable = true, length = 250)
	public String getGiaTri() {
		return giaTri;
	}

	public void setGiaTri(String giaTri) {
		this.giaTri = giaTri;
	}

	@Basic
	@Column(name = "GHI_CHU", nullable = true, length = 1000)
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT", nullable = true)
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true, precision = 0)
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}
	
	@Basic
	@Column(name = "NGUOI_CAP_NHAT", nullable = true, precision = 0)
	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}
	
//	@Basic
//	@Column(name = "NGUOI_CAP_NHAT_ID", nullable = true, precision = 0)
//	public Integer getNguoiCapNhatId() {
//		return nguoiCapNhatId;
//	}
//
//	public void setNguoiCapNhatId(Integer nguoiCapNhatId) {
//		this.nguoiCapNhatId = nguoiCapNhatId;
//	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		QtThamSoHeThongEntity that = (QtThamSoHeThongEntity) o;
		return id == that.id && Objects.equals(phanHe, that.phanHe) && Objects.equals(thamSo, that.thamSo)
				&& Objects.equals(giaTri, that.giaTri) && Objects.equals(ghiChu, that.ghiChu)
				&& Objects.equals(nguoiCapNhat, that.nguoiCapNhat)
				&& Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(trangThai, that.trangThai);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, phanHe, thamSo, ghiChu, giaTri, nguoiCapNhat, ngayCapNhat,
				trangThai);
	}
}
