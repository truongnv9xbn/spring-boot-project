package com.temp.persistance.entity.DanhMuc;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DM_QUAN_HUYEN", catalog = "")
public class DmQuanHuyenEntity {
    private int id;
    private Integer tinhThanhId;
    private String maQuanHuyen;
    private String tenQuanHuyen;
    private String ghiChu;
    private Integer nguoiTaoId;
    private Timestamp ngayTao;
    private Boolean trangThai;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "TINH_THANH_ID")
    public Integer getTinhThanhId() {
        return tinhThanhId;
    }

    public void setTinhThanhId(Integer tinhThanhId) {
        this.tinhThanhId = tinhThanhId;
    }

    @Basic
    @Column(name = "MA_QUAN_HUYEN", nullable = true, length = 50)
    public String getMaQuanHuyen() {
        return maQuanHuyen;
    }

    public void setMaQuanHuyen(String maQuanHuyen) {
        this.maQuanHuyen = maQuanHuyen;
    }

    @Basic
    @Column(name = "TEN_QUAN_HUYEN", nullable = true, length = 250)
    public String getTenQuanHuyen() {
        return tenQuanHuyen;
    }

    public void setTenQuanHuyen(String tenQuanHuyen) {
        this.tenQuanHuyen = tenQuanHuyen;
    }

    @Basic
    @Column(name = "GHI_CHU", nullable = true, length = 1000)
    public String getGhiChu() {
        return ghiChu;
    }

    public void setGhiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_TAO", nullable = true)
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Basic
    @Column(name = "TRANG_THAI", nullable = true, precision = 0)
    public Boolean getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Boolean trangThai) {
        this.trangThai = trangThai;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DmQuanHuyenEntity that = (DmQuanHuyenEntity) o;
        return id == that.id &&
                Objects.equals(tinhThanhId, that.tinhThanhId) &&
                Objects.equals(maQuanHuyen, that.maQuanHuyen) &&
                Objects.equals(tenQuanHuyen, that.tenQuanHuyen) &&
                Objects.equals(ghiChu, that.ghiChu) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao) &&
                Objects.equals(trangThai, that.trangThai);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tinhThanhId, maQuanHuyen, tenQuanHuyen, ghiChu, nguoiTaoId, ngayTao, trangThai);
    }
}
