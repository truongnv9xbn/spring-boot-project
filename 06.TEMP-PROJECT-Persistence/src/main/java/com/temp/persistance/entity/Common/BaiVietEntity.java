package com.temp.persistance.entity.Common;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BAI_VIET", catalog = "")
public class BaiVietEntity {
	private int id;
	private Integer chuyenMucId;
	private String tieuDe;
	private String moTaNgan;
	private String noiDung;
	private String ghiChu;
	private String anhDaiDien;
	private Integer noiBat;
	private Integer thuTuSapXep;
	private String trangThai;
	private Timestamp ngayTao;
	private String nguoiTao;
	private Timestamp ngayCapNhat;
	private String nguoiCapNhat;
	private Timestamp ngayDuyet;
	private String nguoiDuyet;
	private String lyDoTuChoi;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "CHUYEN_MUC_ID", nullable = true)
	public Integer getChuyenMucId() {
		return chuyenMucId;
	}

	public void setChuyenMucId(Integer chuyenMucId) {
		this.chuyenMucId = chuyenMucId;
	}

	@Basic
	@Column(name = "TIEU_DE", nullable = true)
	public String getTieuDe() {
		return tieuDe;
	}

	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}

	@Basic
	@Column(name = "MO_TA_NGAN", nullable = true)
	public String getMoTaNgan() {
		return moTaNgan;
	}

	public void setMoTaNgan(String moTaNgan) {
		this.moTaNgan = moTaNgan;
	}

	@Basic
	@Column(name = "NOI_DUNG", nullable = true)
	public String getNoiDung() {
		return noiDung;
	}

	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}

	@Basic
	@Column(name = "GHI_CHU", nullable = true)
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "ANH_DAI_DIEN", nullable = true)
	public String getAnhDaiDien() {
		return anhDaiDien;
	}

	public void setAnhDaiDien(String anhDaiDien) {
		this.anhDaiDien = anhDaiDien;
	}

	@Basic
	@Column(name = "NOI_BAT", nullable = true)
	public Integer getNoiBat() {
		return noiBat;
	}

	public void setNoiBat(Integer noiBat) {
		this.noiBat = noiBat;
	}

	@Basic
	@Column(name = "THU_TU_SAP_XEP", nullable = true)
	public Integer getThuTuSapXep() {
		return thuTuSapXep;
	}

	public void setThuTuSapXep(Integer thuTuSapXep) {
		this.thuTuSapXep = thuTuSapXep;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true)
	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGUOI_TAO", nullable = true)
	public String getNguoiTao() {
		return nguoiTao;
	}

	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT", nullable = true)
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "NGUOI_CAP_NHAT", nullable = true)
	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	@Basic
	@Column(name = "NGAY_DUYET", nullable = true)
	public Timestamp getNgayDuyet() {
		return ngayDuyet;
	}

	public void setNgayDuyet(Timestamp ngayDuyet) {
		this.ngayDuyet = ngayDuyet;
	}

	@Basic
	@Column(name = "NGUOI_DUYET", nullable = true)
	public String getNguoiDuyet() {
		return nguoiDuyet;
	}

	public void setNguoiDuyet(String nguoiDuyet) {
		this.nguoiDuyet = nguoiDuyet;
	}

	@Basic
	@Column(name = "LY_DO_TU_CHOI", nullable = true)
	public String getLyDoTuChoi() {
		return lyDoTuChoi;
	}

	public void setLyDoTuChoi(String lyDoTuChoi) {
		this.lyDoTuChoi = lyDoTuChoi;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		BaiVietEntity that = (BaiVietEntity) o;
		return id == that.id && Objects.equals(chuyenMucId, that.chuyenMucId) && Objects.equals(tieuDe, that.tieuDe)
				&& Objects.equals(moTaNgan, that.moTaNgan) && Objects.equals(noiDung, that.noiDung)
				&& Objects.equals(ghiChu, that.ghiChu) && Objects.equals(anhDaiDien, that.anhDaiDien)
				&& Objects.equals(noiBat, that.noiBat) && Objects.equals(thuTuSapXep, that.thuTuSapXep)
				&& Objects.equals(trangThai, that.trangThai) && Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(nguoiTao, that.nguoiTao) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(nguoiCapNhat, that.nguoiCapNhat) && Objects.equals(ngayDuyet, that.ngayDuyet)
				&& Objects.equals(nguoiDuyet, that.nguoiDuyet) && Objects.equals(lyDoTuChoi, that.lyDoTuChoi);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, chuyenMucId, tieuDe, moTaNgan, noiDung, ghiChu, anhDaiDien, noiBat, thuTuSapXep,
				trangThai, ngayTao, nguoiTao, ngayCapNhat, nguoiCapNhat, ngayDuyet, nguoiDuyet, lyDoTuChoi);
	}
}
