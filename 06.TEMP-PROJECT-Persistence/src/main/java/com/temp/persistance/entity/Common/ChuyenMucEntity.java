package com.temp.persistance.entity.Common;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CHUYEN_MUC", catalog = "")
public class ChuyenMucEntity {
	private int id;
	private Integer khoaChaId;
	private String tenChuyenMuc;
	private String moTa;
	private String noiDung;
	private String ghiChu;
	private Integer thuTuSapXep;
	private String icon;
	private String trangThai;
	private Timestamp ngayTao;
	private String nguoiTao;
	private Timestamp ngayCapNhat;
	private String nguoiCapNhat;
	private Timestamp ngayDuyet;
	private String nguoiDuyet;
	private String lyDoTuChoi;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "KHOA_CHA_ID", nullable = true)
	public Integer getKhoaChaId() {
		return khoaChaId;
	}

	public void setKhoaChaId(Integer khoaChaId) {
		this.khoaChaId = khoaChaId;
	}

	@Basic
	@Column(name = "TEN_CHUYEN_MUC", nullable = true, length = 250)
	public String getTenChuyenMuc() {
		return tenChuyenMuc;
	}

	public void setTenChuyenMuc(String tenChuyenMuc) {
		this.tenChuyenMuc = tenChuyenMuc;
	}

	@Basic
	@Column(name = "GHI_CHU", nullable = true, length = 1000)
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "NGUOI_TAO", nullable = true, precision = 0)
	public String getNguoiTao() {
		return nguoiTao;
	}

	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true, precision = 0)
	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	@Basic
	@Column(name = "MO_TA", nullable = true, precision = 0)
	public String getMoTa() {
		return moTa;
	}

	public void setMoTa(String moTa) {
		this.moTa = moTa;
	}

	@Basic
	@Column(name = "NOI_DUNG", nullable = true, precision = 0)
	public String getNoiDung() {
		return noiDung;
	}

	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}

	@Basic
	@Column(name = "THU_TU_SAP_XEP", nullable = true, precision = 0)
	public Integer getThuTuSapXep() {
		return thuTuSapXep;
	}

	public void setThuTuSapXep(Integer thuTuSapXep) {
		this.thuTuSapXep = thuTuSapXep;
	}

	@Basic
	@Column(name = "ICON", nullable = true, precision = 0)
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT", nullable = true, precision = 0)
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "NGUOI_CAP_NHAT", nullable = true, precision = 0)
	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	@Basic
	@Column(name = "NGAY_DUYET", nullable = true, precision = 0)
	public Timestamp getNgayDuyet() {
		return ngayDuyet;
	}

	public void setNgayDuyet(Timestamp ngayDuyet) {
		this.ngayDuyet = ngayDuyet;
	}

	@Basic
	@Column(name = "NGUOI_DUYET", nullable = true, precision = 0)
	public String getNguoiDuyet() {
		return nguoiDuyet;
	}

	public void setNguoiDuyet(String nguoiDuyet) {
		this.nguoiDuyet = nguoiDuyet;
	}

	@Basic
	@Column(name = "LY_DO_TU_CHOI", nullable = true, precision = 0)
	public String getLyDoTuChoi() {
		return lyDoTuChoi;
	}

	public void setLyDoTuChoi(String lyDoTuChoi) {
		this.lyDoTuChoi = lyDoTuChoi;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ChuyenMucEntity that = (ChuyenMucEntity) o;
		return id == that.id && Objects.equals(khoaChaId, that.khoaChaId)
				&& Objects.equals(tenChuyenMuc, that.tenChuyenMuc) && Objects.equals(moTa, that.moTa)
				&& Objects.equals(noiDung, that.noiDung) && Objects.equals(ghiChu, that.ghiChu)
				&& Objects.equals(thuTuSapXep, that.thuTuSapXep) && Objects.equals(icon, that.icon)
				&& Objects.equals(trangThai, that.trangThai) && Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(nguoiTao, that.nguoiTao) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(nguoiCapNhat, that.nguoiCapNhat) && Objects.equals(ngayDuyet, that.ngayDuyet)
				&& Objects.equals(nguoiDuyet, that.nguoiDuyet) && Objects.equals(lyDoTuChoi, that.lyDoTuChoi);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, khoaChaId, tenChuyenMuc, moTa, noiDung, ghiChu, thuTuSapXep, icon, trangThai, ngayTao,
				nguoiTao, ngayCapNhat, nguoiCapNhat, ngayDuyet, nguoiDuyet, lyDoTuChoi);
	}
}
