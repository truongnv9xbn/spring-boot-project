package com.temp.persistance.entity.BieuMauBaoCao;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BC_KHAI_THAC", catalog = "")
public class BcKhaiThacEntity {
    private Integer id;
    private Integer bmBaoCaoId;
    private Timestamp tuNgay;
    private Timestamp denNgay;
    private Timestamp ngayTongHop;
    private Integer nguoiDungId;
    private String ctck;
    private String kyBaoCao;
    private Integer donViTinh;
    private Boolean isKhoangTg;
    private Integer bcktId;

    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "BM_BAO_CAO_ID", nullable = true, precision = 0)
    public Integer getBmBaoCaoId() {
        return bmBaoCaoId;
    }

    public void setBmBaoCaoId(Integer bmBaoCaoId) {
        this.bmBaoCaoId = bmBaoCaoId;
    }

    @Basic
    @Column(name = "TU_NGAY")
    public Timestamp getTuNgay() {
        return tuNgay;
    }

    public void setTuNgay(Timestamp tuNgay) {
        this.tuNgay = tuNgay;
    }

    @Basic
    @Column(name = "DEN_NGAY")
    public Timestamp getDenNgay() {
        return denNgay;
    }

    public void setDenNgay(Timestamp denNgay) {
        this.denNgay = denNgay;
    }

    @Basic
    @Column(name = "NGAY_TONG_HOP")
    public Timestamp getNgayTongHop() {
        return ngayTongHop;
    }

    public void setNgayTongHop(Timestamp ngayTongHop) {
        this.ngayTongHop = ngayTongHop;
    }

    @Basic
    @Column(name = "CTCK", nullable = true, precision = 0)
    public String getCtck() {
        return ctck;
    }

    public void setCtck(String ctck) {
        this.ctck = ctck;
    }
    
    @Basic
    @Column(name = "KY_BAO_CAO", nullable = true, precision = 0)
    public String getKyBaoCao() {
        return kyBaoCao;
    }

    public void setKyBaoCao(String kyBaoCao) {
        this.kyBaoCao = kyBaoCao;
    }
    
    @Basic
    @Column(name = "NGUOI_DUNG_ID", nullable = true, precision = 0)
    public Integer getNguoiDungId() {
        return nguoiDungId;
    }

    public void setNguoiDungId(Integer nguoiDungId) {
        this.nguoiDungId = nguoiDungId;
    }
    
    @Basic
    @Column(name = "DON_VI_TINH", nullable = true, precision = 0)
    public Integer getDonViTinh() {
        return donViTinh;
    }

    public void setDonViTinh(Integer donViTinh) {
        this.donViTinh = donViTinh;
    }
    
    @Basic
    @Column(name = "IS_KHOANG_TG", nullable = true, precision = 0)
    public Boolean getIsKhoangTg() {
        return isKhoangTg;
    }

    public void setIsKhoangTg(Boolean isKhoangTg) {
        this.isKhoangTg = isKhoangTg;
    }
    
    @Basic
    @Column(name = "BCKT_ID", nullable = true, precision = 0)
    public Integer getBcktId() {
        return bcktId;
    }

    public void setBcktId(Integer bcktId) {
        this.bcktId = bcktId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BcKhaiThacEntity that = (BcKhaiThacEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(bmBaoCaoId, that.bmBaoCaoId) &&
                Objects.equals(tuNgay, that.tuNgay) &&
                Objects.equals(denNgay, that.denNgay) &&
                Objects.equals(ngayTongHop, that.ngayTongHop) &&
                Objects.equals(nguoiDungId, that.nguoiDungId) &&
                Objects.equals(ctck, that.ctck) &&
                Objects.equals(kyBaoCao, that.kyBaoCao) &&
        		Objects.equals(donViTinh, that.donViTinh) &&
        		Objects.equals(isKhoangTg, that.isKhoangTg)&&
        		Objects.equals(bcktId, that.bcktId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bmBaoCaoId, tuNgay, denNgay, ngayTongHop, nguoiDungId, ctck, kyBaoCao, donViTinh, isKhoangTg, bcktId);
    }
}
