/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author VuongTM
 */
@Entity
@Table(name = "BM_TIEU_DE_HANG_COT", catalog = "")
@XmlRootElement
 
public class BmTieuDeHangCotEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
  	@Column(name = "ID")
  	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Size(max = 200)
    @Column(name = "TEN_COT", length = 200)
    private String tenCot;
    @Size(max = 800)
    @Column(name = "TEN_COT_TIENG_ANH", length = 800)
    private String tenCotTiengAnh;
    @Column(name = "THU_TU")
    private Long thuTu;
    @Column(name = "FISRT_ROW")
    private Long fisrtRow;
    @Column(name = "LAST_ROW")
    private Long lastRow;
    @Column(name = "FISRT_COLUMN")
    private Long fisrtColumn;
    @Column(name = "LAST_COLUMN")
    private Long lastColumn;
    @Column(name = "ROW_SPAN")
    private Long rowSpan;
    @Column(name = "COL_SPAN")
    private Long colSpan;
    private Boolean hide;
    @Column(name = "PHIEN_BAN")
    private Long phienBan;
    
    @JoinColumn(name = "BM_SHEET_ID", referencedColumnName = "ID")
    @ManyToOne
    private BmSheetEntity bmSheetId;
    
    @JoinColumn(name = "BM_TIEU_DE_HANG_ID", referencedColumnName = "ID")
    @ManyToOne
    private BmTieuDeHangEntity bmTieuDeHangId;
    
    

    public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public BmTieuDeHangCotEntity() {
    }

    public BmTieuDeHangCotEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTenCot() {
        return tenCot;
    }

    public void setTenCot(String tenCot) {
        this.tenCot = tenCot;
    }

    public String getTenCotTiengAnh() {
        return tenCotTiengAnh;
    }

    public void setTenCotTiengAnh(String tenCotTiengAnh) {
        this.tenCotTiengAnh = tenCotTiengAnh;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public Long getFisrtRow() {
        return fisrtRow;
    }

    public void setFisrtRow(Long fisrtRow) {
        this.fisrtRow = fisrtRow;
    }

    public Long getLastRow() {
        return lastRow;
    }

    public void setLastRow(Long lastRow) {
        this.lastRow = lastRow;
    }

    public Long getFisrtColumn() {
        return fisrtColumn;
    }

    public void setFisrtColumn(Long fisrtColumn) {
        this.fisrtColumn = fisrtColumn;
    }

    public Long getLastColumn() {
        return lastColumn;
    }

    public void setLastColumn(Long lastColumn) {
        this.lastColumn = lastColumn;
    }

    public Long getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(Long rowSpan) {
        this.rowSpan = rowSpan;
    }

    public Long getColSpan() {
        return colSpan;
    }

    public void setColSpan(Long colSpan) {
        this.colSpan = colSpan;
    }

    public Boolean getHide() {
        return hide;
    }

    public void setHide(Boolean hide) {
        this.hide = hide;
    }

    public Long getPhienBan() {
        return phienBan;
    }

    public void setPhienBan(Long phienBan) {
        this.phienBan = phienBan;
    }

    public BmTieuDeHangEntity getBmTieuDeHangId() {
        return bmTieuDeHangId;
    }

   

	public BmSheetEntity getBmSheetId() {
		return bmSheetId;
	}

	public void setBmSheetId(BmSheetEntity bmSheetId) {
		this.bmSheetId = bmSheetId;
	}

	public void setBmTieuDeHangId(BmTieuDeHangEntity bmTieuDeHangId) {
        this.bmTieuDeHangId = bmTieuDeHangId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmTieuDeHangCotEntity)) {
            return false;
        }
        BmTieuDeHangCotEntity other = (BmTieuDeHangCotEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "java_demo.folder.BmTieuDeHangCot[ id=" + id + " ]";
    }
    
}
