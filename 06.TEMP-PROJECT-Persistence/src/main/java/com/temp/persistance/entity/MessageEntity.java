package com.temp.persistance.entity;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MESSAGE", catalog = "")
public class MessageEntity {
	private int id;
	private Integer nguoiGuiId;
	private Integer nguoiNhanId;
	private String message;
	private String sendImage;
	private Timestamp ngayTao;
	private Timestamp ngayCapNhat;
	private Boolean trangThai;
	private String taiKhoan;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "MESSAGE", nullable = true, length = 1000)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Basic
	@Column(name = "SEND_IMAGE", nullable = true, length = 1000)
	public String getSendImage() {
		return sendImage;
	}

	public void setSendImage(String sendImage) {
		this.sendImage = sendImage;
	}

	@Basic
	@Column(name = "TAI_KHOAN", nullable = true, length = 1000)
	public String getTaiKhoan() {
		return taiKhoan;
	}

	public void setTaiKhoan(String taiKhoan) {
		this.taiKhoan = taiKhoan;
	}

	@Basic
	@Column(name = "NGUOI_GUI_ID", nullable = true, precision = 0)
	public Integer getNguoiGuiId() {
		return nguoiGuiId;
	}

	public void setNguoiGuiId(Integer nguoiGuiId) {
		this.nguoiGuiId = nguoiGuiId;
	}

	@Basic
	@Column(name = "NGUOI_NHAN_ID", nullable = true, precision = 0)
	public Integer getNguoiNhanId() {
		return nguoiNhanId;
	}

	public void setNguoiNhanId(Integer nguoiNhanId) {
		this.nguoiNhanId = nguoiNhanId;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT", nullable = true)
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true, precision = 0)
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		MessageEntity that = (MessageEntity) o;
		return id == that.id && Objects.equals(message, that.message) && Objects.equals(sendImage, that.sendImage)
				&& Objects.equals(nguoiGuiId, that.nguoiGuiId) && Objects.equals(nguoiNhanId, that.nguoiNhanId)
				&& Objects.equals(ngayTao, that.ngayTao) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(trangThai, that.trangThai);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, message, sendImage, nguoiGuiId, nguoiNhanId, ngayTao, ngayCapNhat, trangThai);
	}
}
