
package com.temp.persistance.entity.NguoiDung;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "QT_NGUOI_DUNG", catalog = "")
public class QtNguoiDungEntity {

	private Integer id;
	private String maNguoiDung;
	private String taiKhoan;
	private String matKhau;
	private String matKhauDefault;
	private String hoTen;
	private String email;
	private String diDong;
	private String ghiChu;
	private String nguoiTao;
	private Timestamp ngayTao;
	private Timestamp ngayHetHan;
	private Boolean trangThai;
	private String tokenUser;
	private String anhDaiDien;
	private String quyenJson;
	private Boolean admin;
	private String nguoiCapNhat;
	private Timestamp ngayCapNhat;
	private Timestamp lastChangePass;
//	private Boolean chuKySo;
//	private Boolean isThanhVien;
//	private Integer ctckId;
//	private Integer nguoiTaoId;
//	private Integer dmChucVuId;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Basic
	@Column(name = "MA_NGUOI_DUNG")
	public String getMaNguoiDung() {
		return maNguoiDung;
	}

	public void setTokenUser(String tokenUser) {
		this.tokenUser = tokenUser;
	}

	@Basic
	@Column(name = "ANH_DAI_DIEN")
	public String getAnhDaiDien() {
		return anhDaiDien;
	}

	public void setAnhDaiDien(String anhDaiDien) {
		this.anhDaiDien = anhDaiDien;
	}

	@Basic
	@Column(name = "TOKEN_USER")
	public String getTokenUser() {
		return this.tokenUser;
	}

	public void setMaNguoiDung(String maNguoiDung) {
		this.maNguoiDung = maNguoiDung;
	}

	@Basic
	@Column(name = "TAI_KHOAN")
	public String getTaiKhoan() {
		return taiKhoan;
	}

	public void setTaiKhoan(String taiKhoan) {
		this.taiKhoan = taiKhoan;
	}

	@Basic
	@Column(name = "MAT_KHAU")
	public String getMatKhau() {
		return matKhau;
	}

	public void setMatKhau(String matKhau) {
		this.matKhau = matKhau;
	}

	@Basic
	@Column(name = "MAT_KHAU_DEFAULT")
	public String getMatKhauDefault() {
		return matKhauDefault;
	}

	public void setMatKhauDefault(String matKhauDefault) {
		this.matKhauDefault = matKhauDefault;
	}

	@Basic
	@Column(name = "HO_TEN")
	public String getHoTen() {
		return hoTen;
	}

	public void setHoTen(String hoTen) {
		this.hoTen = hoTen;
	}

	@Basic
	@Column(name = "EMAIL")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Basic
	@Column(name = "DI_DONG")
	public String getDiDong() {
		return diDong;
	}

	public void setDiDong(String diDong) {
		this.diDong = diDong;
	}

	@Basic
	@Column(name = "GHI_CHU")
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "NGAY_TAO")
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGAY_HET_HAN")
	public Timestamp getNgayHetHan() {
		return ngayHetHan;
	}

	public void setNgayHetHan(Timestamp ngayHetHan) {
		this.ngayHetHan = ngayHetHan;
	}

	@Basic
	@Column(name = "TRANG_THAI")
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}

	@Column(name = "QUYEN_JSON")
	@Lob
	public String getQuyenJson() {
		return this.quyenJson;
	};

	public void setQuyenJson(String quyenJson) {
		this.quyenJson = quyenJson;
	};

	@Basic
	@Column(name = "IS_ADMIN")
	public Boolean getAdmin() {
		return admin;
	}

	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}
	
	/**
	 * @return the nguoiTao
	 */
	@Basic
	@Column(name = "NGUOI_TAO")
	public String getNguoiTao() {
		return nguoiTao;
	}

	/**
	 * @param nguoiTao the nguoiTao to set
	 */
	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	/**
	 * @return the nguoiCapNhat
	 */
	@Basic
	@Column(name = "NGUOI_CAP_NHAT")
	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	/**
	 * @param nguoiCapNhat the nguoiCapNhat to set
	 */
	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	/**
	 * @return the ngayCapNhat
	 */
	@Basic
	@Column(name = "NGAY_CAP_NHAT")
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	/**
	 * @param ngayCapNhat the ngayCapNhat to set
	 */
	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	/**
	 *author TruongNV 
	 *date Mar 26, 2022
	 * @return the lastChangePass
	 */
	@Basic
	@Column(name = "LAST_CHANGE_PASS")
	public Timestamp getLastChangePass() {
		return lastChangePass;
	}

	/**
	 *author TruongNV 
	 *date Mar 26, 2022
	 * @param lastChangePass the lastChangePass to set
	 */
	public void setLastChangePass(Timestamp lastChangePass) {
		this.lastChangePass = lastChangePass;
	}
	
	// bỏ
//	@Basic
//	@Column(name = "DM_CHUC_VU_ID")
//	public Integer getDmChucVuId() {
//		return dmChucVuId;
//	}
//
//	public void setDmChucVuId(Integer dmChucVuId) {
//		this.dmChucVuId = dmChucVuId;
//	}

//	@Basic
//	@Column(name = "CHU_KY_SO")
//	public Boolean getChuKySo() {
//		return chuKySo;
//	}
//
//	public void setChuKySo(Boolean chuKySo) {
//		this.chuKySo = chuKySo;
//	}
//
//	@Basic
//	@Column(name = "IS_THANH_VIEN")
//	public Boolean getThanhVien() {
//		return isThanhVien;
//	}
//
//	public void setThanhVien(Boolean thanhVien) {
//		isThanhVien = thanhVien;
//	}
//
//	@Basic
//	@Column(name = "CTCK_ID")
//	public Integer getCtckId() {
//		return ctckId;
//	}
//
//	public void setCtckId(Integer ctckId) {
//		this.ctckId = ctckId;
//	}

//	@Basic
//	@Column(name = "NGUOI_TAO_ID")
//	public Integer getNguoiTaoId() {
//		return nguoiTaoId;
//	}
//
//	public void setNguoiTaoId(Integer nguoiTaoId) {
//		this.nguoiTaoId = nguoiTaoId;
//	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		QtNguoiDungEntity that = (QtNguoiDungEntity) o;
		return Objects.equals(id, that.id) && Objects.equals(maNguoiDung, that.maNguoiDung)
				&& Objects.equals(taiKhoan, that.taiKhoan) && Objects.equals(matKhau, that.matKhau)
				&& Objects.equals(matKhauDefault, that.matKhauDefault) && Objects.equals(hoTen, that.hoTen)
				&& Objects.equals(email, that.email) && Objects.equals(diDong, that.diDong)
				&& Objects.equals(ghiChu, that.ghiChu) && Objects.equals(nguoiTao, that.nguoiTao)
				&& Objects.equals(ngayTao, that.ngayTao) && Objects.equals(ngayHetHan, that.ngayHetHan)
				&& Objects.equals(trangThai, that.trangThai) && Objects.equals(admin, that.admin);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, maNguoiDung, taiKhoan, matKhau, matKhauDefault, hoTen, email, diDong, ghiChu, nguoiTao,
				ngayTao, ngayHetHan, trangThai, admin);
	}
}
