package com.temp.persistance.entity.Common;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VAN_BAN_TAI_LIEU", catalog = "")
public class TaiLieuEntity {
	private int id;
	private String soVanBan;
	private String trichYeuNoiDung;
	private String fileDinhKem;
	private Timestamp ngayBanHanh;
	private Timestamp ngayHieuLuc;
	private Timestamp ngayTao;
	private String nguoiTao;
	private Timestamp ngayCapNhat;
	private String nguoiCapNhat;
	private Timestamp ngayDuyet;
	private String nguoiDuyet;
	private String trangThai;
	private String lyDoTuChoi;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "SO_VAN_BAN", nullable = true)
	public String getSoVanBan() {
		return soVanBan;
	}

	public void setSoVanBan(String soVanBan) {
		this.soVanBan = soVanBan;
	}

	@Basic
	@Column(name = "TRICH_YEU_NOI_DUNG", nullable = true)
	public String getTrichYeuNoiDung() {
		return trichYeuNoiDung;
	}

	public void setTrichYeuNoiDung(String trichYeuNoiDung) {
		this.trichYeuNoiDung = trichYeuNoiDung;
	}

	@Basic
	@Column(name = "FILE_DINH_KEM", nullable = true)
	public String getFileDinhKem() {
		return fileDinhKem;
	}

	public void setFileDinhKem(String fileDinhKem) {
		this.fileDinhKem = fileDinhKem;
	}

	@Basic
	@Column(name = "NGAY_BAN_HANH", nullable = true)
	public Timestamp getNgayBanHanh() {
		return ngayBanHanh;
	}

	public void setNgayBanHanh(Timestamp ngayBanHanh) {
		this.ngayBanHanh = ngayBanHanh;
	}

	@Basic
	@Column(name = "NGAY_HIEU_LUC", nullable = true)
	public Timestamp getNgayHieuLuc() {
		return ngayHieuLuc;
	}

	public void setNgayHieuLuc(Timestamp ngayHieuLuc) {
		this.ngayHieuLuc = ngayHieuLuc;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGUOI_TAO", nullable = true)
	public String getNguoiTao() {
		return nguoiTao;
	}

	public void setNguoiTao(String nguoiTao) {
		this.nguoiTao = nguoiTao;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT", nullable = true)
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "NGUOI_CAP_NHAT", nullable = true)
	public String getNguoiCapNhat() {
		return nguoiCapNhat;
	}

	public void setNguoiCapNhat(String nguoiCapNhat) {
		this.nguoiCapNhat = nguoiCapNhat;
	}

	@Basic
	@Column(name = "NGAY_DUYET", nullable = true)
	public Timestamp getNgayDuyet() {
		return ngayDuyet;
	}

	public void setNgayDuyet(Timestamp ngayDuyet) {
		this.ngayDuyet = ngayDuyet;
	}

	@Basic
	@Column(name = "NGUOI_DUYET", nullable = true)
	public String getNguoiDuyet() {
		return nguoiDuyet;
	}

	public void setNguoiDuyet(String nguoiDuyet) {
		this.nguoiDuyet = nguoiDuyet;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true)
	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	@Basic
	@Column(name = "LY_DO_TU_CHOI", nullable = true)
	public String getLyDoTuChoi() {
		return lyDoTuChoi;
	}

	public void setLyDoTuChoi(String lyDoTuChoi) {
		this.lyDoTuChoi = lyDoTuChoi;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		TaiLieuEntity that = (TaiLieuEntity) o;
		return id == that.id && Objects.equals(soVanBan, that.soVanBan)
				&& Objects.equals(trichYeuNoiDung, that.trichYeuNoiDung)
				&& Objects.equals(fileDinhKem, that.fileDinhKem) && Objects.equals(ngayBanHanh, that.ngayBanHanh)
				&& Objects.equals(ngayHieuLuc, that.ngayHieuLuc) && Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(nguoiTao, that.nguoiTao) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(nguoiCapNhat, that.nguoiCapNhat) && Objects.equals(ngayDuyet, that.ngayDuyet)
				&& Objects.equals(nguoiDuyet, that.nguoiDuyet) && Objects.equals(trangThai, that.trangThai)
				&& Objects.equals(lyDoTuChoi, that.lyDoTuChoi);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, soVanBan, trichYeuNoiDung, fileDinhKem, ngayBanHanh, ngayHieuLuc, ngayTao, nguoiTao,
				ngayCapNhat, nguoiCapNhat, ngayDuyet, nguoiDuyet, trangThai, lyDoTuChoi);
	}
}
