package com.temp.persistance.entity.NguoiDung;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.temp.persistance.embeddable.LkChucNangNhomNguoiEmbeddable;

@Entity
@Table(name = "LK_CHUC_NANG_NHOM_NGUOI", catalog = "")
@IdClass(LkChucNangNhomNguoiEmbeddable.class)
public class LkChucNangNhomNguoiEntity {
	private Integer chucNangId;
	private Integer nhomNguoiDungId;
	private String chucNangChiTiet;

	@Id
	@Basic
	@Column(name = "CHUC_NANG_ID")
	public Integer getChucNangId() {
		return chucNangId;
	}

	public void setChucNangId(Integer chucNangId) {
		this.chucNangId = chucNangId;
	}


	@Basic
	@Column(name = "NHOM_NGUOI_DUNG_ID")
	public Integer getNhomNguoiDungId() {
		return nhomNguoiDungId;
	}

	public void setNhomNguoiDungId(Integer nhomNguoiDungId) {
		this.nhomNguoiDungId = nhomNguoiDungId;
	}

	
	@Basic
	@Column(name = "CHUC_NANG_CHI_TIET")
	public String getChucNangChiTiet() {
		return chucNangChiTiet;
	}

	public void setChucNangChiTiet(String chucNangChiTiet) {
		this.chucNangChiTiet = chucNangChiTiet;
	}
	
	

	public LkChucNangNhomNguoiEntity(Integer chucNangId, Integer nhomNguoiDungId) {
		this.chucNangId = chucNangId;
		this.nhomNguoiDungId = nhomNguoiDungId;
	}
	
	

	public LkChucNangNhomNguoiEntity() {
		super();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		LkChucNangNhomNguoiEntity that = (LkChucNangNhomNguoiEntity) o;
		return Objects.equals(chucNangId, that.chucNangId) && Objects.equals(nhomNguoiDungId, that.nhomNguoiDungId)
				&& Objects.equals(chucNangChiTiet, that.chucNangChiTiet);
	}

	@Override
	public int hashCode() {
		return Objects.hash(chucNangId, nhomNguoiDungId, chucNangChiTiet);
	}
}
