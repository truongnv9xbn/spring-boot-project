package com.temp.persistance.entity;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.temp.persistance.embeddable.LkNguoiDungNhomEmbeddable;

@Entity
@Table(name = "LK_NGUOI_DUNG_NHOM", catalog = "")
@IdClass(LkNguoiDungNhomEmbeddable.class)
public class LkNguoiDungNhomEntity {

	private Integer nguoiDungId;
	private Integer nhomNguoiDungId;

	@Id
	@Basic
	@Column(name = "NGUOI_DUNG_ID")
	public Integer getNguoiDungId() {
		return nguoiDungId;
	}

	public void setNguoiDungId(Integer nguoiDungId) {
		this.nguoiDungId = nguoiDungId;
	}

	@Id
	@Basic
	@Column(name = "NHOM_NGUOI_DUNG_ID")
	public Integer getNhomNguoiDungId() {
		return nhomNguoiDungId;
	}

	public void setNhomNguoiDungId(Integer nhomNguoiDungId) {
		this.nhomNguoiDungId = nhomNguoiDungId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		LkNguoiDungNhomEntity that = (LkNguoiDungNhomEntity) o;
		return Objects.equals(nguoiDungId, that.nguoiDungId) && Objects.equals(nhomNguoiDungId, that.nhomNguoiDungId);
	}

	@Override
	public int hashCode() {
		return Objects.hash(nguoiDungId, nhomNguoiDungId);
	}
}
