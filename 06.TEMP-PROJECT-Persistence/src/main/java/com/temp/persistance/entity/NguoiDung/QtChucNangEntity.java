package com.temp.persistance.entity.NguoiDung;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "QT_CHUC_NANG", catalog = "")
public class QtChucNangEntity {
	private int id;
	private Integer khoaChaId;
	private String maChucNang;
	private String tenChucNang;
	private Integer actionId;
	private String icon;
	private Boolean menu;
	private Boolean chiTiet;
	private String phanHe;
	private Integer thuTu;
	private String ghiChu;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Integer nguoiCapNhatId;
	private Timestamp ngayCapNhat;
	private Boolean deleted;
	private Boolean trangThai;
	private Boolean disabled;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "KHOA_CHA_ID")
	public Integer getKhoaChaId() {
		return khoaChaId;
	}

	public void setKhoaChaId(Integer khoaChaId) {
		this.khoaChaId = khoaChaId;
	}

	@Basic
	@Column(name = "MA_CHUC_NANG")
	public String getMaChucNang() {
		return maChucNang;
	}

	public void setMaChucNang(String maChucNang) {
		this.maChucNang = maChucNang;
	}

	@Basic
	@Column(name = "TEN_CHUC_NANG")
	public String getTenChucNang() {
		return tenChucNang;
	}

	public void setTenChucNang(String tenChucNang) {
		this.tenChucNang = tenChucNang;
	}

	@Basic
	@Column(name = "ACTION_ID")
	public Integer getActionId() {
		return actionId;
	}

	public void setActionId(Integer actionId) {
		this.actionId = actionId;
	}

	@Basic
	@Column(name = "ICON")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	@Basic
	@Column(name = "IS_MENU")
	public Boolean getMenu() {
		return menu;
	}

	public void setMenu(Boolean menu) {
		this.menu = menu;
	}

	@Basic
	@Column(name = "IS_CHI_TIET")
	public Boolean getChiTiet() {
		return chiTiet;
	}

	public void setChiTiet(Boolean chiTiet) {
		this.chiTiet = chiTiet;
	}

	@Basic
	@Column(name = "PHAN_HE")
	public String getPhanHe() {
		return phanHe;
	}

	public void setPhanHe(String phanHe) {
		this.phanHe = phanHe;
	}

	@Basic
	@Column(name = "THU_TU")
	public Integer getThuTu() {
		return thuTu;
	}

	public void setThuTu(Integer thuTu) {
		this.thuTu = thuTu;
	}

	@Basic
	@Column(name = "GHI_CHU")
	public String getGhiChu() {
		return ghiChu;
	}

	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	@Basic
	@Column(name = "NGUOI_TAO_ID")
	public Integer getNguoiTaoId() {
		return nguoiTaoId;
	}

	public void setNguoiTaoId(Integer nguoiTaoId) {
		this.nguoiTaoId = nguoiTaoId;
	}

	@Basic
	@Column(name = "NGAY_TAO")
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGUOI_CAP_NHAT_ID")
	public Integer getNguoiCapNhatId() {
		return nguoiCapNhatId;
	}

	public void setNguoiCapNhatId(Integer nguoiCapNhatId) {
		this.nguoiCapNhatId = nguoiCapNhatId;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT")
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "IS_DELETED")
	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	@Basic
	@Column(name = "TRANG_THAI")
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}
	
 
	@Basic
	@Column(name = "DISABLED")
	public Boolean getDisabled() {
		return disabled;
	}

	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		QtChucNangEntity that = (QtChucNangEntity) o;
		return id == that.id && Objects.equals(khoaChaId, that.khoaChaId) && Objects.equals(maChucNang, that.maChucNang)
				&& Objects.equals(tenChucNang, that.tenChucNang) && Objects.equals(actionId, that.actionId)
				&& Objects.equals(icon, that.icon) && Objects.equals(menu, that.menu)
				&& Objects.equals(chiTiet, that.chiTiet) && Objects.equals(phanHe, that.phanHe)
				&& Objects.equals(thuTu, that.thuTu) && Objects.equals(ghiChu, that.ghiChu)
				&& Objects.equals(nguoiTaoId, that.nguoiTaoId) && Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(nguoiCapNhatId, that.nguoiCapNhatId) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(deleted, that.deleted) && Objects.equals(trangThai, that.trangThai);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, khoaChaId, maChucNang, tenChucNang, actionId, icon, menu, chiTiet, phanHe, thuTu,
				ghiChu, nguoiTaoId, ngayTao, nguoiCapNhatId, ngayCapNhat, deleted, trangThai);
	}
}
