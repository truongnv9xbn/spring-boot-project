package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * The persistent class for the BC_BAO_CAO_GT_DONG database table.
 */
@Entity
@Table(name = "BC_BAO_CAO_GT_DONG")
public class BcBaoCaoGtDongEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "BC_THANH_VIEN_ID")
    private Integer bcThanhVienId;

    @Column(name = "BM_BAO_CAO_ID")
    private Integer bmBaoCaoId;

    @Column(name = "BM_SHEET_HANG")
    private Integer bmSheetHang;

    @Column(name = "CTCK_THONG_TIN_ID")
    private Integer ctckThongTinId;

    @Lob
    @Column(name = "GIA_TRI")
    private String giaTri;

    public BcBaoCaoGtDongEntity() {
    }

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public Integer getBcThanhVienId() {
	return this.bcThanhVienId;
    }

    public void setBcThanhVienId(Integer bcThanhVienId) {
	this.bcThanhVienId = bcThanhVienId;
    }

    public Integer getBmBaoCaoId() {
	return this.bmBaoCaoId;
    }

    public void setBmBaoCaoId(Integer bmBaoCaoId) {
	this.bmBaoCaoId = bmBaoCaoId;
    }

    public Integer getBmSheetHang() {
	return this.bmSheetHang;
    }

    public void setBmSheetHang(Integer bmSheetHang) {
	this.bmSheetHang = bmSheetHang;
    }

    public Integer getCtckThongTinId() {
	return this.ctckThongTinId;
    }

    public void setCtckThongTinId(Integer ctckThongTinId) {
	this.ctckThongTinId = ctckThongTinId;
    }

    public String getGiaTri() {
	return this.giaTri;
    }

    public void setGiaTri(String giaTri) {
	this.giaTri = giaTri;
    }

}