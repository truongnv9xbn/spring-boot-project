/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author VuongTM
 */
@Entity
@Table(name = "BM_TIEU_DE_HANG", catalog = "")
@XmlRootElement
 
public class BmTieuDeHangEntity implements Serializable {

    private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "THU_TU")
    private Long thuTu;
    @Column(name = "PHIEN_BAN")
    private Long phienBan;
    
    @OneToMany(mappedBy = "bmTieuDeHangId")
    private Collection<BmTieuDeHangCotEntity> bmTieuDeHangCotCollection;
    
    @JoinColumn(name = "BM_SHEET_ID", referencedColumnName = "ID")
    @ManyToOne
    private BmSheetEntity bmSheetId;

    public BmTieuDeHangEntity() {
    }
    
    
    public Collection<BmTieuDeHangCotEntity> getBmTieuDeHangCotCollection() {
		return bmTieuDeHangCotCollection;
	}


	public void setBmTieuDeHangCotCollection(Collection<BmTieuDeHangCotEntity> bmTieuDeHangCotCollection) {
		this.bmTieuDeHangCotCollection = bmTieuDeHangCotCollection;
	}


	public BmTieuDeHangEntity(BmSheetEntity bmSheetId) {
		super();
		this.bmSheetId = bmSheetId;
	}

	public BmTieuDeHangEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public Long getPhienBan() {
        return phienBan;
    }

    public void setPhienBan(Long phienBan) {
        this.phienBan = phienBan;
    }

//    @XmlTransient
//    public Collection<BmTieuDeHangCotEntity> getBmTieuDeHangCotCollection() {
//        return bmTieuDeHangCotCollection;
//    }
//
//    public void setBmTieuDeHangCotCollection(Collection<BmTieuDeHangCotEntity> bmTieuDeHangCotCollection) {
//        this.bmTieuDeHangCotCollection = bmTieuDeHangCotCollection;
//    }

    public BmSheetEntity getBmSheetId() {
        return bmSheetId;
    }

    public void setBmSheetId(BmSheetEntity bmSheetId) {
        this.bmSheetId = bmSheetId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmTieuDeHangEntity)) {
            return false;
        }
        BmTieuDeHangEntity other = (BmTieuDeHangEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "java_demo.folder.BmTieuDeHang[ id=" + id + " ]";
    }
    
}
