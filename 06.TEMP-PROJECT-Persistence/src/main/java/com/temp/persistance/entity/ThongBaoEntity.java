package com.temp.persistance.entity;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "THONG_BAO", catalog = "")
public class ThongBaoEntity {
	private Integer id;
	private String tieuDe;
	private String noiDung;
	private String linkHref;
	private String listNguoiNhan;
	private String listNguoiDaXem;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Integer trangThai;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Basic
	@Column(name = "TIEU_DE", nullable = true, length = 500)
	public String getTieuDe() {
		return tieuDe;
	}

	public void setTieuDe(String tieuDe) {
		this.tieuDe = tieuDe;
	}

	@Basic
	@Column(name = "NOI_DUNG", nullable = true, length = 4000)
	public String getNoiDung() {
		return noiDung;
	}

	public void setNoiDung(String noiDung) {
		this.noiDung = noiDung;
	}

	@Basic
	@Column(name = "LINK_HREF", nullable = true, length = 1000)
	public String getlinkHref() {
		return linkHref;
	}

	public void setlinkHref(String linkHref) {
		this.linkHref = linkHref;
	}

	@Basic
	@Column(name = "LIST_NGUOI_NHAN", nullable = true, length = 4000)
	public String getListNguoiNhan() {
		return listNguoiNhan;
	}

	public void setListNguoiNhan(String listNguoiNhan) {
		this.listNguoiNhan = listNguoiNhan;
	}
	
	@Basic
	@Column(name = "LIST_NGUOI_DA_XEM", nullable = true, length = 4000)
	public String getListNguoiDaXem() {
		return listNguoiDaXem;
	}

	public void setListNguoiDaXem(String listNguoiDaXem) {
		this.listNguoiDaXem = listNguoiDaXem;
	}

	@Basic
	@Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
	public Integer getNguoiTaoId() {
		return nguoiTaoId;
	}

	public void setNguoiTaoId(Integer nguoiTaoId) {
		this.nguoiTaoId = nguoiTaoId;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true, precision = 0)
	public Integer getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Integer trangThai) {
		this.trangThai = trangThai;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		ThongBaoEntity that = (ThongBaoEntity) o;
		return id == that.id && Objects.equals(tieuDe, that.tieuDe) && Objects.equals(noiDung, that.noiDung)
				&& Objects.equals(linkHref, that.linkHref) && Objects.equals(listNguoiNhan, that.listNguoiNhan)
				&& Objects.equals(listNguoiDaXem, that.listNguoiDaXem)
				&& Objects.equals(nguoiTaoId, that.nguoiTaoId)
				&& Objects.equals(ngayTao, that.ngayTao)
				&& Objects.equals(trangThai, that.trangThai);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, tieuDe, noiDung, linkHref, listNguoiNhan, listNguoiDaXem, nguoiTaoId, ngayTao,
				trangThai);
	}
}
