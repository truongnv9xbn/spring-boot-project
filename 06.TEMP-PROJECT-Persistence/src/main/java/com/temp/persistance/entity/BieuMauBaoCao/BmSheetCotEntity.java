/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author VuongTM
 */
@Entity
@Table(name = "BM_SHEET_COT", catalog = "")
@XmlRootElement
 
public class BmSheetCotEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Size(max = 200)
    @Column(name = "MA_COT", length = 200)
    private String maCot;
    
    @Size(max = 200)
    @Column(name = "MA_COT_LT", length = 200)
    private String maCotLT;
    
    @Size(max = 1000)
    @Column(name = "TEN_COT", length = 1000)
    private String tenCot;
    @Column(name = "THU_TU")
    private Long thuTu;
    @Size(max = 20)
    @Column(name = "DINH_DANG_TEN", length = 20)
    private String dinhDangTen;
    @Size(max = 20)
    @Column(name = "MAU_SAC", length = 20)
    private String mauSac;
    @Size(max = 20)
    @Column(name = "CAN_LE", length = 20)
    private String canLe;
    @Column(name = "COT_DONG")
    private Boolean cotDong;
    @Column(name = "KHOA_COT")
    private Boolean khoaCot;
    @Size(max = 4000)
    @Column(name = "MO_TA", length = 4000)
    private String moTa;
    @Column(name = "NGAY_CAP_NHAT")
    private Timestamp ngayCapNhat;
    @Column(name = "SU_DUNG")
    private Boolean suDung;
    @Column(name = "PHIEN_BAN")
    private Long phienBan;
    @JoinColumn(name = "BM_BAO_CAO_ID", referencedColumnName = "ID")
    @ManyToOne
    private BmBaoCaoEntity bmBaoCaoByBmBaoCaoId;
    @JoinColumn(name = "BM_SHEET_ID", referencedColumnName = "ID")
    @ManyToOne
    private BmSheetEntity bmSheetByBmSheetId;
    
//    @OneToMany(mappedBy = "BmSheetCotEntityId")
//    private Collection<BmSheetCtEntity> bmSheetCtCollection;

    public BmSheetCotEntity() {
    }

    public BmSheetCotEntity(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMaCot() {
        return maCot;
    }

    public void setMaCot(String maCot) {
        this.maCot = maCot;
    }

    public String getMaCotLT() {
		return maCotLT;
	}

	public void setMaCotLT(String maCotLT) {
		this.maCotLT = maCotLT;
	}

	public String getTenCot() {
        return tenCot;
    }

    public void setTenCot(String tenCot) {
        this.tenCot = tenCot;
    }

    public Long getThuTu() {
        return thuTu;
    }

    public void setThuTu(Long thuTu) {
        this.thuTu = thuTu;
    }

    public String getDinhDangTen() {
        return dinhDangTen;
    }

    public void setDinhDangTen(String dinhDangTen) {
        this.dinhDangTen = dinhDangTen;
    }

    public String getMauSac() {
        return mauSac;
    }

    public void setMauSac(String mauSac) {
        this.mauSac = mauSac;
    }

    public String getCanLe() {
        return canLe;
    }

    public void setCanLe(String canLe) {
        this.canLe = canLe;
    }

    public Boolean getCotDong() {
        return cotDong;
    }

    public void setCotDong(Boolean cotDong) {
        this.cotDong = cotDong;
    }

    public Boolean getKhoaCot() {
        return khoaCot;
    }

    public void setKhoaCot(Boolean khoaCot) {
        this.khoaCot = khoaCot;
    }

    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    public Timestamp getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Timestamp ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    public Boolean getSuDung() {
        return suDung;
    }

    public void setSuDung(Boolean suDung) {
        this.suDung = suDung;
    }

    public Long getPhienBan() {
        return phienBan;
    }

    public void setPhienBan(Long phienBan) {
        this.phienBan = phienBan;
    }

    public BmBaoCaoEntity getBmBaoCaoId() {
        return bmBaoCaoByBmBaoCaoId;
    }

    public void setBmBaoCaoId(BmBaoCaoEntity bmBaoCaoId) {
        this.bmBaoCaoByBmBaoCaoId = bmBaoCaoId;
    }

//    @XmlTransient
//    public Collection<BmSheetCtEntity> getBmSheetCtCollection() {
//        return bmSheetCtCollection;
//    }
//
//    public void setBmSheetCtCollection(Collection<BmSheetCtEntity> bmSheetCtCollection) {
//        this.bmSheetCtCollection = bmSheetCtCollection;
//    }

    public BmBaoCaoEntity getBmBaoCaoByBmBaoCaoId() {
		return bmBaoCaoByBmBaoCaoId;
	}

	public void setBmBaoCaoByBmBaoCaoId(BmBaoCaoEntity bmBaoCaoByBmBaoCaoId) {
		this.bmBaoCaoByBmBaoCaoId = bmBaoCaoByBmBaoCaoId;
	}

	public BmSheetEntity getBmSheetByBmSheetId() {
		return bmSheetByBmSheetId;
	}

	public void setBmSheetByBmSheetId(BmSheetEntity bmSheetByBmSheetId) {
		this.bmSheetByBmSheetId = bmSheetByBmSheetId;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmSheetCotEntity)) {
            return false;
        }
        BmSheetCotEntity other = (BmSheetCotEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "java_demo.folder.BmSheetCotEntity[ id=" + id + " ]";
    }
}
