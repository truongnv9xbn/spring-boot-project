package com.temp.persistance.entity.BieuMauBaoCao;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BC_KHAI_THAC_GT", catalog = "")
public class BcKhaiThacGtEntity {
    private Integer id;
    private Integer bcKhaiThacId;
    private Integer bmBaoCaoId;
    private Integer bmSheetId;
    private Integer bmSheetHangId;
    private Integer bmSheetCotId;
    private Integer bmSheetCtId;
    private Integer rowId;
    private String giaTri;
    private String kyBaoCao;
    private Integer bcktId;

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    @Basic
    @Column(name = "BC_KHAI_THAC_ID", nullable = true)
    public Integer getBcKhaiThacId() {
		return bcKhaiThacId;
	}

	public void setBcKhaiThacId(Integer bcKhaiThacId) {
		this.bcKhaiThacId = bcKhaiThacId;
	}
	
	@Basic
    @Column(name = "BM_SHEET_ID", nullable = true)
	public Integer getBmSheetId() {
		return bmSheetId;
	}

	public void setBmSheetId(Integer bmSheetId) {
		this.bmSheetId = bmSheetId;
	}
	
	@Basic
    @Column(name = "BM_SHEET_HANG_ID", nullable = true)
	public Integer getBmSheetHangId() {
		return bmSheetHangId;
	}

	public void setBmSheetHangId(Integer bmSheetHangId) {
		this.bmSheetHangId = bmSheetHangId;
	}

	@Basic
    @Column(name = "BM_SHEET_COT_ID", nullable = true)
	public Integer getBmSheetCotId() {
		return bmSheetCotId;
	}

	public void setBmSheetCotId(Integer bmSheetCotId) {
		this.bmSheetCotId = bmSheetCotId;
	}

	@Basic
    @Column(name = "BM_SHEET_CT_ID", nullable = true)
	public Integer getBmSheetCtId() {
		return bmSheetCtId;
	}

	public void setBmSheetCtId(Integer bmSheetCtId) {
		this.bmSheetCtId = bmSheetCtId;
	}

	@Basic
    @Column(name = "ROW_ID", nullable = true)
	public Integer getRowId() {
		return rowId;
	}

	public void setRowId(Integer rowId) {
		this.rowId = rowId;
	}

	@Basic
    @Column(name = "GIA_TRI", nullable = true)
	public String getGiaTri() {
		return giaTri;
	}

	public void setGiaTri(String giaTri) {
		this.giaTri = giaTri;
	}

	@Basic
    @Column(name = "BM_BAO_CAO_ID", nullable = true)
    public Integer getBmBaoCaoId() {
        return bmBaoCaoId;
    }

    public void setBmBaoCaoId(Integer bmBaoCaoId) {
        this.bmBaoCaoId = bmBaoCaoId;
    }
    
    @Basic
    @Column(name = "BCKT_ID", nullable = true, precision = 0)
    public Integer getBcktId() {
        return bcktId;
    }

    public void setBcktId(Integer bcktId) {
        this.bcktId = bcktId;
    }
    
    @Basic
    @Column(name = "KY_BAO_CAO", nullable = true, precision = 0)
    public String getKyBaoCao() {
        return kyBaoCao;
    }

    public void setKyBaoCao(String kyBaoCao) {
        this.kyBaoCao = kyBaoCao;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BcKhaiThacGtEntity that = (BcKhaiThacGtEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(bmBaoCaoId, that.bmBaoCaoId) &&
                Objects.equals(bmSheetId, that.bmSheetId) &&
                Objects.equals(bmSheetHangId, that.bmSheetHangId) &&
                Objects.equals(bmSheetCotId, that.bmSheetCotId) &&
                Objects.equals(bmSheetCtId, that.bmSheetCtId)&&
                Objects.equals(rowId, that.rowId)&&
                Objects.equals(giaTri, that.giaTri)&&
                Objects.equals(bcktId, that.bcktId)&&
                Objects.equals(kyBaoCao, that.kyBaoCao)
                ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, bmBaoCaoId, bmSheetId, bmSheetHangId, bmSheetCotId, bmSheetCtId, rowId, giaTri, bcktId, kyBaoCao);
    }
}
