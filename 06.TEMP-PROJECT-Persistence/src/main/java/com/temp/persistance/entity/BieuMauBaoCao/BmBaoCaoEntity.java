package com.temp.persistance.entity.BieuMauBaoCao;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BM_BAO_CAO", catalog = "")
public class BmBaoCaoEntity {
    private Integer id;
    private String maBaoCao;
    private String tenBaoCao;
    private String canCuPhapLy;
    private String nhomBaoCao;
    private Boolean canCbtt;
    private Boolean canImport;
    private String fileDinhKem;
    private String moTa;
    private Integer suDung;
    private Integer trangThai;
    private String loaiBaoCao;
    private String loaiBaoCaoLT;
    private Long phienBan;
    private Timestamp ngayPhienBan;
    private Boolean trichYeu;
    private Integer nguoiTaoId;
    private Timestamp ngayTao;
    private Integer nguoiCapNhatId;
    private Timestamp ngayCapNhat;
    private String doiTuongGui;
    private Integer bmBaoCaoId;
    private Integer kieuBaoCao;
    
    private String loaiTinCbtt;
    private Boolean baoCaoDauVao;
    
    private Integer bmNextId;
    
    @Basic
    @Column(name = "BAO_CAO_DAU_VAO", nullable = true, precision = 0)
    public Boolean getBaoCaoDauVao() {
		return baoCaoDauVao;
	}



	public void setBaoCaoDauVao(Boolean baoCaoDauVao) {
		this.baoCaoDauVao = baoCaoDauVao;
	}



	public BmBaoCaoEntity(Integer id) {
		super();
		this.id = id;
	}
    
    

	public BmBaoCaoEntity() {
		super();
	}



	@Column(name = "LOAI_TIN_CBTT", nullable = true)
    public String getLoaiTinCbtt() {
		return loaiTinCbtt;
	}

	public void setLoaiTinCbtt(String loaiTinCbtt) {
		this.loaiTinCbtt = loaiTinCbtt;
	}

	@Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "MA_BAO_CAO", nullable = true, length = 200)
    public String getMaBaoCao() {
        return maBaoCao;
    }

    public void setMaBaoCao(String maBaoCao) {
        this.maBaoCao = maBaoCao;
    }
    
    @Basic
    @Column(name = "TEN_BAO_CAO", nullable = true, length = 2000)
    public String getTenBaoCao() {
        return tenBaoCao;
    }
    public void setTenBaoCao(String tenBaoCao) {
        this.tenBaoCao = tenBaoCao;
    }
    
    @Basic
    @Column(name = "LOAI_BAO_CAO_LT", nullable = true, length = 200)
    public String getLoaiBaoCaoLT() {
		return loaiBaoCaoLT;
	}

	public void setLoaiBaoCaoLT(String loaiBaoCaoLT) {
		this.loaiBaoCaoLT = loaiBaoCaoLT;
	}

    @Basic
    @Column(name = "CAN_CU_PHAP_LY", nullable = true, length = 2000)
    public String getCanCuPhapLy() {
        return canCuPhapLy;
    }

    public void setCanCuPhapLy(String canCuPhapLy) {
        this.canCuPhapLy = canCuPhapLy;
    }

    @Basic
    @Column(name = "NHOM_BAO_CAO", nullable = true, length = 20)
    public String getNhomBaoCao() {
        return nhomBaoCao;
    }

    public void setNhomBaoCao(String nhomBaoCao) {
        this.nhomBaoCao = nhomBaoCao;
    }

    @Basic
    @Column(name = "CAN_CBTT", nullable = true, precision = 0)
    public Boolean getCanCbtt() {
        return canCbtt;
    }

    public void setCanCbtt(Boolean canCbtt) {
        this.canCbtt = canCbtt;
    }

    @Basic
    @Column(name = "CAN_IMPORT", nullable = true, precision = 0)
    public Boolean getCanImport() {
        return canImport;
    }

    public void setCanImport(Boolean canImport) {
        this.canImport = canImport;
    }

    @Basic
    @Column(name = "FILE_DINH_KEM", nullable = true, length = 4000)
    public String getFileDinhKem() {
        return fileDinhKem;
    }

    public void setFileDinhKem(String fileDinhKem) {
        this.fileDinhKem = fileDinhKem;
    }

    @Basic
    @Column(name = "MO_TA", nullable = true, length = 4000)
    public String getMoTa() {
        return moTa;
    }

    public void setMoTa(String moTa) {
        this.moTa = moTa;
    }

    @Basic
    @Column(name = "SU_DUNG", nullable = true, precision = 0)
    public Integer getSuDung() {
        return suDung;
    }

    public void setSuDung(Integer suDung) {
        this.suDung = suDung;
    }

    @Basic
    @Column(name = "TRANG_THAI", nullable = true, precision = 0)
    public Integer getTrangThai() {
        return trangThai;
    }

    public void setTrangThai(Integer trangThai) {
        this.trangThai = trangThai;
    }

    @Basic
    @Column(name = "LOAI_BAO_CAO", nullable = true, length = 20)
    public String getLoaiBaoCao() {
        return loaiBaoCao;
    }

    public void setLoaiBaoCao(String loaiBaoCao) {
        this.loaiBaoCao = loaiBaoCao;
    }

    @Basic
    @Column(name = "PHIEN_BAN", nullable = true, precision = 0)
    public Long getPhienBan() {
        return phienBan;
    }

    public void setPhienBan(Long phienBan) {
        this.phienBan = phienBan;
    }

    @Basic
    @Column(name = "NGAY_PHIEN_BAN", nullable = true)
    public Timestamp getNgayPhienBan() {
        return ngayPhienBan;
    }

    public void setNgayPhienBan(Timestamp ngayPhienBan) {
        this.ngayPhienBan = ngayPhienBan;
    }

    @Basic
    @Column(name = "TRICH_YEU", nullable = true, precision = 0)
    public Boolean getTrichYeu() {
        return trichYeu;
    }

    public void setTrichYeu(Boolean trichYeu) {
        this.trichYeu = trichYeu;
    }

    @Basic
    @Column(name = "NGUOI_TAO_ID", nullable = true, precision = 0)
    public Integer getNguoiTaoId() {
        return nguoiTaoId;
    }

    public void setNguoiTaoId(Integer nguoiTaoId) {
        this.nguoiTaoId = nguoiTaoId;
    }

    @Basic
    @Column(name = "NGAY_TAO", nullable = true)
    public Timestamp getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Timestamp ngayTao) {
        this.ngayTao = ngayTao;
    }

    @Basic
    @Column(name = "NGUOI_CAP_NHAT_ID", nullable = true, precision = 0)
    public Integer getNguoiCapNhatId() {
        return nguoiCapNhatId;
    }

    public void setNguoiCapNhatId(Integer nguoiCapNhatId) {
        this.nguoiCapNhatId = nguoiCapNhatId;
    }

    @Basic
    @Column(name = "NGAY_CAP_NHAT", nullable = true)
    public Timestamp getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Timestamp ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Basic
    @Column(name = "DOI_TUONG_GUI", nullable = true, length = 50)
    public String getDoiTuongGui() {
        return doiTuongGui;
    }

    public void setDoiTuongGui(String doiTuongGui) {
        this.doiTuongGui = doiTuongGui;
    }

    @Basic
    @Column(name = "BM_BAO_CAO_ID", nullable = true, precision = 0)
    public Integer getBmBaoCaoId() {
        return bmBaoCaoId;
    }

    public void setBmBaoCaoId(Integer bmBaoCaoId) {
        this.bmBaoCaoId = bmBaoCaoId;
    }
    
    @Basic
    @Column(name = "KIEU_BAO_CAO", nullable = true)
    public Integer getKieuBaoCao() {
        return kieuBaoCao;
    }

    public void setKieuBaoCao(Integer kieuBaoCao) {
        this.kieuBaoCao = kieuBaoCao;
    }

    @Basic
    @Column(name = "BM_NEXT_ID", nullable = true, precision = 0)
    public Integer getBmNextId() {
        return bmNextId;
    }

    public void setBmNextId(Integer bmNextId) {
        this.bmNextId = bmNextId;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BmBaoCaoEntity that = (BmBaoCaoEntity) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(maBaoCao, that.maBaoCao) &&
                Objects.equals(tenBaoCao, that.tenBaoCao) &&
                Objects.equals(canCuPhapLy, that.canCuPhapLy) &&
                Objects.equals(nhomBaoCao, that.nhomBaoCao) &&
                Objects.equals(canCbtt, that.canCbtt) &&
                Objects.equals(canImport, that.canImport) &&
                Objects.equals(fileDinhKem, that.fileDinhKem) &&
                Objects.equals(moTa, that.moTa) &&
                Objects.equals(suDung, that.suDung) &&
                Objects.equals(trangThai, that.trangThai) &&
                Objects.equals(loaiBaoCao, that.loaiBaoCao) &&
                Objects.equals(phienBan, that.phienBan) &&
                Objects.equals(ngayPhienBan, that.ngayPhienBan) &&
                Objects.equals(trichYeu, that.trichYeu) &&
                Objects.equals(nguoiTaoId, that.nguoiTaoId) &&
                Objects.equals(ngayTao, that.ngayTao) &&
                Objects.equals(nguoiCapNhatId, that.nguoiCapNhatId) &&
                Objects.equals(ngayCapNhat, that.ngayCapNhat) &&
                Objects.equals(doiTuongGui, that.doiTuongGui) &&
                Objects.equals(bmBaoCaoId, that.bmBaoCaoId)&&
                Objects.equals(kieuBaoCao, that.kieuBaoCao)&&
                Objects.equals(bmNextId, that.bmNextId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, maBaoCao, tenBaoCao, canCuPhapLy, nhomBaoCao, canCbtt, canImport, fileDinhKem, moTa, suDung, trangThai, loaiBaoCao, phienBan, ngayPhienBan, trichYeu, nguoiTaoId, ngayTao, nguoiCapNhatId, ngayCapNhat, doiTuongGui, bmBaoCaoId, kieuBaoCao, bmNextId);
    }
}
