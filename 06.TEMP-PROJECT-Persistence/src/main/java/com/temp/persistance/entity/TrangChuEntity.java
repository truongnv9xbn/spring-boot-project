package com.temp.persistance.entity;

import java.sql.Timestamp;
import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANG_CHU", catalog = "")
public class TrangChuEntity {
	private int id;
	private int luotLike;
	private Integer nguoiDungId;
	private String baiViet;
	private String image;
	private Timestamp ngayTao;
	private Timestamp ngayCapNhat;
	private Boolean trangThai;
	private String taiKhoan;

	@Id
	@Column(name = "ID", nullable = false, precision = 0)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "LUOT_LIKE")
	public int getLuotLike() {
		return luotLike;
	}

	public void setLuotLike(int luotLike) {
		this.luotLike = luotLike;
	}

	@Basic
	@Column(name = "BAI_VIET", nullable = true, length = 1000)
	public String getBaiViet() {
		return baiViet;
	}

	public void setBaiViet(String baiViet) {
		this.baiViet = baiViet;
	}

	@Basic
	@Column(name = "IMAGE", nullable = true, length = 1000)
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Basic
	@Column(name = "TAI_KHOAN", nullable = true, length = 1000)
	public String getTaiKhoan() {
		return taiKhoan;
	}

	public void setTaiKhoan(String taiKhoan) {
		this.taiKhoan = taiKhoan;
	}

	@Basic
	@Column(name = "NGUOI_DUNG_ID", nullable = true, precision = 0)
	public Integer getNguoiDungId() {
		return nguoiDungId;
	}

	public void setNguoiDungId(Integer nguoiDungId) {
		this.nguoiDungId = nguoiDungId;
	}

	@Basic
	@Column(name = "NGAY_TAO", nullable = true)
	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
	}

	@Basic
	@Column(name = "NGAY_CAP_NHAT", nullable = true)
	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	@Basic
	@Column(name = "TRANG_THAI", nullable = true, precision = 0)
	public Boolean getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(Boolean trangThai) {
		this.trangThai = trangThai;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		TrangChuEntity that = (TrangChuEntity) o;
		return id == that.id && luotLike == that.luotLike && Objects.equals(baiViet, that.baiViet)
				&& image == that.image && Objects.equals(nguoiDungId, that.nguoiDungId)
				&& Objects.equals(ngayTao, that.ngayTao) && Objects.equals(ngayCapNhat, that.ngayCapNhat)
				&& Objects.equals(trangThai, that.trangThai);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, luotLike, baiViet, image, nguoiDungId, ngayTao, ngayCapNhat, trangThai);
	}
}
