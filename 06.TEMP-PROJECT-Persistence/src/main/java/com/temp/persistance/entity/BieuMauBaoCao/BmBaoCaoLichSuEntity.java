/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.temp.persistance.entity.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.temp.persistance.entity.NguoiDung.QtNguoiDungEntity;
import com.temp.utils.LocalDateTimeUtils;


/**
 *
 * @author vuongtm1
 */
@Entity
@Table(name = "BM_BAO_CAO_LICH_SU")
 
public class BmBaoCaoLichSuEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    @Column(name = "ID", nullable = false, precision = 0)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @Column(name = "THOI_GIAN")
    private Timestamp thoiGian;
    
    @Column(name = "THAO_TAC")
    private String thaoTac;
    
    @Column(name = "BM_ID")
    private Integer bmId;
    
    @Column(name = "BM_BAO_CAO_ID")
    private Integer bmBaoCaoId;
    
    @Column(name = "SU_DUNG")
    private Integer suDung;
    
    @Column(name = "PHIEN_BAN")
    private Integer phienBan;
    
    @JoinColumn(name = "NGUOI_THUC_HIEN", referencedColumnName = "ID")
    @ManyToOne
    private QtNguoiDungEntity nguoiThucHien;
    
    @Column(name = "BM_NEXT_ID")
    private Integer bmNextId;
    
	public Integer getBmNextId() {
		return bmNextId;
	}

	public void setBmNextId(Integer bmNextId) {
		this.bmNextId = bmNextId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getThoiGian() {
		return thoiGian;
	}

	public void setThoiGian(Timestamp thoiGian) {
		this.thoiGian = thoiGian;
	}

 

	public QtNguoiDungEntity getNguoiThucHien() {
		return nguoiThucHien;
	}

	public void setNguoiThucHien(QtNguoiDungEntity nguoiThucHien) {
		this.nguoiThucHien = nguoiThucHien;
	}

	public String getThaoTac() {
		return thaoTac;
	}

	public void setThaoTac(String thaoTac) {
		this.thaoTac = thaoTac;
	}

	public Integer getBmId() {
		return bmId;
	}

	public void setBmId(Integer bmId) {
		this.bmId = bmId;
	}

	public Integer getBmBaoCaoId() {
		return bmBaoCaoId;
	}

	public void setBmBaoCaoId(Integer bmBaoCaoId) {
		this.bmBaoCaoId = bmBaoCaoId;
	}

	public Integer getSuDung() {
		return suDung;
	}

	public void setSuDung(Integer suDung) {
		this.suDung = suDung;
	}

	public Integer getPhienBan() {
		return phienBan;
	}

	public void setPhienBan(Integer phienBan) {
		this.phienBan = phienBan;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public BmBaoCaoLichSuEntity() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BmBaoCaoLichSuEntity(Integer nguoiThucHien,  String thaoTac, Integer bmId, Integer bmBaoCaoId,
			Integer suDung, Integer phienBan, String tenNguoiThucHien) {
		super();
		this.thoiGian=LocalDateTimeUtils.getCurrentTimestamp();
		//this.nguoiThucHien = nguoiThucHien;
		this.thaoTac = thaoTac;
		this.bmId = bmId;
		this.bmBaoCaoId = bmBaoCaoId;
		this.suDung = suDung;
		this.phienBan = phienBan;
		//this.tenNguoiThucHien = tenNguoiThucHien;
	}

	@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BmBaoCaoLichSuEntity)) {
            return false;
        }
        BmBaoCaoLichSuEntity other = (BmBaoCaoLichSuEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Entity.BmBaoCaoLichSuEntity[ id=" + id + " ]";
    }
    
}
