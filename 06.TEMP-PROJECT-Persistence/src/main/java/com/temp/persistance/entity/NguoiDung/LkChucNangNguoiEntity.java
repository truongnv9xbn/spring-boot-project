package com.temp.persistance.entity.NguoiDung;

import java.util.Objects;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.temp.persistance.embeddable.LkChucNangNguoiEmbeddable;

@Entity
@Table(name = "LK_CHUC_NANG_NGUOI", catalog = "")
@IdClass(LkChucNangNguoiEmbeddable.class)
public class LkChucNangNguoiEntity {
	private Integer chucNangId;
	private Integer nguoiDungId;
	private String chucNangChiTiet;

	@Id
	@Basic
	@Column(name = "CHUC_NANG_ID")
	public Integer getChucNangId() {
		return chucNangId;
	}

	public void setChucNangId(Integer chucNangId) {
		this.chucNangId = chucNangId;
	}

	@Basic
	@Column(name = "NGUOI_DUNG_ID")
	public Integer getNguoiDungId() {
		return nguoiDungId;
	}

	public void setNguoiDungId(Integer nguoiDungId) {
		this.nguoiDungId = nguoiDungId;
	}

	@Basic
	@Column(name = "CHUC_NANG_CHI_TIET")
	public String getChucNangChiTiet() {
		return chucNangChiTiet;
	}

	public void setChucNangChiTiet(String chucNangChiTiet) {
		this.chucNangChiTiet = chucNangChiTiet;
	}

	public LkChucNangNguoiEntity() {
		super();
	}

	public LkChucNangNguoiEntity(Integer chucNangId, Integer nguoiDungId) {
		super();
		this.chucNangId = chucNangId;
		this.nguoiDungId = nguoiDungId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		LkChucNangNguoiEntity that = (LkChucNangNguoiEntity) o;
		return Objects.equals(chucNangId, that.chucNangId) && Objects.equals(nguoiDungId, that.nguoiDungId)
				&& Objects.equals(chucNangChiTiet, that.chucNangChiTiet);
	}

	@Override
	public int hashCode() {
		return Objects.hash(chucNangId, nguoiDungId, chucNangChiTiet);
	}
}
