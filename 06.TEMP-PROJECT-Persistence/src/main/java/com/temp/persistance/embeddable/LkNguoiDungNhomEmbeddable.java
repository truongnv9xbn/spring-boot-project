package com.temp.persistance.embeddable;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LkNguoiDungNhomEmbeddable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	private Integer nguoiDungId;

	@NotNull
	private Integer nhomNguoiDungId;

	
}
