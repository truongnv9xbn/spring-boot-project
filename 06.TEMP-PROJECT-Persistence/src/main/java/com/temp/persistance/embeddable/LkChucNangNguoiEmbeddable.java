package com.temp.persistance.embeddable;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class LkChucNangNguoiEmbeddable implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull
	private Integer chucNangId ;

	@NotNull
	private Integer nguoiDungId;

	
}
