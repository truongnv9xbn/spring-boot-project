package com.temp.persistence.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.MessageBDTO;
import com.temp.model.MessageDTO;

@Repository
@Transactional
public interface MessageDao {
	MessageBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc,String trangThai);
	
	boolean addOrUpdate(MessageDTO dto);
}
