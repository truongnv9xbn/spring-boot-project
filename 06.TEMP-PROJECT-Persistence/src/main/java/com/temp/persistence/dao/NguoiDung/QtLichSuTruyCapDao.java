package com.temp.persistence.dao.NguoiDung;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.NguoiDung.QtLichSuTruyCapBDTO;


@Repository
@Transactional
public interface QtLichSuTruyCapDao {

    /**
     * Get lich su truy cap list - DAO
     * 
     * @param keysearch
     * @param ipThucHien
     * @param tenTaiKhoan
     * @param tuNgay
     * @param denNgay
     * @param logType
     * @param pageNo
     * @param pageSize
     * @param keySort
     * @param desc
     * @param loaiNguoiDung
     * @return
     */
    QtLichSuTruyCapBDTO getLichSuTruyCapList(String keysearch, String ipThucHien,
	    String tenTaiKhoan, String tuNgay, String denNgay, String logType, Integer pageNo,
	    Integer pageSize, String keySort, boolean desc, String loaiNguoiDung, Integer ctckThongTinId);

    /**
     * Get lich su truy cap export list - DAO
     * 
     * @param tuNgay
     * @param denNgay
     * @param keySort
     * @param desc
     * @return
     */
    QtLichSuTruyCapBDTO getListLichSuTruyCapExport(String tuNgay, String denNgay, String keySort,
	    boolean desc);

}
