package com.temp.persistence.dao.BieuMauBaoCao;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDongDTO;

@Repository
@Transactional
public interface BcBaoCaoGtDongDao {

    /**
     * Luu noi dung bao cao dong - DAO
     * 
     * @param dataBaoCaoDynamic
     * @return
     * @throws Exception
     */
    boolean saveNoiDungBaoCaoDynamicDao(BcBaoCaoGtDongDTO dataBaoCaoDynamic) throws Exception;
    BcBaoCaoGtDongDTO getHangDong(int bcThanhVienId, int sheetHang);
    BcBaoCaoGtDongDTO getHangDongCif(int bmBaoCaoId, int sheetHang, int ctckThongTinId);
    
    BcBaoCaoGtDongDTO getGiaTriChiTieuDong(Integer bcThanhVienId,Integer bmSheetHang);
    BcBaoCaoGtDongDTO getHangDongCifListCtck(int bmBaoCaoId, int sheetHang, List<Integer>ctckThongTinId);
}
