package com.temp.persistence.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.TrangChuBDTO;
import com.temp.model.TrangChuDTO;

@Repository
@Transactional
public interface TrangChuDao {
	TrangChuBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc,String trangThai);
	
	boolean addOrUpdate(TrangChuDTO dto);
}
