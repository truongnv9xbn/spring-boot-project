package com.temp.persistence.dao.NguoiDung;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.NguoiDung.ChungThuSoBDTO;
import com.temp.model.NguoiDung.ChungThuSoDTO;
import com.temp.persistance.entity.NguoiDung.ChungThuSoEntity;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Repository
@Transactional
public interface ChungThuSoDao {
	
	ChungThuSoDTO findById(int id);
	
	List<Object> findByUserId(Integer id);
	
	boolean changeStatus(ChungThuSoDTO dto);

	ChungThuSoEntity create(ChungThuSoDTO dto);
	
	boolean update(ChungThuSoDTO dto);

	boolean delete(int id);

	boolean isExistById(int id);

//	ChungThuSoBDTO list(Integer id,
//			Integer nguoiDungId,
//			String taiKhoan,
//			String keys,
//			String serial,
//			String subject,
//			String issuer,
//			Boolean isCaNhan,
//			Integer nguoiTaoId,
//			Timestamp nguoiTao,
//			Integer nguoiCapNhatId,
//			Timestamp ngayCapNhat,
//			Boolean trangThai) ;

	ChungThuSoBDTO list(Integer pageNo, Integer pageSize, boolean desc, String filterkey, String taiKhoan, Integer trangThai);
	
	List<ChungThuSoDTO> listCts(Integer pageNo, Integer pageSize, boolean desc, String filterkey);

	boolean checkExisted(ChungThuSoDTO dto);
}
