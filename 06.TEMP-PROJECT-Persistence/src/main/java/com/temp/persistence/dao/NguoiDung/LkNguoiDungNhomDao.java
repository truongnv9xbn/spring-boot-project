package com.temp.persistence.dao.NguoiDung;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.NguoiDung.LkNguoiDungNhomBDTO;
import com.temp.model.NguoiDung.LkNguoiDungNhomDTO;


/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Repository
@Transactional
public interface LkNguoiDungNhomDao {
	LkNguoiDungNhomDTO findById(int id);

	boolean deleteLkNguoiDungNhomById(int id);

	boolean addOrUpdateLkNguoiDungNhom(List<LkNguoiDungNhomBDTO> ListlkNguoiDungNhomBDTO);
}
