package com.temp.persistence.dao.NguoiDung;

import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.temp.model.DropDownDTO;
import com.temp.model.NguoiDung.ChucNangBDTO;
import com.temp.model.NguoiDung.ChucNangDTO;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Repository
@Transactional
public interface QtChucNangDao {

	ChucNangDTO findById(int id);
	
	DropDownDTO findChucNangCha(int id);
	
	DropDownDTO findAction(int id);

	boolean addOrUpdateChucNang(ChucNangDTO dto);

	boolean deleteChucNangById(int id);

	boolean isChucNangExist(String maChucNang, boolean trangThai);

	boolean isExistById(int id);

	ChucNangBDTO listChucNangs(String keySearch, String tenChucNang, String maChucNang, Integer actionId, String phanHe,
			String ghiChu, String trangThai);

	ChucNangBDTO getAll(String keySearch, String tenChucNang, String maChucNang, Integer actionId, String phanHe,
			String ghiChu, String trangThai);
	
	ChucNangBDTO getDropdownKhoaChaId();
	
	ChucNangBDTO getDropdownAction();
	
	List<ChucNangDTO> listChucNangsForPhanQuyen();
	
	boolean isExistByMa(String maChucNang,  Integer id);
}
