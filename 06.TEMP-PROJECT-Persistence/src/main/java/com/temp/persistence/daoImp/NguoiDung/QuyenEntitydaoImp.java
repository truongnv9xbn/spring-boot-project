package com.temp.persistence.daoImp.NguoiDung;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.DropDownDTO;
import com.temp.model.NguoiDung.QuyenBDTO;
import com.temp.model.NguoiDung.QuyenDTO;
import com.temp.persistance.entity.NguoiDung.LkQuyenNguoiEntity;
import com.temp.persistance.entity.NguoiDung.LkQuyenNhomNguoiEntity;
import com.temp.persistance.entity.NguoiDung.QuyenChiTietEntity;
import com.temp.persistance.entity.NguoiDung.QuyenEntity;
import com.temp.persistence.dao.NguoiDung.QuyenDao;
import com.temp.persistence.responsitories.NguoiDung.QuyenEntityRepository;
import com.temp.utils.NativeQuery;
import com.temp.utils.ObjectMapperUtils;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Component
public class QuyenEntitydaoImp implements QuyenDao {

	private QuyenEntityRepository QuyenEntityRepository;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	public QuyenEntitydaoImp(QuyenEntityRepository QuyenEntityRepository) {
		this.QuyenEntityRepository = QuyenEntityRepository;
	}

	@Override
	public QuyenDTO findById(int id) {

		QuyenDTO dto = new QuyenDTO();
		try {
			QuyenEntity entity = QuyenEntityRepository.findById(id).orElse(null);
			if (entity != null) {

				dto = ObjectMapperUtils.map(entity, QuyenDTO.class);
//			dto.setIsMenu(entity.getMenu()?1:0);
//			dto.setIsMenu(entity.getMenu()?1:0);
//			dto.setIsDeleted(entity.getDeleted()?1:0);
//			dto.setTrangThai(entity.getTrangThai()?1:0);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return dto;
	}

	@Override
	public boolean addOrUpdateQuyen(QuyenDTO dto) {
		try {
			QuyenEntity entity = new ModelMapper().map(dto, QuyenEntity.class);
			QuyenEntity tmp = QuyenEntityRepository.save(entity);
			if (tmp != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return false;
	}

	@Override
	public boolean deleteQuyenById(int id) {
		try {
			if (id > 0) {
				List<QuyenEntity> lstQuyenCon = new ArrayList<>();
				String queryStr = "SELECT c FROM " + QuyenEntity.class.getName() + " c WHERE  c.khoaChaId = :id";
				Query query = entityManager.createQuery(queryStr);
				query.setParameter("id", id);
				lstQuyenCon = query.getResultList();
				if (lstQuyenCon == null || lstQuyenCon.size() == 0) {
					try {
						// // delete by nguoiDungId bảng liên kết
						query = entityManager.createQuery("delete from " + LkQuyenNguoiEntity.class.getName()
								+ " c WHERE (c.QuyenId = :QuyenId ) ");
						query.setParameter("QuyenId", id).executeUpdate();
						query = entityManager.createQuery("delete from " + LkQuyenNhomNguoiEntity.class.getName()
								+ " c WHERE (c.QuyenId = :QuyenId ) ");
						query.setParameter("QuyenId", id).executeUpdate();

						QuyenEntityRepository.deleteById(id);
						Optional<QuyenEntity> obj = QuyenEntityRepository.findById(id);
						if (!obj.isPresent()) {
							return true;
						}

					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isQuyenExist(String maQuyen, boolean trangThai) {
		try {
			Query query = entityManager.createQuery("SELECT c FROM " + QuyenEntity.class.getName()
					+ " c WHERE (c.maQuyen = :maQuyen ) and (c.trangThai =:trangthai)");
			query.setParameter("maQuyen", maQuyen);
			query.setParameter("trangthai", trangThai);

			List<QuyenEntity> listQuyen = query.getResultList();

			if (listQuyen.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return false;
	}

	@Override
	public boolean isExistById(int id) {

		boolean result = QuyenEntityRepository.existsById(id);
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public QuyenBDTO getDropdownKhoaChaId() {

		QuyenBDTO bdto = new QuyenBDTO();
		try {
			List<Object[]> objList = entityManager.createNativeQuery(NativeQuery.Quyen.SQL_DROPDOWN).getResultList();

			bdto.listDropdownKhoaChaId = objList.stream().map(DropDownDTO::new).collect(Collectors.toList());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return bdto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public QuyenBDTO getDropdownAction() {
		QuyenBDTO result = new QuyenBDTO();
		try {
			String stringQuery = "Select u From " + QuyenChiTietEntity.class.getName() + " u ";

			List<QuyenChiTietEntity> listDropDownActionEntity = entityManager.createQuery(stringQuery).getResultList();

			result.listDropdownAction = listDropDownActionEntity.stream().map(fromOject -> {
				DropDownDTO toObject = new DropDownDTO();
				toObject.setLabel(fromOject.getTenQuyen());
				toObject.setValue(fromOject.getId());
				return toObject;
			}).collect(Collectors.toList());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public QuyenBDTO listQuyens(String keySearch, String tenQuyen, String maQuyen, Integer actionId,
			String phanHe, String ghiChu, String trangThai) {
		QuyenBDTO result = new QuyenBDTO();
		try {
			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + QuyenEntity.class.getName() + " u ";

			// generate where
			stringWhere = this.getStrWhere(stringWhere, keySearch, tenQuyen, maQuyen, actionId, phanHe, ghiChu,
					trangThai);

			// TODO query
			stringQuery = stringQuery + stringWhere;

			List<QuyenEntity> listQuyen = entityManager.createQuery(stringQuery).getResultList();

			result.listQuyen = ObjectMapperUtils.mapAll(listQuyen, QuyenDTO.class);
			result.setMessage("Success");
			result.setStatus("200");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return result;

	}

	/**
	 * Get string order by of query
	 * 
	 * @param desc
	 * @param stringOderby
	 * @return String order by
	 */
	private String getStrOrderBy(boolean desc, String stringOderby) {
		if (desc) {
			stringOderby = stringOderby + " asc";
		} else {
			stringOderby = stringOderby + " desc";
		}
		return stringOderby;
	}

	/**
	 * * Get string where of query
	 * 
	 * @param stringWhere
	 * @param keysearch
	 * @param maQuyen
	 * @param tenQuyen
	 * @param trangThai
	 * @return String where
	 */
	private StringBuilder getStrWhere(StringBuilder stringWhere, String keySearch, String tenQuyen,
			String maQuyen, Integer actionId, String phanHe, String ghiChu, String trangThai) {

		if (keySearch != null) {
			keySearch = keySearch.toLowerCase();

			stringWhere.append(" and (( LOWER(u.maQuyen) like '%" + keySearch + "%') ");
			stringWhere.append(" or ( LOWER(u.phanHe) like '%" + keySearch + "%') ");
			stringWhere.append(" or ( LOWER(u.tenQuyen) like '%" + keySearch + "%') ");
			stringWhere.append(" or ( LOWER(u.ghiChu) like '%" + keySearch + "%') )");
		}

		if (maQuyen != null && (!"".endsWith(maQuyen))) {
			maQuyen = maQuyen.toLowerCase();
			stringWhere.append(" and ( LOWER(u.maQuyen) like '%" + maQuyen + "%') ");
		}
		if (tenQuyen != null && (!"".endsWith(tenQuyen))) {
			tenQuyen = tenQuyen.toLowerCase();
			stringWhere.append(" and ( LOWER(u.tenQuyen) like '%" + tenQuyen + "%') ");
		}
		if (trangThai != null && (!"".endsWith(trangThai))) {

			stringWhere.append(" and ( u.trangThai  = " + trangThai + ")");
		}
		if (ghiChu != null && (!"".endsWith(ghiChu))) {
			ghiChu = ghiChu.toLowerCase();
			stringWhere.append(" and ( LOWER(u.ghiChu) like '%" + ghiChu + "%') ");
		}

		return stringWhere;
	}

	/**
	 * * Get string where of query
	 * 
	 * @param stringWhere
	 * @param keysearch
	 * @param maQuyen
	 * @param tenQuyen
	 * @param trangThai
	 * @return String where
	 */
	private StringBuilder getStrWhereNative(StringBuilder stringWhere, String keySearch, String tenQuyen,
			String maQuyen, Integer actionId, String phanHe, String ghiChu, String trangThai) {

		if (keySearch != null) {
			keySearch = keySearch.toLowerCase();

			stringWhere.append(" and (( LOWER(a.MA_CHUC_NANG) like '%" + keySearch + "%') ");
			stringWhere.append(" or ( LOWER(a.TEN_CHUC_NANG) like '%" + keySearch + "%') ");
			stringWhere.append(" or ( LOWER(a.GHI_CHU) like '%" + keySearch + "%') )");
		}

		if (maQuyen != null && (!"".endsWith(maQuyen))) {
			maQuyen = maQuyen.toLowerCase();
			stringWhere.append(" and ( LOWER(a.MA_CHUC_NANG) like '%" + maQuyen + "%') ");
		}
		if (tenQuyen != null && (!"".endsWith(tenQuyen))) {
			tenQuyen = tenQuyen.toLowerCase();
			stringWhere.append(" and ( LOWER(a.TEN_CHUC_NANG) like '%" + tenQuyen + "%') ");
		}
		if (trangThai != null && (!"".endsWith(trangThai))) {

			stringWhere.append(" and ( a.TRANG_THAI  = " + trangThai + ")");
		}
		if (ghiChu != null && (!"".endsWith(ghiChu))) {
			ghiChu = ghiChu.toLowerCase();
			stringWhere.append(" and ( LOWER(a.GHI_CHU) like '%" + ghiChu + "%') ");
		}

		return stringWhere;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<QuyenDTO> listQuyensForPhanQuyen() {
		List<QuyenDTO> result = new ArrayList<QuyenDTO>();
		try {
			String stringQuery = "Select u From " + QuyenEntity.class.getName() + " u where u.trangThai = 1";
			List<QuyenEntity> listQuyen = entityManager.createQuery(stringQuery).getResultList();

			result = ObjectMapperUtils.mapAll(listQuyen, QuyenDTO.class);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public QuyenBDTO getAll(String keySearch, String tenQuyen, String maQuyen, Integer actionId, String phanHe,
			String ghiChu, String trangThai) {
		QuyenBDTO bdto = new QuyenBDTO();
		try {
			StringBuilder stringWhere = new StringBuilder(" where 1=1 ");
			String stringQuery = NativeQuery.Quyen.SQL_LIST;

			// generate where
			stringWhere = this.getStrWhereNative(stringWhere, keySearch, tenQuyen, maQuyen, actionId, phanHe, ghiChu,
					trangThai);

			// TODO query
			stringQuery = stringQuery + stringWhere;

			
			List<Object[]> objList = entityManager.createNativeQuery(stringQuery).getResultList();
			List<QuyenDTO> listData = objList.stream().map(fromOject -> {
				QuyenDTO toObject = new QuyenDTO(true, fromOject);
				return toObject;
			}).collect(Collectors.toList());

			bdto.setListQuyen(listData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return bdto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DropDownDTO findQuyenCha(int idCha) {
		DropDownDTO dto = new DropDownDTO();
		try {
			String sql = "SELECT u FROM " + QuyenEntity.class.getName() + " u where u.id = " + idCha;
			
			Query query = entityManager.createQuery(sql, QuyenEntity.class);
			List<QuyenEntity> QuyenCha = query.getResultList();

			if (QuyenCha.size() > 0) {
				dto.setLabel(QuyenCha.get(0).getTenQuyen());
				dto.setValue(QuyenCha.get(0).getId());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return dto;
	}

	@SuppressWarnings("unchecked")
	@Override
	public DropDownDTO findAction(int actionId) {
		DropDownDTO dto = new DropDownDTO();
		try {
			String sql = "SELECT u FROM " + QuyenChiTietEntity.class.getName() + " u where u.id = " + actionId;

			
			Query query = entityManager.createQuery(sql, QuyenChiTietEntity.class);
			List<QuyenChiTietEntity> listAction = query.getResultList();

			if (listAction.size() > 0) {
				dto.setLabel(listAction.get(0).getTenQuyen());
				dto.setValue(listAction.get(0).getId());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return dto;

	}

	@Override
	public boolean isExistByMa(String maQuyen, Integer id) {
		boolean b = false;
		try {
			b = QuyenEntityRepository.existsByMa(maQuyen, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return b;
	}

}
