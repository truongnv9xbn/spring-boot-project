package com.temp.persistence.daoImp.Common;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.Common.TaiLieuBDTO;
import com.temp.model.Common.TaiLieuDTO;
import com.temp.persistance.entity.Common.TaiLieuEntity;
import com.temp.persistence.dao.Common.TaiLieuDao;
import com.temp.persistence.responsitories.Common.TaiLieuEntityRepository;
import com.temp.utils.ObjectMapperUtils;

/**
 * Generated by Spring Data Generator on 30/10/2019
 */
@Component
public class TaiLieuEntitydaoImp implements TaiLieuDao {

	private TaiLieuEntityRepository taiLieuEntityRepository;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	public TaiLieuEntitydaoImp(TaiLieuEntityRepository taiLieuEntityRepository) {
		this.taiLieuEntityRepository = taiLieuEntityRepository;
	}

	@SuppressWarnings("unchecked")
	@Override
	public TaiLieuBDTO getList(String keysearch, String sSoVanBan, String sTrichYeu, String sNguoiTao, String trangThai,
			Integer pageNo, Integer pageSize, String keySort, boolean desc) {

		try {
			TaiLieuBDTO result = null;

			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + TaiLieuEntity.class.getName() + " u ";
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + TaiLieuEntity.class.getName() + " u ");
			String stringOderby = " order by u." + keySort;

			// generate where
			stringWhere = this.getStrWheres(stringWhere, keysearch, sSoVanBan, sTrichYeu, sNguoiTao, trangThai);

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby);

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby;
			Query q = entityManager.createQuery(stringQuery);
			q = this.SetParrams(q, stringQuery, keysearch, sSoVanBan, sTrichYeu, sNguoiTao, trangThai);
			List<TaiLieuEntity> lstTaiLieu = q.setFirstResult(pageNo * pageSize).setMaxResults(pageSize)
					.getResultList();
			if (lstTaiLieu != null) {

				result = new TaiLieuBDTO();

				// add condition count item
				stringQueryCount.append(stringWhere);

				// get count total item
				Query qCount = entityManager.createQuery(stringQueryCount.toString());
				qCount = this.SetParrams(qCount, stringQueryCount.toString(), keysearch, sSoVanBan, sTrichYeu,
						sNguoiTao, trangThai);

				result.totalElement = (long) qCount.getResultList().get(0);

				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize));
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;

				result.lstTaiLieu = ObjectMapperUtils.mapAll(lstTaiLieu, TaiLieuDTO.class);
				result.setMessage("Success");
				result.setStatus("200");
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return new TaiLieuBDTO();
	}

	private StringBuilder getStrWheres(StringBuilder stringWhere, String keysearch, String sSoVanBan, String sTrichYeu,
			String sNguoiTao, String trangThai) {
		if (keysearch != null) {
			stringWhere.append(" and (( LOWER(u.soVanBan) like :keysearch1) ");
			stringWhere.append(" or ( LOWER(u.trichYeuNoiDung) like :keysearch2) )");
		}

		if (sSoVanBan != null && (!"".endsWith(sSoVanBan))) {
			stringWhere.append(" and ( LOWER(u.soVanBan) like :sSoVanBan) ");
		}

		if (sTrichYeu != null && (!"".endsWith(sTrichYeu))) {
			stringWhere.append(" and ( LOWER(u.trichYeuNoiDung) like :sTrichYeu) ");
		}

		if (sNguoiTao != null && (!"".endsWith(sNguoiTao))) {
			stringWhere.append(" and ( LOWER(u.nguoiTao) like :sNguoiTao) ");
		}

		if (trangThai != null && (!"".endsWith(trangThai))) {
			stringWhere.append(" and ( LOWER(u.trangThai) = :trangThai)");
		}

		return stringWhere;
	}

	private Query SetParrams(Query q, String stringWhere, String keysearch, String sSoVanBan, String sTrichYeu,
			String sNguoiTao, String trangThai) {

		if (stringWhere.contains(":keysearch")) {
			keysearch = keysearch.toLowerCase();
			q.setParameter("keysearch1", '%' + keysearch + '%');
			q.setParameter("keysearch2", '%' + keysearch + '%');
		}

		if (stringWhere.contains(":sSoVanBan")) {
			sSoVanBan = sSoVanBan.toLowerCase();
			q.setParameter("sSoVanBan", '%' + sSoVanBan + '%');
		}

		if (stringWhere.contains(":sTrichYeu")) {
			sTrichYeu = sTrichYeu.toLowerCase();
			q.setParameter("sTrichYeu", '%' + sTrichYeu + '%');
		}

		if (stringWhere.contains(":sNguoiTao")) {
			sNguoiTao = sNguoiTao.toLowerCase();
			q.setParameter("sNguoiTao", '%' + sNguoiTao + '%');
		}

		if (stringWhere.contains(":trangThai")) {
			trangThai = trangThai.toLowerCase();
			q.setParameter("trangThai", trangThai);
		}

		return q;
	}

	@Override
	public TaiLieuDTO findById(int id) {
		try {
			Optional<TaiLieuEntity> eTaiLieuEntity = taiLieuEntityRepository.findById(id);
			if (eTaiLieuEntity.isPresent()) {
				TaiLieuEntity entity = eTaiLieuEntity.get();
				return ObjectMapperUtils.map(entity, TaiLieuDTO.class);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public TaiLieuBDTO listTaiLieus(String keysearch, String sTenTaiLieu, String sMaTaiLieu, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		try {
			TaiLieuBDTO result = null;

			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + TaiLieuEntity.class.getName() + " u ";
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + TaiLieuEntity.class.getName() + " u ");
			String stringOderby = " order by u." + keySort;

			// generate where
			stringWhere = this.getStrWhere(stringWhere, keysearch, sMaTaiLieu, sTenTaiLieu, trangThai);

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby);

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby;
			Query q = entityManager.createQuery(stringQuery);
			q = this.SetParram(q, stringQuery, keysearch, sTenTaiLieu, sMaTaiLieu, trangThai);
			List<TaiLieuEntity> lstTaiLieu = q.setFirstResult(pageNo * pageSize).setMaxResults(pageSize)
					.getResultList();
			if (lstTaiLieu != null) {

				result = new TaiLieuBDTO();

				// add condition count item
				stringQueryCount.append(stringWhere);

				// get count total item
				Query qCount = entityManager.createQuery(stringQueryCount.toString());
				qCount = this.SetParram(qCount, stringQueryCount.toString(), keysearch, sTenTaiLieu, sMaTaiLieu,
						trangThai);

				result.totalElement = (long) qCount.getResultList().get(0);

				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize));
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;

				result.lstTaiLieu = ObjectMapperUtils.mapAll(lstTaiLieu, TaiLieuDTO.class);
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return new TaiLieuBDTO();
	}

	@Override
	public boolean addOrUpdateTaiLieu(TaiLieuDTO dto) {
		try {
			TaiLieuEntity entity = new ModelMapper().map(dto, TaiLieuEntity.class);
			TaiLieuEntity tmp = taiLieuEntityRepository.save(entity);
			if (tmp != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return false;

	}

	@Override
	public boolean deleteTaiLieuById(int id) {
		try {
			if (id > 0) {

				taiLieuEntityRepository.deleteById(id);

				Optional<TaiLieuEntity> obj = taiLieuEntityRepository.findById(id);
				if (!obj.isPresent()) {
					return true;
				}
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isTaiLieuExistUpdate(int id, String maTaiLieu) {
		try {
			if (!StringUtils.isEmpty(maTaiLieu)) {
				maTaiLieu = maTaiLieu.toLowerCase();
			}
			String queryStr = "SELECT c FROM " + TaiLieuEntity.class.getName()
					+ " c WHERE  LOWER(c.maTaiLieu) like :maTaiLieu and c.id !=" + id + "";
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("maTaiLieu", maTaiLieu);
			List<TaiLieuEntity> listTaiLieu = query.getResultList();

			if (listTaiLieu.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isTaiLieuExistAdd(String maTaiLieu) {
		try {
			if (!StringUtils.isEmpty(maTaiLieu)) {
				maTaiLieu = maTaiLieu.toLowerCase();
			}
			String queryStr = "SELECT c FROM " + TaiLieuEntity.class.getName()
					+ " c WHERE  LOWER(c.maTaiLieu) = :maTaiLieu";
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("maTaiLieu", maTaiLieu);
			List<TaiLieuEntity> listTaiLieu = query.getResultList();

			if (listTaiLieu.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return false;
	}

	/**
	 * Get string order by of query
	 * 
	 * @param desc
	 * @param stringOderby
	 * @return String order by
	 */
	private String getStrOrderBy(boolean desc, String stringOderby) {
		if (desc) {
			stringOderby = stringOderby + " desc";
		} else {
			stringOderby = stringOderby + " asc";
		}
		return stringOderby;
	}

	/***
	 * Get string where of query
	 * 
	 * @param stringWhere
	 * @param keysearch
	 * @param sMaTaiLieu
	 * @param sTenTaiLieu
	 * @param trangThai
	 * @return
	 */
	private StringBuilder getStrWhere(StringBuilder stringWhere, String keysearch, String sMaTaiLieu,
			String sTenTaiLieu, String trangThai) {
		if (keysearch != null) {

			stringWhere.append(" and (( LOWER(u.maTaiLieu) like :keysearch1) ");
			stringWhere.append(" or ( LOWER(u.tenTaiLieu) like :keysearch2) ");
			stringWhere.append(" or ( LOWER(u.ghiChu) like :keysearch3) )");
			;
		}

		if (sMaTaiLieu != null && (!"".endsWith(sMaTaiLieu))) {

			stringWhere.append(" and ( LOWER(u.maTaiLieu) like :sMaTaiLieu) ");
		}
		if (sTenTaiLieu != null && (!"".endsWith(sTenTaiLieu))) {

			stringWhere.append(" and ( LOWER(u.tenTaiLieu) like :sTenTaiLieu) ");
		}
		if (trangThai != null && (!"".endsWith(trangThai))) {
			trangThai = trangThai.toLowerCase();
			stringWhere.append(" and ( u.trangThai  = :trangThai)");
		}

		return stringWhere;
	}

	private Query SetParram(Query q, String stringWhere, String keysearch, String sTenTaiLieu, String sMaTaiLieu,
			String trangThai) {

		if (stringWhere.contains(":keysearch")) {
			keysearch = keysearch.toLowerCase();

			q.setParameter("keysearch1", '%' + keysearch + '%');
			q.setParameter("keysearch2", '%' + keysearch + '%');
			q.setParameter("keysearch3", '%' + keysearch + '%');
		}
		if (stringWhere.contains(":sTenTaiLieu")) {
			sTenTaiLieu = sTenTaiLieu.toLowerCase();

			q.setParameter("sTenTaiLieu", '%' + sTenTaiLieu + '%');
		}
		if (stringWhere.contains(":sMaTaiLieu")) {
			sMaTaiLieu = sMaTaiLieu.toLowerCase();

			q.setParameter("sMaTaiLieu", '%' + sMaTaiLieu + '%');
		}
		if (stringWhere.contains(":trangThai")) {
			boolean trangThaiBool = true;
			if ("0".equals(trangThai)) {
				trangThaiBool = false;
			}
			q.setParameter("trangThai", trangThaiBool);
		}

		return q;
	}

	@Override
	public boolean isExistById(int id) {

		try {
			TaiLieuEntity entity = taiLieuEntityRepository.findById(id).orElse(null);

			if (entity != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return false;
	}

//	@Override
//	public boolean isExistByMa(String maTaiLieu, Integer id) {
//		// TODO Auto-generated method stub
//		boolean b = false;
//		try {
//			b = taiLieuEntityRepository.existsByMa(maTaiLieu, id);
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			entityManager.close();
//		}
//		return b;
//	}

}
