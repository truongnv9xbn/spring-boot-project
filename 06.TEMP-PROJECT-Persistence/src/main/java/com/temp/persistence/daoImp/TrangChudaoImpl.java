package com.temp.persistence.daoImp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.MessageDTO;
import com.temp.model.TrangChuBDTO;
import com.temp.model.TrangChuDTO;
import com.temp.persistance.entity.TrangChuEntity;
import com.temp.persistence.dao.TrangChuDao;
import com.temp.persistence.responsitories.TrangChuEntityRepository;
import com.temp.utils.ObjectMapperUtils;

@Component
public class TrangChudaoImpl implements TrangChuDao{
	
	private TrangChuEntityRepository trangChuEntityRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	public TrangChudaoImpl(TrangChuEntityRepository trangChuEntityRepository) {
		this.trangChuEntityRepository = trangChuEntityRepository;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public TrangChuBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai) {
		try {
			TrangChuBDTO result = null;

			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + TrangChuEntity.class.getName() + " u ";
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + TrangChuEntity.class.getName() + " u ");
			String stringOderby = " order by u." + keySort;

			// generate where
			stringWhere = this.getStrWhereAll(stringWhere, trangThai);

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby);

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby;
			List<TrangChuEntity> lst = entityManager.createQuery(stringQuery).getResultList();
			
			if (lst != null) {
				
				result = new TrangChuBDTO();
				
				Query qCount = entityManager.createQuery(stringQueryCount.toString());
				result.totalElement = (long) qCount.getResultList().get(0);
				// trả về 1 page cuối cùng
				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize)); // ép kiểu sang int
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;
				
				result.lstMessage = ObjectMapperUtils.mapAll(lst, MessageDTO.class);
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return new TrangChuBDTO();
	}
	
	private String getStrOrderBy(boolean desc, String stringOderby) {
		if (desc) {
			stringOderby = stringOderby + " desc";
		} else {
			stringOderby = stringOderby + " asc";
		}
		return stringOderby;
	}

	private StringBuilder getStrWhereAll(StringBuilder stringWhere, String trangThai) {
		if (trangThai != null && (!"".endsWith(trangThai))) {
			trangThai = trangThai.toLowerCase();
			stringWhere.append(" and ( u.trangThai  = " + trangThai + ")");
		}

		return stringWhere;
	}

	@Override
	public boolean addOrUpdate(TrangChuDTO dto) {
		try {
			TrangChuEntity entity = new ModelMapper().map(dto, TrangChuEntity.class);
			TrangChuEntity tmp = trangChuEntityRepository.save(entity);
			if (tmp != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return false;
	}
}
