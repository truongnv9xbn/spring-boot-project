package com.temp.persistence.daoImp.NguoiDung;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.LkChucNangNhomNguoiBDTO;
import com.temp.model.NguoiDung.LkChucNangNhomNguoiDTO;
import com.temp.model.file.ChucNangUserBDTO;
import com.temp.model.file.ChucNangUserDto;
import com.temp.persistance.entity.NguoiDung.LkChucNangNhomNguoiEntity;
import com.temp.persistance.entity.NguoiDung.QtChucNangChiTietEntity;
import com.temp.persistance.entity.NguoiDung.QtChucNangEntity;
import com.temp.persistence.dao.NguoiDung.LkChucNangNhomNguoiDao;
import com.temp.persistence.responsitories.NguoiDung.LkChucNangNguoiEntityRepository;
import com.temp.persistence.responsitories.NguoiDung.LkChucNangNhomNguoiEntityRepository;
import com.temp.persistence.responsitories.NguoiDung.LkNguoiDungNhomEntityRepository;
import com.temp.persistence.responsitories.NguoiDung.QtChucNangEntityRepository;
import com.temp.utils.ObjectMapperUtils;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Component
@Primary
public class LkChucNangNhomNguoiEntitydaoImp implements LkChucNangNhomNguoiDao {

	private LkChucNangNhomNguoiEntityRepository lkChucNangNhomNguoiEntityRepository;
	private LkChucNangNguoiEntityRepository lkChucNangNguoiDungRepository;

	private LkNguoiDungNhomEntityRepository lkNhomNguoiDungRepository;

	private QtChucNangEntityRepository chucNangRepository;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	public LkChucNangNhomNguoiEntitydaoImp(LkChucNangNhomNguoiEntityRepository lkChucNangNhomNguoiEntityRepository,
			LkChucNangNguoiEntityRepository lkChucNangNguoiDungRepository,
			QtChucNangEntityRepository chucNangRepository, LkNguoiDungNhomEntityRepository lkNhomNguoiDungRepository) {
		this.lkChucNangNguoiDungRepository = lkChucNangNguoiDungRepository;
		this.chucNangRepository = chucNangRepository;
		this.lkChucNangNhomNguoiEntityRepository = lkChucNangNhomNguoiEntityRepository;
		this.lkNhomNguoiDungRepository = lkNhomNguoiDungRepository;
	}

	@Override
	public LkChucNangNhomNguoiBDTO listLkChucNangNhomNguois(String keysearch, Integer pageNo, Integer pageSize,
			String keySort, boolean desc) {

		try {
			LkChucNangNhomNguoiBDTO result = null;

			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + LkChucNangNhomNguoiEntity.class.getName() + " u ";
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + LkChucNangNhomNguoiEntity.class.getName() + " u ");
			String stringOderby = " order by u." + keySort;

			// generate where
			stringWhere = this.getStrWhere(stringWhere, keysearch);

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby);

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby;
			List<LkChucNangNhomNguoiEntity> lstLkChucNangNhomNguoi = entityManager.createQuery(stringQuery)
					.setFirstResult(pageNo * pageSize).setMaxResults(pageSize).getResultList();
			// láº¥y káº¿t quáº£
			if (lstLkChucNangNhomNguoi != null) {

				result = new LkChucNangNhomNguoiBDTO();

				// add condition count item
				stringQueryCount.append(stringWhere);

				// get count total item
				result.totalElement = (long) entityManager.createQuery(stringQueryCount.toString()).getResultList().get(0);

				// page cuá»‘i cÃ¹ng.
				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize));
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;

				result.listDto = ObjectMapperUtils.mapAll(lstLkChucNangNhomNguoi, LkChucNangNhomNguoiDTO.class);
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return new LkChucNangNhomNguoiBDTO();
	}

	@Override
//	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
	public boolean updateNhomNguoiDungChucNang(List<LkChucNangNhomNguoiDTO> listDto, int id) {

		try {
			List<LkChucNangNhomNguoiEntity> entitys = ObjectMapperUtils.mapAll(listDto, LkChucNangNhomNguoiEntity.class);

			// lkChucNangNhomNguoiEntityRepository.deleteById(id);

			List<LkChucNangNhomNguoiEntity> tmp = lkChucNangNhomNguoiEntityRepository.saveAll(entitys);

			if (tmp != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return false;
	}

	private String getStrOrderBy(boolean desc, String stringOderby) {
		if (desc) {
			stringOderby = stringOderby + " desc";
		} else {
			stringOderby = stringOderby + " asc";
		}
		return stringOderby;
	}

	/***
	 * Get string where of query
	 * 
	 * @param stringWhere
	 * @param keysearch
	 * @param sMaTinhThanh
	 * @param sTenTinhThanh
	 * @param trangThai
	 * @return
	 */
	private StringBuilder getStrWhere(StringBuilder stringWhere, String keysearch) {
		if (keysearch != null) {
			keysearch = keysearch.toLowerCase();

			stringWhere.append(" and (( LOWER(u.chucNangId) like '%" + keysearch + "%') ");
			stringWhere.append(" or ( LOWER(u.nhomNguoiDungId) like '%" + keysearch + "%') )");
		}

		return stringWhere;
	}

	@Override
	public boolean deleteLkChucNangNhomNguoi(int nhomNguoiDungId) {
		try {
			if (nhomNguoiDungId > 0) {
				Query query = entityManager.createQuery(" delete from " + LkChucNangNhomNguoiEntity.class.getName()
						+ " c WHERE (c.nhomNguoiDungId = :nhomNguoiDungId ) ");
				int c = query.setParameter("nhomNguoiDungId", nhomNguoiDungId).executeUpdate();

				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return false;
	}

	@Override
	public boolean isLkChucNangNhomNguoiExistAdd(int nhomNguoiDungId, int chucNangId) {

		try {
			String queryStr = "SELECT c FROM " + LkChucNangNhomNguoiEntity.class.getName()
					+ " c WHERE (c.nhomNguoiDungId !=:nhomNguoiDungId ) and (c.chucNangId =:chucNangId)";
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("nhomNguoiDungId", nhomNguoiDungId);
			query.setParameter("chucNangId", chucNangId);
			List<LkChucNangNhomNguoiEntity> list = query.getResultList();

			if (list.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return false;
	}

	@Override
	public LkChucNangNhomNguoiDTO findByNhomNguoiDungId(int nhomNguoiDungId) {
		try {
			if (nhomNguoiDungId > 0) {
				String queryStr = "SELECT c FROM " + LkChucNangNhomNguoiEntity.class.getName()
						+ " c WHERE (c.nhomNguoiDungId = :nhomNguoiDungId )";
				Query query = entityManager.createQuery(queryStr);
				query.setParameter("nhomNguoiDungId", nhomNguoiDungId);
				List<LkChucNangNhomNguoiEntity> list = query.getResultList();
				if (list.size() > 0) {
					return (LkChucNangNhomNguoiDTO) list;

				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return null;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Integer> listByNguoiDungId(int nhomNguoiDungId) {
		try {
			String StringQuery = "SELECT c  FROM " + LkChucNangNhomNguoiEntity.class.getName()
					+ " c where c.nhomNguoiDungId = " + nhomNguoiDungId;

			Query query = entityManager.createQuery(StringQuery);
			List<LkChucNangNhomNguoiEntity> list = query.getResultList();

			//System.out.println(list);

			List<Integer> results = list.stream().map(LkChucNangNhomNguoiEntity::getChucNangId)
					.collect(Collectors.toList());

			return results;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return null;
	}

	public ChucNangUserBDTO menuGenerateAdmin(boolean isPhanQuyen) {
		ChucNangUserBDTO result;
		try {
			result = new ChucNangUserBDTO();
			// select các chức nang generate menu được phân quyền
			StringBuilder whereChucNangMenu = new StringBuilder(" where ");

			String orderMenu = " ORDER BY u.khoaChaId, c.tenChucNang  ASC";

			String selecChucNangMenu = "Select DISTINCT u.id, u.tenChucNang, u.khoaChaId , u.actionId, u.icon,"
					+ " u.menu, u.nguoiTaoId, u.deleted, u.trangThai, c.vueRoute, c.apiRoute, c.tenChucNang, u.thuTu" + " From "
					+ QtChucNangEntity.class.getName() + " u left join " + QtChucNangChiTietEntity.class.getName()
					+ " c on u.actionId  = c.id";
			
			if(isPhanQuyen) {
				whereChucNangMenu.append("(u.trangThai = 1) and (c.trangThai = 1)");
			}else {
				whereChucNangMenu.append(" (u.menu = 1) and (u.trangThai = 1) and (c.trangThai = 1)");
			}

			String QuerySQLMenu = selecChucNangMenu + whereChucNangMenu + orderMenu;

			List<Object[]> lstChucNangMenu = entityManager.createQuery(QuerySQLMenu).getResultList();

			if (lstChucNangMenu != null && !lstChucNangMenu.isEmpty()) {
				result.setLstChucNangMenu(lstChucNangMenu.stream().map(ChucNangUserDto::new).collect(Collectors.toList()));

				// return ObjectMapperUtils.mapAll(lstChucNang, ChucNangDTO.class);
			}
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return  new ChucNangUserBDTO();
	}
	
	public List<String> getAllChucNangChiTiet() {
		try {
			List<String> lstChucNangChiTiet = null;
			String strQuery = "SELECT DISTINCT API_ROUTE FROM QT_CHUC_NANG_CHI_TIET WHERE TRANG_THAI = 1";
			lstChucNangChiTiet = (List<String>) entityManager.createNativeQuery(strQuery).getResultList();
			return lstChucNangChiTiet;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<String>();
	}
	
	public ChucNangUserBDTO menuGenerate(int nguoiDungId, boolean isPhanQuyen) {
		try {
			ChucNangUserBDTO result = new ChucNangUserBDTO();
			List<Integer> lstChucNangNhomNguoiDung = null;
			List<Integer> lstLkChucNangNguoi = new ArrayList<Integer>();
			List<BigDecimal> lstLkChucNang = null;
			String strQuery = "WITH results(ID, khoa_cha_id, ten_chuc_nang) AS (SELECT ID, khoa_cha_id, ten_chuc_nang"
					+ " FROM QT_CHUC_NANG"
					+ " WHERE ID IN (SELECT CHUC_NANG_ID FROM LK_CHUC_NANG_NGUOI WHERE NGUOI_DUNG_ID = " + nguoiDungId + ")"
					+ " OR ID IN (SELECT CHUC_NANG_ID FROM LK_CHUC_NANG_NHOM_NGUOI"
					+ " WHERE NHOM_NGUOI_DUNG_ID IN (SELECT NHOM_NGUOI_DUNG_ID FROM LK_NGUOI_DUNG_NHOM lk JOIN QT_NHOM_NGUOI_DUNG qtn ON qtn.ID = lk.NHOM_NGUOI_DUNG_ID WHERE NGUOI_DUNG_ID = "
					+ nguoiDungId + " AND (ngay_het_han IS NULL OR TO_CHAR(NGAY_HET_HAN, 'YYYYMMDDHHmm') > TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMMDDHHmm'))))" + " UNION ALL SELECT t.ID, t.khoa_cha_id, t.ten_chuc_nang FROM QT_CHUC_NANG t"
					+ " INNER JOIN results r ON r.khoa_cha_id = t.ID WHERE t.id <> t.KHOA_CHA_ID)"
					+ " SELECT DISTINCT ID FROM results ORDER BY ID";
			lstLkChucNang = (List<BigDecimal>) entityManager.createNativeQuery(strQuery).getResultList();
			if (lstLkChucNang != null && lstLkChucNang.size() > 0) {
				lstLkChucNangNguoi.clear();
				for (BigDecimal item : lstLkChucNang) {
					lstLkChucNangNguoi.add(item.intValue());
				}
			}
			lstChucNangNhomNguoiDung = null;
			if (lstChucNangNhomNguoiDung == null && (lstLkChucNangNguoi == null || lstLkChucNangNguoi.isEmpty())) {
				return null;
			}

			// select các chức nang generate menu được phân quyền
			StringBuilder whereChucNangMenu = new StringBuilder(" where ");
			StringBuilder whereChucNangQuyen = new StringBuilder(" where ");
			String orderMenu = " ORDER BY u.khoaChaId, c.tenChucNang  ASC";

			String selecChucNangMenu = "Select DISTINCT u.id, u.tenChucNang, u.khoaChaId , u.actionId, u.icon,"
					+ " u.menu, u.nguoiTaoId, u.deleted, u.trangThai, c.vueRoute, c.apiRoute, c.tenChucNang, u.thuTu" + " From "
					+ QtChucNangEntity.class.getName() + " u left join " + QtChucNangChiTietEntity.class.getName()
					+ " c on u.actionId  = c.id";

			String selecURIAPI = "Select DISTINCT c.apiRoute" + " From " + QtChucNangEntity.class.getName()
					+ " u left join " + QtChucNangChiTietEntity.class.getName() + " c on u.actionId  = c.id";
			
			
			// if call phan quyen 
			if(isPhanQuyen) {
				whereChucNangMenu.append(" ((u.trangThai = 1) and (c.trangThai = 1)and ( u.id in "
						+ this.megerChucNang(lstChucNangNhomNguoiDung, lstLkChucNangNguoi) + "))");
			}else {
				whereChucNangMenu.append(" ((u.menu = 1) and (u.trangThai = 1) and (c.trangThai = 1)and ( u.id in "
						+ this.megerChucNang(lstChucNangNhomNguoiDung, lstLkChucNangNguoi) + "))");
			}

			

			whereChucNangQuyen.append(" ((u.trangThai = 1) and (c.trangThai = 1)and ( u.id in "
					+ this.megerChucNang(lstChucNangNhomNguoiDung, lstLkChucNangNguoi) + "))");

			String QuerySQLMenu = selecChucNangMenu + whereChucNangMenu + orderMenu;

			String QuerySQLQuyen = selecURIAPI + whereChucNangQuyen;
			List<Object[]> lstChucNangMenu = entityManager.createQuery(QuerySQLMenu).getResultList();
			List<String> lstChucNangQuyen = (List<String>) entityManager.createQuery(QuerySQLQuyen).getResultList();
			if (lstChucNangMenu != null && !lstChucNangMenu.isEmpty()) {
				result.setLstChucNangMenu(lstChucNangMenu.stream().map(ChucNangUserDto::new).collect(Collectors.toList()));

				// return ObjectMapperUtils.mapAll(lstChucNang, ChucNangDTO.class);
			}
			if (lstChucNangQuyen != null && !lstChucNangQuyen.isEmpty()) {
				result.setLstChucNangQuyen(lstChucNangQuyen);
			}

			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return  new ChucNangUserBDTO();
	}

	private StringBuilder getStrWhereNhomNguoiDung(StringBuilder stringWhere, List<Integer> keysearch) {
		if (keysearch != null && keysearch.size() > 0) {
			String sqlIn = "(";
			for (Integer nhomNguoIDung : keysearch) {

				sqlIn += nhomNguoIDung + ", ";
			}
			sqlIn = sqlIn.substring(0, sqlIn.length() - 2);
			sqlIn += ")";
			//System.out.println(sqlIn);
			stringWhere.append(" or ( u.nhomNguoiDungId in " + sqlIn + " )");
		}
		return stringWhere;
	}

	private String megerChucNang(List<Integer> lstCnNhomND, List<Integer> lstCnNguoiDung) {
		List<Integer> tempRst = null;
		if (lstCnNhomND == null && lstCnNguoiDung != null && !lstCnNguoiDung.isEmpty()) {
			return genQueryIn(lstCnNguoiDung);
		} else if (lstCnNguoiDung == null && lstCnNhomND != null && !lstCnNhomND.isEmpty()) {
			return genQueryIn(lstCnNhomND);
		} else if (lstCnNguoiDung == null && lstCnNhomND == null) {
			return null;
		} else {
			List<Integer> lstNotInlstNguoDung = lstCnNhomND.stream().filter(x -> !lstCnNguoiDung.contains(x))
					.collect(Collectors.toList());
			lstCnNguoiDung.addAll(lstNotInlstNguoDung);
			tempRst = lstCnNguoiDung;
			return genQueryIn(tempRst);
		}
	}

	private String genQueryIn(List<Integer> inLst) {
		String sqlIn = "(";
		for (Integer nhomNguoIDung : inLst) {

			sqlIn += nhomNguoIDung + ", ";
		}
		sqlIn = sqlIn.substring(0, sqlIn.length() - 2);
		sqlIn += ")";

		return sqlIn;
	}
}