package com.temp.persistence.daoImp;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.MessageBDTO;
import com.temp.model.MessageDTO;
import com.temp.persistance.entity.MessageEntity;
import com.temp.persistence.dao.MessageDao;
import com.temp.persistence.responsitories.MessageEntityRepository;
import com.temp.utils.ObjectMapperUtils;

@Component
public class MessagedaoImpl implements MessageDao{
	
	private MessageEntityRepository messageEntityRepository;
	
	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	public MessagedaoImpl(MessageEntityRepository messageEntityRepository) {
		this.messageEntityRepository = messageEntityRepository;
	}
	
	@SuppressWarnings({ "unchecked", "unused" })
	@Override
	public MessageBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai) {
		try {
			MessageBDTO result = null;

			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + MessageEntity.class.getName() + " u ";
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + MessageEntity.class.getName() + " u ");
			String stringOderby = " order by u." + keySort;

			// generate where
			stringWhere = this.getStrWhereAll(stringWhere, trangThai);

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby);

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby;
			List<MessageEntity> lst = entityManager.createQuery(stringQuery).getResultList();
			
			if (lst != null) {
				
				result = new MessageBDTO();
				
				Query qCount = entityManager.createQuery(stringQueryCount.toString());
				result.totalElement = (long) qCount.getResultList().get(0);
				// trả về 1 page cuối cùng
				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize)); // ép kiểu sang int
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;
				
				result.lstMessage = ObjectMapperUtils.mapAll(lst, MessageDTO.class);
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return new MessageBDTO();
	}
	
	private String getStrOrderBy(boolean desc, String stringOderby) {
		if (desc) {
			stringOderby = stringOderby + " desc";
		} else {
			stringOderby = stringOderby + " asc";
		}
		return stringOderby;
	}

	private StringBuilder getStrWhereAll(StringBuilder stringWhere, String trangThai) {
		if (trangThai != null && (!"".endsWith(trangThai))) {
			trangThai = trangThai.toLowerCase();
			stringWhere.append(" and ( u.trangThai  = " + trangThai + ")");
		}

		return stringWhere;
	}

	@Override
	public boolean addOrUpdate(MessageDTO dto) {
		try {
			MessageEntity entity = new ModelMapper().map(dto, MessageEntity.class);
			MessageEntity tmp = messageEntityRepository.save(entity);
			if (tmp != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return false;
	}
}
