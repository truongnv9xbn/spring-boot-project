package com.temp.persistence.daoImp.DanhMuc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.DropDownDTO;
import com.temp.model.DanhMuc.QuocTichBDTO;
import com.temp.model.DanhMuc.QuocTichDTO;
import com.temp.persistance.entity.DanhMuc.DmQuocTichEntity;
import com.temp.persistence.dao.DanhMuc.DmQuocTichDao;
import com.temp.persistence.responsitories.DanhMuc.DmQuocTichEntityRepository;
import com.temp.utils.ObjectMapperUtils;

/**
 * 
 * @author TuanNV
 *
 */
@Component // đây là 1 bean
public class DmQuocTichEntitydaoImp implements DmQuocTichDao {
	@Autowired
	private DmQuocTichEntityRepository dmQuocTichEntityRepository;

	@Autowired
	private EntityManager entityManager;

	@Autowired
	public DmQuocTichEntitydaoImp(DmQuocTichEntityRepository dmQuocTichEntityRepository) {
		this.dmQuocTichEntityRepository = dmQuocTichEntityRepository;
	}

	@Override // @Override lại hàm của thằng cha
	// dây là hàm lấy đối tượng theo id
	public QuocTichDTO findById(int id) {
		try {
			Optional<DmQuocTichEntity> eQuocTichEntity = dmQuocTichEntityRepository.findById(id); // kiểm tra giá trik có
																									// null hay không?
			if (eQuocTichEntity.isPresent()) { // nếu không trống
				DmQuocTichEntity entity = eQuocTichEntity.get();
				return ObjectMapperUtils.map(entity, QuocTichDTO.class); // trả về hàm conver từ entity sang DTO

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public QuocTichBDTO listQuocTichs(String keysearch, String sTenQuocTich, String sMaQuocTich, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) { // đây là 1 danh sách có các param
																				// truyền lên
		try {
			QuocTichBDTO result = null; // tạo đối tượng

			StringBuilder stringWhere = new StringBuilder(" where 1=1"); // querry với điều kiện luôn đúng
			String stringQuery = "Select u From " + DmQuocTichEntity.class.getName() + " u "; // seclect bảng
																								// DmQuocTichEntity
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + DmQuocTichEntity.class.getName() + " u "); // tổng bản ghi
			String stringOderby = " order by u." + keySort; // xắp xếp

			// generate where
			stringWhere = this.getStrWhere(stringWhere, keysearch, sMaQuocTich, sTenQuocTich, trangThai); // gọi đến hàm
																											// getStrWhere
																											// có các param
																											// tương ứng

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby); // gọi đến xắp xếp

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby; // ghép sql
			Query q = entityManager.createQuery(stringQuery);
			q = SetParram(q, stringQuery, keysearch, sMaQuocTich, sTenQuocTich);

			List<DmQuocTichEntity> lstQuocTich = q.setFirstResult(pageNo * pageSize).setMaxResults(pageSize)
					.getResultList(); // entityManager để tạo sql trả về 1 danh sách
			if (lstQuocTich != null) { // check cai list khac null

				result = new QuocTichBDTO(); // tạo đối tượng

				// add condition count item
				stringQueryCount.append(stringWhere); // gọi đến câu sql stringWhere

				// get count total item
				// tổng bản ghi ép kiếu sang long
				Query qCount = entityManager.createQuery(stringQueryCount.toString());
				qCount = SetParram(qCount, stringQueryCount.toString(), keysearch, sMaQuocTich, sTenQuocTich);
				result.totalElement = (long) qCount.getResultList().get(0);
				// trả về 1 page cuối cùng
				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize)); // ép kiểu sang int
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;

				result.lstQuocTich = ObjectMapperUtils.mapAll(lstQuocTich, QuocTichDTO.class); // conver danh sách
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return new QuocTichBDTO();
	}

	@Override
	public QuocTichBDTO listAll(String keySort, boolean desc, String trangThai) {
		try {
			QuocTichBDTO result = null;

			StringBuilder stringWhere = new StringBuilder(" where 1=1");
			String stringQuery = "Select u From " + DmQuocTichEntity.class.getName() + " u ";
			StringBuilder stringQueryCount = new StringBuilder(
					"Select count(u.id) From " + DmQuocTichEntity.class.getName() + " u ");
			String stringOderby = " order by u." + keySort;

			// generate where
			stringWhere = this.getStrWhereAll(stringWhere, trangThai);

			// generate oder by
			stringOderby = this.getStrOrderBy(desc, stringOderby);

			// TODO querryquery = ;
			stringQuery = stringQuery + stringWhere + stringOderby;
			List<DmQuocTichEntity> lst = entityManager.createQuery(stringQuery).getResultList();
			// l?y k?t qu?
			if (lst != null) {

				result = new QuocTichBDTO();
				result.lstQuocTich = ObjectMapperUtils.mapAll(lst, QuocTichDTO.class);
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return new QuocTichBDTO();
	}

	@Override
	public boolean addOrUpdateQuocTich(QuocTichDTO dto) { // add và cập nhật
		try {
			DmQuocTichEntity entity = new ModelMapper().map(dto, DmQuocTichEntity.class); // conver
			DmQuocTichEntity tmp = dmQuocTichEntityRepository.save(entity); // có thứ viện tự gen
			if (tmp != null) { // check khác null trả về true còn không ngược lại
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return false;

	}

	@Override
	public boolean deleteQuocTichById(int id) { // xóa theo id
		try {
			if (id > 0) {

				dmQuocTichEntityRepository.deleteById(id); // tự gen câu lệnh xóa truyền vào pram là id

				Optional<DmQuocTichEntity> obj = dmQuocTichEntityRepository.findById(id); // check id không trống
				if (!obj.isPresent()) {
					return true;
				}
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isQuocTichExistUpdate(int id, String maQuocTich) { // check cập nhật
		try {
			if (!StringUtils.isEmpty(maQuocTich)) {
				maQuocTich = maQuocTich.toLowerCase();
			}
			String queryStr = "SELECT c FROM " + DmQuocTichEntity.class.getName()
					+ " c WHERE  LOWER(c.maQuocTich) like :maQuocTich and c.id !=" + id + ""; // query lect bảng
																								// DmQuocTichEntity tìm theo
																								// mã
			// không phân biệt chữ hoa và thường theo id
			Query query = entityManager.createQuery(queryStr); // entityManager tạo sql
			query.setParameter("maQuocTich", maQuocTich); // set parameter
			List<DmQuocTichEntity> listQuocTich = query.getResultList(); // trả về 1 list

			if (listQuocTich.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean isQuocTichExistAdd(String maQuocTich) { // check hàm add
		try {
			if (!StringUtils.isEmpty(maQuocTich)) {
				maQuocTich = maQuocTich.toLowerCase();
			}
			String queryStr = "SELECT c FROM " + DmQuocTichEntity.class.getName()
					+ " c WHERE  LOWER(c.maQuocTich) = :maQuocTich"; // select bảng DmQuocTichEntity ở mã quốc tịch không
																		// phân biệt chữ hoa
			// và thường
			Query query = entityManager.createQuery(queryStr);
			query.setParameter("maQuocTich", maQuocTich);
			List<DmQuocTichEntity> listQuocTich = query.getResultList(); // trả về list

			if (listQuocTich.size() > 0) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Get string order by of query
	 * 
	 * @param desc
	 * @param stringOderby
	 * @return String order by
	 */
	private String getStrOrderBy(boolean desc, String stringOderby) { // tạo hàm xắp xếp tăng giảm với 2 param
		if (desc) {
			stringOderby = stringOderby + " desc";
		} else {
			stringOderby = stringOderby + " asc";
		}
		return stringOderby;
	}

	/***
	 * Get string where of query
	 * 
	 * @param stringWhere
	 * @param keysearch
	 * @param sMaQuocTich
	 * @param sTenQuocTich
	 * @param trangThai
	 * @return
	 */
	private StringBuilder getStrWhere(StringBuilder stringWhere, String keysearch, String sMaQuocTich,
			String sTenQuocTich, String trangThai) {
		if (keysearch != null) { // khác null

			stringWhere.append(" and (( LOWER(u.maQuocTich) like :keysearch1) "); // và tìm kiếm
			stringWhere.append(" or ( LOWER(u.tenQuocTich) like :keysearch2) "); // hoặc
			stringWhere.append(" or ( LOWER(u.ghiChu) like :keysearch3) )"); // hoặc
		} // thêm các điều kiện tìm kiếm

		if (sMaQuocTich != null && (!"".endsWith(sMaQuocTich))) { // khác null và không trống

			stringWhere.append(" and ( LOWER(u.maQuocTich) like :sMaQuocTich ) ");
		}
		if (sTenQuocTich != null && (!"".endsWith(sTenQuocTich))) {

			stringWhere.append(" and ( LOWER(u.tenQuocTich) like :sTenQuocTich) ");
		}
		if (trangThai != null && (!"".endsWith(trangThai))) {
			trangThai = trangThai.toLowerCase();
			stringWhere.append(" and ( u.trangThai  = " + trangThai + ")");
		}

		return stringWhere;
	}

	private Query SetParram(Query q, String stringWhere, String keysearch, String sMaQuocTich, String sTenQuocTich) {

		if (stringWhere.contains(":keysearch")) {
			keysearch = keysearch.toLowerCase(); // không phân biệt chữ hoa và thường
			q.setParameter("keysearch1", '%' + keysearch + '%');
			q.setParameter("keysearch2", '%' + keysearch + '%');
			q.setParameter("keysearch3", '%' + keysearch + '%');
		}
		if (stringWhere.contains(":sMaQuocTich")) {
			sMaQuocTich = sMaQuocTich.toLowerCase();
			q.setParameter("sMaQuocTich", '%' + sMaQuocTich + '%');
		}
		if (stringWhere.contains(":sTenQuocTich")) {
			sTenQuocTich = sTenQuocTich.toLowerCase();
			q.setParameter("sTenQuocTich", '%' + sTenQuocTich + '%');
		}

		return q;
	}

	private StringBuilder getStrWhereAll(StringBuilder stringWhere, String trangThai) {
		if (trangThai != null && (!"".endsWith(trangThai))) {
			trangThai = trangThai.toLowerCase();
			stringWhere.append(" and ( u.trangThai  = " + trangThai + ")");
		}

		return stringWhere;
	}

	@Override
	public boolean isExistById(int id) { // check id

		try {
			DmQuocTichEntity entity = dmQuocTichEntityRepository.findById(id).orElse(null); // luôn đảm bảo id tồn tại

			if (entity != null) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}

		return false;
	}

	@Override
	public List<DropDownDTO> DropDownQuocTich() {
		// query.setParameter("maQuocTich", maQuocTich.toLowerCase());
		List<Object[]> listQuocTich;
		try {
			String queryStr = "SELECT c.id, c.tenQuocTich FROM " + DmQuocTichEntity.class.getName()
					+ " c WHERE c.trangThai = 1"; // select bảng DmQuocTichEntity ở mã quốc tịch không phân biệt chữ hoa
			// và thường
			Query query = entityManager.createQuery(queryStr);
			listQuocTich = query.getResultList();

			if (listQuocTich.isEmpty()) {
				return new ArrayList<>();
			}
			return listQuocTich.stream().map(DropDownDTO::new).collect(Collectors.toList());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return null;
	}

	@Override
	public int getIdByName(String name) {
		int result = 0;
		try {
			Query query = entityManager.createQuery("SELECT c.id FROM " + DmQuocTichEntity.class.getName()
					+ " c WHERE TRIM(c.tenQuocTich) =:name ");
			query.setParameter("name", name);
			
			List<Object> lst = query.getResultList();
			Object item = null;
			if (lst.size() > 0) {
				item = lst.get(0);
				result = new Integer(item.toString());
			} else {
				return result;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}
		return result;
	}

}
