package com.temp.persistence.daoImp.BieuMauBaoCao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.BieuMauBaoCao.BmBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.persistance.entity.BieuMauBaoCao.BmBaoCaoEntity;
import com.temp.persistance.entity.BieuMauBaoCao.BmBaoCaoLichSuEntity;
import com.temp.persistence.dao.BieuMauBaoCao.BieuMauBaoCaoDao;
import com.temp.utils.ObjectMapperUtils;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Component
public class BieuMauBaoCaoEntitydaoImp implements BieuMauBaoCaoDao {
	
	@Autowired
	private EntityManager entityManager;

	@Override
	public BmBaoCaoBDTO getlist(
			Integer pageNo,
			Integer pageSize,
			boolean desc, 
			String strFilter,
			String tenBaoCao,
			String canCuPhapLy) {
		// TODO Auto-generated method stub
		BmBaoCaoBDTO BDTO = new BmBaoCaoBDTO();
		StringBuilder queryString = new StringBuilder("Select u From "+BmBaoCaoEntity.class.getName()+" u ");
		StringBuilder queryWhereString = new StringBuilder(" Where 1=1 and u.kieuBaoCao = 0 And u.id IN (SELECT bm.bmId FROM " 
		+ BmBaoCaoLichSuEntity.class.getName() + " bm WHERE bm.suDung = 1 and u.trangThai = 1)");
		//
		 
		String queryCountString =  "SELECT COUNT(u.id) From "
				+ BmBaoCaoEntity.class.getName()
				+ " u ";
		String queryOrderString = " Order by u.id desc";
		
		try {
			if(strFilter.length()>0) {
				queryWhereString.append(" And UPPER(u.tenBaoCao) like :strFilter");
				
			}
			if(tenBaoCao.length()>0) {
				queryWhereString.append(" And UPPER(u.tenBaoCao) like :tenBaoCao");
				
			}
			if(canCuPhapLy.length()>0) {
				queryWhereString.append(" And UPPER(u.canCuPhapLy) like :canCuPhapLy");
				
			}
			
			Query query = entityManager.createQuery(queryString.toString()
															+queryWhereString.toString()
															+queryOrderString);
			
			Query cQuery = entityManager.createQuery(queryCountString
														+queryWhereString.toString());
			
			if(strFilter.length()>0) {
				query.setParameter("strFilter", "%"+strFilter.toUpperCase()+"%");
				cQuery.setParameter("strFilter","%"+strFilter.toUpperCase()+"%");
			}
			
			if(tenBaoCao.length()>0) {
				query.setParameter("tenBaoCao", "%"+tenBaoCao.toUpperCase()+"%");
				cQuery.setParameter("tenBaoCao", "%"+tenBaoCao.toUpperCase()+"%");
			}
			if(canCuPhapLy.length()>0) {
				query.setParameter("canCuPhapLy", "%"+canCuPhapLy.toUpperCase()+"%");
				cQuery.setParameter("canCuPhapLy", "%"+canCuPhapLy.toUpperCase()+"%");
			}
			 
			List<BmBaoCaoEntity> lst = query.getResultList();
			 
			if (lst != null) {
				// get count total item
				BDTO.totalElement = (Long) cQuery.getResultList().get(0);
				BDTO.pageNo = pageNo + 1;
				BDTO.pageSize = pageSize;
				BDTO.lstBmBaoCao =  ObjectMapperUtils.mapAll(lst, BmBaoCaoDTO.class);
				return BDTO;
			}
		} catch (Exception e) {
			// TODO: handl exception
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return BDTO;
	}

	@Override
	public boolean save(BmBaoCaoDTO dto) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean update(BmBaoCaoDTO dto) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean deleteById(int id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isExistById(int id) throws Exception {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public BmBaoCaoDTO findById(int i) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
