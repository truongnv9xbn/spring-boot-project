package com.temp.persistence.daoImp.BieuMauBaoCao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDongDTO;
import com.temp.persistance.entity.BieuMauBaoCao.BcBaoCaoGtDongEntity;
import com.temp.persistence.dao.BieuMauBaoCao.BcBaoCaoGtDongDao;
import com.temp.persistence.responsitories.BieuMauBaoCao.BcBaoCaoGtDongEntityRepository;
import com.temp.utils.ObjectMapperUtils;

@Component
public class BcBaoCaoGtDongDaoImpl implements BcBaoCaoGtDongDao {

    /**
     * Bao cao dong repository
     */
    private BcBaoCaoGtDongEntityRepository bcBaoCaoGtDongEntityRepository;
    
    @Autowired
    private EntityManager entityManager;

    @Autowired
    public BcBaoCaoGtDongDaoImpl(BcBaoCaoGtDongEntityRepository bcBaoCaoGtDongEntityRepository) {
		super();
		this.bcBaoCaoGtDongEntityRepository = bcBaoCaoGtDongEntityRepository;
    }

    @Override
    public boolean saveNoiDungBaoCaoDynamicDao(BcBaoCaoGtDongDTO dataBaoCaoDynamic)
	    throws Exception {
    	BcBaoCaoGtDongEntity outputRs = new BcBaoCaoGtDongEntity();
    	try {
		BcBaoCaoGtDongEntity entityData = new BcBaoCaoGtDongEntity();
		String queryStr = "SELECT u FROM " + BcBaoCaoGtDongEntity.class.getName()
				+ " u  WHERE u.bcThanhVienId = :bcThanhVienId AND u.bmSheetHang = :bmSheetHang";
		Query query = entityManager.createQuery(queryStr);
		query.setParameter("bcThanhVienId", dataBaoCaoDynamic.getBcThanhVienId());
		query.setParameter("bmSheetHang", dataBaoCaoDynamic.getBmSheetHang());
		List<BcBaoCaoGtDongEntity> ObjectSheet = query.getResultList();
		if (!ObjectSheet.isEmpty() && ObjectSheet.size() > 0) {
		    entityData = ObjectSheet.get(0);
		    entityData.setGiaTri(dataBaoCaoDynamic.getGiaTri());
		}else {
			entityData = ObjectMapperUtils.map(dataBaoCaoDynamic, BcBaoCaoGtDongEntity.class);
		}
	
		// save gia tri bao cao dong.
		outputRs = this.bcBaoCaoGtDongEntityRepository.save(entityData);
    	}
    	catch(Exception e) {
    		
    	}
		finally {
			entityManager.close();
		}
		return (outputRs != null);
    }

    @Override
    public BcBaoCaoGtDongDTO getHangDong(int bcThanhVienId, int bmSheetHang) {
    	try {
    	BcBaoCaoGtDongEntity entityData = new BcBaoCaoGtDongEntity();
		String queryStr = "SELECT u FROM " + BcBaoCaoGtDongEntity.class.getName()
				+ " u  WHERE u.bcThanhVienId = :bcThanhVienId AND u.bmSheetHang = :bmSheetHang";
		Query query = entityManager.createQuery(queryStr);
		query.setParameter("bcThanhVienId", bcThanhVienId);
		query.setParameter("bmSheetHang", bmSheetHang);
		List<BcBaoCaoGtDongEntity> ObjectSheet = query.getResultList();
		if (!ObjectSheet.isEmpty() && ObjectSheet.size() > 0) {
		    entityData = ObjectSheet.get(0);
			return ObjectMapperUtils.map(entityData, BcBaoCaoGtDongDTO.class);
		}
    	}
    	catch(Exception e) {
    		
    	}
    	finally {
			entityManager.close();
		}
		return null;
    }
    

    public BcBaoCaoGtDongDTO getHangDongCifListCtck(int bmBaoCaoId, int bmSheetHang, List<Integer> ctckThongTinId) {
    	try {
    	BcBaoCaoGtDongEntity entityData = new BcBaoCaoGtDongEntity();
		String queryStr = "SELECT u FROM " + BcBaoCaoGtDongEntity.class.getName()
				+ " u  WHERE 1 = 1 ";
		if(bmBaoCaoId > 0) {
			queryStr += " and u.bmBaoCaoId = :bmBaoCaoId ";
		}
		if(bmSheetHang > 0) {
			queryStr += " and u.bmSheetHang = :bmSheetHang ";
		}
		if(ctckThongTinId != null && ctckThongTinId.size() > 0) {
			queryStr += " and u.ctckThongTinId in " + this.genWherein(ctckThongTinId);
		}
		Query query = entityManager.createQuery(queryStr);
		if(bmBaoCaoId > 0) {
			query.setParameter("bmBaoCaoId", bmBaoCaoId);
		}
		if(bmSheetHang > 0) {
			query.setParameter("bmSheetHang", bmSheetHang);
		}
//		if(ctckThongTinId > 0) {
//			query.setParameter("ctckThongTinId", ctckThongTinId);
//		}
		List<BcBaoCaoGtDongEntity> ObjectSheet = query.getResultList();
		if (!ObjectSheet.isEmpty() && ObjectSheet.size() > 0) {
		    entityData = ObjectSheet.get(0);
			return ObjectMapperUtils.map(entityData, BcBaoCaoGtDongDTO.class);
		}
    	}
    	catch(Exception e) {
    		
    	}finally {
			entityManager.close();
		}
		return null;
    }
    
    public BcBaoCaoGtDongDTO getHangDongCif(int bmBaoCaoId, int bmSheetHang, int ctckThongTinId) {
    	try {
    	BcBaoCaoGtDongEntity entityData = new BcBaoCaoGtDongEntity();
		String queryStr = "SELECT u FROM " + BcBaoCaoGtDongEntity.class.getName()
				+ " u  WHERE 1 = 1 ";
		if(bmBaoCaoId > 0) {
			queryStr += " and u.bmBaoCaoId = :bmBaoCaoId ";
		}
		if(bmSheetHang > 0) {
			queryStr += " and u.bmSheetHang = :bmSheetHang ";
		}
		if(ctckThongTinId > 0) {
			queryStr += " and u.ctckThongTinId = :ctckThongTinId ";
		}
		Query query = entityManager.createQuery(queryStr);
		if(bmBaoCaoId > 0) {
			query.setParameter("bmBaoCaoId", bmBaoCaoId);
		}
		if(bmSheetHang > 0) {
			query.setParameter("bmSheetHang", bmSheetHang);
		}
		if(ctckThongTinId > 0) {
			query.setParameter("ctckThongTinId", ctckThongTinId);
		}
		List<BcBaoCaoGtDongEntity> ObjectSheet = query.getResultList();
		if (!ObjectSheet.isEmpty() && ObjectSheet.size() > 0) {
		    entityData = ObjectSheet.get(0);
			return ObjectMapperUtils.map(entityData, BcBaoCaoGtDongDTO.class);
		}
    	}
    	catch(Exception e) {
    		
    	}finally {
			entityManager.close();
		}
		return null;
    }
    
    private StringBuffer genWherein(List<Integer> sIdCTCK) {
		StringBuffer s = new StringBuffer();

		s.append("(");
		for (Integer id : sIdCTCK) {
			s.append(id + ",");
		}
		StringBuffer Rest = new StringBuffer();
		Rest.append(s.substring(0, s.length() - 1).toString());
		Rest.append(")");
		return Rest;

	}

	@Override
	public BcBaoCaoGtDongDTO getGiaTriChiTieuDong(Integer bcThanhVienId, Integer bmSheetHang) {
		
		BcBaoCaoGtDongDTO result= new BcBaoCaoGtDongDTO();
		try {
		List<BcBaoCaoGtDongEntity> outPuts= bcBaoCaoGtDongEntityRepository.getGiaTriChiTieuDong(bcThanhVienId, bmSheetHang);
		List<BcBaoCaoGtDongDTO>	lsGiaTriOutput = ObjectMapperUtils.mapAll(outPuts, BcBaoCaoGtDongDTO.class);
		
		if(lsGiaTriOutput!=null&& lsGiaTriOutput.size()>=1) {
			return lsGiaTriOutput.get(0);
		}
		}
		catch(Exception e) {
			
		}finally {
			entityManager.close();
		}
		return result;
	}

}
