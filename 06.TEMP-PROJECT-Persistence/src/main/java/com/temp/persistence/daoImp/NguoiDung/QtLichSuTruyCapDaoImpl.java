package com.temp.persistence.daoImp.NguoiDung;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.QtLichSuTruyCapBDTO;
import com.temp.model.NguoiDung.QtLichSuTruyCapDTO;
import com.temp.persistence.dao.NguoiDung.QtLichSuTruyCapDao;

/**
 * @author DucHV1
 *
 */
@Component
public class QtLichSuTruyCapDaoImpl implements QtLichSuTruyCapDao {

	@Autowired
	private EntityManager entityManager;

	@Override
	public QtLichSuTruyCapBDTO getLichSuTruyCapList(String keySearch, String ipThucHien, String tenTaiKhoan,
			String tuNgay, String denNgay, String logType, Integer pageNo, Integer pageSize, String keySort,
			boolean sortOrder, String loaiNguoiDung, Integer ctckThongTinId) {

		QtLichSuTruyCapBDTO result = null;

		try {
			// Build select sql query
			StringBuilder strSelectCmd = buildSelectQuery();

			StringBuilder strWhereCmd1 = new StringBuilder(" where 1 = 1 ");
			strWhereCmd1.append(" AND log_Type IN ('Login','Logout')");
			
			strWhereCmd1 = this.buildWhereConditionString(strWhereCmd1, keySearch, ipThucHien, tenTaiKhoan, tuNgay,
					denNgay, logType, loaiNguoiDung, ctckThongTinId);
			String strOrderByCmd1 = " order by ngay_Tao ";
			strOrderByCmd1 = this.buildOrderByType(sortOrder, strOrderByCmd1.toString());

			// Build full sql query
			StringBuilder fullSqlCmd = new StringBuilder();
			fullSqlCmd.append(" SELECT * FROM (" + strSelectCmd + ")");
			fullSqlCmd.append(strWhereCmd1);
			fullSqlCmd.append(strOrderByCmd1);

			// Build count sql query
			StringBuilder countSqlCmd = new StringBuilder();
			countSqlCmd.append(" SELECT count(*) FROM (" + strSelectCmd + ")");
			countSqlCmd.append(strWhereCmd1);
			countSqlCmd.append(strOrderByCmd1);
			
			Query lstLichSuTruyCapQuery = entityManager.createNativeQuery(fullSqlCmd.toString());
			@SuppressWarnings("unchecked")
			List<Object[]> lstLichSuTruyCap = lstLichSuTruyCapQuery.setFirstResult(pageNo * pageSize)
					.setMaxResults(pageSize).getResultList();

			if (lstLichSuTruyCap != null) {
				result = new QtLichSuTruyCapBDTO();

				Query countLichSuTruyCapQuery = entityManager.createNativeQuery(countSqlCmd.toString());
				BigDecimal total =  (BigDecimal) countLichSuTruyCapQuery.getResultList().get(0);
				result.totalElement = total.longValue();

				// page cuối cùng.
				if (pageSize > 0) {
					result.lastPage = (int) ((result.totalElement / pageSize));
				}
				result.pageNo = pageNo + 1;
				result.pageSize = pageSize;

				try {
					// Get result list
					result.lstLichSuTruyCap = lstLichSuTruyCap.stream().map(QtLichSuTruyCapDTO::new)
							.collect(Collectors.toList());
					result.setMessage("Success");
					result.setStatus("200");
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			entityManager.close();
		}

		return result;
	}

	private StringBuilder buildSelectQuery() {
		StringBuilder strSelectCmd = new StringBuilder();
		strSelectCmd.append("SELECT l.id, l.ip_thuc_hien, l.log_type, l.ngay_tao, n.tai_khoan, n.is_thanh_vien,( SELECT rtrim(XMLCAST(XMLAGG(XMLELEMENT(e,ten_viet_tat || '-'|| ten_tieng_viet,'<br />') ORDER BY id ) AS CLOB),',') FROM ctck_thong_tin WHERE (ctck_thong_tin_id IN (SELECT ctck_id FROM lk_nguoi_dung_ctck t2 WHERE t2.nguoi_dung_id = n.id )OR ctck_thong_tin_id = n.ctck_id )AND is_bang_tam = 0 ) tenctck,( SELECT rtrim(XMLCAST(XMLAGG(XMLELEMENT(e,ctck_thong_tin_id,',') ORDER BY id ) AS CLOB),',') FROM ctck_thong_tin WHERE (ctck_thong_tin_id IN (SELECT ctck_id FROM lk_nguoi_dung_ctck t2 WHERE t2.nguoi_dung_id = n.id )OR ctck_thong_tin_id = n.ctck_id )AND is_bang_tam = 0 ) CTCK_IDS FROM qt_log_he_thong l left join qt_nguoi_dung n ON l.nguoi_tao_id = n.id ");
		return strSelectCmd;
	}

	@Override
	public QtLichSuTruyCapBDTO getListLichSuTruyCapExport(String tuNgay, String denNgay, String keySort, boolean desc) {
		QtLichSuTruyCapBDTO result = null;

		try {
			// Build select sql query
			StringBuilder strSelectCmd = buildSelectQuery();

			// Build where static sql query
			StringBuilder strWhereCmd = new StringBuilder(" where 1 = 1 ");
			strWhereCmd.append(" AND log_Type IN ('Login','Logout')");
			// TODO 'apply condition NgayTao = current date'
			// strWhereCmd.append(" AND l.ngayTao = sysdate");

			// and append dynamic conditions search
			strWhereCmd = this.buildWhereConditionString(strWhereCmd, null, null, null, tuNgay, denNgay, null, null,0);

			// Build Order by sql query
			String strOrderByCmd = " order by ngay_Tao ";
			strOrderByCmd = this.buildOrderByType(desc, strOrderByCmd.toString());

			// Build full sql query
			StringBuilder fullSqlCmd = new StringBuilder();
			fullSqlCmd.append(" SELECT * FROM (" + strSelectCmd + ")");
			fullSqlCmd.append(strWhereCmd);
			fullSqlCmd.append(strOrderByCmd);

			// get list export object
			Query lstLichSuTruyCapExportQuery = entityManager.createNativeQuery(fullSqlCmd.toString());
			@SuppressWarnings("unchecked")
			List<Object[]> lstLichSuTruyCapExport = lstLichSuTruyCapExportQuery.getResultList();

			// Get result list export
			if (lstLichSuTruyCapExport != null) {
				result = new QtLichSuTruyCapBDTO();
				result.lstLichSuTruyCap = lstLichSuTruyCapExport.stream().map(QtLichSuTruyCapDTO::new)
						.collect(Collectors.toList());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			entityManager.close();
		}
		return result;
	}

	/**
	 * Build order conditions
	 * 
	 * @param desc
	 * @param stringOderby
	 * @return
	 */
	private String buildOrderByType(boolean desc, String stringOderby) {
		if (desc) {
			stringOderby = stringOderby + " desc";
		} else {
			stringOderby = stringOderby + " asc";
		}
		return stringOderby;
	}

	/**
	 * Build Where conditions string
	 * 
	 * @param stringWhere
	 * @param keysearch
	 * @param ipThucHien
	 * @param noiDung
	 * @param tuNgay
	 * @param denNgay
	 * @param logType
	 * @param trangThai
	 * @return
	 */
	private StringBuilder buildWhereConditionString(StringBuilder stringWhere, String keysearch, String ipThucHien,
			String tenTaiKhoan, String tuNgay, String denNgay, String logType, String loaiNguoiDung, Integer ctckThongTinId) {

		// build filter by keySearch
		if (keysearch != null) {
			keysearch = keysearch.toLowerCase().trim();

			stringWhere.append(" and ( ");
			stringWhere.append(" (LOWER(ip_Thuc_Hien) like '%" + keysearch + "%') ");
			stringWhere.append(" or");
			stringWhere.append(" ( LOWER(tai_Khoan) like '%" + keysearch + "%')");
			stringWhere.append(" )");
		}

		// build filter by IP thuc hien.
		if (ipThucHien != null && (!"".endsWith(ipThucHien))) {

			ipThucHien = ipThucHien.toLowerCase();
			stringWhere.append(" and ( ");
			stringWhere.append("  LOWER(ip_Thuc_Hien) like '%" + ipThucHien + "%'");
			stringWhere.append(" )");
		}

		// build filter by Ngay tao bat dau.
		if (tuNgay != null && (!"".endsWith(tuNgay))) {

			stringWhere.append(" and ( ");
			stringWhere.append(" ngay_Tao >= TO_DATE('" + tuNgay + "','DD/MM/YY')");
			stringWhere.append(" )");
		}

		// build filter by Ngay tao ket thuc.
		if (denNgay != null && (!"".endsWith(denNgay))) {
			stringWhere.append(" and (ngay_Tao <= TO_DATE('" + denNgay + "','DD/MM/YY') + 1)");
		}

		// build filter by Log type.
		if (logType != null && (!"".endsWith(logType))) {
			logType = logType.toLowerCase();
			stringWhere.append(" and ( LOWER(log_Type) like '%" + logType + "%') ");
		}

		// build filter by ten tai khoan
		if (tenTaiKhoan != null && (!"".endsWith(tenTaiKhoan))) {
			tenTaiKhoan = tenTaiKhoan.toLowerCase();
			stringWhere.append(" and ( LOWER(tai_Khoan) like '%" + tenTaiKhoan + "%') ");
		}

		if (loaiNguoiDung != null && (!"".endsWith(loaiNguoiDung))) {
			stringWhere.append(" and ( is_thanh_Vien = " + loaiNguoiDung + ") ");
		}
		
		if (ctckThongTinId != null && ctckThongTinId > 0) {
			stringWhere.append(" and ( ',' || CTCK_IDS || ',' LIKE " + "'%," + ctckThongTinId + ",%'" + ") ");
		}

		return stringWhere;
	}

}
