package com.temp.persistence.responsitories.NguoiDung;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.temp.persistance.entity.NguoiDung.QuyenEntity;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Repository
public interface QuyenEntityRepository extends JpaRepository<QuyenEntity, Integer>,
		JpaSpecificationExecutor<QuyenEntity>, PagingAndSortingRepository<QuyenEntity, Integer> {
	
	@Query("select case when count(c)> 0 then true else false end from QuyenEntity c where lower(c.maQuyen) = lower(:maQuyen) and id!=:id")
	boolean existsByMa(@Param("maQuyen") String maQuyen, @Param("id")Integer id);
}
