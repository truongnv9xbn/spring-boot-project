package com.temp.persistence.responsitories.BieuMauBaoCao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.temp.persistance.entity.BieuMauBaoCao.BcBaoCaoGtDongEntity;

public interface BcBaoCaoGtDongEntityRepository extends JpaRepository<BcBaoCaoGtDongEntity, Integer>, JpaSpecificationExecutor<BcBaoCaoGtDongEntity> {
	
	@Query("select c from BcBaoCaoGtDongEntity c where  c.bcThanhVienId =:bcThanhVienId and  c.bmSheetHang =:bmSheetHang ")
	List<BcBaoCaoGtDongEntity> getGiaTriChiTieuDong(@Param("bcThanhVienId") Integer bcThanhVienId, @Param("bmSheetHang") Integer bmSheetHang);

}
