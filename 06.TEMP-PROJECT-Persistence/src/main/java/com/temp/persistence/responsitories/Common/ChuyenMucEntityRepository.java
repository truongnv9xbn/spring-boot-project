package com.temp.persistence.responsitories.Common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.temp.persistance.entity.Common.ChuyenMucEntity;

@Repository
public interface ChuyenMucEntityRepository extends JpaRepository<ChuyenMucEntity, Integer>,
	PagingAndSortingRepository<ChuyenMucEntity, Integer>, JpaSpecificationExecutor<ChuyenMucEntity> {
	
//	@Query("select case when count(c)> 0 then true else false end from ChuyenMucEntity c where lower(c.maChuyenMuc) = lower(:maChuyenMuc) and id!=:id")
//	boolean existsByMa(@Param("maChuyenMuc") String maChuyenMuc, @Param("id")Integer id);

}
