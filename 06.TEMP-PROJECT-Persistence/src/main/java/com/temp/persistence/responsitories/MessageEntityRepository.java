package com.temp.persistence.responsitories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.temp.persistance.entity.MessageEntity;


@Repository
public interface MessageEntityRepository extends JpaRepository<MessageEntity, Integer>,
PagingAndSortingRepository<MessageEntity, Integer>, JpaSpecificationExecutor<MessageEntity>{
	
//	@Query("select case when count(c)> 0 then true else false end from MessageEntity c where lower(c.maChucVu) = lower(:maChucVu) and id!=:id")
//	boolean existsByMa(@Param("maChucVu") String maChucVu, @Param("id")Integer id);
}
