package com.temp.persistence.responsitories.NguoiDung;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.temp.persistance.entity.LkNguoiDungNhomEntity;

/**
 * Generated by Spring Data Generator on 29/11/2019
 */
@Repository
public interface LkNguoiDungNhomEntityRepository
		extends JpaRepository<LkNguoiDungNhomEntity, Integer>, JpaSpecificationExecutor<LkNguoiDungNhomEntity> {

}
