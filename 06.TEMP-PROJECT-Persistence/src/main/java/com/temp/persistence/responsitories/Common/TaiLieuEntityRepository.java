package com.temp.persistence.responsitories.Common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.temp.persistance.entity.Common.TaiLieuEntity;

@Repository
public interface TaiLieuEntityRepository extends JpaRepository<TaiLieuEntity, Integer>,
	PagingAndSortingRepository<TaiLieuEntity, Integer>, JpaSpecificationExecutor<TaiLieuEntity> {
	
//	@Query("select case when count(c)> 0 then true else false end from TaiLieuEntity c where lower(c.maTaiLieu) = lower(:maTaiLieu) and id!=:id")
//	boolean existsByMa(@Param("maTaiLieu") String maTaiLieu, @Param("id")Integer id);

}
