package com.temp.persistence.responsitories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.temp.persistance.entity.TrangChuEntity;

@Repository
public interface TrangChuEntityRepository extends JpaRepository<TrangChuEntity, Integer>,
		PagingAndSortingRepository<TrangChuEntity, Integer>, JpaSpecificationExecutor<TrangChuEntity> {

}
