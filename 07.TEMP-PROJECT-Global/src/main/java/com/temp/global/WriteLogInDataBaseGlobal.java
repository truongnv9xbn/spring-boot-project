package com.temp.global;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.persistence.dao.NguoiDung.QtLogHeThongDao;

@Component
public class WriteLogInDataBaseGlobal {

	@Autowired
	private QtLogHeThongDao qtLog;

	private static QtLogHeThongDao logStatic;

	@PostConstruct
	private void initStaticDao() {
		logStatic = this.qtLog;
	}

	public static void WriteLogDB(QtLogHeThongDTO dto) {
		logStatic.addOrUpdate(dto);
	}

}
