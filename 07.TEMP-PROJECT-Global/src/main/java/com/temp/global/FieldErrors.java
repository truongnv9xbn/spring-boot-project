package com.temp.global;

import lombok.Getter;
import lombok.Setter;

public class FieldErrors {

	
	@Getter
	@Setter
	public String message;
	@Getter
	@Setter
	public String fieldName;

  
//    public String getErrorMessage() {
//        return errorMessage;
//    }

}
