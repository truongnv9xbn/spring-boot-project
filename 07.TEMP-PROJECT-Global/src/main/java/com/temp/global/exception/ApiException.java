package com.temp.global.exception;

import java.io.Serializable;
import java.time.ZonedDateTime;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

public class ApiException implements Serializable{
	
	private static final long serialVersionUID = -1133749449434742263L;
	
	@Getter
	public final String message;
	@Getter
	public final ZonedDateTime timestamp;
	
	@Getter
	public  HttpStatus httpStatus;
	
	@Setter
	@Getter
	public String status;
	
	@Setter
	@Getter
	public String error;
	
	

	public ApiException(String message, ZonedDateTime timestamp, HttpStatus httpStatus) {
		super();
		this.message = message;
		this.timestamp = timestamp;
		this.httpStatus = httpStatus;
	}

	
	public ApiException(String message, ZonedDateTime timestamp, Throwable throwable) {
		super();
		this.message = message;
		this.timestamp = timestamp;
	}

}
