package com.temp.global.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.modelmapper.spi.ErrorMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class ApiExceptionHandler {

	
	
	 /**
     * Tất cả các Exception không được khai báo sẽ được xử lý tại đây
     */
    @ExceptionHandler(ApiRequestException.class)
    public ResponseEntity<Object> handleAllException(ApiRequestException e, WebRequest request) {
        // quá trình kiểm soat lỗi diễn ra ở đây
    	ApiException apiException = new ApiException(
				e.getMessage(), 
				ZonedDateTime.now(ZoneId.of("Z")), 
				e);

		return new ResponseEntity<Object>(apiException,e.httpStatus);
    }

  

    /**
     * IndexOutOfBoundsException sẽ được xử lý riêng tại đây
     */
    @ExceptionHandler(IndexOutOfBoundsException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public ErrorMessage TodoException(Exception ex, WebRequest request) {
    	   return new ErrorMessage("Đối tượng không tồn tại", ex);
    }
}
