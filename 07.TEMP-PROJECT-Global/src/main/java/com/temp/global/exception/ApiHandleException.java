package com.temp.global.exception;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;




@ControllerAdvice
public class ApiHandleException {
	public static final Logger logger = LoggerFactory.getLogger(ApiHandleException.class);
	

//	@ExceptionHandler(value = { ApiRequestException.class })
//	public ResponseEntity<Object> handleRequestException(ApiRequestException e, HttpStatus httpStatus) {
//		ApiException apiException = new ApiException(
//				e.getMessage(), 
//				ZonedDateTime.now(ZoneId.of("Z")), 
//				e);
//
//		return new ResponseEntity<>(apiException,httpStatus);
//	}
	
	
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	@ExceptionHandler(value = {Exception.class})
	public ResponseEntity<Object> handleRequestException(Exception e, HttpStatus httpStatus) {

		ApiException apiException = new ApiException(
				e.getMessage(), 
				ZonedDateTime.now(ZoneId.of("Z")),
				httpStatus);
		
		return new ResponseEntity<>(apiException, httpStatus);
	}
	
//	@ExceptionHandler(value = { DataIntegrityViolationException.class })
//	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
//		logger.trace(ex.toString());
//		return handleExceptionInternal(ex, "Data conflict", new HttpHeaders(), HttpStatus.CONFLICT, request);
//	}
//
//	@ExceptionHandler({ AccessDeniedException.class })
//	public ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
//		logger.trace(ex.toString());
//		return handleExceptionInternal(ex, "Access denied message here", new HttpHeaders(), HttpStatus.FORBIDDEN,
//				request);
//	}
//
//	@ExceptionHandler({ Exception.class })
//	public ResponseEntity<Object> handleInternalException(Exception ex, WebRequest request) {
//		logger.trace(ex.toString());
//		return handleExceptionInternal(ex, "Internal server error", new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
//				request);
//	}
//	
//	@ExceptionHandler({ExpiredLicenseException.class })
//	public ResponseEntity<Object> handleExpiredLicenseException(Exception ex, WebRequest request) {
//		logger.trace(ex.toString());
//		return handleExceptionInternal(ex, "Expired License, No Active", new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE,
//				request);//406
//	}
	

}
