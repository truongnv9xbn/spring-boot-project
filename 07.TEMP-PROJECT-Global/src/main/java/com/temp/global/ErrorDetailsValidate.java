/**
 * 
 */
package com.temp.global;

import java.util.Date;
import java.util.List;

/**
 * @author thangnn
 *
 */
public class ErrorDetailsValidate {
	private Date timestamp;
	List<FieldErrors> errors;
	private String countErrors;

	public ErrorDetailsValidate(Date timestamp, List<FieldErrors> errors, String countError) {
		super();
		this.timestamp = timestamp;
		this.errors = errors;
		this.countErrors = countError;
	}
}
