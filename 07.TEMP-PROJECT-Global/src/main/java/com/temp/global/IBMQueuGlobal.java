//package com.temp.global;
//
//import java.io.IOException;
//
//import com.example.DTO_LienThong.MSG;
//import com.ibm.mq.MQC;
//import com.ibm.mq.MQEnvironment;
//import com.ibm.mq.MQException;
//import com.ibm.mq.MQMessage;
//import com.ibm.mq.MQPutMessageOptions;
//import com.ibm.mq.MQQueue;
//import com.ibm.mq.MQQueueManager;
//import com.ibm.mq.constants.MQConstants;
//
//import com.temp.utils.Constant;
//
//public class IBMQueuGlobal {
//	public static MSG sendIBMQueu(String xml, String IDS) {
//		String qMGR = ThamSoHeThongGlobal.giaTriThamSo(Constant.QUEUE_MGR).getGiaTri();
//		String hostname = ThamSoHeThongGlobal.giaTriThamSo(Constant.QUEUE_HOSTNAME).getGiaTri();
//		String channel = ThamSoHeThongGlobal.giaTriThamSo(Constant.QUEUE_CHANNEL).getGiaTri();
//		String port_queue = ThamSoHeThongGlobal.giaTriThamSo(Constant.QUEUE_PORT).getGiaTri();
//		String userID = ThamSoHeThongGlobal.giaTriThamSo(Constant.QUEUE_USERID).getGiaTri();
//		String password = ThamSoHeThongGlobal.giaTriThamSo(Constant.QUEUE_PASSWORD).getGiaTri();
//		if(password == null) {
//			password = "";
//		}
//		if (qMGR == null) {
//			//System.out.println("Không tìm thấy dữ liệu của qMGR");
//			return new MSG("Thất bại", "Lỗi MGR");
//		}
//		String qIDS = IDS;
//		if (qIDS == null) {
//			//System.out.println("qIDS đang không có dữ liệu !");
//			return new MSG("Thất bại", "Không tìm thấy dữ liệu của IDS");
//		}
//		if (qMGR != null && qMGR.length() > 0 && qIDS != null && qIDS.length() > 0) {
//
//			MQEnvironment.hostname = hostname;
//			MQEnvironment.channel = channel;
//			MQEnvironment.port = Integer.valueOf(port_queue);
//			MQEnvironment.userID = userID;
//			MQEnvironment.password = password;
//			// set transport properties.
//			MQEnvironment.properties.put(MQC.TRANSPORT_PROPERTY, MQC.TRANSPORT_MQSERIES_CLIENT);
//
//			try {
//				MQQueueManager qManager = new MQQueueManager(qMGR);
//				int openOptions = MQConstants.MQOO_INPUT_AS_Q_DEF | MQConstants.MQOO_OUTPUT;
//				MQQueue queue = qManager.accessQueue(qIDS, openOptions);
//				MQMessage msg = new MQMessage();
//				String decodedToUTF8 = new String(xml.getBytes("UTF-8"), "ISO-8859-1");
//				msg.writeString((String)decodedToUTF8);
//				MQPutMessageOptions pmo = new MQPutMessageOptions();
//				queue.put(msg, pmo);//
//				queue.close();
//				return new MSG("Thành công", "200");
//			} catch (MQException e) {
//				e.printStackTrace();
//				if(e.getCompCode() == 2 && e.getReason() == 2038) {
//					return new MSG("Thất bại", "File quá lớn !");
//				}else if(e.getCompCode() == 2 && e.getReason() == 2058) {
//					return new MSG("Thất bại", "Lỗi Connection MQ !");
//				};
//				
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//			//System.out.println("qIDS & qMGR đang không có dữ liệu !");
//			return new MSG("Thất bại", "Không tìm thấy dữ liệu của qMGR và qIDS");
//		}
//		//System.out.println("qIDS & qMGR đang không có dữ liệu !");
//		return new MSG("Thất bại", "Không tìm thấy dữ liệu của qMGR và qIDS");
//	}
//}
