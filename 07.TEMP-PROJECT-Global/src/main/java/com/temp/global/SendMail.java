package com.temp.global;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.utils.Constant;

public class SendMail {

	public static Boolean SendMail(String emailTo, String tieuDe, String noiDung) {
        ThamSoHeThongDTO mailServer = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_SERVER);
        ThamSoHeThongDTO mailPort = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_PORT);
        ThamSoHeThongDTO mailUser = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_USER);
        ThamSoHeThongDTO mailPassword = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_PASSWORD);
        ThamSoHeThongDTO mailBccConfig = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_BCC_CONFIG);
        ThamSoHeThongDTO mailBcc = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_BCC);
        if(mailServer != null && mailPort != null && mailUser != null && mailPassword != null) {
			Properties prop = new Properties();
			prop.put("mail.smtp.host", mailServer.getGiaTri());
	        prop.put("mail.smtp.port", mailPort.getGiaTri());
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.starttls.enable", "true"); //TLS
//	        String check = PasswordGenerator.encrypt("scmsTV20@)");
//	        String strPassDecrypt = PasswordGenerator.decrypt(mailPassword.getGiaTri());
	        String strPassDecrypt = mailPassword.getGiaTri();
	        Session session = Session.getInstance(prop,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(mailUser.getGiaTri(), strPassDecrypt);
	                    }
	                });
	
	        try {
	
	            Message message = new MimeMessage(session);
	            if(mailBccConfig != null && mailBccConfig.getGiaTri() == "Y") {
		            InternetAddress[] addresses = InternetAddress.parse(mailBcc.getGiaTri());
		            message.setRecipients(Message.RecipientType.CC, addresses);
	            }
	            message.setFrom(new InternetAddress(mailUser.getGiaTri()));
	            message.setRecipients(
	                    Message.RecipientType.TO,
	                    InternetAddress.parse(emailTo)
	            );
	            message.setSubject(tieuDe);
	            message.setContent(noiDung, "text/html; charset=UTF-8");
	
	            
	            
	            Transport.send(message);
	
	            //System.out.println("Done");
	
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
        }
        	
		return true;
	}
	
	
	public static Boolean SendMailTemplate(String emailTo, String tieuDe, String noiDung) {
        ThamSoHeThongDTO mailServer = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_SERVER);
        ThamSoHeThongDTO mailPort = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_PORT);
        ThamSoHeThongDTO mailUser = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_USER);
        ThamSoHeThongDTO mailPassword = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_PASSWORD);
        ThamSoHeThongDTO mailBccConfig = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_BCC_CONFIG);
        ThamSoHeThongDTO mailBcc = ThamSoHeThongGlobal.giaTriThamSo(Constant.MAIL_BCC);
        if(mailServer != null && mailPort != null && mailUser != null && mailPassword != null) {
			Properties prop = new Properties();
			prop.put("mail.smtp.host", mailServer.getGiaTri());
	        prop.put("mail.smtp.port", mailPort.getGiaTri());
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.starttls.enable", "true"); //TLS
//	        String check = PasswordGenerator.encrypt("scmsTV20@)");
//	        String strPassDecrypt = PasswordGenerator.decrypt(mailPassword.getGiaTri());
	        String strPassDecrypt = mailPassword.getGiaTri();
	        Session session = Session.getInstance(prop,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(mailUser.getGiaTri(), strPassDecrypt);
	                    }
	                });
	
	        try {
	
	            Message message = new MimeMessage(session);
	            
	            message.setFrom(new InternetAddress(mailUser.getGiaTri()));
	            message.setRecipients(
	                    Message.RecipientType.TO,
	                    InternetAddress.parse(emailTo)
	            );
	            if(mailBccConfig != null && mailBccConfig.getGiaTri() == "Y") {
		            InternetAddress[] addresses = InternetAddress.parse(mailBcc.getGiaTri());
		            message.setRecipients(Message.RecipientType.CC, addresses);
	            }
	            
	            noiDung= getForm(tieuDe, noiDung);
	            message.setSubject(tieuDe==null?"":tieuDe);
	            message.setContent(noiDung, "text/html; charset=UTF-8");
	            
	            Transport.send(message);
	            //System.out.println("Done");
	            
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
        }
        	
		return true;
	}
	
	private static String getForm(String strSubTieuDe, String strNoiDung) {
		String strEmail = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"padding-bottom: 20px; max-width: 516px; min-width: 220px;\"> <tbody> <tr> <td style=\"width: 8px;\" width=\"8\">&nbsp;</td> <td> <div align=\"center\" class=\"m_205523402140519040mdv2rw\" style=\"border-style: solid; border-width: thin; border-color: #dadce0; border-radius: 8px; padding: 40px 20px;\"> <img alt=\"SCMS\" aria-hidden=\"true\" class=\"CToWUd\" height=\"74\" src=\"http://scms.tinhvan.com/img/logo.png\" style=\"margin-bottom: 16px;\" width=\"74\" /> <div style=\" font-family: 'Google Sans', Roboto, RobotoDraft, Helvetica, Arial, sans-serif; border-bottom: thin solid #dadce0; color: rgba(0, 0, 0, 0.87); line-height: 32px; padding-bottom: 24px; text-align: center; word-break: break-word; \" > <div style=\"font-size: 24px;\"><a style=\"text-decoration: none; color: rgba(0, 0, 0, 0.87);\">"
				+ strSubTieuDe + "</a></div> </div> <div style=\"font-family: Roboto-Regular, Helvetica, Arial, sans-serif; font-size: 14px; color: rgba(0, 0, 0, 0.87); line-height: 20px; padding-top: 20px; text-align: left;\"> " 
				+ strNoiDung + " </div> </div> <div style=\"text-align: left;\"> <div style=\"font-family: Roboto-Regular, Helvetica, Arial, sans-serif; color: rgba(0, 0, 0, 0.54); font-size: 11px; line-height: 18px; padding-top: 12px; text-align: center;\"> <div>Vui lòng giữ bảo mật thông tin tài khoản đã được cấp</div> <div style=\"direction: ltr;\">© 2020 SCMS - Ủy ban chứng khoán nhà nước</div> </div> </div> </td> <td style=\"width: 8px;\" width=\"8\">&nbsp;</td> </tr> </tbody></table>";
		return strEmail;
	}
}
