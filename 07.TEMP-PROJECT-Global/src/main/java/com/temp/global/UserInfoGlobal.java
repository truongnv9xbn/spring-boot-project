package com.temp.global;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.NguoiDung.QtNguoiDungDao;
import com.temp.utils.ObjectMapperUtils;

@Component
public class UserInfoGlobal {
	@Autowired
	private QtNguoiDungDao daoNguoiDung;

	private static QtNguoiDungDao daosNguoiDung;

	@PostConstruct
	private void initStaticDao() {
		daosNguoiDung = this.daoNguoiDung;
	}

	public static QtNguoiDungDTO getUserInfoAuthor() {
		try {
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (!(authentication instanceof AnonymousAuthenticationToken)) {
				String currentUserName = authentication.getName();
				if (!StringUtils.isEmpty(currentUserName)) {
					return ObjectMapperUtils.map(daosNguoiDung.findInfoUser(currentUserName), QtNguoiDungDTO.class);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
}
