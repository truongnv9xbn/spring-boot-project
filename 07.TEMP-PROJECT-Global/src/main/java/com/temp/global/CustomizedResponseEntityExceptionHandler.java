/**
 * 
 */
package com.temp.global;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.gson.Gson;

/**
 * @author thangnn
 *
 */
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	@ExceptionHandler(RuntimeException.class)
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		List<FieldErrors> errors = new ArrayList<FieldErrors>();
		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
			FieldErrors err = new FieldErrors();
			err.fieldName = error.getField();
			err.message = error.getDefaultMessage();
			// add
			errors.add(err);

		}

		ErrorDetailsValidate errorDetails = new ErrorDetailsValidate(new Date(), errors,
				ex.getBindingResult().getErrorCount() + "");
		Gson gson = new Gson();
		String json = gson.toJson(errorDetails);
		return new ResponseEntity(json, HttpStatus.SERVICE_UNAVAILABLE);
	}
}
