package com.temp.global;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.persistence.dao.Common.QtThamSoHeThongDao;

@Component
public class ThamSoHeThongGlobal {

	@Autowired
	private QtThamSoHeThongDao qtHeThong;

	private static QtThamSoHeThongDao qtHeThongStatic;

	@PostConstruct
	private void initStaticDao() {
		qtHeThongStatic = this.qtHeThong;
	}

	public static ThamSoHeThongDTO giaTriThamSo(String tenThamSo) {
		return qtHeThongStatic.findByThamSo(tenThamSo);
	}

}
