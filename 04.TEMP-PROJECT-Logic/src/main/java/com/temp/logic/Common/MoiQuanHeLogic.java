package com.temp.logic.Common;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.Common.MoiQuanHeBDTO;
import com.temp.model.Common.MoiQuanHeDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.Common.DmMoiQuanHeDao;
	
@Component
public class MoiQuanHeLogic {

	@Autowired
	private DmMoiQuanHeDao dmMoiQuanHeDao;

	/**
	 * ThÃ¡Â»Â±c hiÃ¡Â»â€¡n delete mÃ¡Â»â€˜i quan hÃ¡Â»â€¡
	 * 
	 * @param MoiQuanHeId mÃ¡Â»â€˜i quan hÃ¡Â»â€¡ id truyÃ¡Â»Ân vÃƒÂ o
	 * @return thÃƒÂ nh cÃƒÂ´ng thÃƒÂ¬ true ngÃ†Â°Ã¡Â»Â£c lÃ¡ÂºÂ¡i lÃƒÂ  false
	 */
	public boolean deleteMoiQuanHe(int MoiQuanHeId) {
		if (MoiQuanHeId > 0) {
			if (dmMoiQuanHeDao.deleteMoiQuanHeById(MoiQuanHeId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list Moi Quan He
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageMoiQuanHe List
	 */
	public MoiQuanHeBDTO listMoiQuanHes(String filterkey, String sTenMoiQuanHe, String sMaMoiQuanHe, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		MoiQuanHeBDTO lstPageMoiQuanHe = new MoiQuanHeBDTO();

		filterkey = validateParam(filterkey);
		sTenMoiQuanHe = validateParam(sTenMoiQuanHe);
		sMaMoiQuanHe = validateParam(sMaMoiQuanHe);
		keySort = validateParamKeySort(keySort);

		lstPageMoiQuanHe = dmMoiQuanHeDao.listMoiQuanHes(filterkey, sTenMoiQuanHe, sMaMoiQuanHe, pageNo, pageSize,
				keySort, desc, trangThai);

		return lstPageMoiQuanHe;
	}

	public boolean addOrUpDateMoiQuanHe(MoiQuanHeDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				MoiQuanHeDTO oldMoiQuanHe = dmMoiQuanHeDao.findById(dto.getId());
				oldMoiQuanHe.setTenMoiQuanHe(dto.getTenMoiQuanHe());
				oldMoiQuanHe.setMaMoiQuanHe(dto.getMaMoiQuanHe());
				oldMoiQuanHe.setGhiChu(dto.getGhiChu());
				oldMoiQuanHe.setTrangThai(dto.getTrangThai());
				// TODO ngÃ†Â°Ã¡Â»Âi tÃ¡ÂºÂ¡o
				return dmMoiQuanHeDao.addOrUpdateMoiQuanHe(oldMoiQuanHe);

			}
			dto.setNguoiTaoId(userInfo.getId());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return dmMoiQuanHeDao.addOrUpdateMoiQuanHe(dto);
		}

		return false;

	}

	public MoiQuanHeDTO getById(int id) {
		if (id > 0) {
			MoiQuanHeDTO qtDTO = dmMoiQuanHeDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistMoiQuanHeUpdate(MoiQuanHeDTO dto) {

		boolean result = false;
		result = dmMoiQuanHeDao.isMoiQuanHeExistUpdate(dto.getId(), dto.getMaMoiQuanHe());

		return result;
	}

	public boolean isExistMoiQuanHeAdd(MoiQuanHeDTO dto) {

		boolean result = false;
		result = dmMoiQuanHeDao.isMoiQuanHeExistAdd(dto.getMaMoiQuanHe());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = dmMoiQuanHeDao.isExistById(id);

		return result;
	}
	
	public boolean isExistByMa(String maMoiQuanHe, Integer id) {
		boolean result = false;

		result = dmMoiQuanHeDao.isExistByMa(maMoiQuanHe,id);

		return result;
	}
}
