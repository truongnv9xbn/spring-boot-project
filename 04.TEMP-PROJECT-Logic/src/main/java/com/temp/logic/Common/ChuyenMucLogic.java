package com.temp.logic.Common;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.Common.ChuyenMucBDTO;
import com.temp.model.Common.ChuyenMucDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.Common.ChuyenMucDao;

@Component
public class ChuyenMucLogic {

	@Autowired
	private ChuyenMucDao chuyenMucDao;

	public ChuyenMucBDTO getList(String keysearch, String sTenChuyenMuc, String sNoiDung, String sNguoiTao,
			String trangThai, Integer pageNo, Integer pageSize, String keySort, boolean desc) {

		ChuyenMucBDTO lstPageChuyenMuc = new ChuyenMucBDTO();
		keysearch = validateParam(keysearch);
		sTenChuyenMuc = validateParam(sTenChuyenMuc);
		sNoiDung = validateParam(sNoiDung);
		keySort = validateParamKeySort(keySort);
		lstPageChuyenMuc = chuyenMucDao.getList(keysearch, sTenChuyenMuc, sNoiDung, sNguoiTao, trangThai, pageNo,
				pageSize, keySort, desc);

		return lstPageChuyenMuc;
	}

	public boolean deleteChuyenMuc(int ChuyenMucId) {
		if (ChuyenMucId > 0) {
			if (chuyenMucDao.deleteChuyenMucById(ChuyenMucId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list Moi Quan He
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageChuyenMuc List
	 */
	public ChuyenMucBDTO listChuyenMucs(String filterkey, String sTenChuyenMuc, String sMaChuyenMuc, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		ChuyenMucBDTO lstPageChuyenMuc = new ChuyenMucBDTO();

		filterkey = validateParam(filterkey);
		sTenChuyenMuc = validateParam(sTenChuyenMuc);
		sMaChuyenMuc = validateParam(sMaChuyenMuc);
		keySort = validateParamKeySort(keySort);

		lstPageChuyenMuc = chuyenMucDao.listChuyenMucs(filterkey, sTenChuyenMuc, sMaChuyenMuc, pageNo, pageSize,
				keySort, desc, trangThai);

		return lstPageChuyenMuc;
	}

	public boolean addOrUpDateChuyenMuc(ChuyenMucDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				ChuyenMucDTO oldChuyenMuc = chuyenMucDao.findById(dto.getId());
				oldChuyenMuc.setTenChuyenMuc(dto.getTenChuyenMuc());
//				oldChuyenMuc.setMaChuyenMuc(dto.getMaChuyenMuc());
				oldChuyenMuc.setGhiChu(dto.getGhiChu());
				oldChuyenMuc.setTrangThai(dto.getTrangThai());
				// TODO ngÃ†Â°Ã¡Â»Âi tÃ¡ÂºÂ¡o
				return chuyenMucDao.addOrUpdateChuyenMuc(oldChuyenMuc);

			}
			dto.setNguoiTao(userInfo.getHoTen());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return chuyenMucDao.addOrUpdateChuyenMuc(dto);
		}

		return false;

	}

	public ChuyenMucDTO getById(int id) {
		if (id > 0) {
			ChuyenMucDTO qtDTO = chuyenMucDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistChuyenMucUpdate(ChuyenMucDTO dto) {

		boolean result = false;
//		result = chuyenMucDao.isChuyenMucExistUpdate(dto.getId(), dto.getMaChuyenMuc());

		return result;
	}

	public boolean isExistChuyenMucAdd(ChuyenMucDTO dto) {

		boolean result = false;
//		result = chuyenMucDao.isChuyenMucExistAdd(dto.getMaChuyenMuc());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = chuyenMucDao.isExistById(id);

		return result;
	}

//	public boolean isExistByMa(String maChuyenMuc, Integer id) {
//		boolean result = false;
//
//		result = chuyenMucDao.isExistByMa(maChuyenMuc, id);
//
//		return result;
//	}
}
