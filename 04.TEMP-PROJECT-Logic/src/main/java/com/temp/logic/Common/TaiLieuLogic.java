package com.temp.logic.Common;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.Common.TaiLieuBDTO;
import com.temp.model.Common.TaiLieuDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.Common.TaiLieuDao;

@Component
public class TaiLieuLogic {

	@Autowired
	private TaiLieuDao taiLieuDao;

	public TaiLieuBDTO getList(String keysearch, String sSoVanBan, String sTrichYeu, String sNguoiTao, String trangThai,
			Integer pageNo, Integer pageSize, String keySort, boolean desc) {

		TaiLieuBDTO lstPageTaiLieu = new TaiLieuBDTO();

		keysearch = validateParam(keysearch);
		sSoVanBan = validateParam(sSoVanBan);
		sTrichYeu = validateParam(sTrichYeu);
		keySort = validateParamKeySort(keySort);

		lstPageTaiLieu = taiLieuDao.getList(keysearch, sSoVanBan, sTrichYeu, sNguoiTao, trangThai, pageNo, pageSize,
				keySort, desc);

		return lstPageTaiLieu;
	}

	public boolean deleteTaiLieu(int TaiLieuId) {
		if (TaiLieuId > 0) {
			if (taiLieuDao.deleteTaiLieuById(TaiLieuId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list Moi Quan He
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageTaiLieu List
	 */
	public TaiLieuBDTO listTaiLieus(String filterkey, String sTenTaiLieu, String sMaTaiLieu, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		TaiLieuBDTO lstPageTaiLieu = new TaiLieuBDTO();

		filterkey = validateParam(filterkey);
		sTenTaiLieu = validateParam(sTenTaiLieu);
		sMaTaiLieu = validateParam(sMaTaiLieu);
		keySort = validateParamKeySort(keySort);

		lstPageTaiLieu = taiLieuDao.listTaiLieus(filterkey, sTenTaiLieu, sMaTaiLieu, pageNo, pageSize, keySort, desc,
				trangThai);

		return lstPageTaiLieu;
	}

	public boolean addOrUpDateTaiLieu(TaiLieuDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				TaiLieuDTO oldTaiLieu = taiLieuDao.findById(dto.getId());
				oldTaiLieu.setSoVanBan(dto.getSoVanBan());
				oldTaiLieu.setTrichYeuNoiDung(dto.getTrichYeuNoiDung());
				oldTaiLieu.setTrangThai(dto.getTrangThai());
				// TODO ngÃ†Â°Ã¡Â»Âi tÃ¡ÂºÂ¡o
				return taiLieuDao.addOrUpdateTaiLieu(oldTaiLieu);

			}
			dto.setNguoiTao(userInfo.getHoTen());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return taiLieuDao.addOrUpdateTaiLieu(dto);
		}

		return false;

	}

	public TaiLieuDTO getById(int id) {
		if (id > 0) {
			TaiLieuDTO qtDTO = taiLieuDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistTaiLieuUpdate(TaiLieuDTO dto) {

		boolean result = false;
//		result = taiLieuDao.isTaiLieuExistUpdate(dto.getId(), dto.getMaTaiLieu());

		return result;
	}

	public boolean isExistTaiLieuAdd(TaiLieuDTO dto) {

		boolean result = false;
//		result = taiLieuDao.isTaiLieuExistAdd(dto.getMaTaiLieu());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = taiLieuDao.isExistById(id);

		return result;
	}

//	public boolean isExistByMa(String maTaiLieu, Integer id) {
//		boolean result = false;
//
//		result = taiLieuDao.isExistByMa(maTaiLieu, id);
//
//		return result;
//	}
}
