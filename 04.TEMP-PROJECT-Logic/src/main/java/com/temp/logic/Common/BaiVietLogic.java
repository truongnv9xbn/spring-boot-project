package com.temp.logic.Common;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.Common.BaiVietBDTO;
import com.temp.model.Common.BaiVietDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.Common.BaiVietDao;

@Component
public class BaiVietLogic {

	@Autowired
	private BaiVietDao baiVietDao;

	public BaiVietBDTO getList(String keysearch, Integer sChuyenMucId, String sTieuDe, String sMoTaNgan,
			String sNoiDung, String sNguoiTao, Integer sNoiBat, String trangThai, Integer pageNo, Integer pageSize,
			String keySort, boolean desc) {

		BaiVietBDTO lstPageBaiViet = new BaiVietBDTO();

		keysearch = validateParam(keysearch);
		sTieuDe = validateParam(sTieuDe);
		sMoTaNgan = validateParam(sMoTaNgan);
		keySort = validateParamKeySort(keySort);

		lstPageBaiViet = baiVietDao.getList(keysearch, sChuyenMucId, sTieuDe, sMoTaNgan, sNoiDung, sNguoiTao, sNoiBat,
				trangThai, pageNo, pageSize, keySort, desc);

		return lstPageBaiViet;
	}

	public boolean deleteBaiViet(int BaiVietId) {
		if (BaiVietId > 0) {
			if (baiVietDao.deleteBaiVietById(BaiVietId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	public BaiVietBDTO listBaiViets(String filterkey, String sTenBaiViet, String sMaBaiViet, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		BaiVietBDTO lstPageBaiViet = new BaiVietBDTO();

		filterkey = validateParam(filterkey);
		sTenBaiViet = validateParam(sTenBaiViet);
		sMaBaiViet = validateParam(sMaBaiViet);
		keySort = validateParamKeySort(keySort);

		lstPageBaiViet = baiVietDao.listBaiViets(filterkey, sTenBaiViet, sMaBaiViet, pageNo, pageSize, keySort, desc,
				trangThai);

		return lstPageBaiViet;
	}

	public boolean addOrUpDateBaiViet(BaiVietDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				BaiVietDTO oldBaiViet = baiVietDao.findById(dto.getId());
				oldBaiViet.setChuyenMucId(dto.getChuyenMucId());
				oldBaiViet.setTieuDe(dto.getTieuDe());
				oldBaiViet.setMoTaNgan(dto.getMoTaNgan());
				oldBaiViet.setGhiChu(dto.getGhiChu());
				oldBaiViet.setTrangThai(dto.getTrangThai());
				// TODO ngÃ†Â°Ã¡Â»Âi tÃ¡ÂºÂ¡o
				return baiVietDao.addOrUpdateBaiViet(oldBaiViet);

			}
			dto.setNguoiTao(userInfo.getHoTen());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return baiVietDao.addOrUpdateBaiViet(dto);
		}

		return false;

	}

	public BaiVietDTO getById(int id) {
		if (id > 0) {
			BaiVietDTO qtDTO = baiVietDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistBaiVietUpdate(BaiVietDTO dto) {

		boolean result = false;
//		result = baiVietDao.isBaiVietExistUpdate(dto.getId(), dto.getMaBaiViet());

		return result;
	}

	public boolean isExistBaiVietAdd(BaiVietDTO dto) {

		boolean result = false;
//		result = baiVietDao.isBaiVietExistAdd(dto.getMaBaiViet());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = baiVietDao.isExistById(id);

		return result;
	}

//	public boolean isExistByMa(String maBaiViet, Integer id) {
//		boolean result = false;
//
//		result = baiVietDao.isExistByMa(maBaiViet, id);
//
//		return result;
//	}
}
