package com.temp.logic.Common;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.Common.ThamSoHeThongBDTO;
import com.temp.model.Common.ThamSoHeThongDTO;
import com.temp.persistence.dao.Common.QtThamSoHeThongDao;
import com.temp.utils.Constant;

@Component
public class ThamSoHeThongLogic {

	@Autowired
	private QtThamSoHeThongDao qtThamSoHeThongDao;

	/**
	 * Get list trạng thái công ty chứng khoán
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageThamSoHeThong List trạng thái công ty chứng khoán
	 */
	public ThamSoHeThongBDTO listThamSoHeThongs(String filterkey, String sPhanHe, String sThamSo, String sGiaTri,
			Integer pageNo, Integer pageSize, String keySort, boolean desc, String trangThai) {

		ThamSoHeThongBDTO lstPageThamSoHeThong = new ThamSoHeThongBDTO();

		filterkey = validateParam(filterkey);
		sPhanHe = validateParam(sPhanHe);
		sThamSo = validateParam(sThamSo);
		sGiaTri = validateParam(sGiaTri);
		keySort = validateParamKeySort(keySort);

		lstPageThamSoHeThong = qtThamSoHeThongDao.listThamSoHeThongs(filterkey, sPhanHe, sThamSo, sGiaTri, pageNo,
				pageSize, keySort, desc, trangThai);

		return lstPageThamSoHeThong;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param ThamSoHeThong đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean addOrUpDateThamSoHeThong(ThamSoHeThongDTO dto) {
		if (dto != null) {
			Date curr = new Date();
			if (dto.getId() > 0) {
				ThamSoHeThongDTO oldThamSoHeThong = qtThamSoHeThongDao.findById(dto.getId());
				oldThamSoHeThong.setPhanHe(dto.getPhanHe());
				oldThamSoHeThong.setThamSo(dto.getThamSo());
				oldThamSoHeThong.setGiaTri(dto.getGiaTri());
				oldThamSoHeThong.setGhiChu(dto.getGhiChu());
				oldThamSoHeThong.setTrangThai(dto.getTrangThai());
				oldThamSoHeThong.setNgayCapNhat(new Timestamp(curr.getTime()));
				// TODO người tạo
				boolean result = qtThamSoHeThongDao.addOrUpdateThamSoHeThong(oldThamSoHeThong);
				if(result = true) {
					if(oldThamSoHeThong.getThamSo().equals("CHECKTENCKS"))
						Constant.CHECKTENCKS = oldThamSoHeThong.getGiaTri();
				}
				return result;
			}
			return qtThamSoHeThongDao.addOrUpdateThamSoHeThong(dto);
		}

		return false;

	}
	
	public ThamSoHeThongDTO getById(int id) {
		if (id > 0) {
			ThamSoHeThongDTO qtDTO = qtThamSoHeThongDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
			
			
		}
		
		
		return null;
	}
	
	public ThamSoHeThongDTO getByThamSo(String thamSo) {
		if (thamSo != "") {
			ThamSoHeThongDTO qtDTO = qtThamSoHeThongDao.findByThamSo(thamSo);
			if (qtDTO != null) {
				return qtDTO;
			}
			
			
		}
		
		
		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = param.trim();

		return param;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = qtThamSoHeThongDao.isExistById(id);

		return result;
	}

}
