package com.temp.logic.trangchu;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.TrangChuBDTO;
import com.temp.model.TrangChuDTO;
import com.temp.persistence.dao.TrangChuDao;

@Component
public class TrangChuLogic {
	
	@Autowired
	private TrangChuDao dao;
	
	public TrangChuBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc,String trangThai) {
		
		TrangChuBDTO lstPage = new TrangChuBDTO();
		lstPage = dao.filter(pageNo, pageSize, keySort, desc, trangThai);
		return lstPage;
	}
	
	public boolean save(TrangChuDTO dto) {
		if (dto != null) {
			return dao.addOrUpdate(dto);
		}
		return false;

	}
}
