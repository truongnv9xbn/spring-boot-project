package com.temp.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.MessageBDTO;
import com.temp.model.MessageDTO;
import com.temp.persistence.dao.MessageDao;

@Component
public class MessageLogic {
	
	@Autowired
	private MessageDao dao;
	
	public MessageBDTO filter(Integer pageNo, Integer pageSize, String keySort, boolean desc,String trangThai) {
		
		MessageBDTO lstPage = new MessageBDTO();
		lstPage = dao.filter(pageNo, pageSize, keySort, desc, trangThai);

		return lstPage;
	}
	
	public boolean save(MessageDTO dto) {

		// QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
//			if (dto.getId() > 0) {
//				ChucVuDTO oldChucVu = chucVuDao.findById(dto.getId());
//				oldChucVu.setTenChucVu(dto.getTenChucVu());
//				oldChucVu.setMaChucVu(dto.getMaChucVu());
//				oldChucVu.setGhiChu(dto.getGhiChu());
//				oldChucVu.setNhomChucVu(dto.getNhomChucVu());
//				oldChucVu.setDaiDien(dto.getDaiDien());
//				oldChucVu.setTrangThai(dto.getTrangThai());
//				oldChucVu.setBanLanhDao(dto.getBanLanhDao());
//
//				return chucVuDao.addOrUpdateChucVu(oldChucVu);
//
//			}
			// dto.setNguoiTaoId(userInfo.getId());
			return dao.addOrUpdate(dto);
		}

		return false;

	}
}
