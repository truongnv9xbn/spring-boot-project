package com.temp.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.LkNguoiDungNhomDTO;
import com.temp.persistence.dao.NguoiDung.LkNguoiDungNhomDao;

@Component
public class LkNguoiDungNhomLogic {

	@Autowired
	private LkNguoiDungNhomDao LkNguoiDungNhomDao;

	public LkNguoiDungNhomDTO getById(int id) {

		return null;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param LkNguoiDungNhom đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean addOrUpDateLkNguoiDungNhom(List<LkNguoiDungNhomDTO> listLkNguoiDungNhomDTO) {

		return false;

	}

	/**
	 * Thực hiện delete qt nguoi dung
	 * 
	 * @param LkNguoiDungNhomId qt nguoi dung id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean deleteLkNguoiDungNhom(int LkNguoiDungNhomId) {

		return false;
	}

}
