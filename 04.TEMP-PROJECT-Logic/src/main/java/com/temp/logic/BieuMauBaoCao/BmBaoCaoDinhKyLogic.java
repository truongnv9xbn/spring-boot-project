package com.temp.logic.BieuMauBaoCao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDinhKyDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoLichSuDTO;
import com.temp.model.BieuMauBaoCao.KyBcGiaTriBcDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoDinhKyDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoLichSuDao;
import com.temp.utils.TimestampUtils;

@Component
public class BmBaoCaoDinhKyLogic {

    @Autowired
    private BmBaoCaoDinhKyDao bmBaoCaoDinhKyDao;
    
    @Autowired
	private BmBaoCaoLichSuDao bmBaoCaoLichSuDao;
    
    @Autowired
	private BmBaoCaoDao bmBaoCaoDao;

    /**
     * xóa 1 cổ đông
     * 
     * @param BmBaoCaoDinhKyId
     * @return true nếu thành công ngươc lại false không thành công
     */
    public boolean deleteBmBaoCaoDinhKy(int BmBaoCaoDinhKyId) {
	if (BmBaoCaoDinhKyId > 0) {
	    if (this.bmBaoCaoDinhKyDao.deleteBmBaoCaoDinhKyById(BmBaoCaoDinhKyId)) {
		return true;
	    }
	    return false;
	}

	return false;
    }

    /**
     * lấy danh sách cổ đông ctck quan hệ
     * 
     * @param filterkey
     * @param pageNo
     * @param pageSize
     * @param keySort
     * @param desc
     * @return trả về 1 danh sách các cổ đông theo các điều kiện
     */
    public BmBaoCaoDinhKyBDTO listBmBaoCaoDinhKys(String strfilter, Integer CtckId,
	    Integer bmBaoCaoId, String sCMND, Integer pageNo, Integer pageSize, String keySort,
	    boolean desc) {

	QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

	BmBaoCaoDinhKyBDTO lst = this.bmBaoCaoDinhKyDao.getList(strfilter, CtckId, bmBaoCaoId,
		sCMND, userInfo, pageNo, pageSize, keySort, desc);
	return lst;

    }

    /**
     * Thêm mới hoặc cập nhập cổ đông ctck
     * 
     * @param BmBaoCaoDinhKy dữ liệu input
     * @return true nếu cập nhật hoặc update thành công ngược lại là false
     * @throws Exception
     */
    public BmBaoCaoDinhKyDTO addOrUpDateBmBaoCaoDinhKy(BmBaoCaoDinhKyDTO dto) throws Exception {
    	BmBaoCaoDTO bmBaoCao;
	if (dto != null) {
	    if (dto.getId() != null && dto.getId() > 0) {
	    	BmBaoCaoDinhKyDTO updateDto= this.bmBaoCaoDinhKyDao.AddorUpdate(dto);
	    	
	    	if(updateDto.getId()>0) {
	    		bmBaoCao= bmBaoCaoDao.findById(updateDto.getBmBaoCaoId());
				ghiLichSuPhienBan(bmBaoCao, "Cập nhật kỳ báo cáo");
				
				return updateDto;
			}

	    }
	    
	    dto.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
	    BmBaoCaoDinhKyDTO create=this.bmBaoCaoDinhKyDao.AddorUpdate(dto);
	    
	    if(create.getId()>0) {
	    	bmBaoCao= bmBaoCaoDao.findById(create.getBmBaoCaoId());
			ghiLichSuPhienBan(bmBaoCao, "Thêm mới kỳ báo cáo");
			
			return create;
		}

	}
	return new BmBaoCaoDinhKyDTO();

    }

    /**
     * lấy chi tiết 1 cổ đông
     * 
     * @param id định danh id cổ đông
     * @return đối tượng cổ đông
     */
    public BmBaoCaoDinhKyDTO getById(int id) {
	if (id > 0) {
	    BmBaoCaoDinhKyDTO qtDTO = this.bmBaoCaoDinhKyDao.findById(id);
	    if (qtDTO != null) {
//				if (qtDTO.getTyLeNamGiu() != null) {
//					qtDTO.setTyLeNamGiu(qtDTO.getTyLeNamGiu().multiply(new BigDecimal(100)));
//				}
		return qtDTO;
	    }
	}

	return null;
    }

    /**
     * kiểm tra tồn tại object khi update
     * 
     * @param dto
     * @return true nếu tồn tại còn không thì là false
     */
    public boolean isExistBmBaoCaoDinhKyUpdate(BmBaoCaoDinhKyDTO dto) {

//		boolean result = this.bmBaoCaoDinhKyDao.isBmBaoCaoDinhKyExistUpdate(dto.getId(), 0, "",
//				0);

	return true;
    }

    /**
     * kiểm tra trước khi add object
     * 
     * @param dto
     * @return true nếu tồn tại còn không thì là false
     */
    public boolean isExistBmBaoCaoDinhKyAdd(BmBaoCaoDinhKyDTO dto) {

//		boolean result = this.bmBaoCaoDinhKyDao.isBmBaoCaoDinhKyExistAdd("", 0,0);

	return true;
    }

    /**
     * check tồn tại id
     * 
     * @param id
     * @return
     */
    public boolean isExistById(int id) {

	boolean result = false;

	result = this.bmBaoCaoDinhKyDao.isExistById(id);

	return result;
    }

    public List<DropDownDTO> getBaoCaoDinhKyFromBcDropdownLogic(int bieumauId) throws Exception {
	if (bieumauId == 0) {
	    return null;
	}

	return this.bmBaoCaoDinhKyDao.getBaoCaoDinhKyDropDownDao(bieumauId);
    }
    
    public List<KyBcGiaTriBcDTO> getListKyBaoCaoFromBcThanhVien(List<Integer> lstCty) throws Exception {
    	List<KyBcGiaTriBcDTO> lstKyBaoCao = bmBaoCaoDinhKyDao.getListKyBaoCaoFromBcThanhVien(lstCty);
    	for(KyBcGiaTriBcDTO a : lstKyBaoCao) {
    		if(a.getKyBaoCao().equals("N")) {
				a.setKyBaoCao("Ngày");
			}
			else if(a.getKyBaoCao().equals("TU")) {
				a.setKyBaoCao("Tuần");
			}
			else if(a.getKyBaoCao().equals("TH")) {
				a.setKyBaoCao("Tháng");
			}
			else if(a.getKyBaoCao().equals("Q")) {
				a.setKyBaoCao("Quý");
			}
			else if(a.getKyBaoCao().equals("BN")) {
				a.setKyBaoCao("Bán niên");
			}
			else if(a.getKyBaoCao().equals("NAM")) {
				a.setKyBaoCao("Năm");
			}
    	}
    	
    	return lstKyBaoCao;
    }
    
    
    private void ghiLichSuPhienBan(BmBaoCaoDTO dto,String thaoTac) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		
		Integer nguoiThucHien=userInfo.getId();
		Integer bmId=dto.getId();
		Integer bmBaoCaoId=dto.getBmBaoCaoId();
//		Integer suDung=dto.getTrangThai()==null?null:dto.getTrangThai().intValue();
		Integer phienBan=dto.getPhienBan()==null?null:dto.getPhienBan().intValue();
		String tenNguoiThucHien=userInfo.getHoTen();

		BmBaoCaoLichSuDTO bieuMauLichSu= new BmBaoCaoLichSuDTO(userInfo, thaoTac, bmId, bmBaoCaoId, null, phienBan, tenNguoiThucHien);
//				log history version create
				bmBaoCaoLichSuDao.create(bieuMauLichSu);
	}
}
