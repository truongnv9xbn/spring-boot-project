package com.temp.logic.BieuMauBaoCao;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.temp.global.UserInfoGlobal;
import com.temp.model.DataImportExcelBDTO;
import com.temp.model.DataImportExcelDTO;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDTO;
import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDongDTO;
import com.temp.model.BieuMauBaoCao.BcThanhVienDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoLichSuDTO;
import com.temp.model.BieuMauBaoCao.BmSheetBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;
import com.temp.model.BieuMauBaoCao.BmSheetMegerCellDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.BieuMauBaoCao.BcBaoCaoGtDongDao;
import com.temp.persistence.dao.BieuMauBaoCao.BcBaoCaoGtEntityDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoLichSuDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetCotEntityDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetCtEntityDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetEntityDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetHangEntityDao;
import com.temp.utils.Utils;


@Component
public class SheetLogic {

    @Autowired
    private BmBaoCaoDao bmBmBaoCaoDao;

    @Autowired
    private BmSheetEntityDao bmSheetEntityDao;

    @Autowired
    private BmSheetHangEntityDao bmSheetHangEntityDao;

    @Autowired
    private BmSheetCotEntityDao bmSheetCotEntityDao;

    @Autowired
    private BmSheetCtEntityDao bmSheetCtEntityDao;

    @Autowired
    private SheetMegerHeaderLogic logMegerHeader;

    @Autowired
    private BcBaoCaoGtEntityDao bcBaoCaoGtEntityDao;
    
    @Autowired
    private BcBaoCaoGtDongDao bcBaoCaoGtDongEntityDao;

    @Autowired
    private BmBaoCaoLichSuDao bmBaoCaoLichSuDao;

    @Autowired
    private SheetCtLogic sheetCTLogic;

    /**
     * lấy danh sách
     * 
     * @param filterkey
     * @param pageNo
     * @param pageSize
     * @param keySort
     * @param desc
     * @return trả về 1 danh sách theo các điều kiện
     */
    public BmSheetBDTO listSheet(String strfilter, Integer CtckId, Integer bmBaoCaoId, String sCMND,
	    Integer pageNo, Integer pageSize, String keySort, boolean desc) {

	QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

	BmSheetBDTO lst = this.bmSheetEntityDao.listSheet(strfilter, CtckId, bmBaoCaoId, sCMND,
		userInfo, pageNo, pageSize, keySort, desc);
	return lst;

    }
    
    
    public BmSheetBDTO listSheetLazy(String strfilter, Integer CtckId, Integer bmBaoCaoId, String sCMND,
    	    Integer pageNo, Integer pageSize, String keySort, boolean desc) {

    	QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

    	BmSheetBDTO lst = this.bmSheetEntityDao.listSheetLazy(strfilter, CtckId, bmBaoCaoId, sCMND,
    		userInfo, pageNo, pageSize, keySort, desc);
    	return lst;

        }

    /**
     * thêm mới hoặc cập nhật sheet
     * 
     * @param dto
     * @return
     */
    public BmSheetDTO addOrUpdate(BmSheetDTO dto) {
	BmBaoCaoDTO bmBaoCao;
	QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
	if (dto.getId() != null && dto.getId() > 0) {
	    dto.setNgayCapNhat(Utils.getCurrentDate());
	    BmSheetDTO update = this.bmSheetEntityDao.addorUpdate(dto);

	    if (update.getId() > 0) {
		bmBaoCao = update.getBmBaoCaoByBmBaoCaoId();
		ghiLichSuPhienBan(bmBaoCao, "Cập nhật sheet báo cáo");
		return update;
	    }

	}
	dto.setNgayTao(Utils.getCurrentDate());
	dto.setNguoiTaoId(userInfo.getId());
	BmSheetDTO create = this.bmSheetEntityDao.addorUpdate(dto);
	if (create.getId() > 0) {
	    bmBaoCao = create.getBmBaoCaoByBmBaoCaoId();
	    ghiLichSuPhienBan(bmBaoCao, "Thêm mới sheet báo cáo");
	    return create;
	}

	return null;

    }

    private void ghiLichSuPhienBan(BmBaoCaoDTO dto, String thaoTac) {
	QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
	Integer suDung = null;
	Integer nguoiThucHien = userInfo.getId();
	Integer bmId = dto.getId();
	Integer bmBaoCaoId = dto.getBmBaoCaoId();
	Integer phienBan = dto.getPhienBan() == null ? null : dto.getPhienBan().intValue();
	String tenNguoiThucHien = userInfo.getHoTen();

	BmBaoCaoLichSuDTO bieuMauLichSu = new BmBaoCaoLichSuDTO(userInfo, thaoTac, bmId, bmBaoCaoId,
		suDung, phienBan, tenNguoiThucHien);
//				log history version create
	bmBaoCaoLichSuDao.create(bieuMauLichSu);
    }

    /**
     * Tim kiếm dựa trên Id
     * 
     * @param dto
     * @return
     */
    public BmSheetDTO findById(Integer id) {
	return this.bmSheetEntityDao.findById(id);
    }

    /**
     * Tim kiếm dựa trên Id
     * 
     * @param dto
     * @return
     * @throws Exception
     */
    public BmSheetDTO initDisplayEdit(Integer id) throws Exception {
	// region Demo tach formula
	/*
	 * String formula =
	 * "([2840]+[2845]*{SUM,[2840]|[2845]})/(100*[2845]) + {AVG,[2840]|[2845]}";
	 * Pattern pattern = Pattern.compile("\\{(.*?)\\}", Pattern.DOTALL); Matcher
	 * matcher = pattern.matcher(formula); while (matcher.find()) {
	 * sheetCTLogic.findByCongThuc(matcher.group(1)); //
	 * //System.out.println(matcher.group(1)); }
	 */
//   End region demo
	BmSheetDTO dto = this.bmSheetEntityDao.findById(id);
	if (dto != null) {

	    // lấy ds cột
	    List<BmSheetCotDTO> lstCot = this.bmSheetCotEntityDao
		    .listCotExitsGenTable(dto.getBmBaoCaoByBmBaoCaoId().getId(), dto.getId());

	    // lấy ds hàng
	    List<BmSheetHangDTO> lstHang = this.bmSheetHangEntityDao
		    .lstSheetHangExist(dto.getBmBaoCaoByBmBaoCaoId().getId(), dto.getId());

	    // lấy ds cell
	    List<BmSheetCtDTO> lstCell = this.bmSheetCtEntityDao
		    .getLsCellByBaoCaoAndSheet(dto.getBmBaoCaoByBmBaoCaoId().getId(), dto.getId());

	    // lấy header menu
	    BmSheetMegerCellDTO header = this.logMegerHeader
		    .InitHeaderHangCotForExcelForm(dto.getId(), lstCot, true);

	    StringBuilder strTable = new StringBuilder();
	    // nối header
	    strTable.append(header.getHeaderTableStr());
	    strTable.append("<tbody>");
	    if (lstHang != null) {
	    Integer sttHang = 1;
		for (BmSheetHangDTO bmSheetHang : lstHang) {
		    strTable.append("<tr>");
		    strTable.append(
			    "<th style='border: solid 1px;'>" + sttHang + "</th>");
		    sttHang++;
		    if (lstCot != null) {
			for (BmSheetCotDTO sheetCot : lstCot) {

			    if (lstCell != null) {
				Optional<BmSheetCtDTO> cell = lstCell.stream()
					.filter(x -> x.getBmSheetHangId().intValue() == bmSheetHang
						.getId().intValue()
						&& x.getBmSheetCotId().intValue() == sheetCot
							.getId().intValue())
					.findFirst();
				if (cell.isPresent()) {
				    String cellStyle = "";
				    // căn lề
				    if (cell.get().getCanLe() != null) {
					switch (cell.get().getCanLe()) {
					case "CT":
					    cellStyle += "text-align: left;";
					    break;
					case "CP":
					    cellStyle += "text-align: right;";
					    break;
					case "CG":
					    cellStyle += "text-align: center;";
					    break;
					}
				    }
				    if (cell.get().getInDam() != null && cell.get().getInDam()) {
					cellStyle += "font-weight: bold;";
				    }
				    if (cell.get().getInHoa() != null && cell.get().getInHoa()) {
					cellStyle += "text-transform: uppercase;";
				    }
				    if (cell.get().getInNghieng() != null
					    && cell.get().getInNghieng()) {
					cellStyle += "font-style: italic;";
				    }
				    if (cell.get().getGachChan() != null
					    && cell.get().getGachChan()) {
					cellStyle += "text-decoration: underline;";
				    }
				    strTable.append("<td data-id='" + sheetCot.getId() + "|"
					    + bmSheetHang.getId() + "' style='" + cellStyle
					    + "' >");
				    if (cell.get().getDinhDang().equals("LB")) {
					strTable.append("<a data-id='" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "'>" + cell.get().getLabel()
						+ "</a>");
				    } else {
					strTable.append("<a data-id='" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "' >"
						+ bmSheetHang.getMaHang() + "-"
						+ sheetCot.getMaCot() + "</a>");
				    }
				    strTable.append("</td >");
				} else {
				    strTable.append("<td data-id='" + sheetCot.getId() + "|"
					    + bmSheetHang.getId() + "' >");
				    strTable.append(
					    "<button tabindex=\"0\" type=\"button\" data-id='"
						    + sheetCot.getId() + "|" + bmSheetHang.getId()
						    + "' "
						    + "class=\"q-btn inline q-btn-item non-selectable no-outline q-btn--standard q-btn--round bg-primary text-white q-btn--actionable q-focusable q-hoverable\""
						    + " style=\"font-size: 7px;\">"
						    + "<div tabindex=\"-1\" class=\"q-focus-helper\">"
						    + "</div>"
						    + "<div class=\"q-btn__content text-center col items-center q-anchor--skip justify-center row\">"
						    + "<i aria-hidden=\"true\" class=\"material-icons q-icon\" data-id='"
						    + sheetCot.getId() + "|" + bmSheetHang.getId()
						    + "'>edit</i>" + "</div></button>");
				    strTable.append("</td >");

				}
			    } else {
				strTable.append("<td  data-id='" + sheetCot.getId() + "|"
					+ bmSheetHang.getId() + "' >");
				strTable.append("<button tabindex=\"0\" type=\"button\" data-id='"
					+ sheetCot.getId() + "|" + bmSheetHang.getId() + "' "
					+ "class=\"q-btn inline q-btn-item non-selectable no-outline q-btn--standard q-btn--round bg-primary text-white q-btn--actionable q-focusable q-hoverable\""
					+ " style=\"font-size: 7px;\">"
					+ "<div tabindex=\"-1\" class=\"q-focus-helper\">"
					+ "</div>"
					+ "<div class=\"q-btn__content text-center col items-center q-anchor--skip justify-center row\">"
					+ "<i aria-hidden=\"true\" class=\"material-icons q-icon\" data-id='"
					+ sheetCot.getId() + "|" + bmSheetHang.getId()
					+ "'>edit</i>" + "</div></button>");
				strTable.append("</td >");
			    }
			}
			strTable.append("</tr>");
		    }
		}
	    }
	    strTable.append("</tbody></table>");
	    dto.setTableHtmlRaw(strTable.toString());
	    return dto;
	}
	return null;
    }

    public BmSheetDTO initDisplayMergeTitle(Integer id) throws Exception {

	BmSheetDTO dto = this.bmSheetEntityDao.findById(id);
	if (dto != null) {
	    // lấy header menu
	    BmSheetMegerCellDTO header = this.logMegerHeader
		    .InitHeaderHangCotForExcelForm(dto.getId(), null, true);
	    return dto;
	}
	return null;
    }

    /**
     * Delete item base on id
     * 
     * @param dto
     * @return
     * @throws Exception
     */
    public boolean deleteById(Integer id) throws Exception {

	return this.bmSheetEntityDao.deleteById(id);

    }

    /**
     * Check trung ma sheet
     * 
     * @param dto
     * @return
     */
    public boolean checkExistMaSheet(String sheet, Integer sheetId, Integer bmBaoCaoId) {

	return this.bmSheetEntityDao.checkExistMaSheet(sheet, sheetId, bmBaoCaoId);
    }
    
    /**
     * Check trung ma sheet liên thông
     * 
     * @param dto
     * @return
     */
    public boolean checkExistMaSheetLienThong(String sheet, Integer sheetId, Integer bmBaoCaoId) {

	return this.bmSheetEntityDao.checkExistMaSheetLienThong(sheet, sheetId, bmBaoCaoId);
    }

    /**
     * Check trung ma sheet update
     * 
     * @param dto
     * @return
     */
    public boolean checkExistMaSheetUpdate(String sheet, Integer id) {

	return this.bmSheetEntityDao.checkExistMaSheetUpdate(sheet, id);
    }

    /**
     * DropDown Sheet
     * 
     * @param id
     * @return
     */
    public List<DropDownDTO> lstDsSheet(Integer id) {
	return this.bmSheetEntityDao.dsSheet(id);
    }

    /**
     * Get Sheet From Bao cao Dropdown Logic
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    public List<DropDownDTO> getSheetFromBcDropdownLogic(int baocaoId) throws Exception {
	return this.lstDsSheet(baocaoId);
    }

    /**
     * Lay noi dung bao cao sheet e-form logic
     * 
     * @param id
     * @return
     */
    public BmSheetDTO getNoiDungBaoCaoSheetIdLogic(Integer id, String giaTriKyBaoCao, Integer baoCaoId,
	    int thanhVienId) throws Exception {
	BmSheetDTO sheetDto = this.bmSheetEntityDao.findById(id);
	if (sheetDto != null) {

	    // lấy ds hàng
	    List<BmSheetHangDTO> lstHang = this.bmSheetHangEntityDao.lstSheetHangExist(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy ds cột
	    List<BmSheetCotDTO> lstCot = this.bmSheetCotEntityDao.listCotExitsGenTable(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy header menu
	    BmSheetMegerCellDTO header = this.logMegerHeader
		    .InitHeaderHangCotForExcelForm(sheetDto.getId(), lstCot, false);

	    // lấy ds cell
	    List<BmSheetCtDTO> lstCell = this.bmSheetCtEntityDao.getLsCellByBaoCaoAndSheet(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy ds gia tri
	    List<BcBaoCaoGtDTO> lstBaocaoGt = this.bcBaoCaoGtEntityDao.getListBaoCaoGiaTri(baoCaoId,
		    thanhVienId, sheetDto.getId());
	    // lấy ds hàng động
	    BcBaoCaoGtDongDTO eBaoCaoGtDong = this.bcBaoCaoGtDongEntityDao.getHangDong(thanhVienId, sheetDto.getId());
	    List<DataImportExcelDTO> lstGtDong = new ArrayList<DataImportExcelDTO>();
	    if(eBaoCaoGtDong != null) {
	    	Gson gson = new Gson();
		    Type userListType = new TypeToken<List<DataImportExcelDTO>>() {
		    }.getType();
		    lstGtDong = gson.fromJson(eBaoCaoGtDong.getGiaTri(), userListType);
	    }
	    StringBuilder strTable = new StringBuilder();
	    // create header
	    strTable.append(header.getHeaderTableStr());
	    strTable.append("<tbody>");
	    if (lstHang != null) {
	    // lap theo list hang cua sheet
		for (BmSheetHangDTO bmSheetHang : lstHang) {
		    strTable.append("<tr>");
		    //Lap theo list cot cua sheet
		    for (BmSheetCotDTO sheetCot : lstCot) {
		    //Doi voi truong hop list chi tieu khac null
			if (lstCell != null) {
			    Optional<BmSheetCtDTO> cell = lstCell.stream()
				    .filter(x -> x.getBmSheetHangId().intValue() == bmSheetHang
					    .getId().intValue()
					    && x.getBmSheetCotId().intValue() == sheetCot.getId()
						    .intValue()).findFirst();
			    if (cell.isPresent()) {

				// Neu khong co du lieu da nhap, hien thi o textbox trong.
				if (lstBaocaoGt == null) {
				    // Neu o nhap la dinh dang kieu Number || Tien te dat dang input
				    // text
				    if (("S").equals(cell.get().getDinhDang())
					    || ("TT").equals(cell.get().getDinhDang())
					    || ("TLPT").equals(cell.get().getDinhDang())) {
				    	//Them o input theo dinh dang
					strTable.append("<td  data-id='" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "' >");
					strTable.append(" <input ");
					//ToDo: Check cell neu bat buoc thi them data attribute required = 1
					if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
						strTable.append(" data-required=\"1\" ");
					}
					strTable.append(" class=\"form-control gt-cell\" ");
					strTable.append(" type=\"number\" ");
					int bmSheetCtVaoId = (cell.get()
						.getBmSheetCtVaoId()) == null ? 0
							: cell.get().getBmSheetCtVaoId().intValue();
					strTable.append(" id=\"" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "|" + cell.get().getId()
						+ "-" + bmSheetCtVaoId + "|0\"");
					strTable.append(
						" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					strTable.append(" />");

				    }
				    
				    //Neu dinh dang la kieu Ky tu KT 
				    if (("KT").equals(cell.get().getDinhDang())) {
					strTable.append("<td  data-id='" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "' >");
					strTable.append(" <input ");
					//ToDo: Check cell neu bat buoc thi them data attribute required = 1
					if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
						strTable.append(" data-required=\"1\" ");
					}
					strTable.append(" class=\"form-control gt-cell\" ");
					strTable.append(" type=\"text\" ");
					int bmSheetCtVaoId = (cell.get()
						.getBmSheetCtVaoId()) == null ? 0
							: cell.get().getBmSheetCtVaoId().intValue();
					strTable.append(" id=\"" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "|" + cell.get().getId()
						+ "-" + bmSheetCtVaoId + "|0\"");
					strTable.append(
						" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					strTable.append(" /></td>");
				    }

				    //Neu dinh dang la kieu tieu de LB
				    if (("LB").equals(cell.get().getDinhDang())) {
					strTable.append("<td>");
					strTable.append(cell.get().getLabel() == null ? ""
						: cell.get().getLabel());
					strTable.append("</td>");
				    }

				    // Neu khong cau hinh o chi tieu, hien thi cot trong.
				    if (null == (cell.get().getDinhDang())) {
					strTable.append("<td>");
					strTable.append("</td>");
				    }
				    // Neu co gia tri, hien thi o textbox kem gia tri.
				} 
				// Doi voi truong hop list chi tieu = null
				else {
				    // Lay gia tri theo hang - cot
				    Optional<BcBaoCaoGtDTO> bcGiaTri = lstBaocaoGt.stream()
					    .filter(f -> (f.getBmSheetHangId() != null ? (f
						    .getBmSheetHangId()
						    .intValue() == bmSheetHang.getId().intValue())
						    : false)
						    && (f.getBmSheetCotId() != null
							    ? (f.getBmSheetCotId()
								    .intValue() == sheetCot.getId()
									    .intValue())
							    : false)
						    && (f.getBmSheetCtId() != null
							    ? (f.getBmSheetCtId().intValue() == cell
								    .get().getId().intValue())
							    : false))
					    .findFirst();
				    // Neu co gia tri theo filter 
				    if (bcGiaTri.isPresent()) {
					// Neu cell la So/Tien te. hien thi du lieu kieu So/TienTe.
					if (("S").equals(cell.get().getDinhDang())
						|| ("TT").equals(cell.get().getDinhDang())
						|| ("TLPT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
						//ToDo: Check cell neu bat buoc thi them data attribute required = 1
						if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
							strTable.append(" data-required=\"1\" ");
						}
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"number\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|"
						    + bcGiaTri.get().getId() + "\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" value = \""
						    + bcGiaTri.get().getGiaTri() + "\"");
					    strTable.append(" /> </td>");
					}

					// Neu la kieu ky tu, hien thi o textbox kieu text
					if (("KT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
					  //ToDo: Check cell neu bat buoc thi them data attribute required = 1
					    if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
							strTable.append(" data-required=\"1\" ");
						}
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"text\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|"
						    + bcGiaTri.get().getId() + "\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" value = \""
						    + bcGiaTri.get().getGiaTri() + "\"");
					    strTable.append(" /></td>");
					}

					// Neu la kieu label, hien thi label.
					if (("LB").equals(cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append(cell.get().getLabel() == null ? ""
						    : cell.get().getLabel());
					    strTable.append("</td>");
					}

					// Neu khong cau hinh o chi tieu, hien thi cot trong.
					if (null == (cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append("</td>");
					}
				    }
				    // Case gen cell input case BcGiaTri Null.
				    else {
					// Neu o nhap la dinh dang kieu Number || Tien te dat dang
					// input
					// text
					if (("S").equals(cell.get().getDinhDang())
						|| ("TT").equals(cell.get().getDinhDang())
						|| ("TLPT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
					    
						//ToDo: Check cell neu bat buoc thi them data attribute required = 1
					    if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
							strTable.append(" data-required=\"1\" ");
						}
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"number\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|0\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" /></td>");

					}

					if (("KT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
						//ToDo: Check cell neu bat buoc thi them data attribute required = 1
					    if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
							strTable.append(" data-required=\"1\" ");
						}
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"text\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|0\"");
					    strTable.append(" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" /></td>");
					}

					if (("LB").equals(cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append(cell.get().getLabel() == null ? ""
						    : cell.get().getLabel());
					    strTable.append("</td>");
					}

					// Neu khong cau hinh o chi tieu, hien thi cot trong.
					if (null == (cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append("</td>");
					}
					// Neu co gia tri, hien thi o textbox kem gia tri.
				    }

				}

			    } else {
				strTable.append("<td  data-id='" + sheetCot.getId() + "|"
					+ bmSheetHang.getId() + "' >");
				strTable.append("");
				strTable.append("</td >");

			    }
			}
		    }
		    strTable.append("</tr>");
		    //Load dữ liệu hàng động
		    if(lstGtDong != null && lstGtDong.size() > 0) {
		    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
//		    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang() == bmSheetHang.getId()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
		    	if(lstHangDong != null && lstHangDong.size() > 0) {
		    		for(DataImportExcelDTO hangDong : lstHangDong) {
		    			strTable.append("<tr "
		    						+ "data-hangdong='1' data-hang-index='" + hangDong.getIndexCellHang() + "'>");
		    			for(int i = 0; i < lstCot.size(); i++) {
		    				int gtCotIndex = i;
		    				Optional<DataImportExcelDTO> objGTDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue() && x.getIndexCellCot().intValue() == gtCotIndex 
		    						&& x.getIndexCellHang().intValue() == hangDong.getIndexCellHang().intValue()).findFirst();
		    				if(objGTDong.isPresent()) {
		    					DataImportExcelDTO tmpGtDong = objGTDong.get();
		    					strTable.append("<td "
		    									+ "data-hangdong='1' data-id='" 
		    									+ tmpGtDong.getIndexCellCot() + "|" 
		    									+ tmpGtDong.getIndexCellHang() + "'> "
		    									+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" id=\"" 
		    									+ tmpGtDong.getBmBaoCaoId() + "|" 
		    									+ tmpGtDong.getBmSheetHang() + "|" 
		    									+ tmpGtDong.getIndexCellCot() + "|" + tmpGtDong.getIndexCellHang() 
		    									+ "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" value=\"" + tmpGtDong.getGiaTri() 
		    									+ "\" /></td>");
		    				}
		    				else {
		    					strTable.append("<td data-hangdong='1' data-id='" + i + "|" + hangDong.getIndexCellHang() + "'> "
		    									+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" "
		    									+ "id=\"" + hangDong.getBmBaoCaoId() + "|" + hangDong.getBmSheetHang() 
		    									+ "|" + i + "|" + hangDong.getIndexCellHang() 
		    									+ "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" />");
		    									strTable.append("</td >");
		    				}
		    			}
		    			strTable.append("</tr>");
		    		}
		    	}
		    }
		    //Xử lý button hàng động
		    if(bmSheetHang.getHangDong()) {
		    	strTable.append(this.getButtonThemHangDong(lstCot.size(), bmSheetHang.getId()));
//		    	strTable.append("<tr><td colspan='" + lstCot.size() + "'>");
//		    	strTable.append("<button tabindex=\"0\" type=\"button\" role=\"button\" data-coltotal=\"" + lstCot.size() + "\" "
//		    			+ "class=\"q-btn q-btn-item non-selectable no-outline q-btn--push q-btn--rectangle bg-brown-5 text-white q-btn--actionable q-focusable q-hoverable q-btn--wrap\" title=\"Thêm hàng\" style=\"font-size: 12px; text-decoration: none;\">"
//		    			+ "<div tabindex=\"-1\" class=\"q-focus-helper\"></div><div class=\"q-btn__wrapper col row q-anchor--skip\">"
//		    			+ "<div class=\"q-btn__content text-center col items-center q-anchor--skip justify-center row\">"
//		    			+ "<i aria-hidden=\"true\" role=\"img\" class=\"material-icons q-icon notranslate on-left\">add_circle_outline</i>"
//		    			+ "<div>Thêm hàng</div></div></div></button>");
//		    	strTable.append("</td></tr>");
		    }
		}
		strTable.append("</tbody></table>");
		sheetDto.setTableHtmlRaw(strTable.toString());
		return sheetDto;
	    }
	}
	return null;
    }

    /**
     * Lay danh sach sheet theo bao cao.
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    public List<DropDownDTO> getlsSheetbyBaoCaoLogic(Integer baocaoId) throws Exception {
	List<DropDownDTO> lsDropDown = this.getSheetFromBcDropdownLogic(baocaoId);

	return lsDropDown;
    }

    /**
     * Lay gia tri bao cao sheet logic.
     * 
     * @param sheetId
     * @param kyBaoCaoId
     * @param baoCaoId
     * @return
     * @throws Exception
     */
    public BmSheetDTO getGiaTriBaoCaoSheetIdLogic(Integer sheetId, Integer thanhVienId,
	    Integer baoCaoId) throws Exception {
	BmSheetDTO sheetDto = this.bmSheetEntityDao.findById(sheetId);

	if (sheetDto != null) {

	    // lấy ds hàng
	    List<BmSheetHangDTO> lstHang = this.bmSheetHangEntityDao.lstSheetHangExist(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy ds cột
	    List<BmSheetCotDTO> lstCot = this.bmSheetCotEntityDao.listCotExitsGenTable(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy header menu
	    BmSheetMegerCellDTO header = this.logMegerHeader
		    .InitHeaderHangCotForExcelForm(sheetDto.getId(), lstCot, false);

	    // lấy ds cell
	    List<BmSheetCtDTO> lstCell = this.bmSheetCtEntityDao.getLsCellByBaoCaoAndSheet(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy ds gia tri
	    List<BcBaoCaoGtDTO> lstBaocaoGt = this.bcBaoCaoGtEntityDao.getListBaoCaoGiaTri(baoCaoId,
		    thanhVienId, sheetDto.getId());
	    
	 // lấy ds hàng động
	    BcBaoCaoGtDongDTO eBaoCaoGtDong = this.bcBaoCaoGtDongEntityDao.getHangDong(thanhVienId, sheetDto.getId());
	    List<DataImportExcelDTO> lstGtDong = new ArrayList<DataImportExcelDTO>();
	    if(eBaoCaoGtDong != null) {
	    	Gson gson = new Gson();
		    Type userListType = new TypeToken<List<DataImportExcelDTO>>() {}.getType();
		    lstGtDong = gson.fromJson(eBaoCaoGtDong.getGiaTri(), userListType);
	    }
	    
	    StringBuilder strTable = new StringBuilder();
	    
	    // create sheet header
	    strTable.append(header.getHeaderTableStr());
	    strTable.append("<tbody>");
	    if (lstHang != null) {
		for (BmSheetHangDTO bmSheetHang : lstHang) {
		    strTable.append("<tr>");
		    for (BmSheetCotDTO sheetCot : lstCot) {

			if (lstCell != null) {
			    Optional<BmSheetCtDTO> cell = lstCell.stream()
				    .filter(x -> x.getBmSheetHangId().intValue() == bmSheetHang.getId().intValue() && x.getBmSheetCotId().intValue() == sheetCot.getId() .intValue()).findFirst();
			    if (cell.isPresent()) {
				// Get sheet cell gia tri theo Hang - Cot - Cell (ID).
//				// TODO bao gom lay theo ky bao cao
//			    
//			    if(cell.get().getBatBuoc()!= null && cell.get().getBatBuoc().booleanValue()) {
//			    	
//			    }
			    
				if (lstBaocaoGt == null) {
				    // Neu cell la Label, hien thi label.
				    if (("LB").equals(cell.get().getDinhDang())) {
//						strTable.append("<td>");
//						strTable.append(cell.get().getLabel());
//						strTable.append("</td>");
				    	
				    	if(strTable.indexOf("Chỉ tiêu")>=0) {
				    		strTable.append("<td style='white-space:pre-wrap !important'>");
				    	} else {
				    		strTable.append("<td>");
				    	}
						strTable.append(cell.get().getLabel());
						strTable.append("</td>");
				    } else {
						strTable.append("<td>");
						strTable.append(" ");
						strTable.append("</td>");
				    }
				    continue;
				}

				Optional<BcBaoCaoGtDTO> bcGiaTri = lstBaocaoGt.stream()
					.filter(f -> (f.getBmSheetHangId() != null ? (f.getBmSheetHangId().intValue() == bmSheetHang.getId().intValue()): false)
						&& (f.getBmSheetCotId() != null ? (f.getBmSheetCotId().intValue() == sheetCot.getId().intValue()): false)
						&& (f.getBmSheetCtId() != null ? (f.getBmSheetCtId().intValue() == cell.get().getId().intValue()): false)).findFirst();

					// Neu cell co chua gia tri, fill gia tri vao bieu mau
					if (bcGiaTri.isPresent()) {
					    strTable.append("<td  data-id='" + sheetCot.getId() + "|"+ bmSheetHang.getId() + "' >");
					    strTable.append(Utils.formatDecimal(bcGiaTri.get().getGiaTri()));
					    strTable.append("</td>");
					} else {
					    // Neu cell la Label, hien thi label.
					    if (("LB").equals(cell.get().getDinhDang())) {
							strTable.append("<td>");
							strTable.append(cell.get().getLabel());
							strTable.append("</td>");
					    } else {
							strTable.append("<td>");
							strTable.append(" ");
							strTable.append("</td>");
					    }
					}
			    }
			    else {
				strTable.append("<td  data-id='" + sheetCot.getId() + "|"
					+ bmSheetHang.getId() + "' >");
				strTable.append(" ");
				strTable.append("</td >");

			    }
			}
		    }
		    strTable.append("</tr>");

		    //Load dữ liệu hàng động
		    if(lstGtDong != null && lstGtDong.size() > 0) {
		    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
//		    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang() == bmSheetHang.getId()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
		    	if(lstHangDong != null && lstHangDong.size() > 0) {
		    		for(DataImportExcelDTO hangDong : lstHangDong) {
		    			strTable.append("<tr data-hangdong='1' data-hang-index='" + hangDong.getIndexCellHang() + "'>");
		    			for(int i = 0; i < lstCot.size(); i++) {
		    				int gtCotIndex = i;
		    				Optional<DataImportExcelDTO> objGTDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue() && x.getIndexCellCot().intValue() == gtCotIndex 
		    						&& x.getIndexCellHang().intValue() == hangDong.getIndexCellHang().intValue()).findFirst();
		    				if(objGTDong.isPresent()) {
		    					DataImportExcelDTO tmpGtDong = objGTDong.get();
		    					strTable.append("<td data-hangdong='1' data-id='" + tmpGtDong.getIndexCellCot() + "|" + tmpGtDong.getIndexCellHang() + "'> "
		    	    					+ Utils.formatDecimal(tmpGtDong.getGiaTri()) + "</td>"); // truongnv3 format giá trị động
		    				}
		    				else {
		    					strTable.append("<td></td>");
//		    					strTable.append("<td data-hangdong='1' data-id='" + i + "|" + hangDong.getIndexCellHang() + "'> "
//		    	    					+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" id=\"" + hangDong.getBmBaoCaoId() + "|" + hangDong.getBmSheetHang() 
//		    	    					+ "|" + i + "|" + hangDong.getIndexCellHang() + "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" /></td>");
		    				}
		    			}
		    			strTable.append("</tr>");
		    		}
		    	}
		    }
		}
		strTable.append("</tbody></table>");
		sheetDto.setTableHtmlRaw(strTable.toString());
		return sheetDto;
	    }

	}
	return null;
    }

    /**
     * Get sheet form data from import excel file logic
     * 
     * @param result
     * @return
     * @throws Exception
     */
    public List<BmSheetDTO> buildSheetDataFromImportExcelFileLogic(List<DataImportExcelDTO> result,
	    List<DropDownDTO> lsSheetDropDown, int baoCaoId) throws Exception {
	List<BmSheetDTO> outputLsSheetDTO = new ArrayList<BmSheetDTO>();
	BmSheetDTO outputSheetDto = null;

	// Get dynamic sheet detail each sheet
	for (DropDownDTO lsSheetId : lsSheetDropDown) {

	    StringBuilder htmlSheetDynamic = new StringBuilder();

	    // Kiem tra ton tai cua sheet
	    outputSheetDto = new BmSheetDTO();
	    outputSheetDto = this.findById(lsSheetId.getValue());

	    if (outputSheetDto == null) {
		continue;
	    }

	    // Get list import data by sheet id
	    List<DataImportExcelDTO> lsDataBySheetId = result.stream()
		    .filter(f -> f.getBmSheetId().intValue() == lsSheetId.getValue().intValue())
		    .collect(Collectors.toList());
	    if (lsDataBySheetId != null && lsDataBySheetId.size() > 0) {

		// get list hang import
		List<BmSheetHangDTO> lsHangExcel = this.bmSheetHangEntityDao
			.lstSheetHangExist(baoCaoId, lsSheetId.getValue());

		// get list cot import
		List<BmSheetCotDTO> lsCotExcel = this.bmSheetCotEntityDao
			.listCotExitsGenTable(baoCaoId, lsSheetId.getValue());

		// lấy header menu sheet
		BmSheetMegerCellDTO header = this.logMegerHeader
			.InitHeaderHangCotForExcelForm(outputSheetDto.getId(), lsCotExcel, false);

		htmlSheetDynamic.append(header.getHeaderTableStr());
		htmlSheetDynamic.append("<tbody>");

		// lấy ds cell
		List<BmSheetCtDTO> lstCell = this.bmSheetCtEntityDao
			.getLsCellByBaoCaoAndSheet(baoCaoId, lsSheetId.getValue());

		if (lsHangExcel == null || lsCotExcel == null) {
		    continue;
		}

		// Buil table sheet theo hang - cot
		for (BmSheetHangDTO hangExcel : lsHangExcel) {
		    htmlSheetDynamic.append("<tr>");
		    for (BmSheetCotDTO cotExcel : lsCotExcel) {
			if (lstCell == null) {
			    continue;
			}

			Optional<BmSheetCtDTO> cell = lstCell.stream().filter(x -> x
				.getBmSheetHangId().intValue() == hangExcel.getId().intValue()
				&& x.getBmSheetCotId().intValue() == cotExcel.getId().intValue())
				.findFirst();

			Optional<DataImportExcelDTO> giaTriCell = lsDataBySheetId.stream().filter(f -> f.getHang().getId().intValue() == hangExcel.getId().intValue()
					&& f.getCot().getId().intValue() == cotExcel.getId()
						.intValue())
				.findFirst();

			if (cell.isPresent()) {
			    int bmSheetCtVaoId = (cell.get().getBmSheetCtVaoId()) == null ? 0
				    : cell.get().getBmSheetCtVaoId().intValue();
			    if (giaTriCell.isPresent()) {
				if (giaTriCell.get().getChiTieu() != null) {

				    if (("S").equals(cell.get().getDinhDang())
					    || ("TT").equals(cell.get().getDinhDang())
					    || ("TLPT").equals(cell.get().getDinhDang())) {

					// Get cell input type numeric validation
					this.getCellInputNumericValidate(htmlSheetDynamic,
						hangExcel.getId(), cotExcel.getId(), cell.get(),
						giaTriCell.get());
				    }

				    if (("KT").equals(cell.get().getDinhDang())) {
					htmlSheetDynamic.append("<td  data-id='" + cotExcel.getId()
						+ "|" + hangExcel.getId() + "' >");
					htmlSheetDynamic.append(" <input ");
					htmlSheetDynamic.append(" class=\"form-control gt-cell\" ");
					htmlSheetDynamic.append(" type=\"text\" ");
					htmlSheetDynamic.append(" id=" + cotExcel.getId() + "|"
						+ hangExcel.getId() + "|" + cell.get().getId() + "-"
						+ bmSheetCtVaoId + "|"
						+ giaTriCell.get().getBcBaoCaoGtId());
					htmlSheetDynamic.append(" value = \" "
						+ giaTriCell.get().getGiaTri() + "\"");
					htmlSheetDynamic.append(
						" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					htmlSheetDynamic.append(" />");
					htmlSheetDynamic.append("</td >");
				    }

				    // Neu cot label, hien thi label
				    if (("LB").equals(cell.get().getDinhDang())) {
					htmlSheetDynamic.append("<td>");
					htmlSheetDynamic.append(cell.get().getLabel());
					htmlSheetDynamic.append("</td>");
				    }

				    // Neu khong cau hinh o chi tieu, hien thi cot trong.
				    if (null == (cell.get().getDinhDang())) {
					htmlSheetDynamic.append("<td>");
					htmlSheetDynamic.append("</td>");
				    }

				} else {
				    htmlSheetDynamic.append("<td>");
				    htmlSheetDynamic.append(giaTriCell.get().getGiaTri());
				    htmlSheetDynamic.append("</td>");
				}
			    }
			} else {
			    if (giaTriCell.isPresent()) {
				htmlSheetDynamic.append("<td  data-id='" + cotExcel.getId() + "|"
					+ hangExcel.getId() + "' >");
				htmlSheetDynamic.append(giaTriCell.get().getGiaTri());
				htmlSheetDynamic.append("</td >");
			    } else {
				htmlSheetDynamic.append("<td></td>");
			    }
			}

		    }
		    htmlSheetDynamic.append("</tr>");
		}
		htmlSheetDynamic.append("</tbody></table>");
		outputSheetDto.setTableHtmlRaw(htmlSheetDynamic.toString());
		outputLsSheetDTO.add(outputSheetDto);
	    }
	}
	return outputLsSheetDTO;
    }

    /**
     * Get cell input numeric validation.
     * 
     * @param htmlSheetDynamic
     * @param hangExcel
     * @param cotExcel
     * @param cell
     * @param dataImportExcelDTO
     */
    private void getCellInputNumericValidate(StringBuilder htmlSheetDynamic, Integer hangExcel,
	    Integer cotExcel, BmSheetCtDTO cell, DataImportExcelDTO giaTriCell) {

		int bmSheetCtVaoId = (cell.getBmSheetCtVaoId()) == null ? 0
			: cell.getBmSheetCtVaoId().intValue();
		
//		if (StringUtils.isEmpty(giaTriCell.getGiaTri()) && cell.getBatBuoc()){
//			// display cell error.
//		    htmlSheetDynamic.append("<td  data-id='" + cotExcel + "|" + hangExcel + "' >");
//		    htmlSheetDynamic.append(" <input ");
//		    htmlSheetDynamic.append(" class=\"form-control gt-cell\" ");
//		    htmlSheetDynamic.append(" type=\"number\" ");
//		    htmlSheetDynamic.append(" id=" + cotExcel + "|" + hangExcel + "|" + cell.getId() + "-"
//			    + bmSheetCtVaoId + "|" + giaTriCell.getBcBaoCaoGtId());
//		    htmlSheetDynamic.append(" style=\"height: 70%; border: 1px dotted red;\" ");
//		    // htmlSheetDynamic.append(" value = \"" + giaTriCell.getGiaTri() + "\"");
//		    htmlSheetDynamic.append(" />");
//		    // display error message.
//		    htmlSheetDynamic.append("<br/><span style=\"font-size:11px;color:red;\">");
//		    htmlSheetDynamic.append("Không được để trống.");
//		    htmlSheetDynamic.append("</td >");
//		    
//		}
		if (StringUtils.isEmpty(giaTriCell.getGiaTri()) || !StringUtils.isEmpty(giaTriCell.getGiaTri()) && NumberUtils.isParsable(giaTriCell.getGiaTri())) {
			//validate require neu cell bat buoc 	
			if(StringUtils.isEmpty(giaTriCell.getGiaTri()) && cell.getBatBuoc() != null && cell.getBatBuoc().booleanValue()) {
				// display cell error.
			    htmlSheetDynamic.append("<td  data-id='" + cotExcel + "|" + hangExcel + "' >");
			    htmlSheetDynamic.append(" <input ");
			    htmlSheetDynamic.append(" class=\"form-control gt-cell\" ");
			    htmlSheetDynamic.append(" type=\"number\" ");
			    htmlSheetDynamic.append(" id=" + cotExcel + "|" + hangExcel + "|" + cell.getId() + "-" + bmSheetCtVaoId + "|" + giaTriCell.getBcBaoCaoGtId());
			    htmlSheetDynamic.append(" style=\"height: 70%; border: 1px dotted red;\" ");
			    // htmlSheetDynamic.append(" value = \"" + giaTriCell.getGiaTri() + "\"");
			    htmlSheetDynamic.append(" />");
			    // display error message.
			    htmlSheetDynamic.append("<br/><span style=\"font-size:11px;color:red;\">");
			    htmlSheetDynamic.append("Không được để trống.");
			    htmlSheetDynamic.append("</td >");
			}
			else {
			    htmlSheetDynamic.append("<td  data-id='" + cotExcel + "|" + hangExcel + "' >");
			    htmlSheetDynamic.append(" <input ");
			    htmlSheetDynamic.append(" class=\"form-control gt-cell\" ");
			    htmlSheetDynamic.append(" type=\"number\" ");
			    htmlSheetDynamic.append(" id=" + cotExcel + "|" + hangExcel + "|" + cell.getId() + "-"
				    + bmSheetCtVaoId + "|" + giaTriCell.getBcBaoCaoGtId());
			    htmlSheetDynamic
				    .append(" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
			    htmlSheetDynamic.append(" value = \"" + giaTriCell.getGiaTri() + "\"");
			    htmlSheetDynamic.append(" />");
			    htmlSheetDynamic.append("</td >");
			}
		} else {
		    // display cell error.
		    htmlSheetDynamic.append("<td  data-id='" + cotExcel + "|" + hangExcel + "' >");
		    htmlSheetDynamic.append(" <input ");
		    htmlSheetDynamic.append(" class=\"form-control gt-cell\" ");
		    htmlSheetDynamic.append(" type=\"number\" ");
		    htmlSheetDynamic.append(" id=" + cotExcel + "|" + hangExcel + "|" + cell.getId() + "-"
			    + bmSheetCtVaoId + "|" + giaTriCell.getBcBaoCaoGtId());
		    htmlSheetDynamic.append(" style=\"height: 70%; border: 1px dotted red;\" ");
		    // htmlSheetDynamic.append(" value = \"" + giaTriCell.getGiaTri() + "\"");
		    htmlSheetDynamic.append(" />");
		    // display error message.
		    htmlSheetDynamic.append("<br/><span style=\"font-size:11px;color:red;\">");
		    htmlSheetDynamic.append(
			    " Giá trị <b>" + giaTriCell.getGiaTri() + "</b> không đúng định dạng kiểu số.");
		    htmlSheetDynamic.append("</td >");
		}

    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
	Set<Object> seen = ConcurrentHashMap.newKeySet();
	return t -> seen.add(keyExtractor.apply(t));
    }

    /**
     * [TEST-METHOD]
     * 
     * @param kyBaoCaoId
     * @param baoCaoId
     * @param lsSheetIds
     * @return
     * @throws Exception
     */
    public List<BmSheetDTO> testGetSheetFormDataLogic(String giaTriKyBaoCao, Integer baoCaoId,
	    List<DropDownDTO> lsSheetIds) throws Exception {
	List<BmSheetDTO> ouputSheetForm = new ArrayList<BmSheetDTO>();

	for (DropDownDTO sheet : lsSheetIds) {
	    ouputSheetForm.add(
		    this.getNoiDungBaoCaoSheetIdLogic(sheet.getValue(), giaTriKyBaoCao, baoCaoId, 0));
	}
	return ouputSheetForm;
    }

    public boolean checkExistBia(Integer bmBaoCaoId) {

	return this.bmSheetEntityDao.checkExistBia(bmBaoCaoId);
    }

    public BmSheetDTO getSheetBia(Integer bmBaoCaoId) {

	BmSheetDTO result = bmSheetEntityDao.getSheetBia(bmBaoCaoId);
	BmBaoCaoDTO bmBaoCao = null;
	if (result == null) {
	    result = new BmSheetDTO();
	    bmBaoCao = bmBmBaoCaoDao.findById(bmBaoCaoId);
	    result.setBmBaoCaoByBmBaoCaoId(bmBaoCao);
	}
	return result;
    }

    /**
     * Build dynamic sheet form data import excel file logic
     * 
     * @param result
     * @return
     */
    public List<BmSheetDTO> buildSheetDynamicImportExcelLogic(DataImportExcelBDTO result,
	    List<DropDownDTO> lsSheetIds) throws Exception {
	List<BmSheetDTO> outputSheetFormLs = new ArrayList<BmSheetDTO>();

	for (DropDownDTO sheetDto : lsSheetIds) {

	    // set output sheet form data.
	    BmSheetDTO outputSheetForm = new BmSheetDTO();
	    outputSheetForm.setId(sheetDto.getValue());

	    // filter data imported by sheet id.
	    List<DataImportExcelDTO> listDataBySheet = result.getListData().stream()
		    .filter(f -> (f.getBmSheetId() != null)
			    && f.getBmSheetId().intValue() == sheetDto.getValue().intValue())
		    .collect(Collectors.toList());

	    // check list data importd by sheet exist,
	    if (listDataBySheet == null || listDataBySheet.isEmpty()) {
		continue;
	    }

	    // get tong hang - cot theo sheet.
	    int tongHang = listDataBySheet.get(0).getTongHang();
	    int tongCot = listDataBySheet.get(0).getTongCot();
	    int startIndexHang = listDataBySheet.get(0).getIndexCellHang();

	    // Get Sheet info
	    BmSheetDTO sheetInfoDto = this.bmSheetEntityDao
		    .findById(sheetDto.getValue().intValue());
	    // Get ls column of sheet.
	    List<BmSheetCotDTO> lstCot = this.bmSheetCotEntityDao.listCotExitsGenTable(
		    sheetInfoDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetInfoDto.getId());

	    // TODO [Pending] draw sheet header. (dong hang/cot)
//	    StringBuilder htmlDynamicSheet = new StringBuilder(
//		    "<table class=\"q-table\">" + " <thead>" + " </thead>");

	    // UPDATE lấy header menu default (not Cot/hang)
	    StringBuilder htmlDynamicSheet = new StringBuilder();
	    BmSheetMegerCellDTO header = this.logMegerHeader
		    .InitHeaderHangCotForExcelForm(sheetDto.getValue().intValue(), lstCot, false);
	    htmlDynamicSheet.append(header.getHeaderTableStr());
	    boolean isLaChiTieu = true;
	    int hangDongId = 0;
	    // Generate dynamic sheet form
	    for (int i = startIndexHang; i <= (tongHang + startIndexHang); i++) {
		    int hangNum = i;
			DataImportExcelDTO dataImport = listDataBySheet.stream()
				    .filter(f -> (f.getIndexCellHang() != null
					    && f.getIndexCellHang().intValue() == hangNum)
					    && (f.getIndexCellCot() != null
						    && f.getIndexCellCot().intValue() == 0))
				    .findFirst().orElse(null);
			if(!isLaChiTieu) {
				//check nếu là chỉ tiêu tiếp theo hoặc chỉ tiêu cuối cùng thì sẽ thêm nút thêm hàng động
				if(dataImport != null && dataImport.isLaChiTieu() || (hangNum == tongHang + startIndexHang)) {
					htmlDynamicSheet.append(this.getButtonThemHangDong(lstCot.size(), hangDongId));
					isLaChiTieu = true;
					hangDongId = 0;
					htmlDynamicSheet.append("<tr>");
				}
				else {
			    	htmlDynamicSheet.append("<tr data-hangdong='1' data-hang-index='" + hangNum + "'>");
				}
			}
			else {
		    	htmlDynamicSheet.append("<tr>");
			}
			//them button them hang dong sau khi 
			if((dataImport != null && dataImport.isLaChiTieu() || (hangNum == tongHang + startIndexHang))) {
				if(dataImport != null && dataImport.isLaHangDong()) {
					isLaChiTieu = false;
					hangDongId = dataImport.getChiTieu().getBmSheetHangId();
				}
			}
			for (int j = 0; j <= tongCot; j++) {
			    int cotNum = j;
			    dataImport = listDataBySheet.stream()
				    .filter(f -> (f.getIndexCellHang() != null
					    && f.getIndexCellHang().intValue() == hangNum)
					    && (f.getIndexCellCot() != null
						    && f.getIndexCellCot().intValue() == cotNum)) 
				    .findFirst().orElse(null);
			    if (dataImport != null) {
					if (dataImport.isLaChiTieu() && (dataImport.getChiTieu() != null
						&& dataImport.getChiTieu().getId() != null)) {
		
					    // Get BmChiTieuDTO, dinh dang cell, hang - cot ID
					    BmSheetCtDTO bmChiTieuImport = dataImport.getChiTieu();
					    String dinhDangCell = dataImport.getChiTieu().getDinhDang() == null
						    ? StringUtils.EMPTY
						    : dataImport.getChiTieu().getDinhDang();
					    int hangId = bmChiTieuImport.getBmSheetHangId() == null ? 0
						    : bmChiTieuImport.getBmSheetHangId().intValue();
					    int cotId = bmChiTieuImport.getBmSheetCotId() == null ? 0
						    : bmChiTieuImport.getBmSheetCotId().intValue();
		
					    // Check cell value type: So, Tien te, Ti le %;
					    if (("S").equals(dinhDangCell) || ("TT").equals(dinhDangCell)
						    || ("TLPT").equals(dinhDangCell)) {
		
						// Get cell input type numeric validation
						this.getCellInputNumericValidate(htmlDynamicSheet, hangId, cotId,
							bmChiTieuImport, dataImport);
					    }
		
					    // Check cell value type: Ky tu.
					    if (("KT").equals(dinhDangCell)) {
		
						// get value of BmSheetCtVaoID
						int bmSheetCtVaoId = bmChiTieuImport.getBmSheetCtVaoId() == null ? 0
							: bmChiTieuImport.getBmSheetCtVaoId().intValue();
		
						htmlDynamicSheet
							.append("<td  data-id='" + cotId + "|" + hangId + "' >");
						htmlDynamicSheet.append(" <input ");
						htmlDynamicSheet.append(" class=\"form-control gt-cell\" ");
						htmlDynamicSheet.append(" type=\"text\" ");
						htmlDynamicSheet.append(" id=" + cotId + "|" + hangId + "|"
							+ bmChiTieuImport.getId() + "-" + bmSheetCtVaoId + "|"
							+ dataImport.getBcBaoCaoGtId());
						htmlDynamicSheet
							.append(" value = \" " + dataImport.getGiaTri() + "\"");
						htmlDynamicSheet.append(
							" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
						htmlDynamicSheet.append(" />");
						htmlDynamicSheet.append("</td >");
					    }
		
					    // Check cell value type: Label. Show label text.
					    if (("LB").equals(dinhDangCell)) {
						htmlDynamicSheet.append("<td>");
						htmlDynamicSheet.append(bmChiTieuImport.getLabel() == null ? ""
							: bmChiTieuImport.getLabel());
						htmlDynamicSheet.append("</td>");
					    }
		
					} else {
					    // Generate dynamic cell form input with data value imported.
					    this.generateCellDynamicForm(htmlDynamicSheet, dataImport, hangDongId);
//					    isLaChiTieu = false;
					}
			    }
			    
	
			}
			htmlDynamicSheet.append("</tr>");
	    }

	    // set data to form sheet DTO.
	    htmlDynamicSheet.append("</tbody></table>");
	    outputSheetForm.setTableHtmlRaw(htmlDynamicSheet.toString());
	    outputSheetFormLs.add(outputSheetForm);

	}

	return outputSheetFormLs;
    }

    /**
     * Build cell form for dynamic columns & rows.
     * 
     * @param htmlDynamicSheet
     * @param dataImport
     * @throws Exception
     */
    private void generateCellDynamicForm(StringBuilder htmlDynamicSheet,
	    DataImportExcelDTO dataImport, int sheetHangId) throws Exception {
	// TODO Auto-generated method stub
	htmlDynamicSheet.append("<td data-hangdong='1' data-id='" + dataImport.getIndexCellCot() + "|"
		+ dataImport.getIndexCellHang() + "' >");
	htmlDynamicSheet.append(" <input ");
	htmlDynamicSheet.append(" class=\"form-control gt-cell-dynamic\" ");
	htmlDynamicSheet.append(" type=\"text\" ");

	// Gen input field ID type: <input id="BmBaoCaoId|BmSheetId"/>
	htmlDynamicSheet.append(" id=\"" + dataImport.getBmBaoCaoId() + "|"
		+ sheetHangId + "|" + dataImport.getIndexCellCot() + "|"
		+ dataImport.getIndexCellHang() + "\"");
	htmlDynamicSheet
		.append(" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" ");
	htmlDynamicSheet.append(" value = \"" + dataImport.getGiaTri() + "\"");
	htmlDynamicSheet.append(" /></td>");
    }

    /**
     * Get list sheet dropdown by bao cao (not trang bia).
     * 
     * @param baocaoId
     * @return
     * @throws Exception
     */
    public List<DropDownDTO> getlsSheetNotTrangBiabyBaoCaoLogic(Integer baocaoId) throws Exception {
	return this.bmSheetEntityDao.getLsSheetBaoCaoNotTrangBiaDao(baocaoId);
    }

    /**
     * init Eform Gui Bao Cao Khac (BT, TYC, K) Logic
     * 
     * @param sheetId
     * @param baoCaoId
     * @return
     * @throws Exception
     */
    public BmSheetDTO initEformGuiBaoCaoKhacLogic(Integer sheetId, BcThanhVienDTO thanhVienDto)
	    throws Exception {
	BmSheetDTO sheetDto = this.bmSheetEntityDao.findById(sheetId);
	if (sheetDto != null) {
	    // lấy ds hàng
	    List<BmSheetHangDTO> lstHang = this.bmSheetHangEntityDao.lstSheetHangExist(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy ds cột
	    List<BmSheetCotDTO> lstCot = this.bmSheetCotEntityDao.listCotExitsGenTable(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy header menu
	    BmSheetMegerCellDTO header = this.logMegerHeader
		    .InitHeaderHangCotForExcelForm(sheetDto.getId(), lstCot, false);

	    // lấy ds cell
	    List<BmSheetCtDTO> lstCell = this.bmSheetCtEntityDao.getLsCellByBaoCaoAndSheet(
		    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

	    // lấy ds gia tri
	    List<BcBaoCaoGtDTO> lstBaocaoGt = this.bcBaoCaoGtEntityDao.getListBaoCaoGiaTri(
		    thanhVienDto.getBmBaoCaoId(), thanhVienDto.getId(), sheetDto.getId());
	    BcBaoCaoGtDongDTO eBaoCaoGtDong = this.bcBaoCaoGtDongEntityDao.getHangDong(thanhVienDto.getId(), sheetDto.getId());
	    List<DataImportExcelDTO> lstGtDong = new ArrayList<DataImportExcelDTO>();
	    if(eBaoCaoGtDong != null) {
	    	Gson gson = new Gson();
		    Type userListType = new TypeToken<List<DataImportExcelDTO>>() {
		    }.getType();
		    lstGtDong = gson.fromJson(eBaoCaoGtDong.getGiaTri(), userListType);
	    }
	    StringBuilder strTable = new StringBuilder();

	    // create header
	    strTable.append(header.getHeaderTableStr());
	    strTable.append("<tbody>");

	    if (lstHang != null) {
		for (BmSheetHangDTO bmSheetHang : lstHang) {
		    strTable.append("<tr>");
		    for (BmSheetCotDTO sheetCot : lstCot) {

			if (lstCell != null) {
			    Optional<BmSheetCtDTO> cell = lstCell.stream()
				    .filter(x -> x.getBmSheetHangId().intValue() == bmSheetHang
					    .getId().intValue()
					    && x.getBmSheetCotId().intValue() == sheetCot.getId()
						    .intValue())
				    .findFirst();
			    if (cell.isPresent()) {

				// Neu khong co du lieu da nhap, hien thi o textbox trong.
				if (lstBaocaoGt == null) {
				    // Neu o nhap la dinh dang kieu Number || Tien te dat dang input
				    // text
				    if (("S").equals(cell.get().getDinhDang())
					    || ("TT").equals(cell.get().getDinhDang())
					    || ("TLPT").equals(cell.get().getDinhDang())) {
					strTable.append("<td  data-id='" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "' >");
					strTable.append(" <input ");
					strTable.append(" class=\"form-control gt-cell\" ");
					strTable.append(" type=\"number\" ");
					int bmSheetCtVaoId = (cell.get()
						.getBmSheetCtVaoId()) == null ? 0
							: cell.get().getBmSheetCtVaoId().intValue();
					strTable.append(" id=\"" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "|" + cell.get().getId()
						+ "-" + bmSheetCtVaoId + "|0\"");
					strTable.append(
						" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					strTable.append(" />");

				    }

				    if (("KT").equals(cell.get().getDinhDang())) {
					strTable.append("<td  data-id='" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "' >");
					strTable.append(" <input ");
					strTable.append(" class=\"form-control gt-cell\" ");
					strTable.append(" type=\"text\" ");
					int bmSheetCtVaoId = (cell.get()
						.getBmSheetCtVaoId()) == null ? 0
							: cell.get().getBmSheetCtVaoId().intValue();
					strTable.append(" id=\"" + sheetCot.getId() + "|"
						+ bmSheetHang.getId() + "|" + cell.get().getId()
						+ "-" + bmSheetCtVaoId + "|0\"");
					strTable.append(
						" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					strTable.append(" />");
				    }

				    if (("LB").equals(cell.get().getDinhDang())) {
					strTable.append("<td>");
					strTable.append(cell.get().getLabel() == null ? ""
						: cell.get().getLabel());
					strTable.append("</td>");
				    }

				    // Neu khong cau hinh o chi tieu, hien thi cot trong.
				    if (null == (cell.get().getDinhDang())) {
					strTable.append("<td>");
					strTable.append("</td>");
				    }
				    // Neu co gia tri, hien thi o textbox kem gia tri.
				} else {
				    // Lay gia tri theo hang - cot
				    Optional<BcBaoCaoGtDTO> bcGiaTri = lstBaocaoGt.stream()
					    .filter(f -> (f.getBmSheetHangId() != null ? (f
						    .getBmSheetHangId()
						    .intValue() == bmSheetHang.getId().intValue())
						    : false)
						    && (f.getBmSheetCotId() != null
							    ? (f.getBmSheetCotId()
								    .intValue() == sheetCot.getId()
									    .intValue())
							    : false)
						    && (f.getBmSheetCtId() != null
							    ? (f.getBmSheetCtId().intValue() == cell
								    .get().getId().intValue())
							    : false))
					    .findFirst();

				    if (bcGiaTri.isPresent()) {
					// Neu cell la So/Tien te. hien thi du lieu kieu So/TienTe.
					if (("S").equals(cell.get().getDinhDang())
						|| ("TT").equals(cell.get().getDinhDang())
						|| ("TLPT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"number\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|"
						    + bcGiaTri.get().getId() + "\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" value = \""
						    + bcGiaTri.get().getGiaTri() + "\"");
					    strTable.append(" />");
					}

					// Neu la kieu ky tu, hien thi o textbox kieu text
					if (("KT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"text\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|"
						    + bcGiaTri.get().getId() + "\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" value = \""
						    + bcGiaTri.get().getGiaTri() + "\"");
					    strTable.append(" />");
					}

					// Neu la kieu label, hien thi label.
					if (("LB").equals(cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append(cell.get().getLabel() == null ? ""
						    : cell.get().getLabel());
					    strTable.append("</td>");
					}

					// Neu khong cau hinh o chi tieu, hien thi cot trong.
					if (null == (cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append("</td>");
					}
				    }
				    // Case gen cell input case BcGiaTri Null.
				    else {
					// Neu o nhap la dinh dang kieu Number || Tien te dat dang
					// input
					// text
					if (("S").equals(cell.get().getDinhDang())
						|| ("TT").equals(cell.get().getDinhDang())
						|| ("TLPT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"number\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|0\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" />");

					}

					if (("KT").equals(cell.get().getDinhDang())) {
					    strTable.append("<td  data-id='" + sheetCot.getId()
						    + "|" + bmSheetHang.getId() + "' >");
					    strTable.append(" <input ");
					    strTable.append(" class=\"form-control gt-cell\" ");
					    strTable.append(" type=\"text\" ");
					    int bmSheetCtVaoId = (cell.get()
						    .getBmSheetCtVaoId()) == null ? 0
							    : cell.get().getBmSheetCtVaoId()
								    .intValue();
					    strTable.append(" id=\"" + sheetCot.getId() + "|"
						    + bmSheetHang.getId() + "|" + cell.get().getId()
						    + "-" + bmSheetCtVaoId + "|0\"");
					    strTable.append(
						    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
					    strTable.append(" />");
					}

					if (("LB").equals(cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append(cell.get().getLabel() == null ? ""
						    : cell.get().getLabel());
					    strTable.append("</td>");
					}

					// Neu khong cau hinh o chi tieu, hien thi cot trong.
					if (null == (cell.get().getDinhDang())) {
					    strTable.append("<td>");
					    strTable.append("</td>");
					}
					// Neu co gia tri, hien thi o textbox kem gia tri.
				    }

				}

			    } else {
				strTable.append("<td  data-id='" + sheetCot.getId() + "|"
					+ bmSheetHang.getId() + "' >");
				strTable.append("");
				strTable.append("</td >");

			    }
			}
		    }
		    strTable.append("</tr>");
		    if(lstGtDong != null && lstGtDong.size() > 0) {
		    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
//		    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang() == bmSheetHang.getId()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
		    	if(lstHangDong != null && lstHangDong.size() > 0) {
		    		for(DataImportExcelDTO hangDong : lstHangDong) {
		    			strTable.append("<tr "
		    						+ "data-hangdong='1' data-hang-index='" + hangDong.getIndexCellHang() + "'>");
		    			for(int i = 0; i < lstCot.size(); i++) {
		    				int gtCotIndex = i;
		    				Optional<DataImportExcelDTO> objGTDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue() && x.getIndexCellCot().intValue() == gtCotIndex 
		    						&& x.getIndexCellHang().intValue() == hangDong.getIndexCellHang().intValue()).findFirst();
		    				if(objGTDong.isPresent()) {
		    					DataImportExcelDTO tmpGtDong = objGTDong.get();
		    					strTable.append("<td "
		    									+ "data-hangdong='1' data-id='" 
		    									+ tmpGtDong.getIndexCellCot() + "|" 
		    									+ tmpGtDong.getIndexCellHang() + "'> "
		    									+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" id=\"" 
		    									+ tmpGtDong.getBmBaoCaoId() + "|" 
		    									+ tmpGtDong.getBmSheetHang() + "|" 
		    									+ tmpGtDong.getIndexCellCot() + "|" + tmpGtDong.getIndexCellHang() 
		    									+ "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" value=\"" + tmpGtDong.getGiaTri() 
		    									+ "\" /></td>");
		    				}
		    				else {
		    					strTable.append("<td data-hangdong='1' data-id='" + i + "|" + hangDong.getIndexCellHang() + "'> "
		    									+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" "
		    									+ "id=\"" + hangDong.getBmBaoCaoId() + "|" + hangDong.getBmSheetHang() 
		    									+ "|" + i + "|" + hangDong.getIndexCellHang() 
		    									+ "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" />");
		    									strTable.append("</td >");
		    				}
		    			}
		    			strTable.append("</tr>");
		    		}
		    	}
		    }
		    if(bmSheetHang.getHangDong()) {
		    	strTable.append(this.getButtonThemHangDong(lstCot.size(), bmSheetHang.getId()));
//		    	strTable.append("<tr><td colspan='" + lstCot.size() + "'>");
//		    	strTable.append("<button tabindex=\"0\" type=\"button\" role=\"button\" data-coltotal=\"" + lstCot.size() + "\" "
//		    			+ "class=\"q-btn q-btn-item non-selectable no-outline q-btn--push q-btn--rectangle bg-brown-5 text-white q-btn--actionable q-focusable q-hoverable q-btn--wrap\" title=\"Thêm hàng\" style=\"font-size: 12px; text-decoration: none;\">"
//		    			+ "<div tabindex=\"-1\" class=\"q-focus-helper\"></div><div class=\"q-btn__wrapper col row q-anchor--skip\">"
//		    			+ "<div class=\"q-btn__content text-center col items-center q-anchor--skip justify-center row\">"
//		    			+ "<i aria-hidden=\"true\" role=\"img\" class=\"material-icons q-icon notranslate on-left\">add_circle_outline</i>"
//		    			+ "<div>Thêm hàng</div></div></div></button>");
//		    	strTable.append("</td></tr>");
		    }
		}
		strTable.append("</tbody></table>");
		sheetDto.setTableHtmlRaw(strTable.toString());
		return sheetDto;
	    }
	}
	return null;
    }
    
    private String getButtonThemHangDong(int columnSize, int hangId) {
    	StringBuilder buttonGenerateStr = new StringBuilder("<tr><td colspan='" + columnSize + "'>");
    	buttonGenerateStr.append("<button tabindex=\"0\" type=\"button\" role=\"button\" data-hangid=\"" + hangId + "\" data-coltotal=\"" + columnSize + "\" "
    			+ "class=\"q-btn q-btn-item non-selectable no-outline q-btn--push q-btn--rectangle bg-brown-5 text-white q-btn--actionable q-focusable q-hoverable q-btn--wrap\" title=\"Thêm hàng\" style=\"font-size: 12px; text-decoration: none;\">"
    			+ "<div tabindex=\"-1\" class=\"q-focus-helper\"></div><div class=\"q-btn__wrapper col row q-anchor--skip\">"
    			+ "<div class=\"q-btn__content text-center col items-center q-anchor--skip justify-center row\">"
    			+ "<i aria-hidden=\"true\" role=\"img\" class=\"material-icons q-icon notranslate on-left\">add_circle_outline</i>"
    			+ "<div>Thêm hàng</div></div></div></button>");
    	buttonGenerateStr.append("</td></tr>");
    	return buttonGenerateStr.toString();
    }


	public List<BmSheetDTO> getNoiDungBaoCaoAllSheet(String giaTriKyBaoCao, Integer baoCaoId, int thanhVienId) throws Exception {
		List<BmSheetDTO> sheetDtoLst = this.bmSheetEntityDao.listSheetByBmBaoCaoId(baoCaoId);
		if (sheetDtoLst != null && sheetDtoLst.size() > 0) {
			for(BmSheetDTO sheetDto : sheetDtoLst) {
				// lấy ds hàng
			    List<BmSheetHangDTO> lstHang = this.bmSheetHangEntityDao.lstSheetHangExist(
				    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

			    // lấy ds cột
			    List<BmSheetCotDTO> lstCot = this.bmSheetCotEntityDao.listCotExitsGenTable(
				    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

			    // lấy header menu
			    BmSheetMegerCellDTO header = this.logMegerHeader
				    .InitHeaderHangCotForExcelForm(sheetDto.getId(), lstCot, false);

			    // lấy ds cell
			    List<BmSheetCtDTO> lstCell = this.bmSheetCtEntityDao.getLsCellByBaoCaoAndSheet(
				    sheetDto.getBmBaoCaoByBmBaoCaoId().getId(), sheetDto.getId());

			    // lấy ds gia tri
			    List<BcBaoCaoGtDTO> lstBaocaoGt = this.bcBaoCaoGtEntityDao.getListBaoCaoGiaTri(baoCaoId,
				    thanhVienId, sheetDto.getId());
			    // lấy ds hàng động
			    BcBaoCaoGtDongDTO eBaoCaoGtDong = this.bcBaoCaoGtDongEntityDao.getHangDong(thanhVienId, sheetDto.getId());
			    List<DataImportExcelDTO> lstGtDong = new ArrayList<DataImportExcelDTO>();
			    if(eBaoCaoGtDong != null) {
			    	Gson gson = new Gson();
				    Type userListType = new TypeToken<List<DataImportExcelDTO>>() {
				    }.getType();
				    lstGtDong = gson.fromJson(eBaoCaoGtDong.getGiaTri(), userListType);
			    }
			    StringBuilder strTable = new StringBuilder();
			    // create header
			    strTable.append(header.getHeaderTableStr());
			    strTable.append("<tbody>");
			    if (lstHang != null) {
			    // lap theo list hang cua sheet
				for (BmSheetHangDTO bmSheetHang : lstHang) {
				    strTable.append("<tr>");
				    //Lap theo list cot cua sheet
				    for (BmSheetCotDTO sheetCot : lstCot) {
				    //Doi voi truong hop list chi tieu khac null
					if (lstCell != null) {
					    Optional<BmSheetCtDTO> cell = lstCell.stream()
						    .filter(x -> x.getBmSheetHangId().intValue() == bmSheetHang
							    .getId().intValue()
							    && x.getBmSheetCotId().intValue() == sheetCot.getId()
								    .intValue()).findFirst();
					    if (cell.isPresent()) {

						// Neu khong co du lieu da nhap, hien thi o textbox trong.
						if (lstBaocaoGt == null) {
						    // Neu o nhap la dinh dang kieu Number || Tien te dat dang input
						    // text
						    if (("S").equals(cell.get().getDinhDang())
							    || ("TT").equals(cell.get().getDinhDang())
							    || ("TLPT").equals(cell.get().getDinhDang())) {
						    	//Them o input theo dinh dang
							strTable.append("<td  data-id='" + sheetCot.getId() + "|"
								+ bmSheetHang.getId() + "' >");
							strTable.append(" <input ");
							//ToDo: Check cell neu bat buoc thi them data attribute required = 1
							if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
								strTable.append(" data-required=\"1\" ");
							}
							strTable.append(" class=\"form-control gt-cell\" ");
							strTable.append(" type=\"number\" ");
							int bmSheetCtVaoId = (cell.get()
								.getBmSheetCtVaoId()) == null ? 0
									: cell.get().getBmSheetCtVaoId().intValue();
							strTable.append(" id=\"" + sheetCot.getId() + "|"
								+ bmSheetHang.getId() + "|" + cell.get().getId()
								+ "-" + bmSheetCtVaoId + "|0\"");
							strTable.append(
								" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
							strTable.append(" />");

						    }
						    
						    //Neu dinh dang la kieu Ky tu KT 
						    if (("KT").equals(cell.get().getDinhDang())) {
							strTable.append("<td  data-id='" + sheetCot.getId() + "|"
								+ bmSheetHang.getId() + "' >");
							strTable.append(" <input ");
							//ToDo: Check cell neu bat buoc thi them data attribute required = 1
							if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
								strTable.append(" data-required=\"1\" ");
							}
							strTable.append(" class=\"form-control gt-cell\" ");
							strTable.append(" type=\"text\" ");
							int bmSheetCtVaoId = (cell.get()
								.getBmSheetCtVaoId()) == null ? 0
									: cell.get().getBmSheetCtVaoId().intValue();
							strTable.append(" id=\"" + sheetCot.getId() + "|"
								+ bmSheetHang.getId() + "|" + cell.get().getId()
								+ "-" + bmSheetCtVaoId + "|0\"");
							strTable.append(
								" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
							strTable.append(" /></td>");
						    }

						    //Neu dinh dang la kieu tieu de LB
						    if (("LB").equals(cell.get().getDinhDang())) {
							strTable.append("<td>");
							strTable.append(cell.get().getLabel() == null ? ""
								: cell.get().getLabel());
							strTable.append("</td>");
						    }

						    // Neu khong cau hinh o chi tieu, hien thi cot trong.
						    if (null == (cell.get().getDinhDang())) {
							strTable.append("<td>");
							strTable.append("</td>");
						    }
						    // Neu co gia tri, hien thi o textbox kem gia tri.
						} 
						// Doi voi truong hop list chi tieu = null
						else {
						    // Lay gia tri theo hang - cot
						    Optional<BcBaoCaoGtDTO> bcGiaTri = lstBaocaoGt.stream()
							    .filter(f -> (f.getBmSheetHangId() != null ? (f
								    .getBmSheetHangId()
								    .intValue() == bmSheetHang.getId().intValue())
								    : false)
								    && (f.getBmSheetCotId() != null
									    ? (f.getBmSheetCotId()
										    .intValue() == sheetCot.getId()
											    .intValue())
									    : false)
								    && (f.getBmSheetCtId() != null
									    ? (f.getBmSheetCtId().intValue() == cell
										    .get().getId().intValue())
									    : false))
							    .findFirst();
						    // Neu co gia tri theo filter 
						    if (bcGiaTri.isPresent()) {
							// Neu cell la So/Tien te. hien thi du lieu kieu So/TienTe.
							if (("S").equals(cell.get().getDinhDang())
								|| ("TT").equals(cell.get().getDinhDang())
								|| ("TLPT").equals(cell.get().getDinhDang())) {
							    strTable.append("<td  data-id='" + sheetCot.getId()
								    + "|" + bmSheetHang.getId() + "' >");
							    strTable.append(" <input ");
								//ToDo: Check cell neu bat buoc thi them data attribute required = 1
								if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
									strTable.append(" data-required=\"1\" ");
								}
							    strTable.append(" class=\"form-control gt-cell\" ");
							    strTable.append(" type=\"number\" ");
							    int bmSheetCtVaoId = (cell.get()
								    .getBmSheetCtVaoId()) == null ? 0
									    : cell.get().getBmSheetCtVaoId()
										    .intValue();
							    strTable.append(" id=\"" + sheetCot.getId() + "|"
								    + bmSheetHang.getId() + "|" + cell.get().getId()
								    + "-" + bmSheetCtVaoId + "|"
								    + bcGiaTri.get().getId() + "\"");
							    strTable.append(
								    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
							    strTable.append(" value = \""
								    + bcGiaTri.get().getGiaTri() + "\"");
							    strTable.append(" /> </td>");
							}

							// Neu la kieu ky tu, hien thi o textbox kieu text
							if (("KT").equals(cell.get().getDinhDang())) {
							    strTable.append("<td  data-id='" + sheetCot.getId()
								    + "|" + bmSheetHang.getId() + "' >");
							    strTable.append(" <input ");
							  //ToDo: Check cell neu bat buoc thi them data attribute required = 1
							    if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
									strTable.append(" data-required=\"1\" ");
								}
							    strTable.append(" class=\"form-control gt-cell\" ");
							    strTable.append(" type=\"text\" ");
							    int bmSheetCtVaoId = (cell.get()
								    .getBmSheetCtVaoId()) == null ? 0
									    : cell.get().getBmSheetCtVaoId()
										    .intValue();
							    strTable.append(" id=\"" + sheetCot.getId() + "|"
								    + bmSheetHang.getId() + "|" + cell.get().getId()
								    + "-" + bmSheetCtVaoId + "|"
								    + bcGiaTri.get().getId() + "\"");
							    strTable.append(
								    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
							    strTable.append(" value = \""
								    + bcGiaTri.get().getGiaTri() + "\"");
							    strTable.append(" /></td>");
							}

							// Neu la kieu label, hien thi label.
							if (("LB").equals(cell.get().getDinhDang())) {
							    strTable.append("<td>");
							    strTable.append(cell.get().getLabel() == null ? ""
								    : cell.get().getLabel());
							    strTable.append("</td>");
							}

							// Neu khong cau hinh o chi tieu, hien thi cot trong.
							if (null == (cell.get().getDinhDang())) {
							    strTable.append("<td>");
							    strTable.append("</td>");
							}
						    }
						    // Case gen cell input case BcGiaTri Null.
						    else {
							// Neu o nhap la dinh dang kieu Number || Tien te dat dang
							// input
							// text
							if (("S").equals(cell.get().getDinhDang())
								|| ("TT").equals(cell.get().getDinhDang())
								|| ("TLPT").equals(cell.get().getDinhDang())) {
							    strTable.append("<td  data-id='" + sheetCot.getId()
								    + "|" + bmSheetHang.getId() + "' >");
							    strTable.append(" <input ");
							    
								//ToDo: Check cell neu bat buoc thi them data attribute required = 1
							    if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
									strTable.append(" data-required=\"1\" ");
								}
							    strTable.append(" class=\"form-control gt-cell\" ");
							    strTable.append(" type=\"number\" ");
							    int bmSheetCtVaoId = (cell.get()
								    .getBmSheetCtVaoId()) == null ? 0
									    : cell.get().getBmSheetCtVaoId()
										    .intValue();
							    strTable.append(" id=\"" + sheetCot.getId() + "|"
								    + bmSheetHang.getId() + "|" + cell.get().getId()
								    + "-" + bmSheetCtVaoId + "|0\"");
							    strTable.append(
								    " style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
							    strTable.append(" /></td>");

							}

							if (("KT").equals(cell.get().getDinhDang())) {
							    strTable.append("<td  data-id='" + sheetCot.getId()
								    + "|" + bmSheetHang.getId() + "' >");
							    strTable.append(" <input ");
								//ToDo: Check cell neu bat buoc thi them data attribute required = 1
							    if(cell.get().getBatBuoc() != null && cell.get().getBatBuoc().booleanValue()) {
									strTable.append(" data-required=\"1\" ");
								}
							    strTable.append(" class=\"form-control gt-cell\" ");
							    strTable.append(" type=\"text\" ");
							    int bmSheetCtVaoId = (cell.get()
								    .getBmSheetCtVaoId()) == null ? 0
									    : cell.get().getBmSheetCtVaoId()
										    .intValue();
							    strTable.append(" id=\"" + sheetCot.getId() + "|"
								    + bmSheetHang.getId() + "|" + cell.get().getId()
								    + "-" + bmSheetCtVaoId + "|0\"");
							    strTable.append(" style=\"height: 70%; border: 1px dotted #666; margin-bottom: 6%;\" ");
							    strTable.append(" /></td>");
							}

							if (("LB").equals(cell.get().getDinhDang())) {
							    strTable.append("<td>");
							    strTable.append(cell.get().getLabel() == null ? ""
								    : cell.get().getLabel());
							    strTable.append("</td>");
							}

							// Neu khong cau hinh o chi tieu, hien thi cot trong.
							if (null == (cell.get().getDinhDang())) {
							    strTable.append("<td>");
							    strTable.append("</td>");
							}
							// Neu co gia tri, hien thi o textbox kem gia tri.
						    }

						}

					    } else {
						strTable.append("<td  data-id='" + sheetCot.getId() + "|"
							+ bmSheetHang.getId() + "' >");
						strTable.append("");
						strTable.append("</td >");

					    }
					}
				    }
				    strTable.append("</tr>");
				    //Load dữ liệu hàng động
				    if(lstGtDong != null && lstGtDong.size() > 0) {
				    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
//				    	List<DataImportExcelDTO> lstHangDong = lstGtDong.stream().filter(x->x.getBmSheetHang() == bmSheetHang.getId()).filter(distinctByKey(DataImportExcelDTO::getIndexCellHang)).sorted(Comparator.comparing(DataImportExcelDTO::getIndexCellHang)).collect(Collectors.toList());
				    	if(lstHangDong != null && lstHangDong.size() > 0) {
				    		for(DataImportExcelDTO hangDong : lstHangDong) {
				    			strTable.append("<tr "
				    						+ "data-hangdong='1' data-hang-index='" + hangDong.getIndexCellHang() + "'>");
				    			for(int i = 0; i < lstCot.size(); i++) {
				    				int gtCotIndex = i;
				    				Optional<DataImportExcelDTO> objGTDong = lstGtDong.stream().filter(x->x.getBmSheetHang().intValue() == bmSheetHang.getId().intValue() && x.getIndexCellCot().intValue() == gtCotIndex 
				    						&& x.getIndexCellHang().intValue() == hangDong.getIndexCellHang().intValue()).findFirst();
				    				if(objGTDong.isPresent()) {
				    					DataImportExcelDTO tmpGtDong = objGTDong.get();
				    					strTable.append("<td "
				    									+ "data-hangdong='1' data-id='" 
				    									+ tmpGtDong.getIndexCellCot() + "|" 
				    									+ tmpGtDong.getIndexCellHang() + "'> "
				    									+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" id=\"" 
				    									+ tmpGtDong.getBmBaoCaoId() + "|" 
				    									+ tmpGtDong.getBmSheetHang() + "|" 
				    									+ tmpGtDong.getIndexCellCot() + "|" + tmpGtDong.getIndexCellHang() 
				    									+ "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" value=\"" + tmpGtDong.getGiaTri() 
				    									+ "\" /></td>");
				    				}
				    				else {
				    					strTable.append("<td data-hangdong='1' data-id='" + i + "|" + hangDong.getIndexCellHang() + "'> "
				    									+ "<input class=\"form-control gt-cell-dynamic\" type=\"text\" "
				    									+ "id=\"" + hangDong.getBmBaoCaoId() + "|" + hangDong.getBmSheetHang() 
				    									+ "|" + i + "|" + hangDong.getIndexCellHang() 
				    									+ "\" style=\"height: 70%; border: 1px dotted #338DFF; margin-bottom: 6%;\" />");
				    									strTable.append("</td >");
				    				}
				    			}
				    			strTable.append("</tr>");
				    		}
				    	}
				    }
				    //Xử lý button hàng động
				    if(bmSheetHang.getHangDong()) {
				    	strTable.append(this.getButtonThemHangDong(lstCot.size(), bmSheetHang.getId()));
//				    	strTable.append("<tr><td colspan='" + lstCot.size() + "'>");
//				    	strTable.append("<button tabindex=\"0\" type=\"button\" role=\"button\" data-coltotal=\"" + lstCot.size() + "\" "
//				    			+ "class=\"q-btn q-btn-item non-selectable no-outline q-btn--push q-btn--rectangle bg-brown-5 text-white q-btn--actionable q-focusable q-hoverable q-btn--wrap\" title=\"Thêm hàng\" style=\"font-size: 12px; text-decoration: none;\">"
//				    			+ "<div tabindex=\"-1\" class=\"q-focus-helper\"></div><div class=\"q-btn__wrapper col row q-anchor--skip\">"
//				    			+ "<div class=\"q-btn__content text-center col items-center q-anchor--skip justify-center row\">"
//				    			+ "<i aria-hidden=\"true\" role=\"img\" class=\"material-icons q-icon notranslate on-left\">add_circle_outline</i>"
//				    			+ "<div>Thêm hàng</div></div></div></button>");
//				    	strTable.append("</td></tr>");
				    }
				}
				strTable.append("</tbody></table>");
				sheetDto.setTableHtmlRaw(strTable.toString());
				
			    }
				
			}
			return sheetDtoLst;
		}
		return null;
	}
}
