package com.temp.logic.BieuMauBaoCao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.BieuMauBaoCao.BieuMauBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.persistence.dao.BieuMauBaoCao.BieuMauBaoCaoDao;

@Component
public class BieuMauBaoCaoLogic {

	@Autowired
	private BieuMauBaoCaoDao Dao;

	public BieuMauBaoCaoBDTO getlist(Integer pageNo, Integer pageSize, boolean desc, String strFilter, String tenBaoCao,
			String canCuPhapLy) {

		try {
			BieuMauBaoCaoBDTO BBDTO = new BieuMauBaoCaoBDTO();
			BmBaoCaoBDTO BDTO = Dao.getlist(pageNo, pageSize, desc, strFilter, tenBaoCao, canCuPhapLy);

			Map<String, List<BmBaoCaoDTO>> map = new HashMap<String, List<BmBaoCaoDTO>>();
			for (BmBaoCaoDTO dto : BDTO.lstBmBaoCao) {
//				switch (dto.getNhomBaoCao()) {
//					case "HD":
//						dto.setNhomBaoCao("Báo cáo hoạt động");
//						break;
//					case "TCR":
//						dto.setNhomBaoCao("Báo cáo tài chính");
//						break;
//					case "KSKSDB":
//						dto.setNhomBaoCao("Báo cáo kiểm soát - kiểm soát đặc biệt");
//						break;
//					case "Báo cáo tỷ lệ an toàn tài chính":
//						dto.setNhomBaoCao("TLATTC");
//						break;
//					case "HN":
//						dto.setNhomBaoCao("Báo cáo hợp nhất");
//						break;
//					case "DKGDTT":
//						dto.setNhomBaoCao("Đăng ký giao dịch trực tuyến");
//						break;
//					default:
//						dto.setNhomBaoCao("Khác");
//						break;
//				}
				if (!StringUtils.isEmpty(dto.getNhomBaoCao()) && !map.containsKey(dto.getNhomBaoCao())) {
					List<BmBaoCaoDTO> lst = new ArrayList<BmBaoCaoDTO>();
					for (BmBaoCaoDTO dtoChill : BDTO.lstBmBaoCao) {
						if (dtoChill.getNhomBaoCao() != null && dtoChill.getNhomBaoCao().equals(dto.getNhomBaoCao())) {
							lst.add(dtoChill);
						}
					}
					map.put((String) dto.getNhomBaoCao(), lst);
				}
			}
			if (!map.isEmpty()) {
				BBDTO.lstBDTO = map.entrySet().stream()
						.collect(Collectors.toMap(e -> e.getKey(), e -> (List<BmBaoCaoDTO>) e.getValue()));
				return BBDTO;
			}
		} catch (Exception e2) {
			// TODO: handle exception
			e2.printStackTrace();
		}

		return null;

	}

	public boolean save(BmBaoCaoDTO dto) throws Exception {
		return Dao.save(dto);
	}

	public boolean isExistById(int id) throws Exception {
		return Dao.isExistById(id);
	}

	public boolean deleteById(int id) throws Exception {
		return Dao.deleteById(id);
	}

	public BmBaoCaoDTO findById(int id) throws Exception {
		return Dao.findById(id);
	}

}