package com.temp.logic.BieuMauBaoCao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangMenuBDTO;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetHangEntityDao;
import com.temp.utils.Utils;

@Component
public class SheetHangLogic {

	@Autowired
	private BmSheetHangEntityDao bmSheetHangEntityDao;

	/**
	 * thêm mới hoặc cập nhật hàng của sheet
	 * 
	 * @param dto
	 * @return
	 */
	public BmSheetHangDTO addOrUpdate(BmSheetHangDTO dto) {

		if (dto.getId() != null && dto.getId() > 0) {
			dto.setNgayCapNhat(Utils.getCurrentDate());
			return this.bmSheetHangEntityDao.AddOrUpdate(dto);
		}

		return this.bmSheetHangEntityDao.AddOrUpdate(dto);
	}

	/**
	 * Tim kiếm dựa trên Id
	 * 
	 * @param dto
	 * @return
	 */
	public BmSheetHangDTO findById(Integer id) {

		return this.bmSheetHangEntityDao.GetById(id);
	}
	
	public long maxSTTBySheetId(Integer id) {

		return this.bmSheetHangEntityDao.maxSTTBySheetId(id);
	}


	/**
	 * Lấy danh sách cấu hình hàng của sheet
	 * 
	 * @param dto
	 * @return
	 */
	public BmSheetHangMenuBDTO GetList(Integer idBieuMau, Integer pageNo, Integer pageSize) {

		return this.bmSheetHangEntityDao.getList(idBieuMau,pageNo,pageSize);
	}

	/**
	 * Lấy danh sách cấu hình hàng của sheet dropdown
	 * 
	 * @param dto
	 * @return
	 */
	public List<DropDownDTO> GetListDropDownHang(Integer sheetid) {

		return this.bmSheetHangEntityDao.getListDropDownHang(sheetid);
	}

	/**
	 * Check tồn tại mã hàng sheet khi thêm mới
	 * 
	 * @param String mã hàng
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean checkExistMaHangSheet(String maHang, Integer id, Integer sheetId) {
		if (!StringUtils.isEmpty(maHang)) {
			maHang = maHang.trim();
		} else {
			return false;
		}
		return this.bmSheetHangEntityDao.checkExistMaHangSheet(maHang, id ,sheetId);
	}
	
	/**
	 * Check tồn tại mã hàng LT khi thêm mới
	 * 
	 * @param String mã hàng
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean checkExistMaHangLT(String maHangLT, Integer id, Integer sheetId) {
		if (!StringUtils.isEmpty(maHangLT)) {
			maHangLT = maHangLT.trim();
		} else {
			return false;
		}
		return this.bmSheetHangEntityDao.checkExistMaHangLT(maHangLT, id ,sheetId);
	}

	/**
	 * Check tồn tại mã hàng sheet khi thêm mới
	 * 
	 * @param String mã hàng, Integer id
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean checkExistUpdateMaHangSheet(String maHang, Integer id,Integer sheetId) {
		if (!StringUtils.isEmpty(maHang)) {
			maHang = maHang.trim();
		}
		if (StringUtils.isEmpty(maHang) || (StringUtils.isEmpty(maHang) && id == null) || (id == null)) {
			return false;
		}
		return this.bmSheetHangEntityDao.checkExistMaHangSheetUpdate(maHang, id,sheetId);
	}

	/**
	 * Check tồn tại mã hàng sheet khi thêm mới
	 * 
	 * @param String mã hàng, Integer id
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean deleteHang(Integer id) {
		return this.bmSheetHangEntityDao.deleteHang(id);
	}
	
	public boolean resetSTT(Integer id) {
		return this.bmSheetHangEntityDao.resetSTT(id);
	}
	
	public boolean setMaHangBySTT(Integer id) {
		return this.bmSheetHangEntityDao.setMaHangBySTT(id);
	}
}
