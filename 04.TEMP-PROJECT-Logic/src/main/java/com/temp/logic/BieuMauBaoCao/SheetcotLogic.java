package com.temp.logic.BieuMauBaoCao;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotMenuBDTO;
import com.temp.model.dropdown.DropdownMegerCell;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetCotEntityDao;
import com.temp.utils.Utils;

@Component
public class SheetcotLogic {

	@Autowired
	private BmSheetCotEntityDao bmSheetCotEntityDao;

	/**
	 * thêm mới hoặc cập nhật sheet
	 * 
	 * @param dto
	 * @return
	 */
	public BmSheetCotDTO addOrUpdate(BmSheetCotDTO dto) {

		if (dto.getId() != null && dto.getId() > 0) {
			dto.setNgayCapNhat(Utils.getCurrentDate());
			return this.bmSheetCotEntityDao.AddOrUpdate(dto);
		}

		return this.bmSheetCotEntityDao.AddOrUpdate(dto);
	}

	/**
	 * Tim kiếm dựa trên Id
	 * 
	 * @param dto
	 * @return
	 */
	public BmSheetCotDTO findById(Integer id) {

		return this.bmSheetCotEntityDao.GetById(id);
	}

	/**
	 * Lấy danh sách cấu hình cột của sheet
	 * 
	 * @param sheetid
	 * @return
	 */
	public BmSheetCotMenuBDTO GetList(Integer idSheet, Integer pageNo,Integer pageSize) {

		return this.bmSheetCotEntityDao.getList(idSheet,pageNo,pageSize);
	}

	/**
	 * Lấy danh sách dropdown cột
	 * 
	 * @param dto
	 * @return
	 */
	public List<DropDownDTO> GetListDropDownCot(Integer sheetId) {

		return this.bmSheetCotEntityDao.getListDropDown(sheetId);
	}

	/**
	 * Lấy danh sách dropdown cột dùng cho action merger ô
	 * 
	 * @param dto
	 * @return
	 */
	public List<DropdownMegerCell> GetListDropDownMegerCot(Integer sheetId) {

		return this.bmSheetCotEntityDao.getListDropDownMeger(sheetId);
	}

	/**
	 * Check tồn tại mã cột sheet khi thêm mới
	 * 
	 * @param String macot
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean checkExistMaCotSheet(String maCot,Integer sheetId) {
		if (!StringUtils.isEmpty(maCot)) {
			maCot = maCot.trim();
		} else {
			return false;
		}
		return this.bmSheetCotEntityDao.checkExistMaCotSheet(maCot,sheetId);
	}

	/**
	 * Check tồn tại mã cột sheet khi thêm mới
	 * 
	 * @param String macot, Integer id
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean checkExistUpdateMaCotSheet(String maCot, Integer id, Integer sheetId) {
		if (!StringUtils.isEmpty(maCot)) {
			maCot = maCot.trim();
		}
		if (StringUtils.isEmpty(maCot) || (StringUtils.isEmpty(maCot) && id == null) || (id == null)) {
			return false;
		}
		return this.bmSheetCotEntityDao.checkExistMaCotSheetUpdate(maCot, id,sheetId);
	}
	
	/**
	 * Check tồn tại mã cột sheet khi thêm mới
	 * 
	 * @param String macot, Integer id
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean checkExistMaCotLT(String maCotLT, Integer id, Integer sheetId) {
		if (!StringUtils.isEmpty(maCotLT)) {
			maCotLT = maCotLT.trim();
		}
		if (StringUtils.isEmpty(maCotLT) || (StringUtils.isEmpty(maCotLT) && id == null) || (id == null)) {
			return false;
		}
		return this.bmSheetCotEntityDao.checkExistMaCotLT(maCotLT, id,sheetId);
	}
	
	
	/**
	 * Xóa cột
	 * 
	 * @param String macot, Integer id
	 * @return true nếu tồn tại. flase là không tồn tại
	 */
	public boolean deleteColumns(Integer id) {
	
		return this.bmSheetCotEntityDao.deleteRow(id);
	}
	
	public long maxSTTBySheetId(Integer id) {

		return this.bmSheetCotEntityDao.maxSTTBySheetId(id);
	}

}
