package com.temp.logic.BieuMauBaoCao;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetMegerCellDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangCotDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangDTO;
import com.temp.model.dropdown.DropdownMegerCell;
import com.temp.persistence.dao.BieuMauBaoCao.BmTieuDeHangCotEntityDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmTieuDeHangEntityDao;

@Component
public class SheetMegerHeaderLogic {

	@Autowired
	private BmTieuDeHangCotEntityDao bmTieuDeHangCotDao;

	@Autowired
	private BmTieuDeHangEntityDao bmTieuDeHangDao;

	public BmSheetMegerCellDTO InitHeaderHangCot(int sheetId) {
		BmSheetMegerCellDTO res = this.bmTieuDeHangCotDao.InitDisplayMergerHedear(sheetId);
		StringBuilder htmlStr = new StringBuilder("<table class=\"q-table\">" + "	<thead>");

		if (res != null && res.getSoHang() != null && res.getSoHang() > 0 && res.getSoCot() != null
				&& res.getSoCot() > 0) {
			int indexHang = 1;

			for (BmTieuDeHangDTO hang : res.getLstHang()) {
				{
					// data =>(this.tmp ="+hang.getId()+") onclick=remoteRows("+hang.getId()+")
					int indexTmp = indexHang;
					htmlStr.append("<tr>");

					if (res.getSoCot() > 0) {
						int tongCot = res.getSoCot();
						;
						Optional<BmTieuDeHangCotDTO> itemThreeTmp = null;
						if (res.getLstCot() != null) {
							itemThreeTmp = res.getLstCot().stream()
									.filter(x -> x.getFisrtRow() != null && x.getLastRow() != null
											&& x.getFisrtRow().intValue() <= indexTmp
											&& x.getLastRow().intValue() >= indexTmp)
									.findFirst();
						}

						BmTieuDeHangCotDTO dsItem = null;
						if (itemThreeTmp != null && itemThreeTmp.isPresent()) {
							dsItem = itemThreeTmp.get();
							if (dsItem.getColSpan() != null) {
								tongCot = tongCot - dsItem.getColSpan().intValue();
								if (dsItem.getTieuDeHangId() == hang.getId()) {
									tongCot++;
								}
							}
						}

						for (int i = 0; i < tongCot; i++) {
							int index = i + 1;
							if (res.getLstCot() != null) {
								Optional<BmTieuDeHangCotDTO> itemHCTwo = res.getLstCot().stream()
										.filter(x -> (x.getFisrtColumn() != null && x.getFisrtColumn() == index)
												&& (hang.getId() == x.getTieuDeHangId()))
										.findFirst();

								if (itemHCTwo.isPresent()) {

									BmTieuDeHangCotDTO tmpObj = itemHCTwo.get();

									htmlStr.append("<th colspan=\"" + tmpObj.getColSpan() + "\" rowspan= \""
											+ tmpObj.getRowSpan() + "\">" + tmpObj.getTenCot() + "</th>");

								} else {
									htmlStr.append("<th></th>");
								}

							} else {
								htmlStr.append("<th></th>");
							}
						}
					}
				}
				indexHang++;
				htmlStr.append("</tr>");
			}

		}

		htmlStr.append("</thead>");
		res.setHeaderTableStr(htmlStr.toString());
		return res;

	}

	public BmSheetMegerCellDTO InitDisplayCauHinhMegerHangCot(int sheetId) {
		BmSheetMegerCellDTO res = this.bmTieuDeHangCotDao.InitDisplayMergerHedear(sheetId);
		StringBuilder htmlStr = new StringBuilder("<table class=\"q-table\">" + "	<tbody>");

		if (res != null && res.getSoHang() != null && res.getSoHang() > 0 && res.getSoCot() != null
				&& res.getSoCot() > 0) {
			htmlStr.append("<tr>");
			for (int i = 0; i < res.getSoCot(); i++) {

				if (i > 0) {
					htmlStr.append("<td style=\"text-align:center\">" + (i + 1) + "</td>");
				} else {
					htmlStr.append("<td>Xóa</td>");
					htmlStr.append("<td>STT</td>");
					htmlStr.append("<td style=\"text-align:center\">" + (i + 1) + "</td>");

				}
			}
			htmlStr.append("</tr>");
			int indexHang = 1;
			for (BmTieuDeHangDTO hang : res.getLstHang()) {
				{
					int tmpIndexHang = indexHang;
					// data =>(this.tmp ="+hang.getId()+") onclick=remoteRows("+hang.getId()+")
					htmlStr.append("<tr>");
					htmlStr.append("<td data-id='" + hang.getId()
							+ "' class=\"buttonDelete\" style=\"width:50px; text-align:center\"><button tabindex=\"0\" type=\"button\" data-id='"
							+ hang.getId()
							+ "' class=\"button-c q-btn inline q-btn-item non-selectable no-outline q-btn--standard q-btn--round bg-red text-white q-btn--actionable q-focusable q-hoverable\" style=\"font-size: 7px;\">\r\n"
							+ "<i aria-hidden=\"true\" class=\"material-icons q-icon\" data-id='" + hang.getId()
							+ "' >clear</i></button></td>");
					htmlStr.append("<td style=\"width:50px; text-align:center\">" + tmpIndexHang + "</td>");
					for (int j = 1; j <= res.getSoCot(); j++) {
						int indexColumn = j;
						Optional<BmTieuDeHangCotDTO> itemThreeTmp = null;
						if (res.getLstCot() != null) {
							itemThreeTmp = res.getLstCot().stream()
									.filter(x -> x.getFisrtRow() == tmpIndexHang && x.getFisrtColumn() == indexColumn)
									.findAny();
							if (itemThreeTmp != null && itemThreeTmp.isPresent()) {
								BmTieuDeHangCotDTO tmpObj = itemThreeTmp.get();
								htmlStr.append("<td data-hcid=\"" + tmpObj.getId() + "\" colspan=\""
										+ tmpObj.getColSpan() + "\" rowspan= \"" + tmpObj.getRowSpan() + "\">"
										+ tmpObj.getTenCot() + "</td>");
							} else {
								itemThreeTmp = res.getLstCot().stream()
										.filter(x -> x.getFisrtRow() <= tmpIndexHang && x.getLastRow() >= tmpIndexHang
												&& x.getFisrtColumn() <= indexColumn
												&& x.getLastColumn() >= indexColumn)
										.findFirst();
								if (itemThreeTmp == null || !itemThreeTmp.isPresent()) {
									htmlStr.append("<td></td>");
								}
							}
						} else {
							htmlStr.append("<td></td>");
						}
					}
				}
				indexHang++;
				htmlStr.append("</tr>");
			}

		}

		if (res.getSoCot() != null) {
			htmlStr.append("<tr class=\"tnoneheader\">");
			for (int i = 0; i < res.getSoCot(); i++) {
				htmlStr.append("<td></td>");
			}
			htmlStr.append("</tr>");
		}

		htmlStr.append("</tbody></table>");
		res.setHeaderTableStr(htmlStr.toString());
		return res;

	}

	/**
	 * thêm mới hoặc cập nhật tiêu đề hàng
	 * 
	 * @param dto
	 * @return
	 */
	public BmTieuDeHangDTO addOrUpdateHang(BmTieuDeHangDTO dto) {

		return this.bmTieuDeHangDao.AddOrUpdate(dto);
	}

	/**
	 * thêm mới hoặc cập nhật merger hàng
	 * 
	 * @param dto
	 * @return
	 */
	public BmTieuDeHangCotDTO addOrUpdateHangCot(BmTieuDeHangCotDTO megerHangCot) {

		return this.bmTieuDeHangCotDao.AddOrUpdate(megerHangCot);
	}

	/**
	 * Xóa tiêu đề hàng
	 * 
	 * @param sheetid
	 * @return
	 */
	public boolean DeleteRows(Integer tieuDeHangId) {

		return this.bmTieuDeHangCotDao.DeleteByHangCot(tieuDeHangId);
	}

	public boolean DeleteRowHangCot(Integer tieuDeHangId) {

		return this.bmTieuDeHangCotDao.DeleteById(tieuDeHangId);
	}

	/**
	 * Xóa tiêu đề hàng
	 * 
	 * @param sheetid
	 * @return
	 */
	public List<DropdownMegerCell> DropDownHang(Integer sheetId) {

		return this.bmTieuDeHangDao.DropDownHangMeger(sheetId);
	}

	public boolean checkExistAddMeger(Integer sheetId, int fisrtRow, int lastRow, int fisrtColumn, int lastColumn) {
		return this.bmTieuDeHangCotDao.MegerCellValidateExist(sheetId, fisrtRow, lastRow, fisrtColumn, lastColumn);
	}

	/**
	 * Khoi tao header hang - cot cho excel form.
	 * 
	 * @param sheetId
	 * @return
	 */
	public BmSheetMegerCellDTO InitHeaderHangCotForExcelForm(int sheetId, List<BmSheetCotDTO> lstCot,
			boolean isAddCotStt) {

		// [Old source]
		// BmSheetMegerCellDTO res =
		// this.bmTieuDeHangCotDao.InitDisplayMergerHedear(sheetId);

		StringBuilder htmlStr = new StringBuilder("<table class=\"q-table\">" + " <thead>");
		if(lstCot != null && lstCot.get(0) != null && lstCot.get(0).getBmSheetByBmSheetId().getTieuDeChinh() != null && lstCot.get(0).getBmSheetByBmSheetId().getTieuDeChinh().toString() != null) {
			htmlStr.append("<tr><td style=\"text-align:center ; font-weight:bold; font-size: 18px\" colspan='"
					+ lstCot.size() + "'>" + lstCot.get(0).getBmSheetByBmSheetId().getTieuDeChinh().toString() + "</td></tr>");
		}
		if(lstCot != null && lstCot.get(0)!= null && lstCot.get(0).getBmSheetByBmSheetId().getTieuDePhu() != null && lstCot.get(0).getBmSheetByBmSheetId().getTieuDePhu().toString() != null) {
			htmlStr.append("<tr><td style=\"text-align:center ; font-size: 16px; font-style: italic\" colspan='"
					+ lstCot.size() + "'>" + lstCot.get(0).getBmSheetByBmSheetId().getTieuDePhu().toString() + "</td></tr>");
		}

		// [UPDATE] - fix performance
		BmSheetMegerCellDTO res = this.bmTieuDeHangCotDao.getDataDisplayMergerHedearExcel(sheetId);

		// Neu khong co du lieu Hang header or List cot, return du lieu table title
		// html.
		if ((res == null) || (lstCot == null) || (lstCot.size() == 0)) {
			BmSheetMegerCellDTO resErr = new BmSheetMegerCellDTO();
			htmlStr.append("</thead>");
			resErr.setHeaderTableStr(htmlStr.toString());
			return resErr;
		}

		// tong so cot.
		int cotNum = lstCot.size();

		if (res != null && res.getSoHang() != null && res.getSoHang() > 0) {
			int indexHang = 1;

			for (int i = 0; i < res.getLstHang().size(); i++) {
				int tmpIndexHang = indexHang;
				htmlStr.append("<tr>");
				// true: Bo sung them cot STT: Chinh sua sheet.
				if (isAddCotStt) {
					htmlStr.append("<td></td>");
				}
				// false: Ko them cot STT: Nhap data, edit, import excel.
				else {
					htmlStr.append("");
				}

				for (int j = 1; j <= cotNum; j++) {
					int indexColumn = j;
					Optional<BmTieuDeHangCotDTO> itemThreeTmp = null;
					if (res.getLstCot() != null && res.getLstCot().size() > 0) {
						itemThreeTmp = res.getLstCot().stream()
								.filter(x -> x.getFisrtRow() == tmpIndexHang && x.getFisrtColumn() == indexColumn)
								.findAny();
						if (itemThreeTmp != null && itemThreeTmp.isPresent()) {
							BmTieuDeHangCotDTO tmpObj = itemThreeTmp.get();
							htmlStr.append("<th colspan=\"" + tmpObj.getColSpan() + "\" rowspan= \""
									+ tmpObj.getRowSpan() + "\">" + tmpObj.getTenCot() + "</th>");
						} else {
							itemThreeTmp = res.getLstCot().stream()
									.filter(x -> x.getFisrtRow() <= tmpIndexHang && x.getLastRow() >= tmpIndexHang
											&& x.getFisrtColumn() <= indexColumn && x.getLastColumn() >= indexColumn)
									.findFirst();
							if (itemThreeTmp == null || !itemThreeTmp.isPresent()) {
								htmlStr.append("<th></th>");
							}
						}
					} else {
						htmlStr.append("<th></th>");
					}
				}
				indexHang++;
				htmlStr.append("</tr>");
			}
		} else {
			htmlStr.append("<tr>");

			// true: Bo sung them cot STT: Chinh sua sheet.
			if (isAddCotStt) {
				htmlStr.append("<td></td>");
			}
			// false: Ko them cot STT: Nhap data, edit, import excel.
			else {
				htmlStr.append("");
			}

			for (BmSheetCotDTO cotDTO : lstCot) {
//				htmlStr.append("<th>");
//				htmlStr.append(cotDTO.getTenCot());
//				htmlStr.append("</th>");

				if (cotDTO.getTenCot().equals("STT")) {
					htmlStr.append("<th style='width: 10px'>");
				} else if (cotDTO.getTenCot().equals("Chỉ tiêu")) {
					htmlStr.append("<th style='width: 500px;'>");
				} else {
					htmlStr.append("<th>");
				}
				htmlStr.append(cotDTO.getTenCot());
				htmlStr.append("</th>");
			}
			htmlStr.append("</tr>");
		}

		htmlStr.append("</thead>");
		res.setHeaderTableStr(htmlStr.toString());
		return res;

	}

	/**
	 * Kiem tra exist tieu de hang cot
	 * 
	 * @param sheetId
	 * @return
	 * @throws Exception
	 */
	public boolean checkExistTieuDeHangLogic(Integer sheetId) throws Exception {
		return this.bmTieuDeHangDao.checkExistTieuDeHangDao(sheetId);
	}

	public BmSheetMegerCellDTO InitHeaderHangCotRemix(int sheetId) {
		BmSheetMegerCellDTO res = this.bmTieuDeHangCotDao.InitDisplayMergerHedear(sheetId);
		StringBuilder htmlStr = new StringBuilder();
		htmlStr.append(
				"<div class=\"q-table__container q-table--cell-separator q-table__card q-table--no-wrap\" style=\"height:500px;overflow:auto\"><div class=\"q-table__middle scroll\">");// "style=\"display:none\"");
//		htmlStr.append("<p style=\"margin-top:10px;margin-left:20px;font-weight:bold\">" + kyBaoCaoStr == null ? "" : kyBaoCaoStr + "</p>");// "style=\"display:none\"");
		htmlStr.append("<table id=\"divBaoCaoTongHop\" class=\"q-table q-table_n-sticky\" style=\"width: 100%\" >");
		htmlStr.append("<thead>");
		if (res != null && res.getSoHang() != null && res.getSoHang() > 0 && res.getSoCot() != null
				&& res.getSoCot() > 0) {
			int indexHang = 1;

			for (BmTieuDeHangDTO hang : res.getLstHang()) {
				{
					// data =>(this.tmp ="+hang.getId()+") onclick=remoteRows("+hang.getId()+")
					int indexTmp = indexHang;
					htmlStr.append("<tr>");

					if (res.getSoCot() > 0) {
						int tongCot = res.getSoCot();
						;
						Optional<BmTieuDeHangCotDTO> itemThreeTmp = null;
						if (res.getLstCot() != null) {
							itemThreeTmp = res.getLstCot().stream()
									.filter(x -> x.getFisrtRow() != null && x.getLastRow() != null
											&& x.getFisrtRow().intValue() <= indexTmp
											&& x.getLastRow().intValue() >= indexTmp)
									.findFirst();
						}

						BmTieuDeHangCotDTO dsItem = null;
						if (itemThreeTmp != null && itemThreeTmp.isPresent()) {
							dsItem = itemThreeTmp.get();
							if (dsItem.getColSpan() != null) {
								tongCot = tongCot - dsItem.getColSpan().intValue();
								if (dsItem.getTieuDeHangId() == hang.getId()) {
									tongCot++;
								}
							}
						}

						for (int i = 0; i < res.getLstCot().size(); i++) {
							int index = i + 1;
							if (res.getLstCot() != null) {
								Optional<BmTieuDeHangCotDTO> itemHCTwo = res.getLstCot().stream()
										.filter(x -> (x.getFisrtColumn() != null && x.getFisrtColumn() == index)
												&& (hang.getId() == x.getTieuDeHangId()))
										.findFirst();

								if (itemHCTwo.isPresent()) {

									BmTieuDeHangCotDTO tmpObj = itemHCTwo.get();

									htmlStr.append("<th colspan=\"" + tmpObj.getColSpan() + "\" rowspan= \""
											+ tmpObj.getRowSpan() + "\">" + tmpObj.getTenCot() + "</th>");

								} else {
									htmlStr.append("");// <th></th>
								}

							} else {
								htmlStr.append("");// <th></th>
							}
						}
					}
				}
				indexHang++;
				htmlStr.append("</tr>");
			}

		}

		htmlStr.append("</thead>");
		res.setHeaderTableStr(htmlStr.toString());
		return res;

	}

	public BmSheetMegerCellDTO InitHeaderHangCotRemixKhoangTg(int sheetId, String kyBaoCaoStr) {
		BmSheetMegerCellDTO res = this.bmTieuDeHangCotDao.InitDisplayMergerHedear(sheetId);
		StringBuilder htmlStr = new StringBuilder();
//		htmlStr.append("<div class=\"q-table__container q-table--cell-separator q-table__card q-table--no-wrap\" style=\"height:500px;overflow:auto\"><div class=\"q-table__middle scroll\">");// "style=\"display:none\"");
		htmlStr.append("<p style=\"margin-top:10px;margin-left:20px;font-weight:bold\">" + kyBaoCaoStr == null ? ""
				: kyBaoCaoStr + "</p>");// "style=\"display:none\"");
		htmlStr.append("<table id=\"divBaoCaoTongHop\" class=\"q-table q-table_n-sticky\" style=\"width: 100%\" >");
		htmlStr.append("<thead>");
		if (res != null && res.getSoHang() != null && res.getSoHang() > 0 && res.getSoCot() != null
				&& res.getSoCot() > 0) {
			int indexHang = 1;

			for (BmTieuDeHangDTO hang : res.getLstHang()) {
				{
					// data =>(this.tmp ="+hang.getId()+") onclick=remoteRows("+hang.getId()+")
					int indexTmp = indexHang;
					htmlStr.append("<tr>");

					if (res.getSoCot() > 0) {
						int tongCot = res.getSoCot();
						;
						Optional<BmTieuDeHangCotDTO> itemThreeTmp = null;
						if (res.getLstCot() != null) {
							itemThreeTmp = res.getLstCot().stream()
									.filter(x -> x.getFisrtRow() != null && x.getLastRow() != null
											&& x.getFisrtRow().intValue() <= indexTmp
											&& x.getLastRow().intValue() >= indexTmp)
									.findFirst();
						}

						BmTieuDeHangCotDTO dsItem = null;
						if (itemThreeTmp != null && itemThreeTmp.isPresent()) {
							dsItem = itemThreeTmp.get();
							if (dsItem.getColSpan() != null) {
								tongCot = tongCot - dsItem.getColSpan().intValue();
								if (dsItem.getTieuDeHangId() == hang.getId()) {
									tongCot++;
								}
							}
						}

						for (int i = 0; i < res.getLstCot().size(); i++) {
							int index = i + 1;
							if (res.getLstCot() != null) {
								Optional<BmTieuDeHangCotDTO> itemHCTwo = res.getLstCot().stream()
										.filter(x -> (x.getFisrtColumn() != null && x.getFisrtColumn() == index)
												&& (hang.getId() == x.getTieuDeHangId()))
										.findFirst();

								if (itemHCTwo.isPresent()) {

									BmTieuDeHangCotDTO tmpObj = itemHCTwo.get();

									htmlStr.append("<th colspan=\"" + tmpObj.getColSpan() + "\" rowspan= \""
											+ tmpObj.getRowSpan() + "\">" + tmpObj.getTenCot() + "</th>");

								} else {
									htmlStr.append("");// <th></th>
								}

							} else {
								htmlStr.append("");// <th></th>
							}
						}
					}
				}
				indexHang++;
				htmlStr.append("</tr>");
			}

		}

		htmlStr.append("</thead>");
		res.setHeaderTableStr(htmlStr.toString());
		return res;

	}

}
