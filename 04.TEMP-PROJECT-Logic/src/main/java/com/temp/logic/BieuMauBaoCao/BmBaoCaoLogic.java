package com.temp.logic.BieuMauBaoCao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Splitter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.temp.global.UserInfoGlobal;
import com.temp.model.DropDownDTO;
import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDTO;
import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDongDTO;
import com.temp.model.BieuMauBaoCao.BcBaoCaoGtDongJsonDTO;
import com.temp.model.BieuMauBaoCao.BcKhaiThacGtBDTO;
import com.temp.model.BieuMauBaoCao.BcKhaiThacGtDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoExportDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoLichSuDTO;
import com.temp.model.BieuMauBaoCao.BmLichSuBaoCaoBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangCotDTO;
import com.temp.model.BieuMauBaoCao.BmTieuDeHangDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.dropdown.DropdownDefault;
import com.temp.model.uploadfile.UploadFileDefaultDTO;
import com.temp.persistence.dao.BieuMauBaoCao.BcBaoCaoGtDongDao;
import com.temp.persistence.dao.BieuMauBaoCao.BcBaoCaoGtEntityDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmBaoCaoLichSuDao;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetEntityDao;
import com.temp.utils.CellConfig;
import com.temp.utils.TimestampUtils;

@Component
public class BmBaoCaoLogic {

	@Autowired
	private BmBaoCaoDao bmBaoCaoDao;

	@Autowired
	private BcBaoCaoGtEntityDao bcBaoCaoGtEntityDao;

	@Autowired
	private BcBaoCaoGtDongDao bcBaoCaoGtDongDao;

	@Autowired
	private BmBaoCaoLichSuDao bmBaoCaoLichSuDao;

	@Autowired
	private BmSheetEntityDao bmSheetBaoCao;

	@Autowired
	private SheetLogic sheetLogic;

	/**
	 * xóa 1 cổ đông
	 * 
	 * @param BmBaoCaoId
	 * @return true nếu thành công ngươc lại false không thành công
	 */
	public boolean deleteBmBaoCao(int BmBaoCaoId) {
		if (BmBaoCaoId > 0) {
			if (this.bmBaoCaoDao.deleteCoDongById(BmBaoCaoId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	public boolean copyById(int BmBaoCaoId) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		int userId = userInfo.getId();
		if (BmBaoCaoId > 0) {
			Integer idBc = null;
			idBc = this.bmBaoCaoDao.copyByIdInteger(BmBaoCaoId, userId);
			if (idBc != null && idBc > 0) {
				BmBaoCaoDTO bmBcOld = this.bmBaoCaoDao.findById(BmBaoCaoId);
				BmBaoCaoDTO bmBcTemp = this.bmBaoCaoDao.findById(idBc);
				if (bmBcOld != null && bmBcTemp != null)
					ghiLichSuPhienBan(bmBcTemp, "Sao chép từ biểu mẫu báo cáo mã " + bmBcOld.getMaBaoCao(), false);

				return true;
			}
			return false;
		}

		return false;
	}

	public boolean duaVaoSuDung(int BmBaoCaoId) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		int userId = userInfo.getId();
		if (BmBaoCaoId > 0) {
			Integer bmBaoCaoIdOutput = this.bmBaoCaoDao.duaVaoSuDung(BmBaoCaoId, userId);
			if (bmBaoCaoIdOutput > 0) {
//				BmBaoCaoDTO duaVAoSuDung = this.bmBaoCaoDao.findById(bmBaoCaoIdOutput);
				return true;
//				if(duaVAoSuDung.getId()>0) {
//					ghiLichSuPhienBan(duaVAoSuDung, ""
//							+ "Đưa vào sử dụng biểu mẫu báo cáo",true);
//				}
			}
		}
		return false;
	}

	public boolean suDungBaoCao(int id, int isSuDung) {
		if (id > 0) {
			return this.bmBaoCaoLichSuDao.suDungBaoCao(id, isSuDung);
		}
		return false;
	}

	/**
	 * Thêm mới hoặc cập nhập cổ đông ctck
	 * 
	 * @param BmBaoCao dữ liệu input
	 * @return true nếu cập nhật hoặc update thành công ngược lại là false
	 * @throws Exception
	 */
	public BmBaoCaoDTO addOrUpDateBmBaoCao(BmBaoCaoDTO dto) throws Exception {

		if (dto != null) {
			if (dto.getId() != null && dto.getId() > 0) {

				dto.setNgayCapNhat(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
				BmBaoCaoDTO dtoNewUpdate = this.bmBaoCaoDao.AddorUpdate(dto);

				if (dtoNewUpdate.getId() > 0) {
					ghiLichSuPhienBan(dtoNewUpdate, "Cập nhật biểu mẫu báo cáo", false);
				}
			} else {
				dto.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
				BmBaoCaoDTO dtoNewSaved = this.bmBaoCaoDao.AddorUpdate(dto);
				if (dtoNewSaved.getId() > 0) {
					ghiLichSuPhienBan(dtoNewSaved, "Thêm mới biểu mẫu báo cáo", false);
				}

				return dtoNewSaved;
			}

		}
		return new BmBaoCaoDTO();
	}

	public BmBaoCaoDTO addOrUpDateBmBaoCao(BmBaoCaoDTO dto, String lichSuStr) throws Exception {

		if (dto != null) {
			if (dto.getId() != null && dto.getId() > 0) {

				dto.setNgayCapNhat(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
				BmBaoCaoDTO dtoNewUpdate = this.bmBaoCaoDao.AddorUpdate(dto);

				if (dtoNewUpdate.getId() > 0) {
					ghiLichSuPhienBan(dtoNewUpdate, lichSuStr, false);
				}
			} else {
				dto.setNgayTao(TimestampUtils.DateUtilsParseTimeStamp(new Date()));
				BmBaoCaoDTO dtoNewSaved = this.bmBaoCaoDao.AddorUpdate(dto);
				if (dtoNewSaved.getId() > 0) {
					ghiLichSuPhienBan(dtoNewSaved, lichSuStr, false);
				}

				return dtoNewSaved;
			}

		}
		return new BmBaoCaoDTO();
	}

	private void ghiLichSuPhienBan(BmBaoCaoDTO dto, String thaoTac, Boolean duaVaoSuDung) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		Integer suDung = 0;
		Integer nguoiThucHien = userInfo.getId();
		Integer bmId = dto.getId();
		Integer bmBaoCaoId = dto.getBmBaoCaoId();
		if (duaVaoSuDung) {
			suDung = 1;
		}
		Integer phienBan = dto.getPhienBan() == null ? null : dto.getPhienBan().intValue();
		String tenNguoiThucHien = userInfo.getHoTen();

		BmBaoCaoLichSuDTO bieuMauLichSu = new BmBaoCaoLichSuDTO(userInfo, thaoTac, bmId, bmBaoCaoId, suDung, phienBan,
				tenNguoiThucHien);
//				log history version create
		bmBaoCaoLichSuDao.create(bieuMauLichSu);
	}

	/**
	 * lấy chi tiết 1 cổ đông
	 * 
	 * @param id định danh id cổ đông
	 * @return đối tượng cổ đông
	 */
	public BmBaoCaoDTO getById(int id) {
		if (id > 0) {
			BmBaoCaoDTO qtDTO = this.bmBaoCaoDao.findById(id);
			if (qtDTO != null) {
				qtDTO.setGroupSelected(this.getNhomSelected(qtDTO.getNhomBaoCao()));
				qtDTO.setDoiTuongSelected(this.getDoiTuongSelected(qtDTO.getDoiTuongGui()));
				return qtDTO;
			}
		}

		return null;
	}

	public BmBaoCaoExportDTO getDataExport(int bmBaoCaoId) {

		BmBaoCaoExportDTO result = new BmBaoCaoExportDTO();

		if (bmBaoCaoId > 0) {

			// Get sheet bao cao.
			List<BmSheetDTO> listBmSheet = bmSheetBaoCao.listSheetByBmBaoCaoId(bmBaoCaoId);

			List<BmSheetDTO> sortedList = listBmSheet.stream().sorted(Comparator.comparingLong(BmSheetDTO::getThuTu))
					.collect(Collectors.toList());

			sortedList.stream().forEach(s -> s.setSheetNameExcel(s.getTenSheet() + "_" + s.getMaSheet()));

			result.setListBmSheet(sortedList);
			result.setId(bmBaoCaoId);

		}

		return result;
	}

	private List<DropdownDefault> getNhomSelected(String stringnhomId) {

		List<DropdownDefault> selected = new ArrayList<DropdownDefault>();

		if (stringnhomId != null && !stringnhomId.isEmpty()) {
			Map<String, DropdownDefault> map = new HashMap<String, DropdownDefault>();
			DropdownDefault select1 = new DropdownDefault("Báo cáo hoạt động", "HD");
			DropdownDefault select2 = new DropdownDefault("Báo cáo tài chính riêng", "TCR");
			DropdownDefault select3 = new DropdownDefault("Báo cáo hợp nhất", "HN");
			DropdownDefault select4 = new DropdownDefault("Báo cáo tỷ lệ an toàn tài chính", "TLATTC");
			DropdownDefault select5 = new DropdownDefault("Báo cáo đăng ký giao dịch trực tuyến", "DKGDTT");
			DropdownDefault select6 = new DropdownDefault("Khác", "K");
			DropdownDefault select7 = new DropdownDefault("Báo cáo kiểm soát - kiểm soát đặc biệt", "KSKSDB");

			map.put((String) select1.getValue(), select1);
			map.put((String) select2.getValue(), select2);
			map.put((String) select3.getValue(), select3);
			map.put((String) select4.getValue(), select4);
			map.put((String) select5.getValue(), select5);
			map.put((String) select6.getValue(), select6);
			map.put((String) select7.getValue(), select7);

			List<String> resultList = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(stringnhomId);

			for (String string : resultList) {
				selected.add(map.get(string));
			}

		}

		return selected;

	}

	private List<DropdownDefault> getDoiTuongSelected(String stringDoiTuongId) {

		List<DropdownDefault> selected = new ArrayList<DropdownDefault>();

		if (stringDoiTuongId != null) {
			List<String> resultList = Splitter.on(',').trimResults().omitEmptyStrings().splitToList(stringDoiTuongId);

			for (String string : resultList) {
				if (string.equals("CTCK")) {
					selected.add(new DropdownDefault("Công ty chứng khoán", "CTCK"));
				}
				if (string.equals("VPDD")) {
					selected.add(new DropdownDefault("VPĐD công ty chứng khoán nước ngoài tại VN", "VPDD"));
				}
				if (string.equals("CNCTCK")) {
					selected.add(new DropdownDefault("CN công ty chứng khoán nước ngoài tại VN", "CNCTCK"));
				}
			}
		}

		return selected;

	}

	/**
	 * kiểm tra tồn tại object khi update
	 * 
	 * @param dto
	 * @return true nếu tồn tại còn không thì là false
	 */
	public boolean isExistBmBaoCaoUpdate(BmBaoCaoDTO dto) {

//		boolean result = this.bmBaoCaoDao.isBmBaoCaoExistUpdate(dto.getId(), 0, "",
//				0);

		return true;
	}

	/**
	 * kiểm tra trước khi add object
	 * 
	 * @param dto
	 * @return true nếu tồn tại còn không thì là false
	 */
	public boolean isExistBmBaoCaoAdd(BmBaoCaoDTO dto) {

		// boolean result = this.bmBaoCaoDao.isBmBaoCaoExistAdd("", 0,0);

		return true;
	}

	/**
	 * check tồn tại id
	 * 
	 * @param id
	 * @return
	 */
	public boolean isExistById(int id) {

		boolean result = false;

		result = this.bmBaoCaoDao.isExistById(id);

		return result;
	}

	/**
	 * Get BM bao cao dropdown list
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<DropDownDTO> getBmBaoCaoDropDownLogic() throws Exception {
		return this.bmBaoCaoDao.getBmBaoCaoDropDown();

	}

	public BmBaoCaoBDTO listBieuMauBaoCao(String keySearch, String tenBaoCao, String canCuPhapLy, Integer trangThai,
			String loaiBaoCao, Integer pageNo, Integer pageSize, String keySort, boolean desc, Integer kieuBaoCao,
			String maBaoCao, String nhomBaoCao) {

//		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

		BmBaoCaoBDTO lst = this.bmBaoCaoDao.listBieuMauBaoCao(keySearch, tenBaoCao, canCuPhapLy, trangThai, loaiBaoCao,
				pageNo, pageSize, keySort, desc, kieuBaoCao, maBaoCao, nhomBaoCao);

		return lst;

	}

	public Workbook generateFileTemplateExcel(BmBaoCaoExportDTO dto, Workbook workbook, Boolean bieuMauDauRa)
			throws Exception {

		List<BmSheetDTO> listBmSheet = dto.getListBmSheet();

		Collection<BmSheetCotDTO> cots = null;
		Collection<BmSheetHangDTO> hangs = null;
		Collection<BmTieuDeHangDTO> tieuDeHang = null;
		Collection<BmTieuDeHangCotDTO> tieuDeHangCot = null;

		createSheetSetting(workbook, dto);

		for (BmSheetDTO sheetItem : listBmSheet) {
			String tieuDeChinh = sheetItem.getTieuDeChinh();
			String tieuDePhu = sheetItem.getTieuDePhu();

//			sheetItem.setBmSheetCotCollection(sheetItem.getBmSheetCotCollection().stream().filter(x -> x.getSuDung() == true)
//					.collect(Collectors.toList()));
//			
//			sheetItem.setBmSheetHangCollection(sheetItem.getBmSheetHangCollection().stream().filter(x -> x.getSuDung() == true)
//					.collect(Collectors.toList()));

			if (sheetItem.getTrangBia()) {
				// create Trang bia
				BmBaoCaoExportDTO exportDTO = new BmBaoCaoExportDTO();
				exportDTO = getDataExport(dto.getId());
				workbook = this.generateTrangBiaBaoCaoSheet(exportDTO, sheetItem, workbook);

				CellConfig.autoSizeColumns(workbook);

			} else {
				Sheet sheet = workbook.createSheet(sheetItem.getSheetNameExcel());

				cots = sheetItem.getBmSheetCotCollection();
				hangs = sheetItem.getBmSheetHangCollection();
				tieuDeHang = sheetItem.getBmTieuDeHangCollection();
				tieuDeHangCot = sheetItem.getBmTieuDeHangCotCollection();

				int hang = hangs == null ? 0 : hangs.size();
				int cot = cots == null ? 0 : cots.size();
				int tongHangTieuDe = tieuDeHang == null ? 0 : tieuDeHang.size();
				if (tongHangTieuDe <= 1) {
					tongHangTieuDe = 1;
				}
//						int tongTieuDeHangCot= tieuDeHangCot==null?0:tieuDeHangCot.size();

				if (hang > 0 && cot > 0) {
					createTable(sheetItem, tongHangTieuDe, hang, cot, sheet, workbook, tieuDeChinh, tieuDePhu,
							dto.getId(), dto.getThanhVienId());
				}

			}

		}

		if (workbook.getNumberOfSheets() > 1) {
			workbook.setActiveSheet(0);
		}

		return workbook;

	}

	private void createSheetSetting(Workbook workbook, BmBaoCaoExportDTO dto) {
		Sheet sheetSetting = workbook.createSheet("Setting");
		CellStyle cellStyle = this.createCellStyleData(sheetSetting, workbook, false);

		Row row = sheetSetting.createRow(0);
		Cell cell = row.createCell(0, CellType.STRING);
		cell.setCellValue(dto.getId());
		cell.setCellStyle(cellStyle);
		workbook.setSheetHidden(workbook.getSheetIndex("Setting"), true);

	}

	private void createTable(BmSheetDTO sheetItem, int tongHangTieuDe, int rowNumber, int colNumber, Sheet sheet,
			Workbook workbook, String tieuDeChinh, String tieuDePhu, Integer bmBaoCaoId, Integer thanhVienId) {
		Cell cell = null;
		Row row = null;
		CellStyle styleHeader = this.createCellStyleHeader(sheet, workbook, true);
		CellStyle styleCellAlignCenter = this.createCellStyleData(sheet, workbook, false);
		// Tao tieu de
		createTableHeader(sheetItem, tongHangTieuDe, colNumber, sheet, cell, row, workbook, tieuDeChinh, tieuDePhu);

		if (thanhVienId != null && thanhVienId > 0) {
			createTableDataBaoCaoDinhky(workbook, sheet, cell, row, styleCellAlignCenter, tongHangTieuDe, rowNumber,
					colNumber, sheetItem, tieuDePhu, bmBaoCaoId, thanhVienId);
		} else {
			createTableData(workbook, sheet, cell, row, styleCellAlignCenter, tongHangTieuDe, rowNumber, colNumber,
					sheetItem, tieuDePhu);
		}

	}

	private void createTableKhaiThac(BmSheetDTO sheetItem, int tongHangTieuDe, int rowNumber, int colNumber,
			Sheet sheet, Workbook workbook, String tieuDeChinh, String tieuDePhu, Integer bmBaoCaoId,
			BcKhaiThacGtBDTO lstBcKhaiThacGt, Integer hangInSheet, Integer hangInBcKt) {
		Cell cell = null;
		Row row = null;
		CellStyle styleHeader = this.createCellStyleHeader(sheet, workbook, true);
		CellStyle styleCellAlignCenter = this.createCellStyleData(sheet, workbook, false);
		// Tao tieu de
		createTableHeader(sheetItem, tongHangTieuDe, colNumber, sheet, cell, row, workbook, tieuDeChinh, tieuDePhu);

		if (lstBcKhaiThacGt != null && lstBcKhaiThacGt.getLstBc() != null && lstBcKhaiThacGt.getLstBc().size() > 0) {
			createTableDataBaoCaoKhaiThac(workbook, sheet, cell, row, styleCellAlignCenter, tongHangTieuDe, rowNumber,
					colNumber, sheetItem, tieuDePhu, bmBaoCaoId, lstBcKhaiThacGt, hangInSheet, hangInBcKt);
		} else {
			createTableData(workbook, sheet, cell, row, styleCellAlignCenter, tongHangTieuDe, rowNumber, colNumber,
					sheetItem, tieuDePhu);
		}

	}

	/**
	 * Get workbook thong tin trang bia.
	 * 
	 * @param exportDTO
	 * @return
	 * @throws Ex
	 */
	private Workbook generateTrangBiaBaoCaoSheet(BmBaoCaoExportDTO exportDTO, BmSheetDTO thongtinSheetBiaDTO,
			Workbook workbook) throws Exception {
		int rownum = 12;
		Cell cell = null;
		Row row = null;
		List<BmSheetDTO> listBmSheet = exportDTO.getListBmSheet();

		// ---------------------------
//	try {
//	    workbook = new XSSFWorkbook(new FileInputStream("/ExcelTemp/Template_RP.xlsx"));
//	} catch (FileNotFoundException e1) {
//	    // TODO Auto-generated catch block
//	    e1.printStackTrace();
//	    return null;
//	} catch (IOException e1) {
//	    // TODO Auto-generated catch block
//	    e1.printStackTrace();
//	    return null;
//	}

		List<BmSheetDTO> listSheet = listBmSheet.stream().filter(m -> (!m.getTrangBia())).collect(Collectors.toList());

		int sheetNumber = listBmSheet.size();

		Sheet sheetBia = workbook.getSheet("Trang bìa");
		CellStyle st = createCellStyleHeader(sheetBia, workbook, true);

		CellStyle cellDataStyle = this.createCellStyle(sheetBia, workbook, false, 11, true, false, false, 0,
				HorizontalAlignment.LEFT, (short) 0);

		sheetBia.shiftRows(13, 13 + sheetNumber, sheetNumber);

		CellStyle cellStyleNguoiKy = createCellStyleTenNguoiKy(sheetBia, workbook);

		// Tiêu đề 1 :
		row = sheetBia.createRow(3);
		cell = row.createCell(1);
		cell.setCellValue(thongtinSheetBiaDTO.getThongTinCongTy());
		cell.setCellStyle(this.createCellStyleThongTinCongTy(sheetBia, workbook, true, 13, true, true, 0,
				HorizontalAlignment.LEFT, (short) 0));

		// Tiêu đề Cộng hòa xã hội chủ nghĩa việt nam
		cell = row.createCell(2);
		cell.setCellValue(thongtinSheetBiaDTO.getQuocHieu());
		cell.setCellStyle(this.createCellStyleQuocHieu(sheetBia, workbook, true, 11, true, true, 0,
				HorizontalAlignment.CENTER, (short) 0));

//		row = sheetBia.getRow(7);
		sheetBia.addMergedRegion(new CellRangeAddress(7, 7, 1, 3));
		row = sheetBia.getRow(7);
		cell = row.getCell(1);
		cell.setCellValue("Kính gửi: Ủy ban Chứng khoán Nhà nước");
		cell.setCellStyle(this.createCellStyleQuocHieu(sheetBia, workbook, true, 11, true, true, 0,
				HorizontalAlignment.CENTER, (short) 0));

		// Tiêu đề
		row = sheetBia.getRow(9);
		cell = row.getCell(1);
		cell.setCellValue(thongtinSheetBiaDTO.getBmBaoCaoByBmBaoCaoId().getTenBaoCao().toUpperCase());

		// Căn cứ pháp lý
		row = sheetBia.getRow(11);
		cell = row.getCell(1);
		cell.setCellValue(thongtinSheetBiaDTO.getBmBaoCaoByBmBaoCaoId().getCanCuPhapLy());

		// table sheet
		CreationHelper createHelper1 = workbook.getCreationHelper();
		int stt1 = 0;
		for (BmSheetDTO sheetDto : listSheet) {
			stt1++;
			rownum++;
			row = sheetBia.createRow((rownum));

			// STT column
			cell = row.createCell(1, CellType.STRING);
			cell.setCellValue((stt1));
			cell.setCellStyle(this.createCellStyle(sheetBia, workbook, false, 11, true, false, false, 0,
					HorizontalAlignment.CENTER, (short) 0));

			// Noi dung sheet
			cell = row.createCell(2, CellType.STRING);
			cell.setCellValue(sheetDto.getTieuDeChinh() == null ? StringUtils.EMPTY : sheetDto.getTieuDeChinh());
			cell.setCellStyle(cellDataStyle);

			// Mã sheet= [Tên sheet]_[Mã sheet]
			cell = row.createCell(3, CellType.STRING);
			cell.setCellValue(getMaSheet(sheetDto));

			// link sheet

			String sheetExcelName = getSheetNameExcel(sheetDto);
			Hyperlink linkSheet = createHelper1.createHyperlink(HyperlinkType.DOCUMENT);
			linkSheet.setAddress(sheetExcelName == null ? StringUtils.EMPTY : "'" + sheetExcelName + "'!A1");
			cell.setHyperlink(linkSheet);
			cell.setCellStyle(this.createCellStyle(sheetBia, workbook, false, 11, true, false, false, 1,
					HorizontalAlignment.LEFT, IndexedColors.BLUE.getIndex()));

		}

		row = sheetBia.getRow(13 + sheetNumber);
		cell = row.getCell(2);
		cell.setCellValue(thongtinSheetBiaDTO.getBmBaoCaoByBmBaoCaoId().getMoTa());
//		cell.setCellValue(thongtinSheetBiaDTO.getGhiChu());

		// Ghi chú
		row = sheetBia.createRow(14 + sheetNumber);
		cell = row.createCell(1);
		cell.setCellValue(thongtinSheetBiaDTO.getGhiChu());
		cell.setCellStyle(this.createCellStyleGhiChu(sheetBia, workbook, false, 11, true, true, 0,
				HorizontalAlignment.LEFT, (short) 0));

		// Người ký

		row = sheetBia.getRow(16 + sheetNumber);
		cell = row.getCell(3);
		cell.setCellValue("Lập, ngày … tháng … năm …");

		List<String> arrNguoiKy = thongtinSheetBiaDTO.getArrNguoiKy() == null ? new ArrayList<String>()
				: thongtinSheetBiaDTO.getArrNguoiKy();
		row = sheetBia.getRow(17 + sheetNumber);

		int thuTu = 1;
		for (String nguoiKy : arrNguoiKy) {
			cell = row.getCell(thuTu);
			cell.setCellValue(nguoiKy);
			cell.setCellStyle(cellStyleNguoiKy);
			thuTu++;
		}

		row = sheetBia.getRow(18 + sheetNumber);
		thuTu = 1;
		for (String nguoiKy : arrNguoiKy) {
			cell = row.getCell(thuTu);
			if (thuTu == 3) {
				cell.setCellValue("(Ký, họ tên, đóng dấu)");
			} else {
				cell.setCellValue("(Ký, họ tên)");
			}

			thuTu++;
		}

		return workbook;
	}

	private String getSheetNameExcel(BmSheetDTO bmSheet) {
		String sheetName = "";
		sheetName = bmSheet.getTenSheet() + "_" + bmSheet.getMaSheet();

		if (sheetName != null && sheetName.length() > 31) {
			sheetName = sheetName.substring(0, 31);

		}

		return sheetName;
	}

	private String getMaSheet(BmSheetDTO sheetDto) {
		String tenSheet = sheetDto.getTenSheet() == null ? "" : sheetDto.getTenSheet();
		String maSheet = sheetDto.getMaSheet() == null ? "" : sheetDto.getMaSheet();

		return tenSheet + "_" + maSheet;
	}

	/**
	 * Set cell style for header
	 * 
	 * @param sheet
	 * @param workbook
	 * @return
	 */
	private CellStyle createCellStyle(Sheet sheet, Workbook workbook, boolean isFontBold, int fontSize,
			boolean isSetBorder, boolean isFontItalic, boolean isWraptext, int underlineVal, HorizontalAlignment algin,
			short color) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set border
		if (isSetBorder) {
			headerCellStyle.setBorderBottom(BorderStyle.THIN);
			headerCellStyle.setBorderTop(BorderStyle.THIN);
			headerCellStyle.setBorderRight(BorderStyle.THIN);
			headerCellStyle.setBorderLeft(BorderStyle.THIN);
		}

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(isFontBold);
		font.setItalic(isFontItalic);
		font.setFontHeightInPoints((short) fontSize);
		font.setUnderline((byte) underlineVal);
		font.setColor((short) color);
		headerCellStyle.setFont(font);

		// set alignment
		headerCellStyle.setAlignment(algin);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		// Cell wraptext
		headerCellStyle.setWrapText(isWraptext);

		return headerCellStyle;
	}

	private CellStyle createCellStyleGhiChu(Sheet sheet, Workbook workbook, boolean isFontBold, int fontSize,
			boolean isFontItalic, boolean isWraptext, int underlineVal, HorizontalAlignment algin, short color) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(isFontBold);
		font.setItalic(isFontItalic);
		font.setFontHeightInPoints((short) fontSize);
		font.setUnderline((byte) underlineVal);
		font.setColor((short) color);
		headerCellStyle.setFont(font);

		headerCellStyle.setDataFormat((short) 100);

		// set alignment
		headerCellStyle.setAlignment(algin);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		// Cell wraptext
		headerCellStyle.setWrapText(isWraptext);

		return headerCellStyle;
	}

	private CellStyle createCellStyleThongTinCongTy(Sheet sheet, Workbook workbook, boolean isFontBold, int fontSize,
			boolean isFontItalic, boolean isWraptext, int underlineVal, HorizontalAlignment algin, short color) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(isFontBold);
//		font.setItalic(isFontItalic);
		font.setFontHeightInPoints((short) fontSize);
		font.setUnderline((byte) underlineVal);
		font.setColor((short) color);
		headerCellStyle.setFont(font);

		headerCellStyle.setDataFormat((short) 100);

		// set alignment
		headerCellStyle.setAlignment(algin);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		// Cell wraptext
		headerCellStyle.setWrapText(isWraptext);

		return headerCellStyle;
	}

	private CellStyle createCellStyleQuocHieu(Sheet sheet, Workbook workbook, boolean isFontBold, int fontSize,
			boolean isFontItalic, boolean isWraptext, int underlineVal, HorizontalAlignment algin, short color) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(isFontBold);
//		font.setItalic(isFontItalic);
		font.setFontHeightInPoints((short) fontSize);
		font.setUnderline((byte) underlineVal);
		font.setColor((short) color);
		headerCellStyle.setFont(font);

		headerCellStyle.setDataFormat((short) 100);

		// set alignment
		headerCellStyle.setAlignment(algin);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		// Cell wraptext
		headerCellStyle.setWrapText(isWraptext);

		return headerCellStyle;
	}

	private CellStyle createCellStyleTenNguoiKy(Sheet sheet, Workbook workbook) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setBold(true);
		font.setFontHeightInPoints((short) 11);
		headerCellStyle.setFont(font);

		// set alignment
		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		// Cell wraptext

		return headerCellStyle;
	}

	/**
	 * Create header row width style
	 * 
	 * @param row
	 * @param sheet
	 * @param cell
	 * @param rownum
	 * @param style
	 */
	private void createHeaderRow(Row row, Sheet sheet, Cell cell, int rownum, CellStyle style) {
		// Row 1
		row = sheet.createRow(rownum);

		// STT column
		cell = row.createCell(1, CellType.STRING);
		cell.setCellValue("STT");
		cell.setCellStyle(style);

		// Thời gian đăng xuất gần nhất column
		cell = row.createCell(2, CellType.STRING);
		cell.setCellValue("Nội dung");
		cell.setCellStyle(style);

		// SL TK 1 column
		cell = row.createCell(3, CellType.STRING);
		cell.setCellValue("Tên sheet");
		cell.setCellStyle(style);

	}

	private CellStyle createCellStyleHeader(Sheet sheet, Workbook workbook, boolean isSetBold) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.WHITE.index);
		font.setFontHeightInPoints((short) 14);
		font.setBold(true);

		// set border
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		headerCellStyle.setFillBackgroundColor(IndexedColors.GREEN.getIndex());

		// set font
		headerCellStyle.setFont(font);
		headerCellStyle.setAlignment(HorizontalAlignment.LEFT);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);
		headerCellStyle.setFillForegroundColor(IndexedColors.GREEN.index);
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		headerCellStyle.setFont(font);

		return headerCellStyle;
	}

	private CellStyle createCellStyleTieuDeChinh(Sheet sheet, Workbook workbook, boolean isSetBold) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.BLACK.index);
		font.setFontHeightInPoints((short) 16);
		font.setBold(true);

		// set border
		headerCellStyle.setFont(font);

		headerCellStyle.setFillForegroundColor(IndexedColors.WHITE.index);
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		// set font

		headerCellStyle.setAlignment(HorizontalAlignment.CENTER);

		return headerCellStyle;
	}

	private CellStyle createCellStyleTieuDePhu(Sheet sheet, Workbook workbook, String alignment) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.BLACK.index);
		font.setFontHeightInPoints((short) 14);
		font.setBold(false);
		font.setItalic(true);
		// set border
		headerCellStyle.setFont(font);

		headerCellStyle.setFillForegroundColor(IndexedColors.WHITE.index);
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		// set font

		switch (alignment) {
		case "right":
			headerCellStyle.setAlignment(HorizontalAlignment.RIGHT);
			break;

		case "left":
			headerCellStyle.setAlignment(HorizontalAlignment.LEFT);
			break;
		default:
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
			break;
		}

		return headerCellStyle;
	}

	private CellStyle createCellStyleTieuDePhu(Sheet sheet, Workbook workbook) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();
		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setColor(IndexedColors.BLACK.index);
		font.setFontHeightInPoints((short) 14);
		font.setBold(false);
		font.setItalic(true);
		// set border
		headerCellStyle.setFont(font);
		headerCellStyle.setFillForegroundColor(IndexedColors.WHITE.index);
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

		// set font
		headerCellStyle.setAlignment(HorizontalAlignment.RIGHT);

		return headerCellStyle;
	}

	private CellStyle createCellStyleData(Sheet sheet, Workbook workbook, boolean isSetBold) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		// font.setBold(isSetBold);
		font.setFontHeightInPoints((short) 13);
		font.setBold(false);

		// set border
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		headerCellStyle.setFillBackgroundColor(IndexedColors.GREEN.getIndex());
		headerCellStyle.setFillForegroundColor(IndexedColors.WHITE.index);

		// set font
		headerCellStyle.setFont(font);
		headerCellStyle.setAlignment(HorizontalAlignment.LEFT);
		headerCellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		return headerCellStyle;
	}

	private CellStyle cusTomStyle(Sheet sheet, Workbook workbook, Integer color, String canLe, Boolean bold,
			Boolean italic, Boolean upperCase, Boolean underLine) {
		CellStyle cellStyle = sheet.getWorkbook().createCellStyle();

		// set font
		final Font font = sheet.getWorkbook().createFont();
		font.setFontName("Times New Roman");
		font.setFontHeightInPoints((short) 13);
		font.setBold(bold);
		font.setItalic(italic);
		if (underLine) {
			font.setUnderline(Font.U_SINGLE);
		}

		// màu chứ
		font.setColor((short) 0);

		// set font
		cellStyle.setFont(font);

		// căn lề ngang
		if (canLe != null) {
			if (canLe.equals("CP")) {
				cellStyle.setAlignment(HorizontalAlignment.RIGHT);
			}
			if (canLe.equals("CG")) {
				cellStyle.setAlignment(HorizontalAlignment.CENTER);
			}

		} else {
			cellStyle.setAlignment(HorizontalAlignment.LEFT);
		}

		// căn lề dọc
		cellStyle.setVerticalAlignment(VerticalAlignment.TOP);

		// để màu ô
		if (color != null) {
			int col = color;
			cellStyle.setFillForegroundColor((short) col);
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		}

		// set border
		cellStyle.setBorderBottom(BorderStyle.THIN);
		cellStyle.setBorderTop(BorderStyle.THIN);
		cellStyle.setBorderRight(BorderStyle.THIN);
		cellStyle.setBorderLeft(BorderStyle.THIN);
		cellStyle.setFillForegroundColor(IndexedColors.WHITE.index);
//		cellStyle.setFillBackgroundColor(IndexedColors.GREEN.getIndex());

		return cellStyle;
	}

	private void createTableHeader(BmSheetDTO sheetItem, int tongHangTieuDe, int colNumber, Sheet sheet, Cell cell,
			Row row, Workbook workbook, String tieuDeChinh, String tieuDePhu) {

		List<BmSheetCotDTO> cots = sortCotByStt(sheetItem);
		boolean isBasicTable = checkBasicTable(sheetItem, tongHangTieuDe);

		if (isBasicTable) {
			createBasicTableHeader(sheet, cell, row, workbook, cots, tieuDeChinh, tieuDePhu);
		} else {
			createMergeTableHeader(sheet, cell, row, workbook, sheetItem, tongHangTieuDe, colNumber, tieuDeChinh,
					tieuDePhu);
		}

	}

	private void createBasicTableHeader(Sheet sheet, Cell cell, Row row, Workbook workbook, List<BmSheetCotDTO> cots,
			String tieuDeChinh, String tieuDePhu) {

		CellStyle styleTieuDeChinh = this.createCellStyleTieuDeChinh(sheet, workbook, true);
		CellStyle styleTieuDePhu = this.createCellStyleTieuDePhu(sheet, workbook, "center");
		CellStyle styleTieuDePhuRight = this.createCellStyleTieuDePhu(sheet, workbook, "right");
		CellStyle styleHeader = this.createCellStyleHeader(sheet, workbook, true);
		List<String> listTieuDePhu = null;
		if (tieuDePhu != null) {
			listTieuDePhu = Stream.of(tieuDePhu.split("\\|")).collect(Collectors.toList());
		} else {
			listTieuDePhu = new ArrayList<String>();
		}

		int tongTieuDePhu = listTieuDePhu.size();
		int tongSoHangHeader = 2 + tongTieuDePhu;

		int j = 0;
		for (int i = 0; i < tongSoHangHeader; i++) {
			row = sheet.createRow(i);
			j = 0;
			for (BmSheetCotDTO cot : cots) {
				cell = row.createCell(j, CellType.STRING);
				// cell.setCellValue("Header"+i+"-"+j);

				if (i == tongSoHangHeader - 1) {
					cell.setCellStyle(styleHeader);
					cell.setCellValue(cot.getTenCot());
				} else {
					cell.setCellStyle(styleTieuDePhu);
					cell.setCellValue("");
				}

				j++;
			}

		}

		Row rowTieuDeChinh = sheet.getRow(0);

		for (Cell cell2 : rowTieuDeChinh) {
			cell2.setCellStyle(styleTieuDeChinh);
		}
		Short lastCell = rowTieuDeChinh.getLastCellNum();

		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, lastCell.intValue() - 1));

		cell = sheet.getRow(0).getCell(0);
		cell.setCellValue(tieuDeChinh);
		cell.setCellStyle(styleTieuDeChinh);

		for (int i = 0; i < tongTieuDePhu; i++) {
			sheet.addMergedRegion(new CellRangeAddress(i + 1, i + 1, 0, lastCell.intValue() - 1));
			Row rowTieuDePhu = sheet.getRow(i + 1);
			cell = rowTieuDePhu.getCell(0);
			cell.setCellValue(listTieuDePhu.get(i));

			if (i == 2) {
				cell.setCellStyle(styleTieuDePhuRight);
			} else {
				cell.setCellStyle(styleTieuDePhu);
			}

		}

	}

	private void createTableData(Workbook workbook, Sheet sheet, Cell cell, Row row, CellStyle cellStyle,
			int tongHangTieuDe, int tongSoHang, int tongSoCot, BmSheetDTO sheetItem, String tieuDePhu) {
		List<String> listTieuDePhu = null;
		if (tieuDePhu != null) {
			listTieuDePhu = Stream.of(tieuDePhu.split("\\|")).collect(Collectors.toList());
		} else {
			listTieuDePhu = new ArrayList<String>();
		}
		int tongTieuDePhu = listTieuDePhu.size();

		tongHangTieuDe = tongHangTieuDe + tongTieuDePhu + 1;

		int i = tongHangTieuDe;
		int hangBatDau = tongHangTieuDe;
		int hangKetThuc = tongSoHang + tongHangTieuDe;

		for (i = hangBatDau; i < hangKetThuc; i++) {
			row = sheet.createRow(i);
			for (int j = 0; j < tongSoCot; j++) {
				cell = row.createCell(j, CellType.STRING);
				// cell.setCellValue(""+i+"-"+j);
				cell.setCellValue("");
				cell.setCellStyle(cellStyle);
			}
		}

		Collection<BmSheetCtDTO> chiTieus = sheetItem.getBmSheetCtCollection();

		Integer hang = null;
		Integer cot = null;
		if (chiTieus != null) {

			for (BmSheetCtDTO chiTieu : chiTieus) {

				chiTieu = getIndexChiTieu(sheetItem, chiTieu, hangBatDau);
				hang = chiTieu.getIndexHang();
				cot = chiTieu.getIndexCot();
				if (hang != null) {
					Row r = sheet.getRow(hang);

//						[#] start setup cell

					try {

						if (chiTieu.getIndexCot() != null) {
							Cell c = r.getCell(cot);

							String dinhDangCell = chiTieu.getDinhDang();
							if (dinhDangCell == null) {
								dinhDangCell = "";
							}

							switch (dinhDangCell) {
							case "LB":// label

								if (chiTieu.getInHoa()) {
									c.setCellValue(chiTieu.getLabel().toUpperCase());
								} else {
									c.setCellValue(chiTieu.getLabel());
								}
								break;
							case "CT":// công thức
								c.setCellValue("formula");
								break;

							case "KT":// ký tự
								c.setCellValue(
										(chiTieu.getGiaTriDefault() == null) ? "" : chiTieu.getGiaTriDefault() + "");

								if (chiTieu.getInHoa()) {
									c.setCellValue((chiTieu.getGiaTriDefault() == null) ? ""
											: chiTieu.getGiaTriDefault() + "".toUpperCase());
								} else {
									c.setCellValue(chiTieu.getLabel());
								}
								break;

							case "TT":// tiền tệ
								c.setCellValue(
										(chiTieu.getGiaTriDefault() == null) ? "" : chiTieu.getGiaTriDefault() + "");
								break;

							case "S":// số
								c.setCellValue(
										(chiTieu.getGiaTriDefault() == null) ? "" : chiTieu.getGiaTriDefault() + "");
								break;
							default:
								c.setCellValue("");
								break;
							}

							// style
							c.setCellStyle(cusTomStyle(sheet, workbook, chiTieu.getMauSac(), chiTieu.getCanLe(),
									chiTieu.getInDam(), chiTieu.getInNghieng(), chiTieu.getInHoa(),
									chiTieu.getGachChan()));

							if (cot.intValue() == 0 && chiTieu.getDongHang().intValue() == 1) {
								c = setCellComment(c, "Được phép chèn thêm hàng sau hàng này");
							}

						}
						// insert comment hàng động

					} catch (Exception e) {
						// System.out.println(chiTieu.toString());
					}
//						 [#] end setup cell
				}

			}

			sheet = workbook.createSheet(sheetItem.getId() + "_hidden");
			// Tao mat khau bao ve
			sheet.protectSheet("scms");
//			sheet.setDisplayFormulas(true);
			// Tao sheet an chua cell config
			createTableHidden(sheetItem, tongHangTieuDe, tongSoHang, tongSoCot, sheet, sheetItem.getMaSheet(),
					workbook);

			workbook.setSheetHidden(workbook.getSheetIndex(sheetItem.getId() + "_hidden"), true);

			// insert row dynamic
			// get sheet name
			String sheetName = getSheetNameExcel(sheetItem);
			sheet = workbook.getSheet(sheetName);

			int maxSizeRowNumber = sheet.getLastRowNum() * 2;
			for (int j = 0; j <= sheet.getLastRowNum(); j++) {
				row = sheet.getRow(j) != null ? sheet.getRow(j) : sheet.createRow(j);
				cell = row.getCell(0) != null ? row.getCell(0) : row.createCell(0);
				Comment comment = cell.getCellComment();
				if (comment != null) {
					if ("Được phép chèn thêm hàng sau hàng này".equals(comment.getString().toString())) {

						sheet.shiftRows(j + 1, maxSizeRowNumber, 1);
						row = sheet.createRow(j + 1);

						for (int k = 0; k < tongSoCot; k++) {
							cell = row.createCell(k);
							if (k == 0) {
								cell.setCellValue("...");
							} else {
								cell.setCellValue("");
							}
							cell.setCellStyle(cellStyle);
						}
					}
				}

			}
		}
	}

	public Cell setCellComment(Cell cell, String message) {
		CreationHelper factory = cell.getSheet().getWorkbook().getCreationHelper();
		ClientAnchor anchor = factory.createClientAnchor();
		anchor.setCol1(cell.getColumnIndex());
		anchor.setCol2(cell.getColumnIndex() + 3);
		anchor.setRow1(cell.getRowIndex());
		anchor.setRow2(cell.getRowIndex() + 3);
		Comment comment = cell.getSheet().createDrawingPatriarch().createCellComment(anchor);
		comment.setString(factory.createRichTextString(message));
		comment.setAuthor("OpenL");
		// Assign the comment to the cell
		cell.setCellComment(comment);

		return cell;
	}

	private void createTableDataBaoCaoDinhky(Workbook workbook, Sheet sheet, Cell cell, Row row, CellStyle cellStyle,
			int tongHangTieuDe, int tongSoHang, int tongSoCot, BmSheetDTO sheetItem, String tieuDePhu,
			Integer bmBaoCaoId, Integer thanhVienId) {

		Sheet sheetHiden = null;

		// get gia tri chi tieu cua bao cao

		List<BcBaoCaoGtDTO> listGiaTriChiTieu = bcBaoCaoGtEntityDao.getGiaTriByBmBaoCaoIdThanhVienId(bmBaoCaoId,
				thanhVienId);

		Map<Integer, String> mapGiaTriChiTieu = new HashMap<Integer, String>();

		for (BcBaoCaoGtDTO bcBaoCaoGtDTO : listGiaTriChiTieu) {
			mapGiaTriChiTieu.put(bcBaoCaoGtDTO.getBmSheetCtId(), bcBaoCaoGtDTO.getGiaTri());

		}

		List<String> listTieuDePhu = null;
		if (tieuDePhu != null) {
			listTieuDePhu = Stream.of(tieuDePhu.split("\\|")).collect(Collectors.toList());
		} else {
			listTieuDePhu = new ArrayList<String>();
		}
		int tongTieuDePhu = listTieuDePhu.size();

		tongHangTieuDe = tongHangTieuDe + tongTieuDePhu + 1;

		int i = tongHangTieuDe;
		int hangBatDau = tongHangTieuDe;
		int hangKetThuc = tongSoHang + tongHangTieuDe;

		for (i = hangBatDau; i < hangKetThuc; i++) {
			row = sheet.createRow(i);
			for (int j = 0; j < tongSoCot; j++) {
				cell = row.createCell(j, CellType.STRING);
				// cell.setCellValue(""+i+"-"+j);
				cell.setCellValue("");
				cell.setCellStyle(cellStyle);
			}
		}

		Collection<BmSheetCtDTO> chiTieus = sheetItem.getBmSheetCtCollection();

		Integer hang = null;
		Integer cot = null;
		for (BmSheetCtDTO chiTieu : chiTieus) {

			chiTieu = getIndexChiTieu(sheetItem, chiTieu, hangBatDau);
			hang = chiTieu.getIndexHang();
			cot = chiTieu.getIndexCot();

			if (hang != null) {
				Row r = sheet.getRow(hang);

//    						[#] start setup cell

				try {

					if (chiTieu.getIndexCot() != null) {
						Cell c = r.getCell(cot);

						String dinhDangCell = chiTieu.getDinhDang();
						if (dinhDangCell == null) {
							dinhDangCell = "";
						}

						switch (dinhDangCell) {
						case "LB":// label

							if (chiTieu.getInHoa()) {
								c.setCellValue(chiTieu.getLabel().toUpperCase());
							} else {
								c.setCellValue(chiTieu.getLabel());
							}
							break;
						case "CT":// công thức
							c.setCellValue("formula");
							break;

						case "KT":// ký tự
							c.setCellValue((chiTieu.getGiaTriDefault() == null) ? "" : chiTieu.getGiaTriDefault() + "");

							if (chiTieu.getInHoa()) {
								c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
										: mapGiaTriChiTieu.get(chiTieu.getId()) + "".toUpperCase());
							} else {
								c.setCellValue(mapGiaTriChiTieu.get(chiTieu.getId()));
							}
							break;

						case "TT":// tiền tệ
							c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
									: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
							break;

						case "S":// số
							c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
									: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
							break;

						case "TLPT":// Tỷ lệ phần trăm
							c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
									: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
							break;

						default:
							c.setCellValue("");
							break;
						}

						// style
						c.setCellStyle(cusTomStyle(sheet, workbook, chiTieu.getMauSac(), chiTieu.getCanLe(),
								chiTieu.getInDam(), chiTieu.getInNghieng(), chiTieu.getInHoa(), chiTieu.getGachChan()));

					}

				} catch (Exception e) {
					// System.out.println(chiTieu.toString());
				}
//    						 [#] end setup cell
			}

		}

		sheetHiden = workbook.createSheet(sheetItem.getId() + "_hidden");

		sheetHiden.protectSheet("scms");
//    			sheet.setDisplayFormulas(true);
		createTableHidden(sheetItem, tongHangTieuDe, tongSoHang, tongSoCot, sheetHiden, sheetItem.getMaSheet(),
				workbook);

// 		get data dynamic
		BcBaoCaoGtDongDTO dataChiTieuDongDong = bcBaoCaoGtDongDao.getGiaTriChiTieuDong(thanhVienId, sheetItem.getId());

		// convert - json to object
		Gson gson = new Gson();
		List<BcBaoCaoGtDongJsonDTO> lstBcBaoCaoGtDongFromJsonDTO = new ArrayList<BcBaoCaoGtDongJsonDTO>();
		lstBcBaoCaoGtDongFromJsonDTO = gson.fromJson(dataChiTieuDongDong.getGiaTri(),
				new TypeToken<List<BcBaoCaoGtDongJsonDTO>>() {
				}.getType());

		Map<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByChiTieuHangId = null;
		if (lstBcBaoCaoGtDongFromJsonDTO != null && lstBcBaoCaoGtDongFromJsonDTO.size() > 0) {
			lstBcBaoCaoGtDongFromJsonDTO = lstBcBaoCaoGtDongFromJsonDTO.stream().distinct()
					.collect(Collectors.toList());
			// group by hang
			groupByChiTieuHangId = lstBcBaoCaoGtDongFromJsonDTO.stream()
					.collect(Collectors.groupingBy(BcBaoCaoGtDongJsonDTO::getBmSheetHang));
			// put to map
			// Map<Integer, BmSheetHangDTO> mapHangs = getMapHangs(sheetItem);
			List<BmSheetHangDTO> HangDuyet = getHangDong(sheetItem);
			if (HangDuyet != null && HangDuyet.size() > 0) {
				List<BmSheetHangDTO> lstHangDong = HangDuyet.stream()
						.filter(x -> x.getHangDong() != null && x.getHangDong().booleanValue())
						.collect(Collectors.toList());
				if (lstHangDong != null && lstHangDong.size() > 0) {
					int rowInserted = 0;
					for (BmSheetHangDTO bmSheetHangDTO : lstHangDong) {
						if (groupByChiTieuHangId.containsKey(bmSheetHangDTO.getId())) {

							Map<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByChiTieuHangDongIndex = groupByChiTieuHangId
									.get(bmSheetHangDTO.getId()).stream()
									.collect(Collectors.groupingBy(BcBaoCaoGtDongJsonDTO::getIndexCellHang));
							int rowIndex = bmSheetHangDTO.getIndex() + 1 + hangBatDau + rowInserted;

							// fix xuat excel (2020-08-25)
							int lastRow = sheet.getLastRowNum();
							if (lastRow <= rowIndex) {
								lastRow = rowIndex + groupByChiTieuHangDongIndex.size();
							}

							sheet.shiftRows(rowIndex, lastRow, groupByChiTieuHangDongIndex.size());
							int indexHangDong = 0;
							for (Map.Entry<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByHangDongEntry : groupByChiTieuHangDongIndex
									.entrySet()) {
								row = sheet.createRow(rowIndex + indexHangDong);
								if (row != null) {
									List<BcBaoCaoGtDongJsonDTO> lstGiaTriDong = groupByHangDongEntry.getValue();
									for (int k = 0; k < tongSoCot; k++) {
										int cotIndex = k;
										cell = row.createCell(cotIndex, CellType.STRING);
										Optional<BcBaoCaoGtDongJsonDTO> giaTri = lstGiaTriDong.stream()
												.filter(x -> x.getIndexCellCot() != null
														&& x.getIndexCellCot().intValue() == cotIndex)
												.findFirst();
										if (giaTri.isPresent()) {
											cell.setCellValue(giaTri.get().getGiaTri());
										} else {
											cell.setCellValue("");
										}
										cell.setCellStyle(cellStyle);
									}
									indexHangDong++;
									rowInserted++;
								}
							}
//							for(int indexHangDong = 0; indexHangDong < groupByChiTieuHangDongIndex.size(); indexHangDong++) {
//								row = sheet.createRow(bmSheetHangDTO.getIndex() + 1 + hangBatDau + indexHangDong);
//								List<BcBaoCaoGtDongJsonDTO> hangDong = groupByChiTieuHangDongIndex.entrySet().;
//								for (int k = 0; k < tongSoCot; k++) {
//									if(row != null) {
//										cell = row.createCell(1, CellType.STRING);
//										cell.setCellValue("test - " + indexHangDong);
//										cell.setCellStyle(cellStyle);
//									}
//								}
//							}
						}
					}
				}
			}

			// Old
//			sheet.shiftRows(temp + 1, temp + HangDuyet.size(), 2);
//			row = sheet.createRow(temp + 1);
//			if(row != null) {
//				cell = row.createCell(1, CellType.STRING);
//				cell.setCellValue("test");
//			}
//			row = sheet.createRow(temp + 2);
//			if(row != null) {
//				cell = row.createCell(2, CellType.STRING);
//				cell.setCellValue("test");
//			}
//			
//			
//			for (BmSheetHangDTO bmSheetHangDTO : HangDuyet) {
////				if (groupByChiTieuHangId.get(bmSheetHangDTO.getId()) != null) {
////
////					Map<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByChiTieuHangDongIndex = groupByChiTieuHangId
////							.get(bmSheetHangDTO.getId()).stream()
////							.collect(Collectors.groupingBy(BcBaoCaoGtDongJsonDTO::getIndexCellHang));
//////					sheet.shiftRows(bmSheetHangDTO.getIndex() + 1 + temp, bmSheetHangDTO.getIndex() + 1 + temp,
//////							groupByChiTieuHangDongIndex.size());
////
////					
//////					Queue<BcBaoCaoGtDongJsonDTO> queue = new LinkedList<>(
//////							groupByChiTieuHangId.get(bmSheetHangDTO.getId()));
////					for (int j = 0; j < groupByChiTieuHangDongIndex.size(); j++) {
//////						row = sheet.createRow(bmSheetHangDTO.getIndex() + 1 + temp + j);
//////
//////						for (int k = 0; k < tongSoCot; k++) {
//////							cell = row.createCell(k, CellType.STRING);
//////							// get data in queue
//////							BcBaoCaoGtDongJsonDTO dataDynamic = queue.peek();
//////							if (dataDynamic != null) {
//////								cell.setCellValue(dataDynamic.getGiaTri());
//////							} else {
//////								cell.setCellValue("");
//////							}
//////
//////							// delete 1 item queue
//////							queue.poll();
//////							cell.setCellStyle(cellStyle);
//////						}
////
////					}
////					temp = temp + groupByChiTieuHangDongIndex.size();
////				}
//			}
		}

		workbook.setSheetHidden(workbook.getSheetIndex(sheetItem.getId() + "_hidden"), true);

	}

	private void createTableDataBaoCaoKhaiThac(Workbook workbook, Sheet sheet, Cell cell, Row row, CellStyle cellStyle,
			int tongHangTieuDe, int tongSoHang, int tongSoCot, BmSheetDTO sheetItem, String tieuDePhu,
			Integer bmBaoCaoId, BcKhaiThacGtBDTO lstBcKhaiThacGt, Integer hangInSheet, Integer hangInBcKt) {

		Sheet sheetHiden = null;

		// get gia tri chi tieu cua bao cao

//		List<BcBaoCaoGtDTO> listGiaTriChiTieu = bcBaoCaoGtEntityDao.getGiaTriByBmBaoCaoIdThanhVienId(bmBaoCaoId,
//				khaiThacId);
		Map<Integer, String> mapGiaTriChiTieu = new HashMap<Integer, String>();

		for (BcKhaiThacGtDTO bcKhaiThacGtDto : lstBcKhaiThacGt.getLstBc()) {
			mapGiaTriChiTieu.put(bcKhaiThacGtDto.getBmSheetCtId(), bcKhaiThacGtDto.getGiaTri());

		}

		List<String> listTieuDePhu = null;
		if (tieuDePhu != null) {
			listTieuDePhu = Stream.of(tieuDePhu.split("\\|")).collect(Collectors.toList());
		} else {
			listTieuDePhu = new ArrayList<String>();
		}
		int tongTieuDePhu = listTieuDePhu.size();

		tongHangTieuDe = tongHangTieuDe + tongTieuDePhu + 1;

		int i = tongHangTieuDe;
		int hangBatDau = tongHangTieuDe;
		int hangKetThuc = tongSoHang + tongHangTieuDe;

		if (hangInBcKt != null && hangInSheet >= hangInBcKt) {
			for (i = hangBatDau; i < hangKetThuc; i++) {
				row = sheet.createRow(i);
				for (int j = 0; j < tongSoCot; j++) {
					cell = row.createCell(j, CellType.STRING);
					// cell.setCellValue(""+i+"-"+j);
					cell.setCellValue("");
					cell.setCellStyle(cellStyle);
				}
			}

			Collection<BmSheetCtDTO> chiTieus = sheetItem.getBmSheetCtCollection();

			Integer hang = null;
			Integer cot = null;
			for (int j = 0; j < lstBcKhaiThacGt.getLstBc().size(); j++) {
				for (BmSheetCtDTO chiTieu : chiTieus) {
					Integer chiTieuId = chiTieu.getId();
					BcKhaiThacGtDTO dto = lstBcKhaiThacGt.getLstBc().stream()
							.filter(x -> chiTieuId.equals(x.getBmSheetCtId())).findAny().orElse(null);
					chiTieu = getIndexChiTieu(sheetItem, chiTieu, hangBatDau);
					hang = chiTieu.getIndexHang();
					cot = chiTieu.getIndexCot();

					if (hang != null) {
						Row r = sheet.getRow(hang);

//		    						[#] start setup cell

						try {

							if (chiTieu.getIndexCot() != null) {
								Cell c = r.getCell(cot);

								String dinhDangCell = chiTieu.getDinhDang();
								if (dinhDangCell == null) {
									dinhDangCell = "";
								}

								switch (dinhDangCell) {
								case "LB":// label

									if (chiTieu.getInHoa()) {
										c.setCellValue(chiTieu.getLabel().toUpperCase());
									} else {
										c.setCellValue(chiTieu.getLabel());
									}
									break;
								case "CT":// công thức
									c.setCellValue("formula");
									break;

								case "KT":// ký tự
									c.setCellValue((chiTieu.getGiaTriDefault() == null) ? ""
											: chiTieu.getGiaTriDefault() + "");

									if (dto != null) {
										if (chiTieu.getInHoa()) {
											c.setCellValue(
													dto.getGiaTri() == null ? "" : dto.getGiaTri().toUpperCase());
										} else {
											c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
										}
									}
//									if (chiTieu.getInHoa()) {
//										c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//												: mapGiaTriChiTieu.get(chiTieu.getId()) + "".toUpperCase());
//									} else {
//										c.setCellValue(mapGiaTriChiTieu.get(chiTieu.getId()));
//									}
									break;

								case "TT":// tiền tệ
									c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
//									c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//											: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
									break;

								case "S":// số
									c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
//									c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//											: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
									break;

								case "TLPT":// Tỷ lệ phần trăm
									c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
//									c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//											: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
									break;

								default:
									c.setCellValue("");
									break;
								}

								// style
								c.setCellStyle(cusTomStyle(sheet, workbook, chiTieu.getMauSac(), chiTieu.getCanLe(),
										chiTieu.getInDam(), chiTieu.getInNghieng(), chiTieu.getInHoa(),
										chiTieu.getGachChan()));

							}

						} catch (Exception e) {
							// System.out.println(chiTieu.toString());
						}
//		    						 [#] end setup cell
					}
					if (dto != null) {
						lstBcKhaiThacGt.getLstBc().remove(dto);
					}
				}

			}
		}

		else {
			for (i = hangBatDau; i < (hangInBcKt + tongHangTieuDe); i++) {
				row = sheet.createRow(i);
				for (int j = 0; j < tongSoCot; j++) {
					cell = row.createCell(j, CellType.STRING);
					// cell.setCellValue(""+i+"-"+j);
					cell.setCellValue("");
					cell.setCellStyle(cellStyle);
				}
			}

			Collection<BmSheetCtDTO> chiTieus = sheetItem.getBmSheetCtCollection();

			Integer hang = null;
			Integer cot = null;
			for (int j = hangBatDau; j < (hangInBcKt + tongHangTieuDe); j++) {
				for (BmSheetCtDTO chiTieu : chiTieus) {
					Integer chiTieuId = chiTieu.getId();
					BcKhaiThacGtDTO dto = lstBcKhaiThacGt.getLstBc().stream()
							.filter(x -> chiTieuId.equals(x.getBmSheetCtId())).findAny().orElse(null);
					chiTieu = getIndexChiTieu(sheetItem, chiTieu, hangBatDau);
					hang = j;
					cot = chiTieu.getIndexCot();

					if (hang != null) {
						Row r = sheet.getRow(hang);

//		    						[#] start setup cell

						try {

							if (chiTieu.getIndexCot() != null) {
								Cell c = r.getCell(cot);

								String dinhDangCell = chiTieu.getDinhDang();
								if (dinhDangCell == null) {
									dinhDangCell = "";
								}

								switch (dinhDangCell) {
								case "LB":// label

									if (chiTieu.getInHoa()) {
										c.setCellValue(chiTieu.getLabel().toUpperCase());
									} else {
										c.setCellValue(chiTieu.getLabel());
									}
									break;
								case "CT":// công thức
									c.setCellValue("formula");
									break;

								case "KT":// ký tự
									c.setCellValue((chiTieu.getGiaTriDefault() == null) ? ""
											: chiTieu.getGiaTriDefault() + "");

									if (dto != null) {
										if (chiTieu.getInHoa()) {
											c.setCellValue(
													dto.getGiaTri() == null ? "" : dto.getGiaTri().toUpperCase());
										} else {
											c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
										}
									}
//									if (chiTieu.getInHoa()) {
//										c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//												: mapGiaTriChiTieu.get(chiTieu.getId()) + "".toUpperCase());
//									} else {
//										c.setCellValue(mapGiaTriChiTieu.get(chiTieu.getId()));
//									}
									break;

								case "TT":// tiền tệ
									c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
//									c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//											: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
									break;

								case "S":// số
									c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
//									c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//											: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
									break;

								case "TLPT":// Tỷ lệ phần trăm
									c.setCellValue(dto.getGiaTri() == null ? "" : dto.getGiaTri());
//									c.setCellValue((mapGiaTriChiTieu.get(chiTieu.getId()) == null) ? ""
//											: mapGiaTriChiTieu.get(chiTieu.getId()) + "");
									break;

								default:
									c.setCellValue("");
									break;
								}

								// style
								c.setCellStyle(cusTomStyle(sheet, workbook, chiTieu.getMauSac(), chiTieu.getCanLe(),
										chiTieu.getInDam(), chiTieu.getInNghieng(), chiTieu.getInHoa(),
										chiTieu.getGachChan()));

							}

						} catch (Exception e) {
							// System.out.println(chiTieu.toString());
						}
//		    						 [#] end setup cell
					}
					if (dto != null) {
						lstBcKhaiThacGt.getLstBc().remove(dto);
					}
				}

			}

		}

		sheetHiden = workbook.createSheet(sheetItem.getId() + "_hidden");

		sheetHiden.protectSheet("scms");
//    			sheet.setDisplayFormulas(true);
		createTableHidden(sheetItem, tongHangTieuDe, tongSoHang, tongSoCot, sheetHiden, sheetItem.getMaSheet(),
				workbook);

// 		get data dynamic
//		BcBaoCaoGtDongDTO dataChiTieuDongDong = bcBaoCaoGtDongDao.getGiaTriChiTieuDong(lstBcKhaiThacGt.getLstBc().get(0).getBcKhaiThacId(), sheetItem.getId());
//
//		// convert - json to object
//		Gson gson = new Gson();
//		List<BcBaoCaoGtDongJsonDTO> lstBcBaoCaoGtDongFromJsonDTO = new ArrayList<BcBaoCaoGtDongJsonDTO>();
//		lstBcBaoCaoGtDongFromJsonDTO = gson.fromJson(dataChiTieuDongDong.getGiaTri(),
//				new TypeToken<List<BcBaoCaoGtDongJsonDTO>>() {
//				}.getType());
//
//		Map<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByChiTieuHangId = null;
//		if (lstBcBaoCaoGtDongFromJsonDTO != null) {
//			lstBcBaoCaoGtDongFromJsonDTO = lstBcBaoCaoGtDongFromJsonDTO.stream().distinct()
//					.collect(Collectors.toList());
//			// group by hang
//			groupByChiTieuHangId = lstBcBaoCaoGtDongFromJsonDTO.stream()
//					.collect(Collectors.groupingBy(BcBaoCaoGtDongJsonDTO::getBmSheetHang));
//			// put to map
//			// Map<Integer, BmSheetHangDTO> mapHangs = getMapHangs(sheetItem);
//			List<BmSheetHangDTO> HangDuyet = getHangDong(sheetItem);
//			if (HangDuyet != null && HangDuyet.size() > 0) {
//				List<BmSheetHangDTO> lstHangDong = HangDuyet.stream()
//						.filter(x -> x.getHangDong() != null && x.getHangDong().booleanValue())
//						.collect(Collectors.toList());
//				if (lstHangDong != null && lstHangDong.size() > 0) {
//					int rowInserted = 0;
//					for (BmSheetHangDTO bmSheetHangDTO : lstHangDong) {
//						Map<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByChiTieuHangDongIndex = groupByChiTieuHangId
//								.get(bmSheetHangDTO.getId()).stream()
//								.collect(Collectors.groupingBy(BcBaoCaoGtDongJsonDTO::getIndexCellHang));
//						int rowIndex = bmSheetHangDTO.getIndex() + 1 + hangBatDau + rowInserted;
//
//						// fix xuat excel (2020-08-25)
//						int lastRow = sheet.getLastRowNum();
//						if (lastRow <= rowIndex) {
//							lastRow = rowIndex + groupByChiTieuHangDongIndex.size();
//						}
//
//						sheet.shiftRows(rowIndex, lastRow, groupByChiTieuHangDongIndex.size());
//						int indexHangDong = 0;
//						for (Map.Entry<Integer, List<BcBaoCaoGtDongJsonDTO>> groupByHangDongEntry : groupByChiTieuHangDongIndex
//								.entrySet()) {
//							row = sheet.createRow(rowIndex + indexHangDong);
//							if (row != null) {
//								List<BcBaoCaoGtDongJsonDTO> lstGiaTriDong = groupByHangDongEntry.getValue();
//								for (int k = 0; k < tongSoCot; k++) {
//									int cotIndex = k;
//									cell = row.createCell(cotIndex, CellType.STRING);
//									Optional<BcBaoCaoGtDongJsonDTO> giaTri = lstGiaTriDong.stream()
//											.filter(x -> x.getIndexCellCot() != null
//													&& x.getIndexCellCot().intValue() == cotIndex)
//											.findFirst();
//									if (giaTri.isPresent()) {
//										cell.setCellValue(giaTri.get().getGiaTri());
//									} else {
//										cell.setCellValue("");
//									}
//									cell.setCellStyle(cellStyle);
//								}
//								indexHangDong++;
//								rowInserted++;
//							}
//						}
//					}
//				}
//			}
//		}

		workbook.setSheetHidden(workbook.getSheetIndex(sheetItem.getId() + "_hidden"), true);

	}

	private List<BmSheetHangDTO> getHangDong(BmSheetDTO sheetItem) {
		List<BmSheetHangDTO> hangs = (List<BmSheetHangDTO>) sheetItem.getBmSheetHangCollection();
//Map<Integer, BmSheetHangDTO> mapHangs = new HashMap<Integer, BmSheetHangDTO>();
//
		Collections.sort(hangs, new Comparator<BmSheetHangDTO>() {
			@Override
			public int compare(BmSheetHangDTO o1, BmSheetHangDTO o2) {
				return o1.getThuTu().compareTo(o2.getThuTu());
			}
		});

		int index = 0;
		for (BmSheetHangDTO hang : hangs) {
			hang.setIndex(index);

			index++;

		}
		return hangs;
	}

	private void setValueAndStyleCell(Workbook workbook, Sheet sheet, Row r, CellStyle cellStyle, BmSheetCtDTO chiTieu,
			Integer cot) {
//		CellStyle cellStyle = this.oStyleData(sheet, workbook, false);

		Cell c = r.getCell(cot);
		c.setCellValue(chiTieu.getGiaTriDefault());
		c.setCellStyle(cellStyle);

	}

	private BmSheetCtDTO getIndexChiTieu(BmSheetDTO sheetItem, BmSheetCtDTO chiTieu, int hangBatDau) {
		Map<Integer, BmSheetCotDTO> mapCots = getMapCots(sheetItem);
		Map<Integer, BmSheetHangDTO> mapHangs = getMapHangs(sheetItem);

		BmSheetHangDTO hang = mapHangs.get(chiTieu.getBmSheetHangId());
		BmSheetCotDTO cot = mapCots.get(chiTieu.getBmSheetCotId());

		if (hang != null) {
			chiTieu.setIndexHang(hang.getIndex() + hangBatDau);
		}

		if (cot != null) {
			chiTieu.setIndexCot(cot.getIndex());
		}

		if (hang.getHangDong()) {
			chiTieu.setDongHang(1);
		} else {
			chiTieu.setDongHang(0);
		}

		return chiTieu;

	}

	private List<BmSheetHangDTO> sortHangByStt(BmSheetDTO sheetItem) {
		List<BmSheetHangDTO> hangs = (List<BmSheetHangDTO>) sheetItem.getBmSheetHangCollection();

		Collections.sort(hangs, new Comparator<BmSheetHangDTO>() {
			@Override
			public int compare(BmSheetHangDTO o1, BmSheetHangDTO o2) {
				return o1.getThuTu().compareTo(o2.getThuTu());
			}
		});

		int index = 1;
		for (BmSheetHangDTO hang : hangs) {
			hang.setIndex(index);
			index++;

		}
		return hangs;
	}

	private Map<Integer, BmSheetHangDTO> getMapHangs(BmSheetDTO sheetItem) {
		List<BmSheetHangDTO> hangs = (List<BmSheetHangDTO>) sheetItem.getBmSheetHangCollection();
		Map<Integer, BmSheetHangDTO> mapHangs = new HashMap<Integer, BmSheetHangDTO>();

		Collections.sort(hangs, new Comparator<BmSheetHangDTO>() {
			@Override
			public int compare(BmSheetHangDTO o1, BmSheetHangDTO o2) {
				return o1.getThuTu().compareTo(o2.getThuTu());
			}
		});

		int index = 0;
		for (BmSheetHangDTO hang : hangs) {
			hang.setIndex(index);
			mapHangs.put(hang.getId(), hang);
			index++;

		}
		return mapHangs;
	}

	private List<BmSheetCotDTO> sortCotByStt(BmSheetDTO sheetItem) {
		List<BmSheetCotDTO> cots = (List<BmSheetCotDTO>) sheetItem.getBmSheetCotCollection();

		Collections.sort(cots, new Comparator<BmSheetCotDTO>() {
			@Override
			public int compare(BmSheetCotDTO o1, BmSheetCotDTO o2) {
				return o1.getThuTu().compareTo(o2.getThuTu());
			}
		});

		int index = 1;
		for (BmSheetCotDTO cot : cots) {
			cot.setIndex(index);
			index++;
		}

		return cots;
	}

	private Map<Integer, BmSheetCotDTO> getMapCots(BmSheetDTO sheetItem) {
		List<BmSheetCotDTO> cots = (List<BmSheetCotDTO>) sheetItem.getBmSheetCotCollection();
		Map<Integer, BmSheetCotDTO> mapCots = new HashMap<Integer, BmSheetCotDTO>();

		Collections.sort(cots, new Comparator<BmSheetCotDTO>() {
			@Override
			public int compare(BmSheetCotDTO o1, BmSheetCotDTO o2) {
				return o1.getThuTu().compareTo(o2.getThuTu());
			}
		});

		int index = 0;
		for (BmSheetCotDTO cot : cots) {
			cot.setIndex(index);
			mapCots.put(cot.getId(), cot);
			index++;
		}
		return mapCots;
	}

	private Map<Integer, BmSheetCotDTO> getMapCotsByIndex(BmSheetDTO sheetItem) {
		List<BmSheetCotDTO> cots = (List<BmSheetCotDTO>) sheetItem.getBmSheetCotCollection();

		// index
		Map<Integer, BmSheetCotDTO> mapCots = new HashMap<Integer, BmSheetCotDTO>();

		Collections.sort(cots, new Comparator<BmSheetCotDTO>() {
			@Override
			public int compare(BmSheetCotDTO o1, BmSheetCotDTO o2) {
				return o1.getThuTu().compareTo(o2.getThuTu());
			}
		});

		int index = 0;
		for (BmSheetCotDTO cot : cots) {
			cot.setIndex(index);
			mapCots.put(index, cot);
			index++;
		}
		return mapCots;
	}

	private Boolean checkBasicTable(BmSheetDTO sheetItem, int tongHangTieuDe) {
		List<BmTieuDeHangCotDTO> tieuDeHangCot = (List<BmTieuDeHangCotDTO>) sheetItem.getBmTieuDeHangCotCollection();
		boolean dk1 = tieuDeHangCot.size() == 0;
		boolean dk2 = tongHangTieuDe == 1;
		return dk1 && dk2;
	}

	private void createMergeTableHeader(Sheet sheet, Cell cell, Row row, Workbook workbook, BmSheetDTO sheetItem,
			int tongTieuDeTable, int colNumber, String tieuDeChinh, String tieuDePhu) {
		CellStyle styleHeader = this.createCellStyleHeader(sheet, workbook, true);
		CellStyle styleTieuDePhu = this.createCellStyleTieuDePhu(sheet, workbook, "center");
		List<String> listTieuDePhu = null;
		if (tieuDePhu != null) {
			listTieuDePhu = Stream.of(tieuDePhu.split("\\|")).collect(Collectors.toList());
		} else {
			listTieuDePhu = new ArrayList<String>();
		}

		int tongTieuDePhu = listTieuDePhu.size();

		int tongHangTieuDe = 1 + tongTieuDeTable + tongTieuDePhu;

		for (int i = 0; i < tongHangTieuDe; i++) {
			row = sheet.createRow(i);
			for (int j = 0; j < colNumber; j++) {
				cell = row.createCell(j, CellType.STRING);
				// cell.setCellValue("Header"+i+"-"+j);
				cell.setCellValue("");

				if (i <= tongTieuDePhu) {
					cell.setCellStyle(styleTieuDePhu);
				} else {
					cell.setCellStyle(styleHeader);
				}
			}

		}

		this.mergeHeader(sheet, cell, row, workbook, sheetItem, tieuDeChinh, listTieuDePhu, colNumber);

	}

	private void mergeHeader(Sheet sheet, Cell cell, Row row, Workbook workbook, BmSheetDTO sheetItem,
			String tieuDeChinh, List<String> listTieuDePhu, int colNumber) {
		int tongHangTieuDePhu = listTieuDePhu.size();

		CellStyle styleHeader = this.createCellStyleHeader(sheet, workbook, true);

		CellStyle styleTieuDeChinh = this.createCellStyleTieuDeChinh(sheet, workbook, true);

		CellStyle styleTieuDePhu = this.createCellStyleTieuDePhu(sheet, workbook, "center");
		CellStyle styleTieuDePhuRight = this.createCellStyleTieuDePhu(sheet, workbook, "right");

		Collection<BmTieuDeHangCotDTO> bmTieuDeHangCotCollection = sheetItem.getBmTieuDeHangCotCollection();
		Integer fisrtColum = null;
		Integer fisrtRow = null;
		Integer lastColum = null;
		Integer lastRow = null;

		for (BmTieuDeHangCotDTO bmTieuDeHangCotDTO : bmTieuDeHangCotCollection) {
			fisrtColum = bmTieuDeHangCotDTO.getFisrtColumn() == null ? null
					: bmTieuDeHangCotDTO.getFisrtColumn().intValue() - 1;
			fisrtRow = bmTieuDeHangCotDTO.getFisrtRow() == null ? null
					: bmTieuDeHangCotDTO.getFisrtRow().intValue() - 1;
			lastColum = bmTieuDeHangCotDTO.getLastColumn() == null ? null
					: bmTieuDeHangCotDTO.getLastColumn().intValue() - 1;
			lastRow = bmTieuDeHangCotDTO.getLastRow() == null ? null : bmTieuDeHangCotDTO.getLastRow().intValue() - 1;

			// Tổng số hàng tiêu đề= 1 tiêu đề chính+ n tiêu đề phụ + m số header table
			fisrtRow = fisrtRow + tongHangTieuDePhu + 1;
			lastRow = lastRow + tongHangTieuDePhu + 1;

			// merge
			if (fisrtRow != null && lastRow != null && fisrtColum != null && lastColum != null) {

				if (fisrtRow != lastRow || fisrtColum != lastColum) {

					// System.out.println("SheetName:>>" + sheetItem.getTenSheet() + "-- ma " +
					// sheetItem.getMaSheet());
					sheet.addMergedRegion(new CellRangeAddress(fisrtRow, lastRow, fisrtColum, lastColum));

				}

				if (fisrtRow != null) {
					Row r = sheet.getRow(fisrtRow);
					if (fisrtColum != null) {
						Cell c = r.getCell(fisrtColum);
						if (bmTieuDeHangCotDTO != null && bmTieuDeHangCotDTO.getTenCot() != null && c != null) {
							c.setCellValue(bmTieuDeHangCotDTO.getTenCot());
							c.setCellStyle(styleHeader);
						}
					}
				}

			} else if (fisrtRow != null) {
				Row r = sheet.getRow(fisrtRow);
				if (fisrtColum != null) {
					Cell c = r.getCell(fisrtColum);
					if (bmTieuDeHangCotDTO != null && bmTieuDeHangCotDTO.getTenCot() != null && c != null) {
						c.setCellValue(bmTieuDeHangCotDTO.getTenCot());
						c.setCellStyle(styleHeader);
					}
				}
			}
		}

		Row rowTieuDeChinh = sheet.getRow(0);

		for (Cell cell2 : rowTieuDeChinh) {
			cell2.setCellStyle(styleTieuDeChinh);
		}
		Short lastCell = rowTieuDeChinh.getLastCellNum();

		// Tiêu đề chính
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, lastCell.intValue() - 1));

		cell = sheet.getRow(0).getCell(0);
		cell.setCellValue(tieuDeChinh);
		cell.setCellStyle(styleTieuDeChinh);

		// tiêu đề phụ

		int rowStartTieuDePhu = 1;
		for (String tieuDePhu : listTieuDePhu) {
			sheet.addMergedRegion(new CellRangeAddress(rowStartTieuDePhu, rowStartTieuDePhu, 0, colNumber - 1));
			row = sheet.getRow(rowStartTieuDePhu);
			cell = row.getCell(0);
			cell.setCellValue(tieuDePhu);
			cell.setCellStyle(styleTieuDePhu);

			if (rowStartTieuDePhu == 3) {
				cell.setCellStyle(styleTieuDePhuRight);
			} else {
				cell.setCellStyle(styleTieuDePhu);
			}

			rowStartTieuDePhu++;
		}

	}

	public BmBaoCaoBDTO listBmBaoCaos(Integer nguoiDungId, Integer kieuBaoCao, String strfilter, String tenBaoCao,
			String maBaoCao, String ccPhapLy, Integer pageNo, Integer pageSize, String keySort, boolean desc) {
		BmBaoCaoBDTO lst = this.bmBaoCaoDao.getList(nguoiDungId, kieuBaoCao, strfilter, tenBaoCao, maBaoCao, ccPhapLy,
				pageNo, pageSize, keySort, desc);

		return lst;

	}

	public BmLichSuBaoCaoBDTO lichSuThayDoi(int pageNo, Integer pageSize, String keySort, int bmBaoCaoId)
			throws Exception {

		return this.bmBaoCaoDao.lichSuThayDoi(pageNo, pageSize, keySort, bmBaoCaoId);
	}

	private void createTableHidden(BmSheetDTO sheetItem, int tongHangTieuDe, int tongSoHang, int tongSoCot, Sheet sheet,
			String sheetRefer, Workbook workbook) {
		sheetRefer = getSheetNameExcel(sheetItem);

		Cell cell;
		Row row;

		int i = tongHangTieuDe;
		int hangBatDau = tongHangTieuDe;
		int hangKetThuc = tongSoHang + tongHangTieuDe;

		for (i = hangBatDau; i < hangKetThuc; i++) {
			row = sheet.createRow(i);
			for (int j = 0; j < tongSoCot; j++) {
				cell = row.createCell(j, CellType.STRING);
				cell.setCellValue("");
			}
		}

//

		Collection<BmSheetCtDTO> chiTieus = sheetItem.getBmSheetCtCollection();
		Integer hang = null;
		Integer cot = null;

		Integer dongHang = null;
		Integer dongCot = 0;
		Cell c = null;

		for (BmSheetCtDTO chiTieu : chiTieus) {

			// check động hàng
			dongHang = 0;

			dongHang = chiTieu.getDongHang();

			chiTieu = getIndexChiTieu(sheetItem, chiTieu, hangBatDau);

			hang = chiTieu.getIndexHang();
			cot = chiTieu.getIndexCot();

			if (hang != null) {
				Row r = sheet.getRow(hang);
//    						[#] start setup cell
				if (chiTieu.getIndexCot() != null) {
					c = r.getCell(cot);
					if (c != null) {

//					c.setCellFormula("CONCATENATE(" + chiTieu.getId() + ",\",\",ROW('" + sheetRefer + "'!"
//							+ c.getAddress() + "),\"|\",COLUMN('" + sheetRefer + "'!" + c.getAddress() + ")," + 1 + "," + 1 + ")");
						c.setCellFormula("CONCATENATE(" + chiTieu.getId() + ",\",\",ROW('" + sheetRefer + "'!"
								+ c.getAddress() + "),\"|\",COLUMN('" + sheetRefer + "'!" + c.getAddress() + "),\","
								+ dongHang + "\",\"," + dongCot + "\")");
					}
				}
			}

		}
		DataFormat fmt = workbook.createDataFormat();
		CellStyle textStyle = workbook.createCellStyle();
		textStyle.setDataFormat(fmt.getFormat("@"));
		sheet.setDefaultColumnStyle(0, textStyle);

	}

	/**
	 * Lay ds Bao cao (not Bao Cao dinh ky)
	 * 
	 * @param baoCaoType
	 * @return
	 * @throws Ex
	 */
	public List<DropDownDTO> getLsBaoCaoKhacDropDownLogic(String baoCaoType) throws Exception {

		return this.bmBaoCaoDao.getLsBaoCaoKhacDao(baoCaoType);
	}

	public BmBaoCaoBDTO getLsBaoCaoKhacDropDownLogicBDTO(String baoCaoType) throws Exception {
		BmBaoCaoBDTO result = new BmBaoCaoBDTO();
		result = this.bmBaoCaoDao.getLsBaoCaoKhac(baoCaoType);
		if (result != null && result.getLstBmBaoCao() != null && result.getLstBmBaoCao().size() > 0) {
			for (BmBaoCaoDTO temp : result.getLstBmBaoCao()) {
				if (temp.getFileDinhKem() != null)
					temp.setArrFileDinhKem(convertFileDinhKemToArr(temp.getFileDinhKem()));
			}
		}
		return result;
	}

	/**
	 * Get bieu mau bao cao for canh bao.
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<DropDownDTO> getBmBaoCaoForCanhBaoCtLogic() throws Exception {
		// TODO Auto-generated method stub
		return this.bmBaoCaoDao.getBmBaoCaoForCanhBaoCtDao();
	}

	public boolean checkExistMaBaoCao(String maBaoCao, Integer bmId, Integer bmBaoCaoId) throws Exception {
		return this.bmBaoCaoDao.checkExistMaBaoCao(maBaoCao, bmId, bmBaoCaoId);
	}

	public boolean checkExistLoaiBaoCaoLT(String loaiBaoCaoLT, Integer bmId, Integer bmBaoCaoId) throws Exception {
		return this.bmBaoCaoDao.checkExistLoaiBaoCaoLT(loaiBaoCaoLT, bmId, bmBaoCaoId);
	}

	private static void copyRow(Workbook workbook, Sheet worksheet, int sourceRowNum, int destinationRowNum) {
		// Get the source / new row
		Row newRow = worksheet.getRow(destinationRowNum);
		Row sourceRow = worksheet.getRow(sourceRowNum);

		// If the row exist in destination, push down all rows by 1 else create a new
		// row
		if (newRow != null) {
			worksheet.shiftRows(destinationRowNum, worksheet.getLastRowNum(), 1);
		} else {
			newRow = worksheet.createRow(destinationRowNum);
		}

		// Loop through source columns to add to new row
		for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
			// Grab a copy of the old/new cell
			Cell oldCell = sourceRow.getCell(i);
			Cell newCell = newRow.createCell(i);

			// If the old cell is null jump to next cell
			if (oldCell == null) {
				newCell = null;
				continue;
			}

			// Copy style from old cell and apply to new cell
			CellStyle newCellStyle = workbook.createCellStyle();
			newCellStyle.cloneStyleFrom(oldCell.getCellStyle());
			;
			newCell.setCellStyle(newCellStyle);

			// If there is a cell comment, copy
			if (oldCell.getCellComment() != null) {
				newCell.setCellComment(oldCell.getCellComment());
			}

			// If there is a cell hyperlink, copy
			if (oldCell.getHyperlink() != null) {
				newCell.setHyperlink(oldCell.getHyperlink());
			}

			// Set the cell data type
			newCell.setCellType(oldCell.getCellType());

			// Set the cell data value
			switch (oldCell.getCellType()) {
			case Cell.CELL_TYPE_BLANK:
				newCell.setCellValue(oldCell.getStringCellValue());
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				newCell.setCellValue(oldCell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_ERROR:
				newCell.setCellErrorValue(oldCell.getErrorCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				newCell.setCellFormula(oldCell.getCellFormula());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				newCell.setCellValue(oldCell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_STRING:
				newCell.setCellValue(oldCell.getRichStringCellValue());
				break;
			}
		}

		// If there are are any merged regions in the source row, copy to new row
		for (int i = 0; i < worksheet.getNumMergedRegions(); i++) {
			CellRangeAddress cellRangeAddress = worksheet.getMergedRegion(i);
			if (cellRangeAddress.getFirstRow() == sourceRow.getRowNum()) {
				CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.getRowNum(),
						(newRow.getRowNum() + (cellRangeAddress.getLastRow() - cellRangeAddress.getFirstRow())),
						cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn());
				worksheet.addMergedRegion(newCellRangeAddress);
			}
		}
	}

	private List<UploadFileDefaultDTO> convertFileDinhKemToArr(String fileDinhKem) {
		List<UploadFileDefaultDTO> arrFileDinhKem = new ArrayList<UploadFileDefaultDTO>();

		if (!fileDinhKem.equals("")) {

			String[] arrOfStr = fileDinhKem.split(",");
			for (String string : arrOfStr) {
				String[] itemSplit = string.replace("\\", "/").split("/");
				String fileName = itemSplit[itemSplit.length - 1];
				arrFileDinhKem.add(new UploadFileDefaultDTO(fileName, string));
			}

		}

		return arrFileDinhKem;
	}

	public Workbook generateFileTemplateExcelKhaiThac(BcKhaiThacGtBDTO lstBcKhaiThacGt, BmBaoCaoExportDTO dto,
			Workbook workbook, Boolean bieuMauDauRa, Map<Integer, Integer> mapSheet) throws Exception {
		List<BmSheetDTO> listBmSheet = dto.getListBmSheet();

		Collection<BmSheetCotDTO> cots = null;
		Collection<BmSheetHangDTO> hangs = null;
		Collection<BmTieuDeHangDTO> tieuDeHang = null;
		Collection<BmTieuDeHangCotDTO> tieuDeHangCot = null;

		createSheetSetting(workbook, dto);
		if (dto.getKhaiThacId() != null) {
			if (lstBcKhaiThacGt != null && lstBcKhaiThacGt.lstBc != null && lstBcKhaiThacGt.lstBc.size() > 0) {
				for (BmSheetDTO sheetItem : listBmSheet) {
					String tieuDeChinh = sheetItem.getTieuDeChinh();
					String tieuDePhu = sheetItem.getTieuDePhu();

					Integer hangInSheet = sheetItem.getBmSheetHangCollection().size();
					Integer hangInBcKt = mapSheet.get(sheetItem.getId());

//					sheetItem.setBmSheetCotCollection(sheetItem.getBmSheetCotCollection().stream().filter(x -> x.getSuDung() == true)
//							.collect(Collectors.toList()));
//					
//					sheetItem.setBmSheetHangCollection(sheetItem.getBmSheetHangCollection().stream().filter(x -> x.getSuDung() == true)
//							.collect(Collectors.toList()));

					if (sheetItem.getTrangBia()) {
						// create Trang bia
						BmBaoCaoExportDTO exportDTO = new BmBaoCaoExportDTO();
						exportDTO = getDataExport(dto.getId());
						workbook = this.generateTrangBiaBaoCaoSheet(exportDTO, sheetItem, workbook);

						CellConfig.autoSizeColumns(workbook);

					} else {
						Sheet sheet = workbook.createSheet(sheetItem.getSheetNameExcel());

						cots = sheetItem.getBmSheetCotCollection();
						hangs = sheetItem.getBmSheetHangCollection();
						tieuDeHang = sheetItem.getBmTieuDeHangCollection();
						tieuDeHangCot = sheetItem.getBmTieuDeHangCotCollection();

						int hang = hangs == null ? 0 : hangs.size();
						int cot = cots == null ? 0 : cots.size();
						int tongHangTieuDe = tieuDeHang == null ? 0 : tieuDeHang.size();
						if (tongHangTieuDe <= 1) {
							tongHangTieuDe = 1;
						}
//								int tongTieuDeHangCot= tieuDeHangCot==null?0:tieuDeHangCot.size();

						if (hang > 0 && cot > 0) {
							createTableKhaiThac(sheetItem, tongHangTieuDe, hang, cot, sheet, workbook, tieuDeChinh,
									tieuDePhu, dto.getId(), lstBcKhaiThacGt, hangInSheet, hangInBcKt);
						}

					}

				}

				if (workbook.getNumberOfSheets() > 1) {
					workbook.setActiveSheet(0);
				}
			}

		}

		return workbook;
	}

	public boolean checkBatBuocCTBaoCao(Integer bcThanhVien, Integer bmBaoCaoId) throws Exception {
		return this.bmBaoCaoDao.checkBatBuocCTBaoCao(bcThanhVien, bmBaoCaoId);
	}
}
