package com.temp.logic.BieuMauBaoCao;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.DropDownDTO;
import com.temp.model.DropDownStringDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCellMenuBDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.persistence.dao.BieuMauBaoCao.BmSheetCtEntityDao;
import com.temp.utils.Utils;

@Component
public class SheetCtLogic {

    private static final String CONGTHUC_NUM_REGEX = "[^\\d]";
    @Autowired
    private BmSheetCtEntityDao bmSheetCtEntityDao;

    /**
     * thêm mới hoặc cập nhật ct sheet
     * 
     * @param dto
     * @return
     */
    public BmSheetCtDTO addOrUpdate(BmSheetCtDTO dto) {

	if (dto.getId() != null && dto.getId() > 0) {
	    dto.setNgayCapNhat(Utils.getCurrentDate());
	    return this.bmSheetCtEntityDao.AddOrUpdate(dto);
	}

	return this.bmSheetCtEntityDao.AddOrUpdate(dto);
    }

    /**
     * Tim kiếm dựa trên Id
     * 
     * @param dto
     * @return
     */

    public BmSheetCtDTO findById(Integer id) {
	return this.bmSheetCtEntityDao.GetById(id);
    }

    /**
     * Lấy danh sách cấu hình cell của sheet
     * 
     * @param sheetid
     * @return
     */
    public BmSheetCellMenuBDTO GetList(Integer sheetId, Integer cotId, Integer hangId,
	    Integer pageNo, Integer pageSize) {

	return this.bmSheetCtEntityDao.getList(sheetId, cotId, hangId, pageNo, pageSize);
    }

    public boolean deleteCell(Integer id) {
	return this.bmSheetCtEntityDao.deleteCell(id);
    }

    /**
     * validate chon option den
     * 
     * @param tuId
     * @param denId
     * @return
     */
    public boolean validateTuHangDenHang(Integer tuId, Integer denId) {
	return this.bmSheetCtEntityDao.validateTuHangDenHang(tuId, denId);
    }

    /**
     * Lấy danh sách cấu hình cell của sheet dropdown
     * 
     * @param sheetid
     * @return
     */
    public List<DropDownDTO> getDropDown(Integer sheetId, Integer cotId, Integer hangId) {

	return this.bmSheetCtEntityDao.getListDropDown(sheetId, cotId, hangId);
    }

    /**
     * nếu item add mới tồn tại thì return true
     * 
     * @param SheetId
     * @param cotId
     * @param hangId
     * @return
     */
    public boolean checkExistAddCell(Integer SheetId, Integer cotId, Integer hangIdFrom,
	    Integer hangIdTo) {
	return this.bmSheetCtEntityDao.checkExistAddCell(SheetId, cotId, hangIdFrom, hangIdTo);
    }

    /**
     * Lay cong thuc theo chi tieu dropdown
     * 
     * @param sheedId
     * @param cotId
     * @param hangId
     * @return
     */
    public List<DropDownStringDTO> getCongThucDropDown(Integer sheedId, Integer cotId,
	    Integer hangId) {
	return this.bmSheetCtEntityDao.getListCongThucDropDown(sheedId, cotId, hangId);
    }

    /**
     * Lay danh sach sheet ct theo ls danh sach hang.
     * 
     * @param congThuc
     * @return
     * @throws Exception
     */
    public List<Integer> getDsSheetCtByLsSheetHangLogic(String congThuc, String baoCaoId,
	    String sheetBcId, String cotId) throws Exception {

	String[] lsSheetHangId = congThuc.split(CONGTHUC_NUM_REGEX);

	if (lsSheetHangId == null || lsSheetHangId.length == 0) {
	    return null;
	}

	if ((baoCaoId == null || StringUtils.isEmpty(baoCaoId))
		|| (sheetBcId == null || StringUtils.isEmpty(sheetBcId))
		|| (cotId == null || StringUtils.isEmpty(cotId))) {
	    return null;
	}

	// trasform list string (x,"","",y,"",z) to string "y,z"
	List<String> dsListHang = Arrays.asList(lsSheetHangId);
	dsListHang = dsListHang.stream().filter(s -> !StringUtils.isEmpty(s.trim()))
		.collect(Collectors.toList());
	dsListHang.remove(0);
	String dsSheetHang = dsListHang.stream().map(String::valueOf)
		.collect(Collectors.joining(" and "));

	return this.bmSheetCtEntityDao.getDsSheetCtByLsSheetHangDAO(dsSheetHang, baoCaoId,
		sheetBcId, cotId);
    }

    public List<BmSheetCtDTO> getDsCtFromBmBcId(List<Integer> bmBaoCaoId) throws Exception {
	return this.bmSheetCtEntityDao.getDsCtFromBmBcId(bmBaoCaoId);
    }

    public List<BmSheetCtDTO> getLsCellByBaoCaoAndSheet(Integer baoCaoId, Integer sheetId)
	    throws Exception {
	return this.bmSheetCtEntityDao.getLsCellByBaoCaoAndSheet(baoCaoId, sheetId);
    };

    // SUN,[id][id] => [id1]+[id2]+[id3]
    public String findByCongThuc(String formula) {
	String strReturn = "";
	List<BmSheetCtDTO> lstCT = null;
	String[] array = formula.split(",");
	if (array != null && array.length > 1) {
	    if (!array[0].toLowerCase().equals("cif")) {
		String[] arrCtId = array[1].split("\\|");
		String strIdTu = arrCtId[0].replace("[", "").replace("]", "");
		Integer ctIdTu = Utils.tryParseInt(strIdTu);
		Integer ctIdDen = Utils.tryParseInt(arrCtId[1].replace("[", "").replace("]", ""));
		if (ctIdTu != null && ctIdTu > 0 && ctIdDen != null && ctIdDen > 0) {
		    lstCT = this.bmSheetCtEntityDao.getDsCtTrongKhoang(ctIdTu, ctIdDen);
		}
	    }
	    switch (array[0].toUpperCase()) {
	    case "AVG":
//				([2840]+[2841]+[2843]+[2844]+[2845])/5
		if (lstCT != null && lstCT.size() > 0) {
		    strReturn += "(";
		    for (BmSheetCtDTO dto : lstCT) {
			strReturn += "[" + dto.getId() + "]+";
		    }
		    strReturn = strReturn.substring(0, strReturn.length() - 1);
		    strReturn += ")/" + lstCT.size();
		}
		break;
	    default:
//				([2840]+[2841]+[2843]+[2844]+[2845])/5
		if (lstCT != null && lstCT.size() > 0) {
		    strReturn += "(";
		    for (BmSheetCtDTO dto : lstCT) {
			strReturn += "[" + dto.getId() + "]+";
		    }
		    strReturn = strReturn.substring(0, strReturn.length() - 1);
		    strReturn += ")";
		}
		break;
	    }
	}
	return strReturn;
    }

    /**
     * Get o chi tieu kieu so/tien te/tl % dropdown.
     * 
     * @param sheedId
     * @param cotId
     * @param hangId
     * @return
     * @throws Exception
     */
    public List<DropDownDTO> getOChiTieuCanhCanhDropdown(Integer sheedId, Integer cotId,
	    Integer hangId) throws Exception {
	return this.bmSheetCtEntityDao.getOChiTieuCanhCanhDao(sheedId, cotId, hangId);
    }

    public List<BmSheetCtDTO> getLstChiTieuBuildBcDauRa(Integer bmBaoCaoId, Integer sheetId)
	    throws Exception {
	return this.bmSheetCtEntityDao.getLstChiTieuBuildBcDauRa(bmBaoCaoId, sheetId);
    }

    /**
     * [C-20200518] Get data BmSheetCtDTO by field BmSheetCtVaoId. 
     * 
     * @param bmSheetCtVaoId
     * @return
     * @throws Exception
     */
    public BmSheetCtDTO getBmSheetCtByBmSheetCtVaoIdLogic(int bmSheetCtVaoId) throws Exception {
	return this.bmSheetCtEntityDao.getBmSheetCtByBmSheetCtVaoId(bmSheetCtVaoId);
    }
}
