package com.temp.logic.NguoiDung;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DropDownDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.QuyenBDTO;
import com.temp.model.NguoiDung.QuyenDTO;
import com.temp.model.NguoiDung.SubQuyenDTO;
import com.temp.persistence.dao.NguoiDung.QuyenDao;

@Component
public class QuyenLogic {

	@Autowired
	private QuyenDao qtQuyenDao;

	/**
	 * Thực hiện delete quốc tịch
	 * 
	 * @param QuyenId quốc tịch id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean deleteQuyen(int QuyenId) {
		if (QuyenId > 0) {
			if (qtQuyenDao.deleteQuyenById(QuyenId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQuyen List Chức năng
	 */
	@SuppressWarnings("unused")
	public QuyenBDTO getAll(String keySearch, String tenQuyen, String maQuyen, Integer actionId, String phanHe,
			String ghiChu, String trangThai) {

		QuyenBDTO result = new QuyenBDTO();

		keySearch = validateParam(keySearch);
		tenQuyen = validateParam(tenQuyen);
		maQuyen = validateParam(maQuyen);

		result = qtQuyenDao.getAll(keySearch, tenQuyen, maQuyen, actionId, phanHe, ghiChu, trangThai);

//		start build tree

		List<QuyenDTO> listInput = result.getListQuyen();

		List<QuyenDTO> tree = getTree(listInput);
		result.getListQuyen().clear();
		result.setListQuyen(tree);

//		 QuyenDTO root = listInput.stream()                      
//	               .filter(item -> item.getLevel()==1)         
//	               .findAny()                                    
//	               .orElse(null);  
//		 
//		 root=toTree(root, listInput);
//		 
//		 result.getListQuyen().clear();
//		 result.getListQuyen().add(root);

		return result;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQuyen List Chức năng
	 */
	@SuppressWarnings("unused")
	public QuyenBDTO listQuyens(String keySearch, String tenQuyen, String maQuyen, Integer actionId,
			String phanHe, String ghiChu, String trangThai) {

		QuyenBDTO result = new QuyenBDTO();

		keySearch = validateParam(keySearch);
		tenQuyen = validateParam(tenQuyen);
		maQuyen = validateParam(maQuyen);

		result = qtQuyenDao.listQuyens(keySearch, tenQuyen, maQuyen, actionId, phanHe, ghiChu, trangThai);

//		start build tree

		List<QuyenDTO> listInput = result.getListQuyen();

		List<QuyenDTO> tree = getTree(listInput);
		result.getListQuyen().clear();
		result.setListQuyen(tree);

//		 QuyenDTO root = listInput.stream()                      
//	               .filter(item -> item.getLevel()==1)         
//	               .findAny()                                    
//	               .orElse(null);  
//		 
//		 root=toTree(root, listInput);
//		 
//		 result.getListQuyen().clear();
//		 result.getListQuyen().add(root);

		return result;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param Quyen đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean upDateQuyen(QuyenDTO dto) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		QuyenDTO oldDto = qtQuyenDao.findById(dto.getId());
		if (oldDto != null) {
			dto.setActionId(dto.getAction().getValue());
			dto.setKhoaChaId(dto.getQuyenCha().getValue());
			dto.setNgayTao(oldDto.getNgayTao());
			dto.setNguoiTaoId(oldDto.getNguoiTaoId());
			dto.setNguoiCapNhatId(userInfo.getId());
			dto.setNgayCapNhat(Timestamp.valueOf(LocalDateTime.now()));
			return this.qtQuyenDao.addOrUpdateQuyen(dto);
		}

		return false;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param Quyen đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean createQuyen(QuyenDTO dto) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		dto.setNguoiTaoId(userInfo.getId());
		dto.setNgayTao(Timestamp.valueOf(LocalDateTime.now()));
		return this.qtQuyenDao.addOrUpdateQuyen(dto);

	}

	public QuyenDTO getById(int id) {
		QuyenDTO qtDTO = new QuyenDTO();
		qtDTO = qtQuyenDao.findById(id);

		if (qtDTO != null) {
			DropDownDTO QuyenCha;
			if (qtDTO.getKhoaChaId() == 0) {
				qtDTO.setQuyenCha(new DropDownDTO(0, "-- Chọn --"));
				qtDTO.setAction(null);
			} else {
				QuyenCha = qtQuyenDao.findQuyenCha(qtDTO.getKhoaChaId());
				qtDTO.setQuyenCha(QuyenCha);
			}
			DropDownDTO acition = new DropDownDTO();
			if(qtDTO.getActionId() != null) {
				acition = qtQuyenDao.findAction(qtDTO.getActionId());
			}
			qtDTO.setAction(acition);

		}

		return qtDTO;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistQuyen(QuyenDTO dto) {
		// dto.setNgayTao(new Timestamp());
		boolean trangThai = dto.getTrangThai();
		boolean result = false;
		result = qtQuyenDao.isQuyenExist(dto.getMaQuyen(), trangThai);

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = qtQuyenDao.isExistById(id);

		return result;
	}

	public QuyenBDTO getDropdownKhoaChaId() {

		QuyenBDTO lstQuyen = new QuyenBDTO();

		lstQuyen = qtQuyenDao.getDropdownKhoaChaId();

		return lstQuyen;
	}

	/**
	 * Get list dropdown Action
	 * 
	 * 
	 * @return lstPageQuyen List Chức năng
	 */
	public QuyenBDTO getDropdownAction() {

		QuyenBDTO lstQuyen = new QuyenBDTO();

		lstQuyen = qtQuyenDao.getDropdownAction();

		return lstQuyen;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQuyen List Chức năng
	 */
	public boolean changeStatus(SubQuyenDTO dto) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		QuyenDTO oldDto = qtQuyenDao.findById(dto.getId());
		if (oldDto != null) {
			oldDto.setTrangThai(dto.getTrangThai());
			oldDto.setNguoiCapNhatId(userInfo.getId());
			oldDto.setNgayCapNhat(Timestamp.valueOf(LocalDateTime.now()));
			return this.qtQuyenDao.addOrUpdateQuyen(oldDto);
		}
		return false;
	}

	public static List<QuyenDTO> getTree(List<QuyenDTO> list) {
		Map<Integer, QuyenDTO> map = new HashMap<>();
		List<QuyenDTO> result = new ArrayList<QuyenDTO>();

		for (QuyenDTO node : list) {
			map.put(node.getId(), node);
		}

		for (int id : map.keySet()) {
			QuyenDTO current = map.get(id);
			QuyenDTO parrent = map.get(current.getKhoaChaId());
			if (parrent != null) {
				current.setHeader("generic");
				parrent.getChildren().add(current);
			} else {
				current.setHeader("root");
				current.setKhoaChaId(0);
				result.add(current);
			}
		}
		return result;
	}

	public boolean isExistByMa(String maQuyen, Integer id) {
		boolean result = false;

		result = qtQuyenDao.isExistByMa(maQuyen, id);

		return result;
	}

}
