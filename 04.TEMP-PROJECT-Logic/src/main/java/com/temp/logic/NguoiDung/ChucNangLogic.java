package com.temp.logic.NguoiDung;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DropDownDTO;
import com.temp.model.NguoiDung.ChucNangBDTO;
import com.temp.model.NguoiDung.ChucNangDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.SubChucNangDTO;
import com.temp.persistence.dao.NguoiDung.QtChucNangDao;

@Component
public class ChucNangLogic {

	@Autowired
	private QtChucNangDao qtChucNangDao;

	/**
	 * Thực hiện delete quốc tịch
	 * 
	 * @param ChucNangId quốc tịch id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean deleteChucNang(int ChucNangId) {
		if (ChucNangId > 0) {
			if (qtChucNangDao.deleteChucNangById(ChucNangId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageChucNang List Chức năng
	 */
	@SuppressWarnings("unused")
	public ChucNangBDTO getAll(String keySearch, String tenChucNang, String maChucNang, Integer actionId, String phanHe,
			String ghiChu, String trangThai) {

		ChucNangBDTO result = new ChucNangBDTO();

		keySearch = validateParam(keySearch);
		tenChucNang = validateParam(tenChucNang);
		maChucNang = validateParam(maChucNang);

		result = qtChucNangDao.getAll(keySearch, tenChucNang, maChucNang, actionId, phanHe, ghiChu, trangThai);

//		start build tree

		List<ChucNangDTO> listInput = result.getListChucNang();

		List<ChucNangDTO> tree = getTree(listInput);
		result.getListChucNang().clear();
		result.setListChucNang(tree);

//		 ChucNangDTO root = listInput.stream()                      
//	               .filter(item -> item.getLevel()==1)         
//	               .findAny()                                    
//	               .orElse(null);  
//		 
//		 root=toTree(root, listInput);
//		 
//		 result.getListChucNang().clear();
//		 result.getListChucNang().add(root);

		return result;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageChucNang List Chức năng
	 */
	@SuppressWarnings("unused")
	public ChucNangBDTO listChucNangs(String keySearch, String tenChucNang, String maChucNang, Integer actionId,
			String phanHe, String ghiChu, String trangThai) {

		ChucNangBDTO result = new ChucNangBDTO();

		keySearch = validateParam(keySearch);
		tenChucNang = validateParam(tenChucNang);
		maChucNang = validateParam(maChucNang);

		result = qtChucNangDao.listChucNangs(keySearch, tenChucNang, maChucNang, actionId, phanHe, ghiChu, trangThai);

//		start build tree

		List<ChucNangDTO> listInput = result.getListChucNang();

		List<ChucNangDTO> tree = getTree(listInput);
		result.getListChucNang().clear();
		result.setListChucNang(tree);

//		 ChucNangDTO root = listInput.stream()                      
//	               .filter(item -> item.getLevel()==1)         
//	               .findAny()                                    
//	               .orElse(null);  
//		 
//		 root=toTree(root, listInput);
//		 
//		 result.getListChucNang().clear();
//		 result.getListChucNang().add(root);

		return result;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param ChucNang đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean upDateChucNang(ChucNangDTO dto) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		ChucNangDTO oldDto = qtChucNangDao.findById(dto.getId());
		if (oldDto != null) {
			if(dto.getAction() != null) {
				dto.setActionId(dto.getAction().getValue());
			}
			if(dto.getChucNangCha() != null) {
				dto.setKhoaChaId(dto.getChucNangCha().getValue());
			}
			dto.setNgayTao(oldDto.getNgayTao());
			dto.setNguoiTaoId(oldDto.getNguoiTaoId());
			dto.setNguoiCapNhatId(userInfo.getId());
			dto.setNgayCapNhat(Timestamp.valueOf(LocalDateTime.now()));
			return this.qtChucNangDao.addOrUpdateChucNang(dto);
		}

		return false;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param ChucNang đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean createChucNang(ChucNangDTO dto) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		dto.setNguoiTaoId(userInfo.getId());
		dto.setNgayTao(Timestamp.valueOf(LocalDateTime.now()));
		return this.qtChucNangDao.addOrUpdateChucNang(dto);

	}

	public ChucNangDTO getById(int id) {
		ChucNangDTO qtDTO = new ChucNangDTO();
		qtDTO = qtChucNangDao.findById(id);

		if (qtDTO != null) {
			DropDownDTO chucNangCha;
			if (qtDTO.getKhoaChaId() == 0) {
				qtDTO.setChucNangCha(new DropDownDTO(0, "-- Chọn --"));
				qtDTO.setAction(null);
			} else {
				chucNangCha = qtChucNangDao.findChucNangCha(qtDTO.getKhoaChaId());
				qtDTO.setChucNangCha(chucNangCha);
			}
			DropDownDTO acition = new DropDownDTO();
			if(qtDTO.getActionId() != null) {
				acition = qtChucNangDao.findAction(qtDTO.getActionId());
			}
			qtDTO.setAction(acition);

		}

		return qtDTO;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
//	private String validateParamKeySort(String param) {
//
//		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
//				|| "undefined".equals((param + "").toLowerCase()))) {
//			return "id";
//		}
//
//		param = (param == null) ? null : param.trim();
//
//		return param;
//	}

	public boolean isExistChucNang(ChucNangDTO dto) {
		// dto.setNgayTao(new Timestamp());
		boolean trangThai = dto.getTrangThai();
		boolean result = false;
		result = qtChucNangDao.isChucNangExist(dto.getMaChucNang(), trangThai);

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = qtChucNangDao.isExistById(id);

		return result;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageChucNang List Chức năng
	 */
	public ChucNangBDTO getDropdownKhoaChaId() {

		ChucNangBDTO lstChucNang = new ChucNangBDTO();

		lstChucNang = qtChucNangDao.getDropdownKhoaChaId();

		return lstChucNang;
	}

	/**
	 * Get list dropdown Action
	 * 
	 * 
	 * @return lstPageChucNang List Chức năng
	 */
	public ChucNangBDTO getDropdownAction() {

		ChucNangBDTO lstChucNang = new ChucNangBDTO();

		lstChucNang = qtChucNangDao.getDropdownAction();

		return lstChucNang;
	}

	/**
	 * Get list Chức năng
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageChucNang List Chức năng
	 */
	public boolean changeStatus(SubChucNangDTO dto) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		ChucNangDTO oldDto = qtChucNangDao.findById(dto.getId());
		if (oldDto != null) {
			oldDto.setTrangThai(dto.getTrangThai());
			oldDto.setNguoiCapNhatId(userInfo.getId());
			oldDto.setNgayCapNhat(Timestamp.valueOf(LocalDateTime.now()));
			return this.qtChucNangDao.addOrUpdateChucNang(oldDto);
		}
		return false;
	}

//	private static ChucNangDTO toTree(ChucNangDTO root, List<ChucNangDTO> listInput) {
//
//		List<ChucNangDTO> childLists = listInput.stream().filter(item -> item.getKhoaChaId() == root.getId())
//				.collect(Collectors.toList());
//		root.setChildren(childLists);
//
//		if (childLists.size() > 0) {
//			for (ChucNangDTO functions : childLists) {
//				toTree(functions, listInput);
//			}
//		}
//
//		return root;
//	}

	public static List<ChucNangDTO> getTree(List<ChucNangDTO> list) {
		Map<Integer, ChucNangDTO> map = new HashMap<>();
		List<ChucNangDTO> result = new ArrayList<ChucNangDTO>();

		for (ChucNangDTO node : list) {
			map.put(node.getId(), node);
		}

		for (int id : map.keySet()) {
			ChucNangDTO current = map.get(id);
			ChucNangDTO parrent = map.get(current.getKhoaChaId());
			if (parrent != null) {
				current.setHeader("generic");
				parrent.getChildren().add(current);
			} else {
				current.setHeader("root");
				current.setKhoaChaId(0);
				result.add(current);
			}
		}
		return result;
	}

	public boolean isExistByMa(String maChucNang, Integer id) {
		boolean result = false;

		result = qtChucNangDao.isExistByMa(maChucNang, id);

		return result;
	}

}
