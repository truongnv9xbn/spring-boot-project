package com.temp.logic.NguoiDung;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.ThongBaoBDTO;
import com.temp.model.ThongBaoDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.ThongBaoDao;
import com.temp.utils.TimestampUtils;

@Component
public class ThongBaoLogic {

	@Autowired
	private ThongBaoDao ThongBaoDao;

	/**
	 * Get list trạng thái công ty chứng khoán
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * @param list 
	 * 
	 * @return lstPageThongBao List trạng thái công ty chứng khoán
	 */
	public ThongBaoBDTO listThongBao(Integer pageNo, Integer pageSize, String filterkey, Integer nguoiNhanId, boolean desc, Integer trangThai, String list) {

		ThongBaoBDTO lstPageThongBao = new ThongBaoBDTO();

		lstPageThongBao = ThongBaoDao.listThongBao(pageNo, pageSize, filterkey, nguoiNhanId, desc, trangThai, list);
		if(lstPageThongBao != null) {
			for(ThongBaoDTO tBao : lstPageThongBao.lstThongBao) {
				tBao.setNgayTaoStr(TimestampUtils.TimestampToString_ddMMyyyyHHmmss(tBao.getNgayTao()));
			}
		}
		return lstPageThongBao;
	}


	
	public boolean addThongBao(ThongBaoDTO dto) {
		if (dto != null) {
			return ThongBaoDao.addThongBao(dto);
		}

		return false;

	}
	
	public ThongBaoDTO findById(Integer id) {
		if (id > 0) {
			ThongBaoDTO qtDTO = ThongBaoDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}
			return null;
		}
//		
//		
//		return null;
//	}
	
//	public ThongBaoDTO getByThamSo(String thamSo) {
//		if (thamSo != "") {
//			ThongBaoDTO qtDTO = ThongBaoDao.findByThamSo(thamSo);
//			if (qtDTO != null) {
//				return qtDTO;
//			}
//			
//			
//		}
//		
//		
//		return null;
//	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = param.trim();

		return param;
	}

	public boolean isExistById(Integer id) {

		boolean result = false;

		result = ThongBaoDao.isExistById(id);

		return result;
	}
	
	public boolean updateSeen(Integer id) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		ThongBaoDTO tbDTO = this.ThongBaoDao.findById(id);
		if(tbDTO!=null) {
			if(tbDTO.getListNguoiDaXem() == null || (tbDTO.getListNguoiDaXem() != null && !tbDTO.getListNguoiDaXem().contains(";"))  ) {
				tbDTO.setListNguoiDaXem(";" + userInfo.getId() + ";");
			}
			else if(tbDTO.getListNguoiDaXem().indexOf(";" + userInfo.getId() + ";") < 0){
				tbDTO.setListNguoiDaXem(tbDTO.getListNguoiDaXem() + userInfo.getId() + ";");
			}
			return this.ThongBaoDao.updateSeen(tbDTO);
		}
		return false;
	}

	public ThongBaoDTO addThongBao(String tieuDe, String noiDung, String linkHref, String nguoiNhanStr) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public boolean seenAll(List<Integer> lstTbId, Integer nguoiDungId) {
		return this.ThongBaoDao.seenAll(lstTbId, nguoiDungId);
	}

}
