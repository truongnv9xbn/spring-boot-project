package com.temp.logic.NguoiDung;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.NguoiDung.QtLogHeThongBDTO;
import com.temp.model.NguoiDung.QtLogHeThongDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.NguoiDung.QtLogHeThongDao;

@Component
public class QtLogHeThongLogic {

	@Autowired
	private QtLogHeThongDao qtLogHeThongDao;

	/**
	 * Get list
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQtlogHeThong List
	 */
	public QtLogHeThongBDTO listQtlogHeThongs(String filterkey, String ipThucHien, String noiDung, String tuNgay, String denNgay, String logType, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai,String taiKhoan) {

		QtLogHeThongBDTO lstPageQtlogHeThong = new QtLogHeThongBDTO();

		filterkey = validateParam(filterkey);
		ipThucHien = validateParam(ipThucHien);
		logType = validateParam(logType);
		noiDung = validateParam(noiDung);
		keySort = validateParamKeySort(keySort);

		lstPageQtlogHeThong = qtLogHeThongDao.listLogHeThongs(filterkey, ipThucHien,noiDung,tuNgay,denNgay, logType, pageNo, pageSize, keySort,
				desc, trangThai,taiKhoan);

		return lstPageQtlogHeThong;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param QtlogHeThong đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean addOrUpDateQtlogHeThong(QtLogHeThongDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

		if (dto != null) {
			if (dto.getId() > 0) {
				QtLogHeThongDTO oldQtlogHeThong = qtLogHeThongDao.findById(dto.getId());
				oldQtlogHeThong.setIpThucHien(dto.getIpThucHien());
				oldQtlogHeThong.setNoiDung(dto.getNoiDung());
				oldQtlogHeThong.setLogType(dto.getLogType());
				oldQtlogHeThong.setTrangThai(dto.getTrangThai());
				// TODO người tạo
				return qtLogHeThongDao.addOrUpdate(oldQtlogHeThong);

			}
			dto.setNguoiTaoId(userInfo.getId());
			return qtLogHeThongDao.addOrUpdate(dto);
		}

		return false;

	}

	
	public boolean AddLogSYS(QtLogHeThongDTO dto) {

		//QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();

		if (dto != null&&dto.getId()!=null) {
			if (dto.getId() > 0) {
				QtLogHeThongDTO oldQtlogHeThong = qtLogHeThongDao.findById(dto.getId());
				oldQtlogHeThong.setIpThucHien(dto.getIpThucHien());
				oldQtlogHeThong.setNoiDung(dto.getNoiDung());
				oldQtlogHeThong.setLogType(dto.getLogType());
				oldQtlogHeThong.setTrangThai(dto.getTrangThai());
				// TODO người tạo
				return qtLogHeThongDao.addOrUpdate(oldQtlogHeThong);

			}
		//	dto.setNguoiTaoId(userInfo.getId());
			return qtLogHeThongDao.addOrUpdate(dto);
		}

		return false;

	}
	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean deleteMulti(List<Integer> lstId) {
		// TODO Auto-generated method stub
		return qtLogHeThongDao.deleteMulti(lstId);
	}
	public boolean deleteById(int id) throws Exception {
		// TODO Auto-generated method stub
		return qtLogHeThongDao.deleteById(id);
	}

	public boolean isExistById(int id) {
		// TODO Auto-generated method stub
		return qtLogHeThongDao.isExistById(id);
	}
}
