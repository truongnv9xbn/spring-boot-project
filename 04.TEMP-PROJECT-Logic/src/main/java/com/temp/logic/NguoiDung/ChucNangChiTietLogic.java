package com.temp.logic.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.ChucNangChiTietBDTO;
import com.temp.model.NguoiDung.ChucNangChiTietDTO;
import com.temp.persistence.dao.NguoiDung.QtChucNangChiTietDao;

@Component
public class ChucNangChiTietLogic {
	@Autowired
	QtChucNangChiTietDao qtChucNangChiTietDao;

	public ChucNangChiTietBDTO filter(String keySearch, String tenChucNang, String trangThai, Integer pageNo,
			Integer pageSize, String keySort, boolean desc) {
		return qtChucNangChiTietDao.filter(keySearch, tenChucNang, trangThai, pageNo, pageSize, keySort, desc);
	}

	public ChucNangChiTietDTO getById(Integer id) {
		return qtChucNangChiTietDao.findById(id);
	}

	public boolean addOrUpDate(ChucNangChiTietDTO dto) {
		if (dto != null) {
			if (dto.getId() > 0) {
				ChucNangChiTietDTO old = qtChucNangChiTietDao.findById(dto.getId());
				old.setTenChucNang(dto.getTenChucNang());
				old.setApiRoute(dto.getApiRoute());
				old.setVueRoute(dto.getVueRoute());
				old.setApiTemp(dto.getApiTemp());
				old.setTrangThai(dto.isTrangThai());

				return qtChucNangChiTietDao.addOrUpdate(old);
			}
			return qtChucNangChiTietDao.addOrUpdate(dto);
		}
		return false;
	}
	
	public boolean deleteById(int id) {
		if (id > 0) {
			if (qtChucNangChiTietDao.deleteById(id)) {
				return true;
			}
			return false;
		}

		return false;
	}
}
