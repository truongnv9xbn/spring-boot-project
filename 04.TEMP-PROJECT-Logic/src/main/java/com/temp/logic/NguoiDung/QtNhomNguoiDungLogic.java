package com.temp.logic.NguoiDung;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNhomNguoiDungDTO;
import com.temp.persistence.dao.NguoiDung.LkChucNangNhomNguoiDao;
import com.temp.persistence.dao.NguoiDung.QtNhomNguoiDungDao;
import com.temp.utils.LocalDateTimeUtils;
import com.temp.utils.Utils;

@Component
public class QtNhomNguoiDungLogic {

	@Autowired
	private QtNhomNguoiDungDao qtNhomNguoiDungDao;
	
	@Autowired
	private LkChucNangNhomNguoiDao lkChucNangNhomNguoiDao;
	

	/**
	 * Thực hiện delete quốc tịch
	 * 
	 * @param QtNhomNguoiDungId quốc tịch id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean deleteQtNhomNguoiDungById(int id) {
		if (id > 0) {
			if (qtNhomNguoiDungDao.deleteQtNhomNguoiDungById(id)) {
				return true;
			}
			return false;
		}

		return false;
	}

	 

	
	/**
	 * Get list
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQtNhomNguoiDung List Quoc Tich
	 */
	public QtNhomNguoiDungBDTO getList(Integer pageNo,Integer pageSize, String keySort, boolean desc, String keySearch, String tenNhomNguoiDung, String ghiChu, String trangThai) {

		QtNhomNguoiDungBDTO result = new QtNhomNguoiDungBDTO();

		keySearch = validateParam(keySearch);
		tenNhomNguoiDung = validateParam(tenNhomNguoiDung);
		ghiChu = validateParam(ghiChu);
		keySort = validateParamKeySort(keySort);

		result = qtNhomNguoiDungDao.getList(pageNo, pageSize, keySort, desc, keySearch, tenNhomNguoiDung, ghiChu, trangThai);
		List<QtNhomNguoiDungDTO> listInput = result.getLstQtNhomNguoiDung();

		 
		result.setLstQtNhomNguoiDung(listInput);

		return result;
	}
	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param QtNhomNguoiDung đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public QtNhomNguoiDungDTO createQtNhomNguoiDung(QtNhomNguoiDungDTO dto) {
		QtNhomNguoiDungDTO dtoRespon = new QtNhomNguoiDungDTO();
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		dto.setNguoiTao(userInfo.getHoTen());
		dto.setNgayTao(Timestamp.valueOf(LocalDateTime.now()));
		dtoRespon = this.qtNhomNguoiDungDao.addOrUpdateQtNhomNguoiDung(dto);
	 	if(dtoRespon != null) {
	 		return dtoRespon;
	 	}
		return dtoRespon;

	}

	/**
	 * Thực hiện update
	 * 
	 * @param QtNhomNguoiDung đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public QtNhomNguoiDungDTO upDateQtNhomNguoiDung(QtNhomNguoiDungDTO dto) {
		QtNhomNguoiDungDTO dtoRespon = new QtNhomNguoiDungDTO();
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		QtNhomNguoiDungDTO oldDto = qtNhomNguoiDungDao.findById(dto.getId());
		
		if (oldDto != null) {
			Utils.compareAndApplyDifferences(oldDto, dto);
			oldDto.setNguoiCapNhat(userInfo.getHoTen());
			oldDto.setNgayCapNhat(Timestamp.valueOf(LocalDateTime.now()));
			dtoRespon = this.qtNhomNguoiDungDao.addOrUpdateQtNhomNguoiDung(dto);	 
		}
		return dtoRespon;
	}

	public QtNhomNguoiDungDTO getById(int id) {
		List<Integer>listChucNang;
		if (id > 0) {
			QtNhomNguoiDungDTO qtDTO = qtNhomNguoiDungDao.findById(id);
			
			 listChucNang=lkChucNangNhomNguoiDao.listByNguoiDungId(id);
			
			if (qtDTO != null) {
				qtDTO.setListIdChucNang(listChucNang);
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Lấy tất cả dữ liệu
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	public QtNhomNguoiDungBDTO listAll(String keySort, boolean desc, String trangThai) {
		QtNhomNguoiDungBDTO lst = new QtNhomNguoiDungBDTO();
		lst = qtNhomNguoiDungDao.listAll(keySort, desc, trangThai);
		return lst;
	}
	
	public static List<QtNhomNguoiDungDTO> getTree(List<QtNhomNguoiDungDTO> list) {
		Map<Integer, QtNhomNguoiDungDTO> map = new HashMap<>();
		List<QtNhomNguoiDungDTO> result = new ArrayList<QtNhomNguoiDungDTO>();

		for (QtNhomNguoiDungDTO node : list) {
			map.put(node.getId(), node);
		}

		for (int id : map.keySet()) {
			QtNhomNguoiDungDTO current = map.get(id);
			QtNhomNguoiDungDTO parrent = map.get(current.getKhoaChaId());
			if (parrent != null) {
				parrent.getChildren().add(current);
			} else {
				current.setKhoaChaId(0);
				result.add(current);
			}
		}
		return result;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistQtNhomNguoiDung(QtNhomNguoiDungDTO dto) {

		boolean result = false;
		result = qtNhomNguoiDungDao.isQtNhomNguoiDungExist(dto.getId(), dto.getMaNhomNguoiDung(), dto.getTrangThai());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = qtNhomNguoiDungDao.isExistById(id);

		return result;
	}

	/**
	 * Get list Quoc Tich
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQtNhomNguoiDung List Quoc Tich
	 */
	public QtNhomNguoiDungBDTO getDropdown() {

		QtNhomNguoiDungBDTO lstQtNhomNguoiDung = new QtNhomNguoiDungBDTO();

		lstQtNhomNguoiDung = qtNhomNguoiDungDao.getDropdown();

		return lstQtNhomNguoiDung;
	}

	public boolean changeStatus(int id) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		QtNhomNguoiDungDTO oldDto = qtNhomNguoiDungDao.findById(id);
		if (oldDto != null) {
			oldDto.setTrangThai(!oldDto.getTrangThai());
			oldDto.setNguoiCapNhat(userInfo.getHoTen());
			oldDto.setNgayCapNhat(Timestamp.valueOf(LocalDateTime.now()));
			 
			 return this.qtNhomNguoiDungDao.changeStatus(oldDto);
		}
		return false;
	}
}
