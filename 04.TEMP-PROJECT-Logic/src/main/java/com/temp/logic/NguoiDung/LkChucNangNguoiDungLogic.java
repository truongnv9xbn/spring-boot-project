package com.temp.logic.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.LkChucNangNguoiBDTO;
import com.temp.persistence.dao.NguoiDung.LkChucNangNguoiDao;

@Component
public class LkChucNangNguoiDungLogic {

	@Autowired
	private LkChucNangNguoiDao dao;
	
	
	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param ChucNang đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean addOrUpdateLkChucNangNguoi(LkChucNangNguoiBDTO bdto) {

		 return dao.addOrUpdateLkChucNangNguoi(bdto.getListDto());

	}
	
	

	/**
	 * Thực hiện delete quốc tịch
	 * 
	 * @param ChucNangId quốc tịch id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean deleteByNguoiDung(int idNguoiDung) {
		
		return dao.deleteByNguoiDung(idNguoiDung);
	}

	/**
	 * Get list Quoc Tich
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageChucNang List Quoc Tich
	 */
	public LkChucNangNguoiBDTO listQuyen(String filterkey, Integer pageNo,Integer pageSize, String keySort, boolean desc, Integer idNguoiDung) {

		LkChucNangNguoiBDTO bdto = new LkChucNangNguoiBDTO();

		filterkey = validateParam(filterkey);
		 
		keySort = validateParamKeySort(keySort);

		bdto= dao.listQuyen(filterkey, pageNo, pageSize, keySort, desc, idNguoiDung);

		return bdto;
	}

	

 

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}
}
