package com.temp.logic.NguoiDung;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.LkChucNangNhomNguoiBDTO;
import com.temp.model.NguoiDung.LkChucNangNhomNguoiDTO;
import com.temp.model.file.ChucNangUserBDTO;
import com.temp.model.file.ChucNangUserDto;
import com.temp.persistence.dao.NguoiDung.LkChucNangNhomNguoiDao;

@Component
public class LkChucNangNhomNguoiLogic {

	@Autowired
	private LkChucNangNhomNguoiDao lkChucNangNhomNguoiDao;

	public ChucNangUserBDTO getQuyenAndMenuUser(int nguoiDungId, boolean getTreeMenu) {
		ChucNangUserBDTO lstChucNang = lkChucNangNhomNguoiDao.menuGenerate(nguoiDungId, false);
		if (lstChucNang == null) {
			return null;
		}
		if (getTreeMenu) {
			StringBuffer menuStr = new StringBuffer();
			menuStr.append("<div class=\"page-sidebar-wrapper\"> <div class=\"page-sidebar navbar-collapse collapse\""
					+ " style=\"background-color: #063521\"> <ul class=\"page-sidebar-menu \" data-keep-expanded=\"false\""
					+ " data-auto-scroll=\"true\" data-slide-speed= \"200\"> <li class=\"sidebar-toggler-wrapper\">"
					+ "<i class=\"sidebar-toggler fa fa-bars\"></i>" + "</li><br>");
			List<ChucNangUserDto> tmp = getTree(lstChucNang.getLstChucNangMenu());
			if (tmp != null) {
				String s = GenMenuString(tmp, menuStr);
				//
				s = s + "</ul></div></div>";
				lstChucNang.setMenuGenerate(s);
			}
		}

		return lstChucNang;
	}

	public List<String> getAllChucNangChiTiet() {
		return lkChucNangNhomNguoiDao.getAllChucNangChiTiet();
	}

	public ChucNangUserBDTO getMenuAdmin() {
		ChucNangUserBDTO lstChucNang = lkChucNangNhomNguoiDao.menuGenerateAdmin(false);
		if (lstChucNang == null) {
			return null;
		}
		StringBuffer menuStr = new StringBuffer();
		menuStr.append("<div class=\"page-sidebar-wrapper\"> <div class=\"page-sidebar navbar-collapse collapse\""
				+ " style=\"background-color: #063521\"> <ul class=\"page-sidebar-menu \" data-keep-expanded=\"false\""
				+ " data-auto-scroll=\"true\" data-slide-speed= \"200\"> <li class=\"sidebar-toggler-wrapper\">"
				+ "<i class=\"sidebar-toggler fa fa-bars\"></i>" + "</li><br>");
		List<ChucNangUserDto> tmp = getTree(lstChucNang.getLstChucNangMenu());
		String s = GenMenuString(tmp, menuStr);
		//
		s = s + "</ul></div></div>";
		lstChucNang.setMenuGenerate(s);
		return lstChucNang;
	}

	public static List<ChucNangUserDto> getTree(List<ChucNangUserDto> list) {
		if (list == null || list.isEmpty()) {
			return null;
		}
		List<ChucNangUserDto> result = new ArrayList<ChucNangUserDto>();
		Map<Integer, ChucNangUserDto> map = new HashMap<>();
		for (ChucNangUserDto node : list) {
			map.put(node.getId(), node);
		}

		for (int id : map.keySet()) {
			ChucNangUserDto current = map.get(id);
			ChucNangUserDto parrent = map.get(current.getKhoaChaId());
			if (parrent != null) {
				current.setHeader("generic");
				parrent.getChildren().add(current);
			} else {
				current.setHeader("root");
				current.setKhoaChaId(0);
				result.add(current);
			}
		}

		return result;
	}

	public static String GenMenuString(List<ChucNangUserDto> tree, StringBuffer menuStr) {
		if (tree != null) {
			int count = 0;
			for (ChucNangUserDto item : tree.stream().sorted(Comparator.comparing(ChucNangUserDto::getThuTu))
					.collect(Collectors.toList())) {
				if (item.getKhoaChaId() == 0) {
					if (count == 0) {
						// ToanLK: fix menu khi vue route la cac link ngoai he thong
						menuStr.append("<li class=\"title open\" >" + "<a href=\""
								+ (item.getVueRoute().contains(".") ? item.getVueRoute() : "javascript://;") + "\">");
						if (item.getIcon() != null) {
							String iscon = "<i class=\"material-icons icon-menu\">" + item.getIcon() + "</i>";
							menuStr.append(iscon);
						}
						menuStr.append("<span class=\"title\">" + item.getTenChucNang() + "</span>");

						if (item.getChildren().size() > 0) {
							menuStr.append("<span class=\"arrow\"></span>");
						}
						menuStr.append("</a>");
					} else {
						menuStr.append("<li class=\"title\" >" + "<a href=\"javascript://;\">");
						if (item.getIcon() != null) {
							String iscon = "<i class=\"material-icons icon-menu\">" + item.getIcon() + "</i>";
							menuStr.append(iscon);
						}
						menuStr.append("<span class=\"title\">" + item.getTenChucNang() + "</span>");
						if (item.getChildren().size() > 0) {
							menuStr.append("<span class=\"arrow\"></span>");
						}
						menuStr.append("</a>");
					}

					menuChild(item.getChildren(), menuStr, 1);

					menuStr.append("</li>");
					count++;
				}

			}
		}
		return menuStr.toString();
	}

	public static void menuChild(List<ChucNangUserDto> trees, StringBuffer menuStr, int depthLevel) {
		if (trees != null && !trees.isEmpty()) {
			menuStr.append("<ul class=\"sub-menu\">");
			for (ChucNangUserDto cnChild : trees.stream().sorted(Comparator.comparing(ChucNangUserDto::getThuTu))
					.collect(Collectors.toList())) {

				if (cnChild.getChildren().size() > 0) {
					// ToanLK: fix menu khi vue route la cac link ngoai he thong
					menuStr.append("<li class=\"title\" >" + "<a href=\\"
							+ (cnChild.getVueRoute().contains(".") ? cnChild.getVueRoute() : "javascript://;")
							+ " style=\"padding-left: " + (depthLevel * 20) + "px \">");
					if (cnChild.getIcon() != null) {
						String iscon = "<i class=\"material-icons icon-menu\">" + cnChild.getIcon() + "</i>";
						menuStr.append(iscon);
					}
					menuStr.append("<span class=\"title\">" + cnChild.getTenChucNang() + "</span>");
					menuStr.append("<span class=\"arrow\"></span></a>");

					menuChild(cnChild.getChildren(), menuStr, depthLevel + 1);
					menuStr.append("</li>");
				} else {
					menuStr.append("<a href=\"javascript://;\"><li>");
					// ToanLK: fix menu khi vue route la cac link ngoai he thong
					if (cnChild.getVueRoute().contains(".")) {
						String subCN = "<a href=\"" + cnChild.getVueRoute()
								+ "\" target=\"_blank\" class=\"nav-link\" style=\"padding-left: " + (depthLevel * 20)
								+ "px; \">";
						if (cnChild.getIcon() != null) {
							subCN += "<i class=\"material-icons icon-menu\">" + cnChild.getIcon() + "</i>";
						}
						subCN += cnChild.getTenChucNang() + "</a>";
						menuStr.append(subCN);
					} else {
						if (cnChild.getIcon() != null) {
							String iscon = "<router-link class=\"nav-link\" to=" + cnChild.getVueRoute()
									+ " style=\"padding-left: " + (depthLevel * 20) + "px \">"
									+ "<i class=\"material-icons icon-menu\">" + cnChild.getIcon() + "</i>" + " "
									+ cnChild.getTenChucNang() + " </router-link>";
							menuStr.append(iscon);
						} else {
							String element = "<router-link class=\"nav-link\" to=" + cnChild.getVueRoute()
									+ " style=\"padding-left: " + (depthLevel * 20) + "px \">"
									+ cnChild.getTenChucNang() + " </router-link>";
							menuStr.append(element);
						}
					}
					menuStr.append("</li></a>");
				}
			}

			menuStr.append("</ul>");
		}
	}

	/**
	 * Get list
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageLkChucNangNhomNguoi List
	 */
	public LkChucNangNhomNguoiBDTO listLkChucNangNhomNguois(String filterkey, Integer pageNo, Integer pageSize,
			String keySort, boolean desc) {

		return null;
	}

	public boolean add(LkChucNangNhomNguoiDTO dto) {

		return true;
	}

	/**
	 * Thá»±c hiá»‡n delete
	 * 
	 * @param nhomNguoiDungId truyá»n vÃ o
	 * @return thÃ nh cÃ´ng thÃ¬ true ngÆ°á»£c láº¡i lÃ  false
	 */
	public boolean deleteLkChucNangNhomNguoi(int nhomNguoiDungId) {

		return true;
	}

	/**
	 * 
	 * }
	 * 
	 * /** Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
//	private String validateParam(String param) {
//
//		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
//				|| "undefined".equals((param + "").toLowerCase()))) {
//			return null;
//		}
//
//		param = (param == null) ? null : param.trim();
//
//		return param;
//	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
//	private String validateParamKeySort(String param) {
//
//		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
//				|| "undefined".equals((param + "").toLowerCase()))) {
//			return "nhomNguoiDungId";
//		}
//
//		param = (param == null) ? null : param.trim();
//
//		return param;
//	}

	public LkChucNangNhomNguoiDTO getByNhomNguoiDungId(int nhomNguoiDungId) {
		if (nhomNguoiDungId > 0) {
			LkChucNangNhomNguoiDTO qtDTO = lkChucNangNhomNguoiDao.findByNhomNguoiDungId(nhomNguoiDungId);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	public boolean updatekChucNangNhomNguoi(LkChucNangNhomNguoiDTO dto) {

		return true;
	}

	public boolean isLkChucNangNhomNguoiExistAdd(int nhomNguoiDungId, int chucNangId) {
		return true;
	}
}
