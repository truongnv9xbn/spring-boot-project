package com.temp.logic.NguoiDung;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.QtNDIpJoinNguoiDungDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpBDTO;
import com.temp.model.NguoiDung.QtNguoiDungIpDTO;
import com.temp.persistence.dao.NguoiDung.QtNguoiDungIpDao;

@Component
public class QtNguoiDungIpLogic {

	@Autowired
	private QtNguoiDungIpDao qtNguoiDungIpDao;

	/**
	 * Get list Nguoi dung ip
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageNguoiDungIp List Nguoi dung ip
	 */
	public QtNguoiDungIpBDTO listNguoiDungIps(String filterkey, Integer nguoiDungId, String ip, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String hoatDong) {

		QtNguoiDungIpBDTO lstPageQtNguoiDungIp = new QtNguoiDungIpBDTO();

		filterkey = validateParam(filterkey);
		ip = validateParam(ip);
		nguoiDungId = validateParamInteger(nguoiDungId);
		keySort = validateParamKeySort(keySort);

		lstPageQtNguoiDungIp = qtNguoiDungIpDao.listQtNguoiDungIps(filterkey, nguoiDungId, ip, pageNo, pageSize,
				keySort, desc, hoatDong);

		return lstPageQtNguoiDungIp;
	}

	/**
	 * Thực hiện thêm mới hoặc update
	 * 
	 * @param NguoiDungIp đối tượng được thêm mới hoặc update
	 * @return thành công thì true ngược lại là false
	 */
	public boolean addOrUpDateNguoiDungIp(QtNguoiDungIpDTO dto) {
		// QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//		if (dto != null) {
//			if (dto.getId() > 0) {
//				QtNguoiDungIpDTO oldQtNguoiDungIp = qtNguoiDungIpDao.findById(dto.getId());
//				if (!StringUtils.isEmpty(dto.getIp())) {
//					oldQtNguoiDungIp.setIp(dto.getIp());
//				}
//				oldQtNguoiDungIp.setNguoiDungId(dto.getNguoiDungId());
//
//				dto.setHoatDong(true);
//				Date curr = new Date();
//				dto.setThoiGianDangNhap(new Timestamp(curr.getTime()));
//
//				// TODO người tạo
//				return qtNguoiDungIpDao.addOrUpdateQtNguoiDungIp(oldQtNguoiDungIp);
//
//			}
//			dto.setNguoiDungId(dto.getNguoiDungId());
//			return qtNguoiDungIpDao.addOrUpdateQtNguoiDungIp(dto);
//		}
//
//		return false;
		if (dto != null) {
			if (dto.getId() > 0) {
				QtNguoiDungIpDTO oldQtNguoiDungIp = qtNguoiDungIpDao.findById(dto.getId());
				oldQtNguoiDungIp.setIp(dto.getIp());
				oldQtNguoiDungIp.setHoatDong(dto.getHoatDong());
				oldQtNguoiDungIp.setNguoiDungId(dto.getNguoiDungId());
				return qtNguoiDungIpDao.addOrUpdateQtNguoiDungIp(oldQtNguoiDungIp);

			}
			return qtNguoiDungIpDao.addOrUpdateQtNguoiDungIp(dto);
		}

		return false;

	}

	public QtNDIpJoinNguoiDungDTO findByJoinNDId(int id) {
		if (id > 0) {
			QtNDIpJoinNguoiDungDTO qtDTO = qtNguoiDungIpDao.findByJoinNDId(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}
	
	public QtNguoiDungIpDTO findById(int id) {
		if (id > 0) {
			QtNguoiDungIpDTO qtDTO = qtNguoiDungIpDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	private Integer validateParamInteger(Integer param) {

		if (param != null) {
			return param;
		}

		param = (param == null) ? null : param;

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = param.trim();

		return param;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = qtNguoiDungIpDao.isExistById(id);

		return result;
	}

	public boolean isExistNguoiDungIp(QtNguoiDungIpDTO dto) {

		boolean result = false;
		result = qtNguoiDungIpDao.isQtExist(dto.getId(), dto.getIp(), dto.getNguoiDungId());
		return result;
	}

	/**
	 * Thực hiện deleteqt người dùng ip
	 * 
	 * @param NganhNgheKdId id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean delete(int nguoiDungIp) {
		if (nguoiDungIp > 0) {
			if (qtNguoiDungIpDao.deleteById(nguoiDungIp)) {
				return true;
			}
			return false;
		}

		return false;
	}
	public boolean deleteByNguoiDungId(int nguoiDungIp) {
		if (nguoiDungIp > 0) {
			if (qtNguoiDungIpDao.deleteByNguoiDungId(nguoiDungIp)) {
				return true;
			}
			return false;
		}

		return false;
	}

}
