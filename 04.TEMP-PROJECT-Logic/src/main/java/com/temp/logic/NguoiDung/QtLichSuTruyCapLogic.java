package com.temp.logic.NguoiDung;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.model.NguoiDung.QtLichSuTruyCapBDTO;
import com.temp.model.NguoiDung.QtLichSuTruyCapDTO;
import com.temp.model.NguoiDung.QtLichSuTruyCapExportDTO;
import com.temp.persistence.dao.NguoiDung.QtLichSuTruyCapDao;
import com.temp.utils.TimestampUtils;

@Component
public class QtLichSuTruyCapLogic {

    @Autowired
    private QtLichSuTruyCapDao lstcDao;

    /**
     * Constants
     */

    /**
     * Get List Lich Su Truy Cap - Logic
     * 
     * @param keysearch
     * @param ipThucHien
     * @param tenTaiKhoan
     * @param tuNgay
     * @param denNgay
     * @param logType
     * @param pageNo
     * @param pageSize
     * @param keySort
     * @param desc
     * @return
     */
    public QtLichSuTruyCapBDTO getListLichSuTruyCap(String keysearch, String ipThucHien,
	    String tenTaiKhoan, String tuNgay, String denNgay, String logType, Integer pageNo,
	    Integer pageSize, String keySort, boolean desc, String loaiNguoiDung, Integer ctckThongTinId) {

	QtLichSuTruyCapBDTO lstcOutputBDTO = new QtLichSuTruyCapBDTO();

	// valid parameter
	keysearch = this.validateParam(keysearch);
	ipThucHien = this.validateParam(ipThucHien);
	tenTaiKhoan = this.validateParam(tenTaiKhoan);
	tuNgay = this.validateParam(tuNgay);
	denNgay = this.validateParam(denNgay);
	logType = this.validateParam(logType);
	loaiNguoiDung = this.validateParam(loaiNguoiDung);
	keySort = this.validateParamKeySort(keySort);

	// Get list lich su truy cap tu DAO
	lstcOutputBDTO = this.lstcDao.getLichSuTruyCapList(keysearch, ipThucHien, tenTaiKhoan,
		tuNgay, denNgay, logType, pageNo, pageSize, keySort, desc, loaiNguoiDung, ctckThongTinId);

	if (lstcOutputBDTO.getLstLichSuTruyCap().size() > 0) {

	    // set ngay dang nhap
	    lstcOutputBDTO.getLstLichSuTruyCap().stream()
		    .filter(f -> f.logType.toLowerCase().equals("login"))
		    .forEach(s -> s.setNgayDangNhap(s.getNgayTao()));

	    // set ngay dang xuat
	    lstcOutputBDTO.getLstLichSuTruyCap().stream()
		    .filter(f -> f.logType.toLowerCase().equals("logout"))
		    .forEach(s -> s.setNgayDangXuat(s.getNgayTao()));
	}

	return lstcOutputBDTO;
    }

    /**
     * Get content for export file
     * 
     * @param lstLichSuTruyCap
     * @param tuNgay
     * @param denNgay
     * @return
     */
    private List<QtLichSuTruyCapExportDTO> getLichSuTruyCapReportData(
	    List<QtLichSuTruyCapDTO> lstLichSuTruyCap, List<QtLichSuTruyCapDTO> lstDistincQltcDate,
	    String tuNgay, String denNgay) throws Exception {

	List<QtLichSuTruyCapExportDTO> lstExportExcelLichSuTruyCap = new ArrayList<QtLichSuTruyCapExportDTO>();
	QtLichSuTruyCapExportDTO exportDataDTO = null;
	
	//lstDistincQltcDate = lstDistincQltcDate.stream().sorted(Comparator.nullsLast((s1, s2) -> s2. ))

	for (QtLichSuTruyCapDTO item : lstDistincQltcDate) {
	    
	    exportDataDTO = new QtLichSuTruyCapExportDTO();
	    exportDataDTO.setNgayHienTaiStr(item.getNgayTaoDate());
	    
	    exportDataDTO.setTongSoTkUBCN((int) lstLichSuTruyCap.stream()
		    .filter(f -> item.ngayTaoDate.equals(f.ngayTaoDate))
		    .filter(s -> !s.loaiNguoiDung).count());

	    exportDataDTO.setTongSoThanhVien((int) lstLichSuTruyCap.stream()
		    .filter(f -> item.ngayTaoDate.equals(f.ngayTaoDate))
		    .filter(s -> s.loaiNguoiDung).count());

	    lstExportExcelLichSuTruyCap.add(exportDataDTO);
	}

	return lstExportExcelLichSuTruyCap;
    }

    /**
     * Get all data for report by Tu ngay - den ngay
     * 
     * @param tuNgay
     * @param denNgay
     * @param keySort
     * @param desc
     * @return
     */
    public QtLichSuTruyCapBDTO getListLichSuTruyCapExport(String tuNgay, String denNgay,
	    String keySort, boolean desc) throws Exception {

	QtLichSuTruyCapBDTO outputExportBDTO = new QtLichSuTruyCapBDTO();
	List<QtLichSuTruyCapDTO> lstDistincQltcDate = new ArrayList<QtLichSuTruyCapDTO>();

	tuNgay = this.validateParam(tuNgay);
	denNgay = this.validateParam(denNgay);
	keySort = this.validateParamKeySort(keySort);

	if (tuNgay != null) {
	    outputExportBDTO.setExportTuNgay(tuNgay);
	}

	if (denNgay != null) {
	    outputExportBDTO.setExportDenNgay(denNgay);
	}
	
	outputExportBDTO = this.lstcDao.getListLichSuTruyCapExport(tuNgay, denNgay, keySort, desc);

	if (outputExportBDTO.getLstLichSuTruyCap().size() > 0) {

	    // loc dl report theo ngay dang xuat
	    outputExportBDTO.getLstLichSuTruyCap().stream().forEach(item -> item
		    .setNgayTaoDate(TimestampUtils.TimestampToString_ddMMyyyy(item.ngayTao)));
	    
	    lstDistincQltcDate = outputExportBDTO.getLstLichSuTruyCap().stream()
		    .collect(Collectors.groupingBy(QtLichSuTruyCapDTO::getNgayTaoDate)).values()
		    .stream().flatMap(g -> g.stream().limit(1)).collect(Collectors.toList());

	    // set data vao list report.
	    outputExportBDTO.setLstExportExcelLichSuTruyCap(this.getLichSuTruyCapReportData(
		    outputExportBDTO.getLstLichSuTruyCap(), lstDistincQltcDate, tuNgay, denNgay));

	}

	return outputExportBDTO;
    }

    /**
     * Validate param
     * 
     * @param param request param from client
     * @return param validated
     */
    private String validateParam(String param) {

	if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
		|| "undefined".equals((param + "").toLowerCase()))) {
	    return null;
	}

	param = (param == null) ? null : param.trim();

	return param;
    }

    /**
     * Validate param
     * 
     * @param param request param from client
     * @return param validated
     */
    private String validateParamKeySort(String param) {

	if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
		|| "undefined".equals((param + "").toLowerCase()))) {
	    return "id";
	}

	param = (param == null) ? null : param.trim();

	return param;
    }

}
