package com.temp.logic.NguoiDung;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.NguoiDung.ChucNangDTO;
import com.temp.model.NguoiDung.NguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.NhomNguoiDungJoinAllDTO;
import com.temp.model.NguoiDung.QtNguoiDungBDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.model.authen.VerifyLoginDto;
import com.temp.model.file.ChucNangUserBDTO;
import com.temp.model.file.ChucNangUserDto;
import com.temp.persistence.dao.NguoiDung.LkChucNangNguoiDao;
import com.temp.persistence.dao.NguoiDung.LkChucNangNhomNguoiDao;
import com.temp.persistence.dao.NguoiDung.QtNguoiDungDao;
import com.temp.utils.ObjectMapperUtils;
import com.temp.utils.PasswordGenerator;

@Component
public class QtNguoiDungLogic {

	@Autowired
	private QtNguoiDungDao QtNguoiDungDao;

	@Autowired
	private LkChucNangNhomNguoiDao lkChucNangNhomNguoiDao;

	@Autowired
	private LkChucNangNguoiDao lkChucNangNguoiDao;

	@Autowired
	private PasswordEncoder bcryptEncoder;

	/**
	 * Tìm kiếm tài khoản người dùng
	 * 
	 * @param userName
	 * @return obj người dùng
	 */
	public QtNguoiDungDTO findByName(String userName) {
		if (userName == null || StringUtils.isEmpty(userName)) {
			return new QtNguoiDungDTO();
		} else {
			return ObjectMapperUtils.map(this.QtNguoiDungDao.findInfoUser(userName), QtNguoiDungDTO.class);
		}
	}

	public boolean addOrUpdate(QtNguoiDungDTO dto) {

		if (dto != null) {
			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
			if (dto.getId() != null && dto.getId() > 0) {
//				boolean xoaLk = lkNguoiDungCtckDao.deleteAllLkFromUserId(dto.getId());
				QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(dto.getId());
				oldQtNguoiDung.setHoTen(dto.getHoTen());
				oldQtNguoiDung.setEmail(dto.getEmail());
				oldQtNguoiDung.setLstIp(dto.getLstIp());
				oldQtNguoiDung.setLstNhomNguoiDung(dto.getLstNhomNguoiDung());
				oldQtNguoiDung.setDiDong(dto.getDiDong());
				oldQtNguoiDung.setGhiChu(dto.getGhiChu());
				oldQtNguoiDung.setTrangThai(dto.getTrangThai());
				oldQtNguoiDung.setNgayHetHan(dto.getNgayHetHan());
				oldQtNguoiDung.setAdmin(dto.getAdmin());
				return QtNguoiDungDao.addOrUpdate(oldQtNguoiDung);
			}
			dto.setTrangThai(dto.getTrangThai());
			dto.setNguoiTao(userInfo.getHoTen());
			dto.setNgayHetHan(dto.getNgayHetHan());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));

			// gennerate MatKhauDefault
			String matKhauDefault = PasswordGenerator.generateRandomPassword();
			dto.setMatKhau(bcryptEncoder.encode(matKhauDefault));
			dto.setMatKhauDefault(matKhauDefault);

			return QtNguoiDungDao.addOrUpdate(dto);
		}
		return false;
	}

//	public boolean addOrUpdate(QtNguoiDungDTO dto) {
//
//		if (dto != null) {
//			QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
//			if (dto.getId() > 0) {
//				QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(dto.getId());
//				oldQtNguoiDung.setHoTen(dto.getHoTen());
//				oldQtNguoiDung.setEmail(dto.getEmail());
//				oldQtNguoiDung.setDiDong(dto.getDiDong());
//				oldQtNguoiDung.setGhiChu(dto.getGhiChu());
//				oldQtNguoiDung.setTrangThai(dto.getTrangThai());
//				oldQtNguoiDung.setChuKySo(dto.getChuKySo());
//				oldQtNguoiDung.setThanhVien(dto.getThanhVien());
//				oldQtNguoiDung.setNgayHetHan(dto.getNgayHetHan());
//				// TODO người tạo
//				return QtNguoiDungDao.addOrUpdate(oldQtNguoiDung);
//
//			}
//			dto.setTrangThai(dto.getTrangThai());
//			dto.setChuKySo(dto.getChuKySo());
//			dto.setThanhVien(dto.getThanhVien());
//			dto.setNguoiTaoId(userInfo.getId());
//			dto.setNgayHetHan(dto.getNgayHetHan());
//			Date curr = new Date();
//			dto.setNgayTao(new Timestamp(curr.getTime()));
//
//			// gennerate MatKhauDefault
//			String matKhauDefault = PasswordGenerator.generateRandomPassword();
//			dto.setMatKhau(bcryptEncoder.encode(matKhauDefault));
//			dto.setMatKhauDefault(matKhauDefault);
//
//			return QtNguoiDungDao.addOrUpdate(dto);
//		}
//		return false;
//	}

	public boolean changePasswordQtNguoiDung(Integer id, String matKhauMoi) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (id > 0) {
			QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(id);
			oldQtNguoiDung.setMatKhau(bcryptEncoder.encode(matKhauMoi));
//			oldQtNguoiDung.setMatKhauDefault(matKhauMoi);
			// TODO người tạo
			return QtNguoiDungDao.changePasswordQtNguoiDung(oldQtNguoiDung);

		}
		return false;
	}

	public boolean changeStatusQtNguoiDung(Integer id, boolean trangThai, boolean thanhVien, boolean chuKySo) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (id > 0) {
			QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(id);
			oldQtNguoiDung.setTrangThai(trangThai);
//			oldQtNguoiDung.setThanhVien(thanhVien);
//			oldQtNguoiDung.setChuKySo(chuKySo);
			// TODO người tạo
			boolean result = QtNguoiDungDao.changePasswordQtNguoiDung(oldQtNguoiDung);
//			if(result == true) {
//					Constant.ISKYSOUSER = oldQtNguoiDung.getChuKySo();
//			}
			return result;

		}
		return false;
	}

	public boolean resetPasswordQtNguoiDung(Integer id) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (id > 0) {
			QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(id);
			String MatKhauDefault = PasswordGenerator.generateRandomPassword();
			oldQtNguoiDung.setMatKhau(bcryptEncoder.encode(oldQtNguoiDung.getMatKhauDefault()));
//			oldQtNguoiDung.setMatKhauDefault(MatKhauDefault);
			// TODO người tạo
			return QtNguoiDungDao.resetPasswordQtNguoiDung(oldQtNguoiDung);

		}
		return false;
	}

	public boolean logoutUserLoginQtNguoiDung(Integer id) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (id > 0) {
			QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(id);
			oldQtNguoiDung.setTokenUser(null);
			// TODO người tạo
			return QtNguoiDungDao.logoutUserLoginQtNguoiDung(oldQtNguoiDung);

		}
		return false;
	}

	public QtNguoiDungDTO getById(int id) {
		if (id > 0) {
			QtNguoiDungDTO qtDTO = QtNguoiDungDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	public boolean isExistByUsername(String username) {
		if (!username.equals(null)) {
			return QtNguoiDungDao.isExistByUsername(username);
		}
		return false;
	}

	public NguoiDungJoinAllDTO findByJoinAllId(int id) {
		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		ChucNangUserBDTO lstChucNang = new ChucNangUserBDTO();
//		BmBaoCaoBDTO lstBmBaoCao = new BmBaoCaoBDTO();
		List<ChucNangUserDto> listTreeShow = new ArrayList<ChucNangUserDto>();
		List<Integer> listIdTicked = new ArrayList<Integer>();
		List<Integer> listBmBaoCaoIdTicked = new ArrayList<Integer>();
		NguoiDungJoinAllDTO resultDto = QtNguoiDungDao.findByJoinAllId(id);
		if (id > 0) {
			if (userInfo.getTaiKhoan().toLowerCase().equals("administrator")) {
				lstChucNang = lkChucNangNhomNguoiDao.menuGenerateAdmin(true);

			} else {
				lstChucNang = lkChucNangNhomNguoiDao.menuGenerate(userInfo.getId(), true);
			}
//			lstBmBaoCao = bmBaoCaoDao.getListPhanQuyen(1);
			if(lstChucNang != null && lstChucNang.getLstChucNangMenu() != null && lstChucNang.getLstChucNangMenu().size() > 0) {
				listTreeShow = toTree(lstChucNang.getLstChucNangMenu());
			}
			// lkChucNangNguoiDao.findChucNangIdInNhom(id)
			// list id ticked
			listIdTicked = lkChucNangNguoiDao.findChucNangIdByNguoiDungId(id);
			//check neu co la con ma la con khong check thi se ignore
			if(listIdTicked != null && listIdTicked.size() > 0) {
				List<Integer> listIdTickedTmp = new ArrayList<>(listIdTicked);
				for(Integer idTicked : listIdTickedTmp) {
					Optional<ChucNangUserDto> lstChild = 
							lstChucNang.getLstChucNangMenu().stream().filter(x->x.getId() == idTicked && x.getChildren() != null && x.getChildren().size() > 0).findAny();
					if(lstChild != null && lstChild.isPresent()) {
						boolean checkChildTicked = false;
						List<ChucNangUserDto> lstTickedChild = lstChild.get().getChildren();
						for(ChucNangUserDto eChild : lstTickedChild) {
							if(listIdTicked.contains(eChild.getId())) {
								checkChildTicked = true;
							}
						}
						if(!checkChildTicked) {
							listIdTicked.remove(idTicked);
						}
					}
				}
			}
//			listBmBaoCaoIdTicked = lkNguoiDungBaoCaoDao.findBmBaoCaoIdByNguoiDungId(id);

			// getChucvu
//			ChucVuDTO chucVuDTO = dmChucVuDao.findById(resultDto.dmChucVuId);

			resultDto.setListTreeShow(listTreeShow);
			resultDto.setListIdTicked(listIdTicked);
//			if (lstBmBaoCao != null) {
//				resultDto.setListTreeBmBaoCaoShow(lstBmBaoCao.lstBmBaoCao);
//			}

			if (resultDto != null) {
				return resultDto;
			}
		}

		return null;
	}
	
	public NhomNguoiDungJoinAllDTO findByJoinAllId2(int id) {
//		BmBaoCaoBDTO lstBmBaoCao = new BmBaoCaoBDTO();
		List<Integer> listIdTicked = new ArrayList<Integer>();
		List<Integer> listBmBaoCaoIdTicked = new ArrayList<Integer>();
		NhomNguoiDungJoinAllDTO resultDto = QtNguoiDungDao.findByJoinAllId2(id);
		if (id > 0) {
			
//			lstBmBaoCao = bmBaoCaoDao.getListPhanQuyen(1);
			
//			listBmBaoCaoIdTicked = lkNguoiDungBaoCaoDao.findBmBaoCaoIdByNhomNguoiDungId(id);

			resultDto.setListIdTicked(listIdTicked);
//			if (lstBmBaoCao != null) {
//				resultDto.setListTreeBmBaoCaoShow(lstBmBaoCao.lstBmBaoCao);
//			}
			resultDto.setListBmBaoCaoIdTicked(listBmBaoCaoIdTicked);

			if (resultDto != null) {
				return resultDto;
			}
		}

		return null;
	}

	/**
	 * check login và save thông tin login
	 * 
	 * @param ipAddress
	 * @param dto
	 * @return
	 */
	public VerifyLoginDto isLoginVerity(String ipAddress, QtNguoiDungDTO dto, boolean isLogin) {
		return QtNguoiDungDao.isLoginVerify(ipAddress, dto, isLogin);
	}

	private List<Integer> convertListStringToListInt(List<String> listStringInput) {

		List<Integer> resultList = listStringInput.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
		return resultList;
	}

	public QtNguoiDungDTO addOrUpDateGetObject(QtNguoiDungDTO dto) {
		// return this.dmQtNguoiDungDao.addOrUpdateQtNguoiDung(dto);

		if (dto != null) {
			if (dto.getId() > 0) {
				QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(dto.getId());
				if (!StringUtils.isEmpty(dto.getMaNguoiDung())) {

				}
				oldQtNguoiDung.setMaNguoiDung(dto.getMaNguoiDung());
				oldQtNguoiDung.setTaiKhoan(dto.getTaiKhoan());
				oldQtNguoiDung.setHoTen(dto.getHoTen());
				oldQtNguoiDung.setEmail(dto.getEmail());
				oldQtNguoiDung.setDiDong(dto.getDiDong());
				oldQtNguoiDung.setGhiChu(dto.getGhiChu());
				oldQtNguoiDung.setAnhDaiDien(dto.getAnhDaiDien());
				oldQtNguoiDung.setTrangThai(dto.getTrangThai());
//				oldQtNguoiDung.setChuKySo(dto.getChuKySo());
//				oldQtNguoiDung.setThanhVien(dto.getThanhVien());
				Date curr = new Date();
//				dto.setNgayHetHan(new Timestamp(curr.getTime()));
				// oldQtNguoiDung.setMatKhauDefault(dto.getMatKhau());
//				oldQtNguoiDung.setMatKhau(bcryptEncoder.encode(dto.getMatKhau()));

				// TODO người tạo
				QtNguoiDungDTO result = new QtNguoiDungDTO();
				result = QtNguoiDungDao.addOrUpdateGetObject(dto);
//				if(result != null) {
//					Constant.ISKYSOUSER = result.getChuKySo();
//				}
				return result;

			}
			dto.setTrangThai(dto.getTrangThai());
//			dto.setChuKySo(dto.getChuKySo());
//			dto.setThanhVien(dto.getThanhVien());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));

			// gennerate MatKhauDefault
			String MatKhauDefault = PasswordGenerator.generateRandomPassword();
			dto.setMatKhau(bcryptEncoder.encode(MatKhauDefault));
			dto.setMatKhauDefault(MatKhauDefault);
			QtNguoiDungDTO result = new QtNguoiDungDTO();
			result = QtNguoiDungDao.addOrUpdateGetObject(dto);
//			if(result != null) {
//				Constant.ISKYSOUSER = result.getChuKySo();
//			}
			return result;
		}

		return new QtNguoiDungDTO();
	}

	public QtNguoiDungDTO RefreshSaveToken(QtNguoiDungDTO dto) {
		// return this.dmQtNguoiDungDao.addOrUpdateQtNguoiDung(dto);

		if (dto != null) {
			if (dto.getId() > 0) {
				QtNguoiDungDTO oldQtNguoiDung = QtNguoiDungDao.findById(dto.getId());

				oldQtNguoiDung.setMaNguoiDung(dto.getMaNguoiDung());
				oldQtNguoiDung.setTaiKhoan(dto.getTaiKhoan());
				oldQtNguoiDung.setHoTen(dto.getHoTen());
				oldQtNguoiDung.setEmail(dto.getEmail());
				oldQtNguoiDung.setDiDong(dto.getDiDong());
				oldQtNguoiDung.setGhiChu(dto.getGhiChu());

				oldQtNguoiDung.setTrangThai(dto.getTrangThai());
//				oldQtNguoiDung.setChuKySo(dto.getChuKySo());
//				oldQtNguoiDung.setThanhVien(dto.getThanhVien());
				Date curr = new Date();
//				dto.setNgayHetHan(new Timestamp(curr.getTime()));
				oldQtNguoiDung.setMatKhauDefault(dto.getMatKhau());
				// oldQtNguoiDung.setMatKhau(bcryptEncoder.encode(dto.getMatKhau()));
				oldQtNguoiDung.setTokenUser(dto.getTokenUser());
				// TODO người tạo
				return QtNguoiDungDao.addOrUpdateGetObject(oldQtNguoiDung);

			}

		}

		return new QtNguoiDungDTO();
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = QtNguoiDungDao.isExistById(id);

		return result;
	}

	/**
	 * Thực hiện delete qt nguoi dung
	 * 
	 * @param QtNguoiDungId qt nguoi dung id truyền vào
	 * @return thành công thì true ngược lại là false
	 */
	public boolean deleteQtNguoiDung(int QtNguoiDungId) {
		if (QtNguoiDungId > 0) {
			if (QtNguoiDungDao.deleteQtNguoiDungById(QtNguoiDungId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	public boolean isExistQtNguoiDung(QtNguoiDungDTO dto) {

		boolean result = false;

		result = QtNguoiDungDao.isQtNguoiDungExist(dto.getId(), dto.getTaiKhoan(), true);
		return result;
	}

	public QtNguoiDungBDTO listQtNguoiDungs(String filterkey, String hoTen, String taiKhoan,
			String sEmail, String sDiDong, String sMaNguoiDung, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String trangThai,Integer nhomNguoiDungId, Integer loaiNguoiDung) {

		QtNguoiDungBDTO lstPageQtNguoiDung = new QtNguoiDungBDTO();

		filterkey = validateParam(filterkey);
		hoTen = validateParam(hoTen);
		taiKhoan = validateParam(taiKhoan);
		sEmail = validateParam(sEmail);
		sDiDong = validateParam(sDiDong);
		sMaNguoiDung = validateParam(sMaNguoiDung);
		keySort = validateParamKeySort(keySort);

		lstPageQtNguoiDung = QtNguoiDungDao.listNguoiDungs(filterkey, hoTen, taiKhoan, sEmail, sDiDong,
				sMaNguoiDung, pageNo, pageSize, keySort, desc, trangThai, nhomNguoiDungId, loaiNguoiDung);

		return lstPageQtNguoiDung;
	}

	public QtNguoiDungBDTO listQtNguoiDungLogins(String filterkey, Integer dmChucVuId, String hoTen, String taiKhoan,
			String sEmail, String sDiDong, String sMaNguoiDung, Integer pageNo, Integer pageSize, String keySort,
			boolean desc, String trangThai) {

		QtNguoiDungBDTO lstPageQtNguoiDung = new QtNguoiDungBDTO();

		filterkey = validateParam(filterkey);
		hoTen = validateParam(hoTen);
		taiKhoan = validateParam(taiKhoan);
		sEmail = validateParam(sEmail);
		sDiDong = validateParam(sDiDong);
		sMaNguoiDung = validateParam(sMaNguoiDung);
		keySort = validateParamKeySort(keySort);

		lstPageQtNguoiDung = QtNguoiDungDao.listNguoiDungLogins(filterkey, dmChucVuId, hoTen, taiKhoan, sEmail, sDiDong,
				sMaNguoiDung, pageNo, pageSize, keySort, desc, trangThai);

		return lstPageQtNguoiDung;
	}

	public QtNguoiDungBDTO listAllNguoiDung(String keySort, boolean desc, String trangThai) {

		QtNguoiDungBDTO lstPageQtNguoiDung = new QtNguoiDungBDTO();
		keySort = validateParamKeySort(keySort);

		lstPageQtNguoiDung = QtNguoiDungDao.listAllNguoiDung(keySort, desc, trangThai);

		return lstPageQtNguoiDung;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	private List<ChucNangDTO> setDisableListChucNang(List<ChucNangDTO> listChucNangIn,
			List<Integer> listChucNangIdNhom) {

		for (ChucNangDTO dto : listChucNangIn) {
			if (listChucNangIdNhom.contains(dto.getId())) {
				dto.setDisabled(true);
			}

		}

		return getTree(listChucNangIn);
	}

	public static List<ChucNangDTO> getTree(List<ChucNangDTO> list) {
		Map<Integer, ChucNangDTO> map = new HashMap<>();
		List<ChucNangDTO> result = new ArrayList<ChucNangDTO>();

		for (ChucNangDTO node : list) {
			map.put(node.getId(), node);
		}

		for (int id : map.keySet()) {
			ChucNangDTO current = map.get(id);
			ChucNangDTO parrent = map.get(current.getKhoaChaId());
			if (parrent != null) {
				current.setHeader("generic");
				parrent.getChildren().add(current);
			} else {
				current.setHeader("root");
				current.setKhoaChaId(0);
				result.add(current);
			}
		}
		return result;
	}

	public static List<ChucNangUserDto> toTree(List<ChucNangUserDto> list) {
		Map<Integer, ChucNangUserDto> map = new HashMap<>();
		List<ChucNangUserDto> result = new ArrayList<ChucNangUserDto>();

		for (ChucNangUserDto node : list) {
			map.put(node.getId(), node);
		}

		for (int id : map.keySet()) {
			ChucNangUserDto current = map.get(id);
			ChucNangUserDto parrent = map.get(current.getKhoaChaId());
			if (parrent != null) {
				current.setHeader("generic");
				parrent.getChildren().add(current);
			} else {
				current.setHeader("root");
				current.setKhoaChaId(0);
				result.add(current);
			}
		}
		return result;
	}

	public boolean phanQuyenChucNang(NguoiDungJoinAllDTO dto) {

		return lkChucNangNguoiDao.phanQuyenChucNang(dto);
	}
	
	public boolean isExistByMa(String maNguoiDung, Integer id) {
		boolean result = false;

		result = QtNguoiDungDao.isExistByMa(maNguoiDung, id);

		return result;
	}

}
