package com.temp.logic.DanhMuc;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DanhMuc.PhuongXaBDTO;
import com.temp.model.DanhMuc.PhuongXaDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.DanhMuc.PhuongXaDao;

@Component
public class PhuongXaLogic {

	@Autowired
	private PhuongXaDao PhuongXaDao;

	/**
	 * ThÃ¡Â»Â±c hiÃ¡Â»â€¡n delete TÃ¡Â»â€°nh thÃƒÂ nh
	 * 
	 * @param PhuongXaId TÃ¡Â»â€°nh thÃƒÂ nh id truyÃ¡Â»Ân vÃƒÂ o
	 * @return thÃƒÂ nh cÃƒÂ´ng thÃƒÂ¬ true ngÃ†Â°Ã¡Â»Â£c lÃ¡ÂºÂ¡i lÃƒÂ  false
	 */
	public boolean deletePhuongXa(int PhuongXaId) {
		if (PhuongXaId > 0) {
			if (PhuongXaDao.deletePhuongXaById(PhuongXaId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPagePhuongXa List
	 */
	public PhuongXaBDTO listPhuongXas(String strfilter, String sTenPhuongXa, String sMaPhuongXa, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai
			,String sTenTinhThanh,String sTenQuanHuyen) {

		PhuongXaBDTO lstPagePhuongXa = new PhuongXaBDTO();

		strfilter = validateParam(strfilter);
		sTenPhuongXa = validateParam(sTenPhuongXa);
		sMaPhuongXa = validateParam(sMaPhuongXa);
		sTenTinhThanh = validateParam(sTenTinhThanh);
		sTenQuanHuyen = validateParam(sTenQuanHuyen);
		keySort = validateParamKeySort(keySort);
		
		lstPagePhuongXa = PhuongXaDao.listPhuongXas(strfilter, sTenPhuongXa, sMaPhuongXa, pageNo, pageSize, keySort, desc, trangThai, sTenTinhThanh, sTenQuanHuyen);

		return lstPagePhuongXa;
	}
	/**
	 * Lấy tất cả dữ liệu
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	public PhuongXaBDTO getListByTTandQH(String keySort, boolean desc, String trangThai,Integer tinhThanhId,Integer quanHuyenId) {
		PhuongXaBDTO lst = new PhuongXaBDTO();
		lst = PhuongXaDao.getListByTTandQH(keySort, desc, trangThai, tinhThanhId, quanHuyenId);
		return lst;
	}

	public boolean addOrUpDatePhuongXa(PhuongXaDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				PhuongXaDTO oldPhuongXa = PhuongXaDao.findById(dto.getId());
				oldPhuongXa.setTenPhuongXa(dto.getTenPhuongXa());
				oldPhuongXa.setMaPhuongXa(dto.getMaPhuongXa());
				oldPhuongXa.setGhiChu(dto.getGhiChu());
				oldPhuongXa.setTrangThai(dto.getTrangThai());
				oldPhuongXa.setTinhThanhId(dto.getTinhThanhId());
				oldPhuongXa.setQuanHuyenId(dto.getQuanHuyenId());
				return PhuongXaDao.addOrUpdatePhuongXa(oldPhuongXa);

			}
			dto.setNguoiTaoId(userInfo.getId());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return PhuongXaDao.addOrUpdatePhuongXa(dto);
		}

		return false;

	}

	public PhuongXaDTO getById(int id) {
		if (id > 0) {
			PhuongXaDTO qtDTO = PhuongXaDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistPhuongXaUpdate(PhuongXaDTO dto) {

		boolean result = false;
		result = PhuongXaDao.isPhuongXaExistUpdate(dto.getId(), dto.getMaPhuongXa());

		return result;
	}

	public boolean isExistPhuongXaAdd(PhuongXaDTO dto) {

		boolean result = false;
		result = PhuongXaDao.isPhuongXaExistAdd(dto.getMaPhuongXa());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = PhuongXaDao.isExistById(id);

		return result;
	}
}
