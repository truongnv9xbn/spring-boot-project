package com.temp.logic.DanhMuc;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DropDownDTO;
import com.temp.model.DanhMuc.QuocTichBDTO;
import com.temp.model.DanhMuc.QuocTichDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.DanhMuc.DmQuocTichDao;

@Component // đây là 1 bean
public class QuocTichLogic {

	@Autowired
	private DmQuocTichDao dmQuocTichDao;

	public boolean deleteQuocTich(int quocTichId) { // tạo hàm delete theo id
		if (quocTichId > 0) { // nếu id >0
			if (dmQuocTichDao.deleteQuocTichById(quocTichId)) { // gọi đến DB xóa theo id
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list Quoc Tich
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQuocTich List Quoc Tich
	 */
	public QuocTichBDTO listQuocTichs(String filterkey, String sTenQuocTich, String sMaQuocTich, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		QuocTichBDTO lstPageQuocTich = new QuocTichBDTO(); // tạo đối tượng

		filterkey = validateParam(filterkey); // filterkey gọi đến hàm validateParam với tham số filterkey
		sTenQuocTich = validateParam(sTenQuocTich); // sTenQuocTich gọi đến hàm validateParam với tham số sTenQuocTich
		sMaQuocTich = validateParam(sMaQuocTich); // sMaQuocTich gọi đến hàm validateParam với tham số sMaQuocTich
		keySort = validateParamKeySort(keySort); // keySort gọi đến hàm validateParam với tham số keySort

		lstPageQuocTich = dmQuocTichDao.listQuocTichs(filterkey, sTenQuocTich, sMaQuocTich, pageNo, pageSize, keySort,
				desc, trangThai); // list = gọi đến dao với các pram tương ứng

		return lstPageQuocTich; // trả về list
	}

	/**
	 * Lấy tất cả dữ liệu
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	public QuocTichBDTO listAll(String keySort, boolean desc, String trangThai) {
		QuocTichBDTO lst = new QuocTichBDTO();
		lst = dmQuocTichDao.listAll(keySort, desc, trangThai);
		return lst;
	}

	public boolean addOrUpDateQuocTich(QuocTichDTO dto) { // thêm mới và sửa

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor(); // lấy thông tin quản trị người dùng
		if (dto != null) { // thêm mới
			if (dto.getId() > 0) { // cập nhật
				QuocTichDTO oldQuocTich = dmQuocTichDao.findById(dto.getId());
				oldQuocTich.setTenQuocTich(dto.getTenQuocTich());
				oldQuocTich.setMaQuocTich(dto.getMaQuocTich());
				oldQuocTich.setGhiChu(dto.getGhiChu());
				oldQuocTich.setTrangThai(dto.getTrangThai());
				return dmQuocTichDao.addOrUpdateQuocTich(oldQuocTich); // lưu DB

			}
			dto.setNguoiTaoId(userInfo.getId());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime())); // conver Timestamp Date
			return dmQuocTichDao.addOrUpdateQuocTich(dto); // lưu DB
		}

		return false;

	}

	public QuocTichDTO getById(int id) { // Tìm đối tượng theo id
		if (id > 0) { // nếu id >0
			QuocTichDTO qtDTO = dmQuocTichDao.findById(id); // check db để tìm
			if (qtDTO != null) { // nếu có khác null thì trả về đối tượng còn ngược lại là null
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) { // check param

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) { // check params

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistQuocTichUpdate(QuocTichDTO dto) { // check update

		boolean result = false; // tạo 1 bolean
		result = dmQuocTichDao.isQuocTichExistUpdate(dto.getId(), dto.getMaQuocTich()); // gọi đến DB lấy 2 giá trị id
																						// và mã

		return result;
	}

	public boolean isExistQuocTichAdd(QuocTichDTO dto) { // check add

		boolean result = false; // tạo 1 bolean
		result = dmQuocTichDao.isQuocTichExistAdd(dto.getMaQuocTich()); // gọi đến DB lấy giá trị mã

		return result;
	}

	public boolean isExistById(int id) { // check id

		boolean result = false; // tạo 1 bolean

		result = dmQuocTichDao.isExistById(id); // check id ở BD

		return result;
	}

	public List<DropDownDTO> dropdownQuocTich() { // check idx

		return dmQuocTichDao.DropDownQuocTich();
	}

	public int getIdByName(String name) {
		int a = 0;
		if (name != null && name != "") {
			a = dmQuocTichDao.getIdByName(name);
			if(a > 0) {
				return a;
			}
		}
		return a;
	}

}
