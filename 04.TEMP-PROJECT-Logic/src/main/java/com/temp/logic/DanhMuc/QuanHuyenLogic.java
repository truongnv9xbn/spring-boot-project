package com.temp.logic.DanhMuc;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DanhMuc.QuanHuyenBDTO;
import com.temp.model.DanhMuc.QuanHuyenDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.DanhMuc.QuanHuyenDao;

@Component
public class QuanHuyenLogic {

	@Autowired
	private QuanHuyenDao QuanHuyenDao;

	/**
	 * ThÃ¡Â»Â±c hiÃ¡Â»â€¡n delete TÃ¡Â»â€°nh thÃƒÂ nh
	 * 
	 * @param QuanHuyenId TÃ¡Â»â€°nh thÃƒÂ nh id truyÃ¡Â»Ân vÃƒÂ o
	 * @return thÃƒÂ nh cÃƒÂ´ng thÃƒÂ¬ true ngÃ†Â°Ã¡Â»Â£c lÃ¡ÂºÂ¡i lÃƒÂ  false
	 */
	public boolean deleteQuanHuyen(int QuanHuyenId) {
		if (QuanHuyenId > 0) {
			if (QuanHuyenDao.deleteQuanHuyenById(QuanHuyenId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageQuanHuyen List
	 */
	public QuanHuyenBDTO listQuanHuyens(String filterkey, String sTenQuanHuyen, String sMaQuanHuyen, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai,String sTenTinhThanh) {

		QuanHuyenBDTO lstPageQuanHuyen = new QuanHuyenBDTO();

		filterkey = validateParam(filterkey);
		sTenQuanHuyen = validateParam(sTenQuanHuyen);
		sMaQuanHuyen = validateParam(sMaQuanHuyen);
		keySort = validateParamKeySort(keySort);

		lstPageQuanHuyen = QuanHuyenDao.listQuanHuyens(filterkey, sTenQuanHuyen, sMaQuanHuyen, pageNo, pageSize,
				keySort, desc, trangThai,sTenTinhThanh);

		return lstPageQuanHuyen;
	}
	/**
	 * Lấy tất cả dữ liệu
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	public QuanHuyenBDTO getListByTinhThanhID(String keySort, boolean desc, String trangThai,Integer tinhThanhId) {
		QuanHuyenBDTO lst = new QuanHuyenBDTO();
		lst = QuanHuyenDao.getListByTinhThanhID(keySort, desc, trangThai,tinhThanhId);
		return lst;
	}

	public boolean addOrUpDateQuanHuyen(QuanHuyenDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				QuanHuyenDTO oldQuanHuyen = QuanHuyenDao.findById(dto.getId());
				oldQuanHuyen.setTenQuanHuyen(dto.getTenQuanHuyen());
				oldQuanHuyen.setMaQuanHuyen(dto.getMaQuanHuyen());
				oldQuanHuyen.setGhiChu(dto.getGhiChu());
				oldQuanHuyen.setTrangThai(dto.getTrangThai());
				oldQuanHuyen.setTinhThanhId(dto.getTinhThanhId());
				return QuanHuyenDao.addOrUpdateQuanHuyen(oldQuanHuyen);

			}
			dto.setNguoiTaoId(userInfo.getId());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return QuanHuyenDao.addOrUpdateQuanHuyen(dto);
		}

		return false;

	}

	public QuanHuyenDTO getById(int id) {
		if (id > 0) {
			QuanHuyenDTO qtDTO = QuanHuyenDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistQuanHuyenUpdate(QuanHuyenDTO dto) {

		boolean result = false;
		result = QuanHuyenDao.isQuanHuyenExistUpdate(dto.getId(), dto.getMaQuanHuyen());

		return result;
	}

	public boolean isExistQuanHuyenAdd(QuanHuyenDTO dto) {

		boolean result = false;
		result = QuanHuyenDao.isQuanHuyenExistAdd(dto.getMaQuanHuyen());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = QuanHuyenDao.isExistById(id);

		return result;
	}
}
