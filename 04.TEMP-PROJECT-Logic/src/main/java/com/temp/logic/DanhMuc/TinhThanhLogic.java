package com.temp.logic.DanhMuc;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.temp.global.UserInfoGlobal;
import com.temp.model.DanhMuc.TinhThanhBDTO;
import com.temp.model.DanhMuc.TinhThanhDTO;
import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.persistence.dao.DanhMuc.DmTinhThanhDao;

@Component
public class TinhThanhLogic {

	@Autowired
	private DmTinhThanhDao dmTinhThanhDao;

	/**
	 * ThÃ¡Â»Â±c hiÃ¡Â»â€¡n delete TÃ¡Â»â€°nh thÃƒÂ nh
	 * 
	 * @param TinhThanhId TÃ¡Â»â€°nh thÃƒÂ nh id truyÃ¡Â»Ân vÃƒÂ o
	 * @return thÃƒÂ nh cÃƒÂ´ng thÃƒÂ¬ true ngÃ†Â°Ã¡Â»Â£c lÃ¡ÂºÂ¡i lÃƒÂ  false
	 */
	public boolean deleteTinhThanh(int TinhThanhId) {
		if (TinhThanhId > 0) {
			if (dmTinhThanhDao.deleteTinhThanhById(TinhThanhId)) {
				return true;
			}
			return false;
		}

		return false;
	}

	/**
	 * Get list
	 * 
	 * @param filterkey is key search
	 * @param pageNo    page Number
	 * @param pageSize  size of page
	 * @param keySort   column sort
	 * @param desc      type sort true--> desc, false asc
	 * 
	 * @return lstPageTinhThanh List
	 */
	public TinhThanhBDTO listTinhThanhs(String filterkey, String sTenTinhThanh, String sMaTinhThanh, Integer pageNo,
			Integer pageSize, String keySort, boolean desc, String trangThai) {

		TinhThanhBDTO lstPageTinhThanh = new TinhThanhBDTO();

		filterkey = validateParam(filterkey);
		sTenTinhThanh = validateParam(sTenTinhThanh);
		sMaTinhThanh = validateParam(sMaTinhThanh);
		keySort = validateParamKeySort(keySort);

		lstPageTinhThanh = dmTinhThanhDao.listTinhThanhs(filterkey, sTenTinhThanh, sMaTinhThanh, pageNo, pageSize,
				keySort, desc, trangThai);

		return lstPageTinhThanh;
	}
	/**
	 * Lấy tất cả dữ liệu
	 * 
	 * @param keySort
	 * @param desc
	 * @param trangThai
	 * @return
	 */
	public TinhThanhBDTO listAll(String keySort, boolean desc, String trangThai) {
		TinhThanhBDTO lst = new TinhThanhBDTO();
		lst = dmTinhThanhDao.listAll(keySort, desc, trangThai);
		return lst;
	}

	public boolean addOrUpDateTinhThanh(TinhThanhDTO dto) {

		QtNguoiDungDTO userInfo = UserInfoGlobal.getUserInfoAuthor();
		if (dto != null) {
			if (dto.getId() > 0) {
				TinhThanhDTO oldTinhThanh = dmTinhThanhDao.findById(dto.getId());
				oldTinhThanh.setTenTinhThanh(dto.getTenTinhThanh());
				oldTinhThanh.setMaTinhThanh(dto.getMaTinhThanh());
				oldTinhThanh.setGhiChu(dto.getGhiChu());
				oldTinhThanh.setTrangThai(dto.getTrangThai());
				return dmTinhThanhDao.addOrUpdateTinhThanh(oldTinhThanh);

			}
			dto.setNguoiTaoId(userInfo.getId());
			Date curr = new Date();
			dto.setNgayTao(new Timestamp(curr.getTime()));
			return dmTinhThanhDao.addOrUpdateTinhThanh(dto);
		}

		return false;

	}

	public TinhThanhDTO getById(int id) {
		if (id > 0) {
			TinhThanhDTO qtDTO = dmTinhThanhDao.findById(id);
			if (qtDTO != null) {
				return qtDTO;
			}
		}

		return null;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParam(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return null;
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	/**
	 * Validate param
	 * 
	 * @param param request param from client
	 * @return param validated
	 */
	private String validateParamKeySort(String param) {

		if (param != null && (param.isEmpty() || "null".equals((param + "").toLowerCase())
				|| "undefined".equals((param + "").toLowerCase()))) {
			return "id";
		}

		param = (param == null) ? null : param.trim();

		return param;
	}

	public boolean isExistTinhThanhUpdate(TinhThanhDTO dto) {

		boolean result = false;
		result = dmTinhThanhDao.isTinhThanhExistUpdate(dto.getId(), dto.getMaTinhThanh());

		return result;
	}

	public boolean isExistTinhThanhAdd(TinhThanhDTO dto) {

		boolean result = false;
		result = dmTinhThanhDao.isTinhThanhExistAdd(dto.getMaTinhThanh());

		return result;
	}

	public boolean isExistById(int id) {

		boolean result = false;

		result = dmTinhThanhDao.isExistById(id);

		return result;
	}
}
