package com.temp.logic;
//package com.temp.lienthongLogic;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import com.example.DTO_LienThong.CongTyLtDTO;
//import com.example.DTO_LienThong.HeaderDTO;
//import com.example.DTO_LienThong.MSG;
//import com.example.DTO_LienThong.NguoiHanhNgheXmlBDTO;
//import com.example.DTO_LienThong.ObjectCongTyXMLDTO;
//
//import com.temp.global.IBMQueuGlobal;
//import com.temp.global.ThamSoHeThongGlobal;
//import com.temp.persistance.daolienthong.SinhDuLieuDao;
//import com.temp.utils.Constant;
//import com.temp.utils.TimestampUtils;
//import com.temp.utils.Utils;
//import com.temp.utils.UtilsXml;
//
//@Component
//public class SinhDuLieuLogic {
//	@Autowired
//	private SinhDuLieuDao dao;
//
//	public NguoiHanhNgheXmlBDTO SyncNHN(NguoiHanhNgheXmlBDTO bdto) {
//		// TODO Auto-generated method stub
//		try {
//			dao.syncNHN(bdto);
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//		return bdto;
//	}
//
//	public MSG sendNotificationCongTy() {
//		// TODO Auto-generated method stub
//		List<CongTyLtDTO> congTyLtDTO = new ArrayList<>();
//		ObjectCongTyXMLDTO congTyXMLDTO = new ObjectCongTyXMLDTO();
//		HeaderDTO headerDTO = new HeaderDTO("1", TimestampUtils.getDateCurrentTypeString_ddMMMyyy());
//		try {
//			congTyLtDTO = dao.getlstCongTyLt();
//			congTyXMLDTO.setBody(congTyLtDTO);
//			congTyXMLDTO.setHeader(headerDTO);
//
//			String stringXmlCongTy = UtilsXml.jaxbObjectToXML(congTyXMLDTO);
//
//			if (stringXmlCongTy != null && stringXmlCongTy.length() > 0) {
////				//Utils.createFileXML("hoant2", stringXmlCongTy, "congTyXMLDTO.xml");
//				return IBMQueuGlobal.sendIBMQueu(stringXmlCongTy,
//						ThamSoHeThongGlobal.giaTriThamSo(Constant.IDS_CONGTY).getGiaTri());
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
//		return new MSG("Thất bại", "Không có dữ liệu công ty");
//	}
//
//	public String sendNotificationBaoCao(String pKyBaoCao, String pLoaiBaoCao, String pMaSoThue) {
////		String xml = dao.getlstBaoCaoLt(pKyBaoCao, pLoaiBaoCao, pMaSoThue);
//		return dao.getlstBaoCaoLt(pKyBaoCao, pLoaiBaoCao, pMaSoThue);
//	}
//
//	public MSG sendNotificationBaoCao1(String pKyBaoCao, String pLoaiBaoCao, String pMaSoThue) {
//		// TODO Auto-generated method stub
//		MSG msg = new MSG();
//		String IDS = ThamSoHeThongGlobal.giaTriThamSo(Constant.IDS_BAOCAO).getGiaTri();
//		String xml = dao.getlstBaoCaoLt(pKyBaoCao, pLoaiBaoCao, pMaSoThue);
//		if (IDS != null && IDS != "") {
//			if (xml.contains("<ROW>")) {
//				msg = IBMQueuGlobal.sendIBMQueu(xml, IDS);
//				if (msg.getMsg().equals("Thành công")) {
//					msg.setError("false");
//					msg.setMsg("Trả dữ liệu thành công");
//					msg.setXML(xml);
//				} else {
//					msg.setError("true");
//					msg.setMsg("Có lỗi khi lấy dữ liệu hoặc không có dữ liệu");
//					msg.setXML("");
//				}
//			}
//		}
//		return msg;
//	}
//
////		return dao.getlstBaoCaoLt(pKyBaoCao, pLoaiBaoCao, pMaSoThue);
////		List<BaoCaoLtDTO> baoCaoLtDTO = new ArrayList<>();
////		ObjectBaoCaoXMLDTO baoCaoXMLDTO = new ObjectBaoCaoXMLDTO();
////		HeaderDTO headerDTO = new HeaderDTO("0", TimestampUtils.getDateCurrentTypeString_ddMMMyyy());
////		try {
////			baoCaoLtDTO = dao.getlstBaoCaoLt(pKyBaoCao, pLoaiBaoCao, pMaSoThue);
////			baoCaoXMLDTO.setBody(baoCaoLtDTO);
////			baoCaoXMLDTO.setHeader(headerDTO);
////
////			String stringXmlBaoCao = UtilsXml.jaxbObjectToXML(baoCaoXMLDTO);
////
////			if (stringXmlBaoCao != null && stringXmlBaoCao.length() > 0) {
//////				Utils.createFileXML("DarkLord", stringXmlBaoCao, "baoCaoXMLDTO.xml");
//////				return new MSG("Thành công", "200", stringXmlBaoCao);// IBMQueuUtils.sendIBMQueu(stringXmlBaoCao,ThamSoHeThongGlobal.giaTriThamSo(Constant.IDS_BAOCAO).getGiaTri());
////				return baoCaoXMLDTO;
////			}
////		} catch (Exception e) {
////			// TODO: handle exception
////			e.printStackTrace();
////		}
////		return null;
////		return new MSG("Thất bại", "Không có dữ liệu báo cáo");
//
//	public String sendNotificationCongTy(Integer rowPerpage, String maSoThue) {
//		MSG msg = new MSG();
//		String message = "";
//		String IDS = ThamSoHeThongGlobal.giaTriThamSo(Constant.IDS_CONGTY).getGiaTri();
//		String xml = dao.getlstCongTys(rowPerpage, maSoThue);
//		if (IDS != null && IDS != "") {
//			if (xml.contains("<ROW>")) {
//				msg = IBMQueuGlobal.sendIBMQueu(xml, IDS);
//				message = msg.getMsg();
//			} else {
//				message = "Không có dữ liệu";
//			}
//		} else {
//			message = "Thất bại";
//		}
//		return message;
//	}
//
////	public BaoCaoParamsDTO getReport(BaoCaoParamsDTO params) {
////		return dao.getReport(params);
////	}
//}
