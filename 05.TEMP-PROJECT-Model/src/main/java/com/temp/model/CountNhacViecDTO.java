package com.temp.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Getter
@Setter
public class CountNhacViecDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer toiHanChuaGui;
	private Integer quaHanChuaGui;
	private Integer biHuy;
	private Integer viPham;
	private Integer thongBao;

	public CountNhacViecDTO() {
	}

	public CountNhacViecDTO(Object... fields) {
		super();
		toiHanChuaGui = ((BigDecimal) fields[0]).intValue();
		quaHanChuaGui = ((BigDecimal) fields[1]).intValue();
		biHuy = ((BigDecimal) fields[2]).intValue();
		viPham = ((BigDecimal) fields[3]).intValue();
		if(fields.length > 4) {
			thongBao = ((BigDecimal) fields[4]).intValue();
		}
	}
}