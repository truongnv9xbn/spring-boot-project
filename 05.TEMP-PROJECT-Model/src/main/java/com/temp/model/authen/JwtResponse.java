package com.temp.model.authen;

import java.io.Serializable;
import java.util.List;

public class JwtResponse implements Serializable {

	private static final long serialVersionUID = -8091879091924046844L;
	private final String jwttoken;

	private String ipAccess;

	private final List<String> lstFunc;

	private final UserInfoAuthorDTO userInfo;

	private final String menuGennerate;

	public JwtResponse(String jwttoken, List<String> lstFunc, UserInfoAuthorDTO userInfo, String menuGennerate,
			String ipAccess) {
		this.jwttoken = jwttoken;
		this.lstFunc = lstFunc;
		this.userInfo = userInfo;
		this.menuGennerate = menuGennerate;
		this.ipAccess = ipAccess;
	}

	public List<String> getLstFunc() {
		return lstFunc;
	}

	public String getJwttoken() {
		return jwttoken;
	}

	public UserInfoAuthorDTO getUserInfo() {
		return userInfo;
	}

	public String getMenuGennerate() {
		return menuGennerate;
	}

	public String getIpAccess() {
		return ipAccess;
	}

//	public JwtResponse() {
//
//	}
}