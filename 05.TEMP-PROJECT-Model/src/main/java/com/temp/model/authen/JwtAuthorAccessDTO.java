/**
 * 
 */
package com.temp.model.authen;

import lombok.Getter;
import lombok.Setter;

/**
 * @author thangnn
 *
 */

public class JwtAuthorAccessDTO {
	@Getter
	@Setter
	public String TenChucNang;
	@Getter
	@Setter
	public String URLApi;
	
	public JwtAuthorAccessDTO() {
		
	}
}
