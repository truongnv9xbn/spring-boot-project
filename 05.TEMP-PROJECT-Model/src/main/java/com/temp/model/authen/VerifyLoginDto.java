/**
 * 
 */
package com.temp.model.authen;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class VerifyLoginDto {

	public boolean returnVerify;

	public String ipAddress;

}
