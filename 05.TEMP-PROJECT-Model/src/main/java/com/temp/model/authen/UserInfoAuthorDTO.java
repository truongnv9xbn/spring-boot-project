/**
 * 
 */
package com.temp.model.authen;

import com.temp.model.Common.ThamSoHeThongBDTO;

import lombok.Getter;
import lombok.Setter;

/**
 * @author thangnn
 *
 */

public class UserInfoAuthorDTO {

	@Getter
	@Setter
	public int id;
	
	@Getter
	@Setter
	public String hoTen;

	@Getter
	@Setter
	public String maNguoiDung;

	@Getter
	@Setter
	public boolean trangThai;
	@Getter
	@Setter
	public String email;

	@Getter
	@Setter
	public String taiKhoan;
	
	@Getter
	@Setter
	public String hotLine;
	
	@Getter
	@Setter
	public  Boolean admin;
	
//	@Getter
//	@Setter
//	public  Boolean chungThuSo;
	
	@Getter
	@Setter
	public String anhDaiDien;
	
	public UserInfoAuthorDTO(int id, String hoten, String maNguoiDUng, boolean trangThai, String email, String taiKhoan,String hotLine,Boolean admin, String anhDaiDien) {
		this.id = id;
		this.hoTen = hoten;
		this.maNguoiDung = maNguoiDUng;
		this.trangThai = trangThai;
		this.email = email;
		this.taiKhoan = taiKhoan;
		this.hotLine = hotLine;
		this.admin = admin;
		this.anhDaiDien = anhDaiDien;
	}
//	public UserInfoAuthorDTO(int id, String hoten, String maNguoiDUng, boolean trangThai, String email, String taiKhoan,String hotLine,Boolean admin, Boolean chungThuSo, String anhDaiDien) {
//		this.id = id;
//		this.hoTen = hoten;
//		this.maNguoiDung = maNguoiDUng;
//		this.trangThai = trangThai;
//		this.email = email;
//		this.taiKhoan = taiKhoan;
//		this.hotLine = hotLine;
//		this.admin = admin;
//		this.chungThuSo = chungThuSo;
//		this.anhDaiDien = anhDaiDien;
//	}

	public UserInfoAuthorDTO() {

	}
}
