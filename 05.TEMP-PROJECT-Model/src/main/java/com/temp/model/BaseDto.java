package com.temp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseDto implements Serializable {

	private static final long serialVersionUID = -9166579533882709621L;

	private String message;
	private String status;
	

}
