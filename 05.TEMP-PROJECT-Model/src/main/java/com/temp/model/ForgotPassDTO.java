package com.temp.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ForgotPassDTO implements Serializable {

	/**
	 * 
	 */
	private String username ;
	private String email ;
	public ForgotPassDTO() {

	}

}
