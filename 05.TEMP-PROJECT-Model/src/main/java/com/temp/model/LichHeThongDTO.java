package com.temp.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class LichHeThongDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	public int id;

	@Getter
	@Setter
//	@Size(min = 4, message = "Năm nhập vào phải tối thiểu 4 ký tự")
//	@Size(max = 4, message = "Năm nhập vào phải tối đa 4 ký tự")
	public int nam;

	@Getter
	@Setter
	public int tuan;
	
	@Getter
	@Setter
	public Date ngay;
	
	@Getter
	@Setter
	@Size(max = 1000)
	/*@NotEmpty(message = "Không được bỏ trống")*/
	public String ghiChu;

	@Getter
	@Setter
	public Integer nguoiCapNhatId;
	
	@Getter
	@Setter
	public Date ngayCapNhat;


	@Getter
	@Setter
	public Boolean trangThai;

	public LichHeThongDTO() {

	}

}
