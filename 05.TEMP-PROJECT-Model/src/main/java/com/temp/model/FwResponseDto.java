package com.temp.model;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;


public class FwResponseDto implements Serializable {

	private static final long serialVersionUID = -5478113388034244815L;

	public static final Logger logger = LoggerFactory.getLogger(FwResponseDto.class);

	@Setter
	@Getter
	private String message;


	@JsonInclude(Include.NON_NULL)
	private Object object;

	public FwResponseDto() {
		super();
	}

	public FwResponseDto(String message, String timestamp, Object object) {
		super();
		this.message = message;
		this.object = object;
	}

	public FwResponseDto(String message, HttpStatus httpStatus) {
		super();
		this.message = message;
	}

	public FwResponseDto(String message, Object object) {
		this.message = message;
		this.object = object;
	}

	public FwResponseDto(String message, HttpStatus httpStatus, Object object) {
		super();
		this.message = message;
		this.object = object;
	}

}
