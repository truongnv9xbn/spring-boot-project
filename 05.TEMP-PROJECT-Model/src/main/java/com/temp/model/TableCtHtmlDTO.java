package com.temp.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class TableCtHtmlDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private Integer sheetId;
    private String sheetName;
    private String sheetTable;
    private Integer rowNumber;
}
