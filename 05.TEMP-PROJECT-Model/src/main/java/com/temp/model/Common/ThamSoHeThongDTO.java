package com.temp.model.Common;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class ThamSoHeThongDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;

	@Getter
	@Setter
	@Size(max = 10, message = "Phản hệ có kí tự từ 1 đến 10 ký tự")
	private String phanHe;

	@Getter
	@Setter
	@Size(max = 50, message = "Tham số có kí tự từ 1 đến 50 ký tự")
	@NotEmpty(message = "Tham số không được bỏ trống")
	private String thamSo;

	@Getter
	@Setter
	@Size(max = 250, message = "Gía trị có kí tự từ 1 đến 250 ký tự")
	@NotEmpty(message = "Gia Trị Không được bỏ trống")
	private String giaTri;

	@Getter
	@Setter
	@Size(max = 1000, message = " Ghi chú có kí tự từ 1 đến 1000 ký tự")
	private String ghiChu;

	@Getter
	@Setter
	private String nguoiCapNhat;
	
	@Getter
	@Setter
	private Date ngayCapNhat;

	@Getter
	@Setter
	private Boolean trangThai;

	public ThamSoHeThongDTO() {

	}

}
