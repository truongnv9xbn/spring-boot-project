package com.temp.model.Common;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.PageInfo;

public class BaiVietBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<BaiVietDTO> lstBaiViet;

	public BaiVietBDTO() {

	}

}
