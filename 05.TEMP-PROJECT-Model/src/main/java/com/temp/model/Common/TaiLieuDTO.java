package com.temp.model.Common;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class TaiLieuDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;

	@Getter
	@Setter
	@Size(max = 50, message = "Số văn bản nhập không quá 50 ký tự")
	private String soVanBan;

	@Getter
	@Setter
	@Size(max = 2000, message = "Mô tả nhập không quá 2000 ký tự")
	private String trichYeuNoiDung;

	@Getter
	@Setter
	@Size(max = 1000, message = "File đính kèm nhập không quá 1000 ký tự")
	private String fileDinhKem;

	@Getter
	@Setter
	private Date ngayBanHanh;
	
	@Getter
	@Setter
	private String ngayBanHanhStr;

	@Getter
	@Setter
	private Date ngayHieuLuc;
	
	@Getter
	@Setter
	private String ngayHieuLucStr;

	@Getter
	@Setter
	private Date ngayTao;

	@Getter
	@Setter
	private String ngayTaoStr;

	@Getter
	@Setter
	@Size(max = 50, message = "Người tạo nhập không quá 50 ký tự")
	private String nguoiTao;

	@Getter
	@Setter
	private Date ngayCapNhat;

	@Getter
	@Setter
	private String ngayCapNhatStr;

	@Getter
	@Setter
	@Size(max = 50, message = "Người cập nhật nhập không quá 50 ký tự")
	private String nguoiCapNhat;

	@Getter
	@Setter
	private Date ngayDuyet;

	@Getter
	@Setter
	private String ngayDuyetStr;

	@Getter
	@Setter
	@Size(max = 50, message = "Người duyệt nhập không quá 20 ký tự")
	private String nguoiDuyet;
	
	@Getter
	@Setter
	@Size(max = 20, message = "Trạng thái nhập không quá 20 ký tự")
	private String trangThai;

	@Getter
	@Setter
	@Size(max = 1000, message = "Lý do từ chối nhập không quá 1000 ký tự")
	private String lyDoTuChoi;

	public TaiLieuDTO() {

	}

}
