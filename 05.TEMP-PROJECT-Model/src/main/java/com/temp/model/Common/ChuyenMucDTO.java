package com.temp.model.Common;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class ChuyenMucDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;
	
	@Getter
	@Setter
	private int khoaChaId;

	@Getter
	@Setter
	@Size(max = 1000, message = "Tên chuyên mục nhập không quá 1000 ký tự")
	private String tenChuyenMuc;
	
	@Getter
	@Setter
	@Size(max = 2000, message = "Mô tả nhập không quá 2000 ký tự")
	private String moTa;
	
	@Getter
	@Setter
	private String noiDung;

	@Getter
	@Setter
	@Size(max = 2000, message = "Ghi chú nhập không quá 2000 ký tự")
	private String ghiChu;

	@Getter
	@Setter
	private Integer thuTuSapXep;
	
	@Getter
	@Setter
	@Size(max = 100, message = "Icon nhập không quá 100 ký tự")
	private String icon;
	
	@Getter
	@Setter
	@Size(max = 50, message = "Trạng thái nhập không quá 50 ký tự")
	private String trangThai;
	
	@Getter
	@Setter
	private Date ngayTao;
	
	@Getter
	@Setter
	private String ngayTaoStr;
	
	@Getter
	@Setter
	@Size(max = 50, message = "Người tạo nhập không quá 50 ký tự")
	private String nguoiTao;
	
	@Getter
	@Setter
	private Date ngayCapNhat;
	
	@Getter
	@Setter
	private String ngayCapNhatStr;
	
	@Getter
	@Setter
	@Size(max = 50, message = "Người cập nhật nhập không quá 50 ký tự")
	private String nguoiCapNhat;
	
	@Getter
	@Setter
	private Date ngayDuyet;
	
	@Getter
	@Setter
	private String ngayDuyetStr;
	
	@Getter
	@Setter
	@Size(max = 50, message = "Người duyệt nhập không quá 50 ký tự")
	private String nguoiDuyet;
	
	@Getter
	@Setter
	@Size(max = 1000, message = "Lý do từ chối nhập không quá 1000 ký tự")
	private String lyDoTuChoi;

	public ChuyenMucDTO() {

	}

}
