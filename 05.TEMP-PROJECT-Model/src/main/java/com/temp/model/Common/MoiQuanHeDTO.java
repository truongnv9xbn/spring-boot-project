package com.temp.model.Common;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class MoiQuanHeDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;

	@Getter
	@Setter
	@Size(max = 50, message = "Mã mối quan hệ nhập không quá 50 ký tự")
	private String maMoiQuanHe;

	@Getter
	@Setter
	@Size(max = 250, message = "Tên mối quan hệ nhập không quá 250 ký tự")
	@NotEmpty(message = "Không được bỏ trống")
	private String tenMoiQuanHe;

	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú nhập không quá 1000 ký tự")
	private String ghiChu;

	@Getter
	@Setter
	private Integer nguoiTaoId;

	@Getter
	@Setter
	private Date ngayTao;

	@Getter
	@Setter
	private Boolean trangThai;

	public MoiQuanHeDTO() {

	}

}
