package com.temp.model;

import java.io.Serializable;
import java.util.Map;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class DataSettingDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	Map<String, Integer> mapDataSetting;
	Map<Integer, Integer> mapHang;
	Map<Integer, Integer> mapCot;
	
	Map<Integer, Boolean> mapHangDong;
	
	
	public DataSettingDTO() {
		super();
	}
	
	
}