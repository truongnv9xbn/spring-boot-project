/**
 * 
 */
package com.temp.model.cellfunction;

import java.util.List;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class CongThucDTO {
	private List<CongThucTableDTO> cauHinhCt;
	private String congThuc;
	private boolean dungHam;
	private String ham;
	private String cot;
	private Integer sheetBmBaoCaoId;
	private Integer tuHangId;
	private Integer denHangId;

}
