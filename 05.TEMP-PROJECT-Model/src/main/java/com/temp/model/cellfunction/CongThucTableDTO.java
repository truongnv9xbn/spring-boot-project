/**
 * 
 */
package com.temp.model.cellfunction;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class CongThucTableDTO {

	private String heSo;
	private DropDownCT phepToanMot;
	private DropDownCT sheetBmBaoCaoId;
	private DropDownCT cotId;
	private DropDownCT hangId;
	private DropDownCT phepToanHai;
	private DropDownCT chiTieu;
}
