/**
 * 
 */
package com.temp.model.uploadfile;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class UploadPathDTO {

	private String fileName;
	private String pathFile;
	private long size;
	private String fileDownloadUri;
	private String extenstionFile;
}
