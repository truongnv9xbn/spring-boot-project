/**
 * 
 */
package com.temp.model.uploadfile;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class UploadFileDefaultDTO {

	private String nameFile;
	private String path;
	private String pathFile;
	
	
	
	public UploadFileDefaultDTO(String nameFile, String path) {
		super();
		this.nameFile = nameFile;
		this.path = path;
	}
	public UploadFileDefaultDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
