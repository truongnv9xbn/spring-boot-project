/**
 * 
 */
package com.temp.model.uploadfile;

import java.util.List;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class UploadFileResponse {

	private List<UploadPathDTO> lstResUpload;

	public UploadFileResponse(List<UploadPathDTO> lstResUpload) {
		this.lstResUpload = lstResUpload;
	}
}
