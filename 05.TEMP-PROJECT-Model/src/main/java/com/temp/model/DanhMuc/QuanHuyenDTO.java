package com.temp.model.DanhMuc;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.DropDownDTO;
import com.temp.utils.ObjectMapperUtils;

public class QuanHuyenDTO  extends DropDownDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private int id;

	@Getter
	@Setter
	@Size(max = 50, message = "Mã quận huyện nhập không quá 50 ký tự")
	private String maQuanHuyen;


	@Size(max = 250, message = "Tên quận huyện nhập không quá 250 ký tự")
	@NotEmpty(message = "Tên quận huyện không được bỏ trống")
	private String tenQuanHuyen;

	@Getter
	@Setter
	private TinhThanhDTO tinhThanh;
	
	@Getter
	@Setter
	private Integer tinhThanhId;
	
	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú nhập không quá 1000 ký tự")
	private String ghiChu;

	@Getter
	@Setter
	private Integer nguoiTaoId;

	@Getter
	@Setter
	private Date ngayTao;

	@Getter
	@Setter
	private Boolean trangThai;
	
	public QuanHuyenDTO(Object... fields) {
		super();
		TinhThanhDTO tinhThanhDTO = ObjectMapperUtils.map(fields[1],TinhThanhDTO.class);
		this.tinhThanh = tinhThanhDTO;
		
		QuanHuyenDTO quanHuyenDTO = ObjectMapperUtils.map(fields[0],QuanHuyenDTO.class);
		this.id = quanHuyenDTO.getId();
		this.maQuanHuyen = quanHuyenDTO.getMaQuanHuyen();
		this.tenQuanHuyen = quanHuyenDTO.getTenQuanHuyen();
		this.ngayTao =quanHuyenDTO.getNgayTao();
		this.nguoiTaoId = quanHuyenDTO.getNguoiTaoId();
		this.ghiChu = quanHuyenDTO.getGhiChu();
		this.trangThai = quanHuyenDTO.getTrangThai();
	}

	public QuanHuyenDTO() {
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		this.setValue(id);
	}

	public String getTenQuanHuyen() {
		return tenQuanHuyen;
	}

	public void setTenQuanHuyen(String tenQuanHuyen) {
		this.tenQuanHuyen = tenQuanHuyen;
		this.setLabel(tenQuanHuyen);
	}
	
	
}
