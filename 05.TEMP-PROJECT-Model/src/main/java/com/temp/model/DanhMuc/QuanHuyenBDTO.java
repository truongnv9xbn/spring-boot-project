package com.temp.model.DanhMuc;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.PageInfo;

public class QuanHuyenBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<QuanHuyenDTO> lstQuanHuyen;

	public QuanHuyenBDTO() {
		lstQuanHuyen = new ArrayList<>();
	}

}
