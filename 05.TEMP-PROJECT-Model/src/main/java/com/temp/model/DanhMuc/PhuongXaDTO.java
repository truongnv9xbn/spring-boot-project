package com.temp.model.DanhMuc;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.DropDownDTO;
import com.temp.utils.ObjectMapperUtils;

public class PhuongXaDTO  extends DropDownDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;
	
	@Getter
	@Setter
	private TinhThanhDTO tinhThanh;
	
	@Getter
	@Setter
	private Integer tinhThanhId;

	@Getter
	@Setter
	private String tenTinhThanh;
	
	@Getter
	@Setter
	private QuanHuyenDTO quanHuyen;
	
	@Getter
	@Setter
	private Integer quanHuyenId;
	
	@Getter
	@Setter
	private String tenQuanHuyen;
	
	@Getter
	@Setter
	@Size(max = 50, message = "Mã phường xã nhập không quá 50 ký tự")
	private String maPhuongXa;

	

	@Size(max = 250, message = "Tên phường xã nhập không quá 250 ký tự")
	@NotEmpty(message = "Tên phường xã không được bỏ trống")
	private String tenPhuongXa;

	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú nhập không quá 1000 ký tự")
	private String ghiChu;

	@Getter
	@Setter
	private Integer nguoiTaoId;

	@Getter
	@Setter
	private Date ngayTao;

	@Getter
	@Setter
	private Boolean trangThai;
	
	
	
	public PhuongXaDTO(Object... fields) {
		super();
		QuanHuyenDTO quanHuyenDTO = ObjectMapperUtils.map(fields[1],QuanHuyenDTO.class);
		this.quanHuyen = quanHuyenDTO;
		
		TinhThanhDTO tinhThanhDTO = ObjectMapperUtils.map(fields[2],TinhThanhDTO.class);
		this.tinhThanh = tinhThanhDTO;
		
		PhuongXaDTO phuongXaDTO = ObjectMapperUtils.map(fields[0],PhuongXaDTO.class);
		this.id = phuongXaDTO.getId();
		this.maPhuongXa = phuongXaDTO.getMaPhuongXa();
		this.tenPhuongXa = phuongXaDTO.getTenPhuongXa();
		this.tenQuanHuyen = quanHuyenDTO.getTenQuanHuyen();
		this.tenTinhThanh = tinhThanhDTO.getTenTinhThanh();
		this.quanHuyenId = quanHuyenDTO.getId();
		this.tinhThanhId = tinhThanhDTO.getId();
		this.ngayTao = phuongXaDTO.getNgayTao();
		this.nguoiTaoId = phuongXaDTO.getNguoiTaoId();
		this.ghiChu = phuongXaDTO.getGhiChu();
		this.trangThai = phuongXaDTO.getTrangThai();
	}

	public PhuongXaDTO() {
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		this.setValue(id);
	}

	public String getTenPhuongXa() {
		return tenPhuongXa;
	}

	public void setTenPhuongXa(String tenPhuongXa) {
		this.tenPhuongXa = tenPhuongXa;
		this.setLabel(tenPhuongXa);
	}

}
