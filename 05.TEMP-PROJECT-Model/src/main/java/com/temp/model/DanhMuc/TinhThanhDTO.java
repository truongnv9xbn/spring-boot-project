package com.temp.model.DanhMuc;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.DropDownDTO;

public class TinhThanhDTO  extends DropDownDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private int id;

	@Getter
	@Setter
	@Size(max = 50, message = "Mã tỉnh thành nhập không quá 50 ký tự")
	private String maTinhThanh;

	

	@Size(max = 250, message = "Tên tỉnh thành nhập không quá 250 ký tự")
	@NotEmpty(message = "Tên tỉnh thành không được bỏ trống")
	private String tenTinhThanh;

	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú nhập không quá 1000 ký tự")
	private String ghiChu;

	@Getter
	@Setter
	private Integer nguoiTaoId;

	@Getter
	@Setter
	private Date ngayTao;

	@Getter
	@Setter
	private Boolean trangThai;
	
	public TinhThanhDTO(Object... fields) {
		super();
	}

	public TinhThanhDTO() {
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
		this.setValue(id);
	}

	public String getTenTinhThanh() {
		return tenTinhThanh;
	}

	public void setTenTinhThanh(String tenTinhThanh) {
		this.tenTinhThanh = tenTinhThanh;
		this.setLabel(tenTinhThanh);
	}

}
