package com.temp.model.DanhMuc;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

public class QuocTichDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	public int id;

	@Getter
	@Setter
	@Size(max = 50, message = "Mã quốc tịch từ 1 đến 50 ký tự") // giới hạn không vượt quá kí tự
	@NotEmpty(message = "Mã quốc tịch không được bỏ trống") // không được phép trống
	public String maQuocTich;

	@Getter
	@Setter
	@Size(max = 250, message = "Tên quốc tịch nhập không quá 250 ký tự")
	@NotEmpty(message = "Tên quốc tịch không được bỏ trống")
	public String tenQuocTich;

	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú không nhập quá 1000 ký tự")
	public String ghiChu;

	@Getter
	@Setter
	public int nguoiTaoId;

	@Getter
	@Setter
	public Date ngayTao;

	@Getter
	@Setter
	public Boolean trangThai;

	public QuocTichDTO() {

	}

	public QuocTichDTO(int id) {
		this.id = id;
	}

}
