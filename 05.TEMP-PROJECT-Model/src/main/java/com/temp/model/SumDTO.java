package com.temp.model;

import java.io.Serializable;

import lombok.Getter;

public class SumDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	
	@Getter
	private Long sumTrongSo;
	
	public SumDTO(Long sumTrongSo) {
		super();
		this.sumTrongSo = sumTrongSo;
	}
	
	public SumDTO() {
		super();
	}
	
	

 

}
