package com.temp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class DataImportExcelBDTO implements Serializable {
    private static final long serialVersionUID = 1L;

     

    private List<DataImportExcelDTO> listData;
    private List<DataImportExcelDTO> listHeader;
    
	public DataImportExcelBDTO( List<DataImportExcelDTO> listData,
			List<DataImportExcelDTO> listHeader) {
		super();
 
		this.listData = listData;
		this.listHeader = listHeader;
	}
	public DataImportExcelBDTO() {
		super();
		this.listData= new ArrayList<DataImportExcelDTO>();
		this.listHeader= new ArrayList<DataImportExcelDTO>();
	}
    
    
    

}