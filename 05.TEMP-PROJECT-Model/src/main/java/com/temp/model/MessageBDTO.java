package com.temp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class MessageBDTO extends PageInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public List<MessageDTO> lstMessage;
	

	public MessageBDTO() {
		lstMessage = new ArrayList<MessageDTO>();
	}
}
