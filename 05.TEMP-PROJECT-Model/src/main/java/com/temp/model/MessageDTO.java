package com.temp.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class MessageDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;

	private String message;
	
	private String sendImage;
	
	private Integer nguoiGuiId;
	
	private Integer nguoiNhanId;

	public Timestamp ngayTao;
	
	public Timestamp ngayCapNhat;

	private Boolean trangThai;
	
	private String taiKhoan;
	
	private String ngayTaoStr;
	
	private String avatarNguoiGui;
	
	private String avatarNguoiNhan;
	
	public MessageDTO() {
		
	}
	
}
