package com.temp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class PageInfo extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public int pageNo;
	@Getter
	@Setter
	public int pageSize;
	@Getter
	@Setter
	public long totalElement;
	@Getter
	@Setter
	public long lastPage;

	public PageInfo() {

	}

}
