package com.temp.model;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropDownDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer value;
	private String label;
//	private String other1;

	public DropDownDTO(Object... fields) {
		super();
		if ((fields[0].getClass().getSimpleName() + "").equals("BigDecimal")) {
			this.value = ((BigDecimal) fields[0]).intValue();
		} else if ((fields[0].getClass().getSimpleName() + "").equals("Integer")) {
			this.value = ((Integer) fields[0]);
		}
		this.label = (String) fields[1];
	}

	public DropDownDTO(boolean isCongTy, Object... fields) {
		super();
		if ((fields[0].getClass().getSimpleName() + "").equals("BigDecimal")) {
			this.value = ((BigDecimal) fields[0]).intValue();
		} else if ((fields[0].getClass().getSimpleName() + "").equals("Integer")) {
			this.value = ((Integer) fields[0]);
		}
		this.label = (((String) fields[1]) == null ? "":((String) fields[1])+ " - ")  + (String) fields[2];
	}

    public DropDownDTO(boolean isSheet, boolean isSheetCell, Object... fields) {
		super();
		if ((fields[0].getClass().getSimpleName() + "").equals("BigDecimal")) {
			this.value = ((BigDecimal) fields[0]).intValue();
		} else if ((fields[0].getClass().getSimpleName() + "").equals("Integer")) {
			this.value = ((Integer) fields[0]);
		}
// tên hàng - tên cột
		this.label = (String) fields[2] + " - " + (String) fields[1];
	}

	public DropDownDTO() {
		super();
	}

	public DropDownDTO(int value, String label) {
		super();
		this.value = value;
		this.label = label;
	}

}
