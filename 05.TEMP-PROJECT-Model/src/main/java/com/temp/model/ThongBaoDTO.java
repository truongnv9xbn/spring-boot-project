package com.temp.model;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class ThongBaoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Integer id;
	@Getter
	@Setter
	private String tieuDe;
	@Getter
	@Setter
	private String noiDung;
	@Getter
	@Setter
	private String linkHref;
	@Getter
	@Setter
	private String listNguoiNhan;
	@Getter
	@Setter
	private String listNguoiDaXem;
	@Getter
	@Setter
	private Integer nguoiTaoId;
	@Getter
	@Setter
	private Timestamp ngayTao;
	@Getter
	@Setter
	private Boolean trangThai;
	@Getter
	@Setter
	private String ngayTaoStr;

	public ThongBaoDTO() {

	}

}
