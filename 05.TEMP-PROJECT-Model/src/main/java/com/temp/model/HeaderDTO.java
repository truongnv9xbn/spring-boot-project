package com.temp.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Getter
@Setter
@XmlSeeAlso({ HeaderDTO.class })
@XmlRootElement(name = "HEADER")
@XmlAccessorType(XmlAccessType.FIELD)
public class HeaderDTO implements Serializable {
	private static final Long serialVersionUID = 1L;

	@XmlElement(name = "SENDER_CODE")
	private String senderCode;
	
	@XmlElement(name = "SEND_DATE")
	private String sendDate;

	public HeaderDTO() {
	}

	public HeaderDTO(String senderCode,String sendDate) {
		super();
		this.senderCode = senderCode;
		this.sendDate = sendDate;
	}

}