package com.temp.model;

import java.sql.Timestamp;

import lombok.Data;

@Data
public class TrangChuDTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int id;
	
	private int luotLike;

	private String baiViet;
	
	private String image;
	
	private Integer nguoiDungId;

	public Timestamp ngayTao;
	
	public Timestamp ngayCapNhat;

	private Boolean trangThai;
	
	private String taiKhoan;
	
	private String ngayTaoStr;
	
	private String avatar;
	
	public TrangChuDTO() {
		
	}
	
}
