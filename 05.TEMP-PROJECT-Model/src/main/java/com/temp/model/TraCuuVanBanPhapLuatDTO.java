package com.temp.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.uploadfile.UploadFileDefaultDTO;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Getter
@Setter
public class TraCuuVanBanPhapLuatDTO implements Serializable {
	private static final Long serialVersionUID = 1L;
	private Integer id;
	private String soVanBan;
	private String fileDinhKem;
	private String trichYeu;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Timestamp ngayBanHanh;
	private String ngayBanHanhStr;
	private Boolean trangThai;
	private List<UploadFileDefaultDTO> arrFileDinhKem;
	public TraCuuVanBanPhapLuatDTO() {}
     
}