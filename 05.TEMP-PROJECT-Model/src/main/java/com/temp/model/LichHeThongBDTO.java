package com.temp.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class LichHeThongBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<LichHeThongDTO> lstLichHeThong;

	@Getter
	@Setter
	public List<Integer> lstYear;
	
	public LichHeThongBDTO() {

	}

}
