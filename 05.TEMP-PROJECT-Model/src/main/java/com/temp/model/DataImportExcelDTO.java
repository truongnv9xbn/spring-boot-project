package com.temp.model;

import java.io.Serializable;

import com.temp.model.BieuMauBaoCao.BmSheetCotDTO;
import com.temp.model.BieuMauBaoCao.BmSheetCtDTO;
import com.temp.model.BieuMauBaoCao.BmSheetHangDTO;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class DataImportExcelDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer bmBaoCaoId;
	private Integer bmSheetId;
	private BmSheetHangDTO hang;
	private BmSheetCotDTO cot;
	private BmSheetCtDTO chiTieu;
	private Integer indexCellHang;
	private Integer indexCellCot;
	private String giaTri;
	private String sheetName;
	private Integer bcBaoCaoGtId;
	private boolean laChiTieu;
	private int tongHang;
	private int tongCot;
	private boolean laHangDong;
	private Integer bmSheetHang;

	private String getChiTieuId() {
		if (this.chiTieu != null) {
			return chiTieu.getId() + "";
		} else {
			return "null";
		}
	}

	public BmSheetCtDTO getChiTieu() {
		return chiTieu;
	}

	public void setChiTieu(BmSheetCtDTO chiTieu) {
		this.chiTieu = chiTieu;
	}

	public DataImportExcelDTO() {
		super();
		this.bcBaoCaoGtId = 0;
	}

}