package com.temp.model.dropdown;

import java.io.Serializable;

import lombok.Data;

@Data
public class DropdownMegerCell implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Object value;
	private Object label;
//
//	public DropdownMegerCell() {
//		super();
//	}
//
//	public DropdownMegerCell(Object label, Object value) {
//		this.value = value;
//		this.label = label;
//	}

	public DropdownMegerCell(Object... fields) {
		super();

		this.value = fields[0];

		this.label =fields[1];
	}
}
