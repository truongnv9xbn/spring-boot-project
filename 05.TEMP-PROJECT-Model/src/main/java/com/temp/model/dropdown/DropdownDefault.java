package com.temp.model.dropdown;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropdownDefault implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Object value;
	private String label;

	public DropdownDefault() {
		super();
	}

	public DropdownDefault(String label, Object value) {
		this.value = value;
		this.label = label;
	}

}
