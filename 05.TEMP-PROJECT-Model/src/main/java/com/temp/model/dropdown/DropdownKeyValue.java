/**
 * 
 */
package com.temp.model.dropdown;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class DropdownKeyValue {

	private int intKey;
	private Object value;

	public DropdownKeyValue() {

	}

	public DropdownKeyValue(int intKey, Object Value) {
		this.intKey = intKey;
		this.value = Value;
	}
}
