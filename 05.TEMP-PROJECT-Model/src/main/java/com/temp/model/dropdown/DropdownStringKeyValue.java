/**
 * 
 */
package com.temp.model.dropdown;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class DropdownStringKeyValue {

	private String strKey;
	private Object value;

	public DropdownStringKeyValue() {

	}

	public DropdownStringKeyValue(String StrKey, Object Value) {
		this.strKey = StrKey;
		this.value = Value;
	}
}
