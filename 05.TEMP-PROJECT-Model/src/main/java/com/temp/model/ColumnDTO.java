package com.temp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Getter
@Setter
public class ColumnDTO implements Serializable {
	private static final Long serialVersionUID = 1L;
 
	private Integer name;
	
    private String align;
	
    private String label;
	
    private boolean sortable;
    
    private Integer field;
    
    private String style;
    
    
  	public ColumnDTO() {}

	public ColumnDTO(Integer name, String align, String label, boolean sortable, Integer field, String style) {
		super();
		this.name = name;
		this.align = align;
		this.label = label;
		this.sortable = sortable;
		this.field = field;
		this.style = style;
	}
	 
	
	
//    public ColumnDTO(Object... fields) {
//		super();
//		name=((String) fields[0]);
//		align=((String) fields[1]);
//		label=((String) fields[2]);
//		sortable=((String) fields[3]);
//		field=((String) fields[4]);
//		style=((String) fields[5]);
//	}
}