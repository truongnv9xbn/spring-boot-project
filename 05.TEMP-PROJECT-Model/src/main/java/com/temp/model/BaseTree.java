package com.temp.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class BaseTree {
	
	private boolean leaf;
	private BigDecimal level;
	private boolean expend;

}
