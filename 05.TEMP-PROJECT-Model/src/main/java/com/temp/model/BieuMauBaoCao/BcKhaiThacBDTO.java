package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

public class BcKhaiThacBDTO extends PageInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    @Getter
    @Setter
    public List<BcKhaiThacDTO> bcKhaiThacList;
	@Getter
	@Setter
	public LinkedHashSet<String> kyBaoCaoSet;
	@Getter
	@Setter
	public String tenBaoCao;
    
    public BcKhaiThacBDTO() {
	this.bcKhaiThacList = new ArrayList<BcKhaiThacDTO>();
	
	
    }
}
