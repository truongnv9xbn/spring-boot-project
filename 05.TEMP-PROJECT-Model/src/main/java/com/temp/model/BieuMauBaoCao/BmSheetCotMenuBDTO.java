package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */

public class BmSheetCotMenuBDTO extends PageInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private List<BmSheetCotMenuLeftDTO> lstMenuCot;

	public BmSheetCotMenuBDTO() {
		this.lstMenuCot = new ArrayList<>();
	}

}