package com.temp.model.BieuMauBaoCao;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class BmTieuDeHangCotDTO {
	private Integer id;
	private String tenCot;
	private String tenCotTiengAnh;
	private Long thuTu;
	private Long fisrtRow;
	private Long lastRow;
	private Long fisrtColumn;
	private Long lastColumn;
	private Long rowSpan;
	private Long colSpan;
	private Boolean hide;
	private Long phienBan;
	private Integer sheetId;
	private Integer tieuDeHangId;
	private Integer tieuDeHangCotId;
	private BmSheetDTO bmSheetId;
	private BmTieuDeHangDTO bmTieuDeHangId;

	public BmTieuDeHangDTO getBmTieuDeHangId() {
		return bmTieuDeHangId;
	}

	public void setBmTieuDeHangId(BmTieuDeHangDTO bmTieuDeHangId) {
		if (bmTieuDeHangId != null) {
			this.tieuDeHangId = bmTieuDeHangId.getId();
		}
	}

	public BmSheetDTO getBmSheetId() {
		return bmSheetId;
	}

	public void setBmSheetId(BmSheetDTO bmSheetId) {

		if (bmSheetId != null) {
			this.sheetId = bmSheetId.getId();
		}
	}

	public BmTieuDeHangCotDTO() {
	}

	/**
	 * Gen header excel props.
	 * 
	 * @param objects
	 */
	public BmTieuDeHangCotDTO(Object... objects) {
		this.setId(objects[0] == null ? 0 : ((BigDecimal) objects[0]).intValue());
		this.setSheetId(objects[1] == null ? 0 : ((BigDecimal) objects[1]).intValue());
		this.setTieuDeHangId(objects[2] == null ? 0 : ((BigDecimal) objects[2]).intValue());
		this.setTenCot(objects[3] == null ? null : objects[3].toString());
		this.setTenCotTiengAnh(objects[4] == null ? null : objects[4].toString());
		this.setThuTu(objects[5] == null ? 0 : ((BigDecimal) objects[5]).longValue());
		this.setFisrtRow(objects[6] == null ? 0 : ((BigDecimal) objects[6]).longValue());
		this.setLastRow(objects[7] == null ? 0 : ((BigDecimal) objects[7]).longValue());
		this.setFisrtColumn(objects[8] == null ? 0 : ((BigDecimal) objects[8]).longValue());
		this.setLastColumn(objects[9] == null ? 0 : ((BigDecimal) objects[9]).longValue());
		this.setRowSpan(objects[10] == null ? 0 : ((BigDecimal) objects[10]).longValue());
		this.setColSpan(objects[11] == null ? 0 : ((BigDecimal) objects[11]).longValue());
	}

}
