package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import com.temp.model.DropDownStringDTO;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class KyBaoCaoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private boolean active= true;
    private Integer id;
    private String ky;
    private Integer nam;
    private Integer tongHop;
    private Integer tinhDiem;
    private String moTa;
    private Integer nguoiTaoId;
    private Timestamp ngayTao;
    private Integer nguoiCapNhatId;
    private Timestamp ngayCapNhat;
    private Integer trangThai;
    private String giaTriKy;
    
    
    private DropDownStringDTO kyBaoCaoDropdown;
    private DropDownStringDTO giaTriKyDropdown;
    
    private Collection<KyBaoCaoDTO> qlrrKyBaoCaoCtckCollection;
    
	public KyBaoCaoDTO() {
		
		this.qlrrKyBaoCaoCtckCollection= new ArrayList<KyBaoCaoDTO>();
		
	}

	public KyBaoCaoDTO(String tenCongTy) {
		super();
	}

}
