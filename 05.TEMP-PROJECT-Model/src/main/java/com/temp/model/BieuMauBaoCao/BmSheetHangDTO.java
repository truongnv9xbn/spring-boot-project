package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmSheetHangDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String maHang;
	private String maHangLT;
	private String tenHang;
	private Long thuTu;
	private String dinhDangTen;
	private String mauSac;
	private String canLe;
	private Boolean hangDong;
	private String moTa;
	private Boolean suDung;
	private Long trangThai;
	private Long phienBan;
	private Timestamp ngayCapNhat;
	private BmBaoCaoDTO bmBaoCaoByBmBaoCaoId;
	private BmSheetDTO bmSheetByBmSheetId;
	private Integer index;
}