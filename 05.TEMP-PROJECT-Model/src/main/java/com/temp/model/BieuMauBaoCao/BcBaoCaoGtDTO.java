package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class BcBaoCaoGtDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;

	@Getter
	@Setter
	private Integer bmBaoCaoDinhKyId;

	@Getter
	@Setter
	private Integer bmBaoCaoId;

	@Getter
	@Setter
	private Integer bmSheetCotId;

	@Getter
	@Setter
	private Integer bmSheetCtId;

	@Getter
	@Setter
	private Integer bmSheetHangId;

	@Getter
	@Setter
	private Integer bmSheetId;

	@Getter
	@Setter
	private String congThuc;

	@Getter
	@Setter
	private String congThucTong;

	@Getter
	@Setter
	private Integer ctckThongTinId;

	@Getter
	@Setter
	private Integer dmChiTieuId;

	@Getter
	@Setter
	private String giaTri;

	@Getter
	@Setter
	private String idDanhMuc;

	@Getter
	@Setter
	private Timestamp ngayCapNhat;

	@Getter
	@Setter
	private String ngaySoLieu;

	@Getter
	@Setter
	private Timestamp ngayTao;

	@Getter
	@Setter
	private int nguoiTaoId;

	@Getter
	@Setter
	private int phienBan;

	@Getter
	@Setter
	private int rowId;

	@Getter
	@Setter
	private String tenCot;

	@Getter
	@Setter
	private String tenDanhMuc;

	@Getter
	@Setter
	private String tenHang;

	@Getter
	@Setter
	private String tenSheet;

	@Getter
	@Setter
	private String textDanhMuc;

	@Getter
	@Setter
	private String dulieuBaoCao;

	// use for BaoCaoDinhKy
	@Getter
	@Setter
	private String fileDinhKem;

	@Getter
	@Setter
	private String trichYeu;

	@Getter
	@Setter
	private String kieuSheet;

	@Getter
	@Setter
	private Integer bcThanhVienId;

	// Use for Gui Bao Cao Dinh Ky
	@Getter
	@Setter
	private Integer bcGiaTriAddOrUpdateId;

	// update entities
	@Getter
	@Setter
	private Integer bmSheetCtVaoId;

	@Getter
	@Setter
	private String giaTriKyBc;

	@Getter
	@Setter
	private String kyBaoCao;

	@Getter
	@Setter
	private String nam;

	@Getter
	@Setter
	private boolean trangThaiGui;

	@Getter
	@Setter
	private String dinhDang;

	@Getter
	@Setter
	private Integer donViTinh;

	@Getter
	@Setter
	private Integer thuTuHang;

	public BcBaoCaoGtDTO() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Object load for get value.
	 * 
	 * @param getGt
	 * @param field
	 */
	public BcBaoCaoGtDTO(boolean getGt, Object... field) {
		this.setId((field[0] == null) ? 0 : ((BigDecimal) field[0]).intValue());
		this.setBmBaoCaoId((field[1] == null) ? 0 : ((BigDecimal) field[1]).intValue());
		this.setCtckThongTinId((field[2] == null) ? 0 : ((BigDecimal) field[2]).intValue());
		this.setBmSheetCtId((field[3] == null) ? 0 : ((BigDecimal) field[3]).intValue());
		this.setBmSheetId((field[4] == null) ? 0 : ((BigDecimal) field[4]).intValue());
		this.setBmSheetHangId((field[5] == null) ? 0 : ((BigDecimal) field[5]).intValue());
		this.setBmSheetCotId((field[6] == null) ? 0 : ((BigDecimal) field[6]).intValue());
		this.setGiaTri((field[7] == null) ? "" : field[7].toString());
	}

	public BcBaoCaoGtDTO(Object... field) {
		if(field.length > 2) {
			this.setId((field[0] == null) ? 0 : ((BigDecimal) field[0]).intValue());
			this.setBmBaoCaoId((field[1] == null) ? 0 : ((BigDecimal) field[1]).intValue());
			this.setCtckThongTinId((field[2] == null) ? 0 : ((BigDecimal) field[2]).intValue());
			this.setBmSheetCtId((field[3] == null) ? 0 : ((BigDecimal) field[3]).intValue());
			this.setBmSheetId((field[4] == null) ? 0 : ((BigDecimal) field[4]).intValue());
			this.setBmSheetHangId((field[5] == null) ? 0 : ((BigDecimal) field[5]).intValue());
			this.setBmSheetCotId((field[6] == null) ? 0 : ((BigDecimal) field[6]).intValue());
			this.setGiaTri((field[7] == null) ? "" : field[7].toString());
			this.setBmBaoCaoDinhKyId((field[8] == null) ? 0 : ((BigDecimal) field[8]).intValue());
			this.setBcThanhVienId((field[9] == null) ? 0 : ((BigDecimal) field[9]).intValue());
			this.setKieuSheet((field[10] == null) ? "" : field[10].toString());
			this.setNam((field[11] == null) ? "" : field[11].toString());
			this.setKyBaoCao((field[12] == null) ? "" : field[12].toString());
			this.setGiaTriKyBc((field[13] == null) ? "" : field[13].toString());
			this.setBmSheetCtVaoId((field[14] == null) ? 0 : ((BigDecimal) field[14]).intValue());
		}else {
			this.setBmBaoCaoId((field[0] == null) ? 0 : ((BigDecimal) field[0]).intValue());
			this.setGiaTri((field[1] == null) ? "" : field[1].toString());
		}
	}

}
