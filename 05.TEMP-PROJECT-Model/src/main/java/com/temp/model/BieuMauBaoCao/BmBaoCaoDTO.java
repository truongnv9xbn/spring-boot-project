package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import com.temp.model.dropdown.DropdownDefault;
import com.temp.model.uploadfile.UploadFileDefaultDTO;
import com.temp.utils.LocalDateTimeUtils;
import com.temp.utils.TimestampUtils;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmBaoCaoDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String maBaoCao;
	private Integer id;
	private String tenBaoCao;
	private String canCuPhapLy;
	private String nhomBaoCao;
	private Boolean canCbtt;
	private Boolean canImport;
	private String fileDinhKem;
	private String moTa;
	private Integer suDung;
	private Integer trangThai;
	private String loaiBaoCao;
	private String loaiBaoCaoLT;
	private Integer kieuBaoCao;
	private Long phienBan;
	private Timestamp ngayPhienBan;
	private Boolean trichYeu;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Integer nguoiCapNhatId;
	private Timestamp ngayCapNhat;
	private String doiTuongGui;
	private Integer bmBaoCaoId;
	private String ngayCapNhatStr;
	private Integer bmBaoCaoDinhKyId;
	private String kyBaoCao;

	private List<UploadFileDefaultDTO> arrFileDinhKem;

	public List<BmBaoCaoDinhKyDTO> lstBmBaoCaoDinhKy;

	public List<BmSheetDTO> lstBmSheet;
	private String loaiTinCbtt;

	private List<DropdownDefault> groupSelected;

	private List<DropdownDefault> doiTuongSelected;
	private String ngayTaoStr;
	private String nguoiTaoStr;

	private Boolean baoCaoDauVao;
	private String trangThaiStr;

	private Integer bmNextId;

	public Timestamp getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
		this.setNgayTaoStr((LocalDateTimeUtils.convertTimestampToStringDateTime(ngayTao)));

	}

	public BmBaoCaoDTO(Object... fields) {
		super();
		this.bmBaoCaoDinhKyId = fields[0] != null ? ((BigDecimal) fields[0]).intValue() : null;
		this.tenBaoCao = fields[1] != null ? ((String) fields[1]) : "";
		this.kyBaoCao = fields[2] != null ? ((String) fields[2]) : "";
		this.id = fields[3] != null ? ((BigDecimal) fields[3]).intValue() : null;
	}

	public Timestamp getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Timestamp ngayCapNhat) {
		if (ngayCapNhat != null) {
			this.setNgayCapnhatStr(TimestampUtils.TimestampToString_ddMMyyyyHHmmss(ngayCapNhat));
		}
		this.ngayCapNhat = ngayCapNhat;
	}

	public String getNgayCapnhatStr() {
		return ngayCapNhatStr;
	}

	public void setNgayCapnhatStr(String ngayCapNhatStr) {
		this.ngayCapNhatStr = ngayCapNhatStr;
	}

	public BmBaoCaoDTO() {
		super();
	}

}