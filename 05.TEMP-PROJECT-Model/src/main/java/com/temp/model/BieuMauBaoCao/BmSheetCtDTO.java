package com.temp.model.BieuMauBaoCao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;

import com.temp.model.cellfunction.CongThucDTO;

import lombok.Data;

@Data
public class BmSheetCtDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer bmBaoCaoId;
	private Integer sheetId;
	private Integer dmChiTieuId;
	private Integer bmSheetCotId;
	private Integer bmSheetHangId;
	private Integer bmSheetHangTu;
	private Integer bmSheetHangDen;
	private Integer bmSheetCtVaoId;
	private String dinhDang;
	private Long donViTinh;
	private Long giaTriDefault;
	private Long thuTu;
	private String moTa;
	private Timestamp ngayCapNhat;
	private Boolean suDung;
	private Boolean batBuoc;
	private String congThuc;
	private String congThucTong;
	private String congThucText;
	private Long phienBan;
	private String label;
	private CongThucDTO congThucDto;
	private Integer indexHang;
	private Integer indexCot;
	private BmSheetDTO bmSheetDTO;
	private String kieuSheet;
	private Integer hangDong;

	private Integer mauSac;
	private String dinhDangStyle;
	private String canLe;
	private Boolean inDam;
	private Boolean inNghieng;
	private Boolean inHoa;
	private Boolean gachChan;
	private String dinhDangKyTu;
	private Integer dinhDangBmBaoCaoId;

	private Integer dongHang;

	private String filterId;

	public BmSheetCtDTO() {
		// TODO Auto-generated constructor stub
	}

	public BmSheetCtDTO(Object... field) {
		this.setId((field[0] == null) ? null : ((BigDecimal) field[0]).intValue());
		this.setBmBaoCaoId((field[1] == null) ? null : ((BigDecimal) field[1]).intValue());
		this.setSheetId((field[2] == null) ? null : ((BigDecimal) field[2]).intValue());
		this.setBmSheetHangId((field[3] == null) ? null : ((BigDecimal) field[3]).intValue());
		this.setBmSheetCotId((field[4] == null) ? null : ((BigDecimal) field[4]).intValue());
		this.setKieuSheet((field[5] == null) ? null : field[5].toString());
		this.setCongThuc((field[6] == null) ? null : (clobToString((Clob) field[6])));
		this.setHangDong((field[7] == null) ? null : ((BigDecimal) field[7]).intValue());
		this.setGiaTriDefault((field[8] == null) ? null : ((BigDecimal) field[8]).longValue());
		this.setDinhDang((field[9] == null) ? null : field[9].toString());
		this.setDinhDangKyTu((field[10] == null) ? null : field[10].toString());
		this.setCongThucText((field[11] == null) ? null : (clobToString((Clob) field[11])));
		this.setDonViTinh((field[12] == null) ? null : ((BigDecimal) field[12]).longValue());
		this.setFilterId((field[13] == null) ? null : field[13].toString());
	}

	/**
	 * Map props for gen table.
	 * 
	 * @param isGenTable
	 * @param field
	 */
	public BmSheetCtDTO(boolean isGenTable, Object... field) {
		this.setId((field[0] == null) ? 0 : ((BigDecimal) field[0]).intValue());
		this.setBmBaoCaoId((field[1] == null) ? 0 : ((BigDecimal) field[1]).intValue());
		this.setSheetId((field[2] == null) ? 0 : ((BigDecimal) field[2]).intValue());
		this.setBmSheetCotId((field[3] == null) ? 0 : ((BigDecimal) field[3]).intValue());
		this.setBmSheetHangId((field[4] == null) ? 0 : ((BigDecimal) field[4]).intValue());

		this.setDinhDang((field[5] == null) ? "" : field[5].toString());
		this.setLabel((field[6] == null) ? "" : field[6].toString());
		this.setBmSheetCtVaoId((field[7] == null) ? 0 : ((BigDecimal) field[7]).intValue());
		this.setDinhDangStyle((field[8] == null) ? "" : field[8].toString());
		this.setCanLe((field[9] == null) ? "" : field[9].toString());
		this.setInDam((field[10] == null) ? false : (((BigDecimal) field[10]).intValue() == 0 ? false : true));
		this.setInNghieng((field[11] == null) ? false : (((BigDecimal) field[11]).intValue() == 0 ? false : true));
		this.setInHoa((field[12] == null) ? false : (((BigDecimal) field[12]).intValue() == 0 ? false : true));
		this.setGachChan((field[13] == null) ? false : (((BigDecimal) field[13]).intValue() == 0 ? false : true));
		if (field.length > 14 && field[14] != null) {
			this.setBatBuoc(((BigDecimal) field[14]).intValue() == 0 ? false : true);
		}
	}

	private String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();
	}

}
