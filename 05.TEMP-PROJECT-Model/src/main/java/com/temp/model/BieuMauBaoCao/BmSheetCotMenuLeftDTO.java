package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmSheetCotMenuLeftDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer SheetId;

	private String tenCot;
	private String maCot;

	public BmSheetCotMenuLeftDTO(Object... fields) {
		super();

		this.id = ((BigDecimal) fields[0]).intValue();

		this.SheetId = ((BigDecimal) fields[1]).intValue();

		this.tenCot = (String) fields[2];
		this.maCot = (String) fields[3];
	}

}