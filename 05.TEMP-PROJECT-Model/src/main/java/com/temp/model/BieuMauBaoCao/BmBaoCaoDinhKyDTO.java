package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Data;

@Data
public class BmBaoCaoDinhKyDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer bmBaoCaoId;
	private String kyBaoCao;
	private Long t;
	private Long thoiGianMuonNhat;
	private Boolean donViTinh;
	private Long thoiGianLap;
	private Long gioPhutGui;
	private String ngayDuKienInsertStr;
	private Timestamp ngayDuKienInsert;
	private String ghiChu;
	private Integer suDung;
	private Boolean trangThai;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private String tenBmBaoCao;
	private Integer vaoNgay;

	public BmBaoCaoDinhKyDTO(Object... fields) {
		super();
		this.id = ((Integer) fields[0]).intValue();
		this.bmBaoCaoId = fields[1] != null ? ((Integer) fields[1]).intValue() : null;
		this.kyBaoCao = (String) fields[2];
		this.t = fields[3] != null ? ((Long) fields[3]).longValue() : null;
		this.thoiGianMuonNhat = fields[4] != null ? ((Long) fields[4]).longValue() : null;
		this.donViTinh = fields[5] != null ? (Boolean) fields[5] : null;
		this.thoiGianLap = fields[6] != null ? ((Long) fields[6]).longValue() : null;
		this.gioPhutGui = fields[7] != null ? ((Long) fields[7]).longValue() : null;
		this.ngayDuKienInsert = fields[8] != null ? (Timestamp) fields[8] : null;
		this.ghiChu = (String) fields[9];
		this.suDung = fields[10] != null ? ((Integer) fields[10]).intValue() : null;
		this.trangThai = fields[11] != null ? (Boolean) fields[11] : null;
		this.nguoiTaoId = fields[12] != null ? ((Integer) fields[12]).intValue() : null;
		this.ngayTao = (Timestamp) fields[13];
		this.tenBmBaoCao = (String) fields[14];
		this.vaoNgay = fields[15] != null ? ((Integer) fields[15]).intValue() : null;
	}

	public BmBaoCaoDinhKyDTO() {

	}

}
