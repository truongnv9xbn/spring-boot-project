package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

public class KyBaoCaoBDTO extends PageInfo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	

	@Getter
	@Setter
	public List<KyBaoCaoDTO> listKyBaoCaoDTO;


}
