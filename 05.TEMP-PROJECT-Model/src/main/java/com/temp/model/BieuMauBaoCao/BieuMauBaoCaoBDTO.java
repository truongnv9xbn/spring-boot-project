package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

public class BieuMauBaoCaoBDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public Map<String, List<BmBaoCaoDTO>> lstBDTO;
	

	public BieuMauBaoCaoBDTO() {
		lstBDTO = new HashMap<String,List<BmBaoCaoDTO>>();
	}
}
