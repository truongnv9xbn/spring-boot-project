package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;

import lombok.Data;

@Data
public class BcKhaiThacGtDTO implements Serializable, Comparable<BcKhaiThacGtDTO> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer bcKhaiThacId;
    private Integer bmBaoCaoId;
    private Integer bmSheetId;
    private Integer bmSheetHangId;
    private Integer bmSheetCotId;
    private Integer bmSheetCtId;
    private Integer rowId;
    private String giaTri;
    private Integer bcktId;
    private String kyBaoCao;
    
//    public BcKhaiThacGtDTO(Object... fields) {
//    	super();
//		this.id = ((BigDecimal) fields[0]).intValue();
//		this.bmBaoCaoId = ((BigDecimal) fields[1]).intValue();
//		this.ngayTuStr = ((String) TimestampUtils.TimestampToString_ddMMyyyy((Timestamp)fields[2]));
//		this.ngayDenStr = ((String) TimestampUtils.TimestampToString_ddMMyyyy((Timestamp)fields[3]));
//		this.ngayTongHopStr = ((String) TimestampUtils.TimestampToString_ddMMyyyy((Timestamp)fields[4]));
//		this.bmBaoCaoId = ((BigDecimal) fields[5]).intValue();
//		this.tenBaoCao = ((String) fields[6]);
//		this.maBaoCao = ((String) fields[7]);
//		this.nguoiTongHop = ((String) fields[8]);
//    }
    
    public BcKhaiThacGtDTO() {
	// TODO Auto-generated constructor stub
    }
   
    @Override
    public int compareTo(BcKhaiThacGtDTO a) {
      return getId().compareTo(a.getId());
    }
    
}
