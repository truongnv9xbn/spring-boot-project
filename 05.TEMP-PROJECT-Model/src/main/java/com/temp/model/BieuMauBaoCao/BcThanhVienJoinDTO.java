package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class BcThanhVienJoinDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/** Bc_Thanh_Vien */
	@Getter
	@Setter
	private Integer tvId;

	@Getter
	@Setter
	private Timestamp tvNgayGui;

	@Getter
	@Setter
	private Integer tvTrangThai;

	@Getter
	@Setter
	private String tvKyBaoCao;

	@Getter
	@Setter
	private String tvGiaTriKyBc;

	@Getter
	@Setter
	private Integer tvBmBaoCaoKyId;

	@Getter
	@Setter
	private Integer tvCtckThongTinId;
	/** Bc_Thanh_Vien - end */

	/** Ctck_Thong_Tin */
	@Getter
	@Setter
	private String ctckTenTiengViet;
	/** Ctck_Thong_Tin - end */

	/** BM_Bao_Cao_Dinh_Ky */
	@Getter
	@Setter
	private String bcdkKyBaoCao;

	@Getter
	@Setter
	private String tenBaoCao;

	@Getter
	@Setter
	private String loaiBaoCao;

	@Getter
	@Setter
	private String fileDinhKem;

	@Getter
	@Setter
	private Integer huyBaoCao;

	@Getter
	@Setter
	private Integer khoaBaoCao;

	@Getter
	@Setter
	private String fileBaoCao;
	
	@Getter
	@Setter
	private Integer isValidGuiBc;
	
	@Getter
	@Setter
	private String fileName;
	
	@Getter
	@Setter
	private String namCanhBao;
	
	/** BM_Bao_Cao_Dinh_Ky - end */

	public BcThanhVienJoinDTO() {
		// TODO Auto-generated constructor stub
	}

	public BcThanhVienJoinDTO(Object... objects) {

		// thanh vien properties
		this.setTvId((objects[0] == null) ? 0 : ((BigDecimal) objects[0]).intValue());
		this.setTvNgayGui((objects[1] == null) ? null : ((Timestamp) objects[1]));
		this.setTvTrangThai((objects[2] == null) ? 0 : ((BigDecimal) objects[2]).intValue());
		this.setTvBmBaoCaoKyId((objects[3] == null) ? 0 : ((BigDecimal) objects[3]).intValue());
		this.setTvCtckThongTinId((objects[4] == null) ? 0 : ((BigDecimal) objects[4]).intValue());

		// Ctck_Thong_Tin properties
		this.setCtckTenTiengViet((objects[5] == null) ? null : ((String) objects[5]));

		this.setTenBaoCao((objects[6] == null) ? null : ((String) objects[6]));

		// BM_Bao_Cao_Dinh_Ky
		this.setBcdkKyBaoCao((objects[7] == null) ? null : ((String) objects[7]));

		this.setLoaiBaoCao((objects[8] == null) ? null : ((String) objects[8]));

		this.setFileDinhKem((objects[9] == null) ? null : ((String) objects[9]));

		this.setTvKyBaoCao((objects[10] == null) ? null : ((String) objects[10]));

		this.setTvGiaTriKyBc((objects[11] == null) ? null : ((String) objects[11]));

		this.setHuyBaoCao((objects[12] == null) ? null : ((BigDecimal) objects[12]).intValue());

		this.setKhoaBaoCao((objects[13] == null) ? null : ((BigDecimal) objects[13]).intValue());

		this.setFileBaoCao((objects[14] == null) ? null : ((String) objects[14]));
		
		if(objects[15] != null && objects[15] != "") {
			this.setIsValidGuiBc((objects[15] == null) ? null : ((BigDecimal) objects[15]).intValue());
		}
		
		this.setFileName(objects[16] != null ? (String) objects[16] : "");
		
		this.setNamCanhBao(objects[17] != null ? (String) objects[17] : "");
	}

}
