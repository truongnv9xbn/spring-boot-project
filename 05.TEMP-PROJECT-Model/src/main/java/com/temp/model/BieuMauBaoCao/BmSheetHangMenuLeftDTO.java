package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmSheetHangMenuLeftDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer SheetId;

	private String tenHang;
	private String maHang;

	public BmSheetHangMenuLeftDTO(Object... fields) {
		super();

		this.id = ((BigDecimal) fields[0]).intValue();

		this.SheetId = ((BigDecimal) fields[1]).intValue();

		this.tenHang = (String) fields[2];
		this.maHang = (String) fields[3];
	}

}