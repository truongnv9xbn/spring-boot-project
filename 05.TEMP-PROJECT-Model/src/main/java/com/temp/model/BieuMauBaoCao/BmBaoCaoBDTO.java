package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

public class BmBaoCaoBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<BmBaoCaoDTO> lstBmBaoCao;
	@Getter
	@Setter
	public List<BmBaoCaoGroupBy> lstBmBaoCaoDisplay;
	
	public BmBaoCaoBDTO() {
		lstBmBaoCao = new ArrayList<BmBaoCaoDTO>();
		lstBmBaoCaoDisplay = new ArrayList<BmBaoCaoGroupBy>();
		
	}
	

}
