package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;

import org.apache.poi.xssf.usermodel.XSSFSheet;

import lombok.Data;
/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class SheetItemDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;

	XSSFSheet sheetData;
	XSSFSheet sheetSetting;	
	
	
	
	
	
    
    

}