package com.temp.model.BieuMauBaoCao;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class BmBaoCaoGroupBy {

	private String nhomBaoCao;
	private boolean active = true;
	private List<BmBaoCaoDTO> lstBmBaoCao;

	public BmBaoCaoGroupBy() {
		this.lstBmBaoCao = new ArrayList<>();
	}

}
