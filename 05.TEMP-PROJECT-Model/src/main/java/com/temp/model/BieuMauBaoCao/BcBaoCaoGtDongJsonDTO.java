package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BcBaoCaoGtDongJsonDTO implements Serializable {

    private static final long serialVersionUID = 1L;

   // private Integer id;
   // private Integer bmBaoCaoId;
    private Integer bmSheetHang;
   // private Integer bmSheetCot;
    private String giaTri;
    private Integer indexCellCot;
    private Integer indexCellHang;

    public BcBaoCaoGtDongJsonDTO() {
	super();
    }
    
    

}