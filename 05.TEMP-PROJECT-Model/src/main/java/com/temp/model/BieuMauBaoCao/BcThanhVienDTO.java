package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class BcThanhVienDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Integer id;

	@Getter
	@Setter
	private Integer bmBaoCaoDinhKyId;

	@Getter
	@Setter
	private Integer bmBaoCaoId;

	@Getter
	@Setter
	private Integer ctKiemToanId;

	@Getter
	@Setter
	private Integer ctKiemToanVienId;

	@Getter
	@Setter
	private Integer ctckThongTinId;

	@Getter
	@Setter
	private String fileDinhKem;

	@Getter
	@Setter
	private String giaTriKyBc;

	@Getter
	@Setter
	private String kyBaoCao;

	@Getter
	@Setter
	private String lyDo;

	@Getter
	@Setter
	private String lyDoGuiLai;

	@Getter
	@Setter
	private String moTa;

	@Getter
	@Setter
	private String namCanhBao;

	@Getter
	@Setter
	private Timestamp ngayGui;

	@Getter
	@Setter
	private Timestamp ngaySoLieu;

	@Getter
	@Setter
	private Timestamp ngayTao;

	@Getter
	@Setter
	private Integer nguoiDungId;

	@Getter
	@Setter
	private Integer phienBan;

	@Getter
	@Setter
	private String thoiDiemGui;

	@Getter
	@Setter
	private Timestamp thoiHanGiaHan;
	
	@Getter
	@Setter
	private String lyDoGiaHan;

	@Getter
	@Setter
	private Timestamp thoiHanGui;

	@Getter
	@Setter
	private Integer trangThai;

	@Getter
	@Setter
	private String trichYeu;

	@Getter
	@Setter
	private Integer xoaDuLieu;

	@Getter
	@Setter
	private String trangThaiKiemToan;

	@Getter
	@Setter
	private Boolean khoaBaoCao;

	@Getter
	@Setter
	private String loaiBcGui;

	@Getter
	@Setter
	private String giaTriKyBaoCao;

	@Getter
	@Setter
	private Integer huyBaoCao;

	@Getter
	@Setter
	private String tenBaoCao;
	
	@Getter
	@Setter
	private String fileBaoCao;
	
	@Getter
	@Setter
	private Boolean isValidGuiBc;
	
	@Getter
	@Setter
	private String fileName;
	
	@Getter
	@Setter
	private byte[] data;
	
	public BcThanhVienDTO(boolean isGetCtckId, Object... fields) {
		this.setId((fields[0] == null) ? 0 : (Integer) fields[0]);
		this.setCtckThongTinId((fields[1] == null) ? 0 : (Integer) fields[1]);
		if(fields.length == 5 ) {
			this.setNamCanhBao((fields[2] == null) ? "" : (String) fields[2]);
			this.setKyBaoCao((fields[3] == null) ? "" : (String) fields[3]);
			this.setGiaTriKyBc((fields[4] == null) ? "" : (String) fields[4]);
		}
	}

	public BcThanhVienDTO(String lsBcThanhVienId, Object... fields) throws Exception {
		this.setId((fields[0] == null) ? 0 : ((BigDecimal) fields[0]).intValue());
		this.setThoiHanGui(((fields[1] == null) ? null : ((Timestamp) fields[1])));
		// this.setThoiHanGiaHan(((fields[2] == null) ? "" : (String) fields[2]));
		this.setThoiDiemGui(((fields[3] == null) ? "" : (String) fields[3]));
		this.setNgaySoLieu((fields[4] == null) ? null : ((Timestamp) fields[4]));
		this.setNgayGui((fields[5] == null) ? null : ((Timestamp) fields[5]));
		this.setTrangThai(((fields[6] == null) ? 0 : ((BigDecimal) fields[6]).intValue()));
		this.setKyBaoCao(((fields[7] == null) ? "" : fields[7].toString()));
		this.setTenBaoCao(((fields[8] == null) ? "" : fields[8].toString()));
		this.setGiaTriKyBc(((fields[9] == null) ? "" : fields[9].toString()));
		this.setKyBaoCao(((fields[10] == null) ? "" : fields[10].toString()));
	}

	public BcThanhVienDTO() {
		// TODO Auto-generated constructor stub
	}

	public BcThanhVienDTO(Object... fields) {
		this.setId((fields[0] == null) ? 0 : ((BigDecimal) fields[0]).intValue());
		this.setCtckThongTinId((fields[1] == null) ? 0 : ((BigDecimal) fields[1]).intValue());
		this.setNguoiDungId((fields[2] == null) ? 0 : ((BigDecimal) fields[2]).intValue());
		this.setBmBaoCaoId((fields[3] == null) ? 0 : ((BigDecimal) fields[3]).intValue());
		this.setBmBaoCaoDinhKyId((fields[4] == null) ? 0 : ((BigDecimal) fields[4]).intValue());
		this.setCtKiemToanId((fields[5] == null) ? 0 : ((BigDecimal) fields[5]).intValue());
		this.setCtKiemToanVienId((fields[6] == null) ? 0 : ((BigDecimal) fields[6]).intValue());
		this.setGiaTriKyBc((fields[7] == null) ? null : (fields[7].toString()));
		this.setTrichYeu((fields[8] == null) ? null : (fields[8].toString()));
		this.setTrangThai((fields[9] == null) ? 0 : ((BigDecimal) fields[9]).intValue());
		this.setFileDinhKem((fields[10] == null) ? null : (fields[10].toString()));
		this.setNamCanhBao((fields[11] == null) ? null : (fields[11].toString()));
		this.setKyBaoCao((fields[12] == null) ? null : (fields[12].toString()));
		this.setThoiHanGui((fields[13] == null) ? null : ((Timestamp) fields[13]));
		this.setTenBaoCao((fields[14] == null) ? null : (fields[14].toString()));
		if(fields.length > 15) {
			this.setHuyBaoCao((fields[15] == null) ? 0 : ((BigDecimal) fields[15]).intValue());
		}
		if(fields.length > 16) {
			this.fileName = fields[16] != null ? (String) fields[16] : "";
			this.data = fields[17] != null ? (byte[]) fields[17] : null;
		}
	}
	
	public BcThanhVienDTO(Integer isValid, Object... fields) {
		this.setId((fields[0] == null) ? 0 : ((BigDecimal) fields[0]).intValue());
		this.setCtckThongTinId((fields[1] == null) ? 0 : ((BigDecimal) fields[1]).intValue());
		this.setNguoiDungId((fields[2] == null) ? 0 : ((BigDecimal) fields[2]).intValue());
		this.setBmBaoCaoId((fields[3] == null) ? 0 : ((BigDecimal) fields[3]).intValue());
		this.setBmBaoCaoDinhKyId((fields[4] == null) ? 0 : ((BigDecimal) fields[4]).intValue());
		this.setCtKiemToanId((fields[5] == null) ? 0 : ((BigDecimal) fields[5]).intValue());
		this.setCtKiemToanVienId((fields[6] == null) ? 0 : ((BigDecimal) fields[6]).intValue());
		this.setGiaTriKyBc((fields[7] == null) ? null : (fields[7].toString()));
		this.setTrichYeu((fields[8] == null) ? null : (fields[8].toString()));
		this.setTrangThai((fields[9] == null) ? 0 : ((BigDecimal) fields[9]).intValue());
		this.setFileDinhKem((fields[10] == null) ? null : (fields[10].toString()));
		this.setNamCanhBao((fields[11] == null) ? null : (fields[11].toString()));
		this.setKyBaoCao((fields[12] == null) ? null : (fields[12].toString()));
		this.setThoiHanGui((fields[13] == null) ? null : ((Timestamp) fields[13]));
		this.setTenBaoCao((fields[14] == null) ? null : (fields[14].toString()));
		this.setIsValidGuiBc(fields[15] == null ? null : ((BigDecimal) fields[15]).intValue() == 1 ? true : false);
		this.setFileBaoCao((fields[16] == null) ? null : (fields[16].toString()));
	}

}
