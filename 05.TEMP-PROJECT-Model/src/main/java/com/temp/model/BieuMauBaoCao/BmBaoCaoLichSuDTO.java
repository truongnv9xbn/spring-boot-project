package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.sql.Timestamp;

import com.temp.model.NguoiDung.QtNguoiDungDTO;
import com.temp.utils.LocalDateTimeUtils;
import com.temp.utils.TimestampUtils;

import lombok.Data;


/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmBaoCaoLichSuDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Timestamp thoiGian;
    private QtNguoiDungDTO nguoiThucHien;
    private String thaoTac;
    private Integer bmId;
    private Integer bmBaoCaoId;
    private Integer suDung;
    private Integer phienBan;
    private String thoiGianFormat;

     
    public Timestamp getThoiGian() {
		return thoiGian;
	}

	public void setThoiGian(Timestamp thoiGian) {
		this.thoiGian = thoiGian;
		if(this.thoiGian!=null) {
		this.setThoiGianFormat(TimestampUtils.TimestampToString_ddMMyyyyHHmm(thoiGian));
		}
		
		
	}
	

	public BmBaoCaoLichSuDTO() {
	super();
    }

	public BmBaoCaoLichSuDTO(QtNguoiDungDTO nguoiThucHien, String thaoTac, Integer bmId, Integer bmBaoCaoId, Integer suDung,
			Integer phienBan, String tenNguoiThucHien) {
		super();
		this.thoiGian=LocalDateTimeUtils.getCurrentTimestamp();
		this.nguoiThucHien = nguoiThucHien;
		this.thaoTac = thaoTac;
		this.bmId = bmId;
		this.bmBaoCaoId = bmBaoCaoId;
		this.suDung = suDung;
		this.phienBan = phienBan;
	}

}