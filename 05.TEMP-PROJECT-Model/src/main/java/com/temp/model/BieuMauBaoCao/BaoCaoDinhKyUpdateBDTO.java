package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.List;

import com.temp.model.DropDownDTO;
import com.temp.model.DropDownStringDTO;

import lombok.Getter;
import lombok.Setter;

public class BaoCaoDinhKyUpdateBDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Thanh vien DTO.
     */
    @Getter
    @Setter
    private BcThanhVienDTO bcThanhVienDto;

    /**
     * ky bao cao text
     */
    @Getter
    @Setter
    private String kyBaoCaoText;

    /**
     * Gia tri ky selected
     */
    @Getter
    @Setter
    private DropDownStringDTO giaTriKySelected;

    /**
     * Bao cao selected
     */
    @Getter
    @Setter
    private DropDownDTO bmBaoCaoSelected;

    /**
     * To chuc kiem toan selected id
     */
    @Getter
    @Setter
    private DropDownDTO toChucKiemToanSelectedId;

    /**
     * Cong ty kiem toan options list
     */
    @Getter
    @Setter
    private List<DropDownDTO> toChucKiemToanOptions;

    /**
     * Kiem toan vien selected id
     */
    @Getter
    @Setter
    private DropDownDTO kiemToanVienSelectedId;

    /**
     * Kiem toan vien options list
     */
    @Getter
    @Setter
    private List<DropDownDTO> kiemToanVienOptions;

    /**
     * list sheet id.
     */
    @Getter
    @Setter
    private List<DropDownDTO> lsSheetId;

    /**
     * sheet id detail data.
     */
    @Getter
    @Setter
    private BmSheetDTO bmSheetDto;
    
    /**
     * .Ten cong ty CK
     */
    @Getter
    @Setter
    private String congtyCk;
    
    /**
     * Bm Bao Cao DTO
     */
    @Getter
    @Setter
    private BmBaoCaoDTO bmBaoCao;

    public BaoCaoDinhKyUpdateBDTO() {
	// TODO Auto-generated constructor stub
    }

}
