package com.temp.model.BieuMauBaoCao;

import java.math.BigDecimal;
import java.util.Collection;

import lombok.Data;

@Data
public class BmTieuDeHangDTO {
    private Integer id;
    private Long thuTu;
    private Long phienBan;
    private BmSheetDTO bmSheet;
    private Long sheetId;
    private Collection<BmTieuDeHangCotDTO> bmTieuDeHangCotCollection;

    public BmTieuDeHangDTO() {

    }

    /**
     * Gen header excel.
     * 
     * @param objects
     */
    public BmTieuDeHangDTO(Object... objects) {
	this.setId(objects[0] == null ? 0 : ((BigDecimal) objects[0]).intValue());
	this.setSheetId(objects[1] == null ? 0 : ((BigDecimal) objects[1]).longValue());
	this.setThuTu(objects[2] == null ? 0 : ((BigDecimal) objects[2]).longValue());
	this.setPhienBan(objects[3] == null ? 0 : ((BigDecimal) objects[3]).longValue());
    }

}
