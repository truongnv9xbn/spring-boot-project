package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */

public class BmSheetCtBDTO extends PageInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	public List<BmSheetCtDTO> lstCt;

	public BmSheetCtBDTO() {
		this.lstCt = new ArrayList<>();
	}

}