package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.temp.utils.LocalDateTimeUtils;

import lombok.Data;


/**
 * The persistent class for the BM_SHEET database table.
 * 
 */

@Data
public class BmSheetDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer id;
	private String maSheet;
	private String maSheetLT;
	private Integer bmSheetId;
	private String tenSheet;
	private String tieuDeChinh;
	private String tieuDePhu;
	private Boolean trangBia;
	private String khoGiay;
	private String chieuGiay;
	private Long thuTu;
	private String ghiChu;
	private String nguoiKy;
	private String kieuBaoCao;
	private Boolean suDung;
	private Long trangThai;
	private Boolean trangThaibool;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Timestamp ngayCapNhat;
	private String viTriTieuDePhu;
	private String canTieuDePhu;
	private BmBaoCaoDTO bmBaoCaoByBmBaoCaoId;
	private String tableHtmlRaw;
	private String sheetNameExcel;

	private String thongTinCongTy;
	private String quocHieu;
	private String ngayTaoStr;

	private Collection<BmSheetCotDTO> bmSheetCotCollection;
	private Collection<BmSheetHangDTO> bmSheetHangCollection;
	private Collection<BmTieuDeHangDTO> bmTieuDeHangCollection;
	private Collection<BmTieuDeHangCotDTO> bmTieuDeHangCotCollection;
	private Collection<BmSheetCtDTO> bmSheetCtCollection;

	private List<String> arrNguoiKy;

	private List<String> arrTieuDePhu;
	
	public BmSheetDTO(Object... fields) {
		if(fields.length == 5) {
			this.id = ((BigDecimal) fields[0]).intValue();
			this.bmSheetId = ((BigDecimal) fields[1]).intValue();
			this.maSheetLT= fields[2] != null ? (String) fields[2] : "";
			this.nguoiKy= fields[3] != null ? (String) fields[3] : "";
			this.ngayTao = fields[4] != null ? (Timestamp)fields[4] : null;
			if(this.ngayTao != null) {
				this.ngayTaoStr = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayTao);
			}
		}
	}

	public String getSheetNameExcel() {
		return sheetNameExcel;
	}
	
	public void setSheetNameExcel(String sheetNameExcel) {
		this.sheetNameExcel = sheetNameExcel;
	}

	// bs: thanhVienId
	private Integer thanhVienId;

	public String getNguoiKy() {
		return nguoiKy;
	}

	public void setNguoiKy(String nguoiKy) {
		if (nguoiKy != null) {
			arrNguoiKy = new ArrayList<String>();
			this.arrNguoiKy = Stream.of(nguoiKy.split(",")).collect(Collectors.toList());
			this.arrNguoiKy = arrNguoiKy.stream().filter(line -> !"".equals(line)).collect(Collectors.toList());

		}

		this.nguoiKy = nguoiKy;
	}

	public String getTieuDePhu() {
		return tieuDePhu;
	}

	public void setTieuDePhu(String tieuDePhu) {
		this.tieuDePhu = tieuDePhu;

		if (tieuDePhu != null) {
			arrTieuDePhu = new ArrayList<String>();
			this.arrTieuDePhu = Stream.of(tieuDePhu.split("\\|")).collect(Collectors.toList());
			this.arrTieuDePhu = arrTieuDePhu.stream().filter(line -> !"".equals(line)).collect(Collectors.toList());

		}

	}

	public BmSheetDTO() {
		super();
	}

	public BmSheetDTO(Integer id) {
		super();
		this.id = id;
	}

	public BmSheetDTO(Integer id, String maSheet, String tenSheet, Long trangThai) {
		super();
		this.id = id;
		this.maSheet = maSheet;
		this.tenSheet = tenSheet;
		this.trangThai = trangThai;
	}
	
}