package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmSheetMegerCellDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer soHang;
    private Integer soCot;
    private List<BmTieuDeHangCotDTO> lstCot;
    private List<BmTieuDeHangDTO> lstHang;

    private String headerTableStr;

    private List<BmSheetCotDTO> lsSheetCot;
}