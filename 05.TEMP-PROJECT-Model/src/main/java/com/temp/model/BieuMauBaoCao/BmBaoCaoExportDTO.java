package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.List;

import com.temp.model.uploadfile.UploadFileDefaultDTO;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmBaoCaoExportDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer thanhVienId;
	private List<UploadFileDefaultDTO> arrFileDinhKem;
	public List<BmSheetDTO> listBmSheet;
	private Integer khaiThacId;

 
	

}