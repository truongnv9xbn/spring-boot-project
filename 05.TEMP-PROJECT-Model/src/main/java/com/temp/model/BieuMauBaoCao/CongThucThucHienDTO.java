package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

public class CongThucThucHienDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Nam thuc hien canh bao.
     */
    @Getter
    @Setter
    private String namCanhBao;

    /**
     * Ky thuc hien canh bao.
     */
    @Getter
    @Setter
    private String kyCanhBaoCanhBao;
    
    /**
     *  bao cao ID
     */
    @Getter
    @Setter
    private String bmBaoCaoId;

    /**
     * Sheet bao cao ID
     */
    @Getter
    @Setter
    private String bmSheetId;

    /**
     * Sheet hang ID
     */
    @Getter
    @Setter
    private String bmSheetHangId;

    /**
     * Sheet cot ID
     */
    @Getter
    @Setter
    private String bmSheetCotId;

    /**
     * Sheet chi tieu ID
     */
    @Getter
    @Setter
    private String bmSheetChiTieuId;

    /**
     * Co/Khong su dung ham: 1/0
     */
    @Getter
    @Setter
    private String sudungHam;

    /**
     * Ham tinh toan: SUM, COUTIF ...
     */
    @Getter
    @Setter
    private String hamTinhToan;

    /**
     * Ham: Sheet hang ID (Tu hang)
     */
    @Getter
    @Setter
    private String tuHangId;

    /**
     * Ham: Sheet hang ID (Den hang)
     */
    @Getter
    @Setter
    private String denHangId;

    /**
     * Ten o chi tieu
     */
    @Getter
    @Setter
    private String tenOChiTieu;

    /**
     * Gia tri o chi tieu. exp: [19], {SUM,[5]|[6]}
     */
    @Getter
    @Setter
    private String giatriOChitieu;

    /**
     * Phep toan exp: +,-,*,/
     */
    @Getter
    @Setter
    private String phepToan;

    /**
     * Dieu kien CIF exp: ==, < , > ...
     */
    @Getter
    @Setter
    private String dieuKienCif;

    /**
     * Gia tri khi su dung ham CIF
     */
    @Getter
    @Setter
    private String giaTriCif;

    /**
     * STT phep toan.
     */
    @Getter
    @Setter
    private String stt;

    /**
     * He So phep toan.
     */
    @Getter
    @Setter
    private String heSo;
    
    @Getter
    @Setter
    private String dinhDangKyTu;
    

}
