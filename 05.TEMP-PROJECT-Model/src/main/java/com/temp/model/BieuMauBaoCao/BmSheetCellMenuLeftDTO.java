package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmSheetCellMenuLeftDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer SheetId;

	private String tenHang;
	private String tenCot;

	public BmSheetCellMenuLeftDTO(Object... fields) {
		super();

		this.id = ((BigDecimal) fields[0]).intValue();

		this.SheetId = ((BigDecimal) fields[1]).intValue();

		this.tenHang = (String) fields[2];
		this.tenCot = (String) fields[3];
	}

}