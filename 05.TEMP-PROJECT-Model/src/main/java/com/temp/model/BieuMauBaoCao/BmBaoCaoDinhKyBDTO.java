package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

public class BmBaoCaoDinhKyBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<BmBaoCaoDinhKyDTO> lstBmBaoCaoDinhKy;

	public BmBaoCaoDinhKyBDTO() {
		lstBmBaoCaoDinhKy = new ArrayList<BmBaoCaoDinhKyDTO>();
	}

}
