package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.util.List;

import com.temp.model.DropDownDTO;

import lombok.Getter;
import lombok.Setter;

public class BaoCaoDinhKyImportDataBDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private List<BmSheetDTO> detailImportSheet;

    @Getter
    @Setter
    private List<DropDownDTO> lsSheetImport;

}
