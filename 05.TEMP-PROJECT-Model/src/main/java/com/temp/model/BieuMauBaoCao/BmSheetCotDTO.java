package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import lombok.Data;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Data
public class BmSheetCotDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String maCot;
	private String maCotLT;
	private String tenCot;
	private Long thuTu;
	private String dinhDangTen;
	private String mauSac;
	private String canLe;
	private Boolean cotDong;
	private Boolean khoaCot;
	private String moTa;
	private Timestamp ngayCapNhat;
	private Boolean suDung;
	private Long phienBan;
	private BmBaoCaoDTO bmBaoCaoByBmBaoCaoId;
	private BmSheetDTO bmSheetByBmSheetId;
	private Integer bmSheetId;
	private Integer index;

	public BmSheetCotDTO() {

	}

	/**
	 * Lay du lieu BmSheetCot Sd cho chuc nang: Thiet lap sheet cot header.
	 * 
	 * @param objects
	 */
	public BmSheetCotDTO(Object... objects) {
		this.setId((objects[0] == null) ? 0 : ((BigDecimal) objects[0]).intValue());
		this.setBmSheetId((objects[1] == null) ? 0 : ((BigDecimal) objects[1]).intValue());
		this.setMaCot((objects[2] == null) ? null : objects[2].toString());
		this.setTenCot((objects[3] == null) ? null : objects[3].toString());
	}

}