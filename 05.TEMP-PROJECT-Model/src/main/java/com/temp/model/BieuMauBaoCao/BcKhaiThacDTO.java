package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import com.temp.utils.TimestampUtils;

import lombok.Data;

@Data
public class BcKhaiThacDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Integer bmBaoCaoId;
    private Timestamp tuNgay;
    private Timestamp denNgay;
    private Timestamp ngayTongHop;
    private Integer nguoiDungId;
    private String maBaoCao;
    private String tenBaoCao;
    private String ngayTongHopStr;
    private String ngayTuStr;
    private String ngayDenStr;
    private String nguoiTongHop;
    private String nam;
    private String thang;
    private String bannien;
    private String quy;
    private String lstDonVi;
    private String lstBaoCaoDinhKy;
    private String giaTriKyBaoCao;
    private String kyBaoCao;
    private Integer bmBcDkId;
    private String kyBaoCaoStr;
    private String lstDonViStr;
    private String ctck;
    private Integer donViTinh;
    private String donViTinhStr;
    private String giaTriKyBaoCaoFrom;
    private String giaTriKyBaoCaoTo;
    private Boolean isKhoangTg;
    private String namFrom;
    private String namTo;
    private Integer bcktId;
    
    public BcKhaiThacDTO(Object... fields) {
    	super();
		this.id = fields[0] == null ? null : ((BigDecimal) fields[0]).intValue();
		this.bmBaoCaoId = fields[1] == null ? null : ((BigDecimal) fields[1]).intValue();
		this.ngayTuStr = fields[2] == null ? null : ((String) TimestampUtils.TimestampToString_ddMMyyyy((Timestamp)fields[2]));
		this.ngayDenStr = fields[3] == null ? null : ((String) TimestampUtils.TimestampToString_ddMMyyyy((Timestamp)fields[3]));
		this.ngayTongHopStr = fields[4] == null ? null : ((String) TimestampUtils.TimestampToString_ddMMyyyyHHmmss((Timestamp)fields[4]));
		this.bmBaoCaoId = fields[5] == null ? null : ((BigDecimal) fields[5]).intValue();
		this.tenBaoCao = fields[6] == null ? null : ((String) fields[6]);
		this.maBaoCao = fields[7] == null ? null : ((String) fields[7]);
		this.nguoiTongHop = fields[8] == null ? null : ((String) fields[8]);
		this.lstDonViStr = fields[9] == null ? null : ((String) fields[9]);
		this.kyBaoCaoStr = fields[10] == null ? null : ((String) fields[10]);
		this.donViTinh = fields[11] == null ? null : ((BigDecimal) fields[11]).intValue();
		if (fields[12] !=null) {
			if (fields[12] instanceof Boolean) {
				this.isKhoangTg = (Boolean) fields[12];
			} else {
				this.isKhoangTg = ((BigDecimal) fields[12]).intValue() == 1 ? true : false;
			}
		}
		this.bcktId = fields[13] == null ? null : ((BigDecimal) fields[13]).intValue();
    }
    
    public BcKhaiThacDTO() {
	// TODO Auto-generated constructor stub
    }
   
    
    
}
