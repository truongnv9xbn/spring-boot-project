package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class KyBcGiaTriBcDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private String kyBaoCao;
    private String giaTriKyBc;

	public KyBcGiaTriBcDTO() {
		super();
	}
	
	public KyBcGiaTriBcDTO(Object... fields) {
		super();
		this.kyBaoCao = fields[0] != null ? (String) fields[0] : "";
		this.giaTriKyBc = fields[1] != null ? (String) fields[1] : "";
	}

}
