/**
 * 
 */
package com.temp.model.BieuMauBaoCao;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * BcBaoCaoGtDongDTO
 * 
 * @author HienDuc
 *
 */
public class BcBaoCaoGtDongDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private Integer bcThanhVienId;

    @Getter
    @Setter
    private Integer bmBaoCaoId;

    @Getter
    @Setter
    private Integer bmSheetHang;

    @Getter
    @Setter
    private Integer ctckThongTinId;

    @Getter
    @Setter
    private String giaTri;

    public BcBaoCaoGtDongDTO() {
	// TODO Auto-generated constructor stub
    }

}
