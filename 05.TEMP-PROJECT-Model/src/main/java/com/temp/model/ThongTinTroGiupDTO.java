package com.temp.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.uploadfile.UploadFileDefaultDTO;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Getter
@Setter
public class ThongTinTroGiupDTO implements Serializable {
	private static final Long serialVersionUID = 1L;
	private Integer id;
	private String tenTaiLieu;
	private String fileDinhKem;
	private String ghiChu;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Timestamp ngayCapNhat;
	private String ngayCapNhatStr;
	private Integer trangThai;
	private List<UploadFileDefaultDTO> arrFileDinhKem;
	public ThongTinTroGiupDTO() {}
     
}