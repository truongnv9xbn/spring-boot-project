/**
 * 
 */
package com.temp.model.file;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author thangnn
 *
 */
@Getter
@Setter
public class ChucNangUserDto {
	private int id;
	private Integer khoaChaId;
	private String tenChucNang;
	private Integer actionId;
	private String icon;
	private Boolean menu;
	private Integer nguoiTaoId;
	private Timestamp ngayCapNhat;
	private Boolean deleted;
	private Boolean trangThai;
	private String vueRoute;
	private String apiRoute;
	private String tenChucNangRoute;
	private String header;
	private Integer thuTu;

	private List<ChucNangUserDto> children = new ArrayList<ChucNangUserDto>();

	public ChucNangUserDto(Object... fields) {
		super();
		this.id = ((Integer) fields[0]).intValue();
		this.tenChucNang = (String) fields[1];
		this.khoaChaId = (Integer) fields[2];
		this.actionId = ((Integer) fields[3]).intValue();
		this.icon = (String) fields[4];
		this.menu = (Boolean) fields[5];
		this.nguoiTaoId = (Integer) fields[6];
		this.deleted = (Boolean) fields[7];
		this.trangThai = (Boolean) fields[8];
		this.vueRoute = (String) fields[9];
		this.apiRoute = (String) fields[10];
		this.tenChucNangRoute = (String) fields[11];
		this.thuTu = 0;
		if(fields[12] != null) {
			this.thuTu = (Integer) fields[12];
		}
	}

	public ChucNangUserDto() {

	}

	public void addChildrenItem(ChucNangUserDto children) {
		if (!this.children.contains(children))
			this.children.add(children);
	}

}
