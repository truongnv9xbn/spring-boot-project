/**
 * 
 */
package com.temp.model.file;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class FileBieuMauDTO {

	private String fileName;
	private String fileNameOriginal;
	private String pathFile;
	private long size;
	private String fileDownloadUri;
	private String extenstionFile;
	private Integer bmBaoCaoId;
	private Integer kyBaoCaoId;
	private String giaTriKyBc;
}
