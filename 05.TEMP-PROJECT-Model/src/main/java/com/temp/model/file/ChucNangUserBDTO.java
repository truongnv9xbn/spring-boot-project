/**
 * 
 */
package com.temp.model.file;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author thangnn
 *
 */
@Getter
@Setter
public class ChucNangUserBDTO {
	private List<ChucNangUserDto> lstChucNangMenu;
	private List<String> lstChucNangQuyen;
	private String menuGenerate;
}
