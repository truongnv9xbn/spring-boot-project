/**
 * 
 */
package com.temp.model.file;

import java.util.List;

import com.temp.model.NguoiDung.ChungThuSoDTO;

import lombok.Data;

/**
 * @author thangnn
 *
 */
@Data
public class UploadPathDTO {

	private String fileName;
	private String fileNameOriginal;
	private String pathFile;
	private long size;
	private String fileDownloadUri;
	private String extenstionFile;
	private String message;
}
