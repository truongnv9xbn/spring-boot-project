package com.temp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.BieuMauBaoCao.BcThanhVienDTO;
import com.temp.model.BieuMauBaoCao.BcThanhVienJoinDTO;
import com.temp.model.BieuMauBaoCao.BmBaoCaoDTO;
import com.temp.model.BieuMauBaoCao.BmSheetDTO;

import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("serial")
public class ThongTinDuLieuBcDetailBDTO implements Serializable {

    @Getter
    @Setter
    private List<Integer> lsSheetIds;

    @Getter
    @Setter
    private BcThanhVienDTO bcThanhVienDto;

    @Getter
    @Setter
    private BmBaoCaoDTO bmBaoCaoDTO;

    @Getter
    @Setter
    private BmSheetDTO bmSheetDto;

    @Getter
    @Setter
    private List<DropDownDTO> lsSheetNameAndId;

    @Getter
    @Setter
    private BcThanhVienJoinDTO bcThanhVienJoinDto;

    public ThongTinDuLieuBcDetailBDTO() {
	this.lsSheetIds = new ArrayList<Integer>();
	this.bcThanhVienDto = new BcThanhVienDTO();
	this.bmBaoCaoDTO = new BmBaoCaoDTO();
	this.bcThanhVienJoinDto = new BcThanhVienJoinDTO();
    }
}
