package com.temp.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DropDownStringDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String value;
    private String label;

    public DropDownStringDTO(Object... fields) {
	super();
	this.value = (String) fields[0];
	this.label = (String) fields[1];
    }

    public DropDownStringDTO(boolean isLoadCongThuc, Object... fields) {
	super();
	this.value = (fields[0] == null) ? "" : (String) fields[0];
	this.label = ((fields[1] == null) ? "" : (String) fields[1]) + " - "
		+ ((fields[2] == null) ? "" : (String) fields[2]);
    }

    public DropDownStringDTO() {
	super();
    }

    public DropDownStringDTO(String value, String label) {
	super();
	this.value = value;
	this.label = label;
    }

}
