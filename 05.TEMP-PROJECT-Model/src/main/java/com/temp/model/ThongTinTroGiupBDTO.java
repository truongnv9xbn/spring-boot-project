package com.temp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ThongTinTroGiupBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<ThongTinTroGiupDTO> lstBDTO;
	

	public ThongTinTroGiupBDTO() {
		lstBDTO = new ArrayList<ThongTinTroGiupDTO>();
	}

}
