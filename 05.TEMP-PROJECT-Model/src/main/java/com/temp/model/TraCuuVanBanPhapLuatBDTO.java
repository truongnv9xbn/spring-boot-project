package com.temp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class TraCuuVanBanPhapLuatBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<TraCuuVanBanPhapLuatDTO> lstBDTO;
	

	public TraCuuVanBanPhapLuatBDTO() {
		lstBDTO = new ArrayList<TraCuuVanBanPhapLuatDTO>();
	}

}
