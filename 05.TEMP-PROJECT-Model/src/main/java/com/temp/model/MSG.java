package com.temp.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import lombok.Getter;
import lombok.Setter;

/**
 * The persistent class for the BM_SHEET database table.
 * 
 */
@Getter
@Setter
@XmlRootElement(name = "msg")
@XmlSeeAlso({MSG.class})
public class MSG implements Serializable {
	
	private String msg;
	private String error;

	public MSG() {
	}

	public MSG(String msg, String error) {
		super();
		this.msg = msg;
		this.error = error;
	}

	@Override
	public String toString() {
		return "MSG [msg=" + msg + ", error=" + error + "]";
	}

}