package com.temp.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ThongBaoBDTO extends PageInfo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<ThongBaoDTO> lstThongBao;
	@Getter
	@Setter
	public Long countNotification;

	public ThongBaoBDTO() {

	}

}
