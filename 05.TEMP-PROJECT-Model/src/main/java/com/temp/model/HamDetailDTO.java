package com.temp.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HamDetailDTO {
	private String key;
	private List<String> value;
	public HamDetailDTO() {
		super();
	}
}
