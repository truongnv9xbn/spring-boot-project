package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.BaseDto;
import com.temp.model.DropDownDTO;


@Getter
@Setter
public class ChucNangDTO extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
//	@Size(max = 10, message = "Khóa cha từ 1 đến 10 ký tự")
	private int khoaChaId;
	
	@NotNull
	@NotEmpty
	@Size(max = 50, message = "Mã chức năng từ 1 đến 10 ký tự")
	private String maChucNang;
	
	@Size(max = 250, message = "Tên chức năng từ 1 đến 10 ký tự")
	private String tenChucNang;
	private Integer actionId;
	private String icon;
	private Boolean menu;
	private Boolean chiTiet;
	private String phanHe;
	private Integer thuTu;
	private String ghiChu;
	private Integer nguoiTaoId;
	private Timestamp ngayTao;
	private Integer nguoiCapNhatId;
	private Timestamp ngayCapNhat;
	private Boolean deleted;
	private Boolean trangThai;
	private Integer level;
	private boolean leaf;
	private boolean expend;
	private String header;
	private Boolean disabled;
	private String actionName;
	
	private DropDownDTO chucNangCha;
	private DropDownDTO action;
	
	private List<ChucNangDTO> children=new ArrayList<ChucNangDTO>();
	
	 public ChucNangDTO() {

	    }
	    
	    public void addChildrenItem(ChucNangDTO children){
	        if(!this.children.contains(children))
	            this.children.add(children);
	    }
	
	
	 
	
	
	
	public ChucNangDTO(Object... fields) {
		super();
        this.id = ((BigDecimal) fields[0]).intValue();
		this.tenChucNang = (String) fields[1];
	}
	public ChucNangDTO(boolean dropdown,Object... fields ) {
		
		super();
        this.id = ((BigDecimal) fields[0]).intValue();
		this.khoaChaId = ((BigDecimal) fields[1]).intValue();
		this.tenChucNang = (String) fields[2];
		this.icon = (String) fields[3];
		this.trangThai=convertBigToBol((BigDecimal) fields[4]);
		this.action= new DropDownDTO();
		this.getAction().setLabel((String) fields[5]);
     
	}

	public ChucNangDTO(int id, boolean trangThai) {
		super();
		this.id = id;
		this.trangThai = trangThai;
	}
	
	private Boolean convertBigToBol(BigDecimal bigDecimal) {
			return (bigDecimal.intValue()==1);
		
	}
	
}
