package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

public class QuyenChiTietBDTO extends PageInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<QuyenChiTietDTO> lstQuyenChiTiet;

	public QuyenChiTietBDTO() {
		lstQuyenChiTiet = new ArrayList<QuyenChiTietDTO>();
	}
}
