package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class QtLichSuTruyCapExportDTO implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private String tuNgay;
    
    @Getter
    @Setter
    private String denNgay;
    
    @Getter
    @Setter
    private String ngayHienTaiStr;
    
    @Getter
    @Setter
    private Timestamp ngayHienTai;
    
    @Getter
    @Setter
    private int tongSoTkUBCN;
    
    @Getter
    @Setter
    private int tongSoThanhVien;
    
    public QtLichSuTruyCapExportDTO() {
	// TODO Auto-generated constructor stub
	tuNgay = "";
	denNgay = "";
	tongSoTkUBCN = 0;
	tongSoThanhVien = 0;
    }
    
}
