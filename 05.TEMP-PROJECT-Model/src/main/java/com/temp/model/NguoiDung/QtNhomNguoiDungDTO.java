package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class QtNhomNguoiDungDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	
	@Size(max = 50, message = " Mã nhóm người dùng từ 1 đến 50 ký tự")
	private String maNhomNguoiDung;
	
	@Size(max = 250, message = " Tên nhóm người dùng từ 1 đến 250 ký tự")
	@NotEmpty(message = " Tên nhóm người dùng Không được bỏ trống")
	private String tenNhomNguoiDung;

	@Size(max = 1000, message = "ghi chú từ 1 đến 1000 ký tự")
	private String ghiChu;
	
	private String nguoiTao;

	private Timestamp ngayTao;

//	@Size(max = 10, message = " Khóa cha Id từ 1 đến 10 ký tự")
	private Integer khoaChaId;

	private String nguoiCapNhat;

	private Timestamp ngayCapNhat;

	private Boolean trangThai;
	
	public List<Integer> listIdChucNang = new ArrayList<Integer>();
	
	private List<QtNhomNguoiDungDTO> children=new ArrayList<QtNhomNguoiDungDTO>();
	
//	private Timestamp ngayBatDauDung;
	
//	private String ngayHetHanFormat;
//	
//	private String ngayBatDauDungFormat;
//	
//	private Timestamp ngayHetHan;
	
//	private Integer isThanhVien;
	
	
    public void addChildrenItem(QtNhomNguoiDungDTO children){
        if(!this.children.contains(children))
            this.children.add(children);
    }


	public QtNhomNguoiDungDTO(Object... fields) {
		
		this.id = ((BigDecimal) fields[0]).intValue();
		this.tenNhomNguoiDung = (String) fields[1];
		//this.khoaChaId = ((BigDecimal) fields[2]).intValue();

	}
	
	public QtNhomNguoiDungDTO() {}
	

}
