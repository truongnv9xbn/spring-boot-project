package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

public class ChungThuSoDTO implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public Integer id;
	@Getter
	@Setter
	public Integer nguoiDungId;
	@Getter
	@Setter
	public String taiKhoan;
	@Getter
	@Setter
	public String keys;
	@Getter
	@Setter
	public String serial;
	@Getter
	@Setter
	public String subject;
	@Getter
	@Setter
	public String issuer;
	@Getter
	@Setter
	public Boolean isCaNhan;
	@Getter
	@Setter
	public Integer nguoiTaoId;
	@Getter
	@Setter
	public Timestamp ngayTao;
	@Getter
	@Setter
	public Integer nguoiCapNhatId;
	@Getter
	@Setter
	public Timestamp ngayCapNhat;
	@Getter
	@Setter
	public Integer trangThai;
	@Getter
	@Setter
	public String nguoiDung;
	@Getter
	@Setter
	public String caNhanToChuc;
	@Getter
	@Setter
	public String trangThaiStr;
	
	@Getter
	@Setter
	public String tenCongTy;
	
	public ChungThuSoDTO() {

	}

}
