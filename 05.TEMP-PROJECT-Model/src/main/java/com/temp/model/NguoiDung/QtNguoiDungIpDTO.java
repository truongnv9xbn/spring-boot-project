package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class QtNguoiDungIpDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	private int id;
	@Getter
	@Setter
	private Timestamp thoiGianDangNhap;
	@Getter
	@Setter
	private Boolean hoatDong;
	@Getter
	@Setter
	@Size(max = 50, message = "ip từ 1 đến 50 ký tự")
	@NotEmpty(message = "Ip không được bỏ trống")
	private String ip;

	@Getter
	@Setter
	private Integer nguoiDungId;
	
	@Getter
	@Setter
	private String nguoiDungHoTen;

	public QtNguoiDungIpDTO() {
	}

}
