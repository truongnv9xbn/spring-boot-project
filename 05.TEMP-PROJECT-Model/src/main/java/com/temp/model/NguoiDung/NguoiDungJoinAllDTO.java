/**
 * 
 */
package com.temp.model.NguoiDung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.temp.model.file.ChucNangUserDto;
import com.temp.utils.LocalDateTimeUtils;
import com.temp.utils.Utils;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ninhnh
 *
 */
public class NguoiDungJoinAllDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public int id;
	@Getter
	@Setter
	@Size(max = 50, message = "Mã quản trị người dùng có kí tự từ 1 đến 50 ký tự")
	public String maNguoiDung;
	@Getter
	@Setter
	@Size(max = 50, message = "Tài khoản quản trị người dùng có kí tự từ 1 đến 50 ký tự")
	@NotEmpty(message = "Tài khoản Không được bỏ trống")
	public String taiKhoan;
	@Getter
	@Setter
	@Size(max = 250, message = "Mật khẩu quản trị người dùng có kí tự từ 1 đến 250 ký tự")
	public String matKhau;
	@Getter
	@Setter
	@Size(max = 250, message = "Mật khẩu Default quản trị người dùng có kí tự từ 1 đến 250 ký tự")
	public String matKhauDefault;
	@Getter
	@Setter
	@NotEmpty(message = "Họ tên Không được bỏ trống")
	@Size(max = 250, message = "Họ tên người dùng có kí tự từ 1 đến 250 ký tự")
	public String hoTen;
	@Getter
	@Setter
	@Size(max = 100, message = "Email quản trị người dùng có kí tự từ 1 đến 100 ký tự")
	public String email;
	@Getter
	@Setter
	@Size(max = 50, message = "Di động quản trị người dùng có kí tự từ 1 đến 50 ký tự")
	public String diDong;
	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú có kí tự từ 1 đến 1000 ký tự")
	public String ghiChu;
	@Getter
	@Setter
	public String nguoiTao;
	@Getter
	@Setter
	public Timestamp ngayTao;
	@Getter
	@Setter
	public String ngayTaoStr;
	@Getter
	@Setter
	public String nguoiCapNhat;
	@Getter
	@Setter
	public Timestamp ngayCapNhat;
	@Getter
	@Setter
	public String ngayCapNhatStr;
	@Getter
	@Setter
	public Timestamp ngayHetHan;
	@Getter
	@Setter
	public String ngayHetHanStr;
	@Getter
	@Setter
	public Boolean trangThai;
	@Getter
	@Setter
	public Boolean admin;
	@Getter
	@Setter
	@Size(max = 4000, message = "TokenUser từ 1 đến 4000 ký tự")
	public String tokenUser;

	@Getter
	@Setter
	public List<String> lstIp;

	@Getter
	@Setter
	public List<Integer> lstNhomNguoiDungId;

	@Getter
	@Setter
	public List<QtNhomNguoiDungDTO> lstNhomNguoiDung;

	@Getter
	@Setter
	public List<Integer> listChucNangIdNhom;

	@Getter
	@Setter
	public List<Integer> listIdTicked;
	
	@Getter
	@Setter
	public List<ChucNangUserDto> listTreeShow;

	@Getter
	@Setter
	private String anhDaiDien;

	@Getter
	@Setter
	private String tenNhomNguoiDung;

	@Getter
	@Setter
	private String ipLogin;

	@Getter
	@Setter
	private String thoiGianLoginStr;

	public NguoiDungJoinAllDTO() {

	}
	
	public NguoiDungJoinAllDTO(Object... fields) {
		super();
		this.id = Utils.parseIntegerValue(fields[0]);
		this.maNguoiDung = Utils.parseStringValue(fields[1]);
		this.taiKhoan = Utils.parseStringValue(fields[2]);
		this.matKhau = Utils.parseStringValue(fields[3]);
		this.matKhauDefault = Utils.parseStringValue(fields[4]);
		this.hoTen = Utils.parseStringValue(fields[5]);
		this.email = Utils.parseStringValue(fields[6]);
		this.diDong = Utils.parseStringValue(fields[7]);
		this.ghiChu = Utils.parseStringValue(fields[8]);
		this.nguoiTao = Utils.parseStringValue(fields[9]);
		this.ngayTao = (Timestamp) fields[10];
		this.ngayTaoStr = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayTao);
		this.nguoiCapNhat = Utils.parseStringValue(fields[11]);
		this.ngayCapNhat = (Timestamp) fields[12];
		this.ngayCapNhatStr = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayCapNhat);
		this.ngayHetHan = (Timestamp) fields[13];
		this.ngayHetHanStr = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayHetHan);
		this.trangThai = Utils.parseBooleanValue(fields[14]);
		this.tokenUser = Utils.parseStringValue(fields[15]);
		this.anhDaiDien = Utils.parseStringValue(fields[16]);
		this.admin = Utils.parseBooleanValue(fields[17]);
		if(fields.length > 18) {
			this.tenNhomNguoiDung = Utils.parseStringValue(fields[18]);
		}
	}
	
//	public NguoiDungJoinAllDTO(Object... fields) {
//		super();
//		if (fields[0] instanceof Integer) {
//			this.id = ((Integer) fields[0]).intValue();
//		} else {
//			this.id = ((BigDecimal) fields[0]).intValue();
//		}
//		this.maNguoiDung = (String) fields[1];
//		this.taiKhoan = (String) fields[2];
//		this.matKhau = (String) fields[3];
//		this.matKhauDefault = (String) fields[4];
//		if (fields[5] instanceof Integer) {
//			this.dmChucVuId = fields[5] != null ? ((Integer) fields[5]).intValue() : null;
//		} else {
//			this.dmChucVuId = fields[5] != null ? ((BigDecimal) fields[5]).intValue() : null;
//		}
//		this.hoTen = (String) fields[6];
//		this.email = (String) fields[7];
//		this.diDong = (String) fields[8];
//		this.chuKySo = false;
//		if (fields[9] != null) {
//			if (fields[9] instanceof Boolean) {
//				this.chuKySo = (Boolean) fields[9];
//			} else {
//				this.chuKySo = ((BigDecimal) fields[9]).intValue() == 1 ? true : false;
//			}
//		}
//		if (fields[10] instanceof Boolean) {
//			this.thanhVien = (Boolean) fields[10];
//		} else {
//			this.thanhVien = ((BigDecimal) fields[10]).intValue() == 1 ? true : false;
//		}
//		if (fields[11] instanceof Integer) {
//			this.ctckId = fields[11] != null ? ((Integer) fields[11]).intValue() : null;
//		} else {
//			this.ctckId = fields[11] != null ? ((BigDecimal) fields[11]).intValue() : null;
//		}
//		this.ghiChu = (String) fields[12];
//
//		if (fields[13] instanceof Integer) {
//			this.nguoiTaoId = fields[13] != null ? ((Integer) fields[13]).intValue() : null;
//		} else {
//			this.nguoiTaoId = fields[13] != null ? ((BigDecimal) fields[13]).intValue() : null;
//		}
//		this.ngayTao = (Timestamp) fields[14];
//		this.ngayHetHan = (Timestamp) fields[15];
//
//		if (fields[16] instanceof Boolean) {
//			this.trangThai = (Boolean) fields[16];
//		} else {
//			this.trangThai = ((BigDecimal) fields[16]).intValue() == 1 ? true : false;
//		}
//		this.tokenUser = (String) fields[17];
//		this.chucVuTen = fields[18] != null ? (String) fields[18] : "Không có";
////		this.tenCtckThongTin = fields[19] != null ? (clobToString((Clob)fields[19])) : null;
//		if (fields[19] instanceof String) {
//			this.tenCtckThongTin = fields[19] != null ? ((String) fields[19]) : null;
//		} else {
//			this.tenCtckThongTin = fields[19] != null ? (clobToString((Clob) fields[19])) : null;
//		}
//		this.anhDaiDien = fields[20] != null ? ((String) fields[20]) : null;
//		if (fields.length > 21 && fields[21] instanceof String) {
//			this.tenNhomNguoiDung = fields[21] != null ? ((String) fields[21]) : null;
//		} else if (fields.length > 21 && fields[21] instanceof Boolean) {
//			this.admin = fields[21] != null ? ((Boolean) fields[21]) : null;
//		}
//		if (fields.length > 23) {
//			if (fields[22] instanceof String) {
//				this.ipLogin = fields[22] != null ? ((String) fields[22]) : null;
//			}
//			if (fields[23] instanceof String) {
//				this.thoiGianLoginStr = fields[23] != null ? ((String) fields[23]) : null;
//			}
//		}
//
//	}

	private String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();
	}
}
