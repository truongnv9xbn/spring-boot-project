package com.temp.model.NguoiDung;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LkChucNangNhomNguoiDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer chucNangId;
	private Integer nhomNguoiDungId;
	private String  chucNangChiTiet;


	public LkChucNangNhomNguoiDTO() {

	}


	public LkChucNangNhomNguoiDTO(Integer chucNangId, Integer nhomNguoiDungId) {
		this.chucNangId = chucNangId;
		this.nhomNguoiDungId = nhomNguoiDungId;
	}
	

}
