/**
 * 
 */
package com.temp.model.NguoiDung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;
import com.temp.utils.LocalDateTimeUtils;

/**
 * @author ninhnh
 *
 */
@Getter
@Setter
public class NhomNguoiDungJoinAllDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;

	@Size(max = 50, message = " Mã nhóm người dùng từ 1 đến 50 ký tự")
	private String maNhomNguoiDung;

	@Size(max = 250, message = " Tên nhóm người dùng từ 1 đến 250 ký tự")
	@NotEmpty(message = " Tên nhóm người dùng Không được bỏ trống")
	private String tenNhomNguoiDung;

	@Size(max = 1000, message = "ghi chú từ 1 đến 1000 ký tự")
	private String ghiChu;

	private Integer nguoiTaoId;

	private Timestamp ngayTao;

//	@Size(max = 10, message = " Khóa cha Id từ 1 đến 10 ký tự")
	private Integer khoaChaId;

	private Integer nguoiCapNhatId;

	private Timestamp ngayCapNhat;

	private Timestamp ngayHetHan;

	private Timestamp ngayBatDauDung;

	private Boolean trangThai;

	public List<Integer> listIdChucNang = new ArrayList<Integer>();

	private List<QtNhomNguoiDungDTO> children = new ArrayList<QtNhomNguoiDungDTO>();

	private String ngayHetHanFormat;

	private String ngayBatDauDungFormat;

	private Integer isThanhVien;

	public List<Integer> listBmBaoCaoIdTicked;

//	public List<BmBaoCaoDTO> listTreeBmBaoCaoShow;

	public List<Integer> listIdTicked;

	public void addChildrenItem(QtNhomNguoiDungDTO children) {
		if (!this.children.contains(children))
			this.children.add(children);
	}

	public NhomNguoiDungJoinAllDTO() {
	}

	public NhomNguoiDungJoinAllDTO(Object... fields) {

		super();
		if (fields[0] instanceof Integer) {
			this.id = ((Integer) fields[0]).intValue();
		} else {
			this.id = ((BigDecimal) fields[0]).intValue();
		}
		this.maNhomNguoiDung = (String) fields[1];

		this.tenNhomNguoiDung = (String) fields[2];

		this.ghiChu = (String) fields[3];

		if (fields[4] instanceof Integer) {
			this.nguoiTaoId = fields[4] != null ? ((Integer) fields[4]).intValue() : null;
		} else {
			this.nguoiTaoId = fields[4] != null ? ((BigDecimal) fields[4]).intValue() : null;
		}

		this.ngayTao = (Timestamp) fields[5];

		if (fields[6] instanceof Integer) {
			this.nguoiCapNhatId = fields[6] != null ? ((Integer) fields[6]).intValue() : null;
		} else {
			this.nguoiCapNhatId = fields[6] != null ? ((BigDecimal) fields[6]).intValue() : null;
		}

		this.ngayCapNhat = (Timestamp) fields[7];

		this.ngayHetHan = (Timestamp) fields[8];

		if (this.ngayHetHan != null) {
			this.ngayHetHanFormat = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayHetHan);
		}

		this.ngayBatDauDung = (Timestamp) fields[9];

		if (this.ngayBatDauDung != null) {
			this.ngayBatDauDungFormat = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayBatDauDung);
		}

		if (fields[10] instanceof Boolean) {
			this.trangThai = (Boolean) fields[10];
		} else {
			this.trangThai = ((BigDecimal) fields[10]).intValue() == 1 ? true : false;
		}

		this.isThanhVien = (Integer) fields[11];

	}

	private String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();
	}
}
