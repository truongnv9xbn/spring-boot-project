package com.temp.model.NguoiDung;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;
import com.temp.utils.LocalDateTimeUtils;

public class QtLichSuTruyCapDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	@Getter
	@Setter
	public int id;

	/**
	 * Ngay NSD
	 */
	@Getter
	@Setter
	public String ipThucHien;

	/**
	 * Ngay Login/logout
	 */
	@Getter
	public Timestamp ngayTao;
	
	public void setNgayTao(Timestamp ngayTao) {
		this.ngayTao = ngayTao;
		this.ngayTaoDate =LocalDateTimeUtils.convertTimestampToStringddMMyyyyHHmmss(ngayTao);
	
	}
	
	/**
	 * Ngay Dang Nhap
	 */
	@Getter
	public Timestamp ngayDangNhap;
	
	
	
	public void setNgayDangNhap(Timestamp ngayDangNhap) {
		this.ngayDangNhap =ngayDangNhap;
		this.ngayDangNhapString =LocalDateTimeUtils.convertTimestampToStringddMMyyyyHHmmss(ngayDangNhap);
	
	}

	/**
	 * Ngay Dang Xuat
	 */
	@Getter
	public Timestamp ngayDangXuat;
	
	public void setNgayDangXuat(Timestamp ngayDangXuat) {
		this.ngayDangXuat = ngayDangXuat;
		this.ngayDangXuatString =LocalDateTimeUtils.convertTimestampToStringddMMyyyyHHmmss(ngayDangXuat);
	
	}

	/**
	 * ID NSD
	 */
	@Getter
	@Setter
	public int nguoiTaoId;

	/**
	 * Ten Tai Khoan
	 */
	@Getter
	@Setter
	public String tenTaiKhoan;

	/**
	 * Loai NSD
	 */
	@Getter
	@Setter
	public boolean loaiNguoiDung;
	
	/**
	 * Log type
	 */
	@Getter
	@Setter
	public String logType;
	
	/**
	 * Ngay tao Datetime
	 */
	@Getter
	@Setter
	public String ngayTaoDate;
	
	@Getter
	@Setter
	public String ngayDangNhapString;
	
	@Getter
	@Setter
	public String ngayDangXuatString;
	
	@Getter
	@Setter
	public String tenCongTy;
	
	@Getter
	@Setter
	public String loaiNguoiDungStr;
	
	public QtLichSuTruyCapDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public QtLichSuTruyCapDTO(Object... fields) {
		super();
		if(fields[0] != null){
			this.setId(((BigDecimal) fields[0]).intValue());
		}
		this.setIpThucHien((String) fields[1]);
		this.setLogType((String) fields[2]);
		this.setNgayTao((Timestamp) fields[3]);
		this.setTenTaiKhoan((String) fields[4]);
		if(fields[5] != null) {
			if(((BigDecimal) fields[5]).intValue() == 1) {
				this.setLoaiNguoiDung(true);
				this.setLoaiNguoiDungStr("Thành viên");
			}else {
				this.setLoaiNguoiDung(false);
				this.setLoaiNguoiDungStr("UBCK");
			}
		}
		if (fields[6] instanceof String) {
			this.tenCongTy = fields[6] != null ? ((String) fields[6]) : null;
		} else {
			this.tenCongTy = fields[6] != null ? (clobToString((Clob) fields[6])) : null;
		}
		this.ngayTaoDate = "";
	}
	private String clobToString(Clob data) {
		StringBuilder sb = new StringBuilder();
		try {
			Reader reader = data.getCharacterStream();
			BufferedReader br = new BufferedReader(reader);

			String line;
			while (null != (line = br.readLine())) {
				sb.append(line);
			}
			br.close();
		} catch (SQLException e) {
			// handle this exception
		} catch (IOException e) {
			// handle this exception
		}
		return sb.toString();
	}

	 
}
