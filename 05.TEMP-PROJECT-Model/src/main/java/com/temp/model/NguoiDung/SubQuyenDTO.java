package com.temp.model.NguoiDung;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class SubQuyenDTO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	
	private int id;
	
	private Boolean trangThai;
	

	public SubQuyenDTO(int id, boolean trangThai) {
		this.id = id;
		this.trangThai = trangThai;
	}
	
}
