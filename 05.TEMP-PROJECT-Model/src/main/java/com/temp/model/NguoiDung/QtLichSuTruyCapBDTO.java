package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.PageInfo;

public class QtLichSuTruyCapBDTO extends PageInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	public List<QtLichSuTruyCapDTO> lstLichSuTruyCap;
	
	@Getter
	@Setter
	public QtLichSuTruyCapExportDTO exportExcelLichSuTruyCap;
	
	@Getter
	@Setter
	public List<QtLichSuTruyCapExportDTO> lstExportExcelLichSuTruyCap;
	
	@Getter
	@Setter
	public String exportTuNgay;
	
	@Getter
	@Setter
	public String exportDenNgay;

	public QtLichSuTruyCapBDTO() {
		lstLichSuTruyCap = new ArrayList<QtLichSuTruyCapDTO>();
		exportExcelLichSuTruyCap = new QtLichSuTruyCapExportDTO();
		lstExportExcelLichSuTruyCap = new ArrayList<QtLichSuTruyCapExportDTO>();
		exportTuNgay = "";
		exportDenNgay = "";
	}

}
