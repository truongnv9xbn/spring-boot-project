package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.math.BigDecimal;

import com.temp.model.DropDownDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChucNangChiTietDTO extends DropDownDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private int id;
	private String tenChucNang;
	private String vueRoute;
	private String apiRoute;
	private String apiTemp;
	private boolean trangThai;
	
	public ChucNangChiTietDTO() {
		super();
	}
	public ChucNangChiTietDTO(Object... fields) {
		super();
		if (fields[0] instanceof BigDecimal) {
			this.id = ((BigDecimal) fields[0]).intValue();
		} else {
			this.id = ((Integer) fields[0]).intValue();
		}
		this.tenChucNang = fields[1] != null ? (String) fields[1] : "";
		this.vueRoute = fields[2] != null ? (String) fields[2] : "";
		this.apiRoute = fields[3] != null ? (String) fields[3] : "";
		
		if (fields[4] instanceof BigDecimal) {
			Integer trangthaiStr = ((BigDecimal) fields[4]).intValue();
			if(trangthaiStr == 1) {
				this.trangThai = true;
			}else {
				this.trangThai = false;
			}
		} else {
			Integer trangthaiStr = ((Integer) fields[4]).intValue();
			if(trangthaiStr == 1) {
				this.trangThai = true;
			}else {
				this.trangThai = false;
			}
		}
		this.apiTemp = fields[5] != null ? (String) fields[5] : "";
	}
}
