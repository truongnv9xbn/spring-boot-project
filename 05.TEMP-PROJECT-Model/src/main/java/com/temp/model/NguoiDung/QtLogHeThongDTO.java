package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.ParseException;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Data;
import com.temp.utils.DateTimeConvertUtil;

@Data
public class QtLogHeThongDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public Integer id;
	
	@Size(max = 250, message = "Ip thực hiện có từ 1 đến 250 ký tự")
	@NotEmpty(message = "Ip thực hiện Không được bỏ trống")
	public String ipThucHien;
	
	@Size(max = 500, message = "Nội Dung có từ 1 đến 500 ký tự")
	public String noiDung;
	
	@Size(max = 50, message = "logType có từ 1 đến 50 ký tự")
	public String logType;
	
	public Integer nguoiTaoId;

	public Timestamp ngayTao;
	
	private String ngayTaoStr;
	
	private Boolean trangThai;
	
	public String taiKhoan;
	
	public QtLogHeThongDTO(Object... fields){
		super();
		this.id = ((Integer) fields[0]).intValue();
		this.ipThucHien = (String) fields[1];
		this.noiDung = (String) fields[2];
		this.logType = (String) fields[3];
		this.nguoiTaoId = fields[4] != null ? ((Integer) fields[4]).intValue() : null;
		
		this.ngayTao = (fields[5] != null ? (Timestamp) fields[5] : null );
		
//		if(this.ngayTao != null) {
//			this.ngayTaoStr = LocalDateTimeUtils.convertTimestampToStringDateTime(this.ngayTao);
//		}
		if(this.ngayTao != null) {
			this.ngayTaoStr = DateTimeConvertUtil.ConvertDateToString_ddMMyyyyHHmm(this.ngayTao);
		}
		this.taiKhoan = (String) fields[6];
	}

	public QtLogHeThongDTO() {

	}

}
