package com.temp.model.NguoiDung;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

public class LkChucNangNguoiDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	@Getter
	@Setter
	@NotNull
	@NotEmpty
	private Integer chucNangId;
	
	@Getter
	@Setter
	@NotNull
	@NotEmpty
	private Integer nguoiDungId;
	
	@Getter
	@Setter
	@NotNull
	@NotEmpty
	private String chucNangChiTiet;
}
