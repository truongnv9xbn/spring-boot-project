package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.util.List;

import com.temp.model.BaseDto;
import com.temp.model.DropDownDTO;

import lombok.Getter;
import lombok.Setter;

public class QuyenBDTO extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	public List<QuyenDTO> listQuyen;
	
	@Getter
	@Setter
	public List<DropDownDTO> listDropdownKhoaChaId;
	
	@Getter
	@Setter
	public List<DropDownDTO> listDropdownAction;

 

	
	
}
