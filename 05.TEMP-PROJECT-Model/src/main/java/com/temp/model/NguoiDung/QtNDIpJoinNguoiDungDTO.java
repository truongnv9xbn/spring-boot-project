/**
 * 
 */
package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author ninhnh
 *
 */
public class QtNDIpJoinNguoiDungDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private int id;
	@Getter
	@Setter
	private Timestamp thoiGianDangNhap;
	@Getter
	@Setter
	private Boolean hoatDong;
	@Getter
	@Setter
	@Size(max = 50, message = "ip từ 1 đến 50 ký tự")
	@NotEmpty(message = "Ip không được bỏ trống")
	private String ip;

	@Getter
	@Setter
	private int nguoiDungId;

	@Getter
	@Setter
	private String nguoiDungHoTen;

	public QtNDIpJoinNguoiDungDTO() {

	}

	public QtNDIpJoinNguoiDungDTO(Object... fields) {
		super();
		this.setId(((Integer) fields[0]).intValue());
		this.setNguoiDungId(((Integer) fields[1]).intValue());
		this.setNguoiDungHoTen((String) fields[2]);
		this.setThoiGianDangNhap((Timestamp) fields[3]);
		this.setIp((String) fields[4]);
		this.setHoatDong(((Boolean) fields[5]).booleanValue());
	}
}
