package com.temp.model.NguoiDung;

import java.io.Serializable;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class LkNguoiDungNhomDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	@Size(max = 10, message = "Người dùng Id có kí tự từ 1 đến 10 ký tự")
	private int nguoiDungId;
	@Getter
	@Setter
	@Size(max = 10, message = "Nhóm người dùng Id có kí tự từ 1 đến 10 ký tự")
	private int nhomNguoiDungId;

	public LkNguoiDungNhomDTO() {

	}

}
