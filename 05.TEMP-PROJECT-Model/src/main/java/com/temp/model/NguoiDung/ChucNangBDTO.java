package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.BaseDto;
import com.temp.model.DropDownDTO;

public class ChucNangBDTO extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Getter
	@Setter
	public List<ChucNangDTO> listChucNang;
	
	@Getter
	@Setter
	public List<DropDownDTO> listDropdownKhoaChaId;
	
	@Getter
	@Setter
	public List<DropDownDTO> listDropdownAction;

 

	
	
}
