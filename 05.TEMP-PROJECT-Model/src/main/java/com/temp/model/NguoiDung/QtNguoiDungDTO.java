package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.temp.model.uploadfile.UploadFileDefaultDTO;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class QtNguoiDungDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public Integer id;
	@Getter
	@Setter
	@Size(max = 50, message = "Mã quản trị người dùng có kí tự từ 1 đến 50 ký tự")
	public String maNguoiDung;
	@Getter
	@Setter
	@Size(max = 50, message = "Tài khoản quản trị người dùng có kí tự từ 1 đến 50 ký tự")
	@NotEmpty(message = "Tài khoản Không được bỏ trống")
	public String taiKhoan;
	@Getter
	@Setter
	@Size(max = 250, message = "Mật khẩu quản trị người dùng có kí tự từ 1 đến 250 ký tự")
	public String matKhau;
	@Getter
	@Setter
	@Size(max = 250, message = "Mật khẩu Default quản trị người dùng có kí tự từ 1 đến 250 ký tự")
	public String matKhauDefault;
	
	@Getter
	@Setter
	@NotEmpty(message = "Họ tên Không được bỏ trống")
	@Size(max = 250, message = "Họ tên người dùng có kí tự từ 1 đến 250 ký tự")
	public String hoTen;
	@Getter
	@Setter
	@Size(max = 100, message = "Email quản trị người dùng có kí tự từ 1 đến 100 ký tự")
	public String email;
	@Getter
	@Setter
	@Size(max = 50, message = "Di động quản trị người dùng có kí tự từ 1 đến 50 ký tự")
	public String diDong;

	@Getter
	@Setter
	@Size(max = 1000, message = "Ghi chú có kí tự từ 1 đến 1000 ký tự")
	public String ghiChu;
	@Getter
	@Setter
	public String nguoiTao;
	@Getter
	@Setter
	public Timestamp ngayTao;
	@Getter
	@Setter
	public Timestamp ngayHetHan;
	@Getter
	@Setter
	public String ngayHetHanStr;
	@Getter
	@Setter
	public Boolean trangThai;
	@Getter
	@Setter
	public Boolean admin;
	@Getter
	@Setter
	@Size(max = 4000, message = "TokenUser từ 1 đến 4000 ký tự")
	public String tokenUser;
	
	@Getter
	@Setter
	public String nguoiCapNhat;
	
	@Getter
	@Setter
	public Timestamp ngayCapNhat;

	@Getter
	@Setter
	public List<String> lstIp;
	
	@Getter
	@Setter
	public List<Integer> lstNhomNguoiDung;

	@Getter
	@Setter
	private String anhDaiDien;

	@Getter
	@Setter
	private String quyenJson;
	
	@Getter
	@Setter
	private List<UploadFileDefaultDTO> arrFileDinhKem;
 
//	@Getter
//	@Setter
//	public Integer nguoiTaoId;
	
//	@Getter
//	@Setter
//	public Boolean thanhVien;
	
//	@Getter
//	@Setter
//	public Integer dmChucVuId;
	
//	@Getter
//	@Setter
//	public Boolean chuKySo;
//	
//	@Getter
//	@Setter
//	public Integer ctckId;
	
//	@Getter
//	@Setter
//	public List<Integer> lstCtckId;
	
//	@Getter
//	@Setter
//	public List<CtckThongTinJoinAllDTO> lstCtck;
	
	public QtNguoiDungDTO() {

	}

}
