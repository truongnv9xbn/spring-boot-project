package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.temp.model.PageInfo;

import lombok.Getter;
import lombok.Setter;

public class ChucNangChiTietBDTO extends PageInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<ChucNangChiTietDTO> lstChucNangChiTiet;

	public ChucNangChiTietBDTO() {
		lstChucNangChiTiet = new ArrayList<ChucNangChiTietDTO>();
	}
}
