package com.temp.model.NguoiDung;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import com.temp.model.PageInfo;

public class ChungThuSoBDTO extends PageInfo implements Serializable{
	private static final long serialVersionUID = 1L;
	@Getter
	@Setter
	public List<ChungThuSoDTO> listChungThuSo; //list chứng thư số
	
	public ChungThuSoBDTO() {
		this.listChungThuSo = new ArrayList<ChungThuSoDTO>();
	}
	
	
}
